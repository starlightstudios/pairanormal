//--Fragment half of the palette swapping routine. The texel is queried for its color, then this
//  color is checked against a list of existing colors. In case of a match, the output is changed,
//  else the texel is placed.
//--Lighting and other considerations happen after the texel color is selected. This allows the
//  palettes to precede lighting and color multiplications.
uniform sampler2D xTexSampler;

uniform int uTotalColors;
uniform vec4 uSrcColor[128];
uniform vec4 uDstColor[128];

void main(void)
{
    //--Retrieve the texel.
    vec4 tColor = texture(xTexSampler, gl_TexCoord[0].st);

    //--Query
    int tFoundAMatch = 0;
    for(int i = 0; i < uTotalColors; i ++)
    {
        if(tColor == uSrcColor[i])
        {
            tColor = uDstColor[i];
            //tColor.r = 1.0f;
            tFoundAMatch = 1;
        }
    }

    if(tFoundAMatch == 0)
    {
        //tColor.b = 1.0f;
    }

    gl_FragColor = tColor;

    //--Fetch quick-access values from the texel.
    /*int tRed = int(tColor.r * 255.0);
    int tGrn = int(tColor.g * 255.0);
    int tBlu = int(tColor.b * 255.0);
    int tAlp = int(tColor.a * 255.0);

    //--Output values
    int tOutRed = tRed;
    int tOutGrn = tGrn;
    int tOutBlu = tBlu;

    //--Check the colors against a list.
    if(     tRed ==  32 && tGrn ==  48 && tBlu == 128)
    {
        tOutRed = 120;
        tOutGrn =  56;
        tOutBlu = 184;
    }
    else if(tRed ==   0 && tGrn ==  64 && tBlu == 240)
    {
        tOutRed = 144;
        tOutGrn = 112;
        tOutBlu = 232;
    }
    else if(tRed ==   0 && tGrn == 128 && tBlu == 248)
    {
        tOutRed = 208;
        tOutGrn = 152;
        tOutBlu = 240;
    }
    else if(tRed ==  24 && tGrn ==  88 && tBlu == 176)
    {
        tOutRed = 128;
        tOutGrn = 128;
        tOutBlu = 152;
    }
    else if(tRed ==  80 && tGrn == 160 && tBlu == 240)
    {
        tOutRed = 176;
        tOutGrn = 176;
        tOutBlu = 192;
    }
    else if(tRed == 120 && tGrn == 216 && tBlu == 240)
    {
        tOutRed = 224;
        tOutGrn = 224;
        tOutBlu = 232;
    }

    //--Finally place the modified color.
    tColor.r = float(tOutRed / 255.0);
    tColor.g = float(tOutGrn / 255.0);
    tColor.b = float(tOutBlu / 255.0);
    gl_FragColor = tColor;*/
}
