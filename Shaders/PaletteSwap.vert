//--Vertex half of the palette swapping routine. The texel is queried for its color, then this
//  color is checked against a list of existing colors. In case of a match, the output is changed,
//  else the texel is placed.
//--Lighting and other considerations happen after the texel color is selected. This allows the
//  palettes to precede lighting and color multiplications.
void main(void)
{
    //--Provide the current texture coordinates for the fragment half of the
    //  shader.
    gl_TexCoord[0] = gl_MultiTexCoord0;

    //--Standard transformation.
    gl_Position = ftransform();
}
