-- |[ ================================== Pairanormal Launcher ================================== ]|
--This script handles the boot sequence of Pairanormal. It starts up the menu and loads resources needed
-- for it.
local sBasePath = fnResolvePath()

-- |[Fonts]|
--We need these fonts before creating the menu. On later loads, the fonts should already be in memory,
-- and the function will fail safely.
LM_ExecuteScript(sBasePath .. "Fonts/000 Boot Fonts.lua")

--Debug Code
gbSkipDebug = false

--Special: This lets the engine know it can load Pairanormal assets.
DL_AddPath("Root/Pairanormal/Exists/Exists/")

-- |[ ==================================== Graphics Loading ==================================== ]|
--Load all the assets used in Pairanormal.

-- |[Reader Function]|
--For image sequences which have the same base name, but count upwards.
local fnReadImages = function(sPrefix, iCount, sDLPath)
	for i = 1, iCount, 1 do
		DL_ExtractBitmap(sPrefix .. (i-1), sDLPath .. sPrefix .. (i-1))
	end
end

-- |[Backgrounds]|
--Also contains investigation icons.
SLF_Open(sBasePath .. "Datafiles/Backgrounds.slf")
DL_AddPath("Root/Images/Backgrounds/All/")
DL_AddPath("Root/Images/Investigation/Icon/")

--Main pieces.
DL_ExtractBitmap("CharPortraits", "Root/Images/Backgrounds/All/CharPortraits")

--We need to load the school exterior for the title screen.
DL_ExtractBitmap("SchoolExterior", "Root/Images/Backgrounds/All/SchoolExterior")

-- |[UI]|
SLF_Open(sBasePath .. "Datafiles/UI.slf")

--Icon Sheet
DL_AddPath("Root/Images/UI/Icons/")
DL_ExtractBitmap("Icon|XRed",               "Root/Images/UI/Icons/XRed")
DL_ExtractBitmap("Icon|ArrowLftGreen",      "Root/Images/UI/Icons/ArrowLftGreen")
DL_ExtractBitmap("Icon|XWhite",             "Root/Images/UI/Icons/XWhite")
DL_ExtractBitmap("Icon|ArrowWhite",         "Root/Images/UI/Icons/ArrowWhite")
DL_ExtractBitmap("Icon|GearWhite",          "Root/Images/UI/Icons/GearWhite")
DL_ExtractBitmap("Icon|CheckboxEmptyWhite", "Root/Images/UI/Icons/CheckboxEmptyWhite")
DL_ExtractBitmap("Icon|CheckboxFillWhite",  "Root/Images/UI/Icons/CheckboxFillWhite")
DL_ExtractBitmap("Icon|CheckboxCheckWhite", "Root/Images/UI/Icons/CheckboxCheckWhite")
DL_ExtractBitmap("Icon|TrashWhite",         "Root/Images/UI/Icons/TrashWhite")
DL_ExtractBitmap("Icon|ArrowSmallWhiteL",   "Root/Images/UI/Icons/ArrowSmallWhiteL")
DL_ExtractBitmap("Icon|ArrowSmallWhiteR",   "Root/Images/UI/Icons/ArrowSmallWhiteR")
DL_ExtractBitmap("Icon|ArrowPlayWhite",     "Root/Images/UI/Icons/ArrowPlayWhite")
DL_ExtractBitmap("Icon|ArrowFFWhite",       "Root/Images/UI/Icons/ArrowFFWhite")
DL_ExtractBitmap("Icon|VolumeWhite",        "Root/Images/UI/Icons/VolumeWhite")
DL_ExtractBitmap("Icon|VolumeBigWhite",     "Root/Images/UI/Icons/VolumeBigWhite")
DL_ExtractBitmap("Icon|BarEmpty",           "Root/Images/UI/Icons/BarEmpty")
DL_ExtractBitmap("Icon|BarIndicator",       "Root/Images/UI/Icons/BarIndicator")
DL_ExtractBitmap("Icon|BarFull",            "Root/Images/UI/Icons/BarFull")
DL_ExtractBitmap("Icon|Frame",              "Root/Images/UI/Icons/Frame")
DL_ExtractBitmap("Icon|Sun",                "Root/Images/UI/Icons/Sun")
DL_ExtractBitmap("Icon|Moon",               "Root/Images/UI/Icons/Moon")
DL_ExtractBitmap("Icon|House",              "Root/Images/UI/Icons/House")
DL_ExtractBitmap("Icon|MagnifyingGlass",    "Root/Images/UI/Icons/MagnifyingGlass")
DL_ExtractBitmap("Icon|Hand",               "Root/Images/UI/Icons/Hand")

--Boxes
DL_AddPath("Root/Images/UI/Boxes/")
DL_ExtractBitmap("Box|Black",    "Root/Images/UI/Boxes/Black")
DL_ExtractBitmap("Box|White",    "Root/Images/UI/Boxes/White")
DL_ExtractBitmap("Box|Gradient", "Root/Images/UI/Boxes/Gradient")

--Derived UI
DL_AddPath("Root/Images/UI/Derived/")

--Disclaimer.
DL_ExtractBitmap("Derived|Disclaimer",                        "Root/Images/UI/Derived/Disclaimer")
DL_ExtractBitmap("Derived|DisclaimerBtn|Ready|Selected",      "Root/Images/UI/Derived/DisclaimerBtn|Ready|Selected")
DL_ExtractBitmap("Derived|DisclaimerBtn|Ready|Unselected",    "Root/Images/UI/Derived/DisclaimerBtn|Ready|Unselected")
DL_ExtractBitmap("Derived|DisclaimerBtn|NotReady|Selected",   "Root/Images/UI/Derived/DisclaimerBtn|NotReady|Selected")
DL_ExtractBitmap("Derived|DisclaimerBtn|NotReady|Unselected", "Root/Images/UI/Derived/DisclaimerBtn|NotReady|Unselected")

-- |[ =================================== Derived UI Pieces ==================================== ]|
--Common.
DL_AddPath("Root/Images/UI/Common/")
DL_ExtractBitmap("Derived|Common|Back",     "Root/Images/UI/Common/Back")
DL_ExtractBitmap("Derived|Common|Close",    "Root/Images/UI/Common/Close")
DL_ExtractBitmap("Derived|Common|Settings", "Root/Images/UI/Common/Settings")

--Dialogue.
DL_AddPath("Root/Images/UI/Dialogue/")
DL_ExtractBitmap("Derived|Dialogue|BorderBarsBot",   "Root/Images/UI/Dialogue/BorderBarsBot")
DL_ExtractBitmap("Derived|Dialogue|BorderBarsTop",   "Root/Images/UI/Dialogue/BorderBarsTop")
DL_ExtractBitmap("Derived|Dialogue|ContinueIcon",    "Root/Images/UI/Dialogue/ContinueIcon")
DL_ExtractBitmap("Derived|Dialogue|DecisionBlack",   "Root/Images/UI/Dialogue/DecisionBlack")
DL_ExtractBitmap("Derived|Dialogue|DecisionWhite",   "Root/Images/UI/Dialogue/DecisionWhite")
DL_ExtractBitmap("Derived|Dialogue|DecisionBlackHf", "Root/Images/UI/Dialogue/DecisionBlackHf")
DL_ExtractBitmap("Derived|Dialogue|DecisionWhiteHf", "Root/Images/UI/Dialogue/DecisionWhiteHf")
DL_ExtractBitmap("Derived|Dialogue|DialogueBlack",   "Root/Images/UI/Dialogue/DialogueBlack")
DL_ExtractBitmap("Derived|Dialogue|DialogueWhite",   "Root/Images/UI/Dialogue/DialogueWhite")
DL_ExtractBitmap("Derived|Dialogue|InfoBoxBot",      "Root/Images/UI/Dialogue/InfoBoxBot")
DL_ExtractBitmap("Derived|Dialogue|InfoBoxTop",      "Root/Images/UI/Dialogue/InfoBoxTop")
DL_ExtractBitmap("Derived|Dialogue|NameBarLft",      "Root/Images/UI/Dialogue/NameBarLft")
DL_ExtractBitmap("Derived|Dialogue|NameBarRgt",      "Root/Images/UI/Dialogue/NameBarRgt")
DL_ExtractBitmap("Derived|Dialogue|Underbar",        "Root/Images/UI/Dialogue/Underbar")

--Investigation.
DL_AddPath("Root/Images/UI/Investigation/")
DL_ExtractBitmap("Derived|Investigation|Arrows",                 "Root/Images/UI/Investigation/Arrows")
DL_ExtractBitmap("Derived|Investigation|ButtonBlack",            "Root/Images/UI/Investigation/ButtonBlack")
DL_ExtractBitmap("Derived|Investigation|ButtonWhite",            "Root/Images/UI/Investigation/ButtonWhite")
DL_ExtractBitmap("Derived|Investigation|CloseupBacking",         "Root/Images/UI/Investigation/CloseupBacking")
DL_ExtractBitmap("Derived|Investigation|GotSomeToShowMeBox",     "Root/Images/UI/Investigation/GotSomeToShowMeBox")
DL_ExtractBitmap("Derived|Investigation|GotSomeToShowMeNameBox", "Root/Images/UI/Investigation/GotSomeToShowMeNameBox")
DL_ExtractBitmap("Derived|Investigation|Hand",                   "Root/Images/UI/Investigation/Hand")
DL_ExtractBitmap("Derived|Investigation|House",                  "Root/Images/UI/Investigation/House")
DL_ExtractBitmap("Derived|Investigation|IconBacking",            "Root/Images/UI/Investigation/IconBacking")
DL_ExtractBitmap("Derived|Investigation|ItemNameBacking",        "Root/Images/UI/Investigation/ItemNameBacking")
DL_ExtractBitmap("Derived|Investigation|MagnifyingLens",         "Root/Images/UI/Investigation/MagnifyingLens")
DL_ExtractBitmap("Derived|Investigation|MoreDiaLft",             "Root/Images/UI/Investigation/MoreDiaLft")
DL_ExtractBitmap("Derived|Investigation|MoreDiaRgt",             "Root/Images/UI/Investigation/MoreDiaRgt")
DL_ExtractBitmap("Derived|Investigation|TimerBarBlack",          "Root/Images/UI/Investigation/TimerBarBlack")
DL_ExtractBitmap("Derived|Investigation|TimerBarWhite",          "Root/Images/UI/Investigation/TimerBarWhite")

--Load Menu
DL_AddPath("Root/Images/UI/LoadMenu/")
DL_ExtractBitmap("Derived|LoadMenu|Backing",         "Root/Images/UI/LoadMenu/Backing")
DL_ExtractBitmap("Derived|LoadMenu|DeleteBtn",       "Root/Images/UI/LoadMenu/DeleteBtn")
DL_ExtractBitmap("Derived|LoadMenu|Header",          "Root/Images/UI/LoadMenu/Header")
DL_ExtractBitmap("Derived|LoadMenu|Pencil",          "Root/Images/UI/LoadMenu/Pencil")
DL_ExtractBitmap("Derived|LoadMenu|PlayBtn",         "Root/Images/UI/LoadMenu/PlayBtn")
DL_ExtractBitmap("Derived|LoadMenu|SelectedBacking", "Root/Images/UI/LoadMenu/SelectedBacking")

--Settings Menu.
DL_AddPath("Root/Images/UI/Settings/")
DL_ExtractBitmap("Derived|Settings|Backing",          "Root/Images/UI/Settings/Backing")
DL_ExtractBitmap("Derived|Settings|BoxChecked",       "Root/Images/UI/Settings/BoxChecked")
DL_ExtractBitmap("Derived|Settings|BoxUnchecked",     "Root/Images/UI/Settings/BoxUnchecked")
DL_ExtractBitmap("Derived|Settings|HeaderBox",        "Root/Images/UI/Settings/HeaderBox")
DL_ExtractBitmap("Derived|Settings|LowerBtnAccept",   "Root/Images/UI/Settings/LowerBtnAccept")
DL_ExtractBitmap("Derived|Settings|LowerBtnClose",    "Root/Images/UI/Settings/LowerBtnClose")
DL_ExtractBitmap("Derived|Settings|LowerBtnDefaults", "Root/Images/UI/Settings/LowerBtnDefaults")
DL_ExtractBitmap("Derived|Settings|VolumeSliders",    "Root/Images/UI/Settings/VolumeSliders")
 
--Title Menu and Gallery. 
DL_AddPath("Root/Images/UI/Title/")
DL_ExtractBitmap("Derived|Title|BoxBlack",         "Root/Images/UI/Title/BoxBlack")
DL_ExtractBitmap("Derived|Title|BoxWhite",         "Root/Images/UI/Title/BoxWhite")
DL_ExtractBitmap("Derived|Title|GalleryBlackBack", "Root/Images/UI/Title/GalleryBlackBack")
DL_ExtractBitmap("Derived|Title|GalleryBack",      "Root/Images/UI/Title/GalleryBack")
DL_ExtractBitmap("Derived|Title|GalleryLft",       "Root/Images/UI/Title/GalleryLft")
DL_ExtractBitmap("Derived|Title|GalleryRgt",       "Root/Images/UI/Title/GalleryRgt")
DL_ExtractBitmap("Derived|Title|TitleCard",        "Root/Images/UI/Title/TitleCard")

-- |[Overlays]|
DL_AddPath("Root/Images/UI/Overlays/")
DL_ExtractBitmap("VHSOverlay",     "Root/Images/UI/Overlays/VHS")
DL_ExtractBitmap("CreepyOverlay0", "Root/Images/UI/Overlays/Creepy0")
DL_ExtractBitmap("CreepyOverlay1", "Root/Images/UI/Overlays/Creepy1")
DL_ExtractBitmap("CreepyOverlay2", "Root/Images/UI/Overlays/Creepy2")
DL_ExtractBitmap("CreepyOverlay3", "Root/Images/UI/Overlays/Creepy3")

-- |[ ================================== Extraction Function =================================== ]|
--Extracts the requested number of expressions for each character.
local function fnExtractExpressions(saNameList, iaCountList)

	--Arg check.
	if(saNameList  == nil) then return end
	if(iaCountList == nil) then return end
	
	--Arrays are expected to be parallel.
	local i = 1
	while(saNameList[i] ~= nil and iaCountList[i] ~= nil) do
		
		--Add DL path for the character.
		DL_AddPath("Root/Images/Characters/" .. saNameList[i] .. "/")
		
		--Extract the expressions for the character.
		for p = 0, iaCountList[i] - 1, 1 do
			DL_ExtractBitmap(saNameList[i] .. "|Expression" .. p, "Root/Images/Characters/" .. saNameList[i] .. "/Expression" .. p)
		end
		
		--Next.
		i = i + 1
	end
end

-- |[Characters]|
--Open the SLF.
SLF_Open(sBasePath .. "Datafiles/Characters.slf")

--Build parallel arrays for extraction.
local csaNameList = {"ADAM", "AJ", "AJBackpack", "Bret", "Candace", "Cop", "Director", "Eilette", "EiletteO", "EiletteO2", "Elizabeth", "Harold", "Harper", "Kane", "Kid1", "Kid2", "Kid3", "Laura", "Mari", "MariHospital"}
local ciaCountList = {    2,    3,            1,      3,         3,     1,          1,         3,          3,           3,           1,        1,         3,     1,      1,      1,      1,       3,      3,              3}
fnExtractExpressions(csaNameList, ciaCountList)

csaNameList = {"Michelle", "Mistress", "Nelson", "Nurse", "PlayerFlash", "ReichLeif", "Ross", "Ronnie", "Six", "Squirrel", "SquirrelBob", "SquirrelDoll", "SquirrelPuppet", "Stacy", "StacyHospital", "Student1", "Student2", "Student3", "Student4", "Student5", "Student6"}
ciaCountList = {        3,          3,        1,       1,             1,           3,      3,        1,     1,          1,             1,              1,                1,       3,               3,          1,          1,          1,          1,          1,          1}
fnExtractExpressions(csaNameList, ciaCountList)

csaNameList = {"Teacher1", "Teacher2", "Teacher3", "Terry", "Three", "Thug1", "Thug2", "Todd", "YStacy", "YStacyHos", "YTodd"}
ciaCountList = {        1,          1,          1,       3,       3,       1,       1,      3,        1,           1,       1}
fnExtractExpressions(csaNameList, ciaCountList)

--Adults.
csaNameList  = {"BretAdult", "LauraAdult", "MariAdult", "MichelleAdult", "StacyAdult", "ToddAdult"}
ciaCountList = {          3,            3,           3,               3,            3,           3}
fnExtractExpressions(csaNameList, ciaCountList)

--Misc
csaNameList  = {"Cellphone", "ElectronicDiary", "Walkie", "WaiterLeif", "CookRech"}
ciaCountList = {          1,                 1,        1,            1,          1}
fnExtractExpressions(csaNameList, ciaCountList)

-- |[Scene Images]|
SLF_Open(sBasePath .. "Datafiles/Scenes.slf")

--Major Scenes
DL_AddPath("Root/Images/MajorScenes/All/")
saList = {}
saList[1] = {"BiminiLogo", "Bret", "CH2_AJEscape", "CH2_BretGym", "CH2_LauraComputer", "CH2_MariGym", "CH2_MariNecklace", "CH2_MichelleGym", "CH2_MichelleSquash", "CH2_NurseOffice", "CH2_PictureArroyo", "CH2_PictureFoster"}
saList[2] = {"CH2_Shack", "DogBelly", "EiletteConfrontation", "EiletteHit", "EiletteKapow", "FriendEnd", "FrogPhoto", "Hospital_Mari", "HospitalCry", "HospitalHug", "IntroTodd", "KaneFire", "KaneFire2"}
saList[3] = {"KolaBottle", "LauraCrush", "LoveNote", "MariCrush", "MariWeep", "Missing", "NewEilette", "Pyramid", "RollerDerby", "Sandwich", "Stacy", "StacyBricks", "StandingOver", "Todd", "TwinBirth"}
for i = 1, #saList, 1 do
    for p = 1, #saList[i], 1 do
        DL_ExtractBitmap(saList[i][p], "Root/Images/MajorScenes/All/" .. saList[i][p])
    end
end

-- |[Clean]|
SLF_Close()
Debug_PopPrint("[Loading] Graphics.slf Done\n")

-- |[ ====================================== Table Setup ======================================= ]|
-- |[Gallery Indexes]|
--Determines which code unlocks which gallery image. Chapter 1 is here.
DL_AddPath("Root/Variables/Gallery/All/")
csGalleryNames = {}
csGalleryNames[ 0] = "Gallery_SchoolExterior"
csGalleryNames[ 1] = "Gallery_Classroom"
csGalleryNames[ 2] = "Gallery_Hospital"
csGalleryNames[ 3] = "Gallery_WalkHome"
csGalleryNames[ 4] = "Gallery_LivingRoom"
csGalleryNames[ 5] = "Gallery_LivingRoomEvening"
csGalleryNames[ 6] = "Gallery_Kitchen"
csGalleryNames[ 7] = "Gallery_Forest"
csGalleryNames[ 8] = "Gallery_Bedroom"
csGalleryNames[ 9] = "Gallery_BedroomNight"
csGalleryNames[10] = "Gallery_Burning"
csGalleryNames[11] = "Gallery_Scene_Club"
csGalleryNames[12] = "Gallery_Scene_EiletteForest"
csGalleryNames[13] = "Gallery_Scene_HospitalMari"
csGalleryNames[14] = "Gallery_Scene_HospitalCry"
csGalleryNames[15] = "Gallery_Scene_HospitalHug"
csGalleryNames[16] = "Gallery_Scene_IntroTodd"
csGalleryNames[17] = "Gallery_Scene_SpoilerFire"
csGalleryNames[18] = "Gallery_Scene_SpoilerFire2"

--Chapter 2.
csGalleryNames[19] = "Gallery_ClubHouse"
csGalleryNames[20] = "Gallery_Gym"
csGalleryNames[21] = "Gallery_Hallway"
csGalleryNames[22] = "Gallery_Library"
csGalleryNames[23] = "Gallery_Office"
csGalleryNames[24] = "Gallery_Scene_AJEscape"
csGalleryNames[25] = "Gallery_Scene_BretGym"
csGalleryNames[26] = "Gallery_Scene_LauraComputer"
csGalleryNames[27] = "Gallery_Scene_MariGym"
csGalleryNames[28] = "Gallery_Scene_MariNecklace"
csGalleryNames[29] = "Gallery_Scene_MichelleGym"
csGalleryNames[30] = "Gallery_Scene_MichelleSquash"
csGalleryNames[31] = "Gallery_Scene_PictureArroyo"
csGalleryNames[32] = "Gallery_Scene_PictureFoster"

--Image paths for the unlocks.
csGalleryPaths = {}
csGalleryPaths[ 0] = "Root/Images/Backgrounds/All/SchoolExterior"
csGalleryPaths[ 1] = "Root/Images/Backgrounds/All/Classroom"
csGalleryPaths[ 2] = "Root/Images/Backgrounds/All/Hospital"
csGalleryPaths[ 3] = "Root/Images/Backgrounds/All/WalkHome"
csGalleryPaths[ 4] = "Root/Images/Backgrounds/All/LivingRoom"
csGalleryPaths[ 5] = "Root/Images/Backgrounds/All/LivingRoomEvening"
csGalleryPaths[ 6] = "Root/Images/Backgrounds/All/Kitchen"
csGalleryPaths[ 7] = "Root/Images/Backgrounds/All/Forest"
csGalleryPaths[ 8] = "Root/Images/Backgrounds/All/Bedroom"
csGalleryPaths[ 9] = "Root/Images/Backgrounds/All/BedroomNight"
csGalleryPaths[10] = "Root/Images/Backgrounds/All/Burning"

csGalleryPaths[11] = "Root/Images/MajorScenes/All/ClubChins"
csGalleryPaths[12] = "Root/Images/MajorScenes/All/EiletteKapow"
csGalleryPaths[13] = "Root/Images/MajorScenes/All/HospitalMari"
csGalleryPaths[14] = "Root/Images/MajorScenes/All/HospitalCry"
csGalleryPaths[15] = "Root/Images/MajorScenes/All/HospitalHug"
csGalleryPaths[16] = "Root/Images/MajorScenes/All/IntroTodd"
csGalleryPaths[17] = "Root/Images/MajorScenes/All/KaneFire"
csGalleryPaths[18] = "Root/Images/MajorScenes/All/KaneFire2"

--Chapter 2.
csGalleryPaths[19] = "Root/Images/Backgrounds/All/ClubHouse"
csGalleryPaths[20] = "Root/Images/Backgrounds/All/Gym"
csGalleryPaths[21] = "Root/Images/Backgrounds/All/Hallway"
csGalleryPaths[22] = "Root/Images/Backgrounds/All/Library"
csGalleryPaths[23] = "Root/Images/Backgrounds/All/Office"
csGalleryPaths[24] = "Root/Images/MajorScenes/All/CH2_AJEscape"
csGalleryPaths[25] = "Root/Images/MajorScenes/All/CH2_BretGym"
csGalleryPaths[26] = "Root/Images/MajorScenes/All/CH2_LauraComputer"
csGalleryPaths[27] = "Root/Images/MajorScenes/All/CH2_MariGym"
csGalleryPaths[28] = "Root/Images/MajorScenes/All/CH2_MariNecklace"
csGalleryPaths[29] = "Root/Images/MajorScenes/All/CH2_MichelleGym"
csGalleryPaths[30] = "Root/Images/MajorScenes/All/CH2_MichelleSquash"
csGalleryPaths[31] = "Root/Images/MajorScenes/All/CH2_PictureArroyo"
csGalleryPaths[32] = "Root/Images/MajorScenes/All/CH2_PictureFoster"

--Order the gallery to load its variable information.
Save_ReadPairanormalGalleryData(sBasePath .. "../../Saves/GalleryData.slf")

-- |[ ===================================== Audio Loading ====================================== ]|
--Call this subroutine to load the audio for Pairanormal.
LM_SetSilenceFlag(true)
	LM_ExecuteScript(sBasePath .. "Audio/ZRouting.lua")
LM_SetSilenceFlag(false)

-- |[ ======================================= Menu Setup ======================================= ]|
--Drop any events leftover after loading.
Debug_DropEvents()

--Now it's time for the main menu to actually get booted. So do that.
PairanormalMenu_SetProperty("Activate")

--Deactivate the main menu.
MapM_PushMenuStackHead()
	FlexMenu_FlagClose()
DL_PopActiveObject()
