-- |[Bret Presentation]|
--Showing stuff to Bret.

-- |[Arg Resolve]|
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--Variables.
local sScriptPath = gsRoot .. "../Chapter 3/Gameplay_SceneB/3 - Present Bret.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
local sAutoAdvance = gsRoot .. "../Chapter 3/Gameplay_SceneD/7 - PatchGoingHome.lua"

--Saving
gsSaveExecute = gsRoot .. "../Chapter 3/Gameplay_SceneA/3 - Soda Factory.lua"
gsSaveTopic = "No Topic"
gsSaveFlag = "InvestigationCola"

-- |[ ======================================= Functions ======================================== ]|
-- |[Friendship Increment]|
--If this is the first time presenting this object to this character then we increase the friendship tracker for
-- that character.
local fnIncrementFriendship = function(iObjectIndex, iIncrement)
	
    --Arg check.
    if(iObjectIndex == nil) then return end
    if(iIncrement == nil) then return end
    
    --Build strings.
	local i = iObjectIndex
    local p = iBretIndex
    local sObjectString = string.format("%02i", i)
    local sSubString    = string.format("%02i", p)
    local iIsRepeat = VM_GetVar("Root/Variables/Chapter3/InvestigationA/iObjectPresent|" .. sObjectString .. sSubString, "N")
    if(iIsRepeat == 0.0) then
        
        --Flag
        VM_SetVar("Root/Variables/Chapter3/InvestigationA/iObjectPresent|" .. sObjectString .. sSubString, "N", 1.0)
    
        --Variable gain.
        local iBretFriend  = VM_GetVar("Root/Variables/System/Romance/iBretFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iBretFriend", "N", iBretFriend + iIncrement)
    end
end

-- |[Sequence Handler]|
--Almost all sequences can use a standardized template.
local fnStdSequence = function(iFriendshipIndex, saStrings)
    
    --Start.
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    
    for i = 1, #saStrings, 1 do
		fnDialogue(saStrings[i])
    end
    
    --Finish.
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(iFriendshipIndex, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
end

-- |[ ===================================== Chat Sequences ===================================== ]|
--Chatting.
if(sTopicName == "Chat") then
	
	--Variables.
	local iChattedWithBret = VM_GetVar("Root/Variables/Chapter3/Investigation/iChattedWithBret", "N")
	
	--First pass:
	if(iChattedWithBret == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter3/Investigation/iChattedWithBret", "N", 1.0)
		
		--Text.
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
    
		fnDialogue([[[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|Happy]Oh man, can you believe it!?[P][E|Mad] I'm here--[E|Shock] I mean, we're here![P][E|Blush] This is so cool.]])
		fnDialogue([[Thought: Bret looks around, taking in the dilapidated ruins and unholy pits of sewer-like soda surrounded by hanging dolls and oh so many glaring squirrels.]])
		fnDialogue([[Bret: [E|Sad]You know what? We can just go now.]])
		fnDialogue([[Player: Bret!]])
		fnDialogue([[Bret: [E|Happy]Kidding! I'm kidding.[P][E|N] There's actually lots of incredible things to check out.]])
		fnDialogue([[Bret: But man...]])
		fnDialogue([[Thought: He bashfully meets my eye.]])
		fnDialogue([[Bret: [E|Blush]I'm [ITALICS]really[ENDITALICS] glad I'm not doing this alone.]])
        
        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
    
        --Variable gain.
        local iBretFriend  = VM_GetVar("Root/Variables/System/Romance/iBretFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iBretFriend", "N", iBretFriend + 5.0)
	
        --Time loss.
        if(fnDecrementTime(5, sAutoAdvance) == false) then
            PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
        end
		
	--Repeat passes.
	else
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
        
		fnDialogue("[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: Keep an eye out for bottlecaps!")
		fnDialogue([[Player: Player: You got it, \"Bretmeister\".]])
		fnDialogue("Bret: [E|Shock]Right![P] Coolio![P][E|Blush] I'll just be.[P].[P].[P].I'll just be standing here.")
        
        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
	end
	
-- |[ ================================== Object Presentation =================================== ]|
elseif(sTopicName == "WalkieTalkie" or sTopicName == "Laura") then
    fnStdSequence(gciInv3A_WalkieTalkie, 
    {"[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|N]Laura's walkie talkie! How is she doing?",
     "Player: Oh! Uh... great. She's great.[P] She's at home, I think.",
     "Bret: [E|Mad]That means she'll be by a computer![P][E|N] Maybe she can look stuff up for us."})
    
elseif(sTopicName == "Journal") then
    fnStdSequence(gciInv3A_Journal, 
    {"[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Player: Hey, what's this journal say?",
     "Thought: Bret takes the journal and flips to a random page.",
     [[Bret: [E|Mad]\"... I have been working on this project for a long time.[P] If I am successful, perhaps you and I can live the life I had always hoped for us...\"]],
     [[Bret: [E|Mad]\"... Wouldn't it be magnificent?\"]],
     [[Bret: [E|Shock]Hm, there's a bunch of technical drawings here.[P][E|Happy] Hey there, hoser, is this a diary or a research paper...[P][E|N]heh heh.[P] amirite?]]})

elseif(sTopicName == "DiscardedDoll") then
    fnStdSequence(gciInv3A_DiscardedDoll, 
    {[[ [HIDEALLFAST][SHOWCHAR|Bret|Neutral]Player: So... does the phrase \"first button is two stacked acorns\" mean anything to you?]],
     "Bret: [E|Shock]...[P]No?",
     "Player: Cool, cool.[P] I'll just walk around over here.",
     "Bret: [E|Happy]...",
     "Player: What are you smiling for?",
     "Bret: [E|Blush]Nothing![P][P][P][E|N] I just like how weird you are."})
    
elseif(sTopicName == "ShatteredBottle") then
    fnStdSequence(gciInv3A_ShatteredBottle, 
    {"[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|Mad]There are [ITALICS]so many[ENDITALICS] first and second edition bottles here.[P][E|Sad] Or... remnants of them, anyway."})
    
elseif(sTopicName == "SodaPool") then
    fnStdSequence(gciInv3A_SodaPool, 
    {"[HIDEALLFAST][CHARENTER|Bret]Thought: I notice Bret crouching low over one of the sickening green pools.",
     "Player: How can you get so close to those things?",
     "Bret: [E|Shock][CRASH]I WASN'T GOING TO TASTE IT![P][CRASH] I-I SWEAR!",
     "Player: What?[P] I wasn't implying--",
     "Bret: [E|Shock][CRASH]IM JUST COLLECTING A SAMPLE TO LOOK AT UNDER A MICROSCOPE LATER!!!"})

elseif(sTopicName == "Squirrel") then
    fnStdSequence(gciInv3A_Squirrel, 
    {"[HIDEALLFAST][CHARENTER|Bret]Bret: [E|Sad]There's like ton of squirrels here.",
     "Player: Maybe this is where they all live?",
     "Thought: Bret shakes his head.",
     "Bret: [E|Sad]These are grey squirells;[P] supposed to be solitary.",
     "Bret: [E|Mad] My crazy uncle Scooter used to hunt them and wear hats made of their pelts.",
     "Player: . . .",
     "Bret: [E|Happy]Heh! Craaazy old uncle Scoot.[P][P][E|Neutral] Wonder what he's up to nowadays?"})

elseif(sTopicName == "Footprints") then
    fnStdSequence(gciInv3A_Footprints, 
    {"[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|Sad]You know what? No.[P] Just no.[P][E|N] I don't see them, so I'm not going to think about them tonight."})

elseif(sTopicName == "Poster") then
    fnStdSequence(gciInv3A_Poster, 
    {"[HIDEALLFAST][CHARENTER|Bret]Bret: [E|Mad]Lots of old soda brands used to put drugs and stimulants in their formulas,[P][E|N] before that kind of thing was regulated.",
     "Bret: Whatever was in original Kane Kola probaby [ITALICS]did[ENDITALICS] make the miners stronger,[E|Shock] or at least feel like they were."})

elseif(sTopicName == "OSHASign") then
    fnStdSequence(gciInv3A_OHSASign, 
    {"[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|N]This place has been closed since, what?[P] The 50's? the 70s?[P][E|Blush] Basically before we were born."})
    
elseif(sTopicName == "SuspiciousDoll") then
    fnStdSequence(gciInv3A_SuspiciousDoll, 
    {"[HIDEALLFAST][CHARENTER|Bret]Bret: [E|Shock]Well,[P] at least this solves the paranormal club mystery of where all the victorian dolls are coming from.",
     "Player: [ITALICS]Does[ENDITALICS]it, though?",
     "Bret: [E|Sad]I prefer to call it case closed."})

elseif(sTopicName == "DollParts") then
    fnStdSequence(gciInv3A_DollParts, 
    {"[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|N]If I ever need to repair my action figures, I now know where to find some [E|Happy]free,[P][E|Mad] gently used,[P][E|Sad] seriously haunted spare parts."})

elseif(sTopicName == "Keypad") then
    fnStdSequence(gciInv3A_Keypad, 
    {"[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|Mad]If we only knew the number combination to this gate![P][E|Shock] THEN, we could get into the last standing building.",
     "Bret: [E|Sad] Oh man,[E|Mad] who knows what treasures await us in that temple of knowledge?!?"})

-- |[ ======================================= Characters ======================================= ]|
elseif(sTopicName == "Dog") then
    fnStdSequence(gciInv3A_Char_Dog, 
    {"[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Thought: Bret wiggles his fingers under the dog's nose protruding from my backpack and is rewarded with a lick.",
     "Bret: [E|Shock]Boy.[P][E|N] My cats never do that."})
    
elseif(sTopicName == "Mari") then
    fnStdSequence(gciInv3A_Char_Mari, 
    {"[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|Happy]Mari's stoked to be here.[P] I [ITALICS]TOLD[ENDITALICS] them this place was interesting!",
     "Bret:[E|N] I mean, yeah, I wasn't actually expecting it to be [E|Mad]THIS...[P]interesting.",
     "Bret:[E|N] But hey, I'll take a win where I can get it."})

elseif(sTopicName == "Stacy") then
    fnStdSequence(gciInv3A_Char_Stacy, 
    {"[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|Happy]Stacy's stoked to be here.[P] I [ITALICS]TOLD[ENDITALICS] them this place was interesting!",
     "Bret:[E|N] I mean, yeah, I wasn't actually expecting it to be [E|Mad]THIS...[P]interesting.",
     "Bret:[E|N] But hey, I'll take a win where I can get it."})
    
elseif(sTopicName == "Bret") then
    fnStdSequence(gciInv3A_Char_Bret, 
    {"[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Player: Bret, you're shaking!",
     "Bret: [E|Shock]Oh, am I?[P][E|Happy] I'm very excited to be here![P][E|N] Also maybe a little, the teeny tiniest bit...[P][P][E|Sad] scared.",
     "Bret: [E|Shock]I mean I pictured--[E|Mad]the way I pictured it in my mind before I came here...[P]uh,[P][E|Sad] I thought there would be less dolls?"})
    
elseif(sTopicName == "Michelle") then
    fnStdSequence(gciInv3A_Char_Michelle, 
    {"[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|Shock]I wonder why Michelle's over there by herself.[P] Maybe she found something interesting?"})

elseif(sTopicName == "Todd") then
    fnStdSequence(gciInv3A_Char_Todd, 
    {"[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|N]So, Todd's here.[P] Yep, that's cool.",
     "Bret: [SPEEDTEXT] He's a really smart, knowledgeable guy, [E|Mad]But it's not like if the club needed a new expert informant,[E|Sad]",
     "Bret: like I'm not gonna be replaced or anything![E|Shock] So, this is probably not gonna be a regular thing.[ENDSPEEDTEXT]",
     "Bret: [E|N] It's totally fine."})
end
