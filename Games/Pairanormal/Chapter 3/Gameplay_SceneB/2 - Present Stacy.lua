-- |[Stacy Presentation]|
--Showing stuff to Stacy.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--Variables.
local sScriptPath = gsRoot .. "../Chapter 3/Gameplay_SceneB/2 - Present Stacy.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
local sAutoAdvance = gsRoot .. "../Chapter 3/Gameplay_SceneD/7 - PatchGoingHome.lua"

--Saving
gsSaveExecute = gsRoot .. "../Chapter 3/Gameplay_SceneA/3 - Soda Factory.lua"
gsSaveTopic = "No Topic"
gsSaveFlag = "InvestigationCola"

-- |[ ======================================= Functions ======================================== ]|
-- |[Friendship Increment]|
--If this is the first time presenting this object to this character then we increase the friendship tracker for
-- that character.
local fnIncrementFriendship = function(iObjectIndex, iIncrement)
	
    --Arg check.
    if(iObjectIndex == nil) then return end
    if(iIncrement == nil) then return end
    
    --Build strings.
	local i = iObjectIndex
    local p = iStacyIndex
    local sObjectString = string.format("%02i", i)
    local sSubString    = string.format("%02i", p)
    local iIsRepeat = VM_GetVar("Root/Variables/Chapter3/InvestigationA/iObjectPresent|" .. sObjectString .. sSubString, "N")
    if(iIsRepeat == 0.0) then
        
        --Flag
        VM_SetVar("Root/Variables/Chapter3/InvestigationA/iObjectPresent|" .. sObjectString .. sSubString, "N", 1.0)
    
        --Variable gain.
        local iStacyFriend  = VM_GetVar("Root/Variables/System/Romance/iStacyFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iStacyFriend", "N", iStacyFriend + iIncrement)
    end
end

-- |[Sequence Handler]|
--Almost all sequences can use a standardized template.
local fnStdSequence = function(iFriendshipIndex, saStrings)
    
    --Start.
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    
    for i = 1, #saStrings, 1 do
		fnDialogue(saStrings[i])
    end
    
    --Finish.
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(iFriendshipIndex, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
end

-- |[ ===================================== Chat Sequences ===================================== ]|
--Chatting.
if(sTopicName == "Chat") then
	
	--Variables.
	local iChattedWithStacy = VM_GetVar("Root/Variables/Chapter3/Investigation/iChattedWithStacy", "N")
	
	--First pass:
	if(iChattedWithStacy == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter3/Investigation/iChattedWithStacy", "N", 1.0)
		
		--Text.
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
    
		fnDialogue([[[HIDEALLFAST][CHARENTER|Stacy]Player: Hey Stacy!]])
		fnDialogue([[Thought: Instead of answering, Stacy squints at the gate that bars us from the last standing building.]])
		fnDialogue([[Stacy: [E|Mad]Hang on, I've got an idea!]])
		fnDialogue([[Thought: She walks up to the gate, readies herself...[P]then expertly springs on! I can't believe how easily her steely arms latch themselves to the top bar.]])
		fnDialogue([[[CHARENTER|Mari]Mari: [E|Shock]Stacy![P][CRASH] BE CAREFUL!!!]])
		fnDialogue([[Stacy: [E|N]Don't worry! I'm just gonna try to climb over...]])
		fnDialogue([[[CHARENTER|Michelle]Michelle:[E|Happy] Yeah babe, you got it!]])
		fnDialogue([[Thought: To my surprise, Stacy actually manages to keep scrambling higher and higher,[P] until eventually she can throw one long leg over the top.]])
		fnDialogue([[Player: Hey--]])
		fnDialogue([[Stacy: [E|Mad]A-[P]almost there...]])
		fnDialogue([[Thought: All at once, the last leg which props her up against the gate begins to slip.]])
		fnDialogue([[Thought: The club can only watch as she tumbles back down on our side, landing hard in the sticky mud.]])
		fnDialogue([[Mari: [E|Shock]STACY! [P]OH MY GOSH![P][E|Sad] ARE YOU OKAY--]])
		fnDialogue([[Stacy: [E|Happy]N-no worries.[P] Oof. [P]I'm all good--whew! Just a little winded.]])
		fnDialogue([[Thought: Stacy pops back up, trying to dust herself off.]])
		fnDialogue([[Thought: I think I'm the only one who notices the worryingly large gash on her hand, which she casually hides in her jean pocket.]])
		fnDialogue([[Stacy: [E|N]*ahem* Carry on folks![P] We'll just have to find another way up there.]])

        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
    
        --Variable gain.
        local iStacyFriend  = VM_GetVar("Root/Variables/System/Romance/iStacyFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iStacyFriend", "N", iStacyFriend + 5.0)
	
        --Time loss.
        if(fnDecrementTime(5, sAutoAdvance) == false) then
            PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
        end
		
	--Repeat passes.
	else
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
        
		fnDialogue("[HIDEALLFAST][CHARENTER|Stacy]Stacy: [E|Mad]I might try to climb again, but give me a few minutes to recover, [ITALICS]tres bien?[ENDITALICS]")
		fnDialogue("Player: You [ITALICS]really[ENDITALICS] don't have to.")
        
        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
	end
	
-- |[ ================================== Object Presentation =================================== ]|
elseif(sTopicName == "WalkieTalkie" or sTopicName == "Laura") then
    fnStdSequence(gciInv3A_WalkieTalkie, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Stacy: [E|Happy]Laura lucked out--[P]she's the only one who doesn't have to deal with the smell of this place!"})
    
elseif(sTopicName == "Journal") then
    fnStdSequence(gciInv3A_Journal, 
    {[[[HIDEALLFAST][CHARENTER|Stacy]Player: Hey, what's this journal say?]],
     [[Thought: Stacy takes the journal and flips to a random page.]],
     [[Stacy: [E|Mad]\"Your little Bon-Bon is doing well--[P]she's so strong and healthy, and beautiful like her mother...\"]],
     [[Stacy: \"... I have raised her as if she was my own...\"]],
     [[Stacy: [E|Sad] \"... I look forward to the day she can meet you,[P] and we can be the family we were always meant to be.\"]]})

elseif(sTopicName == "DiscardedDoll") then
    fnStdSequence(gciInv3A_DiscardedDoll, 
    {[[[HIDEALLFAST][CHARENTER|Stacy]Player: I thought you hated victorian dolls.[P] Doesn't this creep you out even a little?]],
     [[Stacy: [E|Happy]Heh heh... Let me tell you a story, [PLAYERNAME].[P][E|Mad] There was once a BRAVE captain of a small guerilla platoon.]],
     [[Stacy: [E|N] When the enemy shot her, she said to one of her followers--[P]\"Bring me my red shirt, so none of my men see that I'm bleeding\".]],
     [[Stacy: [E|Shock] The follower came back and said,[P] \"captain![P] We've just discovered the enemy outnumbers us ten to one!\"]],
     [[Player: What did she say to that?]],
     [[Stacy: [E|Mad]\"loyal follower...[P][P][P]bring me my brown pants\".]],
     [[Thought: ...[ITALICS]What does that have to do with my question???[ENDITALICS] ]]})
    
elseif(sTopicName == "ShatteredBottle") then
    fnStdSequence(gciInv3A_ShatteredBottle, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Stacy: [E|Shock][ITALICS]ki o tsukete[ENDITALICS]![P][E|N] Broken glass is no joke, so be careful around it."})
    
elseif(sTopicName == "SodaPool") then
    fnStdSequence(gciInv3A_SodaPool, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Stacy: [E|Happy]Hah![P] this smell is nothing compared to my limburger grilled cheese sandwiches."})

elseif(sTopicName == "Squirrel") then
    fnStdSequence(gciInv3A_Squirrel, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Player: [ITALICS]That[ENDITALICS] doesn't creep you out even a little?",
     "Stacy: [E|N]Would a creeped out person do this?",
     "Thought: Stacy flexes her arms and poses. "})

elseif(sTopicName == "Footprints") then
    fnStdSequence(gciInv3A_Footprints, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Player: [ITALICS]That[ENDITALICS] doesn't creep you out even a little?",
     "Stacy: [E|N]A club captain puts on a brave face for their team.[P] It's simple as that."})

elseif(sTopicName == "Poster") then
    fnStdSequence(gciInv3A_Poster, 
    {"Stacy: [E|N]I think this soda was made for the old quartz miners back in the day. [P]Bret might know more about it."})

elseif(sTopicName == "OSHASign") then
    fnStdSequence(gciInv3A_OHSASign, 
    {[[Stacy: [E|Blush]OSHA stands for \"Occupational Safety and Health Administration\",[P] in case you're wondering.]]})
    
elseif(sTopicName == "SuspiciousDoll") then
    fnStdSequence(gciInv3A_SuspiciousDoll, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Player: [ITALICS]That[ENDITALICS]doesn't creep you out even a little?",
     "Stacy: [E|N]A club captain puts on a brave face for their team.[P] It's simple as that."})

elseif(sTopicName == "DollParts") then
    fnStdSequence(gciInv3A_DollParts, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Player: Are you sure you're okay?",
     "Stacy: [E|Happy]If I wasn't okay, could I do this?",
     "Thought: Stacy flexes her arms and poses. "})

elseif(sTopicName == "Keypad") then
    fnStdSequence(gciInv3A_Keypad, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Stacy: [E|Mad]If only we could get over that gate.",
     "Stacy: [E|Blush] I mean, I've been casually studying some lockpicking on the side.",
     "Stacy: [E|Sad] but alas, I haven't quite mastered electronic numberpad bypasses yet."})

-- |[ ======================================= Characters ======================================= ]|
elseif(sTopicName == "Dog") then
    fnStdSequence(gciInv3A_Char_Dog, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Stacy: Stacy: [E|N]Hey little fella!",
     "Thought: The dog pokes his head out of my backpack, and Stacy scratches the patch of fur right between the dog's eyes.",
     "Stacy: [P][P][P][E|Mad][ITALICS]I would die for you.[ENDITALICS]",
     "Player: Okey doke then."})
    
elseif(sTopicName == "Mari") then
    fnStdSequence(gciInv3A_Char_Mari, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Stacy: [E|N]There's Mari.[P] What a gal."})

elseif(sTopicName == "Stacy") then
    fnStdSequence(gciInv3A_Char_Stacy, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Stacy: [E|N]I'm here if you need anything."})
    
elseif(sTopicName == "Bret") then
    fnStdSequence(gciInv3A_Char_Bret, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Stacy: [E|N]Bret would've definitely chickened out if we weren't here.[P][E|Happy] I'm glad you talked us into coming!"})
    
elseif(sTopicName == "Michelle") then
    fnStdSequence(gciInv3A_Char_Michelle, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Stacy: [E|N]Don't worry about Michelle pouting over there.[P] I'm pretty sure she's not actually afraid of anything out here."})

elseif(sTopicName == "Todd") then
    fnStdSequence(gciInv3A_Char_Todd, 
    {[[[HIDEALLFAST][CHARENTER|Stacy]Stacy: [E|N]Well I'll be, Todd Krause is here--the famous \"personal tutor\" of yours.]],
     "Stacy: [E|Blush] Or maybe we should call him your [ITALICS]senpai[ENDITALICS],[P] hehehehe!",
     "Player: Yeah.[P] Don't you know him?",
     "Stacy: [E|N]Mari knows him better than I do.[P][E|Blush] But since he's your tutor, you might know more than both of us."})
end
