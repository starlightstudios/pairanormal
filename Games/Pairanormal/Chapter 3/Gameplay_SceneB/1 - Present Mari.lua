-- |[Mari Presentation]|
--Showing stuff to Mari.

-- |[Arg Resolve]|
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--Variables.
local sScriptPath = gsRoot .. "../Chapter 3/Gameplay_SceneB/1 - Present Mari.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
local sAutoAdvance = gsRoot .. "../Chapter 3/Gameplay_SceneD/7 - PatchGoingHome.lua"

--Saving
gsSaveExecute = gsRoot .. "../Chapter 3/Gameplay_SceneA/3 - Soda Factory.lua"
gsSaveTopic = "No Topic"
gsSaveFlag = "InvestigationCola"

-- |[ ======================================= Functions ======================================== ]|
-- |[Friendship Increment]|
--If this is the first time presenting this object to this character then we increase the friendship tracker for
-- that character.
local fnIncrementFriendship = function(iObjectIndex, iIncrement)
	
    --Arg check.
    if(iObjectIndex == nil) then return end
    if(iIncrement == nil) then return end
    
    --Build strings.
	local i = iObjectIndex
    local p = iMariIndex
    local sObjectString = string.format("%02i", i)
    local sSubString    = string.format("%02i", p)
    local iIsRepeat = VM_GetVar("Root/Variables/Chapter3/InvestigationA/iObjectPresent|" .. sObjectString .. sSubString, "N")
    if(iIsRepeat == 0.0) then
        
        --Flag
        VM_SetVar("Root/Variables/Chapter3/InvestigationA/iObjectPresent|" .. sObjectString .. sSubString, "N", 1.0)
    
        --Variable gain.
        local iMariFriend  = VM_GetVar("Root/Variables/System/Romance/iMariFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iMariFriend", "N", iMariFriend + iIncrement)
    end
end

-- |[Sequence Handler]|
--Almost all sequences can use a standardized template.
local fnStdSequence = function(iFriendshipIndex, saStrings)
    
    --Start.
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    
    for i = 1, #saStrings, 1 do
		fnDialogue(saStrings[i])
    end
    
    --Finish.
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(iFriendshipIndex, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
end

-- |[ ===================================== Chat Sequences ===================================== ]|
--Chatting.
if(sTopicName == "Chat") then
	
	--Variables.
	local iChattedWithMari = VM_GetVar("Root/Variables/Chapter3/Investigation/iChattedWithMari", "N")
	
	--First pass:
	if(iChattedWithMari == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter3/Investigation/iChattedWithMari", "N", 1.0)
		
		--Text.
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
    
		fnDialogue([[[HIDEALLFAST][CHARENTER|Mari]Mari: [E|Shock]Wow.[P] Wow-wow-woooooow.]])
		fnDialogue([[Player: This place really is something, isn't it?]])
		fnDialogue([[Mari: [CRASH][E|Shock]I KNOW![E|Blush] Never in all my life could I have imagined this place would be SO freakin' HAUNTED.]])
		fnDialogue([[Player: I'm surprised you guys never came here before.]])
		fnDialogue([[Mari: [E|N]It's crazy right? Bret's been talking about coming here for ages, and we always planned to come but then--]])
		fnDialogue([[Thought: Mari points to me, but immediately withdraws her hand, embarrassed.]])
		fnDialogue([[Mari: [E|Sad]Uh, we just got caught up in other stuff I guess. [P]Oh man--[P][E|Mad][CRASH]I'M A TERRIBLE FRIEND!]])
		fnDialogue([[Player: Hey, that's not true--]])
		fnDialogue([[Mari: [E|Mad][CRASH]BRET!!! I'M SORRY WE NEVER CAME HERE BEFORE!!!]])
		fnDialogue([[[CHARENTER|Bret]Bret: [E|Shock]Huh? Oh, uh, it's fine--]])
		fnDialogue([[Mari: [E|Mad][CRASH]YOU ARE SMART AND FUNNY AND I SUPPORT YOU!!!]])
		fnDialogue([[Thought: I decide to keep looking around]])
        
        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
    
        --Variable gain.
        local iMariFriend  = VM_GetVar("Root/Variables/System/Romance/iMariFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iMariFriend", "N", iMariFriend + 5.0)
	
        --Time loss.
        if(fnDecrementTime(5, sAutoAdvance) == false) then
            PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
        end
		
	--Repeat passes.
	else
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
        
		fnDialogue("[HIDEALLFAST][CHARENTER|Mari]Mari: [E|N]Keep your eyes peeled![P] This place is RIPE for investigatin'!")
		fnDialogue("Player: Got it.")
        
        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
	end
	
-- |[ ================================== Object Presentation =================================== ]|
elseif(sTopicName == "WalkieTalkie" or sTopicName == "Laura") then
    fnStdSequence(gciInv3A_WalkieTalkie, 
    {"[HIDEALLFAST][CHARENTER|Mari]Mari: [E|Blush]Laura's walkie talkie! Brings back memories of the club's early days. "})
    
elseif(sTopicName == "Journal") then
    fnStdSequence(gciInv3A_Journal, 
    {"[HIDEALLFAST][CHARENTER|Mari]Player: Hey, what's this journal say?",
     "Thought: Mari takes the journal and flips to the first page.",
     [[Mari: [E|Shock]\"My dearest Anne... I write this journal so that when you awaken, you can know who it was who never left your side...\" ]],
     [[Mari: \"... It was me, George.[P] Not my brother who so cruelly toyed with you.\"]],
     "Mari: [E|Blush] Ooh, spicy![P] A love triangle."})

elseif(sTopicName == "DiscardedDoll") then
    fnStdSequence(gciInv3A_DiscardedDoll, 
    {[[ [HIDEALLFAST][CHARENTER|Mari]Player: Does \"two stacked acorns\" mean anything to you?]],
     "Mari: [E|N]OOH! A riddle! Um... is it an infinity sign?[P][E|Happy] A CHOCOLATE BAR? ",
     "Player: No, I--", 
     "Mari: [E|Happy]TINY ACORN SNOWMAN![P] PAPER CUPS![P][CRASH] SIDEWAYS BOOBS!",
     "Player: Are you just saying random words?",
     "Mari: [E|Shock]DISNEYLAND SALTSHAKERS![P] RED SCARF![P] TIMMY'S STUCK IN THE WELL!!!"})
    
elseif(sTopicName == "ShatteredBottle") then
    fnStdSequence(gciInv3A_ShatteredBottle, 
    {"[HIDEALLFAST][CHARENTER|Mari]Mari: [E|Blush]I'm looking for haunted stuff, but I'm also looking for a bottlecap for Bret~"})
    
elseif(sTopicName == "SodaPool") then
    fnStdSequence(gciInv3A_SodaPool, 
    {"[HIDEALLFAST][CHARENTER|Mari]Player: Do you know what this is?",
     "Mari: [E|N]Nope, but can I just say--[P]you're doing a great job out here and I support you."})

elseif(sTopicName == "Squirrel") then
    fnStdSequence(gciInv3A_Squirrel, 
    {"[HIDEALLFAST][CHARENTER|Mari]Mari: [E|N]Those are squirrels!"})

elseif(sTopicName == "Footprints") then
    fnStdSequence(gciInv3A_Footprints, 
    {"[HIDEALLFAST][CHARENTER|Mari]Player: Do you know what this is?",
     "Mari: [E|N]Nope, but can I just say--[P]you're doing a great job out here and I support you."})

elseif(sTopicName == "Poster") then
    fnStdSequence(gciInv3A_Poster, 
    {"[HIDEALLFAST][CHARENTER|Mari]Player: Do you know what this is?",
     "Mari: [E|N]Nope, but can I just say--[P]you're doing a great job out here and I support you."})

elseif(sTopicName == "OSHASign") then
    fnStdSequence(gciInv3A_OHSASign, 
    {"[HIDEALLFAST][CHARENTER|Mari]Mari: [E|N]OSHA! not sure what it stands for, but it's fun to say.[P][E|Happy] OSHA, OSHA, OSHA."})
    
elseif(sTopicName == "SuspiciousDoll") then
    fnStdSequence(gciInv3A_SuspiciousDoll, 
    {[[ [HIDEALLFAST][CHARENTER|Mari]Player: Does \"second button is tree with one branch\" mean anything to you?]],
     "Mari: [E|Sad]mmmmmnope. The only buttons around here are the ones on that security lock by the gate.",
     "Mari: [E|N] But they only have numbers, not trees."})

elseif(sTopicName == "DollParts") then
    fnStdSequence(gciInv3A_DollParts, 
    {"[HIDEALLFAST][CHARENTER|Mari]Mari: [E|Shock]This [ITALICS]has[ENDITALICS] to be related to the dolls we've found around town...[P][CRASH]BUT HOW!?!?"})

elseif(sTopicName == "Keypad") then
    fnStdSequence(gciInv3A_Keypad, 
    {"[HIDEALLFAST][CHARENTER|Mari]Mari: [E|Shock]Beep boop beep.[P][E|Happy] I'm a number pad, get it?"})

-- |[ ======================================= Characters ======================================= ]|
elseif(sTopicName == "Dog") then
    fnStdSequence(gciInv3A_Char_Dog, 
    {"[HIDEALLFAST][CHARENTER|Mari]Mari: [E|Mad][CRASH]LOOK AT YOU, PRECIOUS ANGEL!!![P][E|Blush] WHO'S A GOOD BOY!?![P] WHO!?!?[P][E|Mad][CRASH] YOU ARE!?!?!",
     "[CHARENTER|AJ]Dog: See, what am teling you, hhuman??[P] True fact, everyone knows; dog is best kind dog. [P]Bigly hero, love by all."})
    
elseif(sTopicName == "Mari") then
    fnStdSequence(gciInv3A_Char_Mari, 
    {"[HIDEALLFAST][CHARENTER|Mari]Mari:[E|N] I was hoping we could come to the cemetary, but [E|Shock]HOLY CRAP THIS PLACE IS COOL!! [P][E|Blush]Like, literally, it's SO haunted right now."})

elseif(sTopicName == "Stacy") then
    fnStdSequence(gciInv3A_Char_Stacy, 
    {"[HIDEALLFAST][CHARENTER|Mari]Mari: [E|N]Stacy [ITALICS]hates[ENDITALICS] these victorian dolls, but she's like, remarkably chill right now. [P]Good for her!"})
    
elseif(sTopicName == "Bret") then
    fnStdSequence(gciInv3A_Char_Bret, 
    {"[HIDEALLFAST][CHARENTER|Mari]Mari: [E|N]I'm glad we went along with Bret, after all. Thanks for helping us decide, [PLAYERNAME]."})
    
elseif(sTopicName == "Michelle") then
    fnStdSequence(gciInv3A_Char_Michelle, 
    {"[HIDEALLFAST][SHOWCHAR|Mari|Neutral]Thought: Mari waves at Michelle, who rolls her eyes."})

elseif(sTopicName == "Todd") then
    fnStdSequence(gciInv3A_Char_Todd, 
    {"[HIDEALLFAST][SHOWCHAR|Mari|Neutral]Mari: [E|N]I haven't hung out with Todd in ages. He's usually so busy at school.[P][E|Blush] I'm kinda excited he's here!"})
end
