--[Todd Presentation]
--FLIP THE SCRIPT, ALL THE CHARACTERS DIE AT THE BEGINNING.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--Variables.
local sScriptPath = gsRoot .. "../Chapter 3/Gameplay_SceneC/7 - Present Todd.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
local sAutoAdvance = gsRoot .. "../Chapter 3/Gameplay_SceneD/7 - PatchGoingHome.lua"

--Saving
gsSaveExecute = gsRoot .. "../Chapter 3/Gameplay_SceneA/4 - Cemetary.lua"
gsSaveTopic = "No Topic"
gsSaveFlag = "InvestigationCemetary"

--[ ========================================= Functions ========================================= ]
--[Friendship Increment]
--If this is the first time presenting this object to this character then we increase the friendship tracker for
-- that character.
local fnIncrementFriendship = function(iObjectIndex, iIncrement)
	
    --Arg check.
    if(iObjectIndex == nil) then return end
    if(iIncrement == nil) then return end
    
    --Build strings.
	local i = iObjectIndex
    local p = iToddIndex
    local sObjectString = string.format("%02i", i)
    local sSubString    = string.format("%02i", p)
    local iIsRepeat = VM_GetVar("Root/Variables/Chapter3/InvestigationB/iObjectPresent|" .. sObjectString .. sSubString, "N")
    if(iIsRepeat == 0.0) then
        
        --Flag
        VM_SetVar("Root/Variables/Chapter3/InvestigationB/iObjectPresent|" .. sObjectString .. sSubString, "N", 1.0)
    
        --Variable gain.
        local iToddFriend  = VM_GetVar("Root/Variables/System/Romance/iToddFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iToddFriend", "N", iToddFriend + iIncrement)
    end
end

--[Sequence Handler]
--Almost all sequences can use a standardized template.
local fnStdSequence = function(iFriendshipIndex, saStrings)
    
    --Start.
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    
    for i = 1, #saStrings, 1 do
		fnDialogue(saStrings[i])
    end
    
    --Finish.
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(iFriendshipIndex, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
end

--[ ======================================= Chat Sequences ====================================== ]
--Chatting.
if(sTopicName == "Chat") then
	
	--Variables.
	local iChattedWithTodd = VM_GetVar("Root/Variables/Chapter3/Investigation/iChattedWithTodd", "N")
	
	--First pass:
	if(iChattedWithTodd == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter3/Investigation/iChattedWithTodd", "N", 1.0)
		
		--Text.
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
    
		fnDialogue([[[HIDEALLFAST][CHARENTER|Todd]Player: Hey Todd.[P] How... uh, how's it going?]])
		fnDialogue([[Todd: [E|N]It's going pretty great.]])
		fnDialogue([[Player: You're not creeped out by all this?]])
		fnDialogue([[Thought: Todd takes in the surroundings and shrugs.]])
		fnDialogue([[Todd: [E|Mad]I haven't lost anyone really significant, knock on wood and all that.]])
		fnDialogue([[Todd: [E|N] Actually--you know what? Being here makes me feel...[P]grateful.[P] Motivated and stuff.]])
		fnDialogue([[Thought: I try to follow along, but there's some piece of wisdom in Todd's thought that I can't grasp. ]])
		fnDialogue([[Thought: I haven't been alive long enough, I guess.]])
        
        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
    
        --Variable gain.
        local iToddFriend  = VM_GetVar("Root/Variables/System/Romance/iToddFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iToddFriend", "N", iToddFriend + 5.0)
	
        --Time loss.
        if(fnDecrementTime(5, sAutoAdvance) == false) then
            PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
        end
		
	--Repeat passes.
	else
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
        
		fnDialogue("[HIDEALLFAST][CHARENTER|Todd]Todd: [E|Mad]What are we doing here, by the way?")
		fnDialogue("Player: Just snooping around until we find something vaguely cool?[P] Bonus points if it proves the existence of ghosts.")
		fnDialogue("Todd: [E|Happy]Mmmm, sounds easy enough.[P] I'll do my best.")
        
        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
	end
	
--[ ==================================== Object Presentation ==================================== ]
elseif(sTopicName == "WalkieTalkie" or sTopicName == "Laura") then
    fnStdSequence(gciInv3A_WalkieTalkie, 
    {"[HIDEALLFAST][CHARENTER|Todd]Todd: [E|Happy]Pfff, you guys have walkie talkies and everything! I love it."})
    
elseif(sTopicName == "Journal") then
    fnStdSequence(gciInv3A_Journal, 
    {"[HIDEALLFAST][CHARENTER|Todd]Player: Can you read this journal for me?", 
     "Todd: [E|N]As your tutor, I would instead encourage you to try and read it yourself.",
     "Player: [SFX|Ugh]Laaaaame![P] I'm just gonna ask someone else."})

elseif(sTopicName == "DiscardedDoll") then
    fnStdSequence(gciInv3A_DiscardedDoll, 
    {"[HIDEALLFAST][CHARENTER|Todd]Todd: [E|Mad]I see dolls like these occassionally in thrift stores and stuff...[P]Always wondered who ends up buying them."})

elseif(sTopicName == "ShatteredBottle") then
    fnStdSequence(gciInv3A_ShatteredBottle, 
    {"[HIDEALLFAST][CHARENTER|Todd]Todd: If these glass bottles had been smashed however many years ago the factory burned down,[P] they'd have eroded by now.", 
     "Todd: [E|Shock]So...[P]is someone coming here regularly to just...[P]smash bottles? ",
     "Thought: [ITALICS]Todd's a better paranormal investigator than I gave him credit for.[ENDITALICS]"})

elseif(sTopicName == "SodaPool") then
    fnStdSequence(gciInv3A_SodaPool, 
    {"[HIDEALLFAST][CHARENTER|Todd]Todd: [E|Shock]*hurk*.[P] Nope.[P] Nope no nooo."})

elseif(sTopicName == "Squirrel") then
    fnStdSequence(gciInv3A_Squirrel, 
    {"[HIDEALLFAST][CHARENTER|Todd]Todd: [E|N]There's an old school urban legend that the squirrels in Foster High LOVE Kane Kola.", 
     "[E|Happy] They'd steal it from kids' backpacks, and break open vending machines and everything."})

elseif(sTopicName == "Footprints") then
    fnStdSequence(gciInv3A_Footprints, 
    {"[HIDEALLFAST][CHARENTER|Todd]Todd: [E|Shock]*hurk*.[P] Nope.[P] Nope no nooo."})

elseif(sTopicName == "Poster") then
    fnStdSequence(gciInv3A_Poster, 
    {"[HIDEALLFAST][CHARENTER|Todd]Todd: [E|Shock]I'm always kind of creeped out by old-school ads.[P][E|Mad] Everyone's white, and they're smiling WAY too much."})

elseif(sTopicName == "OSHASign") then
    fnStdSequence(gciInv3A_OHSASign, 
    {"[HIDEALLFAST][CHARENTER|Todd]Todd: [E|Mad]It is DEEPLY disturbing how easy it was to get into this place."})

elseif(sTopicName == "SuspiciousDoll") then
    fnStdSequence(gciInv3A_SuspiciousDoll, 
    {[[[HIDEALLFAST][CHARENTER|Todd]Player: When I say \"tree with one branch\", what comes to mind?]], 
     "Todd: [E|Sad]Hmm...[P][E|Blush]something that's really easy to draw? Heh."})

elseif(sTopicName == "DollParts") then
    fnStdSequence(gciInv3A_DollParts, 
    {"[HIDEALLFAST][CHARENTER|Todd]Todd: [E|Mad]I see dolls like these occassionally in thrift stores and stuff...[P]Always wondered who ends up buying them."})

elseif(sTopicName == "Keypad") then
    fnStdSequence(gciInv3A_Keypad, 
    {"[HIDEALLFAST][CHARENTER|Todd]Todd: [E|N]I'm surprised this thing still works."})

--[ ======================================== Characters ========================================= ]
elseif(sTopicName == "Dog") then
    fnStdSequence(gciInv3A_Char_Dog, 
    {"[HIDEALLFAST][CHARENTER|Todd]Todd: [E|Shock]Hey, that's your dog![E|N] You brought him along for the adventure, huh?"})
    
elseif(sTopicName == "Mari") then
    fnStdSequence(gciInv3A_Char_Mari, 
    {"[HIDEALLFAST][CHARENTER|Todd]Todd: [E|N]Mari and I used to be neighbors, and we'd get into all kinds of shenanigans back then.", 
     "Todd: [E|Happy] Glad to know she's still down for an adventure."})

elseif(sTopicName == "Stacy") then
    fnStdSequence(gciInv3A_Char_Stacy, 
    {"[HIDEALLFAST][CHARENTER|Todd]Todd: [E|Shock]Oh man, have you seen Stacy play like, any sport?", 
     "Todd: [E|Mad]If we had a team of Stacys, ASB's homecoming budget would be tripled."})
    
elseif(sTopicName == "Bret") then
    fnStdSequence(gciInv3A_Char_Bret, 
    {"[HIDEALLFAST][CHARENTER|Todd]Todd: [E|N]Brad and I share a chemistry class.[P] He's a great lab partner.", 
     "Player: Oh, actually, his name is Bret.",
     "Todd: [E|Shock]Wait... seriously?[P][SFX|Ugh] I've been calling him Brad for a whole year and he never corrected me."})
    
elseif(sTopicName == "Michelle") then
    fnStdSequence(gciInv3A_Char_Michelle, 
    {"[HIDEALLFAST][CHARENTER|Todd]Todd: [E|Mad]Who's that again?", 
     "Player: Her name's Michelle.[P] She's nice!",
     "Todd: [E|Shock]Well why is she standing all the way over there?",
     "Player: Probably because it's the [ITALICS]coolest[ENDITALICS] place to stand."})

elseif(sTopicName == "Todd") then
    fnStdSequence(gciInv3B_Char_Todd, 
    {"[HIDEALLFAST][CHARENTER|Todd]Todd: [E|N]Damn, I'm lookin' good."})
end
