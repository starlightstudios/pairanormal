-- |[Laura Presentation]|
--Showing stuff to Laura.

-- |[Arg Resolve]|
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--Variables.
local sScriptPath = gsRoot .. "../Chapter 3/Gameplay_SceneB/4 - Present Laura.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
local sAutoAdvance = gsRoot .. "../Chapter 3/Gameplay_SceneD/7 - PatchGoingHome.lua"

--Saving
gsSaveExecute = gsRoot .. "../Chapter 3/Gameplay_SceneA/3 - Soda Factory.lua"
gsSaveTopic = "No Topic"
gsSaveFlag = "InvestigationCola"

-- |[ ======================================= Functions ======================================== ]|
-- |[Friendship Increment]|
--If this is the first time presenting this object to this character then we increase the friendship tracker for
-- that character.
local fnIncrementFriendship = function(iObjectIndex, iIncrement)
	
    --Arg check.
    if(iObjectIndex == nil) then return end
    if(iIncrement == nil) then return end
    
    --Build strings.
	local i = iObjectIndex
    local p = iLauraIndex
    local sObjectString = string.format("%02i", i)
    local sSubString    = string.format("%02i", p)
    local iIsRepeat = VM_GetVar("Root/Variables/Chapter3/InvestigationA/iObjectPresent|" .. sObjectString .. sSubString, "N")
    if(iIsRepeat == 0.0) then
        
        --Flag
        VM_SetVar("Root/Variables/Chapter3/InvestigationA/iObjectPresent|" .. sObjectString .. sSubString, "N", 1.0)
    
        --Variable gain.
        local iLauraFriend  = VM_GetVar("Root/Variables/System/Romance/iLauraFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iLauraFriend", "N", iLauraFriend + iIncrement)
    end
end

-- |[Sequence Handler]|
--Almost all sequences can use a standardized template.
local fnStdSequence = function(iFriendshipIndex, saStrings)
    
    --Start.
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    
    for i = 1, #saStrings, 1 do
		fnDialogue(saStrings[i])
    end
    
    --Finish.
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(iFriendshipIndex, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
end

-- |[ ===================================== Chat Sequences ===================================== ]|
--Chatting.
if(sTopicName == "Chat") then
	
	--Variables.
	local iChattedWithLaura = VM_GetVar("Root/Variables/Chapter3/Investigation/iChattedWithLaura", "N")
	
	--First pass:
	if(iChattedWithLaura == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter3/Investigation/iChattedWithLaura", "N", 1.0)
		
		--Text.
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
    
		fnDialogue([[[HIDEALLFAST][CHARENTER|Walkie]Player: H-hello?]])
		fnDialogue([[Laura: H-hello![P] Hi there![P] Wow, I can't believe I can *ahem* hear you from this far out.]])
		fnDialogue([[Player: O-oh?]])
		fnDialogue([[Laura: Gosh, this brings back memories!]])
		fnDialogue([[Laura: This is h-how I used to communicate with the club--[P]hrm-- a couple years ago.[P] It's actually way more fun than texting.]])
		fnDialogue([[Player: Heh.[P] If you say so.]])
		fnDialogue([[Laura: Well, I'm on call if you need me--[P]o-oh, one more thing!]])
		fnDialogue([[Laura: D-d-DON'T press the big red button on top of the walkie, please.]])
		fnDialogue([[Thought: I glance at the handset and locate it between the speaker and the antennae.]])
		fnDialogue([[Player: Why not?]])
		fnDialogue([[Laura: It's a s-siren--[P]sets off a loud, [ITALICS]annoying[ENDITALICS] noise.[P] I can hear it, too, and I *ahem* h-HATE the sound.]])
		fnDialogue([[Laura: Sends me into an a-a-awful panic. [P]Also, since you guys are technically trespassing, you probably don't want to alert anyone to your presence.]])
		fnDialogue([[Laura: *hrm*.[P] So pretty please don't press it!]])
		fnDialogue([[Player: You got it.[P] I won't.]])
		fnDialogue([[Laura: T-this is gonna be so fun!]])

        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
    
        --Variable gain.
        local iLauraFriend  = VM_GetVar("Root/Variables/System/Romance/iLauraFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iLauraFriend", "N", iLauraFriend + 5.0)
	
        --Time loss.
        if(fnDecrementTime(5, sAutoAdvance) == false) then
            PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
        end
		
	--Repeat passes.
	else
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
        
		fnDialogue([[[HIDEALLFAST][CHARENTER|Walkie]Player: Chicmonster didn't write this part.]])
        
        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
	end
	
-- |[ ================================== Object Presentation =================================== ]|
elseif(sTopicName == "WalkieTalkie" or sTopicName == "Laura") then
    fnStdSequence(gciInv3A_WalkieTalkie, 
    {"[HIDEALLFAST][CHARENTER|Walkie]Laura: [VOICE|Laura]Roger that! over and out!",
     "Player: What's that mean?",
     "Laura: You know, I a-[P]actually have no idea."})
    
elseif(sTopicName == "Journal") then
    fnStdSequence(gciInv3A_Journal, 
    {"[HIDEALLFAST][CHARENTER|Walkie]Player: Chic didn't write any dialogue for this."})

elseif(sTopicName == "DiscardedDoll") then
    fnStdSequence(gciInv3A_DiscardedDoll, 
    {"[HIDEALLFAST][CHARENTER|Walkie]Player: I heard the club has been investigating dolls in the woods for some time now.",
     "Laura: That's right.",
     "Laura: We've found a f-few articles reporting on the phenomena since the *ahem*[P] early 80s, but only from badly-written phony magazines.",
     "Laura: Terrible journalism, if you ask me!"})

elseif(sTopicName == "ShatteredBottle") then
    fnStdSequence(gciInv3A_ShatteredBottle, 
    {"[HIDEALLFAST][CHARENTER|Walkie]Laura: Bret's collection of *hrn* kane cola bottlecaps is nearly complete.",
     "Laura: I think he's missing one of the first editions, and the miner's special edition,[P] or something like that."})

elseif(sTopicName == "SodaPool") then
    fnStdSequence(gciInv3A_SodaPool, 
    {"[HIDEALLFAST]Info: *kssssshhhht*",
     "Thought: Nothing but static. Laura must be away from the walkie right now."})

elseif(sTopicName == "Squirrel") then
    fnStdSequence(gciInv3A_Squirrel, 
    {[[ [HIDEALLFAST][CHARENTER|Walkie]Player: Does \"third button is a giant acorn\" mean anything to you?]],
     "Laura: 'fraid not.[P] M-maybe there's a button around somewhere?",
     "Player: hm."})

elseif(sTopicName == "Footprints") then
    fnStdSequence(gciInv3A_Footprints, 
    {"[HIDEALLFAST][CHARENTER|Walkie]Player: There's tons of tiny footprints coming... [P]away from the soda pools. Towards the building over there.",
     "Laura: Sounds like a solid paranormal club mystery to me. *ahem* I-I wish I was there!",
     "Laura: Or maybe not.[P] That's actually terrifying."})

elseif(sTopicName == "Poster") then
    fnStdSequence(gciInv3A_Poster, 
    {"[HIDEALLFAST]Info: *kssssshhhht*",
     "Thought: Nothing but static. Laura must be away from the walkie right now."})

elseif(sTopicName == "OSHASign") then
    fnStdSequence(gciInv3A_OHSASign, 
    {"[HIDEALLFAST][CHARENTER|Walkie]Player: There's a building a little ways up, but its been fenced off.",
     "Laura: That's probably the fortified basement.[P] I think I read something about it once from the original reports of the fire.",
     "Thought: There's a few moments of silence.",
     "Laura: Yeah, h-[P]here it is. 1976.[P] *ahem* No definitive cause, started somewhere inside by the power generators.",
     "Laura: Burnt down everything but a fortified basement.[P] No deaths reported, thank goodness. "})

elseif(sTopicName == "SuspiciousDoll") then
    fnStdSequence(gciInv3A_SuspiciousDoll, 
    {"[HIDEALLFAST][CHARENTER|Walkie]Player: I heard the club has been investigating dolls in the woods for some time now.",
     "Laura: That's right.",
     "Laura: We've found a f-few articles reporting on the phenomena since the *ahem*[P] early 80s, but only from badly-written phony magazines.",
     "Laura: Terrible journalism, if you ask me!"})

elseif(sTopicName == "DollParts") then
    fnStdSequence(gciInv3A_DollParts, 
    {"[HIDEALLFAST]Info: *kssssshhhht*",
     "Thought: Nothing but static. Laura must be away from the walkie right now."})

elseif(sTopicName == "Keypad") then
    fnStdSequence(gciInv3A_Keypad, 
    {[[ [HIDEALLFAST][CHARENTER|Walkie]Player: I don't suppose you can... \"hack\" number pads or anything like that, right?[P] I-is that a thing?]],
     "Thought: I hear Laura giggling over the walkie talkie.",
     "Laura: I wish."})

-- |[ ======================================= Characters ======================================= ]|
elseif(sTopicName == "Dog") then
    fnStdSequence(gciInv3A_Char_Dog, 
    {"[HIDEALLFAST][CHARENTER|Walkie]Laura: The only thing I regret missing about *hrm* t-this expedition,[P] is, erm... not being able to hang out with your dog s-some more!!!",
     "Thought: [ITALICS]What about hanging out with the whole club!?![ENDITALICS]"})
    
elseif(sTopicName == "Mari") then
    fnStdSequence(gciInv3A_Char_Mari, 
    {"[HIDEALLFAST][CHARENTER|Walkie]Laura: Chic didn't write presentation dialogue for this."})

elseif(sTopicName == "Stacy") then
    fnStdSequence(gciInv3A_Char_Stacy, 
    {"[HIDEALLFAST][CHARENTER|Walkie]Laura: Chic didn't write presentation dialogue for this."})
    
elseif(sTopicName == "Bret") then
    fnStdSequence(gciInv3A_Char_Bret, 
    {"[HIDEALLFAST][CHARENTER|Walkie]Laura: Bret's always wanted to go to the Soda Factory.[P] *ahem* Sure, [P]there's nothing particularly paranormal about it, but still!!",
     "Thought: I give the scattered doll parts and lurking squirrels an uneasy look.",
     "Player: You might be surprised."})
    
elseif(sTopicName == "Michelle") then
    fnStdSequence(gciInv3A_Char_Michelle, 
    {"[HIDEALLFAST][CHARENTER|Walkie]Laura: Chic didn't write presentation dialogue for this."})

elseif(sTopicName == "Todd") then
    fnStdSequence(gciInv3A_Char_Todd, 
    {"[HIDEALLFAST][CHARENTER|Walkie]Player: So, Todd's here.",
     "Laura: [P]Really?! *hrm*[P] Todd, like,[P] ASB Todd?[P] In the h-horrifying soda swamp with all of you.",
     "Player: Yeah.",
     "Laura: W-wow.[P] You'd have to be crazy to do something that drastic.",
     "Thought: [SFX|Ugh][ITALICS]What does that say about the rest of us!?[ENDITALICS]"})
end
