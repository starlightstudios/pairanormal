-- |[ ==================================== Dog Presentation ==================================== ]|
--Best presentation.

-- |[Arg Resolve]|
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--Variables.
local sScriptPath = gsRoot .. "../Chapter 3/Gameplay_SceneB/6 - Present Dog.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
local sAutoAdvance = gsRoot .. "../Chapter 3/Gameplay_SceneD/7 - PatchGoingHome.lua"

--Saving
gsSaveExecute = gsRoot .. "../Chapter 3/Gameplay_SceneA/3 - Soda Factory.lua"
gsSaveTopic = "No Topic"
gsSaveFlag = "InvestigationCola"

-- |[ ======================================= Functions ======================================== ]|
-- |[Friendship Increment]|
--If this is the first time presenting this object to this character then we increase the friendship tracker for
-- that character.
local fnIncrementFriendship = function(iObjectIndex, iIncrement)
	
    --Arg check.
    if(iObjectIndex == nil) then return end
    if(iIncrement == nil) then return end
    
    --Build strings.
	local i = iObjectIndex
    local p = iDogIndex
    local sObjectString = string.format("%02i", i)
    local sSubString    = string.format("%02i", p)
    local iIsRepeat = VM_GetVar("Root/Variables/Chapter3/InvestigationA/iObjectPresent|" .. sObjectString .. sSubString, "N")
    if(iIsRepeat == 0.0) then
        
        --Flag
        VM_SetVar("Root/Variables/Chapter3/InvestigationA/iObjectPresent|" .. sObjectString .. sSubString, "N", 1.0)
    
        --Variable gain.
        local iDogFriend  = VM_GetVar("Root/Variables/System/Romance/iDogFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iDogFriend", "N", iDogFriend + iIncrement)
    end
end

-- |[Sequence Handler]|
--Almost all sequences can use a standardized template.
local fnStdSequence = function(iFriendshipIndex, saStrings)
    
    --Start.
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    
    for i = 1, #saStrings, 1 do
		fnDialogue(saStrings[i])
    end
    
    --Finish.
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(iFriendshipIndex, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
end

-- |[ ===================================== Chat Sequences ===================================== ]|
--Chatting.
if(sTopicName == "Chat") then
	
	--Variables.
	local iChattedWithDog = VM_GetVar("Root/Variables/Chapter3/Investigation/iChattedWithDog", "N")
	
	--First pass:
	if(iChattedWithDog == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter3/Investigation/iChattedWithDog", "N", 1.0)
		
		--Text.
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
    
		fnDialogue([[[HIDEALLFAST][CHARENTER|AJ]Thought: The dog worms around in my backpack until it can poke its head out.]])
		fnDialogue([[Dog: [VOICE|Voice|AJ][SFX|Dog]Lissten closaly, humman.[P] Am feelink a little tired from all exitement and cuddles today.]])
		fnDialogue([[Dog:[VOICE|Voice|AJ] Thinking to nestle up in tattered remains of your \"home-wark\"[P] and mebbe taking a little nap.]])
		fnDialogue([[Player: I still can't believe you ate it.[P] It's just paper,[P][SPEEDTEXT] [ITALICS]and my blood sweat and tears.[ENDITALICS][ENDSPEEDTEXT] ]])
		fnDialogue([[Player: Does that really taste so good?]])
		fnDialogue([[Dog: [VOICE|Voice|AJ]No use changing subjact.]])
		fnDialogue([[Dog:[VOICE|Voice|AJ]In the mean times, you can wake me up if have snack??[P] Yes, I'll allow.]])
		fnDialogue([[Dog:[VOICE|Voice|AJ]Or need inspect something to make sure if can eat?[P] Okay, but only on case-by-case,[P] dog decide.]])
		fnDialogue([[Player: *sigh*... Sure.[P] I'll bug you if I need something.]])
		fnDialogue([[Dog:[VOICE|Voice|AJ] [SFX|Dog]Okkey, good.]])

        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
    
        --Variable gain.
        local iDogFriend  = VM_GetVar("Root/Variables/System/Romance/iDogFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iDogFriend", "N", iDogFriend + 5.0)
	
        --Time loss.
        if(fnDecrementTime(5, sAutoAdvance) == false) then
            PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
        end
		
	--Repeat passes.
	else
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
        
		fnDialogue("[HIDEALLFAST][CHARENTER|AJ]Laura: Dog: If finding alything looking maybe tasty,[P] do not hesitate.[P] Bring to me, and I will inspekkt.")
		fnDialogue("Player: Didn't you say you wanted to take a nap?")
		fnDialogue("Dog: Remember, dog nose very powerful.[P] trust me; %%100 good fact.[P] the snack cannot hide.")

        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
	end
	
-- |[ ================================== Object Presentation =================================== ]|
elseif(sTopicName == "WalkieTalkie" or sTopicName == "Laura") then
    fnStdSequence(gciInv3A_WalkieTalkie, 
    {"[HIDEALLFAST][CHARENTER|AJ]Dog: [VOICE|Voice|AJ]*sniff* *sniff*... Is no treat.[P] I have not interestt."})
    
elseif(sTopicName == "Journal") then
    fnStdSequence(gciInv3A_Journal, 
    {"[HIDEALLFAST][CHARENTER|AJ]Dog: [VOICE|Voice|AJ]*sniff* *sniff* Interasting!![P] The leather on journal very, very old.[P] Almost too old to eat, but not quite.",
     "Player: Hey! Take that out of your mouth."})

elseif(sTopicName == "DiscardedDoll") then
    fnStdSequence(gciInv3A_DiscardedDoll, 
    {"[HIDEALLFAST][CHARENTER|AJ]Dog: [VOICE|Voice|AJ]Ack, very hard to sniff here.[P] Too much, everything smellingk like squirrel and sugar."})

elseif(sTopicName == "ShatteredBottle") then
    fnStdSequence(gciInv3A_ShatteredBottle, 
    {"[HIDEALLFAST][CHARENTER|AJ]Dog: ...",
     "Thought: I guess he's dozed off a little."})

elseif(sTopicName == "SodaPool") then
    fnStdSequence(gciInv3A_SodaPool, 
    {"[HIDEALLFAST][CHARENTER|AJ]Dog: [VOICE|Voice|AJ]*sniff* *sniff*. Mm, okay.[P] I taste.",
     "Player: ABSOLUTELY NOT.",
     "Dog: [VOICE|Voice|AJ]Why say no?[P] Some strange thinkg in there, sure,[P] but MOSTly sugar, water, and squirrel pee.[P][SFX|Dog] Probbably very safe to drink!"})

elseif(sTopicName == "Squirrel") then
    fnStdSequence(gciInv3A_Squirrel, 
    {"[HIDEALLFAST][CHARENTER|AJ]Dog:[VOICE|Voice|AJ] Dog decide, too many squirrels here.[P] Tasty, yes true, but--[P]too many."})

elseif(sTopicName == "Footprints") then
    fnStdSequence(gciInv3A_Footprints, 
    {"[HIDEALLFAST][CHARENTER|AJ]Dog:[VOICE|Voice|AJ] Ack, very hard to sniff here.[P] Too much, everythingk smelling like squirrel and sugar."})

elseif(sTopicName == "Poster") then
    fnStdSequence(gciInv3A_Poster, 
    {"[HIDEALLFAST][CHARENTER|AJ]Dog: [VOICE|Voice|AJ]*sniff* *sniff*... Is no treat.[P] I have not interestt."})

elseif(sTopicName == "OSHASign") then
    fnStdSequence(gciInv3A_OHSASign, 
    {"[HIDEALLFAST][CHARENTER|AJ]Dog: [VOICE|Voice|AJ]Humans thinnk they are so smart; buildt a fence???[P] Keep out???",
     "Dog: [VOICE|Voice|AJ]But so easy to dig hole under.[P][SFX|Dog] True fact, dog can outsmartt everytim."})

elseif(sTopicName == "SuspiciousDoll") then
    fnStdSequence(gciInv3A_SuspiciousDoll, 
    {"[HIDEALLFAST][CHARENTER|AJ]Player: Don't these things creep you out?",
     "Dog: [VOICE|Voice|AJ]Do not be afraid; it is not actually tiny human madde of glass.[P] Just another humman trick.",
     "Dog: [VOICE|Voice|AJ]True fact, once time I saw--[P][CRASH]A SNAKE! in a garden.[P] But then dog realize.[P][SFX|Dog] It just a cucumber, so I eat."})

elseif(sTopicName == "DollParts") then
    fnStdSequence(gciInv3A_DollParts, 
    {"[HIDEALLFAST][CHARENTER|AJ]Dog: *sniff* *sniff*... Is no treat.[P] I have not interestt."})

elseif(sTopicName == "Keypad") then
    fnStdSequence(gciInv3A_Keypad, 
    {"[HIDEALLFAST][CHARENTER|AJ]Dog: ...",
     "Thought: I guess he's dozed off a little."})

-- |[ ======================================== Characters ========================================= ]|
elseif(sTopicName == "Dog") then
    fnStdSequence(gciInv3A_Char_Dog, 
    {"[HIDEALLFAST][CHARENTER|AJ]Dog: [VOICE|Voice|AJ][SFX|Dog]Yes, it is Me."})
    
elseif(sTopicName == "Mari") then
    fnStdSequence(gciInv3A_Char_Mari, 
    {"[HIDEALLFAST][CHARENTER|AJ]Dog: [VOICE|Voice|AJ]You say these girl is captain?[P] The one in charge???",
     "Player: Yeah, Mari and Stacy started the club.",
     "Dog:[VOICE|Voice|AJ] Then,[P] if they're being the most ressponsible, I will go home with.",
     "Dog:[VOICE|Voice|AJ] You are not fit to raise dog right.[P][CRASH] NOT GIVE ENOUGH PETTING OR FOOD.",
     "Thought: I sometimes wonder if someone else might take better care of the dog,[P] if only because they're not constantly annoyed by his talking.",
     "Dog: [VOICE|Voice|AJ]WHAT'S THAT??[P] DOG IS NOT ANNOYING. TRUE FACT, [ITALICS]EVERYONE[ENDITALICS] KNOWS.",
     "Player: Oh, right.[P] I forgot we talk to each other with [ITALICS]thoughts[ENDITALICS]."})

elseif(sTopicName == "Stacy") then
    fnStdSequence(gciInv3A_Char_Stacy, 
    {"[HIDEALLFAST][CHARENTER|AJ]Dog:[VOICE|Voice|AJ] You say these girl is captain?[P] The one in charge???",
     "Player: Yeah, Mari and Stacy started the club.",
     "Dog: [VOICE|Voice|AJ]Then,[P] if they're being the most ressponsible, I will go home with.",
     "Dog:[VOICE|Voice|AJ]You are not fit to raise dog right.[P][CRASH] NOT GIVE ENOUGH PETTING OR FOOD.",
     "Thought: I sometimes wonder if someone else might take better care of the dog,[P] if only because they're not constantly annoyed by his talking.",
     "Dog:[VOICE|Voice|AJ] WHAT'S THAT??[P] DOG IS NOT ANNOYING. TRUE FACT, [ITALICS]EVERYONE[ENDITALICS] KNOWS.",
     "Player: Oh, right.[P] I forgot we talk to each other with [ITALICS]thoughts[ENDITALICS]."})
    
elseif(sTopicName == "Bret") then
    fnStdSequence(gciInv3A_Char_Bret, 
    {"[HIDEALLFAST][CHARENTER|AJ]Dog: [VOICE|Voice|AJ]Too skinny, and smells like cat! [P]I would not eat him, not enough nourishing."})
    
elseif(sTopicName == "Michelle") then
    fnStdSequence(gciInv3A_Char_Michelle, 
    {"[HIDEALLFAST][CHARENTER|AJ]Dog: [VOICE|Voice|AJ][SFX|Dog]That's a good one.[P] Bring me to her.",
     "Player: No![P] She's been sneaking you treats all day, you'll get full and won't eat your dinner.",
     "Dog: [VOICE|Voice|AJ][CRASH]THIS KIND OF LOVE;[P] I DESERVE."})

elseif(sTopicName == "Todd") then
    fnStdSequence(gciInv3A_Char_Todd, 
    {"[HIDEALLFAST][CHARENTER|AJ]Dog: [VOICE|Voice|AJ]That ones smells very clean.[P] You sure he not cat?[P] Maybe licking self thorough every morning!"})
end
