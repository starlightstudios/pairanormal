-- |[ ============================ Kane Kola Factory Investigation ============================= ]|
--OH YEAH SPOOKY DOLLS, PEOPLE LOVE THAT.

-- |[ ======================================= Variables ======================================== ]|
-- |[Arguments]|
--Get the name of what we're investigating.
local sExamine = LM_GetScriptArgument(0)

-- |[Variable Setup]|
--Used to simplify the Add Discovered Object calls.
local sBGPath = "Root/Images/Backgrounds/All/SodaFactory"
local sScriptPath = gsRoot .. "../Chapter 3/Gameplay_SceneB/0 - ExamineObject.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
local sAutoAdvance = gsRoot .. "../Chapter 3/Gameplay_SceneD/7 - PatchGoingHome.lua"

--Saving
gsSaveExecute = gsRoot .. "../Chapter 3/Gameplay_SceneA/3 - Soda Factory.lua"
gsSaveTopic = "No Topic"
gsSaveFlag = "InvestigationCola"

-- |[ ======================================= Functions ======================================== ]|
--Stores the object as discovered.
local fnStdStore = function(i)
    
    --Get if we've examined the object before. This will be returned later.
	local iExaminedAlready = VM_GetVar("Root/Variables/Chapter3/InvestigationA/iObject|" .. gczaInv3A_List[i], "N")
    
    --Save it as a discovered object.
	Dia_SetProperty("Add Discovered Object", gczaInv3A_List[i], sBGPath, sScriptPath, gczaInv3A_List[i])
	VM_SetVar("Root/Variables/Chapter3/InvestigationA/iObject|" .. gczaInv3A_List[i], "N", 1.0)
    
    --Pass back.
    return iExaminedAlready
end

--Decrements time. Also saves the game.
local fnStdTime = function()
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(gsSaveFlag, gsSaveExecute, gsSaveTopic)
    end
    
end

--Standard handler for objects. Saves the object, prints a dialogue, advances time.
local fnStdObject = function(iIndex, sLine)
	
    --Argument check.
    if(iIndex == nil) then return end
    
	--Store it in the inventory.
	local i = iIndex
	local iExaminedAlready = fnStdStore(i)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction(sLine)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
    if(iExaminedAlready == 0.0) then fnStdTime(sSaveFlag, sSaveExecute, sSaveTopic) end
end

-- |[ =================================== Examinable Objects =================================== ]|
if(sExamine == "WalkieTalkie") then
    fnStdObject(gciInv3A_WalkieTalkie, [[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: This is Laura's old walkie talkie.[P] She wasn't here today, but she's on the other end if I need to talk.") ]])
    
elseif(sExamine == "Journal") then
    fnStdObject(gciInv3A_Journal, [[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: An old, abandoned journal in the mud.[P] [ITALICS]Just the cherry on top for a place like this[ENDITALICS].") ]])
    
elseif(sExamine == "DiscardedDoll") then
    
	--Store it in the inventory.
    local iExaminedAlready = fnStdStore(gciInv3A_DiscardedDoll)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Doll: [VOICE|Voice|Squirrel][GLITCH][GLITCHTEXT]FIRSTBUT TON[ENDGLITCHTEXT]FIRST BUTTON [GLITCH] IS TWO STACKED ACORNS.[P] BE QUIET AND THEY WONT HEAR MEEEE.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Player: A-[CRASH][HIDEALLFAST]AAAAAHHH!") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[CHARENTER|Bret]Bret: [E|Shock]What? What is it?") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Player: Y-you didn't hear that?") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Bret: [E|Shock]h-h-hear what?") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Player: Uhhh, nothing![P] I'm just a little spooked right now.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(iExaminedAlready == 0.0) then
        fnStdTime()
	end

elseif(sExamine == "ShatteredBottle") then
    
	--Store it in the inventory.
    local iExaminedAlready = fnStdStore(gciInv3A_ShatteredBottle)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: So these bottles are what the fuss is all about.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: I need to keep an eye out for one that isn't broken, then we can get the heck out of here.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(iExaminedAlready == 0.0) then
        fnStdTime()
	end

elseif(sExamine == "SodaPool") then
    fnStdObject(gciInv3A_SodaPool, [[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: This dried up pool looks ancient--and it definitely not made of water.[P] It smells... [P]sickly sweet.") ]])

elseif(sExamine == "Squirrel") then
    
	--Store it in the inventory.
    local iExaminedAlready = fnStdStore(gciInv3A_Squirrel)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST][VOICE|Voice|Squirrel]Squirrel: [GLITCH]THIRD BUTTON IS A GIANT ACORN.[P][GLITCH][GLITCHTEXT]WAAAATCH[ENDGLITCHTEXT] WATCH HUMANS CAREFULEEE") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: Oh, great. I can hear squirrel thoughts, too.[HIDEALLFAST]") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(iExaminedAlready == 0.0) then
        fnStdTime()
	end

elseif(sExamine == "Footprints") then
	
	--Dialogue.
    local iExaminedAlready = fnStdStore(gciInv3A_Footprints)
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: Lots of tiny footprints. like...[P] tiny victorian doll footprints.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: It's...[P]s-so cute! [P][ITALICS]It's cute and not scary at all[ENDITALICS].") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(iExaminedAlready == 0.0) then
        fnStdTime()
	end

elseif(sExamine == "Poster") then
    fnStdObject(gciInv3A_Poster, [[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: I can probably read these big letters...[P]'Kane Cola Makse you Stornger! Kane Cola - The Mniner's Cohice!'") ]])

elseif(sExamine == "OSHASign") then

	--Dialogue.
    local iExaminedAlready = fnStdStore(gciInv3A_OHSASign)
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: 'CLOSED'. There's lots of tiny words that I can't read at the bottom.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: But one look beyond this fence and you can pretty much guess why anyone shouldn't be here.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(iExaminedAlready == 0.0) then
        fnStdTime()
	end

elseif(sExamine == "SuspiciousDoll") then

	--Dialogue.
    local iExaminedAlready = fnStdStore(gciInv3A_SuspiciousDoll)
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Doll: [VOICE|Voice|Squirrel][GLITCH]SECOND BUTTON IS TREE WITH ONE BRANCH.[P] STAY STILL AND[GLITCH][GLITCHTEXT]SEEEMEEEE[ENDGLITCHTEXT] THEY WON'T SEE MEEEE[.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: No way. I'm not reading that doll's mind right now.[P][SFX|World|Ugh] Because dolls aren't alive! ") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(iExaminedAlready == 0.0) then
        fnStdTime()
	end

elseif(sExamine == "DollParts") then
    fnStdObject(gciInv3A_DollParts, [[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: I've seen these doll parts before...[P] And I really wish that wasn't true.") ]])

elseif(sExamine == "Keypad") then
    LM_ExecuteScript(fnResolvePath() .. "Z - Keypad.lua", "Check")

-- |[ ======================================= Characters ======================================= ]|
elseif(sExamine == "Mari") then

    --Indicator
	Dia_SetProperty("Add Discovered Object", "Mari",     sCharPath, sScriptPath, "Mari",     1322,   2, 1322 + 438,   2 + 363)
    
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: Mari looks very pleased to be here.[P] She keeps pointing at every strange thing and shouting some variant of 'Holy crap!'") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(fnDecrementTime(1, sAutoAdvance) == false) then
		PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
	end

elseif(sExamine == "Stacy") then

    --Indicator
	Dia_SetProperty("Add Discovered Object", "Stacy",    sCharPath, sScriptPath, "Stacy",     442, 367,  442 + 438, 367 + 363)
    
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: Stacy hates dolls, but she looks to be putting on a brave face for the rest of us.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(fnDecrementTime(1, sAutoAdvance) == false) then
		PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
	end

elseif(sExamine == "Bret") then

    --Indicator
	Dia_SetProperty("Add Discovered Object", "Bret",     sCharPath, sScriptPath, "Bret",        2,   2,    2 + 438,   2 + 363)
    
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: I can't tell if Bret is excited or terrified.[P] Either way, he's sweating right through his shirt.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(fnDecrementTime(1, sAutoAdvance) == false) then
		PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
	end

elseif(sExamine == "Michelle") then

    --Indicator
	Dia_SetProperty("Add Discovered Object", "Michelle", sCharPath, sScriptPath, "Michelle", 1762,   2, 1762 + 438,   2 + 363)
    
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: Michelle is hanging out as far from the action as possible while still staying in view.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: Her eyes dart around wildly, scanning the dolls and squirrels in the trees. ") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(fnDecrementTime(1, sAutoAdvance) == false) then
		PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
	end

elseif(sExamine == "Laura") then

    --Indicator
	Dia_SetProperty("Add Discovered Object", "Laura",    sCharPath, sScriptPath, "Laura",     882,   2,  882 + 438,   2 + 363)
    
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: Laura is standing by via walkie-talkie.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(fnDecrementTime(1, sAutoAdvance) == false) then
		PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
	end
    
elseif(sExamine == "Todd") then

    --Indicator
	Dia_SetProperty("Add Discovered Object", "Todd", sCharPath, sScriptPath, "Todd", 2, 367, 2 + 438, 367 + 363)
    
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Todd is here, for some reason.[P] Ugh, it feels weird mixing him with my other friends. ") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(fnDecrementTime(1, sAutoAdvance) == false) then
		PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
	end

elseif(sExamine == "Dog") then

    --Indicator
	Dia_SetProperty("Add Discovered Object", "Dog", sCharPath, sScriptPath, "Dog", 1762, 367, 1762 + 438, 367 + 363)
    
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: The goodness on this boy radiates from the backpack he's hiding in.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(fnDecrementTime(1, sAutoAdvance) == false) then
		PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
	end
end