--[Michelle Presentation]
--Showing stuff to Michelle.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--Variables.
local sScriptPath = gsRoot .. "../Chapter 3/Gameplay_SceneB/5 - Present Michelle.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
local sAutoAdvance = gsRoot .. "../Chapter 3/Gameplay_SceneD/7 - PatchGoingHome.lua"

--Saving
gsSaveExecute = gsRoot .. "../Chapter 3/Gameplay_SceneA/3 - Soda Factory.lua"
gsSaveTopic = "No Topic"
gsSaveFlag = "InvestigationCola"

--[ ========================================= Functions ========================================= ]
--[Friendship Increment]
--If this is the first time presenting this object to this character then we increase the friendship tracker for
-- that character.
local fnIncrementFriendship = function(iObjectIndex, iIncrement)
	
    --Arg check.
    if(iObjectIndex == nil) then return end
    if(iIncrement == nil) then return end
    
    --Build strings.
	local i = iObjectIndex
    local p = iMichelleIndex
    local sObjectString = string.format("%02i", i)
    local sSubString    = string.format("%02i", p)
    local iIsRepeat = VM_GetVar("Root/Variables/Chapter3/InvestigationA/iObjectPresent|" .. sObjectString .. sSubString, "N")
    if(iIsRepeat == 0.0) then
        
        --Flag
        VM_SetVar("Root/Variables/Chapter3/InvestigationA/iObjectPresent|" .. sObjectString .. sSubString, "N", 1.0)
    
        --Variable gain.
        local iMichelleFriend  = VM_GetVar("Root/Variables/System/Romance/iMichelleFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iMichelleFriend", "N", iMichelleFriend + iIncrement)
    end
end

--[Sequence Handler]
--Almost all sequences can use a standardized template.
local fnStdSequence = function(iFriendshipIndex, saStrings)
    
    --Start.
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    
    for i = 1, #saStrings, 1 do
		fnDialogue(saStrings[i])
    end
    
    --Finish.
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(iFriendshipIndex, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
end

--[ ======================================= Chat Sequences ====================================== ]
--Chatting.
if(sTopicName == "Chat") then
	
	--Variables.
	local iChattedWithMichelle = VM_GetVar("Root/Variables/Chapter3/Investigation/iChattedWithMichelle", "N")
	
	--First pass:
	if(iChattedWithMichelle == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter3/Investigation/iChattedWithMichelle", "N", 1.0)
		
		--Text.
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
    
		fnDialogue([[[HIDEALLFAST][CHARENTER|Michelle]Player: Hi Michelle.]])
		fnDialogue([[Michelle: [E|N]What's up, weirdo?]])
		fnDialogue([[Player: Just checking in, seeing how it's going, you know, stuff like that.]])
		fnDialogue([[Michelle: [E|Blush]I-It's goin' fine, I guess. ]])
		fnDialogue([[Player: This place is kind of creepy, isn't it?]])
		fnDialogue([[Thought: I take stock of my co-clubmember--[P]her skin is already pale, but today it looks a little clammy.]])
		fnDialogue([[Thought: She just scoffs, crossing her arms.]])
		fnDialogue([[Michelle: [E|N]Pfft--this is nothing. I've seen tons of horror movies that are way worse than this, y'know.[P][E|Happy] Didn't make me bat a bloody eyelash.]])
		fnDialogue([[Player: Oh. Cool. [P]Welp, I'm gonna keep looking around.]])
		fnDialogue([[Michelle: [E|Shock]W-wait!]])
		fnDialogue([[Player: [P][P][P]Yeah?]])
		fnDialogue([[Michelle: [E|Blush]Like I said, *rhm* I obviously am not bothered by this,[P][P] but if you... feel scared or creeped out or whatever, You can come stand by me.]])
		fnDialogue([[Player: Thanks, I will!]])
		fnDialogue([[Michelle: [E|Blush]F-Fine.]])
        
        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
    
        --Variable gain.
        local iMichelleFriend  = VM_GetVar("Root/Variables/System/Romance/iMichelleFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iMichelleFriend", "N", iMichelleFriend + 5.0)
	
        --Time loss.
        if(fnDecrementTime(5, sAutoAdvance) == false) then
            PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
        end
		
	--Repeat passes.
	else
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
        
		fnDialogue("[HIDEALLFAST][CHARENTER|Michelle]Player: Hey, I'm gonna hang out here for a bit.")
		fnDialogue("Michelle: [E|Happy]Cool.[P][E|Blush] I mean, whatever.")

        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
	end

--[ ==================================== Object Presentation ==================================== ]
elseif(sTopicName == "WalkieTalkie" or sTopicName == "Laura") then
    fnStdSequence(gciInv3A_WalkieTalkie, 
    {"[HIDEALLFAST][CHARENTER|Michelle]Michelle: [E|N]Heh, Laura really is the smartest club member--she figured out how to be here without experiencing the smell."})
    
elseif(sTopicName == "Journal") then
    fnStdSequence(gciInv3A_Journal, 
    {"[HIDEALLFAST][CHARENTER|Michelle]Player: Hey, what's this journal say?",
     "Thought: Michelle takes the journal and flips to the last page.",
     "Michelle: [E|N]Dunno.[P][E|Happy] Whoever wrote it stopped on October... 1976.[P][E|Shock] Huh, that's ages ago."})

elseif(sTopicName == "DiscardedDoll") then
    fnStdSequence(gciInv3A_DiscardedDoll, 
    {"[HIDEALLFAST][CHARENTER|Michelle]Michelle: [E|Shock]what.[P] the.[P] fuck.",
     "Player: Yeah."})
    
elseif(sTopicName == "ShatteredBottle") then
    fnStdSequence(gciInv3A_ShatteredBottle, 
    {"[HIDEALLFAST][CHARENTER|Michelle]Michelle: [E|Blush]Watch your step, weirdo."})
    
elseif(sTopicName == "SodaPool") then
    fnStdSequence(gciInv3A_SodaPool, 
    {"[HIDEALLFAST][CHARENTER|Michelle]Michelle: [E|Happy]hrrk.[P] Gross."})

elseif(sTopicName == "Squirrel") then
    fnStdSequence(gciInv3A_Squirrel, 
    {"[HIDEALLFAST][CHARENTER|Michelle]Michelle: [E|N]I could take them in a fight."})

elseif(sTopicName == "Footprints") then
    fnStdSequence(gciInv3A_Footprints, 
    {"[HIDEALLFAST][CHARENTER|Michelle]Player: Do you know what this is?",
     "Michelle: [E|N]The pizzicato footsteps of a creature on its way to mischief, probably. Nerd."})

elseif(sTopicName == "Poster") then
    fnStdSequence(gciInv3A_Poster, 
    {"[HIDEALLFAST][CHARENTER|Michelle]Michelle: [E|Mad]I don't know what the hell that is-- go ask the nerd!"})

elseif(sTopicName == "OSHASign") then
    fnStdSequence(gciInv3A_OHSASign, 
    {"[HIDEALLFAST][CHARENTER|Michelle]Michelle: [E|N]It's not the fences that keep people away from this place--[E|Happy] it's that rubbish smell."})
    
elseif(sTopicName == "SuspiciousDoll") then
    fnStdSequence(gciInv3A_SuspiciousDoll, 
    {[[[HIDEALLFAST][CHARENTER|Michelle]Player: Do you see a \"tree with one branch\" around here?]],
     "Michelle: [E|N]Nuh-uh.[P] All the trees are pretty full of branches, y'weirdo."})

elseif(sTopicName == "DollParts") then
    fnStdSequence(gciInv3A_DollParts, 
    {"[HIDEALLFAST][CHARENTER|Michelle]Michelle: [E|Happy]Hrrk.[P] Gross."})

elseif(sTopicName == "Keypad") then
    fnStdSequence(gciInv3A_Keypad, 
    {"[HIDEALLFAST][CHARENTER|Michelle]Thought: Michelle picks up a rock and smashes it against the rusted lock.[P] The rock clatters to the ground, ineffective.",
     "Michelle: [E|N]Ehh-- it was worth a shot."})

--[ ======================================== Characters ========================================= ]
elseif(sTopicName == "Dog") then
    fnStdSequence(gciInv3A_Char_Dog, 
    {"[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Michelle: [E|Happy]Better watch out.[P] I'll steal that dog from you if you don't treat it right."})
    
elseif(sTopicName == "Mari") then
    fnStdSequence(gciInv3A_Char_Mari, 
    {"[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Michelle: [E|N]Ah, our fearless leaders have finally led us into hell's gaping anus.[P][P][E|Shock] Brilliant."})

elseif(sTopicName == "Stacy") then
    fnStdSequence(gciInv3A_Char_Stacy, 
    {"[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Michelle: [E|N]Ah, our fearless leaders have finally led us into hell's gaping anus.[P][P][E|Shock] Brilliant."})
    
elseif(sTopicName == "Bret") then
    fnStdSequence(gciInv3A_Char_Bret, 
    {"[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Michelle: [E|Happy]Look at him go. Like a kid in a candy store."})
    
elseif(sTopicName == "Michelle") then
    fnStdSequence(gciInv3A_Char_Michelle, 
    {"[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Michelle: [E|Mad]Christ, I have been in some grimy places, but this takes the bloody cake.[P][E|Sad] My boots are ruined, for sure."})

elseif(sTopicName == "Todd") then
    fnStdSequence(gciInv3A_Char_Todd, 
    {"[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Michelle: [E|N]I don't much care for the ASB types.[P][E|Mad] Always feels like they're putting on airs."})
end
