-- |[Keypad]|
--Input the code to advance.

-- |[Arg Resolve]|
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

-- |[Check]|
--First time the player inputs something.
if(sTopicName == "Check") then
	
    --If the player has unlocked the gate:
    local iUnlockedGate = VM_GetVar("Root/Variables/Chapter3/InvestigationA/iUnlockedGate", "N")
    if(iUnlockedGate == 1.0) then

        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALL]Thought: (Go in the unlocked area?)") ]])
        fnCutsceneBlocker()
        
        local sDecisionScript = "\"" .. gsRoot .. "../Chapter 3/Gameplay_SceneB/Z - Keypad.lua\""
        fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Keep Exploring\", " .. sDecisionScript .. ", \"Explore\") ")
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Go Inside\", " .. sDecisionScript .. ", \"Inside\") ")
        fnCutsceneBlocker()
    
    
    --Prompt them to enter the code:
    else

        --Setup.
        gsCodeSoFar = ""
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALL]Thought: There's a number pad here, but I'm not sure if it works--[P]and if it does I'm not sure I want to go beyond this gate.") ]])
        fnCutsceneBlocker()
        
        --What do?
        fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (First Number?)") ]])
        fnCutsceneBlocker()

        local sDecisionScript = "\"" .. gsRoot .. "../Chapter 3/Gameplay_SceneB/Z - Keypad.lua\""
        fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"0\", " .. sDecisionScript .. ", \"First 0\") ")
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"1\", " .. sDecisionScript .. ", \"First 1\") ")
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"2\", " .. sDecisionScript .. ", \"First 2\") ")
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"3\", " .. sDecisionScript .. ", \"First 3\") ")
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"4\", " .. sDecisionScript .. ", \"First 4\") ")
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"5\", " .. sDecisionScript .. ", \"First 5\") ")
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"6\", " .. sDecisionScript .. ", \"First 6\") ")
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"7\", " .. sDecisionScript .. ", \"First 7\") ")
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"8\", " .. sDecisionScript .. ", \"First 8\") ")
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"9\", " .. sDecisionScript .. ", \"First 9\") ")
        fnCutsceneBlocker()
    end

-- |[First Key Input]|
elseif(string.sub(sTopicName, 1, 5) == "First") then

    --Storage
    gsCodeSoFar = gsCodeSoFar .. string.sub(sTopicName, 7, 7)

    --Dialogue
    fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "*Click*") ]])
	fnCutsceneBlocker()
    fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (Second Number?)") ]])
	fnCutsceneBlocker()

    local sDecisionScript = "\"" .. gsRoot .. "../Chapter 3/Gameplay_SceneB/Z - Keypad.lua\""
    fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"0\", " .. sDecisionScript .. ", \"Second 0\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"1\", " .. sDecisionScript .. ", \"Second 1\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"2\", " .. sDecisionScript .. ", \"Second 2\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"3\", " .. sDecisionScript .. ", \"Second 3\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"4\", " .. sDecisionScript .. ", \"Second 4\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"5\", " .. sDecisionScript .. ", \"Second 5\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"6\", " .. sDecisionScript .. ", \"Second 6\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"7\", " .. sDecisionScript .. ", \"Second 7\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"8\", " .. sDecisionScript .. ", \"Second 8\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"9\", " .. sDecisionScript .. ", \"Second 9\") ")
    fnCutsceneBlocker()

-- |[Second Key Input]|
elseif(string.sub(sTopicName, 1, 6) == "Second") then

    --Storage
    gsCodeSoFar = gsCodeSoFar .. string.sub(sTopicName, 8, 8)

    --Dialogue
    fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "*Click*") ]])
	fnCutsceneBlocker()
    fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (Third Number?)") ]])
	fnCutsceneBlocker()

    local sDecisionScript = "\"" .. gsRoot .. "../Chapter 3/Gameplay_SceneB/Z - Keypad.lua\""
    fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"0\", " .. sDecisionScript .. ", \"Third 0\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"1\", " .. sDecisionScript .. ", \"Third 1\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"2\", " .. sDecisionScript .. ", \"Third 2\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"3\", " .. sDecisionScript .. ", \"Third 3\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"4\", " .. sDecisionScript .. ", \"Third 4\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"5\", " .. sDecisionScript .. ", \"Third 5\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"6\", " .. sDecisionScript .. ", \"Third 6\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"7\", " .. sDecisionScript .. ", \"Third 7\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"8\", " .. sDecisionScript .. ", \"Third 8\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"9\", " .. sDecisionScript .. ", \"Third 9\") ")
    fnCutsceneBlocker()

-- |[Third Key Input]|
elseif(string.sub(sTopicName, 1, 5) == "Third") then

    --Storage
    gsCodeSoFar = gsCodeSoFar .. string.sub(sTopicName, 7, 7)

    --Dialogue
    fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "*Click*") ]])
	fnCutsceneBlocker()
    fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: Let's see...") ]])
	fnCutsceneBlocker()
    
    --Correct!
    if(gsCodeSoFar == "870") then
        VM_SetVar("Root/Variables/Chapter3/InvestigationA/iUnlockedGate", "N", 1.0)
        fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[MUSIC|Null]Info: [SFX|Beep]*beep*") ]])
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: I step back.[P] To my surpise, the mechanism attached to the fence unlocks.") ]])
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: Then, the whole gate begins to screech awake, grinding slowly but surely open.") ]])
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: The club is at my side in an instant.") ]])
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[CHARENTER|Bret][CHARENTER|Mari][CHARENTER|Todd][CHARENTER|Stacy][CHARENTER|Michelle]Bret: [E|Shock]It's open! It's open!") ]])
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Mari: [E|Shock]Amazing! How'd you figure it out!?!?") ]])
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Player: I didn't! [P]I-I was just playing with it.") ]])
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: There's an uneasy pause.[P] We all know this is no average club outing, and going deeper might be dangerous.") ]])
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: That said, it's getting late.[P] If I keep exploring, I might have to go home soon.") ]])
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALL]Thought: (Go in the unlocked area?)") ]])
        fnCutsceneBlocker()

        local sDecisionScript = "\"" .. gsRoot .. "../Chapter 3/Gameplay_SceneB/Z - Keypad.lua\""
        fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Keep Exploring\", " .. sDecisionScript .. ", \"Explore\") ")
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Go Inside\", " .. sDecisionScript .. ", \"Inside\") ")
        fnCutsceneBlocker()
        
    --NOPE!
    else
        fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: Huh, nope. That didn't work.") ]])
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
    
    end

-- |[Don't Go Inside]|
elseif(sTopicName == "Explore") then
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALL]Thought: I'll keep looking around before going in there, if at all.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()

-- |[Go Inside, The Suspense Is Killing Me]|
elseif(sTopicName == "Inside") then
    LM_ExecuteScript(fnResolvePath() .. "Z - Mistress Scene.lua", "Start")
end
