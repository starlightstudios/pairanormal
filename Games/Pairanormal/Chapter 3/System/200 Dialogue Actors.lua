-- |[ ================================ Dialogue Actor Creation ================================= ]|
--Dialogue Actors are the spine objects who appear during gameplay. They are animated and have a set
-- of emotion/expression pairs. They must be created here before they are used.
--All characters use the same set of emotions. They are either just the neutral pose if the character
-- has one emotion, or they are neutral/sad/happy/blush/mad/shocked over 3 poses.
-- The format for emotions is always charname_emotionname, with neutral being 'n'.
--These actors are in addition to the ones in Chapter 1. All actors from Chapter 1 return even if
-- Chapter 1 did not load initially.
local function fnCreateCharacter(saNameList, saLowerList, zaEmotionList, faHalfWidthList)

	--Arg check.
	if(saNameList      == nil) then return end
	if(saLowerList     == nil) then return end
	if(zaEmotionList   == nil) then return end
	if(faHalfWidthList == nil) then return end
	
	--Build the name lookups.
	local saLookupList  = {"Neutral", "Sad", "Happy", "Blush", "Mad", "Shocked"}
	local saCompareList = {"n",       "sad", "happy", "blush", "mad", "shock"}
    
    local sPairanormalBasePath = VM_GetVar("Root/Paths/System/Startup/sPairanormalPath", "S")
    local sSpinePath = sPairanormalBasePath .. "Spine/"
	
	--Arrays are expected to be parallel.
	local i = 1
	while(saNameList[i] ~= nil and saLowerList[i] ~= nil and zaEmotionList[i] ~= nil and faHalfWidthList[i] ~= nil) do
        --io.write("Creating character " .. saNameList[i] .. "\n")
        local sUseImgName = saNameList[i]
        local sUseLowerName = saLowerList[i]

		--Create the character using their uppercase name.
		Dia_CreateCharacter(saNameList[i])

			--If the character uses one expression, it's the neutral. The list should only have one entry in that case.
			if(zaEmotionList[i][2] == nil) then
                
                --Add the neutral.
                DiaChar_SetProperty("Add Expression", "Expression0", sSpinePath .. sUseImgName .. "/Expression0", "Root/Images/Characters/" .. sUseImgName .. "/Expression0", zaEmotionList[i][1], "null")
                
                --If the zeroth is just "n" then it's the default _n emotion:
                if(zaEmotionList[i][1] == "n") then
                    DiaChar_SetProperty("Remap Emotion", "Neutral", "Expression0", saLowerList[i] .. "_n")
                
                --Otherwise, use "animation".
                else
                    DiaChar_SetProperty("Remap Emotion", "Neutral", "Expression0", "animation")
                end
		
			--Character uses all six emotions.
            else
                local iEmotions = #zaEmotionList[i]
                if(iEmotions >= 2) then
                    DiaChar_SetProperty("Add Expression", "Expression0", sSpinePath .. sUseImgName .. "/Expression0", "Root/Images/Characters/" .. sUseImgName .. "/Expression0", zaEmotionList[i][1], zaEmotionList[i][2])
                end
                if(iEmotions >= 4) then
                    DiaChar_SetProperty("Add Expression", "Expression1", sSpinePath .. sUseImgName .. "/Expression1", "Root/Images/Characters/" .. sUseImgName .. "/Expression1", zaEmotionList[i][3], zaEmotionList[i][4])
                end
                if(iEmotions >= 6) then
                    DiaChar_SetProperty("Add Expression", "Expression2", sSpinePath .. sUseImgName .. "/Expression2", "Root/Images/Characters/" .. sUseImgName .. "/Expression2", zaEmotionList[i][5], zaEmotionList[i][6])
                end
				
				--Remaps
				for e = 1, 3, 1 do
					
					--Slot Lookups
					local iSlot = ((e-1)*2)+1
					
					--Name lookups
					local sLookupA = "Neutral"
					local sLookupB = "Neutral"
					for p = 1, 6, 1 do
						if(saCompareList[p] == zaEmotionList[i][iSlot+0]) then sLookupA = saLookupList[p] end
						if(saCompareList[p] == zaEmotionList[i][iSlot+1]) then sLookupB = saLookupList[p] end
					end
					
					--Set.
                    if(iEmotions >= iSlot+1) then
                        DiaChar_SetProperty("Remap Emotion", sLookupA, "Expression" .. e-1, zaEmotionList[i][iSlot+0])
                        DiaChar_SetProperty("Remap Emotion", sLookupB, "Expression" .. e-1, zaEmotionList[i][iSlot+1])
                    end
				
				end
		
			end
		
			--Other character properties.
			DiaChar_SetProperty("Half Width", faHalfWidthList[i] * 0.10)
			
			if(saNameList[i] == "Eilette") then
				DiaChar_SetProperty("Render Offset", 85)
			end
		
            --H-flipper. Must be done after all expressions are created.
            if(bRenderHFlip == true) then
                DiaChar_SetProperty("Render HFlip", true)
            end
        
		DL_PopActiveObject()

		--Next.
        --io.write("Done.\n")
		i = i + 1

	end

end

-- |[Debug]|
--Loading chapter 3 characters.
io.write("Loading chapter 3 characters.\n")

--Get the Pairanormal spine path.
local sPairanormalBasePath = VM_GetVar("Root/Paths/System/Startup/sPairanormalPath", "S")
local sSpinePath = sPairanormalBasePath .. "Spine/"

-- |[Adam]|
--Does not have a neutral pose.
Dia_CreateCharacter("ADAM")
    local sImgName = "ADAM"
    local sLower = "adam"
    DiaChar_SetProperty("Add Expression", "Expression0", sSpinePath .. sImgName .. "/Expression0", "Root/Images/Characters/" .. sImgName .. "/Expression0", "happy", "sad", "shock")
    DiaChar_SetProperty("Remap Emotion", "Neutral", "Expression0", "happy")
    DiaChar_SetProperty("Remap Emotion", "Happy",   "Expression0", "happy")
    DiaChar_SetProperty("Remap Emotion", "Sad",     "Expression0", "sad")
    DiaChar_SetProperty("Remap Emotion", "Shock",   "Expression0", "shock")
DL_PopActiveObject()

--Does not use the correct name pattern.
Dia_CreateCharacter("EiletteO2")
    sImgName = "EiletteO2"
    sLower = "eiletteo2"
    DiaChar_SetProperty("Add Expression", "Expression0", sSpinePath .. sImgName .. "/Expression0", "Root/Images/Characters/" .. sImgName .. "/Expression0", "eiletteo_happy", "eiletteo_mad")
    DiaChar_SetProperty("Add Expression", "Expression1", sSpinePath .. sImgName .. "/Expression1", "Root/Images/Characters/" .. sImgName .. "/Expression1", "eiletteo_n", "eiletteo_sad")
    DiaChar_SetProperty("Add Expression", "Expression2", sSpinePath .. sImgName .. "/Expression2", "Root/Images/Characters/" .. sImgName .. "/Expression2", "eiletteo_blush", "eiletteo_shock")
    DiaChar_SetProperty("Remap Emotion", "Happy",   "Expression0", "eiletteo_happy")
    DiaChar_SetProperty("Remap Emotion", "Mad",     "Expression0", "eiletteo_mad")
    DiaChar_SetProperty("Remap Emotion", "Neutral", "Expression1", "eiletteo_n")
    DiaChar_SetProperty("Remap Emotion", "Sad",     "Expression1", "eiletteo_sad")
    DiaChar_SetProperty("Remap Emotion", "Shock",   "Expression2", "eiletteo_shock")
    DiaChar_SetProperty("Remap Emotion", "Blush",   "Expression2", "eiletteo_blush")
DL_PopActiveObject()

--Chic didn't even try I swear.
Dia_CreateCharacter("Mistress")
    sImgName = "Mistress"
    sLower = "mistress"
    DiaChar_SetProperty("Add Expression", "Expression0", sSpinePath .. sImgName .. "/Expression0", "Root/Images/Characters/" .. sImgName .. "/Expression0", "n", "Null")
    DiaChar_SetProperty("Add Expression", "Expression1", sSpinePath .. sImgName .. "/Expression1", "Root/Images/Characters/" .. sImgName .. "/Expression1", "mad", "Null")
    DiaChar_SetProperty("Add Expression", "Expression2", sSpinePath .. sImgName .. "/Expression2", "Root/Images/Characters/" .. sImgName .. "/Expression2", "happy", "Null")
    DiaChar_SetProperty("Remap Emotion", "Neutral", "Expression0", "n")
    DiaChar_SetProperty("Remap Emotion", "Mad",     "Expression1", "mad")
    DiaChar_SetProperty("Remap Emotion", "Happy",   "Expression2", "happy")
DL_PopActiveObject()

--Three in one emote.
Dia_CreateCharacter("Six")
    sImgName = "Six"
    sLower = "six"
    DiaChar_SetProperty("Add Expression", "Expression0", sSpinePath .. sImgName .. "/Expression0", "Root/Images/Characters/" .. sImgName .. "/Expression0", "n", "happy", "sad")
    DiaChar_SetProperty("Remap Emotion", "Neutral", "Expression0", "n")
    DiaChar_SetProperty("Remap Emotion", "Sad",     "Expression0", "sad")
    DiaChar_SetProperty("Remap Emotion", "Happy",   "Expression0", "happy")
DL_PopActiveObject()

-- |[Character Listing]|
--Build a list of characters and their expression lists.
local csaNameList   = {"Director", "Elizabeth", "Kid1", "Kid2", "Kid3", "Nelson", "Nurse", "PlayerFlash", "Squirrel", "SquirrelBob", "SquirrelDoll", "SquirrelPuppet", "Ronnie", "Three", "Thug1", "Thug2", "YStacy", "YStacyHos", "YTodd", "WaiterLeif", "CookRech"}
local csaLowerList  = {"director", "elizabeth", "kid1", "kid2", "kid3", "nelson", "nurse", "playerflash", "squirrel", "squirrelbob", "squirreldoll", "squirrelpuppet", "ronnie", "three", "thug1", "thug2", "ystacy", "ystacyhos", "ytodd", "waiterleif", "cookrech"}
local cfaWidthList  = {       300,         300,    300,    300,    300,      300,     300,           300,        300,           300,            300,              300,      300,     300,     300,     300,      300,         300,    300,          300,        300}

--List of the expressions in the order they are stored in the file. It's not the same for each character.
local csaDirectorList       = {"n"}
local csaElizList           = {"animation"}
local csaKid1List           = {"animation"}
local csaKid2List           = {"animation"}
local csaKid3List           = {"animation"}
local csaNelsonList         = {"animation"}
local csaNurseList          = {"animation"}
local csaPlayerList         = {"n", "mad"}
local csaSquirrelList       = {"animation"}
local csaSquirrelBobList    = {"animation"}
local csaSquirrelDollList   = {"animation"}
local csaSquirrelPuppetList = {"animation"}
local csaRonnieList         = {"animation"}
local csaThreeList          = {"n", "happy", "mad", "sad", "blush", "shock"}
local csaThug1List          = {"animation"}
local csaThug2List          = {"animation"}
local csaYoungStacyList     = {"n"}
local csaYoungStacyHosList  = {"happy", "sad"}
local csaYoungToddList      = {"n", "shock"}
local csaWaiterLeifList     = {"n"}
local csaCookRechList       = {"n"}
local czaEmotionList = {csaDirectorList, csaElizList, csaKid1List, csaKid2List, csaKid3List, csaNelsonList, csaNurseList, csaPlayerList, csaSquirrelList, csaSquirrelBobList, csaSquirrelDollList, csaSquirrelPuppetList, csaRonnieList, csaThreeList, csaThug1List, csaThug2List, csaYoungStacyList, csaYoungStacyHosList, csaYoungToddList, csaWaiterLeifList, csaCookRechList}

--Run it.
fnCreateCharacter(csaNameList, csaLowerList, czaEmotionList, cfaWidthList)

--Aliases
Dia_SetProperty("Add Character Alias", "Three", "Serena")
