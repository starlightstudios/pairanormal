--[Chapter Variables]
--Variables that apply only to this chapter, or are used primarily in it.
DL_AddPath("Root/Variables/Chapter3/")

--[Miscellaneous]
DL_AddPath("Root/Variables/Chapter3/Misc/")

--[Common Investigation]
DL_AddPath("Root/Variables/Chapter3/Investigation/")
if(PairanormalLevel_IsLoading() == false) then
    VM_SetVar("Root/Variables/Chapter3/Investigation/iTimeMax", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter3/Investigation/iTimeLeft", "N", 0.0)
end

--[First Investigation]
--This refers to the soda factory investigation.
DL_AddPath("Root/Variables/Chapter3/InvestigationA/")
if(PairanormalLevel_IsLoading() == false) then
    VM_SetVar("Root/Variables/Chapter3/InvestigationA/iUnlockedGate", "N", 0.0)
end

--Name Lookups
gciInv3A_WalkieTalkie = 1
gciInv3A_Journal = 2
gciInv3A_DiscardedDoll = 3
gciInv3A_ShatteredBottle = 4
gciInv3A_SodaPool = 5
gciInv3A_Squirrel = 6
gciInv3A_Footprints = 7
gciInv3A_Poster = 8
gciInv3A_OHSASign = 9
gciInv3A_SuspiciousDoll = 10
gciInv3A_DollParts = 11
gciInv3A_Keypad = 12
gciInv3A_Char_Dog = 13
gciInv3A_Char_Mari = 14
gciInv3A_Char_Stacy = 15
gciInv3A_Char_Bret = 16
gciInv3A_Char_Michelle = 17
gciInv3A_Char_Todd = 18
gciInv3A_Variable_Total = 18

--Position Data.
gczaInv3A_List = {}
gczaInv3A_List[gciInv3A_WalkieTalkie]    = "WalkieTalkie"
gczaInv3A_List[gciInv3A_Journal]         = "Journal"
gczaInv3A_List[gciInv3A_DiscardedDoll]   = "DiscardedDoll"
gczaInv3A_List[gciInv3A_ShatteredBottle] = "ShatteredBottle"
gczaInv3A_List[gciInv3A_SodaPool]        = "SodaPool"
gczaInv3A_List[gciInv3A_Squirrel]        = "Squirrel"
gczaInv3A_List[gciInv3A_Footprints]      = "Footprints"
gczaInv3A_List[gciInv3A_Poster]          = "Poster"
gczaInv3A_List[gciInv3A_OHSASign]        = "OSHASign"
gczaInv3A_List[gciInv3A_SuspiciousDoll]  = "SuspiciousDoll"
gczaInv3A_List[gciInv3A_DollParts]       = "DollParts"
gczaInv3A_List[gciInv3A_Keypad]          = "Keypad"
gczaInv3A_List[gciInv3A_Char_Dog]        = "Dog"
gczaInv3A_List[gciInv3A_Char_Mari]       = "Mari"
gczaInv3A_List[gciInv3A_Char_Stacy]      = "Stacy"
gczaInv3A_List[gciInv3A_Char_Bret]       = "Bret"
gczaInv3A_List[gciInv3A_Char_Michelle]   = "Michelle"
gczaInv3A_List[gciInv3A_Char_Todd]       = "Todd"

--[Second Investigation]
--This refers to the soda factory investigation.
DL_AddPath("Root/Variables/Chapter3/InvestigationB/")
if(PairanormalLevel_IsLoading() == false) then
    VM_SetVar("Root/Variables/Chapter3/InvestigationB/iUnlockedGate", "N", 0.0)
end

--Name Lookups
gciInv3B_WalkieTalkie = 1
gciInv3B_GraveyardSign = 2
gciInv3B_KaneGrave = 3
gciInv3B_GeorgeGrave = 4
gciInv3B_GeorgeJrGrave = 5
gciInv3B_1Grave = 6
gciInv3B_2Grave = 7
gciInv3B_4Grave = 8
gciInv3B_MyGrave = 9
gciInv3B_JaniceGrave = 10
gciInv3B_AnneGrave = 11
gciInv3B_EiletteGrave = 12
gciInv3B_FroggyStationary = 13
gciInv3B_Journal = 14
gciInv3B_Lock = 15
gciInv3B_Char_Dog = 16
gciInv3B_Char_Mari = 17
gciInv3B_Char_Stacy = 18
gciInv3B_Char_Bret = 19
gciInv3B_Char_Michelle = 20
gciInv3B_Char_Todd = 21
gciInv3B_Variable_Total = 21

--Position Data.
gczaInv3B_List = {}
gczaInv3B_List[gciInv3B_WalkieTalkie]     = "WalkieTalkie"
gczaInv3B_List[gciInv3B_GraveyardSign]    = "GraveSign"
gczaInv3B_List[gciInv3B_KaneGrave]        = "KaneGrave"
gczaInv3B_List[gciInv3B_GeorgeGrave]      = "GeorgeGrave"
gczaInv3B_List[gciInv3B_GeorgeJrGrave]    = "GeorgeJRGrave"
gczaInv3B_List[gciInv3B_1Grave]           = "1Grave"
gczaInv3B_List[gciInv3B_2Grave]           = "2Grave"
gczaInv3B_List[gciInv3B_4Grave]           = "4Grave"
gczaInv3B_List[gciInv3B_MyGrave]          = "MyGrave"
gczaInv3B_List[gciInv3B_JaniceGrave]      = "JaniceGrave"
gczaInv3B_List[gciInv3B_AnneGrave]        = "AnneGrave"
gczaInv3B_List[gciInv3B_EiletteGrave]     = "EiletteGrave"
gczaInv3B_List[gciInv3B_FroggyStationary] = "FroggyStationary"
gczaInv3B_List[gciInv3B_Journal]          = "Journal"
gczaInv3B_List[gciInv3B_Lock]             = "Lock"
gczaInv3B_List[gciInv3B_Char_Dog]         = "Dog"
gczaInv3B_List[gciInv3B_Char_Mari]        = "Mari"
gczaInv3B_List[gciInv3B_Char_Stacy]       = "Stacy"
gczaInv3B_List[gciInv3B_Char_Bret]        = "Bret"
gczaInv3B_List[gciInv3B_Char_Michelle]    = "Michelle"
gczaInv3B_List[gciInv3B_Char_Todd]        = "Todd"

--[Backgrounds]
--List of backgrounds.
SLF_Open(fnResolvePath() .. "../../Datafiles/Backgrounds.slf")
local saList = {"Museum", "Classroom", "SidewalkNight", "Forest", "KaneKolaFactory", "WalkHome", "Graveyard", "SodaFactory", "SquirrelCouncil", "SchoolExterior", "LivingRoom", "BedroomNight", "Car", "ShadowBuilding", "StacyRoom"}

for i = 1, #saList, 1 do
    DL_ExtractBitmap(saList[i], "Root/Images/Backgrounds/All/" .. saList[i])
end
SLF_Close()

