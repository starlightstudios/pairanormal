--[Dog Presentation]
--Deg knows all. Always trust.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--Variables.
local sScriptPath = gsRoot .. "../Chapter 3/Gameplay_SceneC/6 - Present Dog.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
local sAutoAdvance = gsRoot .. "../Chapter 3/Gameplay_SceneD/7 - PatchGoingHome.lua"

--Saving
gsSaveExecute = gsRoot .. "../Chapter 3/Gameplay_SceneA/4 - Cemetary.lua"
gsSaveTopic = "No Topic"
gsSaveFlag = "InvestigationCemetary"

--[ ========================================= Functions ========================================= ]
--[Friendship Increment]
--If this is the first time presenting this object to this character then we increase the friendship tracker for
-- that character.
local fnIncrementFriendship = function(iObjectIndex, iIncrement)
	
    --Arg check.
    if(iObjectIndex == nil) then return end
    if(iIncrement == nil) then return end
    
    --Build strings.
	local i = iObjectIndex
    local p = iDogIndex
    local sObjectString = string.format("%02i", i)
    local sSubString    = string.format("%02i", p)
    local iIsRepeat = VM_GetVar("Root/Variables/Chapter3/InvestigationA/iObjectPresent|" .. sObjectString .. sSubString, "N")
    if(iIsRepeat == 0.0) then
        
        --Flag
        VM_SetVar("Root/Variables/Chapter3/InvestigationA/iObjectPresent|" .. sObjectString .. sSubString, "N", 1.0)
    
        --Variable gain.
        local iDogFriend  = VM_GetVar("Root/Variables/System/Romance/iDogFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iDogFriend", "N", iDogFriend + iIncrement)
    end
end

--[Sequence Handler]
--Almost all sequences can use a standardized template.
local fnStdSequence = function(iFriendshipIndex, saStrings)
    
    --Start.
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    
    for i = 1, #saStrings, 1 do
		fnDialogue(saStrings[i])
    end
    
    --Finish.
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(iFriendshipIndex, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
end

--[ ======================================= Chat Sequences ====================================== ]
--Chatting.
if(sTopicName == "Chat") then
	
	--Variables.
	local iChattedWithDog = VM_GetVar("Root/Variables/Chapter3/Investigation/iChattedWithDog", "N")
	
	--First pass:
	if(iChattedWithDog == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter3/Investigation/iChattedWithDog", "N", 1.0)
		
		--Text.
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
    
		fnDialogue([[[HIDEALLFAST][CHARENTER|AJ]Thought: The dog worms around in my backpack until it can poke its head out.]])
		fnDialogue([[Dog: [SFX|Dog]Lissten closaly, humman.[P] Am feelink a little tired from all exitement and cuddles today.]])
		fnDialogue([[Dog: Thinking to nestle up in tattered remains of your \"home-wark\"[P] and mebbe taking a little nap.]])
		fnDialogue([[Player: I still can't believe you ate it.[P] It's just paper,[P][SPEEDTEXT] [ITALICS]and my blood sweat and tears.[ENDITALICS][ENDSPEEDTEXT] ]])
		fnDialogue([[Player: Does that really taste so good?]])
		fnDialogue([[Dog: No use changing subjact.]])
		fnDialogue([[In the mean times, you can wake me up if have snack??[P] Yes, I'll allow.]])
		fnDialogue([[Or need inspect something to make sure if can eat?[P] Okay, but only on case-by-case,[P] dog decide.]])
		fnDialogue([[Player: *sigh*... Sure.[P] I'll bug you if I need something.]])
		fnDialogue([[Dog: [SFX|Dog]Okkey, good.]])
        
        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
    
        --Variable gain.
        local iDogFriend  = VM_GetVar("Root/Variables/System/Romance/iDogFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iDogFriend", "N", iDogFriend + 5.0)
	
        --Time loss.
        if(fnDecrementTime(5, sAutoAdvance) == false) then
            PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
        end
		
	--Repeat passes.
	else
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
        
		fnDialogue("[HIDEALLFAST][CHARENTER|AJ]Laura: Dog: If finding alything looking maybe tasty,[P] do not hesitate.[P] Bring to me, and I will inspekkt.")
		fnDialogue("Player: Didn't you say you wanted to take a nap?")
		fnDialogue("Dog: Remember, dog nose very powerful.[P] trust me; %%100 good fact.[P] the snack cannot hide.")
        
        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
	end
	
--[ ==================================== Object Presentation ==================================== ]
elseif(sTopicName == "WalkieTalkie" or sTopicName == "Laura") then
    fnStdSequence(gciInv3B_WalkieTalkie, 
    {"[HIDEALLFAST][CHARENTER|AJ]Dog: Is not treat, dog cannot feign interest. Apologies."})
    
elseif(sTopicName == "GraveSign") then
    fnStdSequence(gciInv3B_GraveyardSign, 
    {"[HIDEALLFAST][CHARENTER|AJ]Dog: I has seen dead bird, dead cat, and dead squirrel; [P]but never other dog.", 
     "Dog: [SFX|Dog] Dog decide: probably live forever."})

elseif(sTopicName == "KaneGrave") then
    fnStdSequence(gciInv3B_KaneGrave, 
    {"[HIDEALLFAST][CHARENTER|AJ]Dog: *sniff* *sniff*[P][P] Mmm, smelling other dogs here.", 
     "Player: You mean dogs are buried in this graveyard?",
     "Dog: No no,[P] I meaningk dogs have peed on this."})

elseif(sTopicName == "GeorgeGrave") then
    fnStdSequence(gciInv3B_GeorgeGrave, 
    {"[HIDEALLFAST][CHARENTER|AJ]Dog: ...", 
     "Thought: I guess he's dozed off a little."})

elseif(sTopicName == "GeorgeJRGrave") then
    fnStdSequence(gciInv3B_GeorgeJrGrave, 
    {"[HIDEALLFAST][CHARENTER|AJ]Dog: Oooh yes, I can dig here.", 
     "Player: Absolutely not.",
     "Dog: But this nose smell bones!!![P] Very tasty treat for tummy!"})

elseif(sTopicName == "1Grave") then
    fnStdSequence(gciInv3B_1Grave, 
    {"[HIDEALLFAST][CHARENTER|AJ]Dog: *sniff* *sniff*... This one smels like other stones here. [P] Maybbe, same person visiting?"})

elseif(sTopicName == "2Grave") then
    fnStdSequence(gciInv3B_2Grave, 
    {"[HIDEALLFAST][CHARENTER|AJ]Dog: mm, don't smell too much.[P] Sure there is dead humann burietd in it?"})

elseif(sTopicName == "4Grave") then
    fnStdSequence(gciInv3B_4Grave, 
    {"[HIDEALLFAST][CHARENTER|AJ]Dog: *sniff* ... This one smels like other stones here. [P] Maybbe, same person visiting?"})

elseif(sTopicName == "MyGrave") then
    fnStdSequence(gciInv3B_MyGrave, 
    {"[HIDEALLFAST][CHARENTER|AJ]Dog: *sniff* *sniff* just other dead human?[P] Not have much interest.", 
     "Player: ... That's[GLITCH] my name there. Does it... smell like me?",
     "Dog: No, smells like a dead humman.[P][SFX|Dog] True fact: you are live."})

elseif(sTopicName == "JaniceGrave") then
    fnStdSequence(gciInv3B_JaniceGrave, 
    {"[HIDEALLFAST][CHARENTER|AJ]Dog: ...", 
     "Thought: I guess he's dozed off a little."})

elseif(sTopicName == "AnneGrave") then
    fnStdSequence(gciInv3B_AnneGrave, 
    {"[HIDEALLFAST][CHARENTER|AJ]Dog: ...", 
     "Thought: I guess he's dozed off a little."})

elseif(sTopicName == "EiletteGrave") then
    fnStdSequence(gciInv3B_EiletteGrave, 
    {"[HIDEALLFAST][CHARENTER|AJ]Dog: *sniff*[P][P]... Where have dog smelled this before?[P] Very recent, but not placeing!!"})


elseif(sTopicName == "FroggyStationary") then
    fnStdSequence(gciInv3B_FroggyStationary, 
    {[[[HIDEALLFAST][CHARENTER|AJ]Player: What's a \"bad year?\"]], 
     [[Dog: [SFX|Dog]What mean \"yaer\"?]],
     "Player: Oh right, we're both totally clueless."})

elseif(sTopicName == "Journal") then
    fnStdSequence(gciInv3B_Journal, 
    {"[HIDEALLFAST][CHARENTER|AJ]Dog: *sniff* *sniff*[P][P] Interasting!! The leather on journal very, very old.[P] Almost too old to eat, but not quite.", 
     "Player: Hey! Take that out of your mouth."})

elseif(sTopicName == "Lock") then
    fnStdSequence(gciInv3B_Lock, 
    {"[HIDEALLFAST][CHARENTER|AJ]Dog: [SFX|Dog]LET IN! I am smelling many strange things.[P] Want to peek inside!", 
     "Player: Relax, it's probably just a handful of gardening tools, and...[P]I dunno, spare tombstones?"})

--[ ======================================== Characters ========================================= ]
elseif(sTopicName == "Dog") then
    fnStdSequence(gciInv3A_Char_Dog, 
    {"[HIDEALLFAST][CHARENTER|AJ]Dog: [SFX|Dog]Yes, it is Me."})
    
elseif(sTopicName == "Mari") then
    fnStdSequence(gciInv3A_Char_Mari, 
    {"[HIDEALLFAST][CHARENTER|AJ]Dog: You say these girl is captain?[P] The one in charge???",
     "Player: Yeah, Mari and Stacy started the club.",
     "Dog: Then,[P] if they're being the most ressponsible, I will go home with.",
     "Dog: You are not fit to raise dog right.[P][CRASH] NOT GIVE ENOUGH PETTING OR FOOD.",
     "Thought: I sometimes wonder if someone else might take better care of the dog,[P] if only because they're not constantly annoyed by his talking.",
     "Dog: WHAT'S THAT??[P] DOG IS NOT ANNOYING. TRUE FACT, [ITALICS]EVERYONE[ENDITALICS] KNOWS.",
     "Player: Oh, right.[P] I forgot we talk to each other with [ITALICS]thoughts[ENDITALICS]."})

elseif(sTopicName == "Stacy") then
    fnStdSequence(gciInv3A_Char_Stacy, 
    {"[HIDEALLFAST][CHARENTER|AJ]Dog: You say these girl is captain?[P] The one in charge???",
     "Player: Yeah, Mari and Stacy started the club.",
     "Dog: Then,[P] if they're being the most ressponsible, I will go home with.",
     "Dog: You are not fit to raise dog right.[P][CRASH] NOT GIVE ENOUGH PETTING OR FOOD.",
     "Thought: I sometimes wonder if someone else might take better care of the dog,[P] if only because they're not constantly annoyed by his talking.",
     "Dog: WHAT'S THAT??[P] DOG IS NOT ANNOYING. TRUE FACT, [ITALICS]EVERYONE[ENDITALICS] KNOWS.",
     "Player: Oh, right.[P] I forgot we talk to each other with [ITALICS]thoughts[ENDITALICS]."})
    
elseif(sTopicName == "Bret") then
    fnStdSequence(gciInv3A_Char_Bret, 
    {"[HIDEALLFAST][CHARENTER|AJ]Dog: Too skinny, and smells like cat! [P]I would not eat him, not enough nourishing."})
    
elseif(sTopicName == "Michelle") then
    fnStdSequence(gciInv3A_Char_Michelle, 
    {"[HIDEALLFAST][CHARENTER|AJ]Dog: [SFX|Dog]That's a good one.[P] Bring me to her.",
     "Player: No![P] She's been sneaking you treats all day, you'll get full and won't eat your dinner.",
     "Dog: [CRASH]THIS KIND OF LOVE;[P] I DESERVE."})

elseif(sTopicName == "Todd") then
    fnStdSequence(gciInv3A_Char_Todd, 
    {"[HIDEALLFAST][CHARENTER|AJ]Dog: That ones smells very clean.[P] You sure he not cat?[P] Maybe licking self thorough every morning!"})
end
