--[Michelle Presentation]
--Showing stuff to Michelle.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--Variables.
local sScriptPath = gsRoot .. "../Chapter 3/Gameplay_SceneC/5 - Present Michelle.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
local sAutoAdvance = gsRoot .. "../Chapter 3/Gameplay_SceneD/7 - PatchGoingHome.lua"

--Saving
gsSaveExecute = gsRoot .. "../Chapter 3/Gameplay_SceneA/4 - Cemetary.lua"
gsSaveTopic = "No Topic"
gsSaveFlag = "InvestigationCemetary"

--[ ========================================= Functions ========================================= ]
--[Friendship Increment]
--If this is the first time presenting this object to this character then we increase the friendship tracker for
-- that character.
local fnIncrementFriendship = function(iObjectIndex, iIncrement)
	
    --Arg check.
    if(iObjectIndex == nil) then return end
    if(iIncrement == nil) then return end
    
    --Build strings.
	local i = iObjectIndex
    local p = iMichelleIndex
    local sObjectString = string.format("%02i", i)
    local sSubString    = string.format("%02i", p)
    local iIsRepeat = VM_GetVar("Root/Variables/Chapter3/InvestigationB/iObjectPresent|" .. sObjectString .. sSubString, "N")
    if(iIsRepeat == 0.0) then
        
        --Flag
        VM_SetVar("Root/Variables/Chapter3/InvestigationB/iObjectPresent|" .. sObjectString .. sSubString, "N", 1.0)
    
        --Variable gain.
        local iMichelleFriend  = VM_GetVar("Root/Variables/System/Romance/iMichelleFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iMichelleFriend", "N", iMichelleFriend + iIncrement)
    end
end

--[Sequence Handler]
--Almost all sequences can use a standardized template.
local fnStdSequence = function(iFriendshipIndex, saStrings)
    
    --Start.
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    
    for i = 1, #saStrings, 1 do
		fnDialogue(saStrings[i])
    end
    
    --Finish.
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(iFriendshipIndex, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
end

--[ ======================================= Chat Sequences ====================================== ]
--Chatting.
if(sTopicName == "Chat") then
	
	--Variables.
	local iChattedWithMichelle = VM_GetVar("Root/Variables/Chapter3/Investigation/iChattedWithMichelle", "N")
	
	--First pass:
	if(iChattedWithMichelle == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter3/Investigation/iChattedWithMichelle", "N", 1.0)
		
		--Text.
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
    
		fnDialogue([[[HIDEALLFAST][CHARENTER|Michelle]Player: Hey Michelle.]])
		fnDialogue([[Michelle: [E|Happy]Yeah, whats'it?]])
		fnDialogue([[Player: Nothing! I'm just saying hi.]])
		fnDialogue([[Michelle: [E|Mad]Well, [ITALICS]I'm[ENDITALICS] just saying we shouldn't have gone to the bloody cemetery, all right?[P] I don't know why the hell you picked this place.]])
		fnDialogue([[Player: Look,[P] I'm sure no matter what kind of ghosts are here, they're not the bloodthirsty kind.]])
		fnDialogue([[Michelle: [E|N]Are you actually teasing me right now?]])
		fnDialogue([[Michelle: [E|Happy]You really have the Christ-On-A-Bike NERVE to torture an innocent bystander who is terrified...[P][E|Shock]*ahem* c-[P][E|Blush]cut it out.]])
		fnDialogue([[Player: You're right.[P] I'm sorry--want a hug? ]])
		fnDialogue([[Michelle: [E|Blush]You're the wooooooorst.]])
		fnDialogue([[Player: I'm leaving![P] I'm leaving.]])
        
        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
    
        --Variable gain.
        local iMichelleFriend  = VM_GetVar("Root/Variables/System/Romance/iMichelleFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iMichelleFriend", "N", iMichelleFriend + 5.0)
	
        --Time loss.
        if(fnDecrementTime(5, sAutoAdvance) == false) then
            PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
        end
		
	--Repeat passes.
	else
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
        
		fnDialogue("[HIDEALLFAST][CHARENTER|Michelle]Player: Can I ask just one question about--")
		fnDialogue("Michelle: [E|N]Nope.")
		fnDialogue("Player: Cool.[P] Off I go.")
        
        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
	end
	
--[ ==================================== Object Presentation ==================================== ]
elseif(sTopicName == "WalkieTalkie" or sTopicName == "Laura") then
    fnStdSequence(gciInv3B_WalkieTalkie, 
    {"[HIDEALLFAST][CHARENTER|Michelle]Michelle: [E|N]Glad to see Laura managed to tag along. In her own way."})

elseif(sTopicName == "GraveSign") then
    fnStdSequence(gciInv3B_GraveyardSign, 
    {"[HIDEALLFAST][CHARENTER|Michelle]Michelle: [E|N]When I'm dead, I want to be cremated.[P][E|Shock] And maybe mixed into some paint.", 
     "Michelle: [E|Happy] Yeah, then I could turn into [ITALICS]ghost paint[ENDITALICS]."})

elseif(sTopicName == "KaneGrave") then
    fnStdSequence(gciInv3B_KaneGrave, 
    {"[HIDEALLFAST][CHARENTER|Michelle]Michelle: [E|N]A bit cocky to be buried under a giant statue of yourself.[P][E|Mad] Ugh, rich people."})
    
elseif(sTopicName == "GeorgeGrave") then
    fnStdSequence(gciInv3B_GeorgeGrave, 
    {"[HIDEALLFAST][CHARENTER|Michelle]Michelle: [E|N]How should I know who that is? Ask a nerd."})
 
elseif(sTopicName == "GeorgeJRGrave") then
    fnStdSequence(gciInv3B_GeorgeJrGrave, 
    {"[HIDEALLFAST][CHARENTER|Michelle]Michelle: [E|Blush]What do you want, weirdo?", 
     "Player: Nothing! Nevermind."})

elseif(sTopicName == "1Grave") then
    fnStdSequence(gciInv3B_1Grave, 
    {"[HIDEALLFAST][CHARENTER|Michelle]Michelle: [E|N]How should I know who that is? Ask a nerd."})

elseif(sTopicName == "2Grave") then
    fnStdSequence(gciInv3B_2Grave, 
    {[[[HIDEALLFAST][CHARENTER|Michelle]Michelle: [E|N] See here, that 'II' is a roman numeral. Means \"two\".[P][E|Happy] You're welcome.]]})

elseif(sTopicName == "4Grave") then
    fnStdSequence(gciInv3B_4Grave, 
    {"[HIDEALLFAST][CHARENTER|Michelle]Michelle: [E|N]How should I know who that is? Ask a nerd."})

elseif(sTopicName == "MyGrave") then
    fnStdSequence(gciInv3B_MyGrave, 
    {"[HIDEALLFAST][CHARENTER|Michelle]Player: Why does this gravestone have my name on it?", 
     "Michelle: [E|Sad]I-I don't know, all right?[P] It's none of my business."})

elseif(sTopicName == "JaniceGrave") then
    fnStdSequence(gciInv3B_JaniceGrave, 
    {"[HIDEALLFAST][CHARENTER|Michelle]Michelle: [E|N]How come no one ever writes something [ITALICS]interesting[ENDITALICS] on a tombstone?", 
     [[Michelle: [E|Happy] You could do a riddle, or some cool song lyrics, or just the word \"cock\"?]]})

elseif(sTopicName == "AnneGrave") then
    fnStdSequence(gciInv3B_AnneGrave, 
    {"[HIDEALLFAST][CHARENTER|Michelle]Michelle: [E|N]How should I know who that is? Ask a nerd."})

elseif(sTopicName == "EiletteGrave") then
    fnStdSequence(gciInv3B_EiletteGrave, 
    {[[[HIDEALLFAST][CHARENTER|Michelle]Michelle: [E|N]\"Don't smile unless you really mean it.\" [P][E|Happy]Now [ITALICS]that's[ENDITALICS] good advice.[P] Well done.]]})

elseif(sTopicName == "FroggyStationary") then
    fnStdSequence(gciInv3B_FroggyStationary, 
    {"[HIDEALLFAST][CHARENTER|Michelle]Michelle: [E|Mad]Gross, who throws away trash in a cemetary?"})

elseif(sTopicName == "Journal") then
    fnStdSequence(gciInv3B_Journal, 
    {[[[HIDEALLFAST][CHARENTER|Michelle]Player: Hey, Michelle.[P] Will you read this for me?]], 
     [[Michelle: [E|Blush]... This better not be a song that you wrote, or some like dumbassery...]],
     [[Thought: She takes the journal and flips to a random page.]],
     [[Michelle: [E|Mad]\"Today I stood in front of a mirror--[P]what a funny thing to say--[P]it was the first day I began to recognize myself again...\"]],
     [[Michelle: [E|N]\"... Everyone showers me with smiles and happiness,[P] tokens I never thought I deserved,[P] even though they were the very thing I coveted for so long...\"]],
     [[Michelle: [E|Sad]\"... Now of course I know, these things are meaningless;\"]],
     [[Michelle: \"... the world will dole out love to any Tom and Dick without giving a second thought to the man they are inside...\"]],
     [[Michelle: \"... So there's two choices before me,\"[P] \"... I can wallow in this truth,[P] hating all as I was once hated--\"]],
     [[Michelle: \"--or I can finally do whatever I damn well please!\"]],
     [[Player: Intense.]],
     [[Michelle: [E|Shock]Hmph.[P][E|N] They're on to something, if you ask me.]]})

elseif(sTopicName == "Lock") then
    fnStdSequence(gciInv3B_Lock, 
    {"[HIDEALLFAST][CHARENTER|Michelle]Thought: Michelle picks up a rock and smashes it against the rusted lock. ", 
     "Info: KSSSHT",
     "Thought: The rock clatters to the ground, ineffective.[P] She shrugs.",
     "Michelle: [E|Happy]Ehh--[P]it was worth a shot."})

--[ ======================================== Characters ========================================= ]
elseif(sTopicName == "Dog") then
    fnStdSequence(gciInv3B_Char_Dog, 
    {"[HIDEALLFAST][CHARENTER|Michelle]Michelle: He's the cutest one of you lot, that's for sure."})
    
elseif(sTopicName == "Mari") then
    fnStdSequence(gciInv3B_Char_Mari, 
    {"[HIDEALLFAST][CHARENTER|Michelle]Player: Do you have any idea why Mari wanted to come here so badly?",
     "Michelle:[E|Sad]..."})

elseif(sTopicName == "Stacy") then
    fnStdSequence(gciInv3B_Char_Stacy, 
    {"[HIDEALLFAST][CHARENTER|Michelle]Player: Do you have any idea why Stacy wanted to come here so badly?",
     "Michelle: [E|Sad]..."})

elseif(sTopicName == "Bret") then
    fnStdSequence(gciInv3B_Char_Bret, 
    {"[HIDEALLFAST][CHARENTER|Michelle]Michelle: [E|N]For once, the nerd was right. [P][E|Blush]We should've gone to the soda factory. "})
    
elseif(sTopicName == "Michelle") then
    fnStdSequence(gciInv3B_Char_Michelle, 
    {"[HIDEALLFAST][CHARENTER|Michelle]Michelle: [E|Blush]What do you want, weirdo?",
     "Player: Nothing! Nevermind."})

elseif(sTopicName == "Todd") then
    fnStdSequence(gciInv3B_Char_Todd, 
    {"[HIDEALLFAST][CHARENTER|Michelle]Michelle: [E|N]I don't much care for the ASB types.[P] Always feels like they're putting on airs."})

end
