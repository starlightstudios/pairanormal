--[Keypad]
--Input the code to advance. This is actually the groundskeeper's lock.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--[Check]
--First time the player inputs something.
if(sTopicName == "Check") then
	
    --If the player has unlocked the gate:
    local iUnlockedGate = VM_GetVar("Root/Variables/Chapter3/InvestigationB/iUnlockedGate", "N")
    if(iUnlockedGate == 1.0) then

        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALL]Thought: (Go in the elevator?)") ]])
        fnCutsceneBlocker()
        local sDecisionScript = "\"" .. gsRoot .. "../Chapter 3/Gameplay_SceneC/Z - Keypad.lua\""
        fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Keep Exploring\", " .. sDecisionScript .. ", \"Explore\") ")
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Go Inside\", " .. sDecisionScript .. ", \"Inside\") ")
        fnCutsceneBlocker()
    
    --Prompt them to enter the code:
    else

        --Setup.
        gsCodeSoFar = ""
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALL]Thought: There's a lock with numbers here.[P] I guess if I put the right combination in, it'll unlock?") ]])
        fnCutsceneBlocker()

        --What do?
        fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (First Number?)") ]])
        fnCutsceneBlocker()

        local sDecisionScript = "\"" .. gsRoot .. "../Chapter 3/Gameplay_SceneC/Z - Keypad.lua\""
        fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"0\", " .. sDecisionScript .. ", \"First 0\") ")
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"1\", " .. sDecisionScript .. ", \"First 1\") ")
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"2\", " .. sDecisionScript .. ", \"First 2\") ")
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"3\", " .. sDecisionScript .. ", \"First 3\") ")
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"4\", " .. sDecisionScript .. ", \"First 4\") ")
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"5\", " .. sDecisionScript .. ", \"First 5\") ")
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"6\", " .. sDecisionScript .. ", \"First 6\") ")
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"7\", " .. sDecisionScript .. ", \"First 7\") ")
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"8\", " .. sDecisionScript .. ", \"First 8\") ")
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"9\", " .. sDecisionScript .. ", \"First 9\") ")
        fnCutsceneBlocker()
    end

--[First Key Input]
elseif(string.sub(sTopicName, 1, 5) == "First") then

    --Storage
    gsCodeSoFar = gsCodeSoFar .. string.sub(sTopicName, 7, 7)

    --Dialogue
    fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "*Click*") ]])
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (Second Number?)") ]])
    fnCutsceneBlocker()

    local sDecisionScript = "\"" .. gsRoot .. "../Chapter 3/Gameplay_SceneC/Z - Keypad.lua\""
    fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"0\", " .. sDecisionScript .. ", \"Second 0\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"1\", " .. sDecisionScript .. ", \"Second 1\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"2\", " .. sDecisionScript .. ", \"Second 2\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"3\", " .. sDecisionScript .. ", \"Second 3\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"4\", " .. sDecisionScript .. ", \"Second 4\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"5\", " .. sDecisionScript .. ", \"Second 5\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"6\", " .. sDecisionScript .. ", \"Second 6\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"7\", " .. sDecisionScript .. ", \"Second 7\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"8\", " .. sDecisionScript .. ", \"Second 8\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"9\", " .. sDecisionScript .. ", \"Second 9\") ")
    fnCutsceneBlocker()

--[Second Key Input]
elseif(string.sub(sTopicName, 1, 6) == "Second") then

    --Storage
    gsCodeSoFar = gsCodeSoFar .. string.sub(sTopicName, 8, 8)

    --Dialogue
    fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "*Click*") ]])
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (Third Number?)") ]])
    fnCutsceneBlocker()

    local sDecisionScript = "\"" .. gsRoot .. "../Chapter 3/Gameplay_SceneC/Z - Keypad.lua\""
    fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"0\", " .. sDecisionScript .. ", \"Third 0\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"1\", " .. sDecisionScript .. ", \"Third 1\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"2\", " .. sDecisionScript .. ", \"Third 2\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"3\", " .. sDecisionScript .. ", \"Third 3\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"4\", " .. sDecisionScript .. ", \"Third 4\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"5\", " .. sDecisionScript .. ", \"Third 5\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"6\", " .. sDecisionScript .. ", \"Third 6\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"7\", " .. sDecisionScript .. ", \"Third 7\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"8\", " .. sDecisionScript .. ", \"Third 8\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"9\", " .. sDecisionScript .. ", \"Third 9\") ")
    fnCutsceneBlocker()

--[Third Key Input]
elseif(string.sub(sTopicName, 1, 5) == "Third") then

    --Storage
    gsCodeSoFar = gsCodeSoFar .. string.sub(sTopicName, 7, 7)

    --Dialogue
    fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "*Click*") ]])
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (Fourth Number?)") ]])
    fnCutsceneBlocker()

    local sDecisionScript = "\"" .. gsRoot .. "../Chapter 3/Gameplay_SceneC/Z - Keypad.lua\""
    fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"0\", " .. sDecisionScript .. ", \"Fourth 0\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"1\", " .. sDecisionScript .. ", \"Fourth 1\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"2\", " .. sDecisionScript .. ", \"Fourth 2\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"3\", " .. sDecisionScript .. ", \"Fourth 3\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"4\", " .. sDecisionScript .. ", \"Fourth 4\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"5\", " .. sDecisionScript .. ", \"Fourth 5\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"6\", " .. sDecisionScript .. ", \"Fourth 6\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"7\", " .. sDecisionScript .. ", \"Fourth 7\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"8\", " .. sDecisionScript .. ", \"Fourth 8\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"9\", " .. sDecisionScript .. ", \"Fourth 9\") ")
    fnCutsceneBlocker()

--[Key Key Input]
elseif(string.sub(sTopicName, 1, 6) == "Fourth") then

    --Storage
    gsCodeSoFar = gsCodeSoFar .. string.sub(sTopicName, 8, 8)

    --Dialogue
    fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "*Click*") ]])
    fnCutsceneBlocker()
    fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: Let's see...") ]])
    fnCutsceneBlocker()
    
    --Correct!
    if(gsCodeSoFar == "1992") then
        VM_SetVar("Root/Variables/Chapter3/InvestigationB/iUnlockedGate", "N", 1.0)
        fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[MUSIC|Null]Info: [SFX|Beep]*beep*") ]])
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: I step back.[P] To my surpise, the mechanism attached to the shed unlocks, and the door swings open.") ]])
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: The club is at my side in an instant.") ]])
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[CHARENTER|Bret]Bret: [E|Happy]Well howdy-do!") ]])
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[CHARENTER|Mari]Mari: [E|Shock]Amazing![P][E|N] How'd you figure it out!?!?") ]])
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Player: I didn't![P] I-I was just playing with it.") ]])
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: We peer inside together.[P] It's not as exciting as I'd hoped.") ]])
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[CHARENTER|Stacy]Stacy: [E|Sad]Well, if we ever need powertools, a trash can, or a gardening hoe, we know where to find one.") ]])
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Mari: [E|Shock][ITALICS]BIG MOOD[ENDITALICS].[P][E|Blush] It's me! Haha, get it?") ]])
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[CHARENTER|Todd]Todd: [E|Mad]...") ]])
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Mari: [E|N]I'm a power tool,[P] and a gardening [ITALICS]ho[ENDITALICS].[P] And a trash can.[P][E|Happy] Heyoooo---[P][E|Shock]H-h-hey!") ]])
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: My jaw drops as the wall at the back of the shed begins to slide down.") ]])
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: Behind it sits a strange old chamber, which based on the way it swings, makes me think its being suspended. [P]Over what, I couldn't say.") ]])
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[CHARENTER|Michelle]Michelle: [E|Mad]HELL.[P] NO.") ]])
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Stacy: [E|N]Cool! An elevator to a secret basement!") ]])
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Mari: [E|Shock]Who [ITALICS]knows[ENDITALICS] what kind of secrets its hiding...[P][E|Happy][CRASH]BEST CLUB OUTING EVER!!!!") ]])
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: [P][P]There's an uneasy pause.") ]])
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: Excepting our enthusiastic club captains, nobody is particularly eager to keep going.[P] Maybe it's better to stay in the cemetary and go home afterwards...") ]])
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: But I have to admit... there's an unnerving tug in the pit of my stomach,[P] curious to see what's on the other side of this adventure.") ]])
        fnCutsceneBlocker()

        fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALL]Thought: (Go in the elevator?)") ]])
        fnCutsceneBlocker()
        local sDecisionScript = "\"" .. gsRoot .. "../Chapter 3/Gameplay_SceneC/Z - Keypad.lua\""
        fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Keep Exploring\", " .. sDecisionScript .. ", \"Explore\") ")
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Go Inside\", " .. sDecisionScript .. ", \"Inside\") ")
        fnCutsceneBlocker()
        
    --NOPE!
    else
        fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: That wasn't it. Maybe there's a clue somewhere around here.") ]])
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
    
    end

--[Don't Go Inside]
elseif(sTopicName == "Explore") then
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALL]Thought: I'll keep looking around before going in there, if at all.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()

--[Go Inside, The Suspense Is Killing Me]
elseif(sTopicName == "Inside") then
    LM_ExecuteScript(fnResolvePath() .. "Z - Elevator Scene.lua", "Start")
end
