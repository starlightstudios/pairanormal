-- |[Bret Presentation]|
--Showing stuff to Bret.

-- |[Arg Resolve]|
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--Variables.
local sScriptPath = gsRoot .. "../Chapter 3/Gameplay_SceneC/3 - Present Bret.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
local sAutoAdvance = gsRoot .. "../Chapter 3/Gameplay_SceneD/7 - PatchGoingHome.lua"

--Saving
gsSaveExecute = gsRoot .. "../Chapter 3/Gameplay_SceneA/4 - Cemetary.lua"
gsSaveTopic = "No Topic"
gsSaveFlag = "InvestigationCemetary"

-- |[ ======================================= Functions ======================================== ]|
-- |[Friendship Increment]|
--If this is the first time presenting this object to this character then we increase the friendship tracker for
-- that character.
local fnIncrementFriendship = function(iObjectIndex, iIncrement)
	
    --Arg check.
    if(iObjectIndex == nil) then return end
    if(iIncrement == nil) then return end
    
    --Build strings.
	local i = iObjectIndex
    local p = iBretIndex
    local sObjectString = string.format("%02i", i)
    local sSubString    = string.format("%02i", p)
    local iIsRepeat = VM_GetVar("Root/Variables/Chapter3/InvestigationB/iObjectPresent|" .. sObjectString .. sSubString, "N")
    if(iIsRepeat == 0.0) then
        
        --Flag
        VM_SetVar("Root/Variables/Chapter3/InvestigationB/iObjectPresent|" .. sObjectString .. sSubString, "N", 1.0)
    
        --Variable gain.
        local iBretFriend  = VM_GetVar("Root/Variables/System/Romance/iBretFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iBretFriend", "N", iBretFriend + iIncrement)
    end
end

-- |[Sequence Handler]|
--Almost all sequences can use a standardized template.
local fnStdSequence = function(iFriendshipIndex, saStrings)
    
    --Start.
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    
    for i = 1, #saStrings, 1 do
		fnDialogue(saStrings[i])
    end
    
    --Finish.
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(iFriendshipIndex, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
end

-- |[ ===================================== Chat Sequences ===================================== ]|
--Chatting.
if(sTopicName == "Chat") then
	
	--Variables.
	local iChattedWithBret = VM_GetVar("Root/Variables/Chapter3/Investigation/iChattedWithBret", "N")
	
	--First pass:
	if(iChattedWithBret == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter3/Investigation/iChattedWithBret", "N", 1.0)
		
		--Text.
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
    
		fnDialogue([[[HIDEALLFAST][CHARENTER|Bret]Player: How's it going, \"Bretmeister\"?]])
		fnDialogue([[Bret: [E|Shock]Hm? Oh, you know me.[P][E|Mad] Or I guess you don't know me, I mean--[P][E|Shock]fine![P][E|Sad] I'm doing fine. I'm just...[P] checking out these tombstones.]])
		fnDialogue([[Thought: Bret peers intensely at the carved slabs of stone, his brow furrowing in a sad expression.]])
		fnDialogue([[Thought: For the first time since we've met, he doesn't have anything to say.[P] I take my leave.]])

        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
    
        --Variable gain.
        local iBretFriend  = VM_GetVar("Root/Variables/System/Romance/iBretFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iBretFriend", "N", iBretFriend + 5.0)
	
        --Time loss.
        if(fnDecrementTime(5, sAutoAdvance) == false) then
            PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
        end
		
	--Repeat passes.
	else
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
        
		fnDialogue("[HIDEALLFAST][CHARENTER|Bret]Bret: [E|N]Chic didn't write this section.")
        
        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
	end
	
-- |[ ================================== Object Presentation =================================== ]|
elseif(sTopicName == "WalkieTalkie" or sTopicName == "Laura") then
    fnStdSequence(gciInv3B_WalkieTalkie, 
    {"[HIDEALLFAST][CHARENTER|Bret]Bret: If Laura is at home, she can help by looking stuff up on her computer!"})
    
elseif(sTopicName == "GraveSign") then
    fnStdSequence(gciInv3B_GraveyardSign, 
    {"[HIDEALLFAST][CHARENTER|Bret]Bret: [E|N]Here we are, Foster Falls Cemetary![P][E|Mad] Established in 1945 after the deaths from the quartz mines overflowed the original Pine Falls graveyard."})

elseif(sTopicName == "KaneGrave") then
    fnStdSequence(gciInv3B_KaneGrave, 
    {"[HIDEALLFAST][CHARENTER|Bret]Bret: [E|N]Kane McKay Foster.[P] Pretty important guy.[P][E|Sad] Dead, though."})
    
elseif(sTopicName == "GeorgeGrave") then
    fnStdSequence(gciInv3B_GeorgeGrave, 
    {"[HIDEALLFAST][CHARENTER|Bret]Bret: [E|N]Ah, George Foster! ", 
     [[Bret: [E|Mad]\"The founder of Foster Falls, never to be forgotten by its loyal denizens.[P][E|N] A man who believed in opportunity, enterprise, and us.\"]]})
    
elseif(sTopicName == "GeorgeJRGrave") then
    fnStdSequence(gciInv3B_GeorgeJrGrave, 
    {[[[HIDEALLFAST][CHARENTER|Bret]Player: Why does \"George Foster\" have two graves?]], 
     "Bret: [E|Happy]He doesn't--although I can see why you might think that.[P][E|Shock] Not that I'm trying to be pretentious or anything,[E|Blush] I think you're very smart, [E|Sad]I just---",
     "Bret: uh, what I meant to say is,[P][E|N] this is George Foster Junior, his son."})

elseif(sTopicName == "1Grave") then
    fnStdSequence(gciInv3B_1Grave, 
    {"[HIDEALLFAST][CHARENTER|Bret]Bret: [E|N]I'm afraid I haven't a clue-di a who-di that might be...[P]di."})

elseif(sTopicName == "2Grave") then
    fnStdSequence(gciInv3B_2Grave, 
    {"[HIDEALLFAST][CHARENTER|Bret]Bret: [E|N]I'm afraid I haven't a clue-di a who-di that might be...[P]di."})

elseif(sTopicName == "4Grave") then
    fnStdSequence(gciInv3B_4Grave, 
    {"[HIDEALLFAST][CHARENTER|Bret]Bret: [E|N]I'm afraid I haven't a clue-di a who-di that might be...[P]di."})

elseif(sTopicName == "MyGrave") then
    fnStdSequence(gciInv3B_MyGrave, 
    {"[HIDEALLFAST][CHARENTER|Bret]Bret: [E|Shock]I don't remember ever hearing about this Foster,[P] but there's no date on this tombstone,[P] so who knows when they died?", 
     "Bret: [E|N]Were they a mother?[P] Father?[P][E|Mad] Grandfather?[P] Daughter?",
     "Bret: [E|Shock]Although someone [ITALICS]has[ENDITALICS] left flowers here, [E|Mad]which indicates that someone alive knew them...",
     "Bret: [E|N] but the only Foster alive today is Miss Foster from the school board,[P][E|Mad] so does [ITALICS]she[ENDITALICS] know--",
     "Player: Bret! Don't you think it's weird that this tombstone has my name?",
     "Thought: Bret stands up.",
     "Bret: [P][P][E|N]Oh, you're right.[P][E|Sad] That is weird, huh?"})

elseif(sTopicName == "JaniceGrave") then
    fnStdSequence(gciInv3B_JaniceGrave, 
    {"[HIDEALLFAST][CHARENTER|Bret]Bret: [E|N]That would be Janice Foster, Miss Foster's late mother.", 
     [[Player: Why do people use the word \"late\" to describe someone who has passed away? ]],
     "Bret: [E|Sad]I dunno... [P][E|N]uh, maybe because since they're dead, they'll never be on time to future appointments."})

elseif(sTopicName == "AnneGrave") then
    fnStdSequence(gciInv3B_AnneGrave, 
    {"[HIDEALLFAST][CHARENTER|Bret]Bret: [E|N]I'm afraid I haven't a clue-di a who-di that might be...[P]di."})

elseif(sTopicName == "EiletteGrave") then
    fnStdSequence(gciInv3B_EiletteGrave, 
    {"[HIDEALLFAST][CHARENTER|Bret]Bret: [E|N]I'm afraid I haven't a clue-di a who-di that might be...[P]di."})

elseif(sTopicName == "FroggyStationary") then
    fnStdSequence(gciInv3B_FroggyStationary, 
    {"[HIDEALLFAST][CHARENTER|Bret]Bret: [E|Shock]Oh man, someone left their password thing here.[P][E|N] Better hang on to it-- we wouldn't want someone to steal it!", 
     "Player: Wouldn't we be stealing it?",
     "Bret: [E|Happy]Yeah but we're the good guys![P][P] Except for the fact that we broke in to the graveyard and stuff."})

elseif(sTopicName == "Journal") then
    fnStdSequence(gciInv3B_Journal, 
    {"[HIDEALLFAST][CHARENTER|Bret]Player: Here Bret, read this.",
     "Bret: [E|N]uh, okay.",
     "Thought: He opens the journal to a random page and just stands there.",
     "Bret: [E|N]...",
     "Player: [SFX|Blink]...",
     "Bret: [E|N]...",
     "Player: [SFX|Ugh]Out loud, I mean!",
     "Bret: [E|Happy]Hahaha, okay, okay.[P] Just pulling your tail there. Let's see...",
     [[Bret: [E|Mad] \"I've had to move him so many times, constantly moving, moving... the other, that beast, is a bloodhound...\" ]],
     [[Bret: \"... Anywhere I go, she's waiting in the shadows, ready to destroy me and strike the final blow...\"]],
     [[Bret: \"... But as long as I have the Ace in the pocket, There's a chance I may succeed in all this.[P] She'll never capture it, on my life she WON'T.\"]],
     "Player: ... Hm.",
     "Bret: [E|Happy]Sounds like the most messed up game of hide-and-seek I've ever heard of.[P][E|Sad] Or is it... chess?[P] There were a lot of game analogies in that passage."})

elseif(sTopicName == "Lock") then
    fnStdSequence(gciInv3B_Lock, 
    {"[HIDEALLFAST][CHARENTER|Bret]Bret: [E|N]It's locked,[E|Sad] but there's probably nothing but gardening tools in there, anyway."})

-- |[ ======================================= Characters ======================================= ]|
elseif(sTopicName == "Dog") then
    fnStdSequence(gciInv3B_Char_Dog, 
    {"[HIDEALLFAST][CHARENTER|Bret]Bret: He loves getting a scratch behind the ears."})
    
elseif(sTopicName == "Mari") then
    fnStdSequence(gciInv3B_Char_Mari, 
    {"[HIDEALLFAST][CHARENTER|Bret]Bret: [E|Shock]The captains have taken us ghost hunting here before, but they seemed so insistent on visiting again today.",
     "Bret: [E|Sad]... I kinda wish we had gone to the soda factory instead,[P][E|N] but it's fine.[P] I'm totally fine about it."})

elseif(sTopicName == "Stacy") then
    fnStdSequence(gciInv3B_Char_Stacy, 
    {"[HIDEALLFAST][CHARENTER|Bret]Bret: [E|Shock]The captains have taken us ghost hunting here before, but they seemed so insistent on visiting again today.",
     "Bret: [E|Sad]... I kinda wish we had gone to the soda factory instead,[P][E|N] but it's fine.[P] I'm totally fine about it."})
    
elseif(sTopicName == "Bret") then
    fnStdSequence(gciInv3B_Char_Bret, 
    {"[HIDEALLFAST][CHARENTER|Bret]Bret: [E|N]Hi-de-ho!"})
    
elseif(sTopicName == "Michelle") then
    fnStdSequence(gciInv3B_Char_Michelle, 
    {"[HIDEALLFAST][CHARENTER|Bret]Bret: [E|N]Michelle is our newest member, so she hasn't been to the graveyard as often as the rest of us."})

elseif(sTopicName == "Todd") then
    fnStdSequence(gciInv3B_Char_Todd, 
    {"[HIDEALLFAST][CHARENTER|Bret]Bret: I think Todd's a pretty cool guy. Eh fights students and doesn't afraid of anything.",
     "Player: What?",
     "Bret: Err, nevermind. You wouldn't understand."})
end
