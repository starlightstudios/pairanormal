-- |[Todd Presentation]|
--He smells clean.

-- |[Arg Resolve]|
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--Variables.
local sScriptPath = gsRoot .. "../Chapter 3/Gameplay_SceneB/7 - Present Todd.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
local sAutoAdvance = gsRoot .. "../Chapter 3/Gameplay_SceneD/7 - PatchGoingHome.lua"

--Saving
gsSaveExecute = gsRoot .. "../Chapter 3/Gameplay_SceneA/3 - Soda Factory.lua"
gsSaveTopic = "No Topic"
gsSaveFlag = "InvestigationCola"

-- |[ ========================================= Functions ========================================= ]|
-- |[Friendship Increment]|
--If this is the first time presenting this object to this character then we increase the friendship tracker for
-- that character.
local fnIncrementFriendship = function(iObjectIndex, iIncrement)
	
    --Arg check.
    if(iObjectIndex == nil) then return end
    if(iIncrement == nil) then return end
    
    --Build strings.
	local i = iObjectIndex
    local p = iToddIndex
    local sObjectString = string.format("%02i", i)
    local sSubString    = string.format("%02i", p)
    local iIsRepeat = VM_GetVar("Root/Variables/Chapter3/InvestigationA/iObjectPresent|" .. sObjectString .. sSubString, "N")
    if(iIsRepeat == 0.0) then
        
        --Flag
        VM_SetVar("Root/Variables/Chapter3/InvestigationA/iObjectPresent|" .. sObjectString .. sSubString, "N", 1.0)
    
        --Variable gain.
        local iToddFriend  = VM_GetVar("Root/Variables/System/Romance/iToddFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iToddFriend", "N", iToddFriend + iIncrement)
    end
end

-- |[Sequence Handler]|
--Almost all sequences can use a standardized template.
local fnStdSequence = function(iFriendshipIndex, saStrings)
    
    --Start.
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    
    for i = 1, #saStrings, 1 do
		fnDialogue(saStrings[i])
    end
    
    --Finish.
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(iFriendshipIndex, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
end

-- |[ ======================================= Chat Sequences ====================================== ]|
--Chatting.
if(sTopicName == "Chat") then
	
	--Variables.
	local iChattedWithTodd = VM_GetVar("Root/Variables/Chapter3/Investigation/iChattedWithTodd", "N")
	
	--First pass:
	if(iChattedWithTodd == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter3/Investigation/iChattedWithTodd", "N", 1.0)
		
		--Text.
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
    
		fnDialogue([[[HIDEALLFAST][CHARENTER|Todd]Player: Hey Todd.[P] How... uh, how's it going?]])
		fnDialogue([[Todd: [E|N]It's going pretty great.]])
		fnDialogue([[Player: You're not creeped out by all this?]])
		fnDialogue([[Thought: Todd takes in the surroundings and shrugs.]])
		fnDialogue([[Todd: [E|Mad]I haven't lost anyone really significant, knock on wood and all that.]])
		fnDialogue([[Todd: [E|N] Actually--you know what? Being here makes me feel...[P]grateful.[P] Motivated and stuff.]])
		fnDialogue([[Thought: I try to follow along, but there's some piece of wisdom in Todd's thought that I can't grasp. ]])
		fnDialogue([[Thought: I haven't been alive long enough, I guess.]])

        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()

        --Variable gain.
        local iToddFriend  = VM_GetVar("Root/Variables/System/Romance/iToddFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iToddFriend", "N", iToddFriend + 5.0)
	
        --Time loss.
        if(fnDecrementTime(5, sAutoAdvance) == false) then
            PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
        end

	--Repeat passes.
	else
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
        
		fnDialogue("[HIDEALLFAST][CHARENTER|Todd]Todd: [E|Mad]What are we doing here, by the way?")
		fnDialogue("Player: Just snooping around until we find something vaguely cool?[P] Bonus points if it proves the existence of ghosts.")
		fnDialogue("Todd: [E|Happy]Mmmm, sounds easy enough.[P] I'll do my best.")

        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
	end
	
-- |[ ==================================== Object Presentation ==================================== ]|
elseif(sTopicName == "WalkieTalkie" or sTopicName == "Laura") then
    fnStdSequence(gciInv3B_WalkieTalkie, 
    {"[HIDEALLFAST][CHARENTER|Todd]Todd: Cool, you guys have walkie-talkies and everything!"})
    
elseif(sTopicName == "GraveSign") then
    fnStdSequence(gciInv3B_GraveyardSign, 
    {"[HIDEALLFAST][CHARENTER|Todd]Todd: [E|N]When I die, I'm hoping technology advances enough that my conciousness can be trasferred into, like,[P] a USB drive.",
     "Todd: [E|Happy]Then I could be plugged into any video game.",
     "Player: How is that different from just playing a video game?",
     "Todd: [E|Shock][CRASH]It would be cool, okay?[P][E|Mad] Trust me."})

elseif(sTopicName == "KaneGrave") then
    fnStdSequence(gciInv3B_KaneGrave, 
    {"[HIDEALLFAST][CHARENTER|Todd]Todd: [E|Sad] ASB had to go to Mr. Foster's funeral. It was absolutely massive."})
    
elseif(sTopicName == "GeorgeGrave") then
    fnStdSequence(gciInv3B_GeorgeGrave, 
    {"[HIDEALLFAST][CHARENTER|Todd]Todd: [E|Sad]Everybody touts the accomplishments of Kane McKay Foster, but I happen to know--via several boring AP History reports--",
     "Todd: [E|Shock]--that George Foster Sr. was responsible for the town's most rapid period of growth.",
     "Todd: [E|N]Kane McKay Foster was really just picking up the baton."})
    
elseif(sTopicName == "GeorgeJRGrave") then
    fnStdSequence(gciInv3B_GeorgeJrGrave, 
    {"[HIDEALLFAST][CHARENTER|Todd]Todd: [E|Mad]Yo, thanks to a VERY strange late-night wikipedia binge,[E|Blush] I weirdly know a lot about this guy.",
     "Todd: [E|Shock] He was trying to invent a new kind of energy that was powered with quartz???",
     "Todd: Dude died in a lab explosion.[P] I swear, science in the 50's was absolutely bonkers."})

elseif(sTopicName == "1Grave") then
    fnStdSequence(gciInv3B_1Grave, 
    {"[HIDEALLFAST][CHARENTER|Todd]Todd: [E|N]That's a tombstone.[P] I don't know much more than that."})

elseif(sTopicName == "2Grave") then
    fnStdSequence(gciInv3B_2Grave, 
    {"[HIDEALLFAST][CHARENTER|Todd]Todd: [E|Sad]Oof, does it ever just hit you how this place morbidly connects everyone in town?[P] Someone you know probably lost this person."})

elseif(sTopicName == "4Grave") then
    fnStdSequence(gciInv3B_4Grave, 
    {"[HIDEALLFAST][CHARENTER|Todd]Todd: [E|Sad]Oof, does it ever just hit you how this place morbidly connects everyone in town?[P] Someone you know probably lost this person."})

elseif(sTopicName == "MyGrave") then
    fnStdSequence(gciInv3B_MyGrave, 
    {"[HIDEALLFAST][CHARENTER|Todd]Todd: [E|Mad]Huh.[P] That's... your name,[P] sort of.", 
     "Thought: I can't stand the look developing in Todd's creased brow.[P] Better change the subject."})

elseif(sTopicName == "JaniceGrave") then
    fnStdSequence(gciInv3B_JaniceGrave, 
    {"[HIDEALLFAST][CHARENTER|Todd]Todd: [E|N]Janice Foster started the Foster Falls Women's Clinic down on Grape Street.", 
     "Todd: [E|Shock] Not a scientist [ITALICS]at all[ENDITALICS], but she tried all kinds of weird projects to help women concieve.",
     "[E|Blush][SFX|Ugh] Harper bought me her self-help book once as a gag gift."})

elseif(sTopicName == "AnneGrave") then
    fnStdSequence(gciInv3B_AnneGrave, 
    {"[HIDEALLFAST][CHARENTER|Todd]Todd: [E|Sad]Oof, does it ever just hit you how this place morbidly connects everyone in town?[P] Someone you know probably lost this person."})

elseif(sTopicName == "EiletteGrave") then
    fnStdSequence(gciInv3B_EiletteGrave, 
    {"[HIDEALLFAST][CHARENTER|Todd]Todd: [E|N]That's a tombstone.[P] I don't know much more than that."})

elseif(sTopicName == "FroggyStationary") then
    fnStdSequence(gciInv3B_FroggyStationary, 
    {"[HIDEALLFAST][CHARENTER|Todd]Todd: [E|Shock]Everyone around here has Froggy Stationary.[P][E|Sad] I don't know if we'll ever be able to find the owner of this paper.",
     "Player: Too bad.[P] Seems kind of important."})

elseif(sTopicName == "Journal") then
    fnStdSequence(gciInv3B_Journal, 
    {"[HIDEALLFAST][CHARENTER|Todd]Player: Can you read this journal for me?",
     "Todd: [E|N]As your tutor, I would instead encourage you to try and read it yourself.",
     "Player: [SFX|Ugh]Laaaaame![P] I'm just gonna ask someone else."})

elseif(sTopicName == "Lock") then
    fnStdSequence(gciInv3B_Lock, 
    {"[HIDEALLFAST][CHARENTER|Todd]Todd: [E|Shock]As an ASB representative, I have to ask...[P][P] you guys don't break into places, do you? ",
     "Player: Not successfully, as yet.",
     "Todd: [E|Shock]Hm.[P][E|Blush] I guess that's fine, then."})

-- |[ ======================================== Characters ========================================= ]|
elseif(sTopicName == "Dog") then
    fnStdSequence(gciInv3A_Char_Dog, 
    {"[HIDEALLFAST][CHARENTER|Todd]Todd: [E|Shock]Hey, that's your dog![E|N] You brought him along for the adventure, huh?"})
    
elseif(sTopicName == "Mari") then
    fnStdSequence(gciInv3A_Char_Mari, 
    {"[HIDEALLFAST][CHARENTER|Todd]Todd: [E|N]Mari and I used to be neighbors, and we'd get into all kinds of shenanigans back then.", 
     "Todd: [E|Happy] Glad to know she's still down for an adventure."})

elseif(sTopicName == "Stacy") then
    fnStdSequence(gciInv3A_Char_Stacy, 
    {"[HIDEALLFAST][CHARENTER|Todd]Todd: [E|Shock]Oh man, have you seen Stacy play like, any sport?", 
     "Todd: [E|Mad]If we had a team of Stacys, ASB's homecoming budget would be tripled."})
    
elseif(sTopicName == "Bret") then
    fnStdSequence(gciInv3A_Char_Bret, 
    {"[HIDEALLFAST][CHARENTER|Todd]Todd: [E|N]Brad and I share a chemistry class.[P] He's a great lab partner.", 
     "Player: Oh, actually, his name is Bret.",
     "Todd: [E|Shock]Wait... seriously?[P][SFX|Ugh] I've been calling him Brad for a whole year and he never corrected me."})
    
elseif(sTopicName == "Michelle") then
    fnStdSequence(gciInv3A_Char_Michelle, 
    {"[HIDEALLFAST][CHARENTER|Todd]Todd: [E|Mad]Who's that again?", 
     "Player: Her name's Michelle.[P] She's nice!",
     "Todd: [E|Shock]Well why is she standing all the way over there?",
     "Player: Probably because it's the [ITALICS]coolest[ENDITALICS] place to stand."})
    
elseif(sTopicName == "Todd") then
    fnStdSequence(gciInv3A_Char_Todd, 
    {"[HIDEALLFAST][CHARENTER|Todd]Todd: [E|Sad]As an ASB Rep, I can't say I outright condone this particular adventure.",
     "Todd: [E|N] BUT, graveyards are massively underfunded due to low patronage.[P][E|Happy] So I'm willing to write this off as supporting a local business."})

end
