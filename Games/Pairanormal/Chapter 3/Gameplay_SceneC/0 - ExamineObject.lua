--[ ================================== Graveyard Investigation ================================== ]
--I love dead rocks.

--[ ========================================= Variables ========================================= ]
--[Arguments]
--Get the name of what we're investigating.
local sExamine = LM_GetScriptArgument(0)

--[Variable Setup]
--Used to simplify the Add Discovered Object calls.
local sBGPath = "Root/Images/Backgrounds/All/Graveyard"
local sScriptPath = gsRoot .. "../Chapter 3/Gameplay_SceneC/0 - ExamineObject.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
local sAutoAdvance = gsRoot .. "../Chapter 3/Gameplay_SceneD/7 - PatchGoingHome.lua"

--Saving
gsSaveExecute = gsRoot .. "../Chapter 3/Gameplay_SceneA/4 - Cemetary.lua"
gsSaveTopic = "No Topic"
gsSaveFlag = "InvestigationCemetary"

--[ ========================================= Functions ========================================= ]
--Stores the object as discovered.
local fnStdStore = function(i)
    
    --Get if we've examined the object before. This will be returned later.
	local iExaminedAlready = VM_GetVar("Root/Variables/Chapter3/InvestigationB/iObject|" .. gczaInv3B_List[i], "N")
    
    --Save it as a discovered object.
    Dia_SetProperty("Add Discovered Object", gczaInv3B_List[i], sBGPath, sScriptPath, gczaInv3B_List[i])
	VM_SetVar("Root/Variables/Chapter3/InvestigationB/iObject|" .. gczaInv3B_List[i], "N", 1.0)
    
    --Pass back.
    return iExaminedAlready
end

--Decrements time. Also saves the game.
local fnStdTime = function()
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(gsSaveFlag, gsSaveExecute, gsSaveTopic)
    end
end

--Standard handler for objects. Saves the object, prints a dialogue, advances time.
local fnStdObject = function(iIndex, saLines)
	
    --Argument check.
    if(iIndex == nil) then return end
    
	--Store it in the inventory.
	local i = iIndex
	local iExaminedAlready = fnStdStore(i)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
    
    for i = 1, #saLines, 1 do
		fnDialogue(saLines[i])
    end
    
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
    if(iExaminedAlready == 0.0) then fnStdTime(sSaveFlag, sSaveExecute, sSaveTopic) end
end

--[ ==================================== Examinable Objects ===================================== ]
if(sExamine == "WalkieTalkie" or sExamine == "Laura") then
    fnStdObject(gciInv3B_WalkieTalkie, 
     {"[HIDEALLFAST]Thought: This is Laura's old walkie talkie, which Mari gave to me.[P] Apparently Laura is on the other end if I need to talk."})
    
elseif(sExamine == "GraveSign") then
    fnStdObject(gciInv3B_GraveyardSign, 
    {"[HIDEALLFAST]Thought: This is the town's graveyard.",
     "Even though every tombstone is different, Mari said somewhere it should have a name and a date."})
    
elseif(sExamine == "KaneGrave") then
    fnStdObject(gciInv3B_KaneGrave, 
    {"[HIDEALLFAST]Thought: This tombstone[GLITCH][GLITCH] looks a lot like the statue in front of our school, except newer[GLITCHTEXT]THISIS MY [GLITCH]L [GLITCH]ASTCHANCE T HIS IS MY LA [GLITCH][GLITCH]STCHANCE[ENDGLITCHTEXT]",
     "Thought: Pretty grandiose.[P] 1921-2009"})

elseif(sExamine == "GeorgeGrave") then
    fnStdObject(gciInv3B_GeorgeGrave, 
    {"[HIDEALLFAST]Thought: There's a lot of hard words written here; I should ask someone to help me read it.[P] 1902-1951"})

elseif(sExamine == "GeorgeJRGrave") then
    fnStdObject(gciInv3B_GeorgeJrGrave, 
    {"[HIDEALLFAST]Thought: Another George Foster.[P] But this mark - JR. [P][SFX|Blink] what's that mean?"})

elseif(sExamine == "1Grave") then
    fnStdObject(gciInv3B_1Grave, 
    {"[HIDEALLFAST]Thought: Esther Penrose - There's a lot of hard words written here.[P] I should ask someone to h[GLITCH]elp me read it.[P] 1951-1992"})

elseif(sExamine == "2Grave") then
    fnStdObject(gciInv3B_2Grave, 
    {"[HIDEALLFAST]Thought: II - What wi[GLITCH]ll I do now?[P] No number."})

elseif(sExamine == "4Grave") then
    fnStdObject(gciInv3B_4Grave, 
    {"[HIDEALLFAST]Thought: Jonathan Freeman - 1992 .[P] This grave is very well kept."})

elseif(sExamine == "MyGrave") then
    fnStdObject(gciInv3B_MyGrave, 
    {"[HIDEALLFAST]Michelle: [PLAYERNAME], don't read--!",
     "Thought: [PLAYERNAME].[P].[P].[P]F-Foster.",
     "Thought: That... [GLITCH] [GLITCH]has to be be a coincidence."})

elseif(sExamine == "JaniceGrave") then
    fnStdObject(gciInv3B_JaniceGrave, 
    {[[ [HIDEALLFAST]Thought: Janice Foster - \"lvoing wife and mohhter.\"[P] 1924-1988 ]]})

elseif(sExamine == "AnneGrave") then
    fnStdObject(gciInv3B_AnneGrave, 
    {[[ [HIDEALLFAST]Thought: Anne - \"the graet lvoe of my life.\"[P] 1923-1976.]]})

elseif(sExamine == "EiletteGrave") then
    fnStdObject(gciInv3B_EiletteGrave, 
    {[[ [HIDEALLFAST]Thought: --- \"don't simle unelss you relaly mean it\" 1949-1992. [P]The name is covered in a deep moss.]]})

elseif(sExamine == "FroggyStationary") then
    fnStdObject(gciInv3B_FroggyStationary, 
    {[[ [HIDEALLFAST]Thought: A scrap of paper on the floor that reads \"Psasworrd is the bad yaer\" ]]})

elseif(sExamine == "Journal") then
    fnStdObject(gciInv3B_Journal, 
    {"[HIDEALLFAST]Thought: An old, abandoned journal in a graveyard. [ITALICS]Great[ENDITALICS]."})

elseif(sExamine == "Lock") then
    LM_ExecuteScript(fnResolvePath() .. "Z - Keypad.lua", "Check")
    
--[ ======================================== Characters ========================================= ]
elseif(sExamine == "Mari") then
	Dia_SetProperty("Add Discovered Object", "Mari", sCharPath, sScriptPath, "Mari",     1322,   2, 1322 + 438,   2 + 363)
    fnStdObject(gciInv3B_Char_Mari, 
    {"[HIDEALLFAST]Thought: Mari looks strangely in her element here.",
     "Thought: I imagine as the Paranormal Club president, this isn't the first time she's visited the graveyard."})

elseif(sExamine == "Dog") then
    fnStdObject(gciInv3B_Char_Dog, 
    {"[HIDEALLFAST]Thought: The goodness on this boy radiates from him like a corona. You can see him around corners."})

elseif(sExamine == "Stacy") then
	Dia_SetProperty("Add Discovered Object", "Stacy", sCharPath, sScriptPath, "Stacy",     442, 367,  442 + 438, 367 + 363)
    fnStdObject(gciInv3B_Char_Stacy, 
    {"[HIDEALLFAST]Thought: Stacy is casually leaping over tombstones.[P] Is...[SFX|Ugh] that allowed?"})

elseif(sExamine == "Bret") then
	Dia_SetProperty("Add Discovered Object", "Bret", sCharPath, sScriptPath, "Bret",        2,   2,    2 + 438,   2 + 363)
    fnStdObject(gciInv3B_Char_Bret, 
    {"[HIDEALLFAST]Thought: Bret stoops low, carefully reading every tombstone he comes across like the information is vital."})

elseif(sExamine == "Michelle") then
	Dia_SetProperty("Add Discovered Object", "Michelle", sCharPath, sScriptPath, "Michelle", 1762,   2, 1762 + 438,   2 + 363)
    fnStdObject(gciInv3B_Char_Michelle, 
    {"[HIDEALLFAST]Thought: Michelle looks uncomfortable."})
    
elseif(sExamine == "Todd") then
	Dia_SetProperty("Add Discovered Object", "Todd", sCharPath, sScriptPath, "Todd", 2, 367, 2 + 438, 367 + 363)
    fnStdObject(gciInv3B_Char_Todd, 
    {"[HIDEALLFAST]Thought: Todd is taking a stroll through the paths.[P] He seems to actually be enjoying himself!"})
end