--[Mari Presentation]
--Showing stuff to Mari.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--Variables.
local sScriptPath = gsRoot .. "../Chapter 3/Gameplay_SceneC/1 - Present Mari.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
local sAutoAdvance = gsRoot .. "../Chapter 3/Gameplay_SceneD/7 - PatchGoingHome.lua"

--Saving
gsSaveExecute = gsRoot .. "../Chapter 3/Gameplay_SceneA/4 - Cemetary.lua"
gsSaveTopic = "No Topic"
gsSaveFlag = "InvestigationCemetary"

--[ ========================================= Functions ========================================= ]
--[Friendship Increment]
--If this is the first time presenting this object to this character then we increase the friendship tracker for
-- that character.
local fnIncrementFriendship = function(iObjectIndex, iIncrement)
	
    --Arg check.
    if(iObjectIndex == nil) then return end
    if(iIncrement == nil) then return end
    
    --Build strings.
	local i = iObjectIndex
    local p = iMariIndex
    local sObjectString = string.format("%02i", i)
    local sSubString    = string.format("%02i", p)
    local iIsRepeat = VM_GetVar("Root/Variables/Chapter3/InvestigationB/iObjectPresent|" .. sObjectString .. sSubString, "N")
    if(iIsRepeat == 0.0) then
        
        --Flag
        VM_SetVar("Root/Variables/Chapter3/InvestigationB/iObjectPresent|" .. sObjectString .. sSubString, "N", 1.0)
    
        --Variable gain.
        local iMariFriend  = VM_GetVar("Root/Variables/System/Romance/iMariFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iMariFriend", "N", iMariFriend + iIncrement)
    end
end

--[Sequence Handler]
--Almost all sequences can use a standardized template.
local fnStdSequence = function(iFriendshipIndex, saStrings)
    
    --Start.
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    
    for i = 1, #saStrings, 1 do
		fnDialogue(saStrings[i])
    end
    
    --Finish.
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(iFriendshipIndex, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
end

--[ ======================================= Chat Sequences ====================================== ]
--Chatting.
if(sTopicName == "Chat") then
	
	--Variables.
	local iChattedWithMari = VM_GetVar("Root/Variables/Chapter3/Investigation/iChattedWithMari", "N")
	
	--First pass:
	if(iChattedWithMari == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter3/Investigation/iChattedWithMari", "N", 1.0)
		
		--Text.
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
    
		fnDialogue([[[HIDEALLFAST][CHARENTER|Mari][CHARENTER|Stacy]Mari: [E|Shock][CRASH]WELCOME TO THE FOSTER FALLS CEMETARY! ]])
		fnDialogue([[Stacy: [E|N]Isn't this place cool?]])
		fnDialogue([[Player: Uhh, I guess.[P] Are there really dead people buried underneath us?]])
		fnDialogue([[Stacy: [E|N][ITALICS]Oui Oui![ENDITALICS] We come here often as a club because the place is chock full of potential paranormal activity!]])
		fnDialogue([[Mari: [E|N]Someday I know we'll see a ghost or something like that.[P][E|Happy] But check this out--there's a special reason we came today!]])
		fnDialogue([[Thought: The two club captains lead me to one of the graves.]])
		fnDialogue([[Stacy: [E|N]You know how you like, don't know who you are or where you came from?]])
		fnDialogue([[Player: Yeah...]])
		fnDialogue([[Mari: [E|N]Well, we've been doing some digging. Look at that!]])
		fnDialogue([[Stacy: [E|Sad]... Poor choice of words, Mari.]])
		fnDialogue([[Thought: [P][P][P]I glance down and see...[GLITCH]my name.]])
		fnDialogue([[Player: Wh...[P]why[GLITCHTEXT]MAYBE THIS CAN ANSWER THE MAYBE THIS CANNANSWER THE[ENDGLITCHTEXT]is my name on a tombstone?]])
		fnDialogue([[Mari: [E|Shock]Apparently there was someone in the Foster family who passed away as a baby.]])
		fnDialogue([[Stacy: [E|Mad]But they were born in 1995--[P][E|N]that would make them about your age, wouldn't it?]])
		fnDialogue([[Player: I[GLITCH] ]])
		fnDialogue([[[HIDEALLFAST][BG|Null][BG|Null][Music|Null][Music|Null] ]])
		fnDialogue([[[P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P]Thought: [P][P]I'm staring at a tree.[P] [ITALICS]What just happened?[ENDITALICS].]])
		fnDialogue([[Thought: [BG|Root/Images/Backgrounds/All/Graveyard]I look around--Mari and Stacy are chatting happily over by the Foster monument. I must've blacked out again. ]])
		fnDialogue([[Thought: To avoid arousing suspicion, I decide to keep looking around the cemetary, trying to ignore the uneasy feeling in my stomach.]])
        
        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
    
        --Variable gain.
        local iMariFriend  = VM_GetVar("Root/Variables/System/Romance/iMariFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iMariFriend", "N", iMariFriend + 5.0)
	
        --Time loss.
        if(fnDecrementTime(5, sAutoAdvance) == false) then
            PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
        end
		
	--Repeat passes.
	else
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
        
		fnDialogue("[HIDEALLFAST][CHARENTER|Mari][CHARENTER|Stacy]Mari: [E|N]I hope you don't mind us taking you here, [PLAYERNAME]!")
		fnDialogue("Stacy: [E|N]If anything might help answer a question about who you are or where you came from, then we're gonna answer it!")
		fnDialogue("Player: [P]Oh.")
		fnDialogue("Mari: [E|Happy]Anyway, take a look around and let us know if you find something interesting!")
        
        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
	end
	
--[ ==================================== Object Presentation ==================================== ]
elseif(sTopicName == "WalkieTalkie" or sTopicName == "Laura") then
    fnStdSequence(gciInv3B_WalkieTalkie, 
    {"[HIDEALLFAST][CHARENTER|Mari]Mari: [E|N]Laura's walkie talkie... ahhh.[P][E|Blush] Brings back memories of the club's early days."})
    
elseif(sTopicName == "GraveSign") then
    fnStdSequence(gciInv3B_GraveyardSign, 
    {"[HIDEALLFAST][CHARENTER|Mari]Mari: [E|N]I'm a witch, so when I die I'm probably gonna join Satan's sexy queer harem in the underworld.[P] It's gonna be so rad."})

elseif(sTopicName == "KaneGrave") then
    fnStdSequence(gciInv3B_KaneGrave, 
    {"[HIDEALLFAST][CHARENTER|Mari]Mari: [E|N]Mr. Foster was always kind of a celebrity in this town. [P][E|Sad]It's a little weird now that he's dead."})
    
elseif(sTopicName == "GeorgeGrave") then
    fnStdSequence(gciInv3B_GeorgeGrave, 
    {"[HIDEALLFAST][CHARENTER|Mari]Mari: [E|N]The whole Foster family's been buried here.[P] Not sure who that is, though."})
    
elseif(sTopicName == "GeorgeJRGrave") then
    fnStdSequence(gciInv3B_GeorgeJrGrave, 
    {"[HIDEALLFAST][CHARENTER|Mari]Mari: [E|N]The whole Foster family's been buried here.[P] Not sure who that is, though."})

elseif(sTopicName == "1Grave") then
    fnStdSequence(gciInv3B_1Grave, 
    {[[[HIDEALLFAST][CHARENTER|Mari]Mari: [E|N] Esther Penrose - \"A promise kept in this life and the next.\"[P][E|Shock] It's kind of ominous, don't you think?]]})

elseif(sTopicName == "2Grave") then
    fnStdSequence(gciInv3B_2Grave, 
    {"[HIDEALLFAST][CHARENTER|Mari]Mari: [E|Sad]This tombstone makes me kind of sad.[P] I wonder what happened to the person who wrote it?"})

elseif(sTopicName == "4Grave") then
    fnStdSequence(gciInv3B_4Grave, 
    {"[HIDEALLFAST][CHARENTER|Mari]Player: Do you know who that is?", 
     "Mari: [E|N]Nope, but can I just say--[P][E|Happy]you're doing a great job out here and I support you."})

elseif(sTopicName == "MyGrave") then
    fnStdSequence(gciInv3B_MyGrave, 
    {"[HIDEALLFAST][CHARENTER|Mari]Mari: [E|Shock]There it is![P][E|Happy] Isn't it cool?", 
     "Player: ...",
     "Mari: [E|Happy].[P].[P].[P][E|Sad] [PLAYERNAME]?"})

elseif(sTopicName == "JaniceGrave") then
    fnStdSequence(gciInv3B_JaniceGrave, 
    {"[HIDEALLFAST][CHARENTER|Mari]Player: Do you know who that is?", 
     "Mari: [E|N]Nope, but can I just say--[P][E|Happy]you're doing a great job out here and I support you."})

elseif(sTopicName == "AnneGrave") then
    fnStdSequence(gciInv3B_AnneGrave, 
    {"[HIDEALLFAST][CHARENTER|Mari]Player: Do you know who that is?", 
     "Mari: [E|N]Nope, but can I just say--[P][E|Happy]you're doing a great job out here and I support you."})

elseif(sTopicName == "EiletteGrave") then
    fnStdSequence(gciInv3B_EiletteGrave, 
    {"[HIDEALLFAST][CHARENTER|Mari]Player: Do you know who that is?",
     "Mari: [E|N]Nope, but can I just say--[P][E|Happy]you're doing a great job out here and I support you."})

elseif(sTopicName == "FroggyStationary") then
    fnStdSequence(gciInv3B_FroggyStationary, 
    {"[HIDEALLFAST][CHARENTER|Mari]Mari: [E|Mad]Oooh, a password![P] Too bad we don't know what it's for."})

elseif(sTopicName == "Journal") then
    fnStdSequence(gciInv3B_Journal, 
    {"[HIDEALLFAST][CHARENTER|Mari]Mari: Hi I'm Chicmonster and didn't write a handler for this item."})

elseif(sTopicName == "Lock") then
    fnStdSequence(gciInv3B_Lock, 
    {"[HIDEALLFAST][CHARENTER|Mari]Mari: [E|N]That shed's been here for ages.[P] I wonder if the groundskeeper has forgotten about it."})

--[ ======================================== Characters ========================================= ]
elseif(sTopicName == "Dog") then
    fnStdSequence(gciInv3B_Char_Dog, 
    {"[HIDEALLFAST][CHARENTER|Mari]Mari: [E|N]Who's a good boy! It's you! A conclusion I have come to based on zero research!"})
    
elseif(sTopicName == "Mari") then
    fnStdSequence(gciInv3B_Char_Mari, 
    {"[HIDEALLFAST][CHARENTER|Mari]Mari: [E|N]Take a look around![P] Maybe you'll see something interesting."})

elseif(sTopicName == "Stacy") then
    fnStdSequence(gciInv3B_Char_Stacy, 
    {"[HIDEALLFAST][CHARENTER|Mari]Mari: [E|Shock]I can't believe Stacy smashed that wall![P][E|Sad] And now she wont even let me give her a back massage."})
    
elseif(sTopicName == "Bret") then
    fnStdSequence(gciInv3B_Char_Bret, 
    {"[HIDEALLFAST][CHARENTER|Mari]Mari: [E|Sad]I hope Bret isn't too upset about not being able to go to the soda factory.[P][E|Shock] I've got to make it up to him somehow!!!"})
    
elseif(sTopicName == "Michelle") then
    fnStdSequence(gciInv3B_Char_Michelle, 
    {"[HIDEALLFAST][CHARENTER|Mari]Mari: [E|Sad]Michelle didn't want to come here.[P][E|N] But today's expedition is important!"})

elseif(sTopicName == "Todd") then
    fnStdSequence(gciInv3B_Char_Todd, 
    {"[HIDEALLFAST][CHARENTER|Mari]Mari: [E|N]I haven't hung out with Todd in ages. He's usually so busy at school.[P][E|Happy] I'm kinda excited he's here!"})

end
