--[Stacy Presentation]
--Showing stuff to Stacy.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--Variables.
local sScriptPath = gsRoot .. "../Chapter 3/Gameplay_SceneC/2 - Present Stacy.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
local sAutoAdvance = gsRoot .. "../Chapter 3/Gameplay_SceneD/7 - PatchGoingHome.lua"

--Saving
gsSaveExecute = gsRoot .. "../Chapter 3/Gameplay_SceneA/4 - Cemetary.lua"
gsSaveTopic = "No Topic"
gsSaveFlag = "InvestigationCemetary"

--[ ========================================= Functions ========================================= ]
--[Friendship Increment]
--If this is the first time presenting this object to this character then we increase the friendship tracker for
-- that character.
local fnIncrementFriendship = function(iObjectIndex, iIncrement)
	
    --Arg check.
    if(iObjectIndex == nil) then return end
    if(iIncrement == nil) then return end
    
    --Build strings.
	local i = iObjectIndex
    local p = iStacyIndex
    local sObjectString = string.format("%02i", i)
    local sSubString    = string.format("%02i", p)
    local iIsRepeat = VM_GetVar("Root/Variables/Chapter3/InvestigationB/iObjectPresent|" .. sObjectString .. sSubString, "N")
    if(iIsRepeat == 0.0) then
        
        --Flag
        VM_SetVar("Root/Variables/Chapter3/InvestigationB/iObjectPresent|" .. sObjectString .. sSubString, "N", 1.0)
    
        --Variable gain.
        local iStacyFriend  = VM_GetVar("Root/Variables/System/Romance/iStacyFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iStacyFriend", "N", iStacyFriend + iIncrement)
    end
end

--[Sequence Handler]
--Almost all sequences can use a standardized template.
local fnStdSequence = function(iFriendshipIndex, saStrings)
    
    --Start.
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    
    for i = 1, #saStrings, 1 do
		fnDialogue(saStrings[i])
    end
    
    --Finish.
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(iFriendshipIndex, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
end

--[ ======================================= Chat Sequences ====================================== ]
--Chatting.
if(sTopicName == "Chat") then
	
	--Variables.
	local iChattedWithMari = VM_GetVar("Root/Variables/Chapter3/Investigation/iChattedWithMari", "N")
	
	--First pass:
	if(iChattedWithMari == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter3/Investigation/iChattedWithMari", "N", 1.0)
		
		--Text.
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
    
		fnDialogue([[[HIDEALLFAST][CHARENTER|Stacy][CHARENTER|Mari]Mari: [E|Shock][CRASH]WELCOME TO THE FOSTER FALLS CEMETARY! ]])
		fnDialogue([[Stacy: [E|N]Isn't this place cool?]])
		fnDialogue([[Player: Uhh, I guess.[P] Are there really dead people buried underneath us?]])
		fnDialogue([[Stacy: [E|N][ITALICS]Oui Oui![ENDITALICS] We come here often as a club because the place is chock full of potential paranormal activity!]])
		fnDialogue([[Mari: [E|N]Someday I know we'll see a ghost or something like that.[P][E|Happy] But check this out--there's a special reason we came today!]])
		fnDialogue([[Thought: The two club captains lead me to one of the graves.]])
		fnDialogue([[Stacy: [E|N]You know how you like, don't know who you are or where you came from?]])
		fnDialogue([[Player: Yeah...]])
		fnDialogue([[Mari: [E|N]Well, we've been doing some digging. Look at that!]])
		fnDialogue([[Stacy: [E|Sad]... Poor choice of words, Mari.]])
		fnDialogue([[Thought: [P][P][P]I glance down and see...[GLITCH]my name.]])
		fnDialogue([[Player: Wh...[P]why[GLITCHTEXT]MAYBE THIS CAN ANSWER THE MAYBE THIS CANNANSWER THE[ENDGLITCHTEXT]is my name on a tombstone?]])
		fnDialogue([[Mari: [E|Shock]Apparently there was someone in the Foster family who passed away as a baby.]])
		fnDialogue([[Stacy: [E|Mad]But they were born in 1995--[P][E|N]that would make them about your age, wouldn't it?]])
		fnDialogue([[Player: I[GLITCH] ]])
		fnDialogue([[[BG|Null][HIDEALL][BG|Null][Music|Null][Music|Null] ]])
		fnDialogue([[[P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P][P]Thought: [P][P]I'm staring at a tree.[P] [BG|Root/Images/Backgrounds/All/Graveyard][ITALICS]What just happened?[ENDITALICS].]])
		fnDialogue([[Thought: I look around--Mari and Stacy are chatting happily over by the Foster monument. I must've blacked out again. ]])
		fnDialogue([[Thought: To avoid arousing suspicion, I decide to keep looking around the cemetary, trying to ignore the uneasy feeling in my stomach.]])
        
        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
    
        --Variable gain.
        local iStacyFriend  = VM_GetVar("Root/Variables/System/Romance/iStacyFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iStacyFriend", "N", iStacyFriend + 5.0)
	
        --Time loss.
        if(fnDecrementTime(5, sAutoAdvance) == false) then
            PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
        end
		
	--Repeat passes.
	else
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
        
		fnDialogue("[HIDEALLFAST][CHARENTER|Stacy][CHARENTER|Mari]Mari: [E|N]I hope you don't mind us taking you here, [PLAYERNAME]!")
		fnDialogue("Stacy: [E|N]If anything might help answer a question about who you are or where you came from, then we're gonna answer it!")
		fnDialogue("Player: [P]Oh.")
		fnDialogue("Mari: [E|Happy]Anyway, take a look around and let us know if you find something interesting!")
        
        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
	end
	
--[ ==================================== Object Presentation ==================================== ]
elseif(sTopicName == "WalkieTalkie" or sTopicName == "Laura") then
    fnStdSequence(gciInv3B_WalkieTalkie, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Stacy: [E|Happy]This was a pretty ingenius idea, if I do say so myself.[P] Laura can tag along wherever we go."})

elseif(sTopicName == "GraveSign") then
    fnStdSequence(gciInv3B_GraveyardSign, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Stacy: [E|N]If I were to die, I'd just come back to life as a hot vampire and like, do parkour around the town.", 
     "Stacy: [E|Happy] I'd suck the blood of every jerk dude I see."})

elseif(sTopicName == "KaneGrave") then
    fnStdSequence(gciInv3B_KaneGrave, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Stacy: [E|Mad][ITALICS]Adieu[ENDITALICS], Mr. Foster.[P][E|Shock] I hear the funeral was massive."})

elseif(sTopicName == "GeorgeGrave") then
    fnStdSequence(gciInv3B_GeorgeGrave, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Stacy: [E|N]I know lots of stuff, but I don't know too much about George Foster. "})
    
elseif(sTopicName == "GeorgeJRGrave") then
    fnStdSequence(gciInv3B_GeorgeJrGrave, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Stacy: [E|Shock]I think I remember that guy died really young."})

elseif(sTopicName == "1Grave") then
    fnStdSequence(gciInv3B_1Grave, 
    {[[[HIDEALLFAST][CHARENTER|Stacy]Stacy: [E|N]\"A promise kept in this life and the next\".[P][E|Mad] Damn, that's so mysterious and cool!]]})

elseif(sTopicName == "2Grave") then
    fnStdSequence(gciInv3B_2Grave, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Stacy: [E|Shock]What's that? Sorry, I was just thinking about my excercise routine.[P][E|Blush] Tomorrow's leg day!"})

elseif(sTopicName == "4Grave") then
    fnStdSequence(gciInv3B_4Grave, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Stacy: [E|Sad]That person's grave doesn't say much, but you can tell they're missed."})

elseif(sTopicName == "MyGrave") then
    fnStdSequence(gciInv3B_MyGrave, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Player: Why is my name on a tombstone?", 
     "Stacy: [E|Shock]I don't know.[P][E|N] Mari and I were hoping if we brought you here, you might... remember something?",
     "Player: I have a hard time remembering anything, let alone being dead.",
     "Stacy: [E|Sad]Yeah, we didn't really think it through.[P][E|Blush] But it was worth trying, [ITALICS]n'est ce pas[ENDITALICS]?"})

elseif(sTopicName == "JaniceGrave") then
    fnStdSequence(gciInv3B_JaniceGrave, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Stacy: [E|Shock]What's that? Sorry, I was just thinking about my excercise routine.[P][E|Blush] Tomorrow's leg day"})

elseif(sTopicName == "AnneGrave") then
    fnStdSequence(gciInv3B_AnneGrave, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Stacy: [E|Shock]What's that? Sorry, I was just thinking about my excercise routine.[P][E|Blush] Tomorrow's leg day"})

elseif(sTopicName == "EiletteGrave") then
    fnStdSequence(gciInv3B_EiletteGrave, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Stacy: [E|Happy]HAH! that's a badass slogan.[P][E|N] I wish we knew who's buried under there."})

elseif(sTopicName == "FroggyStationary") then
    fnStdSequence(gciInv3B_FroggyStationary, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Stacy: [E|Shock]A password! Cool! [P][E|Happy]Now we can hack into someone's secret vault and steal all their precious jewels.", 
     "Player: I don't think that's what this is for.",
     "Stacy: [E|Mad]We'll see who has the last laugh when [ITALICS]I'm[ENDITALICS] wearing a bunch of cool jewels."})

elseif(sTopicName == "Journal") then
    fnStdSequence(gciInv3B_Journal, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Player: Hey, what's this book say?", 
     "Thought: Stacy takes the journal and flips to a random page.",
     [[Stacy: [E|N]\"Most people wonder what their own funeral might be like...\" ]],
     [[Stacy: [E|Shock] \"... what a cruel irony that people save their kind words until after you've shuffled off the old mortal coil.\" ]],
     [[Stacy: [E|Mad]\"'Perhaps I was too hard on the boy'.[P] That's all father had to say.\" ]],
     [[Stacy: \"Even now he refuses to repent! [P][E|Shock] I'm sure I'll see him in hell.\" ]],
     "Stacy: [E|Sad] Geez, that is one messed up family."})

elseif(sTopicName == "Lock") then
    fnStdSequence(gciInv3B_Lock, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Stacy: [E|Shock]There's probably nothing in there, but I can break in if you want.", 
     "Player: N-no, that's okay![P] Let's not break anything today, thanks."})

--[ ======================================== Characters ========================================= ]
elseif(sTopicName == "Dog") then
    fnStdSequence(gciInv3B_Char_Dog, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Stacy: [E|N]Hey little fella!",
     "Thought: The dog pokes his head out of my backpack, and Stacy scratches the patch of fur right between the dog's eyes.",
     "Player: Okey doke then.",
     "Stacy: [P][P][P][E|Mad][ITALICS]I would die for you.[ENDITALICS] "})

elseif(sTopicName == "Mari") then
    fnStdSequence(gciInv3B_Char_Mari, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Stacy: [E|N]There's Mari.[P] What a gal."})

elseif(sTopicName == "Stacy") then
    fnStdSequence(gciInv3B_Char_Stacy, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Stacy: [E|N]Don't worry about me![P] I've never felt better!"})

elseif(sTopicName == "Bret") then
    fnStdSequence(gciInv3B_Char_Bret, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Stacy: [E|Sad]I kind of feel bad we couldn't go to the soda factory.[P][E|Happy] But we'll make it up to Bret somehow."})
    
elseif(sTopicName == "Michelle") then
    fnStdSequence(gciInv3B_Char_Michelle, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Stacy: [E|N]Don't worry about Michelle pouting over there.[P] I'm pretty sure she's not actually afraid of anything out here."})

elseif(sTopicName == "Todd") then
    fnStdSequence(gciInv3B_Char_Todd, 
    {[[[HIDEALLFAST][CHARENTER|Stacy]Stacy: [E|N]Well I'll be, Todd Krause is here--the famous \"personal tutor\" of yours.]],
     "Stacy: [E|Blush] Or maybe we should call him your [ITALICS]senpai[ENDITALICS],[P] hehehehe!",
     "Player: Yeah.[P] Don't you know him?",
     "Stacy: [E|N]Mari knows him better than I do.[P][E|Blush] But since he's your tutor, you might know more than both of us."})

end
