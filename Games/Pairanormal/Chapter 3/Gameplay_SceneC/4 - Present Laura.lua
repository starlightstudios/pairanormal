-- |[Laura Presentation]|
--Showing stuff to Laura.

-- |[Arg Resolve]|
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--Variables.
local sScriptPath = gsRoot .. "../Chapter 3/Gameplay_SceneC/4 - Present Laura.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
local sAutoAdvance = gsRoot .. "../Chapter 3/Gameplay_SceneD/7 - PatchGoingHome.lua"

--Saving
gsSaveExecute = gsRoot .. "../Chapter 3/Gameplay_SceneA/4 - Cemetary.lua"
gsSaveTopic = "No Topic"
gsSaveFlag = "InvestigationCemetary"

-- |[ ======================================= Functions ======================================== ]|
-- |[Friendship Increment]|
--If this is the first time presenting this object to this character then we increase the friendship tracker for
-- that character.
local fnIncrementFriendship = function(iObjectIndex, iIncrement)
	
    --Arg check.
    if(iObjectIndex == nil) then return end
    if(iIncrement == nil) then return end
    
    --Build strings.
	local i = iObjectIndex
    local p = iLauraIndex
    local sObjectString = string.format("%02i", i)
    local sSubString    = string.format("%02i", p)
    local iIsRepeat = VM_GetVar("Root/Variables/Chapter3/InvestigationB/iObjectPresent|" .. sObjectString .. sSubString, "N")
    if(iIsRepeat == 0.0) then
        
        --Flag
        VM_SetVar("Root/Variables/Chapter3/InvestigationB/iObjectPresent|" .. sObjectString .. sSubString, "N", 1.0)
    
        --Variable gain.
        local iLauraFriend  = VM_GetVar("Root/Variables/System/Romance/iLauraFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iLauraFriend", "N", iLauraFriend + iIncrement)
    end
end

-- |[Sequence Handler]|
--Almost all sequences can use a standardized template.
local fnStdSequence = function(iFriendshipIndex, saStrings)
    
    --Start.
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    
    for i = 1, #saStrings, 1 do
		fnDialogue(saStrings[i])
    end
    
    --Finish.
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(iFriendshipIndex, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
end

-- |[ ===================================== Chat Sequences ===================================== ]|
--Chatting.
if(sTopicName == "Chat") then
	
	--Variables.
	local iChattedWithLaura = VM_GetVar("Root/Variables/Chapter3/Investigation/iChattedWithLaura", "N")
	
	--First pass:
	if(iChattedWithLaura == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter3/Investigation/iChattedWithLaura", "N", 1.0)
		
		--Text.
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
    
		fnDialogue([[[HIDEALLFAST][CHARENTER|Walkie]Player: H-hello?]])
		fnDialogue([[Laura: H-hello![P] Hi there![P] Wow, I can't believe I can *ahem* hear you from this far out.]])
		fnDialogue([[Player: O-oh?]])
		fnDialogue([[Laura: Gosh, this brings back memories!]])
		fnDialogue([[Laura: This is h-how I used to communicate with the club--[P]hrm-- a couple years ago.[P] It's actually way more fun than texting.]])
		fnDialogue([[Player: Heh.[P] If you say so.]])
		fnDialogue([[Laura: Well, I'm on call if you need me--[P]o-oh, one more thing!]])
		fnDialogue([[Laura: D-d-DON'T press the big red button on top of the walkie, please.]])
		fnDialogue([[Thought: I glance at the handset and locate it between the speaker and the antennae.]])
		fnDialogue([[Player: Why not?]])
		fnDialogue([[Laura: It's a s-siren--[P]sets off a loud, [ITALICS]annoying[ENDITALICS] noise.[P] I can hear it, too, and I *ahem* h-HATE the sound.]])
		fnDialogue([[Laura: Sends me into an a-a-awful panic. [P]Also, since you guys are technically trespassing, you probably don't want to alert anyone to your presence.]])
		fnDialogue([[Laura: *hrm*.[P] So pretty please don't press it!]])
		fnDialogue([[Player: You got it.[P] I won't.]])
		fnDialogue([[Laura: T-this is gonna be so fun!]])
        
        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
    
        --Variable gain.
        local iLauraFriend  = VM_GetVar("Root/Variables/System/Romance/iLauraFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iLauraFriend", "N", iLauraFriend + 5.0)
	
        --Time loss.
        if(fnDecrementTime(5, sAutoAdvance) == false) then
            PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
        end
		
	--Repeat passes.
	else
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
        
		fnDialogue([[[HIDEALLFAST][CHARENTER|Laura]Player: Chicmonster didn't write this part.]])
        
        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
	end
	
-- |[ ================================== Object Presentation =================================== ]|
elseif(sTopicName == "WalkieTalkie" or sTopicName == "Laura") then
    fnStdSequence(gciInv3B_WalkieTalkie, 
    {"[HIDEALLFAST][CHARENTER|Walkie]Laura: Roger that! [P]over and out!", 
     "Player: What's that mean?",
     "Laura: [SFX|Ugh]You know, I a-actually have no idea."})

elseif(sTopicName == "GraveSign") then
    fnStdSequence(gciInv3B_GraveyardSign, 
    {"[HIDEALLFAST][CHARENTER|Walkie]Laura: I h-hope by the time I die, technology is advanced enough to make *ahem*[P] lifelike AI possible.", 
     "Laura: My virtual ghost c-could share cat pictures and butts until the end of time![P] *giggle*"})

elseif(sTopicName == "KaneGrave") then
    fnStdSequence(gciInv3B_KaneGrave, 
    {"[HIDEALLFAST][CHARENTER|Walkie]Info: *kssssshhhht*", 
     "Thought: Nothing but static.[P] Laura must be away from the walkie right now."})

elseif(sTopicName == "GeorgeGrave") then
    fnStdSequence(gciInv3B_GeorgeGrave, 
    {"[HIDEALLFAST][CHARENTER|Walkie]Laura: A-as I'm sure you've figured out, the town *ahem* is named... um, after George Foster and his family."})
    
elseif(sTopicName == "GeorgeJRGrave") then
    fnStdSequence(gciInv3B_GeorgeJrGrave, 
    {"[HIDEALLFAST][CHARENTER|Walkie]Laura: I don't know much about, um...[P] George Jr.", 
     "Thought: I can hear gentle keyboard clicking on the other end.",
     "Laura: Died 1951, age 30.[P] *ahem* Goodness.[P] T-that's kind of young."})

elseif(sTopicName == "1Grave") then
    fnStdSequence(gciInv3B_1Grave, 
    {"[HIDEALLFAST][CHARENTER|Walkie]Laura: Esther Penrose? *hrm* Let's see.", 
     "Thought: I can hear gentle keyboard clicking on the other end.",
     "Laura: Nothing about her except her obituary.[P] S-she died in a street shooting."})

elseif(sTopicName == "2Grave") then
    fnStdSequence(gciInv3B_2Grave, 
    {"[HIDEALLFAST][CHARENTER|Walkie]Info: *kssssshhhht*",
     "Thought: Nothing but static.[P] Laura must be away from the walkie right now."})

elseif(sTopicName == "4Grave") then
    fnStdSequence(gciInv3B_4Grave, 
    {"[HIDEALLFAST][CHARENTER|Walkie]Info: *kssssshhhht*", 
     "Thought: Nothing but static.[P] Laura must be away from the walkie right now."})

elseif(sTopicName == "MyGrave") then
    fnStdSequence(gciInv3B_MyGrave, 
    {"[HIDEALLFAST][CHARENTER|Walkie]Player: Did you know there's a tombstone here with my name on it?", 
     [[Laura: That's not *hrm* unusual, lots of people share names.[P] I've seen a \"Laura\" in the graveyard myself.]],
     "Player: Oh.",
     "Laura: She was a b-bartender who died in a brawl in 1937.[P] I was rather adventurous, wouldn't you say?[P] *giggle*"})
    
elseif(sTopicName == "JaniceGrave") then
    fnStdSequence(gciInv3B_JaniceGrave, 
    {"[HIDEALLFAST][CHARENTER|Walkie]Laura: Janice Foster w-was also an important member in the *ahem* Foster dynasty.[P] She ran a women's clinic for those who couldn't concieve. "})

elseif(sTopicName == "AnneGrave") then
    fnStdSequence(gciInv3B_AnneGrave, 
    {"[HIDEALLFAST][CHARENTER|Walkie]Info: *kssssshhhht*",
     "Thought: Nothing but static.[P] Laura must be away from the walkie right now."})

elseif(sTopicName == "EiletteGrave") then
    fnStdSequence(gciInv3B_EiletteGrave, 
    {"[HIDEALLFAST][CHARENTER|Walkie]Info: *kssssshhhht*",
     "Thought: Nothing but static.[P] Laura must be away from the walkie right now."})

elseif(sTopicName == "FroggyStationary") then
    fnStdSequence(gciInv3B_FroggyStationary, 
    {[[[HIDEALLFAST][CHARENTER|Walkie]Player: Does \"The bad year\" mean anything to you?]], 
     "Laura: It sure does![P] I've h-had plenty of them before."})

elseif(sTopicName == "Journal") then
    fnStdSequence(gciInv3B_Journal, 
    {"[HIDEALLFAST][CHARENTER|Walkie]Laura: Chicmonster didn't write this part."})

elseif(sTopicName == "Lock") then
    fnStdSequence(gciInv3B_Lock, 
    {[[[HIDEALLFAST][CHARENTER|Walkie]Player: I don't suppose you can... \"hack\" number pads or anything like that, right?[P] I-is that a thing?]], 
     "Thought: I hear Laura giggling over the walkie talkie.",
     "Laura: I wish."})

-- |[ ======================================= Characters ======================================= ]|
elseif(sTopicName == "Dog") then
    fnStdSequence(gciInv3B_Char_Dog, 
    {"[HIDEALLFAST][CHARENTER|Walkie]Laura: I heard your dog scrambling around earlier. I think he hit the send button on the walkie-talkie."})
    
elseif(sTopicName == "Mari") then
    fnStdSequence(gciInv3B_Char_Mari, 
    {"[HIDEALLFAST][CHARENTER|Walkie]Laura: Mari often t-takes trips to the cemetery.[P] I wonder why s-she's so intent on going today, though!"})

elseif(sTopicName == "Stacy") then
    fnStdSequence(gciInv3B_Char_Stacy, 
    {"[HIDEALLFAST][CHARENTER|Walkie]Laura: Please remind Stacy NOT to do parkour o-over the tombstones, if you get the chance.",
     "Laura: *hrm*[P] She almost got in trouble for that the last time we were here."})
    
elseif(sTopicName == "Bret") then
    fnStdSequence(gciInv3B_Char_Bret, 
    {"[HIDEALLFAST][CHARENTER|Walkie]Laura: Bret probably has all the dates on those tombstones m... memorized by now![P] Haha..."})

elseif(sTopicName == "Michelle") then
    fnStdSequence(gciInv3B_Char_Michelle, 
    {"[HIDEALLFAST][CHARENTER|Walkie]Player: Does Michelle not like cemeteries? She really didn't want to go today.",
     "Laura: A-as I recall, she *hrmm* never seemed to have an issue before.",
     "Laura: We've gone t-to the cemetary plenty of... plenty of times."})

elseif(sTopicName == "Todd") then
    fnStdSequence(gciInv3B_Char_Todd, 
    {"[HIDEALLFAST][CHARENTER|Walkie]Laura: [E|N]Chicmonster didn't write this part."})
end
