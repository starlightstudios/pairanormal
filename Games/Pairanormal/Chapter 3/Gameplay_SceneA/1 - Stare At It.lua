--[Stare At It]
--Gotta stare at something.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--[Execution]
--Let's look at the stuffed deer!
if(sTopicName == "Deer God") then
    fnDialogue("Thought: I casually stroll over to the stuffed head of the deer.")
    fnDialogue("[CHARENTER|AJBackpack]Dog: *sniff* *sniff*. emcellent.[P] The food is here.")
    fnDialogue("Player: DO NOT eat the dead animal exhibit!")
    fnDialogue("Dog: Dead meat and sugar.[P] Such tempting temptation.[P] If humman truly want prevemting disaster--[P]feeding me tasty treat, please now.")
    fnDialogue("[HIDEALL]Thought: The museum guide gestures to the exhibits around me. ")
    fnDialogue("[SHOWCHAR|Three|Blush]Curator: [FOCUS|Three][VOICE|Voice|Three]George Foster was a savvy businessman,[P] but one of his greater passions was hunting--")
    fnDialogue("Thought: The deer's glassy dead eyes peer down at me,[P] and I get goosebumps.")
    fnDialogue("Thought: [ITALICS]Ugh...[P][P] if Mari and Stacy were here,[P] they'd probably tell a joke to make me feel better.[ENDITALICS]")
    fnDialogue("Thought: [ITALICS]Probably about how that deer has a little booger hanging out of his nose or something.[ENDITALICS]")
    fnDialogue("Curator: [FOCUS|Three][VOICE|Voice|Three][EMOTION|Three|Blush]--arrived in Pine Falls,[P] he fell in love with the variety of wildlife that could be found here.")
    fnDialogue("Curator: [FOCUS|Three][VOICE|Voice|Three][EMOTION|Three|Blush]The museum carries original diary entries--")
    fnDialogue("Thought: I can't help but reflect on my relationship with the two club captains.")
    fnDialogue("Thought: I talk with Mari the most.[P] Something about her presence is really comforting.")
    fnDialogue("Thought: I mean,[P] she's always going out of her way to be nice to me.[P] Kinda makes me want to return the favor somehow.")
    fnDialogue("Curator: [FOCUS|Three][VOICE|Voice|Three][EMOTION|Three|Blush]--Found that the drink contained unique preservative properties.")
    fnDialogue("Thought: In Stacy's case, I keep trying to hang out with her, but she seems generally preoccupied.[P] I hope that can change someday.")
    
    --Rejoin path.
    LM_ExecuteScript(LM_GetCallStack(0), "Path Rejoin")
    
--Nobody takes pictures of interesting things.
elseif(sTopicName == "Picture This") then
    fnDialogue("Thought: I casually stroll over to the collection of photos on the wall.")
    fnDialogue("[CHARENTER|AJBackpack]Dog: *sniff* *sniff*... Mmf.[P] Nnot smellink any TASTY treats over here. [P]But suppose can mmake do.")
    fnDialogue("Player: I took you for a walk JUST before we came here, would it kill you not to eat or poop for just one hour?")
    fnDialogue("Dog: of course not kill! Dog VERY hardy, true fact everyone knows.")
    fnDialogue("Dog: [SFX|Dog]But dog am also booooored.")
    fnDialogue("[HIDEALL]Thought: The museum guide gestures to the exhibits around me.")
    fnDialogue("[SHOWCHAR|Three|Happy]Curator: [FOCUS|Three][VOICE|Voice|Three][EMOTION|Three|Blush]--economy began getting worse.")
    fnDialogue("Curator: [FOCUS|Three][VOICE|Voice|Three][EMOTION|Three|Blush]There wasn't enough work in the town, and after nearly 40 years of searching, oil was never found in Foster Falls--")
    fnDialogue("Thought: The museum has several photos.[P] Groups of people posing together who I don't recognize, buildings I've never seen, all of them probably important.")
    fnDialogue("Thought: I bet Bret would know what they were.")
    fnDialogue("Curator: [FOCUS|Three][VOICE|Voice|Three][EMOTION|Three|Blush]--until 1971, when he perfected the formula for Kane Kola.")
    fnDialogue("Curator: [FOCUS|Three][VOICE|Voice|Three][EMOTION|Three|Blush]While the original factory burnt down in the 70s, the facilities quickly relocated and Foster Falls has been producing--")
    fnDialogue("Thought: Out of the corner of my eye, I see the guide pull out an empty bottle of the infamous cola.")
    fnDialogue("Thought: I tense, half expecting someone to try another Kane Kola Cancel.")
    fnDialogue("Player: [ITALICS]s-stop thinking about that.[P] It happened a long time ago.[ENDITALICS]")

    --If the player walked home with Bret:
    local iBretMentionedFactory = VM_GetVar("Root/Variables/System/Romance/iBretMentionedFactory", "N")
    if(iBretMentionedFactory == 1.0) then
        fnDialogue("Thought: I focus on the photographs, remembering my conversation with Bret a while ago.")
        fnDialogue("Thought: He said he was interested in going to the old soda factory.[P] I really hope he gets up the courage to invite the club along.[P][ITALICS] Or at least me![ENDITALICS]")
    
    --Player did not walk home with Bret:
    else
        fnDialogue("Thought: Bret's a big fan of soda stuff.")
        fnDialogue("Thought: I've got to remember to ask him how that particular interest started. As it is, we don't talk too much.[P] Maybe that can change.")
    end
    
    --Rejoin path.
    LM_ExecuteScript(LM_GetCallStack(0), "Path Rejoin")

--This is a lame pun.
elseif(sTopicName == "Deep Quartz Mining") then
    fnDialogue("Thought: I casually stroll over to the giant quartz.")
    fnDialogue("[CHARENTER|AJBackpack]Dog: *sniff* *sniff*... Mmf.[P] Nnot smellink any TASTY treats over here. [P]But suppose can mmake do.")
    fnDialogue("Player: I took you for a walk JUST before we came here, would it kill you not to eat or poop for just one hour?")
    fnDialogue("Dog: of course not kill! Dog VERY hardy, true fact everyone knows.")
    fnDialogue("Dog: [SFX|Dog]But dog am also booooored.")
    fnDialogue("[HIDEALL]Thought: The museum guide gestures to the exhibits around me.")
    fnDialogue("[SHOWCHAR|Three|Happy]Curator: [FOCUS|Three][VOICE|Voice|Three][EMOTION|Three|Blush]Foster Oil & Ore risked a heavy portion of their assets searching Pine Falls for oil.")
    fnDialogue("Curator: [FOCUS|Three][VOICE|Voice|Three][EMOTION|Three|Blush]Unfortunately, even now, all they had to show for it are our now famous giant quartz deposits--")
    fnDialogue("Thought: I gaze at the natural facets of the massive crystal.[P] My dirtied reflection looks back at me from each tiny pink surface.")
    fnDialogue("Thought: The shifting warm magenta hues remind me a lot of the colors in Michelle's grafitti.")
    fnDialogue([[Curator: [FOCUS|Three][VOICE|Voice|Three][EMOTION|Three|Blush]--The eldest son, noticed this unusual property in the quartz and began developing a brand new battery system known as \"quartzification\".]])
    fnDialogue("Curator: [FOCUS|Three][VOICE|Voice|Three][EMOTION|Three|Blush]Had it been successful, American energy--")
    fnDialogue("Thought: What is Michelle's deal? Some days, she has a sort of standoffish attitude.[P] Other times she's surprisingly vulnerable with me.")
    fnDialogue("Curator: [FOCUS|Three][VOICE|Voice|Three][EMOTION|Three|Blush]--Sadly, much of this research was lost in the same mine explosion--")
    fnDialogue("Thought: Every time I see her I don't know if she's going to trip me or compliment me.")
    fnDialogue("Thought: Although to be fair, we don't know each other that well. Maybe that can change.")
    
    --Rejoin path.
    LM_ExecuteScript(LM_GetCallStack(0), "Path Rejoin")

--This is a lame pun, but it's better than 'Deep Quartz Mining'.
elseif(sTopicName == "Model Behavior") then
    fnDialogue("Thought: I casually stroll over to the scale model of Foster Falls.")
    fnDialogue("[CHARENTER|AJBackpack]Dog: true fact, all this town is Dog's town now. have marked in EVERY corner of bush.[P][SFX|Dog] Many years effort, now, akkomplished.")
    fnDialogue("Player: Thank you for putting that image into my head.")
    fnDialogue("Dog: Every bush except!!! Bush by gate in little grassy lane. CONTSTANTLY marked by cat, no matter how hhard I'm try.[P] but Someday, I eat.")
    fnDialogue("[HIDEALL]Thought: The museum guide gestures to the exhibits around me.")
    fnDialogue("[SHOWCHAR|Three|Happy]Curator: [FOCUS|Three][VOICE|Voice|Three][EMOTION|Three|Blush]Foster Falls' economy is bolstered by two of our largest businesses:")
    fnDialogue("Curator: [FOCUS|Three][VOICE|Voice|Three][EMOTION|Three|Blush]Foster Industries and Froggy Office Supply. Since the 1950s these two companies--")
    fnDialogue("Thought: I think I actually recognize Froggy Office Supply. Most kids in school, including me, have their stationary in our backpacks.")
    fnDialogue("Thought: Thinking about school supplies reminds me of Todd.")
    fnDialogue("Curator: [FOCUS|Three][VOICE|Voice|Three][EMOTION|Three|Blush]--Once known as Ferry Pine, was purchased by a pencil factory and became the Froggy Office Supply we're now--")

    --Walked home with Todd:
    if(true) then
        fnDialogue("Thought: I never felt so stupid as when I tried to walk home with Todd that one time. Nowadays I can barely look him in the eye for our tutoring sessions.")
        fnDialogue("Thought: Harper was right; maybe I was wrong to think he was actually my friend.")
        fnDialogue("Curator: [FOCUS|Three][VOICE|Voice|Three][EMOTION|Three|Blush]--The Foster Falls Quartz Fair. It is surprisingly large tourist attraction, and many spiritual travelers and pagan practitioners--")
    
    --Did not walk home with Todd:
    else
        fnDialogue("Thought: Todd and I remain distant at school. [P]I don't know why, but when we're in public I get very nervous and awkward around him.")
        fnDialogue("Thought: He's usually surrounded by his friends, anyway, so he's probably too busy for me.[P] I hope that won't be the case forever.")
        fnDialogue("Curator: [FOCUS|Three][VOICE|Voice|Three][EMOTION|Three|Blush]--The Foster Falls Quartz Fair. It is surprisingly large tourist attraction, and many spiritual travelers and pagan practitioners--")
    end
    
    --Rejoin path.
    LM_ExecuteScript(LM_GetCallStack(0), "Path Rejoin")


--Paths rejoin.
elseif(sTopicName == "Path Rejoin") then

	--Save the game.
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"MuseumRejoin\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
	PairanormalLevel_FlagCheckpoint("MuseumRejoin")

    --Dialogue.
    fnDialogue("[HIDEALL]???: [VOICE|Voice|Terry]Give it -here-, you fucking jerkface!")
    fnDialogue("???: [VOICE|Voice|Ross]Shhh! Bro, you're gonna get me in trouble.")
    fnDialogue("???: [VOICE|Voice|Terry]Well GREAT, because if I recall correctly, you're the one always getting ME in fucking trouble!!!")
    fnDialogue("Thought: I peer around until I spot the source of the disturbance -- the twins from the assembly are arguing over another bag of potato chips.")
    fnDialogue("Thought: When they see me staring, they freeze.")
    fnDialogue("[CHARENTER|Terry][CHARENTER|Ross]Terry: Oh, hey new kid. Don't tell on us![P][E|Happy] Food isn't allowed in the building, but I'm [ITALICS]starving[ENDITALICS].")
    fnDialogue("Ross: [E|Happy]... Want some?")
    fnDialogue("Thought: He yanks the bag from his sister, offering it to me.")
    fnDialogue("Player: Uh, okay, sure.")
    fnDialogue("Thought: I walk over, reaching into the admittedly delicious smelling pack of cheese puffs.")
    fnDialogue("Thought: The two of them smile and dig in after me, no longer interested in fighting over it.")
    fnDialogue("[CHARENTER|AJBackpack]Dog: [FOCUS|AJBackpack]NOW AM SMELLING TASTY TREAT. NOW AM SMEALLING TASTY TRET!!! GIVE GIVE GIVE!!")
    fnDialogue("Player: No! Cheese is bad for you, remember?")
    fnDialogue("Dog: [FOCUS|AJBackpack]It is true, dog is understands this. But, am willing to mMake. Noble sacrifice.")
    fnDialogue("Dog: [FOCUS|AJBackpack]Please, for just a crumpb. If humman give tasty tret, will not eat other paper in bag.[P] Yes? Deal?")
    fnDialogue("Thought: Sighing, I sneak a single cheese puff behind me, making sure the twins don't spot the unusual backhand motion.")
    fnDialogue("Thought: A slobbery snout wriggles out of my backpabk zipper and gobbles up the airy puff instantaneously.")
    fnDialogue("Thought: My fingers feel like they were hit by wet lightning.[P] Gross.")
    fnDialogue("[CHAREXIT|AJBackpack]Terry: [E|Happy]Hmph! Well, how are you finding the museum, new kid?")
    fnDialogue("Ross: [E|Sad]We've all been coming here every year since like, kindergarten...")
    fnDialogue("Ross: *munch* *munch* So it's all pretty boring,[P][E|N] but this is like, your first time seeing all this stuff, right?")
    fnDialogue("Player: Yeah. It's all right.")
    fnDialogue("Terry: [E|Happy]If it were up to me, I'd have the class go somewhere actually fucking interesting, like Pine Falls Park or the Foster quartz mines!")
    fnDialogue("Terry: [E|Mad] But as you know, [ITALICS]the people[ENDITALICS] voted for whoever looks the hottest or whatever,[P] and here we are.")
    fnDialogue("Ross: [E|Happy]ASB elections happened ages ago, sis.[P] Let it go already!")
    fnDialogue("Thought: Terry shoves her brother with an alarming amount of force, [P]but he simply bends away from her, totally unaffected like a blade of grass in the breeze.")
    fnDialogue("Terry: [E|Mad]Maybe I'm hating them on your behalf, ever think of that?")
    fnDialogue("Terry: Harper turned you down when you asked him to the spring dance last year.")
    fnDialogue("Thought: Ross flashes a rare frown and shrugs.")
    fnDialogue("Thought: But before I know it his easygoing demeanor is back, and he pats me on the shoulder.")
    fnDialogue("Ross: [E|Blush]Be glad you don't have to deal with crushes and stuff, [PLAYERNAME].[P][E|Sad] Relationships are the one thing that have [ITALICS]no[ENDITALICS] chill.")
    fnDialogue("[HIDEALL]Thought: The museum guide moves to another wing of the building, and the three of us scurry to catch up.")
    fnDialogue("Thought: [BG|Null]By the time I've rejoined the crowd, the twins are out of sight.")
    fnDialogue("Thought: The disembodied voices start to swarm my thoughts again, and I don't see Terry or Ross for the rest of the field trip.")
    fnDialogue("Thought: Too bad--[P]I really wish I had someone to talk to.")

    --Next scene.
    LM_ExecuteScript(gsRoot .. "../Chapter 3/Gameplay_SceneA/2 - Club Meeting.lua", "Start")

end