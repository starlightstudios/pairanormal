--[Cemetary!]
--DEAD PEOPLE.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

fnDialogue("Player: I guess...[P] the cemetary sounds interesting?")
fnDialogue("Thought: Mari and Stacy high five, while Bret's face falls.[P] I feel a little bad.")
fnDialogue("Thought: But I am excited to see a graveyard.[P] The place where people store those who have passed.")
fnDialogue("Thought: My heart beats rapidly just thinking of it, and my palms get sweaty, but I don't exactly know why.")
fnDialogue("[HIDEALL][BG|Null][MUSIC|Null]...")

--Save the game.
fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"CemetaryA\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
PairanormalLevel_FlagCheckpoint("CemetaryA")

fnDialogue("[BG|Root/Images/Backgrounds/All/WalkHome][Music|Investigation][CHARENTER|Mari]Mari: [E|N]--then the ghost was like, touching her hands, and they made pottery together - ")
fnDialogue("Mari: - and I was like,[P][E|Shock] 'whoah, that's so hot',[E|Happy] and that's how I got the idea to start the club!")
fnDialogue("[CHARENTER|Michelle]Michelle: [E|Blush]Wasn't there also a gay kiss in that movie?")
fnDialogue("Mari: [E|Blush]Hee Hee. [P]You betcha.")
fnDialogue("Thought: On the way to the cemetary, the club shares tidbits of banter and jokes.")
fnDialogue("Thought: I like how I never have to think too hard about what to say next with them - [P]somehow, the conversation manages to flow on its own.")
fnDialogue("Thought: Even Bret appears to have cheered up a little.")
fnDialogue("[HIDEALL][CHARENTER|Todd]Todd: [E|Shock][PLAYERNAME]! Hey!")
fnDialogue("Thought: Todd waves at me from the end of the street.[P] It's unusual for him to be by himself, I realize.")
fnDialogue("[CHARENTER|Mari]Mari: [E|Shock][CRASH]TODD![P] What's up, bro!?")
fnDialogue("Todd: [E|N]Not much, just walking home.[P] 'sup with you? What are you doing here?")
fnDialogue("Mari: [E|Happy]We're on a PARANORMAL INVESTIGATION!")
fnDialogue("[CHARENTER|Stacy]Stacy: [E|N]Yeah, it's over at the cemetery. Wanna come along?")
fnDialogue("Stacy: [E|Blush] I've been dying to get to know [ITALICS][PLAYERNAME]'s personal tutor[ENDITALICS] better.")
fnDialogue("Thought: What?[P][P] WHAT?!?")
fnDialogue("Mari: [E|Blush]Can we call you [ITALICS]senpai[ENDITALICS] from now on?")
fnDialogue("Todd: [E|Happy]Pf'hahahaha![P] I'd be happy to tag along, but only if you guarantee [E|Mad][ITALICS]never[ENDITALICS] to call me senpai.")
fnDialogue("Thought: Wait, he agreed?[P] But... but this is different from ASB stuff.[P] Does he know what he's getting into? ")
fnDialogue("[CHARENTER|Bret]Bret: [E|Shock]You mean, you're not bothered by hanging out with the Paranormal Club in a Cemetary?")
fnDialogue("Todd: [E|Blush]I dunno... beats doing homework.[P][E|Happy] Besides, I haven't caught up with Mari in a long time.")
fnDialogue("Mari: [E|Happy]Oh my gosh, that's so TRUE!!![P][E|N] Todd, don't worry, you'll have lots of fun.[P][E|Mad][CRASH] I'LL MAKE SURE.")
fnDialogue("Thought: My tutor and my co-captain are... already friends?[P] Oof, my head can't take all the shifting relationship dynamics.")
fnDialogue("Thought: Todd just smiles and walks alongside us.")
fnDialogue("Thought: I immediately feel awkward and don't talk anymore, but luckily Mari chats with him the rest of the way.")
fnDialogue("[HIDEALL][BG|Null][MUSIC|Null]...")

--Save the game.
fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"CemetaryB\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
PairanormalLevel_FlagCheckpoint("CemetaryB")

fnDialogue("[BG|Root/Images/Backgrounds/All/WalkHome]Thought: Before I can finally think of something to say, we stop.")
fnDialogue("[CHARENTER|Stacy]Stacy: [E|Shock]Oh no, [ITALICS]mierde![ENDITALICS] ")
fnDialogue("Thought: Just off the main road,[P] a little trail barely noticeable to the eye meanders alongside a tall brick wall.")
fnDialogue("Thought: Some of the bricks look brighter and sharper than the rest. We walk up to it.")
fnDialogue("[CHARENTER|Mari][CHARENTER|Todd][CHARENTER|Bret][CHARENTER|Michelle]Stacy: [E|Mad]They must've patched the hole up.")
fnDialogue("Michelle: [E|Happy]Well, we tried, but no luck![P] Might as well go home.")
fnDialogue("Mari: [E|N]Pff, yeah right! We're not giving up that easily.")
fnDialogue("Bret: [E|N]We can just take the long way around if we double back up through Broadway--[E|Shock]do you think the main gate is open in the afternoon?")
fnDialogue("Stacy: [E|Happy]I've got a better idea.")
fnDialogue("[CHAREXIT|ALL]Thought: Stacy gets up close to inspect the wall, brushing her slender fingers across each crack and crevice.")
fnDialogue("Thought: Finding one apparently suitable, she tentatively gives it a shove, and a little bit of the brick crumbles away.")
fnDialogue("[IMAGE|Root/Images/MajorScenes/All/StacyBricks][MUSIC|Somber]Stacy: [CRASH]HRK!")
fnDialogue("Thought: Stacy turns and shoves again, much harder--[P]We stand speechless, because the structure does seem to be wavering.")
fnDialogue("Stacy: [CRASH]GRAH!")
fnDialogue("Thought: She uses her whole back to slam against the bricks, digging her heels deep into the mud as traction.")
fnDialogue("Thought: The action looks painful.")
fnDialogue("Mari: Stacy! Stop![P] It's dangerous!!")
fnDialogue("Stacy: One... more.[P][CRASH] HRR-AAAH!")
fnDialogue("Thought: Another slam.[P] Just as she predicted, the bricks collapse and spill over the other side of the wall, creating a sizeable hole we could all easily climb through.")
fnDialogue("Thought: Before Stacy can shoot back a prideful grin, another section of the wall starts crumbling--[P]towards her.")
fnDialogue("Thought: Player: Look out!!!")
fnDialogue("Thought: We can only watch in horror as she darts out of the way.")
fnDialogue("Thought: She's fast enough to avoid a dark rusty one up high from landing right on her head, but three more nick her shoulder, back, and arm as they come down.")
fnDialogue("Thought: [IMAGE|Null]Stacy winces, but regains her composure quickly.")
fnDialogue("[CHARENTER|Stacy]Stacy: [E|Mad]*huf* *huf* *huf*...[P]Well then.[P][E|N] See? Easy.")
fnDialogue("Thought: After some hesitation, we all climb through the hole and walk to the graveyard on the other side.")
fnDialogue("Thought: I think I'm the only one who notices Stacy's badly scratched hand clenched at her side.")
fnDialogue("[HIDEALL][BG|Null][MUSIC|Null]...")

--Save the game.
fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"CemetaryC\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
PairanormalLevel_FlagCheckpoint("CemetaryC")

fnDialogue("[BG|Root/Images/Backgrounds/All/Graveyard][MUSIC|Investigation]Thought: So.[P] This is a cemetery.")
fnDialogue("Thought: There's statues and stones commemorating the dead, who I've been told are entrenched beneath us...")
fnDialogue("Thought: [ITALICS]why would anyone make this a place to go to?[ENDITALICS][P] I don't think I've ever really thought about death until now.")
fnDialogue("Thought: People who die go away forever, and it must be very sad to lose someone--unless they become a ghost like Mari says some people do.")
fnDialogue("Thought: But what is it like to die?[P] Does it hurt?[P] Does it happen to everyone?")
fnDialogue("[CHARENTER|Mari]Mari: [E|N]All right guys! split up, look around, report anything insteresting--[P][E|Happy] and have fun and drink lots of water, okay?[P] You know the drill.")
fnDialogue("Info: [SFX|Discovery]Investigation[SFX|World|Investigation] #004 -- The Graveyard")
fnDialogue("Thought: Everyone chuckles and splits off. [P]They don't seem too worried about it, so maybe I should'nt be either.")
fnDialogue("Thought: Well... here we go.")

--[Loading the Game]
--If currently loading the game, set up the investigation.
if(PairanormalLevel_IsLoading() == true) then
    
    --Clear checkpoint.
    PairanormalLevel_FlagCheckpoint("InvestigationCemetary")
    fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"InvestigationCemetary\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
    
    --Variables.
    local sBGPath = "Root/Images/Backgrounds/All/Graveyard"
    local sScriptPath = gsRoot .. "../Chapter 3/Gameplay_SceneC/0 - ExamineObject.lua"
    
    --Change the background.
    PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Graveyard")
    PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Graveyard")

    --Boot investigation scripts.
    Dia_SetProperty("Clear Investigation Characters")
    Dia_SetProperty("Clear Investigation")
    Dia_SetProperty("Start Investigation") 
    Dia_SetProperty("Set Investigation Image", "Root/Images/Backgrounds/All/Graveyard")
    Dia_SetProperty("Set Investigation Script", gsRoot .. "../Chapter 3/Gameplay_SceneC/0 - ExamineObject.lua")
    Dia_SetProperty("Set Go Home Strings", gsRoot .. "../Chapter 3/Gameplay_SceneD/0 - PatchGoingHome.lua", "Default")
    
    --Talkable Characters.
    Dia_SetProperty("Add Talkable Character", "Mari",     gsRoot .. "../Chapter 3/Gameplay_SceneC/1 - Present Mari.lua")
    Dia_SetProperty("Add Talkable Character", "Stacy",    gsRoot .. "../Chapter 3/Gameplay_SceneC/2 - Present Stacy.lua")
    Dia_SetProperty("Add Talkable Character", "Bret",     gsRoot .. "../Chapter 3/Gameplay_SceneC/3 - Present Bret.lua")
    Dia_SetProperty("Add Talkable Character", "Walkie",   gsRoot .. "../Chapter 3/Gameplay_SceneC/4 - Present Laura.lua")
    Dia_SetProperty("Add Talkable Character", "Michelle", gsRoot .. "../Chapter 3/Gameplay_SceneC/5 - Present Michelle.lua")
    Dia_SetProperty("Add Talkable Character", "AJ",       gsRoot .. "../Chapter 3/Gameplay_SceneC/6 - Present Dog.lua")
    Dia_SetProperty("Add Talkable Character", "Todd",     gsRoot .. "../Chapter 3/Gameplay_SceneC/7 - Present Todd.lua")
    
    --Talkable characters as inventory objects.
    local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
    Dia_SetProperty("Add Discovered Object", "Mari",     sCharPath, sScriptPath, "Mari",     1322,   2, 1322 + 438,   2 + 363)
    Dia_SetProperty("Add Discovered Object", "Stacy",    sCharPath, sScriptPath, "Stacy",     442, 367,  442 + 438, 367 + 363)
    Dia_SetProperty("Add Discovered Object", "Bret",     sCharPath, sScriptPath, "Bret",        2,   2,    2 + 438,   2 + 363)
    Dia_SetProperty("Add Discovered Object", "Laura",    sCharPath, sScriptPath, "Laura",     882,   2,  882 + 438,   2 + 363)
    Dia_SetProperty("Add Discovered Object", "Michelle", sCharPath, sScriptPath, "Michelle", 1762,   2, 1762 + 438,   2 + 363)
    Dia_SetProperty("Add Discovered Object", "Dog",      sCharPath, sScriptPath, "Dog",      1762, 367, 1762 + 438, 367 + 363)
    Dia_SetProperty("Add Discovered Object", "Todd",     sCharPath, sScriptPath, "Todd",        2, 367,    2 + 438, 367 + 363)
    Dia_SetProperty("Clear Discovered Object")
    
    --SLF file adds the examinable objects.
    Dia_SetProperty("Parse SLF File", gsRoot .. "../Chapter 3/MapData/InvestigationAB.slf")
    
    --Check all the items and re-add them to the inventory.
    for i = 1, gciInv3B_Variable_Total-1, 1 do
        
        --Variable name.
        local sVarName = "Root/Variables/Chapter3/InvestigationB/iObject|" .. gczaInv3B_List[i]
        local iHasItem = VM_GetVar(sVarName, "N")
        
        --Has the item. Add it.
        if(iHasItem == 1.0) then
            Dia_SetProperty("Add Discovered Object", gczaInv3B_List[i], sBGPath, sScriptPath, gczaInv3B_List[i], 0, 0, 1, 1)
        end
        Dia_SetProperty("Clear Discovered Object")
    end

    --Modify the time max/left.
    VM_SetVar("Root/Variables/System/Investigation/sInvestigationPath", "S", "Root/Variables/Chapter3/Investigation/")
    local iTimeMax  = VM_GetVar("Root/Variables/Chapter3/Investigation/iTimeMax",  "N")
    local iTimeLeft = VM_GetVar("Root/Variables/Chapter3/Investigation/iTimeLeft", "N")
    Dia_SetProperty("Investigation Time Max",  iTimeMax)
    Dia_SetProperty("Investigation Time Left", iTimeLeft)

--[Normal Operations]
--Otherwise, flag the game to save and boot the investigation normally.
else

    --Set the time max/left.
    fnCutsceneInstruction([[ VM_SetVar("Root/Variables/Chapter3/Investigation/iTimeMax",  "N", 60.0) ]])
    fnCutsceneInstruction([[ VM_SetVar("Root/Variables/Chapter3/Investigation/iTimeLeft", "N", 60.0) ]])

    --Boot investigation scripts.
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Investigation Characters") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Investigation Time Max", 60) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Investigation Time Left", 60) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Set Investigation Image", "Root/Images/Backgrounds/All/Graveyard") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Set Investigation Script", gsRoot .. "../Chapter 3/Gameplay_SceneC/0 - ExamineObject.lua") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Set Go Home Strings", gsRoot .. "../Chapter 3/Gameplay_SceneD/7 - PatchGoingHome.lua", "Default") ]])

    --Talkable characters.
    fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Mari",     gsRoot .. "../Chapter 3/Gameplay_SceneC/1 - Present Mari.lua") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Stacy",    gsRoot .. "../Chapter 3/Gameplay_SceneC/2 - Present Stacy.lua") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Bret",     gsRoot .. "../Chapter 3/Gameplay_SceneC/3 - Present Bret.lua") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Walkie",   gsRoot .. "../Chapter 3/Gameplay_SceneC/4 - Present Laura.lua") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Michelle", gsRoot .. "../Chapter 3/Gameplay_SceneC/5 - Present Michelle.lua") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "AJ",       gsRoot .. "../Chapter 3/Gameplay_SceneC/6 - Present Dog.lua") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Todd",     gsRoot .. "../Chapter 3/Gameplay_SceneC/7 - Present Todd.lua") ]])
    
    --SLF file adds the examinable objects.
    fnCutsceneInstruction([[ Dia_SetProperty("Parse SLF File", gsRoot .. "../Chapter 3/MapData/InvestigationAB.slf") ]])

    --Add the talkable characters as the first examine objects.
    fnCutsceneInstruction([[ Dia_SetProperty("Add Discovered Object", "Mari",     "Root/Images/Backgrounds/All/CharPortraits", gsRoot .. "../Chapter 3/Gameplay_SceneC/0 - ExamineObject.lua", "Mari",     1322,   2, 1322 + 438,   2 + 363) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Discovered Object", "Stacy",    "Root/Images/Backgrounds/All/CharPortraits", gsRoot .. "../Chapter 3/Gameplay_SceneC/0 - ExamineObject.lua", "Stacy",     442, 367,  442 + 438, 367 + 363) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Discovered Object", "Bret",     "Root/Images/Backgrounds/All/CharPortraits", gsRoot .. "../Chapter 3/Gameplay_SceneC/0 - ExamineObject.lua", "Bret",        2,   2,    2 + 438,   2 + 363) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Discovered Object", "Laura",    "Root/Images/Backgrounds/All/CharPortraits", gsRoot .. "../Chapter 3/Gameplay_SceneC/0 - ExamineObject.lua", "Laura",     882,   2,  882 + 438,   2 + 363) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Discovered Object", "Michelle", "Root/Images/Backgrounds/All/CharPortraits", gsRoot .. "../Chapter 3/Gameplay_SceneC/0 - ExamineObject.lua", "Michelle", 1762,   2, 1762 + 438,   2 + 363) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Discovered Object", "Dog",      "Root/Images/Backgrounds/All/CharPortraits", gsRoot .. "../Chapter 3/Gameplay_SceneC/0 - ExamineObject.lua", "Dog",      1762, 367, 1762 + 438, 367 + 363) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Discovered Object", "Todd",     "Root/Images/Backgrounds/All/CharPortraits", gsRoot .. "../Chapter 3/Gameplay_SceneC/0 - ExamineObject.lua", "Todd",        2, 367,    2 + 438, 367 + 363) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])

    fnCutsceneBlocker()
    
    --Time variable.
    VM_SetVar("Root/Variables/System/Investigation/sInvestigationPath", "S", "Root/Variables/Chapter3/Investigation/")
    
    --Save the game.
    fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"InvestigationCemetary\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
    
end
    