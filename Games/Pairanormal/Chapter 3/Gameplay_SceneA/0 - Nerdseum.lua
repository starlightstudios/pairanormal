--[Exec First]
--Executes when the program starts. This will occur with the argument "Start".

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--[Execution]
--Start.
if(sTopicName == "Start") then
    
    --Loading.
    VM_SetVar("Root/Variables/System/Loading/iLoadChapter2", "N", "1.0")
    VM_SetVar("Root/Variables/System/Loading/iLoadChapter3", "N", "1.0")
    LM_ExecuteScript(gsRoot .. "../Chapter 3/System/000 Global Variables.lua")
    LM_ExecuteScript(gsRoot .. "../Chapter 3/System/100 Chapter Variables.lua")
    LM_ExecuteScript(gsRoot .. "../Chapter 2/System/200 Dialogue Actors.lua")
    LM_ExecuteScript(gsRoot .. "../Chapter 3/System/200 Dialogue Actors.lua")
    
    --Save.
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Nerdseum\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
	PairanormalLevel_FlagCheckpoint("Nerdseum")
    
    --Dialogue.
    fnDialogue("Thought: [HIDEALL][MUSIC|DaytimeRedux][BG|Root/Images/Backgrounds/All/Museum]I absentmindedly tap my pencil against my notebook.")
    fnDialogue([[Thought: Today is the day of my first \"field trip\",[P] when classes can go as a group somewhere outside of the school.]])
    fnDialogue("Thought: I should be excited,[P] but I can barely focus.")
    fnDialogue("[SHOWCHAR|AJBackpack|Neutral]Dog: Mff. Dog is bored, true facct.[P] When is LUNCH-time???")
    fnDialogue("Player: Shhh![P] I told you, you could come along today if you were quiet.")
    fnDialogue("Thought: I absentmindedly nudge the bottom of my backpack with my elbow.[P] I feel four tiny paws scrambling to regain balance.")
    fnDialogue("Thought: This was such a bad idea - but thankfully I haven't been caught yet.")
    fnDialogue("Dog: Deg IS beink quiet?! Am noble, honest. MY word is bond. but the pacct, said NOTHING. About wiggling in bag!!!")
    fnDialogue("[HIDECHAR|AJBackpack][SHOWCHAR|Three|Neutral]Curator: [FOCUS|Three][VOICE|Voice|Three][EMOTION|Three|Blush]--not just the result of a savvy businessman, but a multipronged, multigenerational effort to develop the town. Foster Fall's history -")
    fnDialogue("[HIDEALL][SHOWCHAR|Student4|Neutral]Student: [VOICE|Voice|Generic2]*giggle*[P][GLITCH] *giggle* ")
    fnDialogue("Thought: [CRASH][ITALICS]Ouch![P] Nghh, don't draw attention to yourself.[ENDITALICS] ")
    fnDialogue("Thought: Ever since I began to hear the thoughts of my dog,[P] I've been picking up voices everywhere.")
    fnDialogue("Thought: In fact, maybe they were always there.")
    fnDialogue("Thought: In crowded school halls,[P] the only place I ever really encounter people,[P] it must've been hard to notice amongst the regular chatter.")
    fnDialogue("Thought: In this quiet but crowded museum on the other hand,[P] they're hard to ignore.")
    fnDialogue([[ [HIDEALL][SHOWCHAR|Three|Neutral]Curator: [FOCUS|Three][VOICE|Voice|Three][EMOTION|Three|Blush]--region was known as \"Pine Falls\", a small rural village known only for the humble river which ran through it. ]])
    fnDialogue("Curator: [FOCUS|Three][VOICE|Voice|Three][EMOTION|Three|Blush]George Foster knew it was the perfect place to expand his oil business.[P] And with the second world war fully--")
    fnDialogue("Thought: There's any number of little murmurs in a room with me at one time.")
    fnDialogue("Thought: And if I try to focus on any one of them... [GLITCH] ")
    fnDialogue("Thought: Like,[P] for example,[P][GLITCH][GLITCH] on the girl standing behind me who won't stop giggling... [GLITCH] ")
    fnDialogue("[HIDEALL][IMAGE|Root/Images/MajorScenes/All/LoveNote]Thought: It's kind of like I've jumped into her body.")
    fnDialogue("Thought: I watch her take out a pencil[GLITCH][GLITCHTEXT]jumped into her body[ENDGLITCHTEXT] and circle something on the note in her hands.")
    fnDialogue("[HIDEIMAGE]Thought:[GLITCH] Then a blink,[P] and I'm back.")
    fnDialogue("[HIDEALL][SHOWCHAR|Three|Neutral]Curator: [FOCUS|Three][VOICE|Voice|Three][EMOTION|Three|Blush]--What he discovered instead was a gigantic quartz deposit!")
    fnDialogue("Curator: [FOCUS|Three][VOICE|Voice|Three][EMOTION|Three|Blush]Now, who here has been to Foster Falls' annual gemstone fair?")
    fnDialogue("Thought: Figures--[P]everyone has their hand raised except me. ")
    fnDialogue("Curator: [FOCUS|Three][VOICE|Voice|Three]Absolutely,[P] a Foster Falls tradition!")
    fnDialogue("Curator: [FOCUS|Three][VOICE|Voice|Three]As you know, [GLITCHTEXT]JOnathanJoNAThanJonaTHAN[ENDGLITCHTEXT]quartz and other semi-precious minerals used to be our biggest export,[P] until-- ")
    fnDialogue("Player: [P][P][GLITCH][P][SFX|Ugh]*sigh!*")
    fnDialogue("Thought: Okay,[P] I'm starting to get anxious just standing here.")
    fnDialogue("Thought: Maybe I'll take a look around the museum and try to take my mind off.[P].[P].[P][ITALICS]stuff.[ENDITALICS] ")
    fnDialogue("[HIDEALL]Thought: ...")
    
    --What do?
    fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (What shall I look at?)") ]])
    
	local sDecisionScript = "\"" .. gsRoot .. "../Chapter 3/Gameplay_SceneA/1 - Stare At It.lua\""
    fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"The mounted stuffed deer\", " .. sDecisionScript .. ", \"Deer God\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"The photos on the wall\", " .. sDecisionScript .. ", \"Picture This\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"The giant quartz\", " .. sDecisionScript .. ", \"Deep Quartz Mining\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"The scale model of Foster Falls\", " .. sDecisionScript .. ", \"Model Behavior\") ")
    fnCutsceneBlocker()

--Lets you jump ahead!
elseif(sTopicName == "LevelSkipBoot") then
    
    --Loading.
    VM_SetVar("Root/Variables/System/Loading/iLoadChapter2", "N", "1.0")
    VM_SetVar("Root/Variables/System/Loading/iLoadChapter3", "N", "1.0")
    LM_ExecuteScript(gsRoot .. "../Chapter 3/System/000 Global Variables.lua")
    LM_ExecuteScript(gsRoot .. "../Chapter 3/System/100 Chapter Variables.lua")
    LM_ExecuteScript(gsRoot .. "../Chapter 2/System/200 Dialogue Actors.lua")
    LM_ExecuteScript(gsRoot .. "../Chapter 3/System/200 Dialogue Actors.lua")
	
	--Level Skip.
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "(Want to skip ahead?)") ]])

	local sDecisionScript = "\"" .. gsRoot .. "../Chapter 3/Gameplay_SceneA/0 - Nerdseum.lua\""
	fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Run First Scene\",        " .. sDecisionScript .. ", \"Start\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Club Meeting\",           " .. sDecisionScript .. ", \"Club Meeting\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Cemetary Investigation\", " .. sDecisionScript .. ", \"Cemetary\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Kola Investigation\",     " .. sDecisionScript .. ", \"KaneKola\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Arrive Home\",            " .. sDecisionScript .. ", \"Arrive Home\") ")
	fnCutsceneBlocker()

--Level skip options.
elseif(sTopicName == "Club Meeting") then
	LM_ExecuteScript(gsRoot .. "../Chapter 3/Gameplay_SceneA/2 - Club Meeting.lua", "Start")
elseif(sTopicName == "Cemetary") then
	LM_ExecuteScript(gsRoot .. "../Chapter 3/Gameplay_SceneA/2 - Club Meeting.lua", "Graveyard")
elseif(sTopicName == "KaneKola") then
	LM_ExecuteScript(gsRoot .. "../Chapter 3/Gameplay_SceneA/2 - Club Meeting.lua", "Soda Factory")
elseif(sTopicName == "Arrive Home") then
	LM_ExecuteScript(gsRoot .. "../Chapter 3/Gameplay_SceneD/2 - Arrive At Home.lua", "Start")
end
