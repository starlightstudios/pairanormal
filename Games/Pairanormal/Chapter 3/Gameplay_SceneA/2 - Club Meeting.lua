--[Club Meeting]
--Museums suck.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--[Execution]
--Let's look at the stuffed deer!
if(sTopicName == "Start") then

	--Save the game.
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"MeetingTime\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
	PairanormalLevel_FlagCheckpoint("MeetingTime")
    
    --Dialogue.
    fnDialogue("Thought: [MUSIC|Null][BG|Null]...")
    fnDialogue("Info: [MUSIC|Daytime][BG|Root/Images/Backgrounds/All/Classroom][CRASH]*PAM [P][CRASH]PAM [P][CRASH]PAM*")
    fnDialogue("[CHARENTER|Bret]Bret: [E|Happy]Welcome everyone! Welcome to a Paranormal Club meeting.")
    fnDialogue([[Bret: [E|Neutral] I'm your affable host,[E|Mad] Bret \"The Bretmeister (working title)\" Numan.[P][E|Happy] And I have an exciting proposition for you all.]])
    fnDialogue("Bret: [E|Shock]Er...[P][P][P]guys? ")
    fnDialogue("[CHARENTER|Mari][CHARENTER|Stacy][CHARENTER|AJ]Mari: [CRASH][E|Shock]DOGGIE!!!")
    fnDialogue("Stacy: [E|Mad]Who's a good boy? [P][ITALICS]Who is the goodest boyyyy?[ENDITALICS] ")
    
    fnDialogue("Dog: [E|Happy][SFX|Dog]It is me.[P] I am the dog.")
    fnDialogue("Thought: Now that class is over, I finally have a chance to let the dog wander around.")
    fnDialogue("Thought: But he's proving to be more than a distraction for the club.")
    fnDialogue("[CHARENTER|Michelle]Michelle: [E|Mad]I have just met this dog, and I'm ready to give my life to protect it.")
    fnDialogue("Dog: Is cann human see? This is how Dog SUPPOSETD. to be treating.[P] Very nice, many belly rubs.[P] Hhope you are takingg notice.")
    fnDialogue("Laura: [CHARENTER|Laura][E|Blush]Well, I'm more of a cat person, [P]to be *hrm*[P] honest, *ahem*...[P][E|N]but this little fella is pretty darn cute!")
    fnDialogue("Player: If you think he's cute now, wait 'till he eats your homework.")
    fnDialogue("Player: Fun fact, when I told my teachers what happened, all of them [ITALICS]laughed in my face.[ENDITALICS][P] Don't know what that's all about, but watch out.")
    fnDialogue("Mari: [E|Blush]Heehee! *ahem*.[P]Anyway. Sorry, Bret.[P] I know you were trying to say something.")
    fnDialogue("Mari: [E|Neutral]What's this exciting proposition you've got?")
    fnDialogue("Bret: [E|N]Today, I propose that we go to a never-before-explored location-- [CRASH][E|Shock]KANE COLA, THE ORIGINAL SODA FACTORY!")
    fnDialogue("Thought: Both club captains' eyebrows raise.")
    fnDialogue("Mari: [E|Shock][CRASH]BRET THAT'S TOTALLY GENIUS![P] That soda factory burned down in a fire[P]--I bet there's [ITALICS]so many[ENDITALICS] ghosts there!")
    fnDialogue("Bret: [E|Shock]... Right.[P][E|Blush] Well, actually I just want to go for the soda bottle artifacts,[E|Happy] but that works!")
    fnDialogue("Mari: [E|Shock]Ooh, we could even ask Todd to come along! He knows how to get around the preliminary chicken wire fence")
    fnDialogue("Mari: [E|Mad]--or at least he [ITALICS]said[ENDITALICS] he knew how to,[P] [E|N]once when we were kids.")
    fnDialogue("Player: [SFX|World|Blink]!![P] Todd's gonna be there?")
    fnDialogue("Michelle: [E|Happy] Why are you always so interested in where [ITALICS]Todd[ENDITALICS] is going to be, [PLAYERNAME]?")
    fnDialogue("Player: [SFX|Ugh]I'm nooooooot.")
    fnDialogue("Mari: [E|Happy]Okay, it's official! Let's go to--")
    fnDialogue("Stacy: [E|Shock]--Well, hang on a minute Mari.[P] Remember how we said we wanted to go to the graveyard today?")
    fnDialogue("Michelle: [E|Mad]Oh we [ITALICS]did,[ENDITALICS] did we?")
    fnDialogue("Stacy: [E|Mad]YES, Michelle, you've made it very clear you didn't agree with that particular decision.")
    fnDialogue("Laura: [E|Blush]It's okay, M-michelle.[P] I get [P]*hrm*[P] a little bit nervous in graveyards, too.")
    fnDialogue("Michelle: [E|Shock]Oh hang on--[P][CRASH][E|Mad]AS IF!! I'M NOT AFRAID OF GHOSTS, OKAY? ")
    fnDialogue("Thought: [ITALICS]Who even said anything about ghosts?[ENDITALICS] ")
    fnDialogue("Michelle: [E|Sad]I'm just bored of going to the bloody graveyard all the time. [P]There's [ITALICS]nothing[ENDITALICS] new there, anyway.")
    fnDialogue("Stacy: [E|Sad]Look, I know we've been to the graveyard a few times before,[P][E|Neutral] but I promise you there's something VERY unusual we have to get to the bottom of.")
    fnDialogue("Player: What?")
    fnDialogue("Stacy: [E|Shock]W-well, uh--")
    fnDialogue("Mari: [E|Happy][CRASH]ITS A SURPRISE!")
    fnDialogue("Thought: I tilt my head; Everyone's expectant eyes are on me. [P]I don't know why, but I think they want me to decide for them.")
    
    --What do?
    fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (Where do we go today?)") ]])

    local sDecisionScript = "\"" .. gsRoot .. "../Chapter 3/Gameplay_SceneA/2 - Club Meeting.lua\""
    fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Graveyard\",    " .. sDecisionScript .. ", \"Graveyard\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Soda Factory\", " .. sDecisionScript .. ", \"Soda Factory\") ")
    fnCutsceneBlocker()

--Graveyard!
elseif(sTopicName == "Graveyard") then
    LM_ExecuteScript(fnResolvePath() .. "4 - Cemetary.lua", "Start")

--Flammable Soda Factory!
elseif(sTopicName == "Soda Factory") then
    LM_ExecuteScript(fnResolvePath() .. "3 - Soda Factory.lua", "Start")
end