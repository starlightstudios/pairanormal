-- |[Soda Factory]|
--More like *dumb* factory. Nailed. It.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

fnDialogue("Player: I think I want to check out the soda factory.")
fnDialogue("Mari: [E|N]Well, that clinches it. [P][E|Shock][CRASH]THE SODA FACTORY IT IS.")
fnDialogue("Michelle: [E|Happy]Excellent. Who wants to go to a lame cemetery anyway?")
fnDialogue("Stacy: [E|Sad]*sigh*... Well, it's not really what we had planned...[P][E|Neutral]but what the heck? [ITALICS]Laissez-faire[ENDITALICS]!")
fnDialogue("Laura: [E|Blush]I'll be hanging back, as usual.[P] *cough* But good luck to everyone else!")
fnDialogue("Thought: Bret beams at me.[P] His bright smile and grateful gaze is so intense, I can't help but look away.")
fnDialogue("Bret: [E|Happy][ITALICS]Soda factory adventuuure with Bret and the club![P] just believe in your dreeeeaaaams and they will surely come truuuuue![ENDITALICS]")
fnDialogue("[HIDEALL][MUSIC|Null][BG|Null]...")

--Save the game.
fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"SodaRanged\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
PairanormalLevel_FlagCheckpoint("SodaRanged")

--Continue.
fnDialogue("[CHARENTER|Bret][MUSIC|Investigation][BG|Root/Images/Backgrounds/All/WalkHome]Bret: [E|Neutral]--and if we're really lucky,[P] we'll see a [E|Mad]FIRST EDITION MINER'S EXCLUSIVE bottle,")
fnDialogue("Bret: [E|Shock] with its signature blue-brown tint, [E|Happy]three-point curvature silhouette, [E|Shock]and [ITALICS]sparkling silver bottlecap!![ENDITALICS]")
fnDialogue("Thought: On the way to our investigation, the Paranormal Club's resident thing expert has been setting the scene for us.")
fnDialogue("Thought: After burning down decades ago, what remains of the old Kane Kola Factory has continued to rot away behind one of Foster Falls' poorest neighborhoods.")
fnDialogue("[CHARENTER|AJBackpack]Dog: [FOCUS|AJBackpack]Mmf.[P] It's a good think you want to mate with this humman so much;; Otherwis long talking would cCERTamly drive mad.")
fnDialogue("Player: I-I DONT want to [ITALICS]mate[ENDITALICS] with him, whatever that means!")
fnDialogue("[CHARENTER|Todd]Todd: [E|N]Did you say something, [PLAYERNAME]?")
fnDialogue("Player: [SFX|Blink]Nope![P] Uhh... just, you know.[P] I'm surprised you're tagging along.")
fnDialogue("[CHAREXIT|AJBackpack]Todd: [E|Happy]Pfft, No problem.[P] I remember Mari and I used to get up to all kinds of tomfoolery when we were kids.")
fnDialogue("Todd: [E|Sad] Now that I'm so caught up with ASB and stuff, I...[P]I dunno. I kind of miss it.")
fnDialogue("Player: Heh.")
fnDialogue("Todd: [E|N]Plus, I get to hang out with my favorite pupil!")
fnDialogue([[ Thought: He gives me a friendly nudge, and instead of saying something funny or clever back at him,[P] my mouth opens and a weird \"bleeeaaarp\" sound comes out.]])
fnDialogue("Thought: I promptly cover it up with intense, nervous laughter.")
fnDialogue("Todd: [E|Neutral]... Right. Well, looks like that chickenwire fence I was telling you guys about is up ahead.")
fnDialogue("Todd: [E|Mad]Let's see if I can remember where the opening is.")
fnDialogue("[CHAREXIT|ALL]Player: Yes. Good idea.")
fnDialogue("...")
fnDialogue("[BG|Root/Images/Backgrounds/All/Forest][CHARENTER|AJBackpack]Dog: [FOCUS|AJBackpack]Ah, dog is understanding now.[P] It is HIM humman you wisht, to mating.")
fnDialogue("Player: [CRASH]No![P] Stop staying I want to mate with people.")
fnDialogue("Dog: [FOCUS|AJBackpack]Dog decide, it is lie.[P] What else omther reason come to dirty swamps?")
fnDialogue("Dog: [FOCUS|AJBackpack]true facts, only one;[P] to mmake babies.")
fnDialogue("Thought: Before I can respond, we have to crouch through a small barbed fence opening.")
fnDialogue("[HIDEALL][BG|Null][MUSIC|Null]Thought: It scratches my arms and hair, but one by one, we all pass through.")

--Save the game.
fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Factory\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
PairanormalLevel_FlagCheckpoint("Factory")

--Dialogue.
fnDialogue("...")
fnDialogue("Thought: After a short but tiring walk uphill, the group collectively stops in its tracks.")
fnDialogue("[BG|Root/Images/Backgrounds/All/SodaFactory]Player: ...[P] huh.")
fnDialogue("[CHARENTER|Michelle]Michelle: [EMOTION|Michelle|Shock]Jesus.[P] Fucking.[P] Christ.")
fnDialogue("Thought: Streams of rancid Kane Kola ooze out of cracks and crevices and pool into sticky swamps--the liquid is so thick you can't see to the bottom.")
fnDialogue("Thought: Then there's the dolls. Hundreds of victorian dolls strewn on the ground, strung up on tree limbs, assembled and disassembled in every wrong way.")
fnDialogue("Thought: Up in the trees, I notice distinct movement. If I squint, I can just make it out--squirrels.")
fnDialogue("Thought: They're teeming on every branch, moving from here to there, occasionally stopping to look at us.")
fnDialogue("Player: B-Bret.[P] What [ITALICS]is[ENDITALICS] this?")
fnDialogue("[CHARENTER|Bret]Bret: [EMOTION|Bret|Shock]W-w-what? [P]The dolls that are h-hanging everywhere? [P]*gulp* well... I don't exactly know.")
fnDialogue("I d-d-don't remember, uh...[P] hearing about those in my research.")
fnDialogue("[CHARENTER|Mari]Mari: [E|Shock]C... can anyone SMELL that?")
fnDialogue("[CHARENTER|Todd]Todd: [E|Mad]S-so sweet, but in a gross way.[P] I can taste it...[P] [ITALICS]I can taste it[ENDITALICS].")
fnDialogue("Bret: [E|Sad]Don't worry guys... we can get used to it!")
fnDialogue("Mari: [E|Sad]Oof, I feel sorry for that little dog.[P] Their noses are like, a million times more sensitive than ours, you know.")
fnDialogue("[CHARENTER|AJBackpack]Dog: [FOCUS|AJBackpack]*sniff* *sniff*[P][P][P] Dog decide, was eXTRA correkt.")
fnDialogue("Dog: [FOCUS|AJBackpack]This beingk PERfeckt place.[P][CRASH] For mating![P] Such sweet. pungeant smells--alREADY Doggie is feelimg [ITALICS]primed[ENDITALICS] for make babies.")
fnDialogue("[CHAREXIT|AJBackpack]Player: Oh, look at that.[P] I'm ready to throw up now.")
fnDialogue("Bret: [E|Sad]*sigh*...")
fnDialogue("Player: N-no, wait! I didn't mean--")
fnDialogue("Bret: [E|Sad]I'm sorry guys, I was really excited to come here, but this might have actually been a terrible idea.")
fnDialogue("Bret: [E|Sad]Let's just go back to--")
fnDialogue("[CHARENTER|Stacy]Stacy: [E|Shock][ITALICS]YAMEROOOOOOO[ENDITALICS]!!!!")
fnDialogue("[MUSIC|Daytime]Thought: The lanky redhead plants her hands on her hips and strides in, her boots making a distinct *squish* *squish* *squish* on the forest floor.")
fnDialogue("Thought: I can't believe how relaxed she looks.")
fnDialogue("Stacy: [E|Mad]I REFUSE to hear any more of this nonsense.")
fnDialogue("Stacy: [E|Mad]I mean, COME.[P][CRASH] ON![P][E|Happy] This place is [ITALICS]ripe[ENDITALICS] for investigation.")
fnDialogue("Thought: What a choice of words...")
fnDialogue("Stacy: [E|Mad]We can't call ourselves true paranormal detectives if we get scared off on an actual lead!")
fnDialogue("Mari: [E|Shock]You know what? Yeah.[P][E|Mad][CRASH] DONT GIVE UP!")
fnDialogue("Mari: [E|Blush] This is really important to you, Bret. We said we were gonna do it.")
fnDialogue("Thought: I pull my shirt up over my nose, trying to use the dense fabric as an air filter.[P] It helps the tiniest bit.")
fnDialogue([[ Player: --I agree.[P] This was a great suggestion, \"Bretmeister\"! We're not going anywhere. ]])
fnDialogue("Bret: Guys...")
fnDialogue("Thought: I smile at him from under my shirt and give a thumbs up. [P]There's a still look in his green eyes that makes me feel warm.")
fnDialogue("Michelle: [E|Blush]Okay, enough cheese.[P][MUSIC|Investigation] Let's get this over with. ")

-- |[Loading the Game]|
--If currently loading the game, set up the investigation.
if(PairanormalLevel_IsLoading() == true) then
    
    --Clear checkpoint.
    PairanormalLevel_FlagCheckpoint("InvestigationCola")
    fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"InvestigationCola\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
    
    --Variables.
    local sBGPath = "Root/Images/Backgrounds/All/SodaFactory"
    local sScriptPath = gsRoot .. "../Chapter 3/Gameplay_SceneB/0 - ExamineObject.lua"
    
    --Change the background.
    PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/SodaFactory")
    PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/SodaFactory")

    --Boot investigation scripts.
    Dia_SetProperty("Clear Investigation Characters")
    Dia_SetProperty("Clear Investigation")
    Dia_SetProperty("Start Investigation") 
    Dia_SetProperty("Set Investigation Image", "Root/Images/Backgrounds/All/SodaFactory")
    Dia_SetProperty("Set Investigation Script", gsRoot .. "../Chapter 3/Gameplay_SceneB/0 - ExamineObject.lua")
    Dia_SetProperty("Set Go Home Strings", gsRoot .. "../Chapter 3/Gameplay_SceneD/0 - PatchGoingHome.lua", "Default")
    
    --Talkable Characters.
    Dia_SetProperty("Add Talkable Character", "Mari",     gsRoot .. "../Chapter 3/Gameplay_SceneB/1 - Present Mari.lua")
    Dia_SetProperty("Add Talkable Character", "Stacy",    gsRoot .. "../Chapter 3/Gameplay_SceneB/2 - Present Stacy.lua")
    Dia_SetProperty("Add Talkable Character", "Bret",     gsRoot .. "../Chapter 3/Gameplay_SceneB/3 - Present Bret.lua")
    Dia_SetProperty("Add Talkable Character", "Walkie",   gsRoot .. "../Chapter 3/Gameplay_SceneB/4 - Present Laura.lua")
    Dia_SetProperty("Add Talkable Character", "Michelle", gsRoot .. "../Chapter 3/Gameplay_SceneB/5 - Present Michelle.lua")
    Dia_SetProperty("Add Talkable Character", "AJ",       gsRoot .. "../Chapter 3/Gameplay_SceneB/6 - Present Dog.lua")
    Dia_SetProperty("Add Talkable Character", "Todd",     gsRoot .. "../Chapter 3/Gameplay_SceneB/7 - Present Todd.lua")
    
    --Talkable characters as inventory objects.
    local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
    Dia_SetProperty("Add Discovered Object", "Mari",     sCharPath, sScriptPath, "Mari",     1322,   2, 1322 + 438,   2 + 363)
    Dia_SetProperty("Add Discovered Object", "Stacy",    sCharPath, sScriptPath, "Stacy",     442, 367,  442 + 438, 367 + 363)
    Dia_SetProperty("Add Discovered Object", "Bret",     sCharPath, sScriptPath, "Bret",        2,   2,    2 + 438,   2 + 363)
    Dia_SetProperty("Add Discovered Object", "Laura",    sCharPath, sScriptPath, "Laura",     882,   2,  882 + 438,   2 + 363)
    Dia_SetProperty("Add Discovered Object", "Michelle", sCharPath, sScriptPath, "Michelle", 1762,   2, 1762 + 438,   2 + 363)
    Dia_SetProperty("Add Discovered Object", "Dog",      sCharPath, sScriptPath, "Dog",      1762, 367, 1762 + 438, 367 + 363)
    Dia_SetProperty("Add Discovered Object", "Todd",     sCharPath, sScriptPath, "Todd",        2, 367,    2 + 438, 367 + 363)
    Dia_SetProperty("Clear Discovered Object")
    
    --SLF file adds the examinable objects.
    Dia_SetProperty("Parse SLF File", gsRoot .. "../Chapter 3/MapData/InvestigationAA.slf")
    
    --Check all the items and re-add them to the inventory.
    for i = 1, gciInv3A_Variable_Total-1, 1 do
        
        --Variable name.
        local sVarName = "Root/Variables/Chapter3/InvestigationA/iObject|" .. gczaInv3A_List[i]
        local iHasItem = VM_GetVar(sVarName, "N")
        
        --Has the item. Add it.
        if(iHasItem == 1.0) then
            Dia_SetProperty("Add Discovered Object", gczaInv3A_List[i], sBGPath, sScriptPath, gczaInv3A_List[i], 0, 0, 1, 1)
        end
        Dia_SetProperty("Clear Discovered Object")
    end

    --Modify the time max/left.
    VM_SetVar("Root/Variables/System/Investigation/sInvestigationPath", "S", "Root/Variables/Chapter3/Investigation/")
    local iTimeMax  = VM_GetVar("Root/Variables/Chapter3/Investigation/iTimeMax",  "N")
    local iTimeLeft = VM_GetVar("Root/Variables/Chapter3/Investigation/iTimeLeft", "N")
    Dia_SetProperty("Investigation Time Max",  iTimeMax)
    Dia_SetProperty("Investigation Time Left", iTimeLeft)

-- |[Normal Operations]|
--Otherwise, flag the game to save and boot the investigation normally.
else

    --Save the game.
    fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"InvestigationCola\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

    --Set the time max/left.
    fnCutsceneInstruction([[ VM_SetVar("Root/Variables/Chapter3/Investigation/iTimeMax",  "N", 60.0) ]])
    fnCutsceneInstruction([[ VM_SetVar("Root/Variables/Chapter3/Investigation/iTimeLeft", "N", 60.0) ]])

    --Boot investigation scripts.
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Investigation Characters") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Investigation Time Max", 60) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Investigation Time Left", 60) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Set Investigation Image", "Root/Images/Backgrounds/All/ClubHouse") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Set Investigation Script", gsRoot .. "../Chapter 3/Gameplay_SceneB/0 - ExamineObject.lua") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Set Go Home Strings", gsRoot .. "../Chapter 3/Gameplay_SceneD/7 - PatchGoingHome.lua", "Default") ]])

    --Talkable characters.
    fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Mari",     gsRoot .. "../Chapter 3/Gameplay_SceneB/1 - Present Mari.lua") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Stacy",    gsRoot .. "../Chapter 3/Gameplay_SceneB/2 - Present Stacy.lua") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Bret",     gsRoot .. "../Chapter 3/Gameplay_SceneB/3 - Present Bret.lua") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Walkie",   gsRoot .. "../Chapter 3/Gameplay_SceneB/4 - Present Laura.lua") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Michelle", gsRoot .. "../Chapter 3/Gameplay_SceneB/5 - Present Michelle.lua") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "AJ",       gsRoot .. "../Chapter 3/Gameplay_SceneB/6 - Present Dog.lua") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Todd",     gsRoot .. "../Chapter 3/Gameplay_SceneB/7 - Present Todd.lua") ]])
    
    --SLF file adds the examinable objects.
    fnCutsceneInstruction([[ Dia_SetProperty("Parse SLF File", gsRoot .. "../Chapter 3/MapData/InvestigationAA.slf") ]])

    --Add the talkable characters as the first examine objects.
    fnCutsceneInstruction([[ Dia_SetProperty("Add Discovered Object", "Mari",     "Root/Images/Backgrounds/All/CharPortraits", gsRoot .. "../Chapter 3/Gameplay_SceneB/0 - ExamineObject.lua", "Mari",     1322,   2, 1322 + 438,   2 + 363) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Discovered Object", "Stacy",    "Root/Images/Backgrounds/All/CharPortraits", gsRoot .. "../Chapter 3/Gameplay_SceneB/0 - ExamineObject.lua", "Stacy",     442, 367,  442 + 438, 367 + 363) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Discovered Object", "Bret",     "Root/Images/Backgrounds/All/CharPortraits", gsRoot .. "../Chapter 3/Gameplay_SceneB/0 - ExamineObject.lua", "Bret",        2,   2,    2 + 438,   2 + 363) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Discovered Object", "Laura",    "Root/Images/Backgrounds/All/CharPortraits", gsRoot .. "../Chapter 3/Gameplay_SceneB/0 - ExamineObject.lua", "Laura",     882,   2,  882 + 438,   2 + 363) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Discovered Object", "Michelle", "Root/Images/Backgrounds/All/CharPortraits", gsRoot .. "../Chapter 3/Gameplay_SceneB/0 - ExamineObject.lua", "Michelle", 1762,   2, 1762 + 438,   2 + 363) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Discovered Object", "Dog",      "Root/Images/Backgrounds/All/CharPortraits", gsRoot .. "../Chapter 3/Gameplay_SceneB/0 - ExamineObject.lua", "Dog",      1762, 367, 1762 + 438, 367 + 363) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Discovered Object", "Todd",     "Root/Images/Backgrounds/All/CharPortraits", gsRoot .. "../Chapter 3/Gameplay_SceneB/0 - ExamineObject.lua", "Todd",        2, 367,    2 + 438, 367 + 363) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])

    fnCutsceneBlocker()
    
    --Time variable.
    VM_SetVar("Root/Variables/System/Investigation/sInvestigationPath", "S", "Root/Variables/Chapter3/Investigation/")
    
end
    