--[Found Special Thing]
--This plays if the special event sequence at either location played out.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--[Execution]
--Well that happened.
if(sTopicName == "SpecialStart") then
    
	--Save the game.
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"WellThatHappened\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
	PairanormalLevel_FlagCheckpoint("WellThatHappened")
    
    fnDialogue("[BG|Null][MUSIC|Null]Thought: ...")
    fnDialogue("Info: Plap, plap, plap.")
    fnDialogue("[BG|Root/Images/Backgrounds/All/SchoolExterior][MUSIC|Theme]Thought: I listen to the sound of our sneakers hitting the pavement as we round back to the school's front gate.[P] For the past few minutes, its been the only sound anyone has made.")
    fnDialogue("[CHARENTER|Bret]Bret: [E|Sad]... Huh")
    fnDialogue("[CHARENTER|Todd]Todd: [E|Mad]...[ITALICS] Yo[ENDITALICS].")
    fnDialogue("[CHARENTER|Mari]Mari: [E|Sad]Y-[P]yeah.")
    fnDialogue("Thought: I'm having a hard time taking it all in, myself.")
    fnDialogue("[CHARENTER|Stacy]Stacy: [E|Shock]Whew![P] Heh...[P][E|Sad]hm.")
    fnDialogue("[CHARENTER|Michelle]Michelle: [E|Mad]Tsk.")
    fnDialogue("Thought: ...")
    fnDialogue("Mari: [E|Sad]Okay.[P] Okay, okay![P][E|N] It's like this, right?")
	fnDialogue("Mari: [E|Sad]W-we've been looking for an actual mystery our WHOOOOLE life,[P] right?")
    fnDialogue("Thought: We all stop.")
    fnDialogue("Bret: [E|Shock]I mean it's really only just been the past three years--")
    fnDialogue("Mari: [E|Shock]And now we've actually found [CRASH]REAL, TANGIBLE proof that [ITALICS]something[ENDITALICS] is going on in this town.")
    fnDialogue("Mari: [E|Sad] And like, um...[P][P][E|Mad][CRASH] I FEEL LIKE I DON'T KNOW WHAT THE HECK WE SHOULD DO!?!?")
    fnDialogue("Michelle: [E|Happy]There's no way we're the only ones involved in all this.[P][E|Mad] It probably goes all the way to the top!")
    fnDialogue("[CHARENTER|AJ]Dog: Next time; Dog recommend.[P] Everyone travel in bag aswell too.[P] This will protecct itendity.")
    fnDialogue("[CHAREXIT|AJ]Bret: [E|Mad][SPEEDTEXT]What if they know that [E|Sad]no one should know [E|N]but now we know and [E|Shock]they know that we know???[ENDSPEEDTEXT] ")
    fnDialogue("Stacy: [E|Shock]What if someone sends agents to HUSH US UP!?")
    fnDialogue("Todd: [E|Sad]Of all days to slack off on ASB responsibilities...")
    fnDialogue("Michelle: [E|Mad][CRASH]We're bloody fucked!")
    fnDialogue("Thought: The club murmurs in agreement, but it all ends in a long silence.[P] Nobody offers an answer.")
    fnDialogue("Player: ...[P] Um.")
    fnDialogue("Thought: The club is startled.[P] I'm as surprised as anyone else that I have something to say, but I continue.")
    fnDialogue("Player: Well, I was just thinking...")
    fnDialogue("Player: The first time I joined this club, um... all the stuff you guys were talking about.")
    fnDialogue("Player: Like, ghosts and demons and stuff.[P] It sounded really scary.[P][SFX|Ugh] I was actually terrified of ever encountering any of those things.")
    fnDialogue("Thought: Mari's eyebrows crease with concern.[P] Guiltily, I hurry on to my next point.")
    fnDialogue("Player: --But the more time I spent with everyone, the more you guys made it seem like it was...[P] actually kind of fun?")
    fnDialogue("Player: Like, no matter how creepy or weird things got, as long as we all stuck together,[P] we'd have a good time.")
    fnDialogue("Player: And honestly, I don't see why things should be any different now.")
    fnDialogue("Player: We should keep doing whatever we want, and... just try our best? Or something?")
    fnDialogue("Stacy: [E|Shock]... Whoah, [PLAYERNAME].")
    fnDialogue("Mari: [E|Sad]That's... [P][P][CRASH][E|Blush]THAT'S REALLY SWEET!")
    fnDialogue("Michelle: [E|Happy]I can't believe I'm saying it, but the weirdo is right.[P] FUCK anyone who tries to mess with us.")
    fnDialogue("Bret: [E|Mad]Yeah, fu-- [E|Blush]er, screw them!")
    fnDialogue("Stacy: [E|Mad]I don't know about you guys, but I think we need to HUG IT OUT.")
    fnDialogue("Todd: [E|Shock]Wait, do I have to do this?[E|Mad] I feel like I'm not really part of---")
    fnDialogue("Mari: [E|Mad][CRASH]SHUT UP AND GET IN HERE, TODD.")
    fnDialogue("[HIDEALL]Thought: Before I know what's going on, everyone is clumped together, arms squeezing each other tightly.[P] Maybe a little too tightly.")
    fnDialogue("Thought: ...")
    fnDialogue("[CHARENTER|Mari][CHARENTER|Stacy][CHARENTER|Bret][CHARENTER|Michelle][CHARENTER|Todd]Player: Well, That was fun... ish. I don't know about you guys, but I should be getting home.")
    fnDialogue("[CHARENTER|AJ]Dog: [EMOTION|AJ|Happy]Emxcellent. [P]There has been a grumbly in my tommies. I am MOST HUNGRY and ready to eat;[P][P] The crunchies at home.")
    fnDialogue("Mari: [E|N]Yeah, it's about that time, isn't it?[P][E|Happy] Okay darling investigators, let's call it a day!!!")
	fnDialogue("[CHAREXIT|ALL]Player: Bye everyone...")
	fnDialogue("Thought: The moment everyone starts shuffling off, all the enthusiasm I had to keep going and stay strong starts to peel away with them.")
	fnDialogue("Thought: The hairs on the back of my neck raise. I mean... what if everyone is right, and we're in real danger?[P] What if--")
	fnDialogue("[CHARENTER|Stacy]Stacy: [PLAYERNAME], wait!")
	fnDialogue("Player: [SFX|World|Blink]Stacy! What's up?")
    fnDialogue("Stacy: [P][P][P][E|Mad]... Can I pet your dog one last time?")
	fnDialogue("Player: H-[P][SFX|World|Ugh]Hahaha... sure.")
    fnDialogue("Thought: That does it. Before everyone heads off for the day, I should see if one of them wants to walk home with me.")
    
    --What do?
    fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (Who should I walk home with?)") ]])

    local sDecisionScript = "\"" .. gsRoot .. "../Chapter 3/Gameplay_SceneD/1a - Go Home Alone.lua\""
    fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Go Home Alone\",    " .. sDecisionScript .. ", \"Continue\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Bret\",     " .. sDecisionScript .. ", \"Bret\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Laura\",    " .. sDecisionScript .. ", \"Laura\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Mari\",     " .. sDecisionScript .. ", \"Mari\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Stacy\",    " .. sDecisionScript .. ", \"Stacy\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Michelle\", " .. sDecisionScript .. ", \"Michelle\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Todd\",     " .. sDecisionScript .. ", \"Todd\") ")
    fnCutsceneBlocker()

--Did not find the special thing.
elseif(sTopicName == "NormalStart") then

	--Save the game.
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Normal\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
	PairanormalLevel_FlagCheckpoint("Normal")
    
    fnDialogue("[BG|Null][MUSIC|Null]Thought: ...")
    fnDialogue("[BG|Root/Images/Backgrounds/All/SchoolExterior][MUSIC|Theme][CHARENTER|Mari][CHARENTER|Stacy][CHARENTER|Bret][CHARENTER|Michelle][CHARENTER|Todd]Player: Well, That was fun... ish. I don't know about you guys, but I should be getting home.")
    fnDialogue("[CHARENTER|AJ]Dog: Emxcellent. [P]There has been a grumbly in my tommies. I am MOST HUNGRY and ready to eat; The crunchies at home.")
    fnDialogue("Mari: [E|N]Yeah, it's about that time, isn't it?[P][E|Happy] Okay darling investigators, let's call it a day!!!")
    fnDialogue("Stacy: [PLAYERNAME][P][P][P][E|Mad]... Can I pet your dog one last time?")
	fnDialogue("Player: H-[P][SFX|World|Ugh]Hahaha... sure.")
    fnDialogue("Thought: Before everyone heads off for the day, I should see if one of them wants to walk home with me.")
    
    --What do?
    fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (Who should I walk home with?)") ]])

    local sDecisionScript = "\"" .. gsRoot .. "../Chapter 3/Gameplay_SceneD/1a - Go Home Alone.lua\""
    fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Go Home Alone\", " .. sDecisionScript .. ", \"Continue\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Bret\",     " .. sDecisionScript .. ", \"Bret\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Laura\",    " .. sDecisionScript .. ", \"Laura\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Mari\",     " .. sDecisionScript .. ", \"Mari\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Stacy\",    " .. sDecisionScript .. ", \"Stacy\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Michelle\", " .. sDecisionScript .. ", \"Michelle\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Todd\",     " .. sDecisionScript .. ", \"Todd\") ")
    fnCutsceneBlocker()


end