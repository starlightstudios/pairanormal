-- |[ ===================================== Go Home Handler ==================================== ]|
-- |[Arg Resolve]|
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--Set this variable.
VM_SetVar("Root/Variables/System/Player/iChapterDestination", "N", 3.0)

-- |[Skip]|
if(sTopicName == "Continue") then
    LM_ExecuteScript(gsRoot .. "../Chapter 3/Gameplay_SceneD/2 - Arrive At Home.lua", "Start")

-- |[Bret]|
elseif(sTopicName == "Bret") then
    LM_ExecuteScript(gsRoot .. "../Chapter Walk/Walkhome Bret 0.lua", "Start")

-- |[Laura]|
elseif(sTopicName == "Laura") then
    LM_ExecuteScript(gsRoot .. "../Chapter Walk/Walkhome Laura 0.lua", "Start")

-- |[Michelle]|
elseif(sTopicName == "Michelle") then
    LM_ExecuteScript(gsRoot .. "../Chapter Walk/Walkhome Michelle 0.lua", "Start")

-- |[Mari]|
elseif(sTopicName == "Mari") then
    LM_ExecuteScript(gsRoot .. "../Chapter Walk/Walkhome Mari 0.lua", "Start")

-- |[Stacy]|
elseif(sTopicName == "Stacy") then
    LM_ExecuteScript(gsRoot .. "../Chapter Walk/Walkhome Stacy 0.lua", "Start")

-- |[Todd]|
elseif(sTopicName == "Todd") then
    LM_ExecuteScript(gsRoot .. "../Chapter Walk/Walkhome Todd 0.lua", "Start")
    
end

