--[Stacy Presentation]
--Showing stuff to Stacy.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--[Chat Sequences]
--Chatting.
if(sTopicName == "Chat") then
	
	--Variables.
	local iChattedWithStacy = VM_GetVar("Root/Variables/Chapter1/Investigation/iChattedWithStacy", "N")
	
	--First pass:
	if(iChattedWithStacy == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Investigation/iChattedWithStacy", "N", 1.0)
		
		--Text.
		fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
		fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
		fnCutsceneBlocker()
		fnDialogue([[[HIDEALLFAST][SHOWCHAR|Stacy|Neutral]Stacy: [E|Neutral][ITALICS]Guten-tag ![ENDITALICS][P] If it isn't [PLAYERNAME]!]])
		fnDialogue([[Player: Hi, Stacy.]])
		fnDialogue([[Stacy: [E|Happy]Hey, have you tried these? I made them.]])
		fnDialogue([[Thought: Stacy reaches for one of the grilled cheese sandwiches on a nearby platter.]])
		fnDialogue([[Thought: She waves it under her pointed nose, wafting its pungeant flavor before taking a bite.]])
		fnDialogue([[Stacy: [E|Shock]*munch*[P] *munch*[P] Muuk shuur yoo toolk too uuveryone b'furr yoo goo humm. ]])
		fnDialogue([[Player: ... what?]])
		fnDialogue([[Stacy: [E|Neutral]*gulp* Make sure you talk to everyone!]])
		fnDialogue([[Stacy: After all,[P][E|Happy] you never know when a crucial piece of information might help you later on down the line.]])
		fnDialogue([[Thought: I see a mischievous glint in her eye.[P] That sounds suspiciously more like a warning than friendly advice. ]])
		fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
		fnCutsceneBlocker()
		
	--Repeat passes.
	else
		fnStdInvText([[[HIDEALLFAST][SHOWCHAR|Stacy|Neutral]Stacy: [ITALICS]Bonjour[ENDITALICS]! What's cooking, good lookin'?]])
	end

--Desk.
elseif(sTopicName == "Desk") then
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnDialogue([[[HIDEALLFAST][SHOWCHAR|Stacy|Neutral]Stacy: [E|Mad]Highschool.[P] desks.[P] are.[P] the.[P] worst.[P] [E|Happy]Do you honestly think this Adonis-like frame could ever fit comfortably in those gonvernment-approved torture devices? ]])
	fnDialogue([[Thought: She lifts up her shirt, revealing a slender stomach strapped with firm, wiry muscle.]])
	fnDialogue([[Player: I.[P].[P].[P] don't know how to respond.]])
	fnDialogue([[Stacy: [E|Blush]Yeah, I have that effect on people.]])
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()
	
--Grafitti.
elseif(sTopicName == "Grafitti") then
	fnStdInvText([[[HIDEALLFAST][SHOWCHAR|Stacy|Neutral]Stacy: [E|Neutral]Yeah, I couldn't draw my way out of a paper bag if I tried.[P][E|Mad] It's one of my [ITALICS]few[ENDITALICS] flaws.]])

--Whiteboard.
elseif(sTopicName == "WhiteBoard") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Stacy|Neutral]Stacy: [E|Neutral]A whiteboard![P] [E|Happy]Or, [ITALICS]un pizzaron[ENDITALICS], in Spanish.")

--ReadPoster.
elseif(sTopicName == "ReadPoster") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Stacy|Neutral]Stacy: [E|Neutral]I love reading; especially audiobooks!")

--EarthPoster.
elseif(sTopicName == "EarthPoster") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Stacy|Neutral]Stacy: [E|Neutral]Recycling is cool and all, but I think our efforts are better spent holding corporations accountable for the waste generated from product manufacturing.")

--Window.
elseif(sTopicName == "Window") then
	fnStdInvText([[[HIDEALLFAST][SHOWCHAR|Stacy|Neutral]Stacy: [E|Mad]\"Surely, said I, surely that is something at my window lattice.[P][E|Neutral] Let me see, then, what thereat is, and this mystery explore!\"]])

--Computer.
elseif(sTopicName == "Computer") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Stacy|Neutral]Stacy: [E|Neutral]Once the new computer lab is built, I bet Laura's gonna spend a lot of time there.")

--Apple.
elseif(sTopicName == "Apple") then
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnDialogue([[[HIDEALLFAST][SHOWCHAR|Stacy|Neutral]Stacy: [E|Mad]Ooh, darn! I should have put some apples in my grilled cheese sandwiches!]])
	fnDialogue([[Player: Aren't apples a fruit?[P] That seems like a weird combo.]])
	fnDialogue([[Stacy: [E|Neutral]Well, it's theorized that the subtle sweetness of apples helps bring out the [ITALICS]umami[ENDITALICS],[P] the savoriness of many salty dishes. ]])
	fnDialogue([[Thought: When Stacy talks about food, I...[P]W-why is my face getting all hot?]])
	fnDialogue([[Stacy: [E|Neutral]If you like, I can...[P][E|Blush]prepare a private charcuterie plate for you sometime.[P] See if you fancy any of the flavors.]])
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()

--Sandwiches.
elseif(sTopicName == "Sandwiches") then
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnDialogue([[[HIDEALLFAST][SHOWCHAR|Stacy|Neutral]Stacy: [E|Shock]Aha! [P][E|Happy]I see you've spotted my latest dish; Stacy's delicious grilled cheese sandwiches! ]])
	fnDialogue([[Stacy: [E|Neutral]Today I used a delicious Mexican [ITALICS]Oaxaquena[ENDITALICS] Cheese.[P] It's very similar to Mozzarella, but super stringy! And it has a sweet, milky smell.]])
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()

--Pills.
elseif(sTopicName == "Pills") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Stacy|Neutral]Stacy: [E|Shock]Whoah, uh... Those are Mari's pills. [P][E|Sad]Do me a fave, leave those alone; Mari really doesn't like it when you touch them.")

--Newspaper.
elseif(sTopicName == "Newspaper") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Stacy|Neutral]Stacy: [E|Neutral]I don't read the newspaper much, unless we have to for a school project or something.")

--Clipboard.
elseif(sTopicName == "Clipboard") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Stacy|Neutral]Stacy: [E|Neutral]A clipboard! or [ITALICS]presse-papiers[ENDITALICS] as the French say.")

--Person: Mari.
elseif(sTopicName == "Mari") then
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnDialogue([[[HIDEALLFAST][SHOWCHAR|Stacy|Neutral]Stacy: [E|Happy]Mari's my best friend![P][E|Neutral] As you can imagine, we spend a lot of time together;[P] I can't tell you how many times people thought we're dating. ]])
	fnDialogue([[Stacy: [E|Blush]I mean, sure,[P] I've cupped her boobs,[P] but it was strictly platonic.]])
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()

--Person: Stacy.
elseif(sTopicName == "Stacy") then
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnDialogue([[[HIDEALLFAST][SHOWCHAR|Stacy|Neutral]Thought: Stacy stretches, inadvertently showing off the limber, muscular curve of her back.]])
	fnDialogue([[Stacy: [E|Neutral]Ahhh, yeah.[P] Club president, intellectual, smokin' hot babe.[P] No big deal.]])
	fnDialogue([[Player: Don't forget part time model.]])
	fnDialogue([[Thought: She flashes me a mischievous smile.]])
	fnDialogue([[Stacy: [E|Happy]Hah![P][E|Blush] we're gonna get along great, [PLAYERNAME].]])
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()

--Person: Bret.
elseif(sTopicName == "Bret") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Stacy|Neutral]Stacy: [E|Neutral]Bret joined our club, like, a few months after we got established.[P] He loves Paranormal stuff, and he's wicked smart.[BR] Heh,[P] nerd.")

--Person: Michelle.
elseif(sTopicName == "Michelle") then
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnDialogue([[[HIDEALLFAST][SHOWCHAR|Stacy|Neutral]Stacy: [E|Neutral]Is it weird that I've always wanted to spar with Michelle? ]])
	fnDialogue([[Stacy: [E|Blush]Just picture it; two smokin' hot punks,[P] bashing each other to bits,[P] the sunlight cascading off of our ripped leather jackets... ]])
	fnDialogue([[Stacy: [E|Mad]The battle would be glorious.]])
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()

--Person: Laura.
elseif(sTopicName == "Laura") then
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnDialogue([[[HIDEALLFAST][SHOWCHAR|Stacy|Neutral]Stacy: [E|Neutral]Whenever we're out exploring, Laura's holding down the fort at the school library.]])
	fnDialogue([[Stacy: She looks up references,[P] writes on our blog,[P] and keeps our group texts under control.]])
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()

end

