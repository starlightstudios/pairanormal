--[Laura Presentation]
--Showing stuff to Laura.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--[Chat Sequences]
--Chatting.
if(sTopicName == "Chat") then
	
	--Variables.
	local iChattedWithLaura = VM_GetVar("Root/Variables/Chapter1/Investigation/iChattedWithLaura", "N")
	
	--First pass:
	if(iChattedWithLaura == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Investigation/iChattedWithLaura", "N", 1.0)
		
		--Text.
		fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
		fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
		fnCutsceneBlocker()
		fnDialogue([[[HIDEALLFAST][SHOWCHAR|Laura|Neutral]Thought: Laura seems pleased to see me approach, although as soon as I get near, her gaze drops bashfully to the floor.]])
		fnDialogue([[Laura: [E|Blush]Hello![P]*ahem* Nice to meet you, I'm Laura!]])
		fnDialogue([[Thought: I'm still a few lengths away from her, and I hop forward the last couple steps.[P] Laura nearly crouches, startled.]])
		fnDialogue([[Player: Erm, we've already been introduced.[P] I'm [PLAYERNAME].]])
		fnDialogue([[Thought: She straightens up, slightly flushed, and we stand there looking at each other for a few awkward moments.]])
		fnDialogue([[Thought: It seems that she hadn't forgotten my name, but that she didn't know what else to say.]])
		fnDialogue([[Player: So.[P].[P]. [P]Paranormal Club!]])
		fnDialogue([[Laura: [E|Neutral]Yep![P] Pretty cool, right?]])
		fnDialogue([[Player: I guess so, yeah.]])
		fnDialogue([[Laura: Hm.]])
		fnDialogue([[Player: [SFX|World|Ugh][P].[P].[P].]])
		fnDialogue([[Laura: [SFX|World|Ugh][P].[P].[P].]])
		fnDialogue([[Player: So-]])
		fnDialogue([[Laura: Yes?]])
		fnDialogue([[Player: What exactly...[P] [ITALICS]is[ENDITALICS] the Paranormal Club? Like, what do you guys do here?]])
		fnDialogue([[Thought: Laura chuckles, tugging at the hem of her skirt.]])
		fnDialogue([[Laura: [E|Neutral]If we're not hanging out at our clubhouse, *ahem* we're usually out going on missions!]])
		fnDialogue([[Laura: [E|Happy] I mean, everyone else does, not me.[P] I'm more of an indoorsy type.]])
		fnDialogue([[Player: Club missions?]])
		fnDialogue([[Laura: [E|Neutral]When the club wants to investigate something mysterious in town, they go on a \"mission\"; a group expedition to[P] *ahem*[P] explore an area of interest.]])
		fnDialogue([[Laura: I'm sure you'll get to go along on the very next one.[P][E|Happy] Everyone goes together.]])
		fnDialogue([[Player: Oh, cool!]])
		fnDialogue([[Laura: Yep!]])
		fnDialogue([[Player: Hm.]])
		fnDialogue([[Laura: [SFX|World|Ugh][P].[P].[P].]])
		fnDialogue([[Player: [SFX|World|Ugh][P].[P].[P].]])
		fnDialogue([[Laura: [E|Blush]Em...[P] er... s-[P]so![P] How did you get roped into our little club?]])
		fnDialogue([[Laura: [E|Neutral]I'm guessing *hrmph* since you don't know anything about the Paranormal Investigation aspect, [P][E|Happy]Mari or Stacy talked you into it somehow.]])
		fnDialogue([[Player: Yes, actually! Well, we met in the hospital [GLITCHTEXT]A FEW MONTHS AGO[ENDGLITCHTEXT][GLITCH]a few months ago.]])
		fnDialogue([[Thought: I wince but smile. Trying to remember things... hurts. ]])
		fnDialogue([[Player: ... And...[P] after they saw me in school today, they asked me to come by.[P] But anyway, what about you? How did you meet them?]])
		fnDialogue([[Thought: Laura appears not to notice my discomfort, continuing to darn her skirt.]])
		fnDialogue([[Laura: [E|Neutral]Oh, w-[P]we've been friends since forever.[P] Probably 4th or 5th grade, I think.]])
		fnDialogue([[Laura: [E|Shock] Mari and Stacy formed the club the first year in high school, and I signed up right away! The rest is history.]])
		fnDialogue([[Player: I see!]])
		fnDialogue([[Laura: [E|Neutral]Yep!]])
		fnDialogue([[Player: Hm.]])
		fnDialogue([[Laura: [SFX|World|Ugh][P].[P].[P]..]])
		fnDialogue([[Player: [SFX|World|Ugh][P].[P].[P].]])
		fnDialogue([[Thought: For the first time, Laura looks me in the eye, although with some difficulty.]])
		fnDialogue([[Laura: [E|Sad]I'm sorry, *ahem* I'm really awful at talking to people.[P] I don't really know what to say, or where to jump in--]])
		fnDialogue([[Player: [SFX|World|Crash]Me too!]])
		fnDialogue([[Thought: She giggles, tucking a strand of cyan hair behind her ear.]])
		fnDialogue([[Laura: [E|Happy]Hopefully we have more in common than blunderous communication skills.[P][E|Blush] I'm s-sure... over the course of the year...[P] we'll manage to get[P] *ahem*[P] to know each other better!!!]])
		fnDialogue([[Player: Yep.]])
		fnDialogue([[Laura: Hm...]])
		fnDialogue([[Player: [SFX|World|Ugh][P].[P].[P].]])
		fnDialogue([[Laura: [E|Neutral]A-[P]Anyway, I won't keep you!!![P] I'm glad you're part of the Club, [PLAYERNAME]!]])
		fnDialogue([[Player: Thanks, Laura.]])
		fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
		fnCutsceneBlocker()
		
	--Repeat passes.
	else
		fnStdInvText("[HIDEALLFAST][SHOWCHAR|Laura|Neutral]Laura: Hello again, [PLAYERNAME]!")
	end
	
--Desk.
elseif(sTopicName == "Desk") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Laura|Neutral]Laura: [E|Mad]Urgh, I hate high school desks.[P] They're so icky and cramped!")
	
--Grafitti.
elseif(sTopicName == "Grafitti") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Laura|Neutral]Laura: [E|Happy]Some classes can be *ahem* kind of a snooze fest.[P][E|Shock] If I didn't have something to fidget with, I'd probably be carving up desks, too.")

--Whiteboard.
elseif(sTopicName == "WhiteBoard") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Laura|Neutral]Laura: [E|Neutral][SFX|World|Ugh]...???")

--ReadPoster.
elseif(sTopicName == "ReadPoster") then
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnDialogue([[[HIDEALLFAST][SHOWCHAR|Laura|Neutral]Laura: [E|Happy]I do most of my reading on a screen! ]])
	fnDialogue([[Laura: [E|Neutral]But I probably read t-[P]twice as much as a stuffy old adult who says I should get my nose off of the computer and [P]*ahem* [P]into a book... hehehe.]])
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()

--EarthPoster.
elseif(sTopicName == "EarthPoster") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Laura|Neutral]Laura: [E|Neutral][SFX|World|Ugh]...???")

--Window.
elseif(sTopicName == "Window") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Laura|Neutral]Laura: [E|Neutral]I-I dont go outside too much, but I love sunlight.")

--Computer.
elseif(sTopicName == "Computer") then
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnDialogue([[[HIDEALLFAST][SHOWCHAR|Laura|Neutral]Laura: [E|Neutral]Did you hear they're building a new computer lab this year?[P][E|Blush] High end rigs,[P] ginormous towers,[P] premium software,[P] and blazing fast school-wide wi-fi to boot!]])
	fnDialogue([[Laura: [E|Blush] Heheh... I'll give you three guesses where I'm gonna be spending all my free time.]])
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()
	
--Apple.
elseif(sTopicName == "Apple") then
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnDialogue([[[HIDEALLFAST][SHOWCHAR|Laura|Neutral]Laura: [E|Neutral]It's fake!]])
	fnDialogue([[Player: Tell me about it!]])
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()

--Sandwiches.
elseif(sTopicName == "Sandwiches") then
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnDialogue([[[HIDEALLFAST][SHOWCHAR|Laura|Neutral]Laura: [E|Shock]For all our club's positives, the eclectic food selection rarely makes the list.]])
	fnDialogue([[Laura: [E|Neutral] B-But hey,[P] nothing a delivery pizza can't solve.]])
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()

--Pills.
elseif(sTopicName == "Pills") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Laura|Neutral]Laura: [E|Mad]T-[P]Those are Mari's![P][E|Shock] e-[P]Em...[P]I -really- wouldn't touch them if I were you.")

--Newspaper.
elseif(sTopicName == "Newspaper") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Laura|Neutral]Laura: [E|Sad]Bummer about Mr. Foster, eh?[P] It was his education grant that paid for our new computer lab, you know.")

--Clipboard.
elseif(sTopicName == "Clipboard") then
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnDialogue([[[HIDEALLFAST][SHOWCHAR|Laura|Neutral]Laura: [E|Neutral]Clipboards are kind of cool,[P] don't you think?[P] If you hold one in your hand, it *ahem* gives you this strange, authoritative power.]])
	fnDialogue([[Laura: Probably because if you're using one...[P]everyone thinks you must be very organized.]])
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()

--Person: Mari.
elseif(sTopicName == "Mari") then
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnDialogue([[[HIDEALLFAST][SHOWCHAR|Laura|Neutral]Laura: [E|Neutral]Mari is an awesome club president.[P] She bought me a walkie talkie so I c-[P]could communicate with the club while they're out on missions.]])
	fnDialogue([[Thought: She giggles, pointing to her pocket.]])
	fnDialogue([[Laura: [E|Happy]But a short while later, my parents gave me a cellphone.[P] So now everyone just texts.]])
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()

--Person: Stacy.
elseif(sTopicName == "Stacy") then
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnDialogue([[[HIDEALLFAST][SHOWCHAR|Laura|Neutral]Laura: [E|Neutral]If you're going on a dangerous mission, make sure to bring Stacy along![P] Did you know she can bench press,[P] like,[P] 50 pounds?]])
	fnDialogue([[Laura: [SFX|World|Ugh][E|Blush] It's probably thanks to her the whole school doesn't[P] *hrmph*[P] think this club is a bunch of dweebs.]])
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()
	
--Person: Bret.
elseif(sTopicName == "Bret") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Laura|Neutral]Laura: [E|Happy]Bret is absolutely hilarious![P][E|Neutral] But, he's fairly extroverted compared to me...[P] You can imagine our conversations are often one sided.")

--Person: Michelle.
elseif(sTopicName == "Michelle") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Laura|Neutral]Laura: [E|Neutral]D-[P]don't be intimidated by Michelle, she is actually super sweet.[P] And quite brave![P][E|Blush] She'll always have your back if you need her.")

--Person: Laura.
elseif(sTopicName == "Laura") then
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnDialogue([[[HIDEALLFAST][SHOWCHAR|Laura|Neutral]Laura: [E|Neutral]I'm sort of the \"mission control\" around here.]])
	fnDialogue([[Laura: [E|Happy]I keep everyone connected, a-[P]and if the club needs me to look something up for them, I'm never far from a book or a search engine in the Library.]])
	fnDialogue([[Laura: [E|Blush]Em... I also keep a record of all the club's adventures on our blog!]])
	fnDialogue([[Thought: She smiles, shrugging her shoulders all the way up to her pink ears.]])
	fnDialogue([[Laura: [E|Neutral]I'd love to be a journalist someday.]])
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()
	
end

