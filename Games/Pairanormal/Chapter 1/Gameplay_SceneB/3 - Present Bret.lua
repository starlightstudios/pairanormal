--[Bret Presentation]
--Showing stuff to Bret.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--[Chat Sequences]
--Chatting.
if(sTopicName == "Chat") then
	
	--Variables.
	local iChattedWithBret = VM_GetVar("Root/Variables/Chapter1/Investigation/iChattedWithBret", "N")
	
	--First pass:
	if(iChattedWithBret == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Investigation/iChattedWithBret", "N", 1.0)
		
		--Text.
		fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
		fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
		fnCutsceneBlocker()
		fnDialogue([[[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Thought: Bret smiles as I approach.]])
		fnDialogue([[Bret: [E|Neutral]Hey there, [PLAYERNAME]! Welcome to the Pairanormal Club! How ya doin'?]])
		fnDialogue([[Player: I'm doing good.]])
		fnDialogue([[Bret: [E|Happy]You look good! ]])
		fnDialogue([[Player:...]])
		fnDialogue([[Bret: [E|Shock][SFX|World|Ugh]I mean, [E|Blush]I said that you look good because [E|Mad]you said that you're doing good. [E|Neutral]Not because I think you're cu---]])
		fnDialogue([[Bret: [E|Blush]I mean, not to say that I think you look ugly.[P][E|Mad] I am totally--[E|Blush]you look as good as you think you look--]])
		fnDialogue([[Thought: Bret looks like he's about to collapse.[P] I can't stop staring at his mouth, for some reason.]])
		fnDialogue([[Player: How do you do that?]])
		fnDialogue([[Bret: [E|Shock]Huh? What?]])
		fnDialogue([[Player: You know...[P] talk.[P] So easily.]])
		fnDialogue([[Thought: Bret turns bright red, nervously scratching the back of his neck.]])
		fnDialogue([[Bret: [E|Sad]Oh, yeah.[P] Sorry.[P] I've been told I tend to... ramble on sometimes.]])
		fnDialogue([[Player: Well no, I didn't mean -- I just don't know how to talk to people.[P] You seem so confident.]])
		fnDialogue([[Thought: Bret looks pleasantly surprised.]])
		fnDialogue([[Bret: [E|Neutral]Uh... huh.[P][E|Happy] Haha![P][E|Blush] Gosh, nobody's ever said my rambling was a net positive.[P] ]])
		fnDialogue([[Bret: [E|Neutral] I guess the secret is when I get nervous, my filter turns off and I just... [E|Mad]talk![P][E|Happy] Anything to fill up the unbearable awkward silence between two strangers.]])
		fnDialogue([[Player: Oh.]])
		fnDialogue([[Bret: [E|Happy]Yep.]])
		fnDialogue([[Player: [P].[P].[P].]])
		fnDialogue([[Bret: [P].[P].[P].]])
		fnDialogue([[Player: [P].[P].[P].?]])
		fnDialogue([[Bret: [E|Blush]--But if you have trouble speaking, my tip to you is just letting other people talk, you know?]])
		fnDialogue([[Bret: [E|Neutral] At a party, all you have to do is say [E|Mad]\"what's your deal\"?[E|Neutral] And watch the other guy tell you his life story, and then [E|Shock]voila! You're the bestest of pals.]])
		fnDialogue([[Player: Ah, so...[P] what's your deal, Bret?]])
		fnDialogue([[Thought: Bret stands up straight.]])
		fnDialogue([[Bret: [E|Happy]I'm Bret, the Paranormal Club's... thing expert, I guess![P][E|Blush] Talk to me about things... yeah. ]])
		fnDialogue([[Player: [P][P][P]What?]])
		fnDialogue([[Bret: [E|Happy]You know, stuff! [E|Neutral]Items. [E|Mad]Objects. [E|Neutral]I know a lotta stuff about stuff. Don't ask me why--I don't really know how I have all this knowledge.]])
		fnDialogue([[Bret: [E|Happy]Yes I do.[E|Sad] I'm alone and spend a lot of time on the internet.]])
		fnDialogue([[Player: ... Right.[P] I still don't really know what any of that means.]])
		fnDialogue([[Thought: After a pause, Bret pushes his glasses up on his nose,[P] taking a moment to look you up and down.]])
		fnDialogue([[Bret: [E|Neutral]Okay.[P] Well... [E|Sad]this is just a guess, but I'm thinking you're not currently staying with anyone related to you?]])
		fnDialogue([[Bret: [E|Shock] And you're definitely not staying with a foster family.]])
		fnDialogue([[Thought: I conjure up Eilette, my current \"caretaker\", in my mind.[P] As far as I know, she checks both boxes.]])
		fnDialogue([[Player: W-What makes you think that?]])
		fnDialogue([[Bret: [E|Sad]Well your outfit is brand new, so I couldn't tell too much from that.[P][E|Mad] But your backpack, pencils, and shoes are also brand new...]])
		fnDialogue([[Bret: [E|Neutral]If you were living with relatives, [E|Shock]they might give you a few hand-me downs or something to borrow.]])
		fnDialogue([[Thought: My fists clench.[P] I don't really like being reminded of just how alone I am.]])
		fnDialogue([[Bret: [E|Mad]--But your stuff is also high end.[P][E|Happy] I mean, those are some pretty expensive sneaks![E|Neutral] I saw them displayed on a rack at the mall a couple weeks ago.]])
		fnDialogue([[Bret: [E|Blush]Yep, that's right. I go to the mall.[E|Mad] Love the mall, can't get enough.[E|Neutral] I'm pretty much the embodiment of manliness...]])
		fnDialogue([[Bret: In case you couldn't already tell by my [SPEEDTEXT][E|Sad]gangly arms, [E|Ehock]stooped posture, and [E|blush]extensive collection of Legend of Zelda figurines...[ENDSPEEDTEXT] ]])
		fnDialogue([[Player: Uh, Bret.]])
		fnDialogue([[Bret: [E|shock]Mm?[P] Oh, right.[P][E|Neutral] Expensive school supplies definitely leaves out government involvement, if you know what I mean.]])
		fnDialogue([[Bret: [E|Sad]In case the 25-year-old gym equipment we have wasn't enough indication, [E|Happy]those guys don't like spending any more money than they have to.]])
		fnDialogue([[Thought: He takes a step back.]])
		fnDialogue([[Bret: [E|Neutral]Whoever is taking care of you is spending a lot of money, even though they're not related to you in any way.]])
		fnDialogue([[Bret: [E|Mad]And they're not obligated to by the state... huh. ]])
		fnDialogue([[Thought: Bret gets lost in his own musings, but then realizes I'm still there, pondering the troubling implications myself.]])
		fnDialogue([[Bret: [E|Sad]Err, yes, well...[P] Did I get it right?]])
		fnDialogue([[Player: Uhh... yeah.[P] I guess you did.[P] Pretty cool talent you've got there. ]])
		fnDialogue([[Thought: He waves at me bashfully.]])
		fnDialogue([[Bret: [E|Blush]Nah, it's dumb.[P] [E|Neutral]I'm only right, like one out of ten times.[P][E|Sad] Anyway, I feel like I've kept you for too long.]])
		fnDialogue([[Player: O-oh, no![P] I'm just doing what you said, right?[P] \"Let someone else talk, and then you're the bestest of pals.\"]])
		fnDialogue([[Thought: Bret flushes.]])
		fnDialogue([[Bret: [E|Blush]You want to be my...? I mean, yeah, but psssshhht![E|Happy] We're pals, all right? But you go out there and make pals with everyone else, okay?]])
		fnDialogue([[Bret: [E|Mad] Go on, get![E|Neutral] I'll be here if you have any more questions.]])
		fnDialogue([[Thought: I take my leave, a hint of unease in my temples.[P] I hate to admit it, but even [ITALICS]I[ENDITALICS] don't know who is really looking after me, or why.]])
		fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
		fnCutsceneBlocker()
		
	--Repeat passes.
	else
		fnStdInvText("[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: Hey there, [PLAYERNAME]! What can I do ya for?")
	end
	
--Desk.
elseif(sTopicName == "Desk") then
	fnStdInvText([[[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|Neutral]Mannnn, high school desks. What's the deal with those, huh?[P][E|Sad] They've been around for decades, and literally no one was like,[P][E|Mad] \"Enough\"! [E|Sad]Just...[P][E|Mad] \"Enough, dude![P] Seriously!\"]])
	
--Grafitti.
elseif(sTopicName == "Grafitti") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|Happy]Most of my doodling is done on the perfectly respectable margins of my notebooks.")

--Whiteboard.
elseif(sTopicName == "WhiteBoard") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|Shock]Did you know that whiteboard markers were invented 15 years after whiteboards? [E|Happy]What a wild world we live in.")

--ReadPoster.
elseif(sTopicName == "ReadPoster") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|Neutral]Reading is like... [E|Mad][ITALICS]so[ENDITALICS] fundamental.[P] Like, totally.")

--EarthPoster.
elseif(sTopicName == "EarthPoster") then
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnDialogue([[[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|Shock]The poster instructs us to \"save the earth\", but it doesn't really say how.]])
	fnDialogue([[Bret: [E|Neutral]Kind of an untindended metaphor for the american education system, [E|happy]am I right?[P][E|Blush] Hehe, I'll shut up now.]])
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()
	
--Window.
elseif(sTopicName == "Window") then
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnDialogue([[[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|Neutral]Oh man, that's a window all right.[P] It's got those...[P] [E|Mad]got those [ITALICS]panes[ENDITALICS].[P][E|Neutral] Got that sick [E|Mad][ITALICS]glass[ENDITALICS].[P] MMM.]])
	fnDialogue([[Bret: [E|Neutral]Don't even get me started on that opening and closing shutter action![P][E|Happy] Whoo!]])
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()
	
--Computer.
elseif(sTopicName == "Computer") then
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnDialogue([[[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|Neutral]ohoh yeah, baby. [ITALICS]The computer[ENDITALICS].[P] Hehe... [E|Happy]She and I are [ITALICS]very[ENDITALICS] well acquainted.]])
	fnDialogue([[Thought: Bret suddenly straightens up, turning bright red.]])
	fnDialogue([[Bret: [E|Shock]Oh, wait, I didn't mean like--[E|Blush]that came out weird.[P] [E|Sad]What I mean is, I'm...[E|Neutral]I'm totally using my computer for... just like research, [E|Mad]only... y'know?]])
	fnDialogue([[Bret: [E|Blush]Not like, I mean--what I'm doing is just... [E|Mad]wholesome, okay?[P] [E|Sad]Totally PG, wholesome, just... [E|Mad]family oriented]])
	fnDialogue([[Bret: --[E|Blush]basically just for homework.[P] School stuff, you know?[P] [E|Mad]Wholesome.]])
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()

--Apple.
elseif(sTopicName == "Apple") then
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnDialogue([[[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|Neutral]What is it with Americans and apples?[E|Mad] First you hate 'em, then you turn em into alcohol, then you burn down apple crops because alcohol is bad,]])
	fnDialogue([[Bret: [E|Happy] then you sell 'em as a health food and start giving them to teachers.[P][E|Neutral] It's like, make up your mind, right?]])
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()
	
--Sandwiches.
elseif(sTopicName == "Sandwiches") then
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnDialogue([[[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|Happy]Oh man, I'm all ABOUT this grilled cheese Stacy made right here.]])
	fnDialogue([[Bret: [E|Sad]Last year I remember a few dodgy episodes where she rolled out the Limburger. [P][E|Mad]The classroom stunk for weeks! ]])
	fnDialogue([[Thought: Bret shrugs, pushing his thick glasses a little further up his nose.]])
	fnDialogue([[Bret: [E|Happy]They still tasted pretty good, though.]])
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()

--Pills.
elseif(sTopicName == "Pills") then
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnDialogue([[[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|Neutral]Hey man, what are you doing with those questionable pharmaceuticals?[P][E|Shock] Are you popping pills?[P][E|Mad] Are you riding the white pony? ]])
	fnDialogue([[Thought: He crosses his arms.]])
	fnDialogue([[Bret: [E|Sad]I'm sorry, but I can't join you.]])
	fnDialogue([[Bret: [E|Neutral]If the \"Double Dragon\" arcade machine behind the dumpster of \"Charlie's Pizza Parlor\" is to be trusted [P]-- and I believe it is--[P][E|Happy] [ITALICS]\"Winners don't do drugs!\"[ENDITALICS] ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()

--Newspaper.
elseif(sTopicName == "Newspaper") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|Neutral]That's the Foster Falls Chronicles.[P] Pressed with recycled paper as part of a waste-free initiative the city started about two years ago.")

--Clipboard.
elseif(sTopicName == "Clipboard") then
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnDialogue([[[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|Happy]When I was a kid, me and my brother used to put our toes in the clip end of a clipboard we found and see who could endure the pain longer.]])
	fnDialogue([[Thought: Bret shakes his head, grimacing.]])
	fnDialogue([[Bret: [E|Sad]I don't know why I just told you that.]])
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()

--Person: Mari.
elseif(sTopicName == "Mari") then
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnDialogue([[[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|Neutral]Mari is a preeeeetty cool president, as far as presidents go. I joined this club a couple years ago,]])
	fnDialogue([[Bret: after she consulted me about a battered-up victorian doll which she found,[P] and I quote --[P][E|Sad] \"tied up in the woods\".]])
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()

--Person: Stacy.
elseif(sTopicName == "Stacy") then
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnDialogue([[[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|Neutral]Stacy's always calling me a nerd, but you wanna know who's a bigger nerd than me?[P][E|Mad] Stacy.]])
	fnDialogue([[Bret: [E|Shock]Little miss \"ooh, I like to learn languages for fun because it makes me mysterious and kind of hot!\" ]])
	fnDialogue([[Bret: [E|Mad]Like, what?[P] What is that?[P][E|Sad]... Aw, who am I kidding. [E|Mad]I'm incredibly jealous.]])
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()

--Person: Bret.
elseif(sTopicName == "Bret") then
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnDialogue([[[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|Happy]Hey, it's me![P][E|Blush] Gosh, what can I say about myself that I haven't said a hundred times before and need to stop?]])
	fnDialogue([[Bret: [E|Neutral]I like pizza, video games, collecting bottlecaps,[P][E|Blush] and making Sims of me and characters from Disney movies so that I can finally live out my fantasy of dating Mulan.]])
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()

--Person: Michelle.
elseif(sTopicName == "Michelle") then
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnDialogue([[[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|Mad]Don't be fooled! [E|Neutral]Michelle [ITALICS]looks[ENDITALICS] like the type of girl who would steal my lunch money in elementary school,]])
	fnDialogue([[Bret: [E|Blush] but she has the [ITALICS]heart[ENDITALICS] of someone who would steal my lunch money now.]])
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()
	
--Person: Laura.
elseif(sTopicName == "Laura") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|Neutral]Laura writes the club blog for us, but what's [E|Mad]WAY cooler to me is that she designed and coded the website, [E|Neutral]too.")

end
