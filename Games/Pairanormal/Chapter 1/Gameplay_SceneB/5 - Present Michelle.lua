--[Michelle Presentation]
--Showing stuff to Michelle.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--[Chat Sequences]
--Chatting.
if(sTopicName == "Chat") then
	
	--Variables.
	local iChattedWithMichelle = VM_GetVar("Root/Variables/Chapter1/Investigation/iChattedWithMichelle", "N")
	
	--First pass:
	if(iChattedWithMichelle == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Investigation/iChattedWithMichelle", "N", 1.0)
		
		--Dialogue.
		fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
		fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
		fnCutsceneBlocker()
		fnDialogue([[[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Thought: Michelle is draped casually across a couple of school desks which have been haphazardly pushed together.]])
		fnDialogue([[Thought: Her long, fishnet-clad legs are propped up on a desk by a couple of chunky black leather boots.]])
		fnDialogue([[Thought: As I approach, she expertly blows a ginormous pink bubblegum bubble, bulging elegantly out of her glossy, deep purple lipstick.]])
		fnDialogue([[Thought: She gives me a coy smile.]])
		fnDialogue([[Michelle: [E|Happy] Well, well, if it isn't--]])
		fnDialogue([[Info: *POP*!]])
		fnDialogue([[Thought: The bubble erupts, splattering across half of Michelle's face.]])
		fnDialogue([[[HIDEALLFAST]Michelle: [SFX|World|Crash]Mmmph![P] FUCK![P] SHIT.[P] For FUCK---[P] ... fuck's sake...!]])
		fnDialogue([[Thought: Eyes wide in horror, Michelle whirls around, turning her gum-covered face away as she picks pink pieces out of her hair.]])
		fnDialogue([[Michelle: [SFX|World|Crash]FuckShitFUCKDAMNIT!!!]])
		fnDialogue([[Player: Oh man, are you oka--]])
		fnDialogue([[Thought: Michelle's arm darts to my collar as I lean in,[P] grabbing it with an iron fist and locking in place to establish distance.]])
		fnDialogue([[[SHOWCHAR|Michelle|Mad]Michelle: [E|Mad]What the hell do you think you're doing!?[P][SFX|World|Crash] Quit getting in my face!]])
		fnDialogue([[Thought: Having scraped off the gum, she quickly balls it up with her free hand and slams it under the bottom of the desk.]])
		fnDialogue([[Player: I'm sorry, I didn't mean to--]])
		fnDialogue([[Michelle: [E|Mad]It's not funny, all right?[P][E|Neutral] I happen to be an expert bubble blower.]])
		fnDialogue([[Michelle: [E|Mad]There [ITALICS]happens[ENDITALICS] to be a cold draft in here, which affects the consistency of gum when it's exposed to air.]])
		fnDialogue([[Thought: She thumbs the corner of her mouth, checking it to make sure her lipstick is intact.]])
		fnDialogue([[Player: I wasn't laughing, I swear--]])
		fnDialogue([[Michelle: [E|Happy]I didn't notice the draft until just now.[P][E|Shock] But you feel it, don't you?]])
		fnDialogue([[Thought: She yanks me towards her,[P] her thick lashes wild and wide.]])
		fnDialogue([[Michelle: [SFX|World|Crash][E|Mad]DONT YOU???]])
		fnDialogue([[Player: I-I feel it![P][SFX|Ugh] Oh, I feel it.]])
		fnDialogue([[Thought: Her face lingers there, inches from mine, for several uncomfortable moments.]])
		fnDialogue([[Thought: Suddenly, the intensity drops from her expression.[P] She releases me, a hint of redness creeping into her cheeks.]])
		fnDialogue([[Michelle: [E|Blush]Sorry.[P] I didn't mean to grab you.[E|Sad] I'm still learning to--[P]I mean, [E|Neutral]Stacy said I should ask people before I touch them.]])
		fnDialogue([[Player: That's a good tip.]])
		fnDialogue([[Thought: Michelle slumps back into the desk, arms crossed.]])
		fnDialogue([[Michelle: [E|Neutral]What do you want, anyway?]])
		fnDialogue([[Player: Oh, n-nothing...[P]I just wanted to meet you.]])
		fnDialogue([[Thought: She raises an eyebrow.]])
		fnDialogue([[Michelle: [E|Neutral]Meet [ITALICS]me[ENDITALICS]?[P].[P].[P].[E|Blush]Why?]])
		fnDialogue([[Player: Well, I'm trying to get to know everyone in club.[P] It's my first day and all.]])
		fnDialogue([[Player: Mari and Stacy said I should learn everything I can about everyone here,[P] since we're gonna be spending a lot of time together in the future.]])
		fnDialogue([[Michelle: [E|Blush][P][P][P]Oh.]])
		fnDialogue([[Thought: She tosses her hair, looking out the window.]])
		fnDialogue([[Michelle: [E|Happy]Welp, we've met.[P] Welcome to the club.[P] Sorry again for grabbing you, I won't do it again.]])
		fnDialogue([[Player: At least,[P] not without my permission.]])
		fnDialogue([[Michelle: [E|Shock]wh-what?]])
		fnDialogue([[Thought: I notice a stray piece of gum leftover in her hair.[P] Without thinking, I reach for it.[P] To my surprise, she doesn't make any moves to kill me.]])
		fnDialogue([[Thought: Perfectly still, her eyes follow me as I pull out the gum and flick it away,[P] letting her curly purple lock fall gently back down to her cheek.]])
		fnDialogue([[Player: Okay then.[P] See you around, Michelle.]])
		fnDialogue([[Michelle: [E|Blush]Wh...[P]Whatever![P][P][E|Happy] Y'weirdo!]])
		fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
		fnCutsceneBlocker()

	--Repeat passes.
	else
		fnStdInvText("[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Michelle: What do you want, weirdo?")
	end
	
--Desk.
elseif(sTopicName == "Desk") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Michelle: [E|Neutral]... It's a desk.")
	
--Grafitti.
elseif(sTopicName == "Grafitti") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Michelle: [E|Neutral]Pah, that's nothing.[P][E|Happy] You oughta see the carvings I made on the back row of desks in room 120-B. ")

--Whiteboard.
elseif(sTopicName == "WhiteBoard") then

	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnDialogue([[[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Michelle: [E|Happy]My best prank to date was when I wrote \"Michelle is probably the best student in Foster Falls\" on the whiteboard.]])
	fnDialogue([[Player: Wait,[P][SFX|World|Blink] how is that a prank?]])
	fnDialogue([[Michelle: [E|Neutral]BECAUSE weirdo,[P] when they erased the message, the letters I had written in permanent ink were left behind.]])
	fnDialogue([[Michelle: [E|Mad][SFX|World|Crash] \"PENIS\" ]])
	fnDialogue([[Thought: She nods her head slowly in satisfaction.]])
	fnDialogue([[Michelle: [E|Happy]*sigh*...[P]Classic.]])
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()

--ReadPoster.
elseif(sTopicName == "ReadPoster") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Michelle: [E|Neutral]Ugh, school posters.")

--EarthPoster.
elseif(sTopicName == "EarthPoster") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Michelle: [E|Neutral]Ugh, school posters.")

--Window.
elseif(sTopicName == "Window") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Michelle: [E|Neutral]... Its a window, weirdo.")

--Computer.
elseif(sTopicName == "Computer") then
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnDialogue([[[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Michelle: [E|Mad]The computers here are SO SLOW.]])
	fnDialogue([[Michelle: Last year I [ITALICS]literally[ENDITALICS] typed my entire final English essay on my phone in the time it took for one of these stone age machines to start up.]])
	fnDialogue([[Player: Whoah![P] That's impressive.]])
	fnDialogue([[Michelle: [E|Neutral]Yeah, Mr. Cortez didn't really agree.[P] Probably cuz I only wrote like,[P] two paragraphs.]])
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()

--Apple.
elseif(sTopicName == "Apple") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Michelle: [E|Neutral]Why don't teachers put something actually interesting on their desks instead of fake fruit?[E|Shock] I'm thinking a voodoo doll,[P] or like, a tiny replica French guillotine.")

--Sandwiches.
elseif(sTopicName == "Sandwiches") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Michelle: [E|Neutral]I love Stacy, but I'm gonna have to pass.[P] I've got enough gum to last me through the term, thanks very much.")

--Pills.
elseif(sTopicName == "Pills") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Michelle: [E|Shock][SFX|World|Crash]Oi![P][E|Mad] Leave those alone.[P] Those are Mari's, all right?[P] She doesn't like it when you touch her things.")

--Newspaper.
elseif(sTopicName == "Newspaper") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Michelle: [E|Neutral]I'll get my news off of Twitter like any respectable teen, thanks very much.")

--Clipboard.
elseif(sTopicName == "Clipboard") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Michelle: [E|Neutral]... That's a clipboard, weirdo.")

--Person: Mari.
elseif(sTopicName == "Mari") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Michelle: [E|Neutral]Mari and Stacy met me last year in the girl's bathroom.[P] Practically begged me to join the club... what can I say?[P][E|Happy] I'm a desirable asset.")

--Person: Stacy.
elseif(sTopicName == "Stacy") then
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnDialogue([[[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Michelle: [E|Neutral]Stacy's [ITALICS]probably[ENDITALICS] the second coolest person at this school after me.]])
	fnDialogue([[Michelle: [E|Happy] She might look like a string bean in uniform,[P] but that girl can out-bench press every member of the football team.]])
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()
	
--Person: Bret.
elseif(sTopicName == "Bret") then
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnDialogue([[[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Michelle: [E|Neutral]Listen up, weirdo.[P][E|Mad] Don't even THINK about picking on or laying a hand on that little know-it-all nerd, all right?]])
	fnDialogue([[Michelle: [E|Blush] The position has already been taken by me.]])
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()

--Person: Michelle.
elseif(sTopicName == "Michelle") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Michelle: [E|Shock]What, I'm not gonna just open up and tell you my life story, all right?[P][E|Neutral] If you wanna get close to me, you gotta earn it. ")

--Person: Laura.
elseif(sTopicName == "Laura") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Michelle: [E|Neutral]Laura doesn't talk much, but don't let that fool you.[P] She's one of the smartest girls in this school--[P][E|Happy]and I'm including the teachers in that.")

end

