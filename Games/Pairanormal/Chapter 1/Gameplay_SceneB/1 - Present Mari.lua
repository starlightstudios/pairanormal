--[Mari Presentation]
--Showing stuff to Mari.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--[Chat Sequences]
--Chatting.
if(sTopicName == "Chat") then
	
	--Variables.
	local iChattedWithMari = VM_GetVar("Root/Variables/Chapter1/Investigation/iChattedWithMari", "N")
	
	--First pass:
	if(iChattedWithMari == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter1/Investigation/iChattedWithMari", "N", 1.0)
		
		--Text.
		fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
		fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
		fnCutsceneBlocker()
		fnDialogue([[[HIDEALLFAST][SHOWCHAR|Mari|Neutral]Mari: [E|Neutral]Hello hello! How's it going? [E|Mad][SFX|World|Crash]ARE YOU MAKING FRIENDS!?!?[P][SFX|World|Crash] ARE YOU STAYING HYDRATED?!?]])
		fnDialogue([[Mari: [P][P][P]I'm your mom now, deal with it.]])
		fnDialogue([[Player: I'm doing good.]])
		fnDialogue([[Mari: [E|Neutral]Excellent. [P][E|Sad]I've been worried about the vibe in here all day; can't quite get a reading on it.]])
		fnDialogue([[Mari: [E|Neutral] Unfortunately I'm not allowed to sage the room.]])
		fnDialogue([[Player: \"Sage the room\"?]])
		fnDialogue([[Mari: [E|Blush]It's a witch thing. [P][E|Neutral]Before you enter a new space, you light a bundle of sage and smoke every corner to purify the area and ward off evil spirits.]])
		fnDialogue([[Mari: I used to do it all the time in here,[P] but [E|Mad]APPARENTLY lighting a bundle of sticks in school is a [P]\"fire hazard\" [P]and now I'm not allowed. ]])
		fnDialogue([[Mari: [E|Sad]But like,[P] if you don't kill evil spirits that are hanging out around here,[P][SFX|World|Crash] you're gonna have bigger problems on your hands than a fire,[P][E|Mad] KAREN.]])
		fnDialogue([[Player: Are evil spirits...[P]is that a real thing?]])
		fnDialogue([[Thought: Mari smiles sheepishly and shrugs.]])
		fnDialogue([[Mari: [E|Happy]I'm not leaving it to chance. ]])
		fnDialogue([[Mari: [E|Neutral]Anyway, enough schmoozing with the president. [P][E|Blush]Go, mingle! have fun!]])
		fnDialogue([[Player: Roger that. ]])
		fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
		fnCutsceneBlocker()
		
	--Repeat passes.
	else
		fnStdInvText("[HIDEALLFAST][SHOWCHAR|Mari|Neutral]Mari: What's up? Go on, don't be afraid - mingle!")
	end
	
--Desk.
elseif(sTopicName == "Desk") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Mari|Neutral]Mari: [E|Neutral]I don't get why we suddenly went from tables in elementary school to these crummy little desks.[P][SFX|World|Crash][E|Shock] TABLES WERE SO MUCH COOLER.")
	
--Grafitti.
elseif(sTopicName == "Grafitti") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Mari|Neutral]Mari: [E|Neutral]That's nothing compared to Michelle's desk doodles.[P] She's the best darn desk doodler in the world.")

--Whiteboard.
elseif(sTopicName == "WhiteBoard") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Mari|Neutral]Mari: [E|Neutral]That's a whiteboard, all right.")

--ReadPoster.
elseif(sTopicName == "ReadPoster") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Mari|Neutral]Mari: [E|Neutral]My literary tastes have gradually shifted from contemporary fantasy[P][E|Blush] to the more sophisticated tentacle smut fiction.")

--EarthPoster.
elseif(sTopicName == "EarthPoster") then
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnDialogue([[[HIDEALLFAST][SHOWCHAR|Mari|Shock]Mari: [E|Shock][SFX|World|Crash]SAVE THE EARTH!!!]])
	fnDialogue([[Player: Wh-what?[P] is it under attack or something!?]])
	fnDialogue([[Thought: Mari shrugs. ]])
	fnDialogue([[Mari: [E|Neutral]Probably.]])
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()
	
--Window.
elseif(sTopicName == "Window") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Mari|Neutral]Mari: [E|Neutral]Yep, that's a window.")

--Computer.
elseif(sTopicName == "Computer") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Mari|Neutral]Mari: [E|Shock][SFX|World|Crash]BEEP BEEP BOOP![P][E|Neutral] hehe, Get it?[P][P][P][E|Happy] That's what computers sound like.")

--Apple.
elseif(sTopicName == "Apple") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Mari|Neutral]Mari: [E|Neutral]Apple a day keeps the doctor away.[P] And teachers are notoriously terrified of doctors.")

--Sandwiches.
elseif(sTopicName == "Sandwiches") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Mari|Neutral]Mari: [E|Neutral]Stacy makes a mean grilled cheese sandwich.[P] And by 'mean', I mean it goes down fighting.")


--Pills.
elseif(sTopicName == "Pills") then
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnDialogue([[[HIDEALLFAST][SHOWCHAR|Mari|Neutral]Thought: Mari's demeanor changes as soon as she sees the bottle of pills.]])
	fnDialogue([[Mari: [E|Mad][SFX|World|Crash]HEY!!!]])
	fnDialogue([[Thought: She snatches them away and places them back where you found them.]])
	fnDialogue([[Mari: [E|Mad]Why are you just... touching pills?[P][SFX|World|Crash] You can't just go around touching stuff!!!]])
	fnDialogue([[Player: I'm sorry, I thought you said--]])
	fnDialogue([[Thought: She sighs, turning away.]])
	fnDialogue([[Mari: [E|Sad]Sorry, [PLAYERNAME].[P] I'm so sorry... c-can you just give me a minute?]])
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()

--Newspaper.
elseif(sTopicName == "Newspaper") then
	
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnDialogue([[[HIDEALLFAST][SHOWCHAR|Mari|Neutral]Mari: [E|Neutral]Ol' Mr. Foster finally croaked.[P] I guess I should feel sad, but.[P].[P].[P][E|Shock] I don't?]])
	fnDialogue([[Player: Who was \"Ol Mr. Foster\"?]])
	fnDialogue([[Mari: [E|Neutral]I guess you could say he helped found the town, sort of?[P] It's his statue you see at the front of the school every day.[BR] He was kind of a big deal, I guess.]])
	fnDialogue([[Player: Oh.[P] How come you don't feel sad?]])
	fnDialogue([[Thought: Mari scrunches up her nose, thinking.]])
	fnDialogue([[Mari: [E|Sad]I mean, I never met the guy.[P][E|Neutral] I think his daughter is on the school board, though. ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()

--Clipboard.
elseif(sTopicName == "Clipboard") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Mari|Neutral]Mari: [E|Neutral] That's a clipboard, all right.")

--Person: Mari.
elseif(sTopicName == "Mari") then
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnDialogue([[[HIDEALLFAST][SHOWCHAR|Mari|Neutral]Mari: [E|Neutral].[P].[P].[P][E|Happy]THATS ME! [P][E|Neutral]Paranormal Investigation Club co-president!]])
	fnDialogue([[[E|Happy] I'm a witch and I know exactly how to seduce 23 different types of paranormal beings,[P] so I'm very qualified.]])
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()
	
--Person: Stacy.
elseif(sTopicName == "Stacy") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Mari|Neutral]Mari: [E|Neutral]Stacy is my best friend.[P][E|Blush] That means, if I ever do manage to summon a hot succubus boyfriend, she's the only person I would share him with.")
	
--Person: Bret.
elseif(sTopicName == "Bret") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Mari|Neutral]Mari: [E|Shock]I don't know what the club would do without Bret.[P][E|Neutral] He's basically a walking encyclopedia. ")

--Person: Michelle.
elseif(sTopicName == "Michelle") then
	fnStdInvText("[HIDEALLFAST][SHOWCHAR|Mari|Neutral]Mari: [E|Neutral]Michelle joined our club last year.[P][E|Happy] She's basically a [SFX|World|Crash]TOTAL BADASS.")

--Person: Laura.
elseif(sTopicName == "Laura") then
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnDialogue([[[HIDEALLFAST][SHOWCHAR|Mari|Neutral][HIDEALLFAST][SHOWCHAR|Mari|Neutral]Mari: [E|Sad]One time in Junior High, Barton Ashley pushed Laura down some stairs.]])
	fnDialogue([[Mari: [E|Neutral]The next day all the school printers didn't spit out anything but a doctor's note his mom sent in the week before--]])
	fnDialogue([[Mari: [E|Happy][SFX|World|Crash] EXCUSING HIM FROM PE BECAUSE OF HEMMORRHOIDS.]])
	fnDialogue([[Mari: [E|Neutral] Now I'm not saying those two incidents are related,[P][E|Happy] but I am saying Laura is a genius hacker tech witch who should not be trifled with!!!]])
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()

end
