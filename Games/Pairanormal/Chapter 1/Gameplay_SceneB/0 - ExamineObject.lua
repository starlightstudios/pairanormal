--[Investigation of the Classroom]
--The first investigation sequence in the classroom.

--[Arguments]
--Get the name of what we're investigating.
local sExamine = LM_GetScriptArgument(0)

--[Variable Setup]
--Used to simplify the Add Discovered Object calls.
local sBGPath = "Root/Images/Backgrounds/All/Classroom"
local sScriptPath = gsRoot .. "Gameplay_SceneB/0 - ExamineObject.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
--No auto-advance during this scene.

--Saving
local sSaveExecute = gsRoot .. "Gameplay_SceneA/5 - Investigation Prelude.lua"
local sSaveTopic = "No Topic"
local sSaveFlag = "Investigation"

--[Desk]
if(sExamine == "Desk") then
	
	--Store it in the inventory.
	local i = gciInvA_Desk
	Dia_SetProperty("Add Discovered Object", gczaObjA[i][1], sBGPath, sScriptPath, gczaObjA[i][1])
	VM_SetVar("Root/Variables/Chapter1/InvestigationA/iObject|" .. gczaObjA[i][1], "N", 1.0)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: looks like you could sit here.[P] And there's a little table attached.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Saving.
	PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
	
--[Grafitti]
elseif(sExamine == "Grafitti") then
	
	--Store it in the inventory.
	local i = gciInvA_Grafitti
	Dia_SetProperty("Add Discovered Object", gczaObjA[i][1], sBGPath, sScriptPath, gczaObjA[i][1])
	VM_SetVar("Root/Variables/Chapter1/InvestigationA/iObject|" .. gczaObjA[i][1], "N", 1.0)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: Somebody who sat here was a very skilled artist.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Saving.
	PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)

--[Whiteboard]
elseif(sExamine == "WhiteBoard") then
	
	--Store it in the inventory.
	local i = gciInvA_Whiteboard
	Dia_SetProperty("Add Discovered Object", gczaObjA[i][1], sBGPath, sScriptPath, gczaObjA[i][1])
	VM_SetVar("Root/Variables/Chapter1/InvestigationA/iObject|" .. gczaObjA[i][1], "N", 1.0)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: Eilette has one of these in her house.[P] You can write on it with special markers.[P] ONLY the special markers.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Saving.
	PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)

--[ReadPoster]
elseif(sExamine == "ReadPoster") then
	
	--Store it in the inventory.
	local i = gciInvA_ReadPoster
	Dia_SetProperty("Add Discovered Object", gczaObjA[i][1], sBGPath, sScriptPath, gczaObjA[i][1])
	VM_SetVar("Root/Variables/Chapter1/InvestigationA/iObject|" .. gczaObjA[i][1], "N", 1.0)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: I wonder what that poster says.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Saving.
	PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)

--[EarthPoster]
elseif(sExamine == "EarthPoster") then
	
	--Store it in the inventory.
	local i = gciInvA_EarthPoster
	Dia_SetProperty("Add Discovered Object", gczaObjA[i][1], sBGPath, sScriptPath, gczaObjA[i][1])
	VM_SetVar("Root/Variables/Chapter1/InvestigationA/iObject|" .. gczaObjA[i][1], "N", 1.0)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: Aha! The planet we're all [ITALICS]allegedly[ENDITALICS] standing on.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Saving.
	PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)

--[Computer]
elseif(sExamine == "Computer") then
	
	--Store it in the inventory.
	local i = gciInvA_Computer
	Dia_SetProperty("Add Discovered Object", gczaObjA[i][1], sBGPath, sScriptPath, gczaObjA[i][1])
	VM_SetVar("Root/Variables/Chapter1/InvestigationA/iObject|" .. gczaObjA[i][1], "N", 1.0)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: That's a computer.[P] I don't have too much experience with them.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Saving.
	PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)

--[Sandwiches]
elseif(sExamine == "Sandwiches") then
	
	--Store it in the inventory.
	local i = gciInvA_Sandwiches
	Dia_SetProperty("Add Discovered Object", gczaObjA[i][1], sBGPath, sScriptPath, gczaObjA[i][1])
	VM_SetVar("Root/Variables/Chapter1/InvestigationA/iObject|" .. gczaObjA[i][1], "N", 1.0)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: It's just two pieces of bread and a slice of cheese in between them.[P] And it's grilled.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Saving.
	PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)

--[Pills]
elseif(sExamine == "Pills") then
	
	--Store it in the inventory.
	local i = gciInvA_Pills
	Dia_SetProperty("Add Discovered Object", gczaObjA[i][1], sBGPath, sScriptPath, gczaObjA[i][1])
	VM_SetVar("Root/Variables/Chapter1/InvestigationA/iObject|" .. gczaObjA[i][1], "N", 1.0)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: A bottle of pills.[P] I definitely don't know any of the words on the label.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Saving.
	PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)

--[Newspaper]
elseif(sExamine == "Newspaper") then
	
	--Store it in the inventory.
	local i = gciInvA_Newspaper
	Dia_SetProperty("Add Discovered Object", gczaObjA[i][1], sBGPath, sScriptPath, gczaObjA[i][1])
	VM_SetVar("Root/Variables/Chapter1/InvestigationA/iObject|" .. gczaObjA[i][1], "N", 1.0)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: There's a picture of an important looking man on this paper.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Saving.
	PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)

--[Clipboard]
elseif(sExamine == "Clipboard") then
	
	--Store it in the inventory.
	local i = gciInvA_Clipboard
	Dia_SetProperty("Add Discovered Object", gczaObjA[i][1], sBGPath, sScriptPath, gczaObjA[i][1])
	VM_SetVar("Root/Variables/Chapter1/InvestigationA/iObject|" .. gczaObjA[i][1], "N", 1.0)

	--Store it in the inventory.
	Dia_SetProperty("Add Discovered Object", "Clipboard", sBGPath, sScriptPath, "Clipboard")
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: There's lots of underlined words, highlighted passages, and stickers. [BR] I guess whoever uses this clipboard is a big fan of being organized.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Saving.
	PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)

--[Window]
elseif(sExamine == "Window") then
	
	--Store it in the inventory.
	local i = gciInvA_Window
	Dia_SetProperty("Add Discovered Object", gczaObjA[i][1], sBGPath, sScriptPath, gczaObjA[i][1])
	VM_SetVar("Root/Variables/Chapter1/InvestigationA/iObject|" .. gczaObjA[i][1], "N", 1.0)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: Windows are relaxing to look out of.[P] I'm glad there's one in the classroom.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Saving.
	PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
	
--[Apple]
elseif(sExamine == "Apple") then
	
	--Store it in the inventory.
	local i = gciInvA_Apple
	Dia_SetProperty("Add Discovered Object", gczaObjA[i][1], sBGPath, sScriptPath, gczaObjA[i][1])
	VM_SetVar("Root/Variables/Chapter1/InvestigationA/iObject|" .. gczaObjA[i][1], "N", 1.0)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: It's fake!") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Saving.
	PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)

--[Mari]
elseif(sExamine == "Mari") then

	Dia_SetProperty("Add Discovered Object", "Mari",     sCharPath, sScriptPath, "Mari",     1322,   2, 1322 + 438,   2 + 363)
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: Mari. Co-captain of the club with Stacy. Super nice gal. Keeps trying to get me to read these comic books with tentacles on the cover...") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: So far I haven't been brave enough to try, but I admit I'm curious.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()

	--Saving.
	PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)

--[Stacy]
elseif(sExamine == "Stacy") then

	Dia_SetProperty("Add Discovered Object", "Stacy",    sCharPath, sScriptPath, "Stacy",     442, 367,  442 + 438, 367 + 363)
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: Stacy. Co-Captain of the club with Mari. Does spontaneous pushups all the time. I have the feeling everyone in the club has a mild crush on her.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Saving.
	PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)

--[Bret]
elseif(sExamine == "Bret") then

	Dia_SetProperty("Add Discovered Object", "Bret",     sCharPath, sScriptPath, "Bret",        2,   2,    2 + 438,   2 + 363)
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: Bret. Mari calls him a \"thing expert\", and he's quite a chatterbox. Not that I mind; I really like looking at his mouth when he talks.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Saving.
	PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)

--[Laura]
elseif(sExamine == "Laura") then

	Dia_SetProperty("Add Discovered Object", "Laura",    sCharPath, sScriptPath, "Laura",     882,   2,  882 + 438,   2 + 363)
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: Laura. Doesn't say much, but I notice her eyeing her phone and computers a lot. Nice bracelet, too.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Saving.
	PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)

elseif(sExamine == "Michelle") then

	Dia_SetProperty("Add Discovered Object", "Michelle", sCharPath, sScriptPath, "Michelle", 1762,   2, 1762 + 438,   2 + 363)
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: Michelle. I kinda like her dark makeup, but if I told her that she might very well stomp on me or something.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Saving.
	PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
end