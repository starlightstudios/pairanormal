--[Sample Script]
--Useful script that holds prototypes for common actions. Copy and paste!

--[Timing]
--Wait a bit.
fnCutsceneWait(25)
fnCutsceneBlocker()
	
--[Dialogue samples]
--From actual Chapter 1! How exciting!
fnUnlockGallery("Gallery_Classroom")
fnDialogue("Thought:[BG|Root/Images/Backgrounds/All/Classroom][SFX|World|Crash][MUSIC|Null] *PAM*[P] [SFX|World|Crash] *PAM*[P] [SFX|World|Crash] *PAM*")
fnDialogue("[SHOWCHAR|Mari|Neutral]Mari:[E|Neutral][Music|Daytime]Welcome everyone!")

fnDialogue("Mari:[E|Happy]Welcome to the very first PARANORMAL INVESTIGATION CLUB MEETING of the year!!!")
fnDialogue("[SHOWCHAR|Laura|Neutral][SHOWCHAR|Bret|Neutral][SHOWCHAR|Michelle|Neutral][SHOWCHAR|Stacy|Neutral]Thought: [SFX|World|Applause] *Applause*")

--Save the game.
fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Starting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
PairanormalLevel_FlagCheckpoint("Starting")
fnDialogue("Mari:[E|Neutral][SFX|World|Crash]WHAT IS THE PURPOSE OF THIS--")
fnDialogue("Stacy:[E|Shock]Mari! [P]Wait!")
fnDialogue("Mari:[E|Neutral].[P].[P].[P][SFX|World|Blink] What?")
fnDialogue("Stacy:[E|Happy]You forgot to introduce yourself.")
fnDialogue("Mari:[E|Blush]Oh, right...[P] right, okay. *ahem*")
fnDialogue("Mari:[E|Shock][SFX|World|Crash]MY NAME IS MARI![P] [E|Neutral]And I'm the club's co-president!")
fnDialogue("Stacy:[E|Neutral]And [ITALICS]je suis[ENDITALICS] Stacy--")
fnDialogue("Stacy:[E|Neutral]Bodybuilder,[P] dance troupe leader,[P][E|Happy] part time model--[P][E|Neutral] and the other club co-president. ")

--[Branching Stuff]
--What to tell them you like eating:
fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (What should I tell them?)") ]])

local sDecisionScript = "\"" .. gsRoot .. "Gameplay_SceneA/1 - Bret Answer.lua\""
fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"PB&J\", " .. sDecisionScript .. ", \"PB&J\") ")
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"BLT\",  " .. sDecisionScript .. ", \"BLT\") ")
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"What do those letters mean?\",  " .. sDecisionScript .. ", \"Letters\") ")
fnCutsceneBlocker()