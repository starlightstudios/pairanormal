--[Chapter Variables]
--Variables that apply only to this chapter, or are used primarily in it.
DL_AddPath("Root/Variables/Chapter1/")

--[Miscellaneous]
DL_AddPath("Root/Variables/Chapter1/Misc/")

--Quiz.
if(PairanormalLevel_IsLoading() == false) then
    VM_SetVar("Root/Variables/Chapter1/Misc/iQuizPoints", "N", 0.0)
end

--[Common Investigation]
DL_AddPath("Root/Variables/Chapter1/Investigation/")
if(PairanormalLevel_IsLoading() == false) then
    VM_SetVar("Root/Variables/Chapter1/Investigation/iTimeMax", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter1/Investigation/iTimeLeft", "N", 0.0)
end

--[First Investigation]
--Chatting with characters.
DL_AddPath("Root/Variables/Chapter1/InvestigationA/")
if(PairanormalLevel_IsLoading() == false) then
    VM_SetVar("Root/Variables/Chapter1/InvestigationA/iChattedWithMari", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter1/InvestigationA/iChattedWithStacy", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter1/InvestigationA/iChattedWithBret", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter1/InvestigationA/iChattedWithMichelle", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter1/InvestigationA/iChattedWithLaura", "N", 0.0)
end

--Name Lookups
gciInvA_Desk = 1
gciInvA_Grafitti = 2
gciInvA_ReadPoster = 3
gciInvA_EarthPoster = 4
gciInvA_Computer = 5
gciInvA_Sandwiches = 6
gciInvA_Pills = 7
gciInvA_Newspaper = 8
gciInvA_Clipboard = 9
gciInvA_Window = 10
gciInvA_Apple = 11
gciInvA_Whiteboard = 12
gciInvA_Total = 13

--Position Data.
gczaObjA = {}
--              [NAME]          [X]   [Y]   [W]   [H]
gczaObjA[ 1] = {"Desk",        1681, 1231,  366,  128}
gczaObjA[ 2] = {"Grafitti",    2019, 1404,  432,   54}
gczaObjA[ 3] = {"ReadPoster",   250,  736,  197,  540}
gczaObjA[ 4] = {"EarthPoster",   10,  707,  228,  525}
gczaObjA[ 5] = {"Computer",    1532, 1018,  224,  201}
gczaObjA[ 6] = {"Sandwiches",   949, 1149,  294,   66}
gczaObjA[ 7] = {"Pills",        745, 1416,   79,   58}
gczaObjA[ 8] = {"Newspaper",   2364,  835,  157,  206}
gczaObjA[ 9] = {"Clipboard",   1178,  970,  121,  132}
gczaObjA[10] = {"Window",      2297,  358,  263,  360}
gczaObjA[11] = {"Apple",       1376, 1162,   54,   46}
gczaObjA[12] = {"WhiteBoard",   707,  753,  820,  336}

--Construct script variables.
if(PairanormalLevel_IsLoading() == false) then
    for i = 1, gciInvA_Total - 1, 1 do
        VM_SetVar("Root/Variables/Chapter1/InvestigationA/iObject|" .. gczaObjA[i][1], "N", 0.0)
    end
end

--[Second Investigation]
DL_AddPath("Root/Variables/Chapter1/InvestigationB/")

--Name Lookups
gciInvB_Books     = 1
gciInvB_Plant     = 2
gciInvB_Magazines = 3
gciInvB_Door      = 4
gciInvB_Statues   = 5
gciInvB_Jacket    = 6
gciInvB_Notepad   = 7
gciInvB_Kitchen   = 8
gciInvB_Total     = 9

--Position Data.
gciaObjB = {}
--             [NAME]        [X]   [Y]   [W]   [H]
gciaObjB[1] = {"Books",        0,  237,  194, 1098}
gciaObjB[2] = {"Plant",      431,  941,  195,  438}
gciaObjB[3] = {"Magazines", 1130, 1429,  329,  117}
gciaObjB[4] = {"Door",      1212,  574,  344,  774}
gciaObjB[5] = {"Statue",    2021, 1042,  533,  211}
gciaObjB[6] = {"Jacket",    1738,  623,  149,  601}
gciaObjB[7] = {"Notepad",   1953,  751,  176,  180}
gciaObjB[8] = {"Kitchen",   1964,  454,  596,  525}

--Construct script variables.
if(PairanormalLevel_IsLoading() == false) then
    for i = 1, gciInvB_Total - 1, 1 do
        VM_SetVar("Root/Variables/Chapter1/InvestigationB/iObject|" .. gciaObjB[i][1], "N", 0.0)
    end
end

--[Backgrounds]
--List of backgrounds.
SLF_Open(fnResolvePath() .. "../../Datafiles/Backgrounds.slf")
local saList = {"Classroom", "Hospital", "SchoolExterior", "WalkHome", "LivingRoom", "Kitchen", "Forest", "BedroomNight", "Burning", "StacyRoom", "BasketballCourt", "Bathroom", "Sunset", 
                "EndOfChapterOne", "EndOfChapterTwo", "EndOfChapterThree", "EndOfChapterFour", "RunHome"}

for i = 1, #saList, 1 do
    DL_ExtractBitmap(saList[i], "Root/Images/Backgrounds/All/" .. saList[i])
end
SLF_Close()
