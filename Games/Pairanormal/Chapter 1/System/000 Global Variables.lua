-- |[Global Variables]|
--These are global variables and constants the program is going to need.
gcfTicksPerSecond = 60.0
gcfSecondsPerTick = 1.0 / 60.0

--Variables used by save files.
DL_AddPath("Root/Variables/System/Player/")
VM_SetVar("Root/Variables/System/Player/Playername",     "S", "Jay")
VM_SetVar("Root/Variables/System/Player/LastBackground", "S", "No Background")
VM_SetVar("Root/Variables/System/Player/LastMusic",      "S", "No Music")
VM_SetVar("Root/Variables/System/Player/PlayTime",       "N", 0.0)
VM_SetVar("Root/Variables/System/Player/VisibleChars",   "S", "Nobody")
VM_SetVar("Root/Variables/System/Player/iChapterDestination",  "N", 3.0)
VM_SetVar("Root/Variables/System/Player/iWalkedWithMariA",     "N", 0.0)
VM_SetVar("Root/Variables/System/Player/iWalkedWithBretA",     "N", 0.0)
VM_SetVar("Root/Variables/System/Player/iWalkedWithStacyA",    "N", 0.0)
VM_SetVar("Root/Variables/System/Player/iWalkedWithLauraA",    "N", 0.0)
VM_SetVar("Root/Variables/System/Player/iWalkedWithMichelleA", "N", 0.0)
VM_SetVar("Root/Variables/System/Player/iWalkedWithToddA",     "N", 0.0)

-- |[Variables Used In Walkhomes]|
VM_SetVar("Root/Variables/System/Player/iReadToddsMind", "N", 0.0)


--Variables used by investigations.
DL_AddPath("Root/Variables/System/Investigation/")
VM_SetVar("Root/Variables/System/Investigation/sInvestigationPath", "S", "Root/Variables/Chapter1/InvestigationA/")

-- |[Roooomance!]|
DL_AddPath("Root/Variables/System/Romance/")
VM_SetVar("Root/Variables/System/Romance/sLastWalkHome", "S", "Nobody")
VM_SetVar("Root/Variables/System/Romance/iMariFriend", "N", 0.0)
VM_SetVar("Root/Variables/System/Romance/iStacyFriend", "N", 0.0)
VM_SetVar("Root/Variables/System/Romance/iMichelleFriend", "N", 0.0)
VM_SetVar("Root/Variables/System/Romance/iBretFriend", "N", 0.0)
VM_SetVar("Root/Variables/System/Romance/iLauraFriend", "N", 0.0)
VM_SetVar("Root/Variables/System/Romance/iToddFriend", "N", 0.0)
VM_SetVar("Root/Variables/System/Romance/iDogFriend", "N", 0.0)

VM_SetVar("Root/Variables/System/Romance/sBuddy", "S", "Mari")

-- |[Misc]|
DL_AddPath("Root/Variables/ChapterVars/Chapter2/")
VM_SetVar("Root/Variables/ChapterVars/Chapter2/iGreetedToddWarmly", "N", 0.0)

DL_AddPath("Root/Variables/ChapterVars/Chapter3/")
VM_SetVar("Root/Variables/ChapterVars/Chapter3/iMetMistress", "N", 0.0)

DL_AddPath("Root/Variables/ChapterVars/Chapter4/")
VM_SetVar("Root/Variables/ChapterVars/Chapter4/iMetAdam", "N", 0.0)
VM_SetVar("Root/Variables/ChapterVars/Chapter4/iDecoratedGym", "N", 0.0)
    
DL_AddPath("Root/Variables/ChapterVars/Chapter5/")
VM_SetVar("Root/Variables/ChapterVars/Chapter5/iToddAlong", "N", 0.0)

VM_SetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToTodd", "N", 0.0)
VM_SetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToBret", "N", 0.0)
VM_SetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToLaura", "N", 0.0)
VM_SetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToMichelle", "N", 0.0)

VM_SetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToThree", "N", 0.0)
VM_SetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToSix", "N", 0.0)
VM_SetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToDog", "N", 0.0)
VM_SetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToTwins", "N", 0.0)

-- |[Present Array]|
--Array used to track which characters have been shown objects. These are the indices in the arrays,
-- the arrays are declared for each object during an investigation in the chapter variables.
iMariIndex = 1
iStacyIndex = 2
iMichelleIndex = 3
iBretIndex = 4
iLauraIndex = 5
iToddIndex = 6
iDogIndex = 7
iTerryIndex = 8
iRossIndex = 9
iHighestIndex = 9
