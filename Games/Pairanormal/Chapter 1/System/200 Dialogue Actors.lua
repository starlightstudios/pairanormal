-- |[ ================================ Dialogue Actor Creation ================================= ]|
--Dialogue Actors are the spine objects who appear during gameplay. They are animated and have a set
-- of emotion/expression pairs. They must be created here before they are used.
--All characters use the same set of emotions. They are either just the neutral pose if the character
-- has one emotion, or they are neutral/sad/happy/blush/mad/shocked over 3 poses.
-- The format for emotions is always charname_emotionname, with neutral being 'n'.
local function fnCreateCharacter(saNameList, saLowerList, zaEmotionList, faHalfWidthList)

	--Arg check.
	if(saNameList      == nil) then return end
	if(saLowerList     == nil) then return end
	if(zaEmotionList   == nil) then return end
	if(faHalfWidthList == nil) then return end
	
    --Get the Pairanormal spine path.
    local sPairanormalBasePath = VM_GetVar("Root/Paths/System/Startup/sPairanormalPath", "S")
    local sSpinePath = sPairanormalBasePath .. "Spine/"
    
	--Build the name lookups.
	local saLookupList  = {"Neutral", "Sad", "Happy", "Blush", "Mad", "Shock"}
	local saCompareList = {"n",       "sad", "happy", "blush", "mad", "shock"}
	
	--Arrays are expected to be parallel.
	local i = 1
	while(saNameList[i] ~= nil and saLowerList[i] ~= nil and zaEmotionList[i] ~= nil and faHalfWidthList[i] ~= nil) do

		--Create the character using their uppercase name.
		Dia_CreateCharacter(saNameList[i])

			--If the character uses one expression, it's the neutral. The list should only have one entry in that case.
			if(zaEmotionList[i][2] == nil) then
                
                --Add the neutral.
				DiaChar_SetProperty("Add Expression", "Expression0", sSpinePath .. saNameList[i] .. "/Expression0", "Root/Images/Characters/" .. saNameList[i] .. "/Expression0", saLowerList[i] .. "_n", "null")
                
                --If the zeroth is just "n" then it's the default _n emotion:
                if(zaEmotionList[i][1] == "n") then
                    DiaChar_SetProperty("Remap Emotion", "Neutral", "Expression0", saLowerList[i] .. "_n")
                
                --Otherwise, use "animation".
                else
                    DiaChar_SetProperty("Remap Emotion", "Neutral", "Expression0", "animation")
                end
		
			--Character uses all six emotions.
			else
				DiaChar_SetProperty("Add Expression", "Expression0", sSpinePath .. saNameList[i] .. "/Expression0", "Root/Images/Characters/" .. saNameList[i] .. "/Expression0", saLowerList[i] .. "_" .. zaEmotionList[i][1], saLowerList[i] .. "_" .. zaEmotionList[i][2])
				DiaChar_SetProperty("Add Expression", "Expression1", sSpinePath .. saNameList[i] .. "/Expression1", "Root/Images/Characters/" .. saNameList[i] .. "/Expression1", saLowerList[i] .. "_" .. zaEmotionList[i][3], saLowerList[i] .. "_" .. zaEmotionList[i][4])
				DiaChar_SetProperty("Add Expression", "Expression2", sSpinePath .. saNameList[i] .. "/Expression2", "Root/Images/Characters/" .. saNameList[i] .. "/Expression2", saLowerList[i] .. "_" .. zaEmotionList[i][5], saLowerList[i] .. "_" .. zaEmotionList[i][6])
				
				--Remaps
				for e = 1, 3, 1 do
					
					--Slot Lookups
					local iSlot = ((e-1)*2)+1
					
					--Name lookups
					local sLookupA = "Neutral"
					local sLookupB = "Neutral"
					for p = 1, 6, 1 do
						if(saCompareList[p] == zaEmotionList[i][iSlot+0]) then sLookupA = saLookupList[p] end
						if(saCompareList[p] == zaEmotionList[i][iSlot+1]) then sLookupB = saLookupList[p] end
					end
					
					--Set.
					DiaChar_SetProperty("Remap Emotion", sLookupA, "Expression" .. e-1, saLowerList[i] .. "_" .. zaEmotionList[i][iSlot+0])
					DiaChar_SetProperty("Remap Emotion", sLookupB, "Expression" .. e-1, saLowerList[i] .. "_" .. zaEmotionList[i][iSlot+1])
				
				end
		
			end
		
			--Other character properties.
			DiaChar_SetProperty("Half Width", faHalfWidthList[i] * 0.10)
			
			if(saNameList[i] == "Eilette") then
				DiaChar_SetProperty("Render Offset", 85)
			end
		
		DL_PopActiveObject()

		--Next.
		i = i + 1

	end

end

--Get the Pairanormal spine path.
local sPairanormalBasePath = VM_GetVar("Root/Paths/System/Startup/sPairanormalPath", "S")
local sSpinePath = sPairanormalBasePath .. "Spine/"

--Mari
Dia_CreateCharacter("Mari")
    sImgName = "Mari"
    sLower = "mari"
    DiaChar_SetProperty("Add Expression", "Expression0", sSpinePath .. sImgName .. "/Expression0", "Root/Images/Characters/" .. sImgName .. "/Expression0", "mari_n", "mari_sad")
    DiaChar_SetProperty("Add Expression", "Expression1", sSpinePath .. sImgName .. "/Expression1", "Root/Images/Characters/" .. sImgName .. "/Expression1", "mari_mad", "mari_shock")
    DiaChar_SetProperty("Add Expression", "Expression2", sSpinePath .. sImgName .. "/Expression2", "Root/Images/Characters/" .. sImgName .. "/Expression2", "mari_blush", "mari_happy")
    DiaChar_SetProperty("Remap Emotion", "Neutral", "Expression0", "mari_n")
    DiaChar_SetProperty("Remap Emotion", "Sad",     "Expression0", "mari_sad")
    DiaChar_SetProperty("Remap Emotion", "Mad",     "Expression1", "mari_mad")
    DiaChar_SetProperty("Remap Emotion", "Shock",   "Expression1", "mari_shock")
    DiaChar_SetProperty("Remap Emotion", "Shocked", "Expression1", "mari_shock")
    DiaChar_SetProperty("Remap Emotion", "Happy",   "Expression2", "mari_happy")
    DiaChar_SetProperty("Remap Emotion", "Blush",   "Expression2", "mari_blush")
DL_PopActiveObject()

-- |[Character Listing]|
--Build a list of characters and their expression lists.
local csaNameList   = {"AJ", "AJBackpack", "Bret",  "Cellphone", "Cop", "Eilette", "ElectronicDiary", "Kane", "Laura", "MariHospital", "Michelle", "Stacy", "StacyHospital", "Todd", "Walkie"}
local csaLowerList  = {"aj", "ajbackpack", "bret",  "cellphone", "cop", "eilette", "electronicdiary", "kane", "laura",         "mari", "michelle", "stacy", "stacy",         "todd", "walkie"}
local cfaWidthList  = { 293,          293,    319,          263,   330,       338,               518,    380,     241,            261,        261,     285,     271,            271,      178}
local cfaHeightList = {   0,            0,      0,            0,     0,         0,                 0,      0,       0,              0,          0,       0,       0,              0,        0}

--List of the expressions in the order they are stored in the file. It's not the same for each character.
local csaAJList = {"blush", "mad", "happy", "n", "sad", "shock"}
local csaAJBackpackList = {"animation"}
local csaBretList = {"mad", "shock", "happy", "n", "sad", "blush"}
local csaCellphoneList = {"animation"}
local csaCopList = {"n"}
local csaDiaryList = {"animation"}
local csaEiletteList = {"happy", "mad", "blush", "n", "sad", "shock"}
local csaKaneList = {"n"}
local csaLauraList = {"blush", "happy", "mad", "n", "sad", "shock"}
local csaMariHospitalList = {"blush", "happy", "mad", "shock", "sad", "n"}
local csaMichelleList = {"happy", "shock", "blush", "mad", "sad", "n"}
local csaStacyList = {"blush", "happy", "mad", "shock", "n", "sad"}
local csaStacyHospitalList = {"blush", "happy", "mad", "shock", "n", "sad"}
local csaToddList = {"blush", "sad", "mad", "shock", "n", "happy"}
local csaWalkieList = {"animation"}
local czaEmotionList = {csaAJList, csaAJBackpackList, csaBretList, csaCellphoneList, csaCopList, csaEiletteList, csaDiaryList , csaKaneList, csaLauraList, csaMariHospitalList, csaMichelleList, csaStacyList, csaStacyHospitalList, csaToddList, csaWalkieList}

--Run it.
fnCreateCharacter(csaNameList, csaLowerList, czaEmotionList, cfaWidthList)

-- |[Remap]|
Dia_SetProperty("Add Character Alias", "AJ", "Dog")
Dia_SetProperty("Add Character Alias", "AJBackpack", "Dog")
