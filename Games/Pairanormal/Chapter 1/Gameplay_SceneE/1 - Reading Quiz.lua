--[1 - Reading Quiz]
--You have met your match this day, Todd!

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--[Checkpoint]
--Save the game.
if(PairanormalLevel_IsLoading() == false) then
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Starting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

--Set data.
else
	PairanormalLevel_FlagCheckpoint("Starting")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Kitchen")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Kitchen")
	fnDialogue([[ [Music|Daytime][SHOWCHAR|Todd|Neutral]Thought: ... ]])
end

--Flag.
PairanormalLevel_FlagCheckpoint("Starting")

--[Execution]
--Captain Kirk never gave up, and neither will you!
if(sTopicName == "Read") then
	fnDialogue([[Player: Um...[P] First word...[P] Ouy. Ouaaay. ]])
	fnDialogue([[Todd: [E|Neutral]Close.[P] Keep trying.]])
	fnDialogue([[Player: Oauy.[P] Ayuooo?]])
	fnDialogue([[Thought: Blood rises to my cheeks.[P] I can feel Todd's eyes on me.]])
	fnDialogue([[Player: Oy,[P] oyyy... Wait.[P] Oh, wait![P] \"You!\"]])
	fnDialogue([[Todd: [E|Happy]Yep! nice!]])
	fnDialogue([[Thought: I look at the rest of the dizzyingly long paragraph.[P] Oh boy.]])
	fnDialogue([[Todd: [E|Blush]Heh, This one might be a little too long.[P][E|Neutral] Why don't we try something simpler?]])
	
--Captain Kirk whined a lot, and so shall you!
elseif(sTopicName == "Whine") then
	fnDialogue([[Player: Ahh...[P]I'm sorry, this one looks kind of tough.[P] I can already tell it's too much for me.]])
	fnDialogue([[Todd: [E|Neutral]Okay, sure![P] Let's try something simpler.]])

--Captain Kirk hated Todd, and so should you!
elseif(sTopicName == "ScrewYouTodd") then
	fnDialogue([[Player: Heh, um...[P] are we sure this is in English?]])
	fnDialogue([[Todd: [E|Happy]Pf'hahahaha![P][P][P] Sorry, this is cut from a book I like.[P][E|Blush] I've been told my tastes are... kinda esoteric?]])
	fnDialogue([[Todd: [E|Neutral] But you're right, this definitely isn't for everyone.[P] Let's try a different passage.]])

end

--Dialogue rejoins here.
fnDialogue([[Thought: Todd flips to the next passage.[P] I can tell that the text size is larger, and the words aren't as complex.]])
fnDialogue([[Thought: [ITALICS]Hmunas. Atlgoetehr a cnotardctioyr and edelpy aflwed secpeis.[ENDITALICS] ]])
fnDialogue([[Thought: [ITALICS]And eyt... adn eyt, smeohwo I kenw taht teyh rpreesetned teh bset ohpe of teh gaxaly. Phrepas teh olny ophe.[ENDITALICS] ]])

--Decision sequence.
fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "(What should I do?)") ]])

local sDecisionScript = "\"" .. gsRoot .. "Gameplay_SceneE/2 - Reading Quiz The Second.lua\""
fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Try to read it anyway\", " .. sDecisionScript .. ", \"Read\") ")
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Complain\",  " .. sDecisionScript .. ", \"Whine\") ")
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Try to distract Todd\",  " .. sDecisionScript .. ", \"ScrewYouTodd\") ")
fnCutsceneBlocker()
