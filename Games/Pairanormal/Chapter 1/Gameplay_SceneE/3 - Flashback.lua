--[3 - FLASHBACK]
--Damn you Ming the Merciless!
local sTopicName = "No Topic"

--[Checkpoint]
--Save the game.
if(PairanormalLevel_IsLoading() == false) then
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Starting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

--Set data.
else
	PairanormalLevel_FlagCheckpoint("Starting")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Forest")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Forest")
end

--Flag.
PairanormalLevel_FlagCheckpoint("Starting")

--Gallery unlock.
fnCutsceneBlocker()
fnUnlockGallery("Gallery_Forest")

--[Dialogue]
fnDialogue([[[BG|Root/Images/Backgrounds/All/Forest][SHOWCHAR|Eilette|Neutral]Thought: [Music|Null]I'm [GLITCH]at the edge of a wooded grove a few miles outside of town.[P] Eilette opens the passenger car door, and I peer out cautiously.]])
fnDialogue([[Player: [P][P][P]What are we doing here?]])
fnDialogue([[Eilette: [E|Neutral]Something difficult.]])
fnDialogue([[[HIDEALL]Thought: I watch as she unfolds a wheelchair from the trunk.[P] In the month after I'd been released from the hospital, it was my main method of transportation.]])
fnDialogue([[Thought: Sliding it up to me, she waits as I tediously position myself into it.]])
fnDialogue([[Thought: Together we survey the hills.[P] The fall breeze is cool but not brisk.[P] It lazily whips around all the tall trees, pulling off tendrils of red and purple leaves.]])
fnDialogue([[Player: Looks kind of nice.]])
fnDialogue([[[SHOWCHAR|Eilette|Neutral]Eilette: [E|Neutral]I enjoy seasons, aesthetically.[P][E|Shock] The amount of money I have to spend protecting myself from them is another matter.]])
fnDialogue([[Player: You [ITALICS]do[ENDITALICS] wear a lot of coats, don't you?]])
fnDialogue([[Thought: She grabs the handles of my wheelchair and begins pushing me up the rocky incline.]])
fnDialogue([[Thought: On pavement, I'd gotten pretty good at rolling around, but my chair just wasn't equipped for nature walks.]])
fnDialogue([[Thought: After a few short minutes, we're at the crest of the hill.]])
fnDialogue([[Eilette: [E|Shock]Now [PLAYERNAME], I trust you've been doing your physical therapy.]])
fnDialogue([[Player: What?[P] The leg thing?[P] Oh... yeeeaaah. totally.[P] Day and night.]])
fnDialogue([[Eilette: [E|Happy]Excellent.[P] In that case, its time you started advancing to the next level.]])
fnDialogue([[Player: What... like... actually walking?[P] I--uhh, I c-can't do that!]])
fnDialogue([[Eilette: [E|Shock]Astonishingly, you can.[P][E|Neutral] The doctors have told me that despite your extended inactivity while in the coma...]])
fnDialogue([[Eilette: ... For whatever reason, you haven't lost muscle mass or experienced any sort of the typical deterioration.]])
fnDialogue([[Eilette: [E|Shock] By all accounts, you are ambulatory.]])
fnDialogue([[Player: O-okay, maybe that's what the [ITALICS]doctors[ENDITALICS] say but...[P] I'm not,[P] like,[P] ready.[P][P][P][CRASH] I'm not ready to walk!]])
fnDialogue([[Eilette: [E|Mad]Yes, I anticipated that.[P] But unfortunately we just don't have time to waste.]])
fnDialogue([[Eilette: [E|Neutral] So,[P][P][P][E|Sad] I've devised a method of speeding up the process.]])
fnDialogue([[Player: Wha--?]])
fnDialogue([[[HIDEALL]Thought: Eilette takes a few steps forward.[P] To my horror, [Music|Tension]I watch her draw a revolver from the inside of her coat.]])

--Gallery unlock.
fnCutsceneBlocker()
fnUnlockGallery("Gallery_Scene_EiletteForest")
fnDialogue([[[IMAGE|Root/Images/MajorScenes/All/EiletteKapow]Player: [CRASH]Oh my god,[P] [ITALICS]is that a gun!?![ENDITALICS] ]])
fnDialogue([[Eilette: Ah, good, you recognize this.[P] I think the effectiveness of this exercise would be lessened if I had to explain what it is.]])
fnDialogue([[Player: Exercise?[P] What are you talking about--]])
fnDialogue([[Eilette: Now, before I begin, I want to assure you that these are loaded with fairly harmless pellets.[P] Furthermore, I'm expertly trained in using this weapon.]])
fnDialogue([[Eilette: I will never actually aim at you.]])
fnDialogue([[Player: [SPEEDTEXT]Wait a minute, wait a minute,[P] Eilette--[ENDSPEEDTEXT] ]])
fnDialogue([[Eilette: I will, however, for the sake of realism, [ITALICS]aim very, very close.[ENDITALICS] ]])
fnDialogue([[[IMAGE|Null]Info: [GUNSHOT]BANG!!!]])
fnDialogue([[Thought: The next few moments feel like a white blur.[P] As a \"harmless pellet\" slices through the wheel of my chair, I throw myself to the ground.]])
fnDialogue([[Player: Oh my god![P] [ITALICS]OH MY GOD!![ENDITALICS] ]])
fnDialogue([[Eilette: I apologize for having to resort to this,[P] [PLAYERNAME],[P] but I need to be certain that you can walk or even run from danger if you're ever confronted with it.]])
fnDialogue([[Info: [GUNSHOT]BANG!!!]])
fnDialogue([[Thought: I claw desperately at the ground, heaving myself forward by my elbows.]])
fnDialogue([[Player: WHEN in my life will I ever have to run from from someone [ITALICS]SHOOTING AT ME!?!?[ENDITALICS] ]])
fnDialogue([[Eilette: ... You'd be surprised.]])
fnDialogue([[Info: [GUNSHOT]BANG!!!]])
fnDialogue([[Thought: My heart is pounding. I want to scream, but it feels like all the air has left my lungs.]])
fnDialogue([[Thought: I can only pull myself forward, sprawling from tree to tree, desperately trying to get away.]])
fnDialogue([[Info: [GUNSHOT]BANG!!!]])
fnDialogue([[Player: S-stop![P] Please!]])
fnDialogue([[Info: [GUNSHOT]BANG!!!]])
fnDialogue([[Player: I'm begging you, this is really really scary!]])
fnDialogue([[Info: [GUNSHOT]BANG!!!]])
fnDialogue([[[BG|Null]Player: [CRASH]STO[GLITCH]OOO[CRASH]OOO[CRASH]OOO[CRASH]OP!! ]])
fnDialogue([[Thought: [P][P][P]Slowly, the ringing in my ears recedes.[P] I notice a flock of birds taking off from the treetops, disappearing into the distant hills.]])
fnDialogue([[Thought: [P][P][P]I realize I'm leaning against a tree, all alone and shivering.]])
fnDialogue([[[BG|Root/Images/Backgrounds/All/Forest]Thought: Little by little, the world slows down again.[P] I see Eilette emerge from the trees, gun in tow.]])
fnDialogue([[[SHOWCHAR|Eilette|Happy]Eilette: [Music|Null][E|Happy]Excellent work, [PLAYERNAME].]])
fnDialogue([[Eilette: [E|Neutral]Your running speed isn't quite up to par, so I would recommend in the future that you run in a zig-zag pattern to reduce the likelihood of being hit.]])
fnDialogue([[Player: A zig-zag pattern. [P][P][P][CRASH]Are you... f-freaking crazy?!? [P]I [P]could [P]have [P]died.]])
fnDialogue([[Eilette: [E|Neutral]Yes, you could have.[P] That was the point of the exercise.]])
fnDialogue([[Player: No, I mean, you were literally about to kill me! ]])
fnDialogue([[Thought: Eilette reaches for my shoulder, but I instinctively slap it away, snarling. ]])
fnDialogue([[Player: [CRASH]How could you DO this!?!?[P][P] Do you have any idea how I[P].[P].[P].]])
fnDialogue([[Player: [CRASH]YOU ARE THE ONLY PERSON I TRUST!]])
fnDialogue([[Thought: At this Eilette positively recoils, shocked.]])
fnDialogue([[Player: Trust-ED.[P] Past tense.[P] That's definitely over with now. ]])
fnDialogue([[Thought: She looks down at her gun, shoving it in her pocket uncharacteristically sheepishly.]])
fnDialogue([[Thought: I hug the tree tighter. ]])
fnDialogue([[Eilette: [E|Blush].[P].[P].[P]You have a point there.[P] I suppose I didn't really consider how potentially traumatic this could be.[P] I'm sorry.]])
fnDialogue([[Thought: I close my eyes. As the wind whoops through the autumn branches, I dig my fingers into the bark, daring anyone to pull me away. ]])
fnDialogue([[Eilette: [E|Sad]I'm simply trying to demonstrate... [E|Blush]Well, this is a planet of rules, [PLAYERNAME].]])
fnDialogue([[Eilette: Several bizarre, precise rules that it expects you know and follow.]])
fnDialogue([[Eilette: [E|Mad]Someone in your disadvantaged position, who hasn't quite learned how to live here,[P] but is expected to survive,[P] will suffer if they don't learn.]])
fnDialogue([[Eilette: [E|Neutral] That's why you have to try.[P][P] [E|Blush]You NEED to actually try.]])
fnDialogue([[Thought: I sob hoarsely.]])
fnDialogue([[Player: But why?[P][CRASH] [ITALICS]Why???[ENDITALICS] ]])
fnDialogue([[Player: What's the point of going through this?[P] What's my reward for...[P] for putting up with being miserable all the time?]])
fnDialogue([[Eilette: [E|Sad].[P].[P].[P] Hm. ]])
fnDialogue([[Thought: She shrugs.]])
fnDialogue([[Eilette: [E|Shock]That's a very good question.]])
fnDialogue([[[HIDEALL]Thought: My so-called caretaker turns and starts making her way to the car.[P] I screech.]])
fnDialogue([[Thought: I want to say bad words at her, but [GLITCH]of course I don't even know any[GLITCH]. ]])
fnDialogue([[[BG|Null]...]])

LM_ExecuteScript(gsRoot .. "Gameplay_SceneE/4 - Pizzatime.lua")
