--[2 - Reading Quiz]
--The sequel nobody asked for!

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--[Checkpoint]
--Save the game.
if(PairanormalLevel_IsLoading() == false) then
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Starting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

--Set data.
else
	PairanormalLevel_FlagCheckpoint("Starting")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Kitchen")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Kitchen")
	fnDialogue([[ [Music|Daytime][SHOWCHAR|Todd|Neutral]Thought: ... ]])
end

--Flag.
PairanormalLevel_FlagCheckpoint("Starting")

--[Execution]
--Captain Picard never gave up, and neither will you!
if(sTopicName == "Read") then
	fnDialogue([[Player: Whew. okay. first word. Mmm... muhnas. Muhhhh--]])
	fnDialogue([[Thought: [ITALICS]Please, universe...[P] if anyone is out there, if anyone is listening who can help... [P]let me die in a freak accident so I don't have to read this stupid paragraph![ENDITALICS] ]])
	fnDialogue([[Todd: [E|Neutral]Try starting with the \"h\" ]])
	fnDialogue([[Player: Hhh...[P] Hmmm.[P] Hum... nas.[P] Humnas.[P] Humnas.[P] ]])
	fnDialogue([[Player: Oh! H-humans?[P] Is it humans?]])
	fnDialogue([[Todd: [E|Happy]Yes! That's the first word! ]])
	fnDialogue([[Player: [SFX|World|Ugh][ITALICS]Yaaaaaayyy.[ENDITALICS] ]])
	fnDialogue([[Todd: [E|Blush]Okay, you're doing great, but I actually want to find something you can REALLY read.[P] Just... power right through, y'know?]])
	
--Captain Picard said 'Engage' a lot.
elseif(sTopicName == "Whine") then
	fnDialogue([[Player: Okay, this one looks good![P] I can definitely...[P] almost -probably- read this,[P] maybe.[P] ]])
	fnDialogue([[Player: Umm...[P] c-can you make the letters bigger somehow?]])
	fnDialogue([[Todd: [E|Shock]Oh, interesting.[P][E|Mad] Uhhh, yeah, let me see here...]])

--Remember the episode where Captain Picard hated Todd? Best episode of the series.
elseif(sTopicName == "ScrewYouTodd") then
	fnDialogue([[Player: Ah yes, this is much better.[P] *ahem*]])
	fnDialogue([[Player: \"Today was our 50th day orbiting earth. Other is getting antsy, and is eager to set down on the planet, but I--\"]])
	fnDialogue([[Todd: [E|Happy]Pf'hahahaha! [P]That's not quite what it says.]])
	fnDialogue([[Player: But isn't what I'm saying much more interesting?]])
	fnDialogue([[Todd: [E|Neutral]Kind of.[P] [E|Mad]Come on, you don't have to be nervous -- I'm here to help you, not to judge you. [E|Neutral] Look, why don't you try this?]])

end

--Dialogue rejoins here.
fnDialogue([[Thought: Todd flips a few pages back in the packet to a new passage.[P] This one is only a short phrase.]])
fnDialogue([[Thought: The letters are so large they take up the entire page. I sink into my chair.]])
fnDialogue([[Thought: [ITALICS]\"I OD ONT IKLE EGREN GGES DNA AHM. I OD ONT IKLE EMTH, ASM I AM\"[ENDITALICS] ]])
fnDialogue([[Thought: I look desperately to the accompanying picture for some clue, but it's so absurd it hardly helps.]])
fnDialogue([[Player: [SFX|World|Ugh]Uhh...[P] Yeah,[P] I don't think I can read. ]])
fnDialogue([[Todd: [E|Shock]Oh come on.[P][E|Mad] You're pulling my leg, right?]])
fnDialogue([[Player: I know my letters, I think.[P] But...[P] they're all jumbled up on the page.]])
fnDialogue([[Todd: [E|Mad]Oh.[P][P][P][E|Shock] Ohhhhh.[P][P][E|Blush] Are you,[P] um,[P] dyslexic?]])
fnDialogue([[Player: Yes.[P] No...[P] what does dyslexic mean?]])
fnDialogue([[Thought: Todd is silent for a long time.[P] He just looks down at the table, his thick brows furrowed in concentration.]])
fnDialogue([[Todd: [Music|Somber][E|Sad]Huh.[P] Uh, [PLAYERNAME], please don't take this the wrong way, but...[P] are the rumors about you...[P] actually true?]])
fnDialogue([[Player:... [ITALICS]What rumors[ENDITALICS]?]])
fnDialogue([[Thought: He winces -- I'm not sure if it's because he's embarrassed about what he just said,[P] or what he's about to.]])
fnDialogue([[Todd: [E|Sad][ITALICS]Are you maybe[P] possibly[P] a recently revived comatose person with no memory of who they are or where they came from??[ENDITALICS] ]])
fnDialogue([[Thought: A deranged giggle escapes me.[P] Michelle was right-- news really does travel [ITALICS]fast[ENDITALICS]. ]])
fnDialogue([[Player: ... Is it bad if I say \"yes\"?]])
fnDialogue([[Todd: [E|Shock]... Ah.[P] Whoooo.[P] Damn.]])
fnDialogue([[Player: Yep.[P] I've been getting that a lot lately.]])
fnDialogue([[Thought: Todd slowly stands up.]])
fnDialogue([[Todd: [E|Sad][PLAYERNAME], you're a very special person.[P] You're real fun to hang out with, as a pal, but...[P][E|Mad] I honestly don't think I can tutor you, as is.]])
fnDialogue([[Todd: [E|Sad] I-I think you need a professional. ]])
fnDialogue([[Player: Yeah, you're probably right.]])
fnDialogue([[Player: I mean, Eilette hired a fellow student to tutor me so you could answer my questions about student life, because she thought I wouldn't know how to make friends.]])
fnDialogue([[Todd: [E|Sad]Ah, I see.]])
fnDialogue([[Player: But I already made friends today.[P] I joined a club and everything.[P] So I don't need your help.[P] I can figure it out on my own.]])
fnDialogue([[Todd: [E|Sad][P][P][P]Right.]])
fnDialogue([[Player: I don't need you or anyone.[P] I'm just fine with the way things are.[P] I didn't even want a tutor in the first place.]])
fnDialogue([[Thought: The doorbell rings.]])
fnDialogue([[Todd: [E|Shock][CRASH] O-Oh, pizza!]])
fnDialogue([[Thought: At speeds surpassed only by light itself, Todd snatches the money on the counter and dashes to the door.]])
fnDialogue([[[HIDEALL]Thought: If he knows what's good for him,[P][GLITCH] he'll never return.]])
fnDialogue([[Thought: I bury my head in my arms, soaking up[GLITCH] the tears as another painful memory surfaces.[GLITCH] [BG|Null] ]])

LM_ExecuteScript(gsRoot .. "Gameplay_SceneE/3 - Flashback.lua")
