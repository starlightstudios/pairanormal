--[5 - Flashback Again]
--The best kind of time.
local sTopicName = "No Topic"

--[Checkpoint]
--Save the game.
if(PairanormalLevel_IsLoading() == false) then
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Starting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

--Set data.
else
	PairanormalLevel_FlagCheckpoint("Starting")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Null")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Null")
end

--Flag.
PairanormalLevel_FlagCheckpoint("Starting")

--[Dialogue]
--I hope that pizza gave Todd indigestion.
fnDialogue([[Player: Huhhh-hot!]])

--Gallery unlock.
fnCutsceneBlocker()
fnUnlockGallery("Gallery_Burning")
fnDialogue([[Thought:[Music|Tension] My eyes snap open.[BG|Root/Images/Backgrounds/All/Burning][P] I feel flames crackling and leering all around me--a burning building.[P] How did I get here?!?]])
fnDialogue([[Man: [Voice|Voice|Kane]*cough* cough*]])
fnCutsceneBlocker()
fnUnlockGallery("Gallery_Scene_SpoilerFire")
fnDialogue([[[IMAGE|Root/Images/MajorScenes/All/KaneFire]Thought: I look down, and to my horror see a frail, thin stranger held fast by my powerful fists.]])
fnDialogue([[Thought: It's then that I realize something is wrong with my body -- I'm huge, bulky, muscular.]])
fnDialogue([[Thought: A grown, hairy man.]])
fnDialogue([[Thought: It feels like a memory, a real one![P] But I definitely don't recognize anything here.]])
fnDialogue([[Thought: [ITALICS]Could this somehow be... from before my coma?[ENDITALICS] ]])
fnDialogue([[Player: Where.[P].[P].[P] is.[P].[P].[P] Other?]])
fnDialogue([[Thought: My words come out slowly, painfully.[P] For some reason, they're hard to form.]])
fnDialogue([[Thought: The man looks at me, terrified and confused. Then, something dawns on him. ]])
fnDialogue([[Man: [Voice|Voice|Kane]My god... Subject One, is that you?[P] Did you actually jump? Did you survive?]])
fnDialogue([[Player: [CRASH]Other!!![P][CRASH] Where is Other?!?]])
fnDialogue([[Man: [Voice|Voice|Kane]It really is you, isn't it?[P] I can't believe it... so, our theory was right.]])
fnDialogue([[Man: [Voice|Voice|Kane]You are actually capable of... of this![P] Where did you get this body? ]])
fnDialogue([[Thought: I let out an anguished moan, and slam the man against the wall again.]])
fnDialogue([[Player: [CRASH]S[GLITCH]TOP![P] Answer... m-[P]my...[P] [P][P][CRASH]question!]])
fnDialogue([[Man: [Voice|Voice|Kane]Relax, relax, One...[P] *cough* [P]*cough*...[P] If you're referring to Subject Two, he is alive and well.]])
fnDialogue([[Player: [CRASH]WHERE.[P][CRASH] WHERE IS--]])
fnDialogue([[Man: [Voice|Voice|Kane]I'm not going to tell you where he is! Do you have any idea-- *cough* --how important these experiments are?]])
fnDialogue([[Man: [Voice|Voice|Kane]We are close to the biggest breakthrough in human history--]])
fnDialogue([[Player: NO!!![P] STOP![P] No more breakthrough.[P][P][P][CRASH] NO MORE BREAK US.]])
fnDialogue([[Info: CRACK!!!]])
fnDialogue([[Thought: A chilling snap is heard as I slam him once more. The man's right arm now dangles limply as he screams in pain.]])
fnDialogue([[Thought: I can't tell if the heat on my face is from growing anger, or the the gradually advancing flames.]])
fnDialogue([[Man: [Voice|Voice|Kane]*wheeze*[P] *wheeze*[P] You.[P].[P].[P] [ITALICS]bastard.[ENDITALICS] ]])
fnDialogue([[Thought: I'm startled as I feel him grab me right back.]])
fnDialogue([[Man: [Voice|Voice|Kane]You and your... whatever the both of you are, are MONSTERS.]])
fnDialogue([[Man: [Voice|Voice|Kane]My brother is dead thanks to you; I would never release your like back into the world after [ITALICS]what you've done.[ENDITALICS] ]])
fnDialogue([[Man: [Voice|Voice|Kane]And you've made a fatal error coming back here to try and confront me,[P] because now I won't rest until you're locked right back up with Two, or DEAD!!!]])
fnDialogue([[Man: [Voice|Voice|Kane][CRASH] DEAD, you hear me?]])
fnDialogue([[[IMAGE|Null]Thought: The flames roar and crackle.[P] My grip tightens.]])
fnDialogue([[Player: You think.[P].[P].[P] I am afraid of you?]])
fnDialogue([[Thought: I reach up to grab his face, swinging it down low to the flames.]])
fnCutsceneBlocker()
fnUnlockGallery("Gallery_Scene_SpoilerFire2")
fnDialogue([[[IMAGE|Root/Images/MajorScenes/All/KaneFire2]Player: I am ... special.[P] Other is special.[P] I am...[P] not the one...[P]who is afraid.]])
fnDialogue([[Thought: The licking sparks dance dangerously close to his battered cheek.[P] I can feel his body shuddering in terror.]])
fnDialogue([[Player: I am not the one...[P] who made the mistake. ]])
fnDialogue([[Man: [Voice|Voice|Kane]W-wait![P] Stop![P][CRASH] Please!]])
fnDialogue([[Player: I will not rest.[P] I will never rest.]])
fnDialogue([[Thought: I hear a door slam behind me.]])
fnDialogue([[Police: [Voice|Voice|Police][CRASH]POLICE![P] [CRASH]HANDS IN THE AIR!]])
fnDialogue([[Man: [Voice|Voice|Kane][SPEEDTEXT][CRASH]KILL HIM! FOR THE LOVE OF GOD! HE'S CRAZY![ENDSPEEDTEXT] ]])
fnDialogue([[Player: I w-will never leave...[P] until yuhh... [P]you PAY.]])
fnDialogue([[Man: [Voice|Voice|Kane][CRASH]HELP MEEEEEE!]])
fnDialogue([[Info: [IMAGE|Null][GUNSHOT]BANG!]])
fnDialogue([[Thought: I feel a blast of cool air, followed by searing pain in my left rib.[GLITCH][GLITCH] My vision goes blank.]])
fnDialogue([[Thought: Suddenly, I'm in another corner of the room, watching the hulking, muscular body that pinned the stranger to the wall crumple to the ground, a bullet in his back.]])
fnDialogue([[[SHOWCHAR|Kane|Neutral]Thought: The once captive man leaps up from the flames, cradling his face.[P] He looks around, dazed.]])
fnDialogue([[[SHOWCHAR|Cop|Neutral]Officer: [Voice|Voice|Police][Focus|Cop]We got him sir![P] Quickly, come with me![P] This building could collapse any second now!!]])
fnDialogue([[Man: [Focus|Kane][Voice|Voice|Kane][E|Neutral]No... wait.[P] I've seen this before![P] This isn't...]])
fnDialogue([[Thought: The police officer turns to me.]])
fnDialogue([[Officer: [Voice|Voice|Police][Focus|Cop][E|Neutral]Detective Rodriguez, secure the area until the fire department gets here.[P] I'm gonna clear this guy out.]])
fnDialogue([[Thought: I stare blankly.]])
fnDialogue([[Thought: The stranger's eyes snap to me as the officer approaches him.[P] He sucks in his breath in shock.]])
fnDialogue([[Man: [Focus|Kane][Voice|Voice|Kane]It's you, isn't it? You jumped![P][CRASH] YOU JUMPED![P] I SAW IT!!!]])
fnDialogue([[Officer: [Voice|Voice|Police][Focus|Cop]Sir, you have to leave, it is not safe to--]])
fnDialogue([[Man: [Focus|Kane][Voice|Voice|Kane]Officer, arrest this man![P] He-- I... He jumped, don't you see?[P][CRASH][ITALICS] This man is an imposter![ENDITALICS] ]])
fnDialogue([[Thought: Frustrated, the officer scoops up the man's frail body and proceeds to drag him out.[P] I make my way towards the back entrance.]])
fnDialogue([[Man: [Focus|Kane][Voice|Voice|Kane]This isn't over, One![P] I [GLITCH]will hunt you down, you hear me?[P] I will n[GLITCH]ot rest!]])
fnDialogue([[[HIDEALL]Man: THIS[GLITCH][P] ISN'T[P][GLITCH] OVER!!!!!![GLITCH]!![GLITCH][GLITCH][GLITCH] ]])
fnDialogue([[Thought:[CHAREXIT|ALL][IMAGE|Null][BG|Null][Music|Null]...]])

LM_ExecuteScript(gsRoot .. "Gameplay_SceneE/6 - Credits.lua")
 