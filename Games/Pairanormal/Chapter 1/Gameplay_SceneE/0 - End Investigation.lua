--[0 - End Investigation]
--All done, sucka!

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--[Execution]
--Start.
if(sTopicName == "Default") then
	
	--Question:
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALL]Thought: (Really end the investigation?)") ]])

	local sDecisionScript = "\"" .. gsRoot .. "Gameplay_SceneE/0 - End Investigation.lua\""
	fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"All Done\", " .. sDecisionScript .. ", \"Bypass\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Not Yet\",  " .. sDecisionScript .. ", \"Dont\") ")
	fnCutsceneBlocker()
	return
	
--Don't go home just yet.
elseif(sTopicName == "Dont") then

	--Resume investigation mode.
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()
	return
end

--Scene setup.
fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
fnCutsceneBlocker()
	
--Save the game.
if(PairanormalLevel_IsLoading() == false) then
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Starting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

--Set data.
else
	PairanormalLevel_FlagCheckpoint("Starting")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/LivingRoom")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/LivingRoom")
	fnDialogue([[ [Music|Daytime]Thought: ... ]])
end

--Flag.
PairanormalLevel_FlagCheckpoint("Starting")

--Set the investigation time max to -1. This stops it from appearing on the screen.
Dia_SetProperty("Investigation Time Max", -1)

--Dialogue.
fnDialogue([[[HIDEALL]Thought: [CRASH]The doorbell rings.[P] I jump like an arrow strung up on a bow of nerves.]])
fnDialogue([[[SHOWCHAR|Eilette|Neutral]Eilette: [E|Neutral]I'll get it.]])
fnDialogue([[[HIDEALL]Thought: She disappears to the front door, and I briefly consider making a break for it.]])
fnDialogue([[???: [Voice|Voice|Todd]Uhh, hey.[P] Ms. Mira?]])
fnDialogue([[Eilette: Greetings, Todd.[P] Your pupil eagerly awaits you in the dining room.]])
fnDialogue([[Todd: [P].[P].[P].right.]])
fnDialogue([[Thought: [ITALICS]Right![ENDITALICS] ]])

--Gallery unlock.
fnCutsceneBlocker()
fnUnlockGallery("Gallery_Kitchen")
fnDialogue([[Thought: [BG|Root/Images/Backgrounds/All/Kitchen]I rush into the dining room and take a seat. I hear them crossing the living room, heading to me.]])
fnDialogue([[Eilette: --kind of you to take time out of your day for this.]])
fnDialogue([[Todd: Well, you are paying me a small fortune, which I'd be kind of crazy to turn down.]])
fnDialogue([[Todd: Again, I hate to keep bringing this up, but are you sure you wouldn't want to lower my rate? ]])
fnDialogue([[Eilette: It's really no trouble, I have so much money.[P] Can I offer you a refreshment? ]])
fnDialogue([[Thought: Should the table look like this?[P] I feel like I need a notebook or a pen or something.[P] [ITALICS]What do I do with my hands?[ENDITALICS] ]])
fnDialogue([[Todd: Hey. You must be [PLAYERNAME], right? ]])
fnDialogue([[[SHOWCHAR|Todd|Neutral][SHOWCHAR|Eilette|Neutral]Thought: I look up at the entryway to the dining room.[P] A slim young man clutches a stack of books, his brown eyes darting back and forth.]])
fnDialogue([[Player: Greetings![P] I'm [PLAYERNAME]--[P]I mean, yes, that's me.]])
fnDialogue([[Eilette: [E|Neutral][PLAYERNAME], this is Todd, your tutor. He's one of the brighter young minds at your school.]])
fnDialogue([[Eilette: [E|Shock] Todd, for the first few tutoring sessions I'd like you to focus on assessing [PLAYERNAME]'s aptitude in their various subjects.]])
fnDialogue([[Todd: [E|Blush]Yeah, I've got some stuff drawn up.[P][E|Neural] We'll be taking a look at English and Math today--]])
fnDialogue([[Thought: He looks back at me, his lips quirking up into a playful smile.]])
fnDialogue([[Todd: [E|Happy]Sound good?]])
fnDialogue([[Eilette: [E|Happy]Excellent. I will leave you two alone.]])
fnDialogue([[Eilette: [E|Neutral][PLAYERNAME], shortly, a \"pizza\" will arrive at the house,[P] which I understand is the traditional food eaten at study-related events.]])
fnDialogue([[Eilette: The money to pay for it is on the kitchen counter. ]])
fnDialogue([[Eilette: [E|Mad]Please remember to wash your face after eating it, as it is high in lipid content, which blemishes the complexion.]])
fnDialogue([[[HIDECHAR|Eilette]Thought: Eilette retires to her office. Todd watches her leave like a hawk, then darts to the table, plopping his books down in the center.]])
fnDialogue([[Todd: [E|Mad]Okay, just tell me now--what the heck is going on? Am I about to get murdered?]])
fnDialogue([[Player: W-what?]])
fnDialogue([[Todd: [E|Neutral]I don't know if I should be telling you this but your mom paid me a TON of money to tutor you, like, I can go to any college I want now money. ]])
fnDialogue([[Todd: [E|Shock]I'm kinda freaking out because I think the whole thing must be a scam or something and I'm about to get murdered... ]])
fnDialogue([[Todd: [E|Neutral]OR the person I'm tutoring is some kind of mutant chained to the basement like Sloth from the Goonies--]])
fnDialogue([[Thought: He looks at me, brows raised.]])
fnDialogue([[Todd: [E|Neutral]--And you're obviously not the latter, so like...[E|Mad] what's the deal?]])
fnDialogue([[Player: ...[P][SFX|World|Blink]What's the Goonies?]])
fnDialogue([[Thought: Todd looks at you blankly. Then, he bursts out laughing.]])
fnDialogue([[Todd: [E|Happy]Pfahaha! okay, okay, good one.]])
fnDialogue([[Player: Seriously. I have no idea what you're talking about.]])
fnDialogue([[Todd: [E|Shock]Oh. Uhhh.[P] Right. You're serious.[E|Sad] Um![P] *ahem* um... sorry. Right.[P] The Goonies is just a movie.]])
fnDialogue([[Player: Oh. Is it good?]])
fnDialogue([[Todd: [E|Happy]Yeah, it's freaking awesome![P][P][E|Blush] Hm, I mean, well, it's a little sexist, actually.[P][E|Mad] Also there's like, [ITALICS]zero[ENDITALICS] black representation... Hm.[P][SFX|World|Ugh][E|Neutral] It's okay.]])
fnDialogue([[Player: Heh. ]])
fnDialogue([[Todd: [E|Neutral]Anyway, whatever. Let's get to work!]])
fnDialogue([[Thought: Todd rifles through a bookbag he brought along, pulling out a short stack of printed papers on it. ]])
fnDialogue([[Todd: [E|Blush]I don't -really- know what I'm doing here, I mean, I'm good at school and stuff, but I don't have too much experience tutoring.]])
fnDialogue([[Todd: [E|Neutral]I made an aptitude test like Ms. Mira asked-- If it seems weird or too easy or something, just let me know.]])
fnDialogue([[Thought: He slides the stack of papers over. the first page has several cut out passages of text.]])
fnDialogue([[Todd: [E|Neutral]The first part is reading, then after that there's math.[P] Go ahead and read the first passage. Let me know if there's a word you don't understand.]])
fnDialogue([[Thought: [ITALICS]\"ouy onkw hatw I hnkit?\" hse ayse. \"ahtt oplep's emmriose rea ybeam het leuf yeth bnur to ayts livea.\"[ENDITALICS] ]])
fnDialogue([[Player: uhhh...]])
fnDialogue([[Thought: [ITALICS]\"hwehert ohtse emmriose vhae yna aaulct prtoecneima or ton, it odens't ttarme as fra as teh manitencea of flie si oncerednc.\"[ENDITALICS] ]])
fnDialogue([[Thought: [SFX|World|Ugh]As I look through the passage, none of the words particularly leap out at me... that is, are legible at all.[P] [ITALICS]SHOOT SHOOT SHOOT![ENDITALICS] ]])

--Decision sequence.
fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (What should I do?)") ]])

local sDecisionScript = "\"" .. gsRoot .. "Gameplay_SceneE/1 - Reading Quiz.lua\""
fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Try to read it anyway\", " .. sDecisionScript .. ", \"Read\") ")
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Complain\",  " .. sDecisionScript .. ", \"Whine\") ")
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Try to distract Todd\",  " .. sDecisionScript .. ", \"ScrewYouTodd\") ")
fnCutsceneBlocker()
