--[4 - Pizzatime!]
--The best kind of time.
local sTopicName = "No Topic"

--[Checkpoint]
--Save the game.
if(PairanormalLevel_IsLoading() == false) then
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Starting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

--Set data.
else
	PairanormalLevel_FlagCheckpoint("Starting")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Kitchen")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Kitchen")
end

--Flag.
PairanormalLevel_FlagCheckpoint("Starting")

--[Dialogue]
--It'd be better if Todd didn't get any pizza.
fnDialogue([[[BG|Root/Images/Backgrounds/All/Kitchen]Thought: [Music|Daytime]I hear footsteps coming back into the dining room.[P] Todd nervously emerges, pizza in hand.]])
fnDialogue([[[SHOWCHAR|Todd|Neutral]Todd: [E|Blush]... Looks like we're in luck.[P] Your mo-- [P]I mean, your caretake-- [P]Eh, Ms. Mira ordered the supreme.[P][E|Neutral] It's got everything but pineapple on it!]])
fnDialogue([[Thought: He slides it gingerly on the table.]])
fnDialogue([[Todd: [E|Sad]H-hey,[P] are you okay?]])
fnDialogue([[Thought: I look up at him, bleary eyed.[P] I remember the club members' kindness from today, and Michelle's words.]])
fnDialogue([[Thought: [ITALICS]Fuck 'em[ENDITALICS].[P][P][P] I might as well be honest.]])
fnDialogue([[Player: I've never been to school.[P] I've been in a coma most of my life.[P] Nobody knows how I survived or where I came from.]])
fnDialogue([[Player: When I woke up, they... Eilette is my caretaker now.]])
fnDialogue([[Thought: Todd nods.[P] He gestures for me to keep sharing.]])
fnDialogue([[Player: I don't want to feel like a freak.[P] I don't want to be embarassed all the time by everything I do.]])
fnDialogue([[Player: I... NEED someone[P] to just hang out with me and help me figure stuff out...[P]and not judge me. ]])
fnDialogue([[Thought: I feel a hand patting me on the back in a slow, awkward rhythm.]])
fnDialogue([[Todd: [E|Mad]I wish I had something encouraging to say. But it's like, I don't know if anything I can think of would help.]])
fnDialogue([[Todd: [E|Shock] What you're going through...[P]It's like...[P]It must be...[P]damn!]])
fnDialogue([[Player: Yeah. Thanks anyway.]])
fnDialogue([[Todd: [E|Blush].[P].[P].[P][PLAYERNAME]?]])
fnDialogue([[Thought: I realize how close Todd is standing to me.[P] He looks embarrassed and cant meet my eye.[P] I can smell his deodorant.]])
fnDialogue([[Todd: [E|Blush]I'm...[P][E|Happy] Aw, to hell with it. I'm gonna help you catch up, okay?]])
fnDialogue([[Todd: [E|Mad] I mean, it's my job to tutor you but...[E|Neutral]I want to do a good job.[P] You deserve that much.]])
fnDialogue([[Player: Wh--[P]Really?]])
fnDialogue([[Thought: Our eyes meet.[P] He smiles nervously, as though he can't believe it either.]])
fnDialogue([[Todd: [E|Neutral]Anyway, lets have some pizza and relax for a little bit.[P] We've got plenty of time to do this, right?]])
fnDialogue([[Thought: I nod.]])

--Gallery unlock.
fnCutsceneBlocker()
fnUnlockGallery("Gallery_Scene_IntroTodd")
fnDialogue([[[HIDEALL][IMAGE|Root/Images/MajorScenes/All/IntroTodd]Thought: Todd and I sit back down and have a slice. We spend the rest of the night just chatting.]])
fnDialogue([[Thought: Todd is a good listener, and tells lots of jokes to make me feel at ease.]])
fnDialogue([[Thought: I suddenly feel so relieved, and am reminded of all the people I met today.]])
fnDialogue([[[IMAGE|Null]Thought: Maybe I can do this.[P] [ITALICS]Maybe I can make it through High School in one piece.[ENDITALICS] ]])
fnDialogue([[[BG|Null]...]])

--Gallery unlock.
fnCutsceneBlocker()
fnUnlockGallery("Gallery_Bedroom")
fnUnlockGallery("Gallery_BedroomNight")
fnDialogue([[[BG|Root/Images/Backgrounds/All/BedroomNight]Thought:[Music|Somber] It's night time now. ]])
fnDialogue([[Thought: Woof, what a day. I let the solitude of the moment sink in. ]])
fnDialogue([[Thought: The hospital-- those weeks where I did nothing but lie in a bed, dreading every hour I was awake-- feels like a lifetime ago.]])
fnDialogue([[Thought: Now... I smile, thinking of all the friends I've made. I look forward to more hugs with Mari and Stacy.]])
fnDialogue([[Thought: I look forward to Laura's shy smiles. ]])
fnDialogue([[Thought: I look forward to Bret's nervous, rapid jokes. ]])
fnDialogue([[Thought: I look forward to Michelle's bubblegum mishaps. ]])
fnDialogue([[Thought: I look forward to Todd's lessons.]])
fnDialogue([[Thought:[Music|Null] I close my eyes, my mind deliciously quiet and cozy.[P][P][P][GLITCH][BG|Null] ]])

LM_ExecuteScript(gsRoot .. "Gameplay_SceneE/5 - Flashback.lua")