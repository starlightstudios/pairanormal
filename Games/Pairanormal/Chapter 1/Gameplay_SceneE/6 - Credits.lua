--[6 - Credits]
--In this version, immediately goes to the next chapter.
fnDialogue("[CHAPTEREND|Root/Images/Backgrounds/All/EndOfChapterOne] ")
LM_ExecuteScript(gsRoot .. "../Chapter 2/Gameplay_SceneA/0 - Hallway Disaster.lua", "Start")

--[=[
fnDialogue([[Credits: [Voice|Null][BG|Null][Music|Theme]End of Chapter 1]])
fnDialogue([[Credits: Developed by Mike \"Salty Justice\" Upshall and Tess \"Chicmonster\" Young (@thechicmonster)]])
fnDialogue([[Credits: Music by Mag.Lo (maglo.bandcamp.com)]])
fnDialogue([[Credits: Additional score by Bensound.com]])
fnDialogue([[Credits: Special thanks to Mari and Stacy of Geek Remix (youtube.com/user/GeekRemix)]])
fnDialogue([[Credits: Special thanks Laura Kate Dale (@LauraKBuzz)]])
fnDialogue([[Credits: Special thanks Todd Schlickbernd (@Toddly_Enough)]])
fnDialogue([[Credits: Special thanks Bret aka HoodiePanda (youtube.com/user/HoodiePanda)]])
fnDialogue([[Credits: Special thanks Michelle aka MangaMinx (youtube.com/user/TheRPGMinx)]])
fnDialogue([[Credits: And thanks to you for playing our demo! We paid for this game from our own pockets and Patreon fundraising.]])
fnDialogue([[Credits: If you would like to play future chapters, please consider backing us on patreon.com/chicmonster.]])
fnDialogue([[Credits: Thanks to our $10+ Backers:: Alvin DelaCruz, Amelia Sparke, Avley, Brando De Niz]])
fnDialogue([[Credits: Completed Thunder, J, Jessie Cook, Kalen Bridge, MeowMix64, Redmanticore, RedPandah]])
fnDialogue([[Credits: Shepkarian, Taylor Kamohoali'i Bird, The D-Pad, and Toasty Justice.]])
fnDialogue([[ [TOMAINMENU] ]])
]=]
