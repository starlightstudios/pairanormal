--[0 - Going Home]
--Leaving. The game pops up a confirmation dialogue.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--[Execution]
--Demo.
if(false) then
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnDialogue("Thought:Thank you for playing the Pairanormal demo! Press the X button in the top left corner to restart the demo.")
    return

--Start.
elseif(sTopicName == "Default") then
	
	--Question:
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALL]Thought: (Should I head home now?)") ]])

	local sDecisionScript = "\"" .. gsRoot .. "Gameplay_SceneC/0 - Going Home.lua\""
	fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Go Home\", " .. sDecisionScript .. ", \"GoHome\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Not Yet\",  " .. sDecisionScript .. ", \"Dont\") ")
	fnCutsceneBlocker()
	return
	
--Don't go home just yet.
elseif(sTopicName == "Dont") then

	--Resume investigation mode.
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()
	return
end

--Scene setup.
fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
fnCutsceneBlocker()
	
--Save the game.
if(PairanormalLevel_IsLoading() == false) then
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Starting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

--Set data.
else
	PairanormalLevel_FlagCheckpoint("Starting")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Classroom")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Classroom")
	fnDialogue([[ [Music|Daytime]Thought: ... ]])
end

--Flag.
PairanormalLevel_FlagCheckpoint("Starting")

--Gallery unlock.
fnCutsceneBlocker()
fnUnlockGallery("Gallery_WalkHome")

--Dialogue.
fnDialogue([[[HIDEALL]Thought: The day winds down.[P] I grab my backpack and start to make my way to the door.]])
fnDialogue([[Player: Well...[P] I'm gonna head home. [P]Bye guys.]])
fnDialogue([[[SHOWCHAR|Mari|Neutral][SHOWCHAR|Stacy|Neutral][SHOWCHAR|Laura|Neutral][SHOWCHAR|Bret|Neutral][SHOWCHAR|Michelle|Neutral]Thought: The club members crow out various versions of \"Goodbye!\" as I turn into the hallway.]])
fnDialogue([[Laura: [E|Neutral]S-[P]see you later!]])
fnDialogue([[Bret: [E|Happy]After while, crocodile![P][E|Shock] Wait, nobody says that [E|Neutral]anymore.]])
fnDialogue([[Bret: [E|Mad] Oh wait, you wouldn't even know that if I didn't tell you.[P][E|Sad] Aw man, I was [E|Neutral]almost cool for a second.]])
fnDialogue([[Michelle: [E|Neutral]See ya, weirdo.]])
fnDialogue([[[HIDEALL]Thought: I duck out.]])
fnDialogue([[Player: Well, that... wasn't too bad, I guess.[P].[P].[P][GLITCH] ]])
fnDialogue([[[BG|Null]Thought: [Music|Null]Oh boy. A familiar feeling comes over me. [GLITCH]I stumble down the hallway [GLITCHTEXT]to the nearest bathroom[ENDGLITCHTEXT] but can't remember where it is.]])
fnDialogue([[Thought: Please, let me[GLITCH] make it the[GLITCH]re in time.[P] [GLITCH]And don't let anyone see!]])
fnDialogue([[[BG|Root/Images/Backgrounds/All/Hospital][SHOWCHAR|MariHospital|Neutral][SHOWCHAR|StacyHospital|Neutral]Thought: [Music|Somber]Crap.[P] Blacked out again.[P] I really hope I didn't fall down this time...]])
fnDialogue([[Thought: Looks like I'm back at that hospital memory.]])
fnDialogue([[Mari: [FOCUS|MariHospital][EMOTION|MariHospital|Neutral]Sorry, I hope we didn't interrupt you sleeping or something--[P][EMOTION|MariHospital|Shock]but we've never seen you around here before.[P][EMOTION|MariHospital|Neutral] Foster Falls is a pretty small town. ]])
fnDialogue([[Mari: [FOCUS|MariHospital]But if you don't want to tell us anything,[P] that's okay,[P] we're already INTRUDING AND [EMOTION|MariHospital|Shock]OH MY GOD STACY PUT DOWN THAT CLIPBOARD!]])
fnDialogue([[Thought: I look over and realize Stacy has picked up the clipboard hanging on the side of my bed.]])
fnDialogue([[Thought: When she notices me looking at her, it clatters to the floor, and she bites her lips sheepishly.]])
fnDialogue([[Stacy: [FOCUS|StacyHospital][EMOTION|StacyHospital|Shock]oops, eheh! [P][EMOTION|StacyHospital|Happy][ITALICS]Pardonnez mois[ENDITALICS].]])
fnDialogue([[Player: [EMOTION|MariHospital|Neutral]Learn anything interesting?]])
fnDialogue([[Stacy: [FOCUS|StacyHospital][EMOTION|StacyHospital|Shock]Oh, n-no, not at all![P] I mean,[P][EMOTION|StacyHospital|Blush] who can read that chicken scratch handwriting that doctors write in? ]])
fnDialogue([[Stacy: [FOCUS|StacyHospital]Ummm... something about \"Medium Parental Loo\"?[E|Shock] \"Temporal Lobe\"[P]--oh, they're talking about your lobes!]])
fnDialogue([[Mari: [FOCUS|MariHospital][EMOTION|MariHospital|Neutral]Aha! The ballsack.]])
fnDialogue([[Stacy: [FOCUS|StacyHospital][EMOTION|StacyHospital|Mad]NO,[P] MARI,[P][EMOTION|StacyHospital|Neutral] your lobes are in your brain, I think.]])
fnDialogue([[Thought: Mari crosses her arms.]])
fnDialogue([[Mari: [FOCUS|MariHospital][EMOTION|MariHospital|Neutral]I've dated a couple guys where you wouldn't tell the difference.]])
fnDialogue([[Thought: Stacy sighs, picking up the clipboard and setting it by me. ]])
fnDialogue([[Stacy: [FOCUS|StacyHospital][EMOTION|StacyHospital|Neutral]There was another underlined part.[P].[P].[P] \"spontaneous termination of comatose state\".[P][P] Um...[P][E|Sad] were you in a coma?]])
fnDialogue([[Thought: I sink back into my bed.]])
fnDialogue([[Player: Yes.[P] I mean, the doctors said so.]])
fnDialogue([[Thought: I feel heat creeping up my cheeks.[P] I suddenly don't want to talk to these girls, who are looking at me too intensely, too much pity in their eyes.]])
fnDialogue([[Thought: I want to pull back the curtain and go back to sleep.]])
fnDialogue([[Mari: [FOCUS|MariHospital][EMOTION|MariHospital|Shock]WOW, I--[P][P][EMOTION|MariHospital|Sad] don't even really know what to say.[P] I've never encountered anything like that.]])
fnDialogue([[Stacy: [FOCUS|StacyHospital][E|Sad]I'm sorry.[P] We shouldn't have pried.]])
fnDialogue([[Mari: [FOCUS|MariHospital][E|Sad]Are you...[P] okay?]])
fnDialogue([[Stacy: [FOCUS|StacyHospital][E|Sad]Probably a redundant question to ask someone in a hospital...]])
fnDialogue([[Player: [P].[P].[P]. ]])
fnDialogue([[Player: Everything has just been a confusing blur since I woke up and...[P]when I did wake up,[P] nobody c--]])
fnDialogue([[Thought: My throat slams shut.[P] I realize I'm about to cry.]])
fnDialogue([[Player: Nobody came to see me.]])
fnDialogue([[Mari: [FOCUS|MariHospital][EMOTION|MariHospital|Shock][EMOTION|StacyHospital|Mad]!!!]])
fnDialogue([[Stacy: [FOCUS|MariHospital]...]])
fnDialogue([[Player: I keep trying to remember what happened before all this, try to make some sense of it all.[P] But I can't.]])
fnDialogue([[Player: And I know I can't, because...[P] apparently I haven't been making any memories for a long time. ]])
fnDialogue([[Thought: I gesture at the hospital around me.]])
fnDialogue([[Player: This is the only thing I know![P] This horrible place where I'm all alone.]])
fnDialogue([[Player: I'm s-[P]so scared...[P] I'm s--]])
fnDialogue([[Thought: I can no longer form words,[P] and I bury my head in my arm in shame as tears dribble out of my bleary red eyes.]])
fnDialogue([[[HIDEALL]Thought: Ugh.[P].[P].[P] can't believe [GLITCH]I just blurted all of that out to a couple of total strangers.[P] Am I that [GLITCHTEXT]AM I THAT DESPERATE FOR ATTENTION[ENDGLITCHTEXT] desperate for attention, or just pathetic?[GLITCH][GLITCH] ]])
fnDialogue([[Player: ...[P] [BG|Root/Images/Backgrounds/All/SchoolExterior][Music|Null]And back to reality.]])
fnDialogue([[Player: Thanks for reminding me of that random cringey moment from my past, brain.[P] Wouldn't want to be TOO happy!!!]])
fnDialogue([[Stacy: Hey![P] [PLAYERNAME]! Wait up!]])
fnDialogue([[Mari: You're forgetting uuuuuuus!]])
fnDialogue([[Thought: I turn and see the Paranormal club co-captains a few feet away.]])
fnDialogue([[Thought: [Music|Theme]They each take to one side of me, and we walk together as though it were perfectly normal. ]])
fnDialogue([[[SHOWCHAR|Mari|Neutral][SHOWCHAR|Stacy|Neutral]Thought: Mari holds her arms high, wiggling her fingers maniacally.]])
fnDialogue([[Mari:[E|Happy] DID YOU REALLY THINK YOU COULD ESCAPE?[P][CRASH] MWAHAHAHAAAA!]])
fnDialogue([[Stacy: [E|Neutral]Everyone got to get to know YOU better today, but did you get to know everyone else?[P] Only one way to find out, and that is a POP QUIZ.]])
fnDialogue([[Player: Wait, what?!]])
fnDialogue([[Stacy: [E|Neutral]A good Paranormal Club Member is always paying attention to their surroundings. The slightest detail could be important in an investigation!]])
fnDialogue([[Stacy: Soooo, hopefully YOU gathered all the data you could about the other members.[P] Quizmaster Mari, go ahead and ask...[P][E|Happy] THE FIRST QUESTION!!!]])
fnDialogue([[Mari: [E|Neutral][P].[P].[P].]])
fnDialogue([[Stacy: [E|Neutral][P][P]Well?]])
fnDialogue([[Mari: [E|Mad]Shut up![P] I'm trying to think of one.]])
fnDialogue([[Stacy: [E|Sad]Ah, geez.[P] We literally talked about this twenty minutes ago...]])
fnDialogue([[Thought: Well, at least I'm as prepared for this quiz as the quizmasters.]])
fnDialogue([[[HIDEALL]...[P][P][P] ]])
fnDialogue([[Thought: We start towards my home. We walk for some time before Mari suddenly shatters the silence.]])
fnDialogue([[[BG|Root/Images/Backgrounds/All/WalkHome][SHOWCHAR|Mari|Neutral]Mari: [E|Neutral]Okay, okay, okay![P] Here's one --[P][P][E|Shock][CRASH] WHO.[P][CRASH] IS.[P][CRASH] THE CLUB PRESIDENT?]])
fnDialogue([[[SHOWCHAR|Stacy|Neutral]Stacy: [E|Neutral]That's too easy![P][E|Mad] You gotta come up with something harder than that.]])
fnDialogue([[Mari: [CRASH][E|Mad]YOU JUST JUMP IN WHEN YOU HAVE A SUPER AWESOME QUESTION, BUDDY![P][CRASH] MAYBE THIS IS LIKE A WARM UP QUESTION, OKAY???]])
fnDialogue([[Stacy: [E|Neutral]All right, all right.[P] Geez.]])
	
--Decision Script.
fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (Who's the president of the Pairanormal Investigation Club?)") ]])

local sDecisionScript = "\"" .. gsRoot .. "Gameplay_SceneC/1 - First Answer.lua" .. "\""
fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"It's Mari\", " .. sDecisionScript .. ", \"Mari\") ")
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"It's Stacy\",  " .. sDecisionScript .. ", \"Stacy\") ")
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"It's Bret\",  " .. sDecisionScript .. ", \"Bret\") ")
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"It's Laura\",  " .. sDecisionScript .. ", \"Laura\") ")
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"It's Michelle\",  " .. sDecisionScript .. ", \"Michelle\") ")
fnCutsceneBlocker()
