--[4 - Fourth Answer]
--This quiz just keeps going doesn't it?

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)
	
--Save the game.
if(PairanormalLevel_IsLoading() == false) then
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Starting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

--Set data.
else
	PairanormalLevel_FlagCheckpoint("Starting")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/WalkHome")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/WalkHome")
	fnDialogue([[ [Music|Theme][SHOWCHAR|Mari|Neutral][SHOWCHAR|Stacy|Neutral]Thought: ... ]])
end

--Flag.
PairanormalLevel_FlagCheckpoint("Starting")

--[Execution]
--Michellicans!
if(sTopicName == "Michelle") then
	local iQuizPoints = VM_GetVar("Root/Variables/Chapter1/Misc/iQuizPoints", "N")
	VM_SetVar("Root/Variables/Chapter1/Misc/iQuizPoints", "N", iQuizPoints + 1)
	
	--Dialogue.
	fnDialogue([[Thought: Mari victoriously blows on an invisible trumpet.]])
	fnDialogue([[Mari: [E|Shock][CRASH]Trn-trn-trnnnnnnn![P][E|Happy] That's right!]])
	fnDialogue([[Stacy: [E|Neutral]Wow, you were really paying attention out there, [PLAYERNAME].[P] Good job.]])
	fnDialogue([[Mari: [E|Blush]Or [ITALICS]maybe[ENDITALICS] you were really paying attention to Michelle.[P] Ehhh?[P] Ehhhh?]])
	fnDialogue([[Player: All right, all right, cut it out.]])
	
--Anything else.
else
	fnDialogue([[Stacy:[E|Sad] Ooh, not quite.]])
	fnDialogue([[Mari:[E|Neutral] Don't be too hard on yourself, [PLAYERNAME].[P] That one was tough to know unless you were really paying attention.]])
	fnDialogue([[Stacy: [E|Happy]Our second-newest member is Michelle![P][E|Neutral] She actually joined our club last year.]])

end

--Rejoin.
fnDialogue([[Mari: [E|Neutral]Hm.[P].[P].[P][E|Sad] I'm out of questions.]])
fnDialogue([[Stacy: [E|Shock]Wait![P] We still haven't asked any questions about Laura.]])
fnDialogue([[Stacy: [E|Mad]Hmm...]])
fnDialogue([[Mari: [E|Neutral]Okay, I've got one.]])
fnDialogue([[Mari: *ahem*.]])
fnDialogue([[Mari: Everyone in the Paranormal Club goes to various locations when we're investigating stuff,[P] but LAURA usually goes somewhere else.[P][P][P][CRASH][E|Shock] WHERE DOES SHE GO?]])
fnDialogue([[Stacy: [E|Shock]Oh damn, that's [ITALICS]another[ENDITALICS] good one!]])
fnDialogue([[Mari: [E|Neutral]I am like, super good at this.]])
fnDialogue([[Stacy: [E|Neutral]Yeah, seriously.[P] You should be like a real life Quizmaster on TV and stuff.]])
fnDialogue([[Mari: [E|Neutral]I would be [ITALICS]the best[ENDITALICS] Quizmaster if I didn't have,[P] like,[P] crippling social anxiety.]])
fnDialogue([[Mari: [E|Neutral]You think we should host a show or something?]])
fnDialogue([[Stacy: [E|Neutral]Pft, maybe in a parallel universe!]])
fnDialogue([[Mari: [E|Neutral]Now answer the question, [PLAYERNAME]! ]])

--Decision Script.
fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (Where does Laura go during a mission?)") ]])

local sDecisionScript = "\"" .. gsRoot .. "Gameplay_SceneC/5 - Fifth Answer.lua" .. "\""
fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"She goes home\", " .. sDecisionScript .. ", \"Home\") ")
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"She goes to the library\",  " .. sDecisionScript .. ", \"Library\") ")
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"She goes to the old computer lab\",  " .. sDecisionScript .. ", \"Computer\") ")
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"She goes to our clubhouse\",  " .. sDecisionScript .. ", \"Clubhouse\") ")
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Trick question! She comes with us.\",  " .. sDecisionScript .. ", \"ComeWith\") ")
fnCutsceneBlocker()

