--[2 - Second Answer]
--It always comes down to Bret doesn't it.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)
	
--Save the game.
if(PairanormalLevel_IsLoading() == false) then
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Starting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

--Set data.
else
	PairanormalLevel_FlagCheckpoint("Starting")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/WalkHome")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/WalkHome")
	fnDialogue([[ [Music|Theme][SHOWCHAR|Mari|Neutral][SHOWCHAR|Stacy|Neutral]Thought: ... ]])
end

--Flag.
PairanormalLevel_FlagCheckpoint("Starting")

--[Execution]
--He knows useless trivia.
if(sTopicName == "Facts") then
	local iQuizPoints = VM_GetVar("Root/Variables/Chapter1/Misc/iQuizPoints", "N")
	VM_SetVar("Root/Variables/Chapter1/Misc/iQuizPoints", "N", iQuizPoints + 1)
	
	--Dialogue.
	fnDialogue([[Stacy: [E|Neutral]TRICK QUESTION![P][CRASH] THEY'RE ALL TRUE!]])
	fnDialogue([[Mari: [E|Shock]Wait, [CRASH]SERIOUSLY???[P][P][P][CRASH] When did he eat FOUR bowls of Maple Bacon Mac and Cheese?]])
	fnDialogue([[Thought: Stacy gets a faraway look in her eye.]])
	fnDialogue([[Stacy: [E|Mad][ITALICS]April 17th, 2008[ENDITALICS].[P] It was awesome.]])
	fnDialogue([[Mari: [E|Neutral]But, you are sort of right, [PLAYERNAME], in that Bret's knowledge is one of his most noticeable features.]])
	fnDialogue([[Stacy: [E|Neutral]That's obviously SUPER useful for club expeditions.]])
	fnDialogue([[Mari: [E|Neutral]Did I tell you about the time I found a beat up victorian doll in the woods?[P] Bret took a look at it--]])
	fnDialogue([[Stacy: [E|Mad][CRASH]NO, PLEASE DO NOT TALK ABOUT THIS.]])
	fnDialogue([[Mari: [E|Happy]--and discovered that inside the doll, there was[P].[P].[P].[ITALICS]a dead squirrel[ENDITALICS]! ]])
	fnDialogue([[Stacy: [E|Mad][CRASH]LALALALALALA IM NOT HEARING THIS AGAIN.]])
	fnDialogue([[Mari: [E|Neutral]If it hadn't been for Bret, we would have never known that there's someone in this town who puts small, dead, forest creatures inside of victorian dolls.]])
	fnDialogue([[Mari: [P][P][P][E|Happy][ITALICS]yaaaaaay[ENDITALICS] !]])

--Anything else.
else
	fnDialogue([[Stacy: [E|Neutral]TRICK QUESTION![P][CRASH] THEY'RE ALL TRUE!]])
	fnDialogue([[Mari: [E|Shock]Wait, [CRASH]SERIOUSLY???[P][P][P][CRASH] When did he eat FOUR bowls of Maple Bacon Mac and Cheese?]])
	fnDialogue([[Thought: Stacy gets a faraway look in her eye.]])
	fnDialogue([[Stacy: [E|Mad][ITALICS]April 17th, 2008[ENDITALICS].[P] It was awesome.]])
	fnDialogue([[Mari: [E|Neutral]But anyway, I would say that one of Bret's more noticeable traits is that he's got a lot of obscure knowledge, and he's really observant.]])
	fnDialogue([[Stacy: [E|Neutral]That's obviously SUPER useful for club expeditions.]])
	fnDialogue([[Mari: [E|Neutral]Did I tell you about the time I found a beat up victorian doll in the woods?[P] Bret took a look at it--]])
	fnDialogue([[Stacy: [E|Mad][CRASH]NO, PLEASE DO NOT TALK ABOUT THIS.]])
	fnDialogue([[Mari: [E|Happy]--and discovered that inside the doll, there was[P].[P].[P].[ITALICS]a dead squirrel[ENDITALICS]! ]])
	fnDialogue([[Stacy: [E|Mad][CRASH]LALALALALALA IM NOT HEARING THIS AGAIN.]])
	fnDialogue([[Mari: [E|Neutral]If it hadn't been for Bret, we would have never known that there's someone in this town who puts small, dead, forest creatures inside of victorian dolls.]])
	fnDialogue([[Mari: [P][P][P][E|Happy][ITALICS]yaaaaaay[ENDITALICS] !]])
	
end

--Rejoin.
fnDialogue([[Stacy: [E|Neutral][P][P][P]Okay, time for question number three!]])
fnDialogue([[Stacy: WHAT kind of cheese was in my grilled cheese sandwiches?]])
fnDialogue([[Mari: [E|Neutral]If you don't think you can pronounce it, you're on the right track.]])
fnDialogue([[Stacy: [E|Mad][SPEEDTEXT][ITALICS]How dare you make fun of my cheese connoiseurship, which you know I take VERY seriously !!![ENDITALICS][ENDSPEEDTEXT] ]])
	
--Decision Script.
fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (What cheese was in those sandwiches?)") ]])

local sDecisionScript = "\"" .. gsRoot .. "Gameplay_SceneC/3 - Third Answer.lua" .. "\""
fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Limburger\", " .. sDecisionScript .. ", \"Limburger\") ")
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Oaxaquena\",  " .. sDecisionScript .. ", \"Oaxaquena\") ")
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Parmigiano Reggiano\",  " .. sDecisionScript .. ", \"Reggiano\") ")
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"New York Cheddar\",  " .. sDecisionScript .. ", \"Cheddar\") ")
fnCutsceneBlocker()
