--[7 - Flashback]
--Store arguments.
local sTopicName = "No Topic"
	
--Save the game.
if(PairanormalLevel_IsLoading() == false) then
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Starting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

--Set data.
else
	PairanormalLevel_FlagCheckpoint("Starting")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Hospital")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Hospital")
end

--Flag.
PairanormalLevel_FlagCheckpoint("Starting")

--More hospital scenes.
fnDialogue([[[HIDEALL][BG|Root/Images/Backgrounds/All/Hospital]Thought: [Music|Null]I don't know how long it's been,[P] but after what seems like ages, I lift my head, tired of crying,[P] and begin smearing away the sop of tears from my nose and chin.]])
fnDialogue([[Thought: I can't hear anything except for a dull ringing in my ears,[P] that distant \"beep\", \"beep\" of a monitor,[P] and my own occasional sniffle. ]])
fnDialogue([[Thought: I reach for the corner of a bedsheet to wipe my eyes with, but realize that someone has my hand in a vicegrip.]])
fnDialogue([[Thought: I blink.]])

--Gallery unlock.
fnCutsceneBlocker()
fnUnlockGallery("Gallery_Scene_HospitalCry")
fnDialogue([[ [Image|Root/Images/MajorScenes/All/HospitalCry]Thought: [Music|Somber]I look up, and see Stacy and Mari, closer than ever.]])
fnDialogue([[Thought: Stacy's hand is clutching me so tightly, her arm is shaking.[P] Mari is a blubbery mess, tears streaking down her cheeks and soaking her hospital gown.]])
fnDialogue([[Player: [P][P][P]You guys are.[P].[P].[P] still here?]])
fnDialogue([[[Image|Null][SHOWCHAR|MariHospital|Neutral]Thought: Mari squeaks, her voice barely above a whisper.]])
fnDialogue([[Mari: [FOCUS|MariHospital][EMOTION|MariHospital|Shock][CRASH]Stacy!![P][EMOTION|MariHospital|Blush] C-can you push our beds together?]])
fnDialogue([[[ShowChar|StacyHospital|Neutral]Thought: Stacy nods as though they were thinking the same thing.]])
fnDialogue([[Stacy: [FOCUS|StacyHospital][EMOTION|StacyHospital|Mad][ITALICS]Mais oui, mon amis[ENDITALICS]. ]])
fnDialogue([[Thought: Checking around quickly to make sure no one is watching, she stretches out her long leg and hooks it under Mari's bed.]])
fnDialogue([[ [CRASH][SFX|World|Ugh] With a restrained grunt, it's dragged up next to mine.]])
fnDialogue([[Mari: [FOCUS|MariHospital][EMOTION|MariHospital|Sad][P][P][P]Do you want a hug?]])
fnDialogue([[Thought: I realize that I do, very badly, and nod.]])
fnDialogue([[Thought: Mari gently puts her arms around me.[P] Her embrace is warm and soft.]])
fnDialogue([[Thought: I bury my head in the crook of her neck.[P] Stacy reaches around the two of us with her free arm and gives the group a power squeeze.]])

--Gallery unlock.
fnCutsceneBlocker()
fnUnlockGallery("Gallery_Scene_HospitalHug")
fnDialogue([[[HIDEALL][Image|Root/Images/MajorScenes/All/HospitalHug]Thought: My lungs release a long, shivering sigh--[P]it feels like I hadn't even been really breathing before.]])
fnDialogue([[Thought: And now, I'm fully aware of the cool, crisp air flowing in and out;[P] of every heartbeat.]])
fnDialogue([[Thought: Mari giggles, whispering into my ear.]])
fnDialogue([[Mari: [FOCUS|MariHospital]By the way, what's your name? I'm Mari.]])
fnDialogue([[Stacy: [FOCUS|StacyHospital][ITALICS]Je m'apelle[ENDITALICS] Stacy.]])
fnDialogue([[Player: My name's [PLAYERNAME]. It's nice to meet you. ]])
fnDialogue([[[BG|Null][IMAGE|Null][HIDEALL]Thought: [Music|Null]...]])

LM_ExecuteScript(gsRoot .. "Gameplay_SceneC/8 - Arrive Home.lua")