-- |[5 - Fifth Answer]|
--Aw geez.

-- |[Arg Resolve]|
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)
	
--Save the game.
if(PairanormalLevel_IsLoading() == false) then
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Starting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

--Set data.
else
	PairanormalLevel_FlagCheckpoint("Starting")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/WalkHome")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/WalkHome")
	fnDialogue([[ [Music|Theme][SHOWCHAR|Mari|Neutral][SHOWCHAR|Stacy|Neutral]Thought: ... ]])
end

--Flag.
PairanormalLevel_FlagCheckpoint("Starting")

-- |[Execution]|
--Libairy.
if(sTopicName == "Library") then
	local iQuizPoints = VM_GetVar("Root/Variables/Chapter1/Misc/iQuizPoints", "N")
	VM_SetVar("Root/Variables/Chapter1/Misc/iQuizPoints", "N", iQuizPoints + 1)
	
	--Dialogue.
	fnDialogue([[Mari: [E|Happy][CRASH]YES![P] That's right!]])
	fnDialogue([[Stacy: [E|Neutral]Nicely done, [PLAYERNAME].[P] I bet Laura is really pleased that you took the time to get to know her.]])
	fnDialogue([[Mari: [E|Neutral]Yeah, she has a difficult time opening up to new people, so don't be afraid to chat.]])

--Anything else.
else
	fnDialogue([[Mari: [E|Sad]mmm, not quite]])
	fnDialogue([[Stacy: [E|Shock]I mean, Laura [ITALICS]has[ENDITALICS] gone there during a meeting,[P][E|Neutral] but usually she spends her time at the Library.]])
	fnDialogue([[Mari: [E|Neutral]I know Laura is kinda shy, but I'm sure she would love to get to know you better, [PLAYERNAME]![P] Don't be afraid to ask her questions.]])

end

--Variables.
local iQuizPoints = VM_GetVar("Root/Variables/Chapter1/Misc/iQuizPoints", "N")

--Rejoin.
fnDialogue([[Thought: Stacy scratches her chin.]])
fnDialogue([[Stacy: [E|Neutral]So, let's see... how well did you do?]])

--FAILURE!
if(iQuizPoints == 0) then
	fnDialogue([[Stacy: [E|Neutral][PLAYERNAME] got none of the questions right...]])
	fnDialogue([[Mari: [E|Neutral]Well, that's how it goes on your first try...]])
	fnDialogue([[Mari: [E|Mad][SFX|World|Crash]BUT DON'T BE DISCOURAGED!!![P][E|Neutral] I know all of this is kind of new to you, and believe me, opening up to people can be challenging.]])
	fnDialogue([[Stacy: [E|Neutral]Yeah, don't feel bad.[P] The quiz was just a way to encourage you to get to know everyone.]])
	fnDialogue([[Stacy: [E|Happy]Take your time; everyone in the Paranormal club is super friendly and welcoming.]])
	fnDialogue([[Player: Right.]])
	
elseif(iQuizPoints == 1) then
	fnDialogue([[Stacy: [E|Neutral][PLAYERNAME] got one of the questions right...]])
	fnDialogue([[Stacy: [E|Sad]Kind of a rough turnout.[P][E|Neutral] I think you could have done a little more investigating.]])
	fnDialogue([[Mari: [E|Mad][SFX|World|Crash]BUT DON'T BE DISCOURAGED!!![P][E|Neutral] I know all of this is kind of new to you, and believe me, opening up to people can be challenging.]])
	fnDialogue([[Stacy: [E|Neutral]Yeah, don't feel bad.[P] The quiz was just a way to encourage you to get to know everyone.]])
	fnDialogue([[Stacy: [E|Happy]Take your time; everyone in the Paranormal club is super friendly and welcoming.]])
	fnDialogue([[Player: Right.]])
	
elseif(iQuizPoints == 2) then
	fnDialogue([[Stacy: [E|Neutral][PLAYERNAME] got two of the questions right...]])
	fnDialogue([[Stacy: [E|Sad]Kind of a rough turnout.[P][E|Neutral] I think you could have done a little more investigating.]])
	fnDialogue([[Mari: [E|Mad][SFX|World|Crash]BUT DON'T BE DISCOURAGED!!![P][E|Neutral] I know all of this is kind of new to you, and believe me, opening up to people can be challenging.]])
	fnDialogue([[Stacy: [E|Neutral]Yeah, don't feel bad.[P] The quiz was just a way to encourage you to get to know everyone.]])
	fnDialogue([[Stacy: [E|Happy]Take your time; everyone in the Paranormal club is super friendly and welcoming.]])
	fnDialogue([[Player: Right.]])
	
elseif(iQuizPoints == 3) then
	fnDialogue([[Stacy: [E|Neutral][PLAYERNAME] got three of the questions right!]])
	fnDialogue([[Stacy: [E|Happy] All right! not too shabby.]])
	fnDialogue([[Mari: [E|Shock]You obviously took the time to speak with everyone,[P][E|Neutral] and you really put yourself out there, which isn't easy to do.]])
	
elseif(iQuizPoints == 4) then
	fnDialogue([[Stacy: [E|Neutral][PLAYERNAME] got four of the questions right!]])
	fnDialogue([[Stacy: [E|Happy] All right! not too shabby.]])
	fnDialogue([[Mari: [E|Shock]You obviously took the time to speak with everyone,[P][E|Neutral] and you really put yourself out there, which isn't easy to do.]])
	
elseif(iQuizPoints == 5) then
	fnDialogue([[Stacy: [E|Neutral][PLAYERNAME] got five of the questions right!]])
	fnDialogue([[Stacy: [E|Happy] NAILED IT.]])
	fnDialogue([[Mari: [E|Shock]I'm impressed, because if I were in the same spot I'd have probably gotten them all wrong.]])
	fnDialogue([[Mari: [E|Sad]Yes that includes the first one... I'm really bad at quizzes...]])
end

--Rejoin.
fnDialogue([[Stacy: [E|Neutral]I think,[P] as a general rule,[P] [ITALICS]the more you interact with club members, the more they'll like you[ENDITALICS]. ]])
fnDialogue([[Mari: [E|Shock][CRASH]BUT I WOULD ALSO LIKE TO ADD,[P][E|Neutral] like,[P] just because you like one club member more than another, [ITALICS]you shouldn't ignore everyone else.[ENDITALICS] ]])
fnDialogue([[Stacy: [E|Shock]Yeah, that's kind of rude.]])
fnDialogue([[Mari: [E|Neutral]Every club member has something special to offer![P][E|Happy] That's why we all make such a great team!]])
fnDialogue([[Player: Sounds good.[P] I'll try to keep that in mind.]])
fnDialogue([[Thought: The three of us walk in peaceful silence for a few minutes. ]])
fnDialogue([[Thought: [P][P][P]Mari suddenly stops, clutching me by the arm.]])
fnDialogue([[Mari: [CRASH][E|Shock]OH MY GOD, LOOK!!![P][CRASH][E|Happy] A DOGGIEEEEE!!!!]])
fnDialogue([[Player: Eh???]])
fnDialogue([[Thought: off in the distance, we see a small dog, digging a hole in someone's yard.]])
fnDialogue([[[SHOWCHAR|AJ|Neutral]Thought: [Focus|AJ]It looks to be healthy, but the muddy paws and ratted fur coat indicate it's had its share of wandering.]])
fnDialogue([[Thought: The dog's ears twitch.[P] It becomes aware of our existence, and takes a defensive posture.]])
fnDialogue([[Stacy: [E|Shock]It doesn't have a collar.[P] Is it a stray?]])
fnDialogue([[Mari: [E|Mad][CRASH]QUICK![P][CRASH] LETS ADOPT IT!!!]])
fnDialogue([[Thought: [Focus|AJ][EMOTION|AJ|Mad]As the three of us approach, the dog's back arches, and it begins to snarl. ]])
fnDialogue([[Stacy: [E|Happy]Ehh, let's not.]])
fnDialogue([[Mari: [E|Shock]But--but!! [P][CRASH][E|Blush]I CANT JUST LEAVE IT HERE!]])
fnDialogue([[Thought: Stacy slings off her backpack.]])
fnDialogue([[Stacy: [E|Neutral]I've got some leftover grilled cheese here.[P] We could give the little dude some food.]])
fnDialogue([[Thought: As Stacy rustles around in the contents of her backpack, Mari stops her.]])
fnDialogue([[Mari: [E|Sad]No, wait.[P] Isn't cheese bad for dogs?[P][CRASH] I think I read somewhere that dairy makes them really sick!]])
fnDialogue([[Thought: Stacy withdraws the cheese sandwich from her backpack.]])
fnDialogue([[Thought: [Focus|AJ][EMOTION|AJ|Neutral]The dog's snarl fades, and its eyes grow round with curiosity.[P] It sniffles the air and licks its chops in anticipation.]])
fnDialogue([[Stacy: [P].[P].[P].Looks to me like it's hungry.[P] What do you think, [PLAYERNAME]?]])

--Decision Script.
fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "(What should we do?)") ]])

local sDecisionScript = "\"" .. gsRoot .. "Gameplay_SceneC/6 - AJ Decision.lua" .. "\""
fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Feed the little guy\", " .. sDecisionScript .. ", \"Feed\") ")
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Don't risk getting him sick\",  " .. sDecisionScript .. ", \"Dont\") ")
fnCutsceneBlocker()

