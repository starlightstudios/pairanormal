--[8 - Arrive Home]
--Back to reality. You know. Again.
	
--Save the game.
local sTopicName = "No Topic"
	
--Save the game.
if(PairanormalLevel_IsLoading() == false) then
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Starting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

--Set data.
else
	PairanormalLevel_FlagCheckpoint("Starting")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/WalkHome")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/WalkHome")
end

--Flag.
PairanormalLevel_FlagCheckpoint("Starting")

--Gallery unlock.
fnCutsceneBlocker()
fnUnlockGallery("Gallery_LivingRoom")
fnDialogue([[[HIDEALL][BG|Root/Images/Backgrounds/All/WalkHome]Thought: [Music|Daytime]The sun begins to dip behind the rooftops of the suburb. I take a house key out of my pocket and unlock the porch door I'm standing in front of.]])
fnDialogue([[Deep breath.[P][P][P] I step inside.]])
fnDialogue([[[BG|Root/Images/Backgrounds/All/LivingRoom]Thought: The inside of this house looks like ones I've seen in a magazine;[P] the furniture is elegant, black, sleek.]])

if(gbSkipDebug == false) then
    fnDialogue([[Thought: Every accent on a shelf or photo hanging on the wall looks carefully chosen, placed just so.]])
    fnDialogue([[Thought: Perfect, really.[P] But too perfect for any normal person to live in.]])
    fnDialogue([[[SHOWCHAR|Eilette|Neutral]Eilette: [E|Neutral]Greetings, [PLAYERNAME]. Welcome back.]])
    fnDialogue([[Thought: A prim young woman is seated on the couch, reading through some kind of planner on her tablet.]])
    fnDialogue([[Thought: With gloved hands, She picks up freshly poured hot coffee and takes a sip.]])
    fnDialogue([[Thought: Aside from her perfectly lined lips drawing around the rim of the plain charcoal mug,[P] her face remains completely expressionless.]])
    fnDialogue([[Eilette: [E|Neutral]Won't you take a seat?[P] If it's all right with you, I would like to talk about your day.]])
    fnDialogue([[Thought: I sit across from her, dropping my backpack at my feet.[P] I actually didn't realize how heavy it was until I took it off.[P] [ITALICS]Whew![ENDITALICS] ]])
    fnDialogue([[Thought: I don't know much about Eilette, my so-called caretaker.[P] So far, her motivation for taking me in is a total mystery.]])
    fnDialogue([[Thought: Did someone saddle me with her unexpectedly?[P] Did she know my parents?[P] Is she working for some kind of childcare agency?]])
    fnDialogue([[Thought: For two months, she hasn't given me a clue.[P] All I know is, when the hospital released me, it was into the back of her car.]])
    fnDialogue([[Eilette: [E|Neutral]How was your first day at school?]])
    fnDialogue([[Player: It was tough.[P] But...[P] I guess I made it through.]])
    fnDialogue([[Thought: She nods curtly.[P] Another swig of coffee.]])
    fnDialogue([[Eilette: [E|Happy]Well done.[P] I expected as much would happen.[P] Have you begun acquiring friends yet?]])
    fnDialogue([[Player: Well, I don't know if I would call them 'friends', but...[P] I joined an after-school club.]])
    fnDialogue([[Thought: Eilette takes a sharp breath, her nostrils flaring.[P] She places the mug and the tablet down on the table between us.]])
    fnDialogue([[Thought: A few moments pass as she appears to swirl the coffee in her mouth, like thoughts, before swallowing.]])
    fnDialogue([[Eilette: [E|Shock]An extracurricular club? Extraordinary.]])
    fnDialogue([[Eilette: [E|Happy][PLAYERNAME], you're assimilating to high school life at an excellent rate.[P] You should be very proud of your progress.]])
    fnDialogue([[Player: Yeah... [P]sure. ]])
    fnDialogue([[Eilette: [E|Neutral]Anyway, that's enough pleasantries.[P] I have an announcement to make-- I've hired a personal tutor for you.]])
    fnDialogue([[Player: Huh?]])
    fnDialogue([[Eilette: [E|Neutral][PLAYERNAME], you've missed several years of school.[P][E|Happy] If you don't want to be the embarrassment of your class, you'll have to put some serious effort into catching up.]])
    fnDialogue([[Thought: My face reddens. ]])
    fnDialogue([[Eilette: [E|Neutral]You'll be studying with him 3 nights a week, on any subject you feel you're struggling the most with.]])
    fnDialogue([[Eilette: [E|Shock] I must admit, I wasn't anticipating you'd actually be [ITALICS]talking[ENDITALICS] to anyone from your school yet,[P] let alone joining a club -- ]])
    fnDialogue([[Eilette: Instead of contracting a professional, I opted for a highschooler like yourself.[P][E|Neutral] I thought he might be able to answer your questions about student life.]])
    fnDialogue([[Player: R-right...[P] I guess I can--]])
    fnDialogue([[Eilette: [E|Happy]Actually, You might want to get ready.[P] He'll be stopping by soon for your first tutoring session a little later this evening.]])
    fnDialogue([[Player: [CRASH]WHAT?[P] This evening???[P] But I just got back from--]])
    fnDialogue([[Eilette: [E|Mad]That's[P] enough.]])
    fnDialogue([[Thought: A chill goes down my spine, and I bitterly bite my tongue.]])
    fnDialogue([[Thought: Eilette stands up, observing the clock on the wall.[P] She smooths down her pencil skirt, as though she could feel every errant speck of dust on it.]])
    fnDialogue([[Eilette: [E|Happy]I'd say you have about an hour before he arrives.[P] Take care to get your affairs in order before then.]])
    fnDialogue([[[HIDEALL]Player: Hm... looks like I've got a little bit of time before this tutor arrives.]])
    fnDialogue([[Info: Most investigations are limited to a certain amount of time before you have to leave.]])
    fnDialogue([[Info: Pay attention to the top right bar. As you take actions, time will go by.]])
    fnDialogue([[Info: When you're out of time, you have no choice but to leave the scene and move on.]])
end
fnDialogue([[Info: Make sure to use your time investigating wisely!]])

--[Loading the Game]
--If currently loading the game, set up the investigation.
if(PairanormalLevel_IsLoading() == true) then
	
	--Flag.
	PairanormalLevel_FlagCheckpoint("Investigation")

	--Variables.
	local sBGPath = "Root/Images/Backgrounds/All/LivingRoom"
	local sScriptPath = gsRoot .. "Gameplay_SceneD/0 - ExamineObject.lua"
	
	--Change the background.
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/LivingRoom")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/LivingRoom")

	--Give the player 30 units of time, boot investigation scripts.
	Dia_SetProperty("Clear Investigation Characters")
	Dia_SetProperty("Clear Investigation")
	Dia_SetProperty("Start Investigation") 
	Dia_SetProperty("Set Investigation Image", "Root/Images/Backgrounds/All/LivingRoom")
	Dia_SetProperty("Set Investigation Script", gsRoot .. "Gameplay_SceneD/0 - ExamineObject.lua")
	Dia_SetProperty("Set Go Home Strings", gsRoot .. "Gameplay_SceneE/0 - End Investigation.lua", "Default")
    
    --Talkable Characters
	Dia_SetProperty("Add Talkable Character", "Eilette", gsRoot .. "Gameplay_SceneD/1 - Present Eilette.lua")
	
	--Eilette as an examinable object.
	local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
	Dia_SetProperty("Add Discovered Object", "Eilette", "Root/Images/Backgrounds/All/CharPortraits", gsRoot .. "Gameplay_SceneD/0 - ExamineObject.lua", "Eilette", 442, 2, 442 + 438, 2 + 363)
	Dia_SetProperty("Clear Discovered Object")
    
    --SLF file adds the examinable objects.
    Dia_SetProperty("Parse SLF File", gsRoot .. "MapData/InvestigationBA.slf")
	
	--Check all the items and re-add them to the inventory.
	for i = 1, gciInvB_Total-1, 1 do
		
		--Variable name.
		local sVarName = "Root/Variables/Chapter1/InvestigationB/iObject|" .. gciaObjB[i][1]
		local iHasItem = VM_GetVar(sVarName, "N")
		
		--Has the item. Add it.
		if(iHasItem == 1.0) then
			Dia_SetProperty("Add Discovered Object", gciaObjB[i][1], sBGPath, sScriptPath, gciaObjB[i][1])
		end
		Dia_SetProperty("Clear Discovered Object")
	end

	--Modify the time max/left.
    VM_SetVar("Root/Variables/System/Investigation/sInvestigationPath", "S", "Root/Variables/Chapter1/Investigation/")
	local iTimeMax  = VM_GetVar("Root/Variables/Chapter1/Investigation/iTimeMax",  "N")
	local iTimeLeft = VM_GetVar("Root/Variables/Chapter1/Investigation/iTimeLeft", "N")
	Dia_SetProperty("Investigation Time Max",  iTimeMax)
	Dia_SetProperty("Investigation Time Left", iTimeLeft)

--[Normal Operations]
--Otherwise, flag the game to save and boot the investigation normally.
else

	--Save the game.
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Investigation\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

	--Set the time max/left.
	fnCutsceneInstruction([[ VM_SetVar("Root/Variables/Chapter1/Investigation/iTimeMax",  "N", 30.0) ]])
	fnCutsceneInstruction([[ VM_SetVar("Root/Variables/Chapter1/Investigation/iTimeLeft", "N", 30.0) ]])

	--Give the player 30 units of time, boot investigation scripts.
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Investigation Characters") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Investigation Time Max", 30) ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Investigation Time Left", 30) ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Set Investigation Image", "Root/Images/Backgrounds/All/LivingRoom") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Set Investigation Script", gsRoot .. "Gameplay_SceneD/0 - ExamineObject.lua") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Set Go Home Strings", gsRoot .. "Gameplay_SceneE/0 - End Investigation.lua", "Default") ]])
    
    --Talkable characters.
	fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Eilette", gsRoot .. "Gameplay_SceneD/1 - Present Eilette.lua") ]])

    --SLF file adds the examinable objects.
    fnCutsceneInstruction([[ Dia_SetProperty("Parse SLF File", gsRoot .. "MapData/InvestigationBA.slf") ]])
	
	--Add Eilette as an examinable object.
	fnCutsceneInstruction([[ Dia_SetProperty("Add Discovered Object", "Eilette", "Root/Images/Backgrounds/All/CharPortraits", gsRoot .. "Gameplay_SceneD/0 - ExamineObject.lua", "Eilette", 442, 2, 442 + 438, 2 + 363) ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    
    --Set the time variables.
    VM_SetVar("Root/Variables/System/Investigation/sInvestigationPath", "S", "Root/Variables/Chapter1/Investigation/")
end

--Checkpoint notification.
PairanormalLevel_FlagCheckpoint("Investigation")
fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Investigation\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
