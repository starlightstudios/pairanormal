--[1 - First Answer]
--First answer to the pop quiz.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)
	
--Save the game.
if(PairanormalLevel_IsLoading() == false) then
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Starting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

--Set data.
else
	PairanormalLevel_FlagCheckpoint("Starting")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/WalkHome")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/WalkHome")
	fnDialogue([[ [Music|Theme]Thought:[SHOWCHAR|Mari|Neutral][SHOWCHAR|Stacy|Neutral] ... ]])
end

--Flag.
PairanormalLevel_FlagCheckpoint("Starting")

--[Execution]
--Mari.
if(sTopicName == "Mari") then
	local iQuizPoints = VM_GetVar("Root/Variables/Chapter1/Misc/iQuizPoints", "N")
	VM_SetVar("Root/Variables/Chapter1/Misc/iQuizPoints", "N", iQuizPoints + 1)
	
	fnDialogue([[Thought: Mari punches the air.]])
	fnDialogue([[Mari: [E|Happy]YES![P][P][E|Neutral] [PLAYERNAME] gets 1 point, and we get 0.]])
	fnDialogue([[Stacy: [E|Neutral][P][P][P][E|Mad]Wait, when did this become a competition? Why are we even keeping score?]])
	fnDialogue([[Mari: [E|Happy]Hehehehe... Only losers ask why we keep score.]])
	fnDialogue([[Stacy: [E|Shock]We [ITALICS]are[ENDITALICS] losing, by your count!]])

--Stacy.
elseif(sTopicName == "Stacy") then
	local iQuizPoints = VM_GetVar("Root/Variables/Chapter1/Misc/iQuizPoints", "N")
	VM_SetVar("Root/Variables/Chapter1/Misc/iQuizPoints", "N", iQuizPoints + 1)
	
	fnDialogue([[Mari: [E|Sad]Er - oh well that's not right.]])
	fnDialogue([[Stacy: [E|Neutral]Hey, it's technically correct. We're co-captains.]])
	fnDialogue([[Mari: [E|Happy]Okay, one point for you, and none for us.]])
	fnDialogue([[Stacy: [E|Neutral][P][P][P][E|Mad]Wait, when did this become a competition? Why are we even keeping score?]])
	fnDialogue([[Mari: [E|Happy]Hehehehe... Only losers ask why we keep score.]])
	fnDialogue([[Stacy: [E|Shock]We [ITALICS]are[ENDITALICS] losing, by your count!]])

--Anyone else.
else
	fnDialogue([[Mari:[E|Shock] What, no!]])
	fnDialogue([[Stacy: [E|Neutral]Hmmm, I suspect you totally threw that on purpose.]])
	fnDialogue([[Mari: [E|Happy]The club president is meeeeeee!]])
	fnDialogue([[Stacy: [E|Neutral]In title only, of course. [P]We share responsibilites as co-captains, pretty much.]])
	fnDialogue([[Thought: Mari nods vigorously.]])
	

end

--Rejoin.
fnDialogue([[Mari: Right, now the next question.[P] Stacy, why don't you ask next, [CRASH][SPEEDTEXT]SINCE THIS IS SO EASY FOR YOU APPARENTLY--[ENDSPEEDTEXT] ]])
fnDialogue([[Stacy: [E|Neutral]Question two:: Which of these five things is true about Bret?]])
fnDialogue([[Mari: [E|Neutral][P].[P].[P].I hate you.]])
	
--Decision Script.
fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (Which of these is true?)") ]])

local sDecisionScript = "\"" .. gsRoot .. "Gameplay_SceneC/2 - Second Answer.lua" .. "\""
fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"He knows lots of obscure facts\", " .. sDecisionScript .. ", \"Facts\") ")
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"He has a vintage bottlecap collection\",  " .. sDecisionScript .. ", \"Bottlecaps\") ")
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"He owns four cats\",  " .. sDecisionScript .. ", \"Cats\") ")
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"He once ate four bowls of maple bacon mac and cheese for lunch\",  " .. sDecisionScript .. ", \"Gross\") ")
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"He has the highest scores on 7 of the 12 arcade machines at Charlie's Pizza Parlor\",  " .. sDecisionScript .. ", \"Arcade\") ")
fnCutsceneBlocker()
