--[6 - Much Ado About AJ]
--Dogs are lactose intolerant.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)
	
--Save the game.
if(PairanormalLevel_IsLoading() == false) then
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Starting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

--Set data.
else
	PairanormalLevel_FlagCheckpoint("Starting")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/WalkHome")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/WalkHome")
	fnDialogue([[ [Music|Theme][SHOWCHAR|Mari|Neutral][SHOWCHAR|Stacy|Neutral][SHOWCHAR|AJ|Neutral]Thought: ... ]])
end

--Flag.
PairanormalLevel_FlagCheckpoint("Starting")

--[Execution]
--Feed him.
if(sTopicName == "Feed") then
	fnDialogue([[Player: W-well, he's hungry, isn't he? [P] We should give him something to eat.]])
	fnDialogue([[Thought: Stacy nods.]])
	fnDialogue([[Stacy: [E|Neutral]Heck yeah![P][E|Happy] If a dog appreciates my culinary genius, who am I to deny it a delicious grilled cheese, [ITALICS]n'est pas?[ENDITALICS] ]])
	fnDialogue([[Thought: Mari groans, shifting from foot to foot nervously.]])
	fnDialogue([[Thought: Stacy holds out the sandwich to the dog, who is still several yards away.[P] It dashes towards the food, but suddenly stops, a few feet from the three of us.]])
	fnDialogue([[Dog: [Voice|Voice|AJ][FOCUS|AJ][P].[P].[P]. ]])
	fnDialogue([[Thought: The dog eyes us warily.[P] It begins to growl again, crouching low.[EMOTION|AJ|Mad] ]])
	fnDialogue([[Stacy: [E|Sad]I don't think it'll take it from my hand...[P] Hm...]])
	fnDialogue([[Thought: Stacy places the sandwich down on a piece of notebook paper, then steps away.[P] We all do the same.]])
	fnDialogue([[Dog: [Voice|Voice|AJ][FOCUS|AJ][P].[P].[P].]])
	fnDialogue([[Thought: Slowly,[P] slowly,[P] the dog cautiously approaches the food.]])
	fnDialogue([[Thought: It takes a step,[P] then looks at us,[P] then takes a step.[P] All of us hold our breath.]])
	fnDialogue([[Dog: [Voice|Voice|AJ][FOCUS|AJ][E|Blush]*sniff*[P] *sniff*[P] ]])
	fnDialogue([[Thought: The dog nuzzles the sandwich with its nose, throwing out a tentative lick. ]])
	fnDialogue([[Dog: [Voice|Voice|AJ][FOCUS|AJ][P][P][P]Good. ]])
	fnDialogue([[Player: [CRASH]Whoah, what?!?![P] Did that dog just--]])
	fnDialogue([[Thought: Deciding the offering is acceptable, it takes the sandwich in its mouth and dashes off.[P] I look at Mari and Stacy, expecting them to be as shocked as I am.]])
	fnDialogue([[Stacy: [E|Neutral]Ah, you see?[P] It took the sandwich. I'm sure it'll be fine.]])
	fnDialogue([[Mari: [E|Sad]Ugh...[P] I guess so. ]])
	fnDialogue([[[HIDECHAR|AJ]Thought: I guess they didn't hear it.[P] Or maybe I imagined the whole thing.]])
	fnDialogue([[Thought: Taking a deep breath, we continue on our way.]])

--Anything else.
else
	fnDialogue([[Player: I dunno.[P] I feel bad, but I don't want him to get sick.]])
	fnDialogue([[Stacy: [E|Sad]Hm, I guess you're right.[P] Better not risk it.]])
	fnDialogue([[Thought: She puts the sandwich away and re-equips her backpack.[P] Meanwhile, Mari and I watch the dog.]])
	fnDialogue([[Dog: [Voice|Voice|AJ][Focus|AJ][P].[P].[P].]])
	fnDialogue([[Mari: [E|Sad]Poor lil' guuuuuuuy.[P][CRASH][E|Mad] I REALLY WISH WE COULD FEED YOU!!!]])
	fnDialogue([[Thought: The dog sniffles at us, then looks to some nearby bushes.]])
	fnDialogue([[Dog: [Voice|Voice|AJ][Focus|AJ]Hungry.]])
	fnDialogue([[Player: [CRASH]Whoah, what?!?![P] Did that dog just--]])
	fnDialogue([[Thought: It takes to the bushes, dashing out of sight.[P] I look at Mari and Stacy, expecting them to be as shocked as I am.]])
	fnDialogue([[Mari: [E|Blush]I love youuuuuu, doggiiiiiieeeeee!!!!]])
	fnDialogue([[Stacy: [E|Mad]May the force be with you, traveller.]])
	fnDialogue([[Thought: Stacy calmly salutes the bushes into which the dog disappeared.]])
	fnDialogue([[[HIDECHAR|AJ]Thought: I guess they didn't hear it say anything. Or maybe I imagined the whole thing.[P] Taking a deep breath, we continue on our way.]])

end

--Rejoin.
fnDialogue([[Thought: After walking for a few more minutes, I see my house in the distance.]])
fnDialogue([[Player: That's me over there.]])
fnDialogue([[Stacy: [E|Neutral]Hey, you live pretty close to the school![P] Convenient.]])
fnDialogue([[Mari: [E|Neutral]Did you have a good day, overall?]])
fnDialogue([[Thought: I nod.]])
fnDialogue([[Player: Thanks to you guys, I think so.]])
fnDialogue([[Thought: Mari beams, and Stacy playfully scratches her nose, blushing.]])
fnDialogue([[Stacy: [E|Blush]That's good to hear.[P] I was kind of worried that we might be hovering or something.]])
fnDialogue([[Mari: [E|Neutral]If it ever seems like we're getting too up in your biz, like -[P] [CRASH] [E|Mad]JUST LET US KNOW!!! ]])
fnDialogue([[Thought: I shake my head. ]])
fnDialogue([[Player: No, no you guys have been great.[P] You've been really nice.]])
fnDialogue([[Thought: My chest tightens ever so slightly.]])
fnDialogue([[Player: I'm very lucky.[P].[P].[P] I'm very glad we met.]])
fnDialogue([[Mari: [E|Blush]D-don't say that![P] You're gonna make me cryyyyyyy.]])
fnDialogue([[Stacy: [E|Mad]All right, we're gonna bail before the waterworks start... [P]*sniff*!]])
fnDialogue([[[HIDEALL]Thought: I wave at the two girls as they depart. [GLITCH] One more memory begins to surface in the back of my mind[GLITCH].]])
fnDialogue([[Thought: Even though[GLITCHTEXT]EVEN THOUGH IT HURTS[ENDGLITCHTEXT] it hurts a little to think of it, I let myself be transported to it.]])

LM_ExecuteScript(gsRoot .. "Gameplay_SceneC/7 - Flashback.lua")