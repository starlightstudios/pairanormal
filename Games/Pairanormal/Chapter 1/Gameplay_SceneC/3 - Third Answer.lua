--[3 - Third Answer]
--This sucks.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)
	
--Save the game.
if(PairanormalLevel_IsLoading() == false) then
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Starting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

--Set data.
else
	PairanormalLevel_FlagCheckpoint("Starting")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/WalkHome")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/WalkHome")
	fnDialogue([[ [Music|Theme][SHOWCHAR|Mari|Neutral][SHOWCHAR|Stacy|Neutral]Thought: ... ]])
end

--Flag.
PairanormalLevel_FlagCheckpoint("Starting")

--[Execution]
--Messicans!
if(sTopicName == "Oaxaquena") then
	local iQuizPoints = VM_GetVar("Root/Variables/Chapter1/Misc/iQuizPoints", "N")
	VM_SetVar("Root/Variables/Chapter1/Misc/iQuizPoints", "N", iQuizPoints + 1)
	
	--Dialogue.
	fnDialogue([[Stacy: [E|Neutral]Hey, that's exactly right!]])
	fnDialogue([[Mari: [E|Happy]You must have really been into those sandwiches, [PLAYERNAME].]])
	fnDialogue([[Player: They were the perfect blend of buttery,[P] crispy,[P] stretchy,[P] and delicious.]])
	fnDialogue([[Thought: Stacy smiles wide.]])
	fnDialogue([[Stacy: [E|Blush]Hey, thanks! That's really cool of you to pay so much attention, [PLAYERNAME]! ]])
	fnDialogue([[Thought: Mari giggles, wiggling her eyebrows.]])
	fnDialogue([[Mari: [E|Blush]MMMMMmmmmmMMMMmmmm![P] [ITALICS]Cheeeeeesy looooooove connectiooooooon!!![ENDITALICS] ]])
	
--Anything else.
else
	fnDialogue([[Thought: Stacy shakes her head.]])
	fnDialogue([[Stacy: [E|Neutral] Nope![P] The actual cheese I used was [ITALICS]queso Oaxaquena[ENDITALICS], a delicious mexican string cheese.]])
	fnDialogue([[Mari: [E|Neutral]Although you picked a cheese that Stacy [ITALICS]would[ENDITALICS] put in her sandwich.]])
	fnDialogue([[Stacy: [E|Mad]I'm already brewing up my next culinary concoction.]])

end

--Rejoin.
fnDialogue([[Mari: [P][P][P][E|Neutral]Anyway, here's question number FIVE. ]])
fnDialogue([[Stacy: [E|Neutral]Four. We're on the fourth question.]])
fnDialogue([[Mari: [E|Shock]Wh-- are you sure? [P][P][P] *mumble* one, two... three... *mumble*]])
fnDialogue([[Mari: [E|Neutral]Well, whatever. Pretend I skipped one!]])
fnDialogue([[Mari: [E|Mad][CRASH]Question Five- Aside from you,[P][CRASH] WHO is the newest member of the Paranormal Club?]])
fnDialogue([[Stacy: [E|Shock]Oh damn, that's a really good one.]])
fnDialogue([[Mari: [E|Neutral]I know. Thank you.]])
	
--Decision Script.
fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (Who is the newest club member other than me?)") ]])

local sDecisionScript = "\"" .. gsRoot .. "Gameplay_SceneC/4 - Fourth Answer.lua" .. "\""
fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Bret\", " .. sDecisionScript .. ", \"Bret\") ")
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Laura\",  " .. sDecisionScript .. ", \"Laura\") ")
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Michelle\",  " .. sDecisionScript .. ", \"Michelle\") ")
fnCutsceneBlocker()
