-- |[Eilette Presentation]|
--She's not having any of it, is she?

-- |[Arg Resolve]|
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--Constants.
local sAutoAdvance = gsRoot .. "Gameplay_SceneE/0 - End Investigation.lua"

--Saving
local sSaveExecute = gsRoot .. "Gameplay_SceneC/8 - Arrive Home.lua"
local sSaveTopic = "No Topic"
local sSaveFlag = "Investigation"

-- |[Chat Sequences]|
--Chatting.
if(sTopicName == "Eilette" or sTopicName == "Chat") then
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnDialogue([[[HIDEALL][SHOWCHAR|Eilette|Neutral]Player: Hey, Eilette. ]])
	fnDialogue([[Eilette: [E|Neutral]Greetings, [PLAYERNAME].[E|Shock] Hadn't you best be preparing for your tutor's arrival?]])
	fnDialogue([[Player: Sure, I guess so.]])
	fnDialogue([[Eilette: [E|Neutral]On your way, then.]])
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(fnDecrementTime(5, sAutoAdvance) == false) then
		PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
	end
	
--Books.
elseif(sTopicName == "Books") then
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnDialogue([[[HIDEALL][SHOWCHAR|Eilette|Neutral]Eilette: [E|Neutral]Always remember to be a discerning reader, [PLAYERNAME]. Just because something is written in a book doesn't make it true.]])
	fnDialogue([[Eilette: [E|Mad] Especially if that book is labeled \"Fiction\".]])
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(fnDecrementTime(5, sAutoAdvance) == false) then
		PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
	end
	
--Plant.
elseif(sTopicName == "Plant") then
	fnStdInvText("[HIDEALL][SHOWCHAR|Eilette|Neutral]Eilette: [E|Neutral]Domesticated plants are essential for improving household oxygen levels.")
	
	--Time loss.
	if(fnDecrementTime(5, sAutoAdvance) == false) then
		PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
	end

--Magazines.
elseif(sTopicName == "Magazines") then
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnDialogue([[[HIDEALL][SHOWCHAR|Eilette|Neutral]Eilette: [E|Neutral]My most recent magazine subscription is \"Teen Beat\". ]])
	fnDialogue([[Eilette: [E|Happy]If you have questions about small-time celebrity dalliances[P] or are in need of fashion advice,[P] I'm now very well informed on those topics.]])
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(fnDecrementTime(5, sAutoAdvance) == false) then
		PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
	end

--Door.
elseif(sTopicName == "Door") then
	fnStdInvText("[HIDEALL][SHOWCHAR|Eilette|Neutral]Eilette: [E|Mad]Keep that closed, or it gets cold inside.")
	
	--Time loss.
	if(fnDecrementTime(5, sAutoAdvance) == false) then
		PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
	end

--Statues.
elseif(sTopicName == "Statue") then
	fnStdInvText("[HIDEALL][SHOWCHAR|Eilette|Neutral]Eilette: [E|Neutral]I prefer minimalistic decor.")
	
	--Time loss.
	if(fnDecrementTime(5, sAutoAdvance) == false) then
		PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
	end
	
--Jacket.
elseif(sTopicName == "Jacket") then
	fnStdInvText("[HIDEALL][SHOWCHAR|Eilette|Neutral]Eilette: [E|Mad]I'm not fond of the cold.")
	
	--Time loss.
	if(fnDecrementTime(5, sAutoAdvance) == false) then
		PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
	end

--Notepad.
elseif(sTopicName == "Notepad") then
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnDialogue([[[HIDEALL][SHOWCHAR|Eilette|Neutral]Eilette: [E|Neutral]The key to success in any field is staying informed, motivated, and most importantly; organized.]])
	fnDialogue([[Eilette: [E|Happy]All of my day-to-day activities are planned and scheduled for maximum efficiency.]])
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(fnDecrementTime(5, sAutoAdvance) == false) then
		PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
	end

--Kitchen.
elseif(sTopicName == "Kitchen") then
	fnStdInvText("[HIDEALL][SHOWCHAR|Eilette|Neutral]Eilette: [E|Neutral]When the tutor arrives, situate yourselves in the kitchen.[P] It should have adequate lighting, space, and comfort for your study session.")
	
	--Time loss.
	if(fnDecrementTime(5, sAutoAdvance) == false) then
		PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
	end

end
