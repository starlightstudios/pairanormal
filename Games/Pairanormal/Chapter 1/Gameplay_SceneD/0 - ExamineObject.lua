--[Investigation of the Living Room]
--UNCOVER ALL SECRETS.

--[Arguments]
--Get the name of what we're investigating.
local sExamine = LM_GetScriptArgument(0)

--[Variable Setup]
--Used to simplify the Add Discovered Object calls.
local sBGPath = "Root/Images/Backgrounds/All/LivingRoom"
local sScriptPath = gsRoot .. "Gameplay_SceneD/0 - ExamineObject.lua"
local sAutoAdvance = gsRoot .. "Gameplay_SceneE/0 - End Investigation.lua"

--Saving
local sSaveExecute = gsRoot .. "Gameplay_SceneC/8 - Arrive Home.lua"
local sSaveTopic = "No Topic"
local sSaveFlag = "Investigation"

--[Present Eilette To Herself]
if(sExamine == "Eilette") then

	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnDialogue([[[HIDEALLFAST][SHOWCHAR|Eilette|Neutral]Player: Hey, Eilette. ]])
	fnDialogue([[Eilette: [E|Neutral]Greetings, [PLAYERNAME].[E|Shock] Hadn't you best be preparing for your tutor's arrival?]])
	fnDialogue([[Player: Sure, I guess so.]])
	fnDialogue([[Eilette: [E|Neutral]On your way, then.]])
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(fnDecrementTime(5, sAutoAdvance) == false) then
		PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
	end

--[Books]
elseif(sExamine == "Books") then
	
	--Store it in the inventory.
	local i = gciInvB_Books
	Dia_SetProperty("Add Discovered Object", gciaObjB[i][1], sBGPath, sScriptPath, gciaObjB[i][1])
	local iExaminedAlready = VM_GetVar("Root/Variables/Chapter1/InvestigationB/iObject|" .. gciaObjB[i][1], "N")
	VM_SetVar("Root/Variables/Chapter1/InvestigationB/iObject|" .. gciaObjB[i][1], "N", 1.0)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Player: I can already tell I'll never read anything in these boring tomes.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(iExaminedAlready == 0.0) then
		if(fnDecrementTime(1, sAutoAdvance) == false) then
			PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
		end
	end
	
--[Plant]
elseif(sExamine == "Plant") then
	
	--Store it in the inventory.
	local i = gciInvB_Plant
	Dia_SetProperty("Add Discovered Object", gciaObjB[i][1], sBGPath, sScriptPath, gciaObjB[i][1])
	local iExaminedAlready = VM_GetVar("Root/Variables/Chapter1/InvestigationB/iObject|" .. gciaObjB[i][1], "N")
	VM_SetVar("Root/Variables/Chapter1/InvestigationB/iObject|" .. gciaObjB[i][1], "N", 1.0)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Player: This little shrub adds some much needed life into the room.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(iExaminedAlready == 0.0) then
		if(fnDecrementTime(1, sAutoAdvance) == false) then
			PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
		end
	end

--[Magazines]
elseif(sExamine == "Magazines") then
	
	--Store it in the inventory.
	local i = gciInvB_Magazines
	Dia_SetProperty("Add Discovered Object", gciaObjB[i][1], sBGPath, sScriptPath, gciaObjB[i][1])
	local iExaminedAlready = VM_GetVar("Root/Variables/Chapter1/InvestigationB/iObject|" .. gciaObjB[i][1], "N")
	VM_SetVar("Root/Variables/Chapter1/InvestigationB/iObject|" .. gciaObjB[i][1], "N", 1.0)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: I'm fairly certain Eilette's home decor tastes are entirely based off of the pages of this office furniture catalogue.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(iExaminedAlready == 0.0) then
		if(fnDecrementTime(1, sAutoAdvance) == false) then
			PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
		end
	end

--[Door]
elseif(sExamine == "Door") then
	
	--Store it in the inventory.
	local i = gciInvB_Door
	Dia_SetProperty("Add Discovered Object", gciaObjB[i][1], sBGPath, sScriptPath, gciaObjB[i][1])
	local iExaminedAlready = VM_GetVar("Root/Variables/Chapter1/InvestigationB/iObject|" .. gciaObjB[i][1], "N")
	VM_SetVar("Root/Variables/Chapter1/InvestigationB/iObject|" .. gciaObjB[i][1], "N", 1.0)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: I can't count the number of times I've considered running through that, but to where?") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(iExaminedAlready == 0.0) then
		if(fnDecrementTime(1, sAutoAdvance) == false) then
			PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
		end
	end

--[Statues]
elseif(sExamine == "Statue") then
	
	--Store it in the inventory.
	local i = gciInvB_Statues
	Dia_SetProperty("Add Discovered Object", gciaObjB[i][1], sBGPath, sScriptPath, gciaObjB[i][1])
	local iExaminedAlready = VM_GetVar("Root/Variables/Chapter1/InvestigationB/iObject|" .. gciaObjB[i][1], "N")
	VM_SetVar("Root/Variables/Chapter1/InvestigationB/iObject|" .. gciaObjB[i][1], "N", 1.0)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: These statues still have the pricetag on them[P][CRASH]--whoah, they're super expensive!") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(iExaminedAlready == 0.0) then
		if(fnDecrementTime(1, sAutoAdvance) == false) then
			PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
		end
	end

--[Jacket]
elseif(sExamine == "Jacket") then
	
	--Store it in the inventory.
	local i = gciInvB_Jacket
	Dia_SetProperty("Add Discovered Object", gciaObjB[i][1], sBGPath, sScriptPath, gciaObjB[i][1])
	local iExaminedAlready = VM_GetVar("Root/Variables/Chapter1/InvestigationB/iObject|" .. gciaObjB[i][1], "N")
	VM_SetVar("Root/Variables/Chapter1/InvestigationB/iObject|" .. gciaObjB[i][1], "N", 1.0)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: I wonder if Eilette has her clothes specially tailored,[P] or if she makes 'em cling perfectly to her form with sheer willpower.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(iExaminedAlready == 0.0) then
		if(fnDecrementTime(1, sAutoAdvance) == false) then
			PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
		end
	end

--[Notepad]
elseif(sExamine == "Notepad") then
	
	--Store it in the inventory.
	local i = gciInvB_Notepad
	Dia_SetProperty("Add Discovered Object", gciaObjB[i][1], sBGPath, sScriptPath, gciaObjB[i][1])
	local iExaminedAlready = VM_GetVar("Root/Variables/Chapter1/InvestigationB/iObject|" .. gciaObjB[i][1], "N")
	VM_SetVar("Root/Variables/Chapter1/InvestigationB/iObject|" .. gciaObjB[i][1], "N", 1.0)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: Well I'll give her this;[P] Eilette is probably the most organized person I know.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(iExaminedAlready == 0.0) then
		if(fnDecrementTime(1, sAutoAdvance) == false) then
			PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
		end
	end

--[Kitchen]
elseif(sExamine == "Kitchen") then
	
	--Store it in the inventory.
	local i = gciInvB_Kitchen
	Dia_SetProperty("Add Discovered Object", gciaObjB[i][1], sBGPath, sScriptPath, gciaObjB[i][1])
	local iExaminedAlready = VM_GetVar("Root/Variables/Chapter1/InvestigationB/iObject|" .. gciaObjB[i][1], "N")
	VM_SetVar("Root/Variables/Chapter1/InvestigationB/iObject|" .. gciaObjB[i][1], "N", 1.0)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: Eilette tells me she subscribes to a \"raw vegan diet\", which I'm pretty sure means she doesn't know how to cook.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(iExaminedAlready == 0.0) then
		if(fnDecrementTime(1, sAutoAdvance) == false) then
			PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
		end
	end

--Eilette, examination.
elseif(sExamine == "Eilette") then

	local sScriptPath = gsRoot .. "Gameplay_SceneD/0 - ExamineObject.lua"
	Dia_SetProperty("Add Discovered Object", "Eilette", sCharPath, sScriptPath, "Eilette", 442, 2, 442 + 438, 2 + 363)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: She's my \"caretaker\", is probably older than me, and has a fondness for winter coats. That is literally all I know about her.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(fnDecrementTime(1, sAutoAdvance) == false) then
		PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
	end
	
end