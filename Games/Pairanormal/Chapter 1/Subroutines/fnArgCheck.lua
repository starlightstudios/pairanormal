--[Function: fnArgCheck]
--Standardized argument checker, using arguments held by the LuaManager. Returns true if the arguments pass
-- muster, false if they don't.
--Built from: ./ZLaunch.lua
--Example: fnArgCheck(2)
function fnArgCheck(iRequiredArgs)
	local iArgs = LM_GetNumOfArgs()
	if(iArgs < iRequiredArgs) then
		Debug_ForcePrint(fnResolvePath() .. fnResolveName() .. ": Error, incorrect argument count. Got " .. iArgs .. " expected " .. iRequiredArgs .. ".\n")
		return false
	end
	
	return true
end