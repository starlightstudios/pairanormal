--[Function: fnDecrementTime]
--Reduces the timer for investigations by the listed amount. Fires the listed script 
--Returns true if it executed the next section because time ran out, false otherwise.
--Built from: ./ZLaunch.lua
--Example: local bForcedNextSection = fnDecrementTime(5, gsRoot .. "Next Script.lua")
function fnDecrementTime(iTimeLoss, sNextScript)

	--Argument check.
	if(iTimeLoss   == nil) then return false end
	if(sNextScript == nil) then return false end
    
    --Determine which variables to use.
    local sVarPath = VM_GetVar("Root/Variables/System/Investigation/sInvestigationPath", "S")
	
	--Time Loss.
	local iTimeLeft = Dia_SetProperty("Get Investigation Time")
	Dia_SetProperty("Investigation Time Left", iTimeLeft - iTimeLoss)
	VM_SetVar(sVarPath .. "iTimeLeft", "N", iTimeLeft - iTimeLoss)
	
	--Force script.
	if(iTimeLeft - iTimeLoss < 1) then
		LM_ExecuteScript(sNextScript, "Bypass")
		return true
	end

	return false
end