--[Function: fnDialogue]
--Simple dialogue. Automatically adds the cutscene blocker afterwards.
--Built from: ./ZLaunch.lua
--Example: fnDialogue("[SHOWCHAR|Mari|Neutral]Mari: Hi, I'm Mari!")
function fnDialogue(sDialogue)
	
	local sString = "Dia_SetProperty(\"Set Dialogue\", \"" .. sDialogue .. "\")"
	
	fnCutsceneInstruction(sString)
	fnCutsceneBlocker()
end