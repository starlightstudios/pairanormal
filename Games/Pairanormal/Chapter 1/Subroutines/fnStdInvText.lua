--[Function: fnStdInvText]
--Creates a standard investigation text display. Used for one-line responses.
--Built from: ./ZLaunch.lua
--Example: fnStdInvText("Yep, that's a cow all right.")
function fnStdInvText(sString)

	--Argument check.
	if(sString == nil) then return end
	
	--Build the string we'll be using.
	local sBuildString = "Dia_SetProperty(\"Set Dialogue\", \"" .. sString .. "\")"
	
	--Investigations always use the same basic setup.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction(sBuildString)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()

end