--[Function: fnUnlockGallery]
--Unlocks the provided gallery image. Done in the event queue.
--Built from: ./ZLaunch.lua
--Example: fnUnlockGallery("Gallery_Hospital")
function fnUnlockGallery(sVariableName)
	
    if(sVariableName == nil) then return end
    
	local sString = "VM_SetVar(\"Root/Variables/Gallery/All/" .. sVariableName .. "\", \"N\", 1)"
	
	fnCutsceneInstruction(sString)
end