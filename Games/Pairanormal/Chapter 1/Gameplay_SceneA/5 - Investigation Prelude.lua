--[5 - Investigation Prelude]
--Run up to the first investigation sequence.

--[Execution]
--Save the game.
local sTopicName = "No Topic"
if(fnArgCheck(1) == true) then
    sTopicName = LM_GetScriptArgument(0)
end
	
--Save the game.
if(PairanormalLevel_IsLoading() == false) then
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Starting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

--Set data.
else
	PairanormalLevel_FlagCheckpoint("Starting")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Classroom")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Classroom")
	fnDialogue([[ [Music|Daytime]Thought: ... ]])
end

--[Standard]
--Normal gameplay.
if(sTopicName == "No Topic") then
    fnDialogue("Thought: [BG|Root/Images/Backgrounds/All/Classroom][Music|NULL]A few hours have passed, and the hallway outside our classroom has become completely empty and quiet.")
    fnDialogue("Thought: Club members snack on their leftover lunches and the club's party snacks, milling about and chatting amongst themselves, ready-but-not-quite-ready to head home.")
    fnDialogue("Player: [Music|Daytime]Right. I guess I just... relax? Hang out? I probably shouldn't leave without at least talking to everyone a little bit.")
    fnDialogue([[Info: This is the game's \"Investigate\" Mode. Explore the room by clicking on objects of interest.]])
	fnDialogue("Info: You can also talk to people nearby. Click the icon in the bottom left corner to switch modes.")
    fnDialogue("Info: The bottom bar has found objects. Show them to someone and see what happens! ")
    fnDialogue([[Info: If you're done investigating and want to continue in the story, click the House icon.]])
    fnDialogue("Info: The game saves your actions automatically. Exit anytime by clicking the Red X icon.")

--Tutorial only.
elseif(sTopicName == "Tutorial") then
    nDialogue([[Info: This is the game's \"Investigate\" Mode. Explore the room by clicking on objects of interest.]])
	fnDialogue("Info: You can also talk to people nearby. Click the icon in the bottom left corner to switch modes.")
    fnDialogue("Info: The bottom bar has found objects. Show them to someone and see what happens! ")
    fnDialogue([[Info: If you're done investigating and want to continue in the story, click the House icon.]])
    fnDialogue("Info: The game saves your actions automatically. Exit anytime by clicking the Red X icon.")
    fnDialogue([[Info: In this demo copy, the story ends immediately after the investigation.]])
    fnDialogue("Info: To restart the demo, press the X icon.")

--Bypass tutorial.
elseif(sTopicName == "SkipTut") then

end

--[Loading the Game]
--If currently loading the game, set up the investigation.
if(PairanormalLevel_IsLoading() == true) then
	
	--Variables.
	local sBGPath = "Root/Images/Backgrounds/All/Classroom"
	local sScriptPath = gsRoot .. "Gameplay_SceneB/0 - ExamineObject.lua"
	
	--Change the background.
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Classroom")

	--Boot investigation scripts.
	Dia_SetProperty("Clear Investigation Characters")
	Dia_SetProperty("Clear Investigation")
	Dia_SetProperty("Start Investigation") 
	Dia_SetProperty("Set Investigation Image", "Root/Images/Backgrounds/All/LivingRoom")
	Dia_SetProperty("Set Investigation Script", gsRoot .. "Gameplay_SceneB/0 - ExamineObject.lua")
	Dia_SetProperty("Set Go Home Strings", gsRoot .. "Gameplay_SceneC/0 - Going Home.lua", "Default")
	
	--Talkable Characters.
	Dia_SetProperty("Add Talkable Character", "Mari",     gsRoot .. "Gameplay_SceneB/1 - Present Mari.lua")
	Dia_SetProperty("Add Talkable Character", "Stacy",    gsRoot .. "Gameplay_SceneB/2 - Present Stacy.lua")
	Dia_SetProperty("Add Talkable Character", "Bret",     gsRoot .. "Gameplay_SceneB/3 - Present Bret.lua")
	Dia_SetProperty("Add Talkable Character", "Laura",    gsRoot .. "Gameplay_SceneB/4 - Present Laura.lua")
	Dia_SetProperty("Add Talkable Character", "Michelle", gsRoot .. "Gameplay_SceneB/5 - Present Michelle.lua")
	
	--Talkable characters as inventory objects.
	local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
	Dia_SetProperty("Add Discovered Object", "Mari",     sCharPath, sScriptPath, "Mari",     1322,   2, 1322 + 438,   2 + 363)
	Dia_SetProperty("Add Discovered Object", "Stacy",    sCharPath, sScriptPath, "Stacy",     442, 367,  442 + 438, 367 + 363)
	Dia_SetProperty("Add Discovered Object", "Bret",     sCharPath, sScriptPath, "Bret",        2,   2,    2 + 438,   2 + 363)
	Dia_SetProperty("Add Discovered Object", "Laura",    sCharPath, sScriptPath, "Laura",     882,   2,  882 + 438,   2 + 363)
	Dia_SetProperty("Add Discovered Object", "Michelle", sCharPath, sScriptPath, "Michelle", 1762,   2, 1762 + 438,   2 + 363)
	Dia_SetProperty("Clear Discovered Object")
    
    --SLF file adds the examinable objects.
    Dia_SetProperty("Parse SLF File", gsRoot .. "MapData/InvestigationAA.slf")
	
	--Check all the items and re-add them to the inventory.
	for i = 1, gciInvA_Total-1, 1 do
		
		--Variable name.
		local sVarName = "Root/Variables/Chapter1/InvestigationA/iObject|" .. gczaObjA[i][1]
		local iHasItem = VM_GetVar(sVarName, "N")
		
		--Has the item. Add it.
		if(iHasItem == 1.0) then
			Dia_SetProperty("Add Discovered Object", gczaObjA[i][1], sBGPath, sScriptPath, gczaObjA[i][1])
		end
		Dia_SetProperty("Clear Discovered Object")
	end

	--Modify the time max/left. These are always -1 during this scene.
	Dia_SetProperty("Investigation Time Max",  -1)
	Dia_SetProperty("Investigation Time Left", -1)

--[Normal Operations]
--Otherwise, flag the game to save and boot the investigation normally.
else

	--Save the game.
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Investigation\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

	--Set the time max/left.
	fnCutsceneInstruction([[ VM_SetVar("Root/Variables/Chapter1/Investigation/iTimeMax",  "N", -1.0) ]])
	fnCutsceneInstruction([[ VM_SetVar("Root/Variables/Chapter1/Investigation/iTimeLeft", "N", -1.0) ]])

	--Boot investigation scripts.
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Investigation Characters") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Investigation Time Max", -1) ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Investigation Time Left", -1) ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Set Investigation Image", "Root/Images/Backgrounds/All/Classroom") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Set Investigation Script", gsRoot .. "Gameplay_SceneB/0 - ExamineObject.lua") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Set Go Home Strings", gsRoot .. "Gameplay_SceneC/0 - Going Home.lua", "Default") ]])
	
	--Talkable characters.
	fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Mari",     gsRoot .. "Gameplay_SceneB/1 - Present Mari.lua") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Stacy",    gsRoot .. "Gameplay_SceneB/2 - Present Stacy.lua") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Bret",     gsRoot .. "Gameplay_SceneB/3 - Present Bret.lua") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Laura",    gsRoot .. "Gameplay_SceneB/4 - Present Laura.lua") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Michelle", gsRoot .. "Gameplay_SceneB/5 - Present Michelle.lua") ]])

    --SLF file adds the examinable objects.
    fnCutsceneInstruction([[ Dia_SetProperty("Parse SLF File", gsRoot .. "MapData/InvestigationAA.slf") ]])
	
	--Add the talkable characters as the first examine objects.
	fnCutsceneInstruction([[ Dia_SetProperty("Add Discovered Object", "Mari",     "Root/Images/Backgrounds/All/CharPortraits", gsRoot .. "Gameplay_SceneB/0 - ExamineObject.lua", "Mari",     1322,   2, 1322 + 438,   2 + 363) ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Add Discovered Object", "Stacy",    "Root/Images/Backgrounds/All/CharPortraits", gsRoot .. "Gameplay_SceneB/0 - ExamineObject.lua", "Stacy",     442, 367,  442 + 438, 367 + 363) ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Add Discovered Object", "Bret",     "Root/Images/Backgrounds/All/CharPortraits", gsRoot .. "Gameplay_SceneB/0 - ExamineObject.lua", "Bret",        2,   2,    2 + 438,   2 + 363) ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Add Discovered Object", "Laura",    "Root/Images/Backgrounds/All/CharPortraits", gsRoot .. "Gameplay_SceneB/0 - ExamineObject.lua", "Laura",     882,   2,  882 + 438,   2 + 363) ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Add Discovered Object", "Michelle", "Root/Images/Backgrounds/All/CharPortraits", gsRoot .. "Gameplay_SceneB/0 - ExamineObject.lua", "Michelle", 1762,   2, 1762 + 438,   2 + 363) ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	
	fnCutsceneBlocker()
end

--Checkpoint notification.
PairanormalLevel_FlagCheckpoint("Investigation")
fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Investigation\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")