--[Exec First]
--Executes when the program starts. This will occur with the argument "Start".

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--[Execution]
--Character showcase time.
if(sTopicName == "Start") then
    
    --Load everyone.
    LM_ExecuteScript(gsRoot .. "../Chapter 2/System/200 Dialogue Actors.lua")
    LM_ExecuteScript(gsRoot .. "../Chapter 3/System/200 Dialogue Actors.lua")
    LM_ExecuteScript(gsRoot .. "../Chapter 4/System/200 Dialogue Actors.lua")
    LM_ExecuteScript(gsRoot .. "../Chapter 5/System/200 Dialogue Actors.lua")
    
    fnDialogue("[CHARENTER|Eilette]Eilette: [E|N]Ah, [PLAYERNAME]. [E|Shock]Finished already, are you?")
    
    --[=[
    fnDialogue("[HIDEALL][SHOWCHAR|PlayerFlash]PlayerFlash:[E|Neutral] Neutral.")
    fnDialogue("[HIDEALL][SHOWCHAR|PlayerFlash]PlayerFlash:[E|Mad] Mad.")
    fnDialogue("[HIDEALL][SHOWCHAR|SquirrelBob]SquirrelBob: Neutral.")
    fnDialogue("[HIDEALL][SHOWCHAR|SquirrelPuppet]SquirrelPuppet: Neutral.")
    fnDialogue("[HIDEALL][SHOWCHAR|Squirrel]Squirrel: Neutral.")
    fnDialogue("[HIDEALL][SHOWCHAR|SquirrelDoll]SquirrelDoll: Neutral.")
    
    fnDialogue("[HIDEALL][SHOWCHAR|AJBackpack]AJ: [IMAGESCROLL|Root/Images/Backgrounds/All/RunHome][VOICE|Voice|AJ]Thought bubble 1.")
    fnDialogue("AJ: [VOICE|Voice|AJ]Thought bubble 2.")
    fnDialogue("[CHAPTEREND|Root/Images/Backgrounds/All/EndOfChapterOne] ")
    fnDialogue("AJ: [VOICE|Voice|AJ]Thought bubble 3.")
    fnDialogue("AJ: [VOICE|Voice|AJ]Thought bubble 4.")
    
    fnDialogue("[HIDEALL][SHOWCHAR|Cellphone]Cellphone:[E|Neutral] Neutral.")
    fnDialogue("[HIDEALL][SHOWCHAR|ElectronicDiary]ElectronicDiary:[E|Neutral] Neutral.")
    fnDialogue("[HIDEALL][SHOWCHAR|Walkie]Walkie:[E|Neutral] Neutral.")
    
    fnDialogue("[HIDEALL][SHOWCHAR|ADAM]ADAM: Madam I'm ADAM.")
    fnDialogue("ADAM: [E|Happy]Happy.")
    fnDialogue("ADAM: [E|Sad]Sad.")
    fnDialogue("ADAM: [E|Shock]Shock.")
    
    fnDialogue("[HIDEALL][SHOWCHAR|AJ]AJ:[E|Neutral] Neutral.")
    fnDialogue("AJ: [E|Happy]Happy.")
    fnDialogue("AJ: [E|Blush]Blush.")
    fnDialogue("AJ: [E|Sad]Sad.")
    fnDialogue("AJ: [E|Shock]Shock.")
    fnDialogue("AJ: [E|Mad]Mad.")
    
    fnDialogue("[HIDEALL][SHOWCHAR|Bret]Bret:[E|Neutral] Neutral.")
    fnDialogue("Bret: [E|Happy]Happy.")
    fnDialogue("Bret: [E|Blush]Blush.")
    fnDialogue("Bret: [E|Sad]Sad.")
    fnDialogue("Bret: [E|Shock]Shock.")
    fnDialogue("Bret: [E|Mad]Mad.")
    
    fnDialogue("[HIDEALL][SHOWCHAR|Candace]Candace:[E|Neutral] Neutral.")
    fnDialogue("Candace: [E|Happy]Happy.")
    fnDialogue("Candace: [E|Sad]Sad.")
    fnDialogue("Candace: [E|Shock]Shock.")
    fnDialogue("Candace: [E|Mad]Mad.")
    
    fnDialogue("[HIDEALL][SHOWCHAR|Cop]Cop: Neutral.")

    fnDialogue("[HIDEALL][SHOWCHAR|Director]Director: Yes.")
    
    fnDialogue("[HIDEALL][SHOWCHAR|Eilette]Eilette:[E|Neutral] Neutral.")
    fnDialogue("Eilette: [E|Happy]Happy.")
    fnDialogue("Eilette: [E|Blush]Blush.")
    fnDialogue("Eilette: [E|Sad]Sad.")
    fnDialogue("Eilette: [E|Shock]Shock.")
    fnDialogue("Eilette: [E|Mad]Mad.")

    fnDialogue("[HIDEALL][SHOWCHAR|EiletteO2]EiletteO2:[E|Neutral] Neutral")
    fnDialogue("EiletteO2: [E|Happy]Happy.")
    fnDialogue("EiletteO2: [E|Mad]Mad.")
    fnDialogue("EiletteO2: [E|Sad]Sad.")
    fnDialogue("EiletteO2: [E|Shock]Shock.")
    fnDialogue("EiletteO2: [E|Blush]Blush.")
    
    fnDialogue("[HIDEALL][SHOWCHAR|Elizabeth]Elizabeth: Neutral.")
    
    fnDialogue("[HIDEALL][SHOWCHAR|Harold]Harold: Neutral.")
    
    fnDialogue("[HIDEALL][SHOWCHAR|Harper]Harper:[E|Neutral] Neutral.")
    fnDialogue("Harper: [E|Happy]Happy.")
    fnDialogue("Harper: [E|Blush]Blush.")
    fnDialogue("Harper: [E|Sad]Sad.")
    fnDialogue("Harper: [E|Shock]Shock.")
    fnDialogue("Harper: [E|Mad]Mad.")
    
    fnDialogue("[HIDEALL][SHOWCHAR|Kane]Kane: Neutral.")

    fnDialogue("[HIDEALL][SHOWCHAR|Kid1]Kid1: Neutral.")
    fnDialogue("[HIDEALL][SHOWCHAR|Kid2]Kid2: Neutral.")
    fnDialogue("[HIDEALL][SHOWCHAR|Kid3]Kid3: Neutral.")
    
    fnDialogue("[HIDEALL][SHOWCHAR|Laura]Laura:[E|Neutral] Neutral.")
    fnDialogue("Laura: [E|Happy]Happy.")
    fnDialogue("Laura: [E|Blush]Blush.")
    fnDialogue("Laura: [E|Sad]Sad.")
    fnDialogue("Laura: [E|Shock]Shock.")
    fnDialogue("Laura: [E|Mad]Mad.")
    
    fnDialogue("[HIDEALL][SHOWCHAR|Mari]Mari:[E|Neutral] Neutral.")
    fnDialogue("Mari: [E|Happy]Happy.")
    fnDialogue("Mari: [E|Blush]Blush.")
    fnDialogue("Mari: [E|Sad]Sad.")
    fnDialogue("Mari: [E|Shock]Shock.")
    fnDialogue("Mari: [E|Mad]Mad.")
    
    fnDialogue("[HIDEALL][SHOWCHAR|MariHospital]MariHospital:[E|Neutral] Neutral.")
    fnDialogue("MariHospital: [E|Happy]Happy.")
    fnDialogue("MariHospital: [E|Blush]Blush.")
    fnDialogue("MariHospital: [E|Sad]Sad.")
    fnDialogue("MariHospital: [E|Shock]Shock.")
    fnDialogue("MariHospital: [E|Mad]Mad.")
    
    fnDialogue("[HIDEALL][SHOWCHAR|Michelle]Michelle:[E|Neutral] Neutral.")
    fnDialogue("Michelle: [E|Happy]Happy.")
    fnDialogue("Michelle: [E|Blush]Blush.")
    fnDialogue("Michelle: [E|Sad]Sad.")
    fnDialogue("Michelle: [E|Shock]Shock.")
    fnDialogue("Michelle: [E|Mad]Mad.")
    
    fnDialogue("[HIDEALL][SHOWCHAR|Mistress]Mistress:[E|Neutral] Neutral.")
    fnDialogue("Mistress: [E|Happy]Happy.")
    fnDialogue("Mistress: [E|Mad]Mad.")

    fnDialogue("[HIDEALL][SHOWCHAR|Nelson]Nelson: Neutral.")

    fnDialogue("[HIDEALL][SHOWCHAR|Nurse]Nurse: Neutral.")

    fnDialogue("[HIDEALL][SHOWCHAR|PlayerFlash]PlayerFlash:[E|Neutral] Neutral.")
    fnDialogue("PlayerFlash: [E|Mad]Mad.")
    
    fnDialogue("[HIDEALL][SHOWCHAR|Reich]Reich:[E|Neutral] Neutral.")
    fnDialogue("Reich: [E|Happy]Happy.")
    fnDialogue("Reich: [E|Blush]Blush.")
    fnDialogue("Reich: [E|Sad]Sad.")
    fnDialogue("Reich: [E|Shock]Shock.")
    fnDialogue("Reich: [E|Mad]Mad.")
    
    fnDialogue("[HIDEALL][SHOWCHAR|Leif]Leif:[E|Neutral] Neutral.")
    fnDialogue("Leif: [E|Happy]Happy.")
    fnDialogue("Leif: [E|Blush]Blush.")
    fnDialogue("Leif: [E|Sad]Sad.")
    fnDialogue("Leif: [E|Shock]Shock.")
    fnDialogue("Leif: [E|Mad]Mad.")
    
    fnDialogue("[HIDEALL][SHOWCHAR|Ross]Ross:[E|Neutral] Neutral.")
    fnDialogue("Ross: [E|Happy]Happy.")
    fnDialogue("Ross: [E|Blush]Blush.")
    fnDialogue("Ross: [E|Sad]Sad.")
    fnDialogue("Ross: [E|Shock]Shock.")
    fnDialogue("Ross: [E|Mad]Mad.")
    
    fnDialogue("[HIDEALL][SHOWCHAR|Ronnie]Ronnie:[E|Neutral] Neutral.")

    fnDialogue("[HIDEALL][SHOWCHAR|Six]Six:[E|Neutral] Neutral.")
    fnDialogue("Six: [E|Happy]Happy.")
    fnDialogue("Six: [E|Sad]sad.")

    fnDialogue("[HIDEALL][SHOWCHAR|Squirrel]Squirrel: Neutral.")

    fnDialogue("[HIDEALL][SHOWCHAR|SquirrelDoll]SquirrelDoll: Neutral.")
    
    fnDialogue("[HIDEALL][SHOWCHAR|Stacy]Stacy:[E|Neutral] Neutral.")
    fnDialogue("Stacy: [E|Happy]Happy.")
    fnDialogue("Stacy: [E|Blush]Blush.")
    fnDialogue("Stacy: [E|Sad]Sad.")
    fnDialogue("Stacy: [E|Shock]Shock.")
    fnDialogue("Stacy: [E|Mad]Mad.")
    
    fnDialogue("[HIDEALL][SHOWCHAR|StacyHospital]StacyHospital:[E|Neutral] Neutral.")
    fnDialogue("StacyHospital: [E|Happy]Happy.")
    fnDialogue("StacyHospital: [E|Blush]Blush.")
    fnDialogue("StacyHospital: [E|Sad]Sad.")
    fnDialogue("StacyHospital: [E|Shock]Shock.")
    fnDialogue("StacyHospital: [E|Mad]Mad.")
    
    fnDialogue("[HIDEALL][SHOWCHAR|Student1]Student1: Neutral.")
    fnDialogue("[HIDEALL][SHOWCHAR|Student2]Student2: Neutral.")
    fnDialogue("[HIDEALL][SHOWCHAR|Student3]Student3: Neutral.")
    fnDialogue("[HIDEALL][SHOWCHAR|Student4]Student4: Neutral.")
    fnDialogue("[HIDEALL][SHOWCHAR|Student5]Student5: Neutral.")
    fnDialogue("[HIDEALL][SHOWCHAR|Student6]Student6: Neutral.")
    fnDialogue("[HIDEALL][SHOWCHAR|Teacher1]Teacher1: Neutral.")
    fnDialogue("[HIDEALL][SHOWCHAR|Teacher2]Teacher2: Neutral.")
    fnDialogue("[HIDEALL][SHOWCHAR|Teacher3]Teacher3: Neutral.")
    
    fnDialogue("[HIDEALL][SHOWCHAR|Terry]Terry:[E|Neutral] Neutral.")
    fnDialogue("Terry: [E|Happy]Happy.")
    fnDialogue("Terry: [E|Blush]Blush.")
    fnDialogue("Terry: [E|Sad]Sad.")
    fnDialogue("Terry: [E|Shock]Shock.")
    fnDialogue("Terry: [E|Mad]Mad.")

    fnDialogue("[HIDEALL][SHOWCHAR|Three]Three:[E|Neutral] Neutral.")
    fnDialogue("Three: [E|Blush]Blush.")
    fnDialogue("Three: [E|Happy]Happy.")
    fnDialogue("Three: [E|Mad]Mad.")
    fnDialogue("Three: [E|Sad]Sad.")
    fnDialogue("Three: [E|Shock]Shocked.")

    fnDialogue("[HIDEALL][SHOWCHAR|Thug1]Thug1: Neutral.")
    fnDialogue("[HIDEALL][SHOWCHAR|Thug2]Thug2: Neutral.")
    fnDialogue("[HIDEALL][SHOWCHAR|YStacy]YStacy: Neutral.")
    
    fnDialogue("[HIDEALL][SHOWCHAR|Todd]Todd:[E|Neutral] Neutral.")
    fnDialogue("Todd: [E|Happy]Happy.")
    fnDialogue("Todd: [E|Blush]Blush.")
    fnDialogue("Todd: [E|Sad]Sad.")
    fnDialogue("Todd: [E|Shock]Shock.")
    fnDialogue("Todd: [E|Mad]Mad.")

    fnDialogue("[HIDEALL][SHOWCHAR|YStacyHos]YStacyHos:[E|Neutral] Neutral.")
    fnDialogue("YStacyHos: [E|Sad]Sad.")
    fnDialogue("YStacyHos: [E|Happy]Happy.")

    fnDialogue("[HIDEALL][SHOWCHAR|YTodd]YTodd: Neutral.")]=]
    
	--Level Skip.
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "(Now What?)") ]])

	local sDecisionScript = "\"" .. gsRoot .. "Gameplay_SceneA/Z - Character Showcase.lua\""
	fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Replay\", " .. sDecisionScript .. ", \"Start\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Done\", " .. sDecisionScript .. ", \"GoToChapter1\") ")
	fnCutsceneBlocker()
elseif(sTopicName == "GoToChapter1") then
    LM_ExecuteScript(gsRoot .. "Gameplay_SceneA/0 - Meeting.lua", "LevelSkipBoot")
end
