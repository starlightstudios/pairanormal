-- |[Exec First]|
--Executes when the program starts. This will occur with the argument "Start".

-- |[Arg Resolve]|
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

-- |[Execution]|
--Demo. Used for a demo that was given out a while ago, I left it here for nostalgic purposes only.
if(sTopicName == "Demo") then
	fnDialogue("Thought:[BG|Root/Images/Backgrounds/All/Classroom]Please enter your character's name. Saving is disabled in this demo.")
    Dia_SetProperty("Name Entry String", "My Name Is")
	fnDialogue("Thought:[ACTIVATENAMEENTRY]...")
	fnDialogue("Thought:Your name is [PLAYERNAME].")

	--What to tell them you like eating:
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: Would you like a description of how investigations work?") ]])

	local sDecisionScript = "\"" .. gsRoot .. "Gameplay_SceneA/5 - Investigation Prelude.lua\""
	fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Tutorial is Yes\", " .. sDecisionScript .. ", \"Tutorial\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"No thanks\",  " .. sDecisionScript .. ", \"SkipTut\") ")
	fnCutsceneBlocker()

--Start.
elseif(sTopicName == "Start") then

	--Dialogue sequence.
	fnUnlockGallery("Gallery_Classroom")
	fnDialogue("Thought:[BG|Root/Images/Backgrounds/All/Classroom][SFX|World|Crash][MUSIC|Null] [SPEEDTEXT]*PAM*[P] [SFX|World|Crash] *PAM*[P] [SFX|World|Crash] *PAM*[ENDSPEEDTEXT]")
	fnDialogue("[SHOWCHAR|Mari|Neutral]Mari:[E|Neutral][Music|Daytime]Welcome everyone!")
	fnDialogue("Mari:[E|Happy]Welcome to the very first PARANORMAL INVESTIGATION CLUB MEETING of the year!!!")
	fnDialogue("[SHOWCHAR|Laura|Neutral][SHOWCHAR|Bret|Neutral][SHOWCHAR|Michelle|Neutral][SHOWCHAR|Stacy|Neutral]Thought: [SFX|World|Applause] *Applause*")
	
	--Save the game.
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Starting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
	PairanormalLevel_FlagCheckpoint("Starting")
	fnDialogue("Mari:[E|Neutral][SFX|World|Crash]WHAT IS THE PURPOSE OF THIS--")
	fnDialogue("Stacy:[E|Shock]Mari! [P]Wait!")
	fnDialogue("Mari:[E|Neutral].[P].[P].[P][SFX|World|Blink] What?")
	fnDialogue("Stacy:[E|Happy]You forgot to introduce yourself.")
	fnDialogue("Mari:[E|Blush]Oh, right...[P] right, okay. *ahem*")
	fnDialogue("Mari:[E|Shock][SFX|World|Crash]MY NAME IS MARI![P] [E|Neutral]And I'm the club's co-president!")
	fnDialogue("Stacy:[E|Neutral]And [ITALICS]je suis[ENDITALICS] Stacy--")
	fnDialogue("Stacy:[E|Neutral]Bodybuilder,[P] dance troupe leader,[P][E|Happy] part time model--[P][E|Neutral] and the other club co-president. ")
	fnDialogue("Thought:[SFX|World|Applause] *Applause*")
	fnDialogue("Mari:[E|Shock][CRASH]WHAT IS THE PURPOSE OF THIS CLUB, YOU MAY ASK?[P] SIMPLE --")
	fnDialogue("Mari:[E|Blush]TO FIND DEMONS TO DATE VIA VARIOUS PARANORMAL INVESTIGATION TECHNIQUES!!!")
	fnDialogue("Thought:Wait a minute [P][P]-- what?[P] Is that what this club is about?")
	fnDialogue("Mari:[E|Shock][SFX|World|Crash]AND NOT -JUST- DEMONS!!![P] I AM ALSO OPEN TO DATING CREATURES OF THE UNDERWORLD;")
	fnDialogue("Mari:[E|Mad][SFX|World|Crash]WEREWOLVES, [P][SFX|World|Crash]VAMPIRES, [P][SFX|World|Crash]ZOMBIES[P][E|Blush] --as long as their brains haven't rotted fully and they can give me their consent-- ")
	fnDialogue("Thought:This is not what I signed up for![P] Which means I probably should have known what I was signing up for before, you know, doing it.")
	fnDialogue("Mari:[E|Mad][SFX|World|Crash]ALSO, MERMAIDS, [P][SFX|World|Crash]CENTAURS, [P][SFX|World|Crash]GHOSTS...[P][E|Neutral]definitely ghosts.")
	fnDialogue("Mari:[E|Mad]Um...[P][E|Mad][SFX|World|Crash] ALIENS...[P] BLUE-SKINNED PLANT PEOPLE...[P] [SFX|World|Crash]SLIMEGIRLS![P][E|Sad] What else is there?")
	fnDialogue("Stacy:[E|Shock]Okay,[P] that's enough Mari.[P] You're scaring our guest!")
	fnDialogue("Mari:[E|Blush]... Right.[P][E|Sad] Sorry.[P] I'm just... I'm just workshopping, you know.[P][E|Blush] No bad ideas here, people.")
	fnDialogue("Stacy:[E|Neutral]The [ITALICS]actual[ENDITALICS] mission of our club is to investigate the paranormal--[P] be it in the community,[P] in our history,[P] or on the world wide web.")
	fnDialogue("Bret:[E|Mad]To uncover the truth where others may prefer to turn a blind eye!")
	fnDialogue("Laura:[E|Blush]T-[P]to forge lifelong friendships!")
	fnDialogue("Michelle:[E|Neutral]To do something to pass the time in this hell we call highschool.")
	fnDialogue("Stacy:[E|Mad]So that when we are finally liberated from the chains of obligatory education...")
	fnDialogue("Mari:[E|Shock][SFX|World|Crash]WE'LL [P][SFX|World|Crash]BE [P][SFX|World|Crash]O[P][SFX|World|Crash]KAY!!!")
	fnDialogue("Thought:[SFX|World|Applause]*Applause*")
	fnDialogue("Thought:Aaahh, well. That doesn't seem too bad. [P]I'm back on board.")
	fnDialogue("[HIDECHAR|Laura][HIDECHAR|Bret][HIDECHAR|Michelle]Thought:Stacy turns to face me.")
	fnDialogue("Stacy:[E|Neutral][SFX|Blink]Hey, newcomer.[P] Why don't you introduce yourself to everyone?")
	fnDialogue("Thought:That's my cue...*gulp*[P] here we go.")
    Dia_SetProperty("Name Entry String", "My Name Is")
	fnDialogue("Thought:[ACTIVATENAMEENTRY]...")
	
	--Save the game.
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"NamedMyself\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
	PairanormalLevel_FlagCheckpoint("NamedMyself")
	
	--Dialogue.
	fnDialogue("Player:Uhhh, greetings, guys.[P] My name is [PLAYERNAME]. I'm a new student at Foster Falls.")
	fnDialogue("Mari:[E|Happy][PLAYERNAME], I'd like you to meet everyone!")
	fnDialogue("[SHOWCHAR|Mari|Neutral][SHOWCHAR|Stacy|Neutral]Thought:She gestures to the mostly empty classroom.[P] The only other three students wave nervously at me.")
	fnDialogue("Stacy: [E|Neutral]This is Laura.[FOCUS|Laura][SHOWCHAR|Laura|Neutral][P] She's our principal researcher and runs the club blog.")
	fnDialogue("Laura: [E|Neutral]H-hey.")
	fnDialogue("Mari: [E|Happy][HIDECHAR|Laura] THIS IS BRET! [FOCUS|Bret][SHOWCHAR|Bret][P][E|Neutral] He's like, our.[P].[P].[P] boots-on-the-ground thingimajig expert!")
	fnDialogue("Bret: [E|Shock][SFX|BLINK][P][P][P]I am?")
	fnDialogue("Mari: [E|Neutral]I made up the title just now.[P][E|Shock] BUT IT TOTALLY SUITS YOU,[P] [E|Neutral]don'cha think?")
	fnDialogue("Bret: [E|Blush]Well, it is true that, like, my memory and knowledge of obscure [E|Smile]paraphernalia has allowed me to...[P][E|Shock] y'know, [P][P][E|Neutral]to make meaningful contributions when we're on missions.")
	fnDialogue([[Bret: [E|Blush]But I dunno, [P][SPEEDTEXT]is an [E|Mad][P]\"expert\" actually a good title for someone like me? [E|Neutral][P]Like, for example, I remember just last week when I had to take a look at this --[ENDSPEEDTEXT] ]])
	fnDialogue("Stacy: [E|Happy][HIDECHAR|Bret][SHOWCHAR|Michelle|Neutral]-- [FOCUS|Michelle]And lastly, I present Michelle! ")
	fnDialogue("Michelle: [E|Happy]Hey.")
	fnDialogue("Stacy: [E|Neutral]Michelle is our...[P][E|Sad] um[P].[P].[P].")
	fnDialogue("Michelle: [E|Happy][P].[P].[P].")
	fnDialogue("Stacy: [E|Neutral]-- She's our friend![P][E|Happy] eh... hehe.")
	fnDialogue("Thought: Michelle looks as though she's about to say something,[P] but at the last minute decides she's satisfied with the description.")
	fnDialogue("Michelle: [E|Mad]...[P][E|Blush]Nice to meet you, [PLAYERNAME].")
	fnDialogue("Stacy: [E|Neutral]Right!")
	fnDialogue("Stacy: [E|Neutral][HIDECHAR|Michelle]Well now that we've gotten introductions out of the way...")
	fnDialogue("Mari: [E|Shock][SFX|World|Crash]LETS GET TO KNOW OUR NEWEST MEMBER BETTER WITH AN INTENSE, ONE-ON-ONE QUESTION AND ANSWER SEGMENT!")
	fnDialogue("Player: Wait, what?[P] H-hang on, I don't know if that's the best--")
	fnDialogue("Stacy: [E|Happy]Everyone gets to ask [PLAYERNAME] one question.")
	fnDialogue("Thought: She places a reassuring hand on my shoulder.")
	fnDialogue("Stacy: [E|Neutral]But don't ask anything [ITALICS]too[ENDITALICS] personal, okay?")
	fnDialogue("Thought: That does little to calm me.")
	fnDialogue("Thought: It might just be my imagination, but I see the club members exchange [GLITCH][GLITCHTEXT]LOOKS[ENDGLITCHTEXT]looks, something troubling I can't quite place just beyond their furrowed brows.")
	fnDialogue("Thought: I wonder if everyone can see me shaking.")
	fnDialogue("[SHOWCHAR|Bret|Neutral]Thought: [Focus|Bret]Bret stands, an eager smile on his face.")
	fnDialogue("Bret: [E|Happy]Me first! [E|Neutral]What's a better sandwich? PB&J or a BLT?")
	fnDialogue("Thought: An innocent enough query.[P].[P].[P] but something is off about it.[P] [GLITCH]Feels more like a test.") 
	fnDialogue("Thought: Again, I'm sure I'm imagining it,[P] but even the walls lean in, anticipating my answer.")

	--What to tell them you like eating:
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (What should I tell them?)") ]])

	local sDecisionScript = "\"" .. gsRoot .. "Gameplay_SceneA/1 - Bret Answer.lua\""
	fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"PB&J\", " .. sDecisionScript .. ", \"PB&J\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"BLT\",  " .. sDecisionScript .. ", \"BLT\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"What do those letters mean?\",  " .. sDecisionScript .. ", \"Letters\") ")
	fnCutsceneBlocker()

-- |[Level Skip]|
--Lets you jump ahead!
elseif(sTopicName == "LevelSkipBoot") then
	
	--Level Skip.
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "(Want to skip ahead?)") ]])

	local sDecisionScript = "\"" .. gsRoot .. "Gameplay_SceneA/0 - Meeting.lua\""
	fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Run First Scene\",      " .. sDecisionScript .. ", \"Start\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Character Showcase\",   " .. sDecisionScript .. ", \"Showcase\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"First Flashback\",      " .. sDecisionScript .. ", \"FirstFlashback\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"First Investigation\",  " .. sDecisionScript .. ", \"FirstInvestigation\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Second Investigation\", " .. sDecisionScript .. ", \"SecondInvestigation\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Credits\",              " .. sDecisionScript .. ", \"Credits\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Chapter Select\",       " .. sDecisionScript .. ", \"Chapter Select\") ")
	fnCutsceneBlocker()


--Debug.
elseif(sTopicName == "Showcase") then
	LM_ExecuteScript(gsRoot .. "Gameplay_SceneA/Z - Character Showcase.lua", "Start")

--Level skip options.
elseif(sTopicName == "FirstFlashback") then
	LM_ExecuteScript(gsRoot .. "Gameplay_SceneA/3 - Flashback.lua")
	
elseif(sTopicName == "FirstInvestigation") then
	LM_ExecuteScript(gsRoot .. "Gameplay_SceneA/5 - Investigation Prelude.lua", "SkipTut")
	
elseif(sTopicName == "SecondInvestigation") then
	LM_ExecuteScript(gsRoot .. "Gameplay_SceneC/8 - Arrive Home.lua")
	
elseif(sTopicName == "Credits") then
	LM_ExecuteScript(gsRoot .. "Gameplay_SceneE/5 - Flashback.lua")

-- |[Chapter Select]|
--Skips to a chapter.
elseif(sTopicName == "Chapter Select") then
	
	--Level Skip.
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "(Select Chapter)") ]])

	local sDecisionScript = "\"" .. gsRoot .. "Gameplay_SceneA/0 - Meeting.lua\""
	fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Chapter 1\", " .. sDecisionScript .. ", \"Start\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Chapter 2\", " .. sDecisionScript .. ", \"SkipToChapter2\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Chapter 3\", " .. sDecisionScript .. ", \"SkipToChapter3\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Chapter 4\", " .. sDecisionScript .. ", \"SkipToChapter4\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Chapter 5\", " .. sDecisionScript .. ", \"SkipToChapter5\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Walkhomes\", " .. sDecisionScript .. ", \"Walkhomes\") ")
	fnCutsceneBlocker()

--Chapter skip options.
elseif(sTopicName == "SkipToChapter2") then
    LM_ExecuteScript(gsRoot .. "../Chapter 2/Gameplay_SceneA/0 - Hallway Disaster.lua", "LevelSkipBoot")
elseif(sTopicName == "SkipToChapter3") then
    LM_ExecuteScript(gsRoot .. "../Chapter 3/Gameplay_SceneA/0 - Nerdseum.lua", "LevelSkipBoot")
elseif(sTopicName == "SkipToChapter4") then
    LM_ExecuteScript(gsRoot .. "../Chapter 4/Gameplay_SceneA/0 - Parks and Wreck.lua", "LevelSkipBoot")
elseif(sTopicName == "SkipToChapter5") then
    LM_ExecuteScript(gsRoot .. "../Chapter 5/Gameplay_SceneA/0 - Aliennapped.lua", "LevelSkipBoot")
elseif(sTopicName == "Walkhomes") then
    LM_ExecuteScript(gsRoot .. "../Chapter 4/Gameplay_SceneA/0 - Parks and Wreck.lua", "WalkhomeBoot")
end
