--[4 - Michelle's Answer]
--So I guess she did get to ask one question after all!

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)
	
--Save the game.
if(PairanormalLevel_IsLoading() == false) then
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Starting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

--Set data.
else
	PairanormalLevel_FlagCheckpoint("Starting")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Classroom")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Classroom")
	fnDialogue([[ [Music|Null][SHOWCHAR|Mari|Neutral][SHOWCHAR|Laura|Neutral][SHOWCHAR|Bret|Neutral][SHOWCHAR|Michelle|Neutral][SHOWCHAR|Stacy|Neutral]Thought: ... ]])
end

--[Execution]
--High School is like prison, except worse.
if(sTopicName == "WhatDo") then
	fnDialogue("Player: H-heh! I wish it was that easy... Oh man, What am I gonna do?")
	fnDialogue("Michelle:[E|Sad]...")
	fnDialogue("Thought: Michelle sighs. I'm not sure if she's exasperated, or doesn't really have an answer.")
	
--Gettin' kicked off the internet for this one!
elseif(sTopicName == "Urin8") then
	fnDialogue("Thought: Reflecting on my predicament, I'm surprised to find a sense of calm filling me. It's like a big weight that has been following me around has been lifted. ")
	fnDialogue("Player: I can't believe it but... this is kind of a relief, actually? I was so worried about everyone finding out, now that they do--I don't have to worry about it anymore.")
	fnDialogue("Michelle: [E|Happy] That's the spirit, babe.")
	fnDialogue("Thought: She nods approvingly.")

end

--Dialogue rejoins here.
fnDialogue("Thought: An uncomfortable silence fills the room. The club members look amongst each other, not really sure what to say next.")
fnDialogue("Bret: S-so...")
fnDialogue("Thought: Bret pushes his glasses up his nose nervously. He half chuckles to himself.")
fnDialogue("Bret: [E|Happy][Music|Daytime]Does this mean you've really, actually, never had a Peanut Butter and Jelly Sandwich, OR a Bacon Lettuce Tomato sandwich?")
fnDialogue("Player: ... No. Not that I know of.")
fnDialogue("Thought: A contagious smile spreads across his face. The other club members light up, too; some kind of realization hitting them all at the same time.")
fnDialogue("Bret: [E|Happy]Well I [ITALICS]have[ENDITALICS] to take you to Jonathan's Deli now.[P] That's where you find the best sandwiches in town!")
fnDialogue("Laura: And if you've never played a video game before... then that means! [P]*cough*[P] [E|Happy] That means!!! I-I have so many cool titles for you to try out!")
fnDialogue([[Laura: \"Mr. Needlemouse 3 (and Knuckles)\" is just so awesome!]])
fnDialogue("Mari: YOU HAVE TO READ TENTACLE FIASCO PARTS 1-5.[E|Happy] IT WILL CHANGE YOUR LIFE! ")
fnDialogue("Stacy: ...[E|Happy]And I will have the [ITALICS]distinguished[ENDITALICS] honor... of introducing you to the Backstreet Boy's uplifting library of songs.")
fnDialogue("Thought: All at once, the club members chatter excitedly. I can't help but get caught up in it. Suddenly, I feel Mari's hand on my arm.")
fnDialogue("Mari: ... Welcome to the club, [PLAYERNAME].")
fnDialogue("Thought: [Music|Daytime][HIDEALL][BG|Null]I always thought what had happened to me was something to be ashamed of, but put into this new perspective...")
fnDialogue("Thought: I see there are aspects that maybe... aren't so bad? Who knew?")

--Run the next script.
LM_ExecuteScript(gsRoot .. "Gameplay_SceneA/5 - Investigation Prelude.lua", "No Topic")