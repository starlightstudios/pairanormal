--[2 - Laura Answer]
--Player provides an answer to Laura's question.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)
	
--Save the game.
if(PairanormalLevel_IsLoading() == false) then
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Starting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

--Set data.
else
	PairanormalLevel_FlagCheckpoint("Starting")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Classroom")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Classroom")
	fnDialogue([[ [Music|Daytime][SHOWCHAR|Mari|Neutral][SHOWCHAR|Stacy|Neutral][SHOWCHAR|Laura|Neutral]Thought: [Focus|Laura]... ]])
end
PairanormalLevel_FlagCheckpoint("Starting")

--[Execution]
--HOW TO LIE 101: NOT LIKE THIS.
if(sTopicName == "Recommend") then
	fnDialogue("Thought: [Focus|Laura]I clasp my sweaty hands closed, circling my left and right thumbs around each other.")
	fnDialogue("Player: Oh! Ahah... let's see.[P] I don't know, really.[P] Do you have any recommendations?")
	fnDialogue("Thought: Laura looks surprised, but pleased to hear my answer.")
	fnDialogue("Laura: [E|Shock][SFX|World|Blink]W-who me? I... well... yes![P][E|Blush] *ahem* Yes I do! I mean.")
	fnDialogue("Thought: I can tell Laura isn't accustomed to speaking up.")
	fnDialogue("Thought: Although she's excited about the topic, she frequently looks around the room, almost expecting a club member to leap up and stop her.")
	fnDialogue("Laura:[E|Blush] It all depends on what sort of console you've got,[P] or[P] what genre interests you...[P] but if you want...[P] we c-[P]could maybe... talk about it sometime!!!")
	fnDialogue("Thought: I have nothing to do but watch her ears gradually redden as she talks, my own heart hammering furiously.")
	fnDialogue("Thought: It's as though we're in a contest to see who explodes from embarrassment first.")
	fnDialogue("Laura: [E|Blush]... In summary, [P]*cough*[P] *ahem*[P] I'd be happy to recommend a game...[P] I p-play a lot of them.")
	fnDialogue("Thought: She plops back down in her chair.[P] I exhale.")
	
--HOW TO LIE 101: DON'T ACTUALLY TELL THE TRUTH, IDIOT.
elseif(sTopicName == "NoPlay") then
	fnDialogue("Player: [Focus|Laura]Ah... I've never played any.")
	fnDialogue("Thought: Laura's face falls.[P] The clubmates murmur in surprise.")
	fnDialogue("Laura:[E|Sad] Not even a... [P]l-little browser game on Newsgrounds[P] or something?")
	fnDialogue("Thought: She shrinks down into her desk, not really paying attention as I shake my head.")
	fnDialogue("Player: I guess...[P] they're not really my thing?")
	fnDialogue("Laura: [E|Blush]R-[P]right.[P] They're n-not everyone's..[P] *ahem* cup of tea, so to speak.")
	

--HOW TO LIE 101: DO THIS.
elseif(sTopicName == "TooMany") then
	fnDialogue("Player: [Focus|Laura]Oh... gosh. [P]There's... so many games, you know.")
	fnDialogue("Thought: Laura smiles and nods, waiting to hear more.[P] So far so good.")
	fnDialogue("Player: I... couldn't possibly pick one?")
	fnDialogue("Laura: [E|Happy]Yes![P] I-I understand completely![P][E|Blush] I mean, from console[P] to console, genre to genre?[P] T-[P]there's just so many to choose from!")
	fnDialogue("Thought: I have nothing to do but watch her ears gradually redden as she talks, my heart hammering furiously.")
	fnDialogue("It's as though we're in a contest to see who explodes from embarrassment first.")
	fnDialogue("Laura: [E|Blush]... In summary,[P] *cough* [P]*ahem*[P] I'd be happy to...[P]talk about it...[P] more in depth with you![P] A-[P]at a later time!")
	fnDialogue("Player: Yeah, totally.")
	fnDialogue("Thought: She plops back down in her chair.[P] I exhale.")
	
end

--Dialogue rejoins here.
fnDialogue("Mari: [E|Neutral]Great question, Laura...[P][Hidechar|Laura] anyone else wanna ask stuff?")
fnDialogue("Thought: The club members shift uncomfortably. This time I'm sure I didn't imagine it -- Laura looks at Bret, an eyebrow raised.[P] He shrugs.")
fnDialogue("Thought: [SFX|World|Crash]I knew it! [GLITCH] [ITALICS]Something is very, very wrong[ENDITALICS]. Mari and Stacy exchange worried glances, tapping their hands, crossing and uncrossing arms.")
fnDialogue("Thought: They're waiting for something.[P] Something terrible.")
fnDialogue("Michelle: [Showchar|Michelle|Neutral][E|Happy][Music|Null]Yeah, I got a question.")
fnDialogue("Thought: [Music|Tension]Michelle's voice cuts through the tense room.")
fnDialogue("Thought: The confidence in her tone, the way she looks at me, sets alarm bells off in my head.")
fnDialogue("Mari: [E|Shock]MICHELLE! [GLITCH][P]R-REMEMBER WHAT WE TALKED ABOUT--")
fnDialogue("Thought: It finally dawns on me.[P] My legs are buzzing like they do when they're tucked away for too long.[P] My throat[GLITCH] is so tight I could barely breathe. [GLITCH] ")
fnDialogue("Stacy: [E|Shock]Don't ask anything too personal! We wouldn't want to-- ")
fnDialogue("Thought: [GLITCH][GLITCH]They know, don't they?[GLITCHTEXT]EVERYONEHERE[ENDGLITCHTEXT] ")
fnDialogue("Thought: Everyone here knows.")
fnDialogue("Michelle: [E|Mad]Is it true that you recently woke up in a hospital from a coma,[P] and nobody knows who you are or where you came from?[GLITCH] [GLITCH] [GLITCH] ")

--Execute the next script.
LM_ExecuteScript(gsRoot .. "Gameplay_SceneA/3 - Flashback.lua")