--[1 - Bret Answer]
--Player provides an answer to Bret's question.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)
	
--Save the game.
if(PairanormalLevel_IsLoading() == false) then
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Starting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

--Set data.
else
	PairanormalLevel_FlagCheckpoint("Starting")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Classroom")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Classroom")
	fnDialogue([[ [Music|Daytime][SHOWCHAR|Mari|Neutral][SHOWCHAR|Stacy|Neutral][SHOWCHAR|Bret|Neutral]Thought: [Focus|Bret]... ]])
end

--Flag.
PairanormalLevel_FlagCheckpoint("Starting")

--[Execution]
--Peanut Butter and Jelly San'ich
if(sTopicName == "PB&J") then
	fnDialogue("Thought: [Focus|Bret]I shrug, picking the one that sounds the most plausible.")
	fnDialogue("Player: Uhhh...[P] PB&J?")
	fnDialogue("Thought: Bret frowns, sinking back into his chair.[P] Laura giggles at him.")
	fnDialogue("Bret: [E|Sad][P][ITALICS]O [P]h [P] .[P][ENDITALICS] ")
	fnDialogue("Mari: [E|Neutral]Whatever you do, don't make out with Bret.[P] He hates peanut butter.")
	fnDialogue("Bret: [SFX|World|Crash][E|Shock]MARI--[P]WHAT!!! [E|Blush]I-I would never--[P] I mean, that is to say, [E|Mad]I hate peanut butter, like, TRUE, but not [E|Blush]just because you... ")
	fnDialogue("Bret: I mean... If [E|Mad]you and I [SPEEDTEXT]were to...[E|Shock]NOT THAT I WANT TO!!! [E|Blush]What I'm TRYING TO SAY IS--[ENDSPEEDTEXT] ")
	fnDialogue("Mari: [E|Neutral]Don't make out with Ronald from Advanced Calculus, either.[P] He's allergic to peanuts.")
	fnDialogue("Player: [P][P][P]Right.")
	fnDialogue("Thought: I take a moment to think.[P] Then, it hits me.")
	fnDialogue("Player: Ooooh, [ITALICS]Peanut Butter[ENDITALICS].[P] PB.[P] That's the PB in PB&J.")
	fnDialogue("Stacy: [E|Neutral]That's right! [P][E|Happy]Hehe, did you figure that out just now?")
	
--Bacon Lettuce and Tomato, sucka.
elseif(sTopicName == "BLT") then
	fnDialogue("Thought: [Focus|Bret]I shrug, picking the most plausible one.")
	fnDialogue("Player: Uhhh...[P] BLT?.")
	fnDialogue("Thought: Bret seems enormously pleased with this answer!")
	fnDialogue("Bret: [E|Happy]YES! [P][E|Mad]RIGHT ON![P][E|Happy] Oh man, if you like BLT's, I've got to take you to Jonathan's.")
	fnDialogue("Bret: [E|Shock] It's this deli down the street, and they grow their [E|Neutral]OWN tomatoes, and the bacon is maple bacon [SPEEDTEXT]imported from [E|Happy]Ca-na-daaaa. so the BLT is, like, so crunchy and sweet and--[ENDSPEEDTEXT] ")
	fnDialogue("[Showchar|Michelle|Happy]Michelle: [E|Happy]looks like Bret is asking you on a date, [PLAYERNAME].")
	fnDialogue("Thought: Laura giggles while Bret lurches at her, red as a beet.")
	fnDialogue("Bret: [E|Shock]MICHELLE![P][E|Neutral] WHAT!!!")
	fnDialogue("Bret: [E|Mad][P][E|Blush] I-I would never... [E|Mad]I mean, that is to say... [E|Sad]if they and I were... ")
	fnDialogue("Bret: [E|Shock]I mean, I only want to show them... [E|Blush]I'm just trying to be friendly![E|Mad][SPEEDTEXT] Not to say that I, like, don't think that they are--[ENDSPEEDTEXT] ")
	fnDialogue("[Hidechar|Michelle]Thought: I take a moment to think. Then, it hits me.")
	fnDialogue("Player: Ooooh, [ITALICS]Bacon[ENDITALICS] and [ITALICS]Tomato[ENDITALICS]. BT. That's the BT in BLT.")
	fnDialogue("Stacy: [E|Neutral]That's right! [P][E|Happy]Hehe, did you figure that out just now?")

--I forgot how to English.
elseif(sTopicName == "Letters") then
	fnDialogue("Thought: [Focus|Bret]I shrug.")
	fnDialogue([[Player: What does \"BLT\" and \"PB&J\" mean?]])
	fnDialogue([[Mari: [SFX|World|Blink][E|Shock]Oh! Right![P] Uhh...[E|Neutral]BLT stands for \"Bacon, Lettuce and Tomato\", and PB&J stands for \"Peanut Butter and Jelly\".]])
	fnDialogue("Stacy:[E|Neutral] Pretty Straightforward. [P] It's all right if you didn't get it at first, though.")

end

--Dialogue rejoins here.
fnDialogue("Player: What?[P] No![P] No, no, no... I mean, I knew that.[P] I totally knew that. [P][P][P][ITALICS]I'm normal[ENDITALICS].")
fnDialogue("Thought: The classroom is peppered with nervous laughter. My abdomen contracts. I'm not sure how or why, but instinctively I sense something is [ITALICS]wrong[ENDITALICS].")
fnDialogue("[GLITCHTEXT]WAS IT SOMETHING I SAI[ENDGLITCHTEXT] Was it something I said? I do my best [GLITCH]to pretend I don't notice.")
fnDialogue("Mari: [Hidechar|Bret][E|Neutral] Righty ho, [CRASH]WHO HAS THE NEXT QUESTION!?")
fnDialogue("Thought: [Showchar|Laura|Neutral][Focus|Laura]Laura raises her hand.")
fnDialogue("Mari: [E|Neutral]Geez, Laura, this isn't a classroom.")
fnDialogue("Thought: Playfully indignant, the hand remains raised.")
fnDialogue("Mari: [E|Sad]*sigh*...")
fnDialogue("Laura: [E|Happy]...")
fnDialogue("Mari: [E|Neutral]Yes, the young lady down in front?[P] [SFX|World|Crash][E|Mad]WITH THE CUTE BLUE HAIR!?")
fnDialogue("Laura: [E|Neutral]What's your favorite videogame, [PLAYERNAME]?")
	
--What to tell them you like eating:
fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (What's my favourite video game?)") ]])

local sDecisionScript = "\"" .. gsRoot .. "Gameplay_SceneA/2 - Laura Answer.lua" .. "\""
fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"What do you recommend?\", " .. sDecisionScript .. ", \"Recommend\") ")
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"I've never played any\",  " .. sDecisionScript .. ", \"NoPlay\") ")
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"There's too many to pick just one!\",  " .. sDecisionScript .. ", \"TooMany\") ")
fnCutsceneBlocker()

--Why isn't Pandemonium on this list, Chicmonster? You can't pretend like you haven't heard of it.
