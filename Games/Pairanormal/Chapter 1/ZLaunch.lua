-- |[Chapter 1 Launcher]|
--Launch point for the first chapter. Build subroutines and do loading here.

-- |[Paths]|
--Default paths.
gsRoot = fnResolvePath()
gsSubroutinePath = gsRoot .. "Subroutines/"

-- |[Primary Routines]|
--These routines do not rely on secondary loading or setup.
if(gbHasBuildRoutines == nil) then
    gbHasBuildRoutines = true
    LM_ExecuteScript(gsSubroutinePath .. "fnArgCheck.lua")
    LM_ExecuteScript(gsSubroutinePath .. "fnCutsceneBlocker.lua")
    LM_ExecuteScript(gsSubroutinePath .. "fnCutsceneInstruction.lua")
    LM_ExecuteScript(gsSubroutinePath .. "fnCutsceneWait.lua")
    LM_ExecuteScript(gsSubroutinePath .. "fnDecrementTime.lua")
    LM_ExecuteScript(gsSubroutinePath .. "fnDialogue.lua")
    LM_ExecuteScript(gsSubroutinePath .. "fnStdInvText.lua")
    LM_ExecuteScript(gsSubroutinePath .. "fnUnlockGallery.lua")
end

-- |[Remaps]|
--Remaps for CRASH to make it easier to read.
Dia_SetProperty("Add Tag Remap", "[CRASH]", "[SFX|World|Crash]")

--Gunshot.
Dia_SetProperty("Add Tag Remap", "[GUNSHOT]", "[SFX|World|Gunshot]")

--Remaps HIDEALL to CHAREXIT just to allow consistency.
Dia_SetProperty("Add Tag Remap", "[CHAREXIT|ALL]", "[HIDEALL]")

--Remaps all characters so they can quickly enter. This is always the neutral pose, if you want another pose use long-form.
local i = 1
local gsCharList = {"ADAM", "AJ", "AJBackpack", "Bret", "Candace", "Cellphone", "Cop", "Director", "Eilette", "EiletteO", "EiletteO2", "ElectronicDiary", "Elizabeth", "Harold", "Harper", "Kane", "Kid1", "Kid2", "Kid3", "Laura", "Mari", "MariHospital", "Michelle", "Mistress", "Nelson", "Nurse", "PlayerFlash", "Reich", "Leif", "Ross", "Ronnie", "Six", "Squirrel", "SquirrelBob", "SquirrelDoll", "SquirrelPuppet", "Stacy", "StacyHospital", "Student1", "Student2", "Student3", "Student4", "Student5", "Student6", "Teacher1", "Teacher2", "Teacher3", "Terry", "Three", "Thug1", "Thug2", "YStacy", "Todd", "YStacyHos", "YTodd", "BretAdult", "LauraAdult", "MariAdult", "MichelleAdult", "StacyAdult", "ToddAdult", "Walkie", "WaiterLeif", "CookRech"}
while(gsCharList[i] ~= nil) do
    Dia_SetProperty("Add Tag Remap", "[CHARENTER|" .. gsCharList[i] .. "]", "[SHOWCHAR|" .. gsCharList[i] .. "|Neutral]")
    Dia_SetProperty("Add Tag Remap", "[CHAREXIT|" .. gsCharList[i] .. "]", "[HIDECHAR|" .. gsCharList[i] .. "]")
    i = i + 1
end

-- |[Variable Building]|
--Variables and constants get declared.
LM_ExecuteScript(gsRoot .. "System/000 Global Variables.lua")
LM_ExecuteScript(gsRoot .. "System/100 Chapter Variables.lua")
LM_ExecuteScript(gsRoot .. "System/200 Dialogue Actors.lua")

--Set these load variables.
DL_AddPath("Root/Variables/System/Loading/")
VM_SetVar("Root/Variables/System/Loading/iLoadChapter2", "N", "0.0")
VM_SetVar("Root/Variables/System/Loading/iLoadChapter3", "N", "0.0")
VM_SetVar("Root/Variables/System/Loading/iLoadChapter4", "N", "0.0")
VM_SetVar("Root/Variables/System/Loading/iLoadChapter5", "N", "0.0")

--Drop events so the gameplay doesn't start before the loading catches up.
Debug_DropEvents()

-- |[Execution]|
--Demo! Leftover code from years ago, but it brings back memories.
if(false) then
    LM_ExecuteScript(gsRoot .. "Gameplay_SceneA/0 - Meeting.lua", "Demo")

--Level skip!
elseif(PairanormalMenu_SetProperty("Is Level Select Active") == true) then
    LM_ExecuteScript(gsRoot .. "Gameplay_SceneA/0 - Meeting.lua", "LevelSkipBoot")

--Pass to the opening, if not currently loading.
elseif(PairanormalLevel_IsLoading() == false) then
	
    --Start in Chapter 1.
    if(PairanormalMenu_SetProperty("Is Chapter 2 Skip Active") == false) then
            
        --No level skip, run the first scene.
        if(PairanormalMenu_SetProperty("Is Level Select Active") == false) then
            LM_ExecuteScript(gsRoot .. "Gameplay_SceneA/0 - Meeting.lua", "Start")
        
        --Level skip.
        else
            LM_ExecuteScript(gsRoot .. "Gameplay_SceneA/0 - Meeting.lua", "LevelSkipBoot")
        end
    
	--Start in Chapter 2.
	else
            
        --No level skip, run the first scene.
        if(PairanormalMenu_SetProperty("Is Level Select Active") == false) then
            LM_ExecuteScript(gsRoot .. "../Chapter 2/Gameplay_SceneA/0 - Hallway Disaster.lua", "Start")
        
        --Level skip.
        else
            LM_ExecuteScript(gsRoot .. "../Chapter 2/Gameplay_SceneA/0 - Hallway Disaster.lua", "LevelSkipBoot")
        end
	
	end

--If loading, we need to run to the checkpoint. This will be done by the C++ code.
else
	io.write("Running to checkpoint.\n")

end