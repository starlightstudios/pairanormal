-- |[ ============================ Walking Home With Laura, Scene 1 ============================ ]|
--Walking home with Laura for the second time.

-- |[Arg Resolve]|
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

-- |[ ======================================= Execution ======================================== ]|
--Begin.
if(sTopicName == "Start") then

    --Add 5 to friendship score.
    local iLauraFriend = VM_GetVar("Root/Variables/System/Romance/iLauraFriend", "N")
    VM_SetVar("Root/Variables/System/Romance/iLauraFriend", "N", iLauraFriend + 50.0)

    --Found the ADAM scene:
    local iMetAdam = VM_GetVar("Root/Variables/ChapterVars/Chapter4/iMetAdam", "N")
    if(iMetAdam == 1.0) then
        fnDialogue("[HIDEALL][MUSIC|Love]Thought: I didn't even have to ask Laura to leave with me.")
        fnDialogue("[CHARENTER|Laura]Thought: Once we had been ushered out of the storage room by Principal Arroyo, and Terry and Ross said their goodbyes, we simply fell in step leaving the school.")
        fnDialogue("Thought: It feels nice, but I'm careful not to smile too wide.[P] Laura doesn't seem very happy herself.")
        fnDialogue("Laura: [E|N][P]So.")
        fnDialogue("Player: .[P].[P]. ")
        fnDialogue("Laura: [E|Sad][SFX|World|Ugh]hm.")
        fnDialogue("Player: [SFX|World|Ugh]yeah.")
        fnDialogue("Laura: [E|Shock]Adam is...[P] an artificial intelligence.")
        fnDialogue("Player: Seems that way.")
        fnDialogue("Laura: [E|Shock]Built by... a creepy *ahem*[P] secret organization, and my high school principal.")
        fnDialogue("Player: Yep.")
        fnDialogue("Laura: ...")
        fnDialogue("Player: ...")
        fnDialogue("Laura: [E|Happy]H... heh.")
        fnDialogue("Player: ???[P] What's so funny?")
        fnDialogue("Laura: [E|Happy]I was just thinking, how funny it was... [P]I finally find someone who likes me for me, and who I like...[P] and he's a computer.[P] And I'm a hacker. ")
        fnDialogue("Player: Heh. Yeah, I guess that is kind of funny.")
        fnDialogue("Laura: [E|Happy]Haha... hahaha![P] It's almost too good to be true! Hahahaha!")
        fnDialogue("Player: What the heck just happened back there?")
        fnDialogue("Laura: [E|Happy]I don't know!!!")
        fnDialogue("Thought: Laura laughs louder and louder, until she's almost doubled over and wheezing.[P] I find myself joining in.")
        fnDialogue("Thought: We laugh and laugh, so hard we have to sink down into the sidewalk to keep from falling down.")
        fnDialogue("Thought: I gasp and try to catch my breath.[P] But my grin drops.")
        fnDialogue("Thought: Laura is crying.")
        fnDialogue("Player: Laura...")
        fnDialogue("Laura: [E|Sad]I-It's fine.[P][E|N] *sniff* I'm happy for him, really I am. And I'm glad I'll be able to see him whenever I want.")
        fnDialogue("Laura: Miss Arroyo said she'll... p-program him into a phone so we can text.[P][E|Sad] *sob*")
        
    --Didn't meet ADAM:
    else
        fnDialogue("[HIDEALL][MUSIC|Love]Thought: I didn't even have to ask Laura to leave with me.")
        fnDialogue("[CHARENTER|Laura]Thought: Once we had been ushered out of the storage room by Terry and Ross, we simply fell in step leaving the school.")
        fnDialogue("Thought: It feels nice, but I'm careful not to smile too wide.[P] Laura doesn't seem very happy herself.")
        fnDialogue("Laura: [E|N][P]So.")
        fnDialogue("Player: .[P].[P]. ")
        fnDialogue("Laura: [E|Sad][SFX|World|Ugh]hm.")
        fnDialogue("Player: [SFX|World|Ugh]yeah.")
        fnDialogue("Laura: [E|Sad]Well.[P] We gave it our best shot, I suppose.")
        fnDialogue("Laura: I really did do everything in my power... but it obviously wasn't meant to be. ")
        fnDialogue("Thought: I expected if we ran out of time this would end in tears, and so it has.[P] Laura stops in the middle of the street, head bowed.")
        fnDialogue("Thought: She suddenly stamps her feet like she's punishing the earth for putting her on it.")
        fnDialogue("Laura: [E|Mad]But... bahhhahha.[P][CRASH] *STOMP* I-it was nice wh-while [P]*sniff* it flipping lasted.[P] It's a n-n-nice [P][CRASH]*STOMP* memory [P][CRASH]*STOMP* at least![P][P][P][E|Sad] *sob* ww-wwaaaahhhh...")
    end
	
	--Dialogue.
    fnDialogue("Thought: I have nothing useful to say, as usual.")
    fnDialogue("Thought: But I understand that caring about someone who cares for you, just not in the same way, is it's own special kind of torture. ")
    fnDialogue("Laura: [E|Sad][PLAYERNAME]... *sniff*... Oh, [PLAYERNAME], it's not fair!!![P] I hate being alone, I just [ITALICS]hate[ENDITALICS] it. ")
    fnDialogue("Thought: I try to swallow the knot in my throat, and before I know it my arms are wrapped gently around Laura's shaking shoulders.")
    fnDialogue("Thought: A very special kind of torture, indeed.")
	
	--Save the game.
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Ethics\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
	PairanormalLevel_FlagCheckpoint("Ethics")
    
	--What to say to Bret:
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (What should I say?)") ]])

	local sDecisionScript = "\"" .. gsRoot .. "../Chapter Walk/Walkhome Laura 1.lua\""
	fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"There's someone else out there for you...\", " .. sDecisionScript .. ", \"SomeoneElse\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Adam doesn't know what he's missing\", " .. sDecisionScript .. ", \"Missing\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Just hold her\",  " .. sDecisionScript .. ", \"Hold\") ")
	fnCutsceneBlocker()

--Hopeful!
elseif(sTopicName == "SomeoneElse") then

	--Dialogue.
    fnDialogue("Player: Hey, hey. I used to be alone too, and it was terrifying to think that maybe things would never change.[P] But they did.")
    fnDialogue("Thought: I squeeze a little tighter, brushing the ends of her hair with the palms of my hand.")
    fnDialogue("Player: I've been so... surprised by the kindness of people around me--[P]people like you.")
    fnDialogue("Player: And if things worked out for a weirdo coma-baby, they'll work out for you, won't they?")
    fnDialogue("Laura: [E|Sad]*sniffle* ...")
    fnDialogue("Player: You never know when you'll meet someone who'll really like you.[P] They might not be here today, but m-maybe they are.")
    fnDialogue("Player: Maybe they're someone you already know![P] A friend who's been here the whole time, and who like h...")
    fnDialogue("Player: Harbors, uh, secret feelings for you, or something like that.")
    fnDialogue("Laura: [E|Happy]*sob* A-are you sure you haven't played any visual novels lately?[P] It sounds like you're *hrm* describing the plot of one to me.")
    --SHE'S ON TO US
    fnDialogue("Player: Haha, no way! I don't even know how to work a computer.")
    fnDialogue("Laura: [E|Happy]*giggle* Don't worry, someday I'm sure you will. They're great--*sigh*.[P][E|Sad] Unfortunately real life is rarely so exciting.")
    fnDialogue("Player:...")

    --Rejoin.
    LM_ExecuteScript(LM_GetCallStack(0), "Rejoin")

--Sassy!
elseif(sTopicName == "Missing") then

	--Dialogue.
    fnDialogue("Player: Well, I dunno about you, but I feel sorry for Adam.")
    fnDialogue("Laura: [E|Shock]You feel sorry f-for *sob* [ITALICS]him[ENDITALICS]?")
    fnDialogue("Player: He'll never know what it really feels like to date you. ")
    fnDialogue("Laura:[E|Shock]...")
    fnDialogue("Player: And he's missing out, I bet.")
    fnDialogue("Player: After all, you're smart and pretty, and you've got a cute nose, and you're funny, and you smell nice and your hair is soft--")
    fnDialogue("Laura: [E|Blush][SFX|World|Blink]H-huh?")
    fnDialogue("Player: ... Nothing! Nevermind.[P] I'm bad at this.")

    --Rejoin.
    LM_ExecuteScript(LM_GetCallStack(0), "Rejoin")

--Touching!
elseif(sTopicName == "Hold") then

	--Dialogue.
    fnDialogue("Thought: There aren't any words that I can offer.[P] But the one thing I'm happy to do is stay and be there for her.")
    fnDialogue("Thought: My embrace tightens, and so does Laura's.[P] It's not easy, but every painful moment we've shared has brought us closer together.[P] I wouldn't trade it for anything. ")
    fnDialogue("Info: [CRASH]*HONK*")
    fnDialogue("Laura: [E|Shock]Uh!!!")
    fnDialogue("Thought: A couple of passengers in the car in front of us wave impatiently.[P] Embarrassed, The two of us hop up onto the sidewalk where we probably belong.")
    fnDialogue("Laura: [E|Blush]*hrm* Oh dear... s-sorry.[P] I didn't really realize we were hugging for so long.")
    fnDialogue("Player: Yeah, me neither.")
    fnDialogue("Laura: [E|Happy]H-hehe.")
    fnDialogue("Player: ...")
    fnDialogue("Laura: [E|N]...")
    fnDialogue("Player: It felt nice, though.")
    fnDialogue("Laura: [E|Blush]!!! Er, y-y-yes. It did.")

    --Rejoin.
    LM_ExecuteScript(LM_GetCallStack(0), "Rejoin")

--Rejoin!
elseif(sTopicName == "Rejoin") then

	--Dialogue.
    fnDialogue("Thought: Laura timidly nibbles at her bracelet, and I rock onto my heels.[P] Another classic awkward silence.")
    fnDialogue("Laura: [E|N]I should thank you, [PLAYERNAME].")
    fnDialogue("Player: What?[P] Oh please, it was nothing--")
    fnDialogue("Laura: [E|N]I mean it.")
    fnDialogue("Thought: She pokes my shoulder playfully.")
    fnDialogue("Laura: [E|Mad]You've been s-supporting me throughout all of this.*sniff*.[P] And sure, things didn't work out the way I hoped, but...")
    fnDialogue("Laura: [E|Happy] Oh, goodness.[P] K-knowing me, I'd have just given up on Adam from the start! I'd be wondering my whole life about it.[P][E|Blush] So really, thank you.")
    fnDialogue("Player: [SFX|World|Ugh]Pffff, whaaat?[P] Dyahhh, weeeellllgyaaahhh, [P]I was just.[P] Nyaaaa.")
    fnDialogue("Player: *ahem*, I mean--[P]I'd better be getting home.")
    fnDialogue("Thought: Laura giggles, and we share a much-needed big breath of air.[P] It's time for us to go our seperate ways.")
    fnDialogue("[BG|Null]Thought: ...")
    
	--Save the game.
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"LimoScene\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
	PairanormalLevel_FlagCheckpoint("LimoScene")
    
    --Next script.
    LM_ExecuteScript(gsRoot .. "../Chapter 4/Gameplay_SceneD/0 - Walk Home.lua", "LimoScene")
    
end
