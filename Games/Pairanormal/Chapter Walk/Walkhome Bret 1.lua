-- |[ ============================ Walking Home With Bret, Scene 1 ============================= ]|
--Walking home with Bret for the first time. Always leads to chapter 4.

-- |[Arg Resolve]|
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

-- |[ ======================================= Execution ======================================== ]|
--Begin.
if(sTopicName == "Start") then

    --Add 5 to friendship score.
    local iBretFriend = VM_GetVar("Root/Variables/System/Romance/iBretFriend", "N")
    VM_SetVar("Root/Variables/System/Romance/iBretFriend", "N", iBretFriend + 50.0)
	
	--Dialogue.
	fnDialogue("[HIDEALL][MUSIC|Love]Thought: Bret catches my eye moments after his name comes to mind.[P] [ITALICS]Are we such good friends now that he thinks about me, too??[ENDITALICS]")
	fnDialogue("[CHARENTER|Bret]Bret: [E|N]Hey, [PLAYERNAME]![P] We walking home together or what?")
	fnDialogue("Player: What?")
	fnDialogue("Bret: [E|Shock]... I'll take that as a yes.[P][E|Happy] Come on!")
	fnDialogue("[HIDEALL][BG|Null]Thought: ...")
	fnDialogue("[BG|Root/Images/Backgrounds/All/Cafe][CHARENTER|Bret]Player: So, this is the infamous ''Jonathan's Deli''. What a nice surprise.")
	fnDialogue("Bret: [E|Happy]Oh, thanks goodness! I'm glad you think so, I mean, like, [E|Blush]I keep hoping to drag you down here.")
	fnDialogue("Bret: [E|Mad] This place.[P] Has the BEST.[P] Sandwiches.")
	fnDialogue("Thought: I glance out the booth window while Bret reads the menu to me.")
	fnDialogue("Thought: I was a little suspicious when we didn't take the usual route home--but then Bret pointed out the little deli's sign, nestled between two townhouses.")
	fnDialogue("Thought: As soon as we stepped inside, the warm savory air sank into every pore, soothing me like a hot buttery drink.[P] I can't wait to try the food.")
	fnDialogue("[CHARENTER|WaiterLeif]Waiter: [FOCUS|WaiterLeif][VOICE|Voice|Leif]Okay kids, what'll it be?")
	fnDialogue("Player: [SFX|World|Blink]S-seven![P] Or..is it Eight?")
	fnDialogue("Waiter: [FOCUS|WaiterLeif][VOICE|Voice|Leif]You want the number seven or the number eight special?")
	fnDialogue("Bret: [E|N]No, that's okay, we're here to order the sandwiches.[P] Can we get two BLTs?")
	fnDialogue("Waiter: [FOCUS|WaiterLeif][VOICE|Voice|Leif]We've only got enough for one, sorry.[P] Sandwich of the day is PB&J if you want it though.")
	fnDialogue("Bret: [E|Mad]Yuck! No way.")
	fnDialogue("Waiter: [FOCUS|WaiterLeif][VOICE|Voice|Leif][P][P][P]... you saying our sandwiches are bad?")
	fnDialogue("Bret: [E|Shock]N-no.[P] Oh, I wasn't saying that, I just hate peanut b--")
	fnDialogue("Waiter: [FOCUS|WaiterLeif][VOICE|Voice|Leif]I'll have you know my other--[P]I mean, my [ITALICS]brother[ENDITALICS], the chef, makes an EXCELLENT PBJ.")
	fnDialogue("Waiter: [FOCUS|WaiterLeif][VOICE|Voice|Leif]Best damn PBJ in the world![P] And you come in here, into [ITALICS]my[ENDITALICS] restaurant, and insult his crafstmanship?")
	fnDialogue("Player: Wait a minute, Seven...[P]Eight...[P]uh, sir.[P] You [ITALICS]own[ENDITALICS] this restaurant?")
	fnDialogue("Waiter: [FOCUS|WaiterLeif][VOICE|Voice|Leif]No, I'm just a waiter.[P][CRASH] BUT I TAKE A LOT OF PRIDE IN MY WORK!")
	fnDialogue("Thought: Great.[P] I should order something quickly--who knows what they did to the last poor food critic who came here.")
    
	--Save the game.
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"StupidSandwiches\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
	PairanormalLevel_FlagCheckpoint("StupidSandwiches")
    
	--Decision:
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (What do I order?)") ]])

	local sDecisionScript = "\"" .. gsRoot .. "../Chapter Walk/Walkhome Bret 1.lua\""
	fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"PBJ\", " .. sDecisionScript .. ", \"PBJ\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"BLT\", " .. sDecisionScript .. ", \"BLT\") ")
	fnCutsceneBlocker()
    
--PBJ:
elseif(sTopicName == "PBJ") then
	
	--Dialogue.
	fnDialogue("Player: I'll have the PB&J.[P] Bret, you take the BLT.[P] Are we all done here, gentlemen?")
	fnDialogue("Bret: [E|Sad]...")
	fnDialogue("Waiter: [FOCUS|WaiterLeif][VOICE|Voice|Leif]...")
	fnDialogue("Bret: [E|Sad]Y-yeah, I guess so...")
	fnDialogue("Waiter: [FOCUS|WaiterLeif][VOICE|Voice|Leif]--wasn't even going to say anything, really...")
	fnDialogue("Player: Great. ")
	fnDialogue("Thought: I take a sip of my complementary water decisively, and Reich or Leif sulks away.")
	fnDialogue("[CHAREXIT|WaiterLeif]Bret: [E|Mad] Uh, I'm not really one to ever badmouth a service professional,[E|Shock] but did that guy seem a little off to you?")
	fnDialogue("Player: Heh heh... [ITALICS]you have no idea[ENDITALICS].")
	fnDialogue("[HIDEALL][BG|Null]Thought: ...")
	fnDialogue("[BG|Root/Images/Backgrounds/All/Cafe][CHARENTER|Bret]Bret: [E|Shock]Wow.[P] I... have never watched someone experience food like that before.")
	fnDialogue("Player: Muhh?")
	fnDialogue("Thought: Did I finish already?")
	fnDialogue("Thought: One moment, a beautifully-constructed PB&J sandwich slid onto the table before me.[P] Even Bret had to admit it looked good.")
	fnDialogue("Thought: The first bite was divine;[P] crunchy bread carefully enveloping the sweet jam and smooth butter.")
	fnDialogue("Thought: I was also surprised by crisp plantain chips, which added so much texture and enhanced the saltiness of the peanut butter.")
	fnDialogue("Thought: And suddenly, the yolk of an egg, buried deep at the sandwiche's core, burst in my mouth, flooding my tastebuds with a creamy, savory element.")
	fnDialogue("Thought: Before I knew it, the whole thing was gone.")

    --Rejoin.
    LM_ExecuteScript(LM_GetCallStack(0), "Rejoin")

--BLT:
elseif(sTopicName == "BLT") then

    --Dialogue.
	fnDialogue("Player: Okay, enough of this.[P] We're ordering a single BLT, and Bret and I are going to share it. ")
	fnDialogue("Waiter: [FOCUS|WaiterLeif][VOICE|Voice|Leif]Ugh fine, I'll be right back.")
	fnDialogue("Bret: [E|Mad]Wow.[P] Uh, I'm not really one to ever badmouth a service professional, [E|Shock]but did that guy seem a little... off to you?")
	fnDialogue("Player: Heh heh...[P] [ITALICS]you have no idea[ENDITALICS].")
	fnDialogue("[HIDEALL][BG|Null]Thought: ...")
	fnDialogue("[BG|Root/Images/Backgrounds/All/Cafe][CHARENTER|Bret]Bret: [E|Shock]Wow.[P] I... have never watched someone experience food like that before.")
	fnDialogue("Player: Muhh?")
	fnDialogue("Thought: Did I finish already?")
	fnDialogue("Thought: The only thing left on my plate is hot globs of food residue dripping from my chin and fingers.[P] I hastily wipe them off with my napkin.")
	fnDialogue("Thought: One moment, a beautifully-constructed BLT sandwich slid onto the table before me.[P] The first bite was exquisite;")
	fnDialogue("Thought: A warm earthy dark bread enveloping layers of thinly sliced tomatoes, crispy but sizeable strips of bacon, and crisp fresh lettuce.")
	fnDialogue("Thought: And there was some kind of sauce, slathered on the whole thing--like a mayonnaisey-mustard-cheesy... I didn't even have the vocabulary to describe it...")
	fnDialogue("Thought: ... but it salted the tomatoes, sweetened the bacon, and gave the lettuce a beautiful briny kick.")
	fnDialogue("Thought: Before I knew it, my half of the sandwich was gone,[P] and I tried not to eye Bret's portion so hungrily. ")

    --Rejoin.
    LM_ExecuteScript(LM_GetCallStack(0), "Rejoin")

--Rejoin.
elseif(sTopicName == "Rejoin") then

	--Dialogue.
	fnDialogue("Player: I don't know what just happened.[P] I... am [SFX|World|Ugh]ashamed of what I became.")
	fnDialogue("Bret: [E|Happy]Hahaha! You kidding?[P][E|Blush] Honestly, I'm jealous of you, kind of.")
	fnDialogue("Bret: [E|N]I guess when you haven't eaten a whole lot of food, everything you try tastes new and awesome.")
	fnDialogue("Bret: [E|Mad]Makes you kind of wonder, doesn't it?[SPEEDTEXT] Everybody has to [ITALICS]try[ENDITALICS] food for the first time.")
	fnDialogue("Bret: For example, the first sandwich was invented by the Earl of Sandwich, well he instructed his chef, so it was probably actually invented--[ENDSPEEDTEXT] ")
	fnDialogue("Bret: [E|Sad]Ah, sorry.[P] Rambling again.")
	fnDialogue("Player: You don't have to stop.")
	fnDialogue("Bret: [E|N]Oh, I wasn't saying anything important, just some facts about the Earl of Sandwich.")
	fnDialogue("Bret: [E|Happy] I have no idea how I even got there--Earl of Sandwich![P] Ridiculous.")
	fnDialogue("Thought: The joy of the moment evaporates in the uncomfortable silence.[P] I fiddle with some flatware on my plate.")
	fnDialogue("Player: *hrm*.[P][P] Bret. You uh...[P]you [ITALICS]like[ENDITALICS] me, right?")
	fnDialogue("Thought: Bret's eyebrows arch.")
	fnDialogue("Bret: [E|Shock]Y[P]eah.[P] I mean, in what way?[P][E|Blush] N-nevermind, forget that first part,[E|Mad] obviously you meant as a friend.")
	fnDialogue("Bret: [E|N] Of course. Yes.[P][E|Blush] I like you a lot.[P][E|Sad] As a friend.")
	fnDialogue("Player: Be honest.[P] I've never made it seem like--I thought you were [ITALICS]annoying[ENDITALICS], right?[P] If so, I'm sorry. I-I don't feel that way at all. ")
	fnDialogue("Bret: [E|Shock]No... Oh god, no![P][E|Mad] I never thought that YOU...[E|Blush]thought that about me.[E|Mad] I was just being dumb![E|Sad] I-- mm.")
	fnDialogue("Thought: Bret sinks into the booth seat, his back making a faint 'squeeeek' as he slides down.")
	fnDialogue("Bret: [E|Sad][PLAYERNAME], um.[P] Before you showed up.[P] I've always sort of known people don't...[ITALICS]enjoy[ENDITALICS] having me around.")
	fnDialogue("Bret: I get nervous, and I'm not very charismatic and I ramble and stuff.")
	fnDialogue("Bret: Even the club--[P]I have lots of fun and all, you know but,[P] sometimes I feel like if I wasn't some kind of walking encyclopedia...[P][P]well, you get it.")
	fnDialogue("Player: Oh.")
	fnDialogue("Bret: [E|Shock]Hey[P]... You're smiling!?")
	fnDialogue("Player: [CRASH] What?[P] I'm not smiling! ")
	fnDialogue("Bret: [E|Sad]There's a huge grin on your face.")
	fnDialogue("Player: Nooo--oooh wait, yes I am.[P] I'm so sorry, it's definitely not appropriate for the situation but[P] I'm honestly really happy that you opened up to me.")
	fnDialogue("Player: And maybe there's a part of me that's glad to know I'm not the only one who feels out of place sometimes?")
	fnDialogue("Player: It's kind of messed up to say but there it is.")
	fnDialogue("Bret: [E|N]Psssht.")
	fnDialogue("Player: You know what?[P] This might be my naive inner coma baby who doesn't know about the real world talking...")
	fnDialogue("Player: ... But I think you should just ask the club how they feel. [P]And how you feel. It worked for me.")
	fnDialogue("Player: ... Although I'm not going to lie, the sandwiches probably had something to do with it.[P] Definitely ask them over these DELICIOUS sandwiches.")
	fnDialogue("Player: You're smiling too, now, by the way.")
	fnDialogue("Bret: [E|Happy]Pretty sure I'm not.[P] Impossible.")
	fnDialogue("Thought: Our eyes meet, and Bret's nose flushes in a very nice shade of pink, probably similar to the tone my own face is turning.")
	fnDialogue("Thought: How did I become this person?[P] Someone who can barely walk now giving wholesome advice about friendship.")
	fnDialogue("Info: [CRASH]*crash*")
	fnDialogue("Player: Wha--")
	fnDialogue("[CHARENTER|CookRech][CHARENTER|WaiterLeif]Waiter: [FOCUS|WaiterLeif][VOICE|Voice|Leif]Good evening, it's me, the waiter again.")
	fnDialogue("Cook: [FOCUS|CookRech][VOICE|Voice|Reich]And I'm the chef! Glad you liked the sandwiches, [PLAYERNAME].[P][E|Shock] I mean, RANDOM CUSTOMER.")
	fnDialogue("Thought: [ITALICS]Graaagh, I forgot about them.[ENDITALICS]")
	fnDialogue("Waiter: [FOCUS|WaiterLeif][VOICE|Voice|Leif]Here's your check.")
	fnDialogue("Bret: [E|N]But I didn't ask--")
	fnDialogue("Waiter: [FOCUS|WaiterLeif][VOICE|Voice|Leif][SPEEDTEXT]IT'S NEARLY EIGHT AND ONE OF YOU HAS A CURFEW[ENDSPEEDTEXT][P][SFX|World|Ugh]--I mean, probably.")
	fnDialogue("Bret: [E|Shock]Right.[P][E|Sad] Uh, I've got my wallet here in my backpack, hang on.")
	fnDialogue("[CHAREXIT|Bret]Thought: ...")
	fnDialogue("Cook: [FOCUS|CookRech][VOICE|Voice|Reich]Your friend is super cute, by the way.[P] Did you have a nice date?")
	fnDialogue("Player: It's NOT a date! And WHAT are you guys doing here??")
	fnDialogue("Cook: [FOCUS|CookRech][VOICE|Voice|Reich]We're spying on you obviously.[P] But you have nothing to worry about. ")
	fnDialogue("Waiter: [FOCUS|WaiterLeif][VOICE|Voice|Leif]He hasn't even tried to slip a narcotic into your drink even [ITALICS]once[ENDITALICS]. ")
	fnDialogue("Twins: [FOCUS|WaiterLeif][FOCUSMOD|CookRech|true]That's a keeper!")
	fnDialogue("Player: [FOCUSMOD|CookRech|False][SFX|Ugh]I... speechless.[P] There are no words. Look, I'm [ITALICS]trying[ENDITALICS] to do this whole friendship connecting thing, which is VERY hard for me normally.")
	fnDialogue("Player: If you guys get all in here with your weirdness, you might RUIN it!!!")
	fnDialogue("Waiter: [FOCUS|WaiterLeif][VOICE|Voice|Leif]Excuse me, what do you think we are, amateurs?")
	fnDialogue("Cook: [FOCUS|CookRech][VOICE|Voice|Reich]We're only here to ensure your safety.")
	fnDialogue([[Cook: [FOCUS|CookRech][VOICE|Voice|Reich]If you feel threatened for any reason, just give us \"the signal\", and we will... take care of him, if you know what I mean.]])
	fnDialogue("Player: This is [ITALICS]not[ENDITALICS] a date, and you have made my life unimaginably more horrifying with that information--")
	fnDialogue("[CHARENTER|Bret]Bret: [E|Happy]Heeere it is![P][E|N] Okay, all paid up. Ready to head home, [PLAYERNAME]?")
	fnDialogue([[Thought: I nod my head as slowly as possible, trying not to make any sort of movement that would consitute as a \"signal\".]])
	fnDialogue("Player: Bye, uh... you two.")
	fnDialogue("Twins: [FOCUS|WaiterLeif][FOCUSMOD|CookRech|true]Come back soon!")
	fnDialogue("[HIDEALL][BG|Null]Thought: ...")
    
	--Save the game.
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"LimoScene\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
	PairanormalLevel_FlagCheckpoint("LimoScene")
    
    --Next script.
    LM_ExecuteScript(gsRoot .. "../Chapter 4/Gameplay_SceneD/0 - Walk Home.lua", "LimoScene")
end
