-- |[ ============================ Walking Home With Stacy, Scene 0 ============================ ]|
--Walking home with Stacy for the first time. This can be called from one of two different places.
-- The chapter variable marks which location to move the script back to when this ends.

-- |[Arg Resolve]|
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

-- |[ ======================================= Execution ======================================== ]|
--Begin.
if(sTopicName == "Start") then

    --Add 5 to friendship score.
    local iStacyFriend = VM_GetVar("Root/Variables/System/Romance/iStacyFriend", "N")
    VM_SetVar("Root/Variables/System/Romance/iStacyFriend", "N", iStacyFriend + 50.0)

    --Flag.
    VM_SetVar("Root/Variables/System/Player/iWalkedWithStacyA", "N", 1.0)
	
	--Dialogue.
	fnDialogue("[HIDEALL][MUSIC|Love][CHARENTER|Mari]Mari: [E|N]Thanks for coming to hang out with us again, [PLAYERNAME]!")
	fnDialogue("Player: I hang out with you guys all the time. [P]I'm a member of this club.")
	fnDialogue("Mari: [E|Happy]Yeah, but that doesn't mean you don't deserve to be thanked!")
	fnDialogue("Player: Heh. Um... where's Stacy?")
	fnDialogue("[CHARENTER|Stacy]Stacy: [E|Blush]Hey, [PLAYERNAME]![E|N] Wanna walk home together?")
	fnDialogue("Player: Again? Sure, why not?")
	fnDialogue("Stacy: [E|Blush]Excellent.")
	fnDialogue("Thought: Mari looks from Stacy, to me, and back to Stacy.[P] A creepy smile slowly grows on her face.")
	fnDialogue("Player: Uh, Mari, do you maybe want to join--")
	fnDialogue("Mari: [E|Shock][CRASH]OH MY GOSH!! I TOTALLY FORGOT I HAVE TO... uhh...[P][P][CRASH] DO SOME WITCH STUFF BACK HOME!")
	fnDialogue("Player: Okay.")
	fnDialogue("Stacy: [E|Shock]Mari.")
	fnDialogue("Mari: [E|Shock]AND I REALLY HAVE TO HURRY, SO, I'M REALLY SORRY.[P][E|Blush][CRASH] I CAN'T JOIN YOU GUYS BUT HAVE A GREAT TIME!!![P][CRASH] ALONE!!![P][CRASH] TOGETHER!!!")
	fnDialogue("Player: Okaaaaayyyy...")
	fnDialogue("Stacy: [E|Mad][ITALICS]Mari.[ENDITALICS]")
	fnDialogue("Mari: [E|Happy][ITALICS]Now's your chance, Stacy.[ENDITALICS]")
	fnDialogue("Stacy: [E|Mad]MARI OH MY GOD.[P] WE'RE LEAVING.")
	fnDialogue("Mari: [E|Happy]Bye guuuuys! Have fuuuuuunnnn!!!")
	fnDialogue("[HIDEALL][BG|Null]Thought: ...")
	fnDialogue("[BG|Root/Images/Backgrounds/All/WalkHome][CHARENTER|Stacy]Player: So... how was class today?")
	fnDialogue("Stacy: [E|Happy]Hah! ''How was class''? You sound like my dad.")
	fnDialogue("Stacy: [E|N]Let's seee... I snuck in a nap between PE and French... Finally cleaned that piece of bubblegum on my locker.[P] Soooo, actually pretty productive. How about you?")
	fnDialogue("Player: I dunno. I think maybe I'm starting to finally enjoy class?")
	fnDialogue("Player: It's still really hard to understand all the lectures, but sometimes the materials are kind of interesting.")
	fnDialogue("Stacy: [E|Shock]Oh yeah? For example?")
	fnDialogue("Player: Well, in Biology I learned about genes,[P] and how you can inherit stuff from your parents like eye colors and things.")
	fnDialogue("Player: We're supposed to make squares based on people in our family, to chart similar traits, but--")
	fnDialogue("Stacy: [E|N]Hey, [PLAYERNAME]! What color are your eyes? ")
	fnDialogue("Player: ?")
	fnDialogue("Thought: Stacy leans in to inspect the pigmentation of my pupils.[P] As she gets closer however, I get more concerned about the pigmentation of my cheeks.")
	fnDialogue("Stacy: [E|Shock]Incredible.")
	fnDialogue("Player: what?")
	fnDialogue("Stacy: [E|Blush]...[P][P][ITALICS]They're brown[ENDITALICS].")
	fnDialogue("Player: [SFX|World|Blink]Is that... unusual?")
	fnDialogue("Stacy: [E|Blush]Er... [P][SFX|World|Ugh]No.[P][E|Sad] It's actually the most common eye color.")
	fnDialogue("Player: Oh.")
	fnDialogue("Thought: That playful smile disappears from Stacy's dimpled cheeks once more, and my stomach sinks. [ITALICS]Dangit![ENDITALICS]")
	fnDialogue("Thought: I never know what to say.[P] I could be the world's foremost scientist on eye pigmentation and probably still manage to screw it up.")
	fnDialogue("Thought: If I just had a clue what Stacy was thinking...[GLITCH]")
	fnDialogue("Stacy: [E|N]Hm? [P]Hey, [PLAYERNAME], why'd you stop?")
	fnDialogue("Player: Oh, sorry. ")
	fnDialogue("Thought: Of course.[P] [ITALICS]Of course[ENDITALICS].[P] I CAN find out what Stacy's thinking about--I'm a freakin' mind reader, aren't I?")
	fnDialogue("Player: Uhh...[P]Stacy, can you tell me more about eyes?")
	fnDialogue("Stacy: [E|Shock]Hahaha, sure![P][E|Blush] Let's see... they say that the eyes are the window to the soul. ")
	fnDialogue("Thought: On the other hand...[GLITCH]Stacy is my friend.")
	fnDialogue("Thought: And there seems to be something about herself she's trying to keep private--what if I a[GLITCH]ccidentally stumble into it?")
	fnDialogue("Stacy: [E|Happy]There isn't any science to really back that up,[P] but I find when I look straight into someone's eyes, I can really make 'em squirm.[P][E|N] It's kind of fun!")
	fnDialogue("Thought: Stacy's eyes meet mine once more.[P] The intensity and intimacy of her [GLITCH]knowing gaze certainly makes me want to squirm.")
	fnDialogue("Thought: [GLITCH] I'll also never have a more clear shot at her memories--it's now or never.")
    
	--What to say to Stacy:
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (What should I do?)") ]])

	local sDecisionScript = "\"" .. gsRoot .. "../Chapter Walk/Walkhome Stacy 0.lua\""
	fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Jump into Stacy's Memories\", " .. sDecisionScript .. ", \"MindJack\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Keep talking\", " .. sDecisionScript .. ", \"BatterChox\") ")
	fnCutsceneBlocker()

--Mind Jacking!
elseif(sTopicName == "MindJack") then
	
	--Dialogue.
	fnDialogue("Thought: [GLITCH][GLITCH]Here we[GLITCHTEXT]JUSTGOTOSLEEP JUST GOOOO TO SLEEEEEP JUST[ENDGLITCHTEXT] go.")
	fnDialogue("[HIDEALL][BG|Null][MUSIC|Null]Thought: .[P][GLITCH].[GLITCH][P].[P]")
	fnDialogue("[BG|Root/Images/Backgrounds/All/StacyRoom][MUSIC|Somber]Thought: I open my bleary eyes, and find myself lying on the bed of...[GLITCH]I guess, Stacy's room.[P] It looks pretty normal.")
	fnDialogue("Thought: Daylight filters through a window.[P] Backstreet Boys posters line the walls, and pink string lights are taped delicately around my bed frame.")
	fnDialogue("Thought: My french homework is on the desk.")
	fnDialogue("Thought: Monsieur Thierry said if I could finish the backlog before Friday,[P] he wouldn't tell my dad I'd been slacking off in class.")
	fnDialogue("Thought: I should get up out of bed, and finish it.[P] But I can't.[P] The blanket on me feels like it weighs a ton.")
	fnDialogue("Thought: I can barely move, let alone get up.")
	fnDialogue("Info: [SFX|World|Cellphone]Riiiiiing!")
	fnDialogue("Thought: I should pick up the phone. But it's probably not an emergency.[P] I try to close my eyes again.[P] Eventually, it stops ringing.")
	fnDialogue("Thought: [P][P][P]My dad paid good money for this Summer intensive French class.[P] I'm wasting his money--if I could just apply myself more.")
	fnDialogue("Thought: Monsieur Terry believed in me, I had to beg him to let me do catchup homework. I lied to his face, didn't I?")
	fnDialogue("Thought: I knew I couldn't catch up, because I can't even get out of bed.[P] I'm so worthless I can't even get out of bed and do a single stupid French excercise.")
	fnDialogue("Thought: And I'll be kicked out of the class and everyone will be furious at me.")
	fnDialogue("Thought: [P][P][P].[P].[P].[P]If I didn't exist, dad wouldn't have to worry so much.[P] And he wouldn't have to pay for expensive classes.")
	fnDialogue("Thought: If I just--[P]no, stop. Stop thinking like this, Stacy.[P] Take a deep breath.")
	fnDialogue("Thought: Maybe if I just sleep a little more, I'll feel better.[P] That's it.[P] When I wake up, I'll feel refreshed and renewed and not so worthless...")
	fnDialogue("Info: [SFX|World|Cellphone]Riiiiiing!")
	fnDialogue("Thought: This might be serious, after all.")
	fnDialogue("Thought: I pick up the phone, trying not to sound like I just woke up. It's --[P][P] holy shit, nearly two in the afternoon?[P] I slept that long??")
	fnDialogue("Phone: [CHARENTER|Cellphone][FOCUS|Cellphone][VOICE|Voice|Mari][CRASH]STACYYYYY!!!")
	fnDialogue("Player: [VOICE|Voice|Stacy]*ahem*[P] [ITALICS]Bonjour,[ENDITALICS] Mari! What's up?")
	fnDialogue("Mari: [FOCUS|Cellphone][VOICE|Voice|Mari][CRASH]FRIENDSHIP ADVENTURES, that's what's up! Come on!![P] I'm heading down to the old clubhouse to get her ready for the new school year.")
	fnDialogue("Mari: [FOCUS|Cellphone][VOICE|Voice|Mari]Then maybe we can swing down to Charlie's Pizza Parlour for a follow up dinner?")
	fnDialogue("Mari: [FOCUS|Cellphone][VOICE|Voice|Mari][CRASH] OOH,[P] or we could go to 7-11 for a SNACK RUN! What do you think?[P] You pick!")
	fnDialogue("Player: [VOICE|Voice|Stacy]Oh...[P] uh, yeah, I can't go.")
	fnDialogue("Mari: [FOCUS|Cellphone][VOICE|Voice|Mari][CRASH]WHAT?!?! Why not?[P] I need my co-club captain to watch my back in case any ghosts try to attack me!")
	fnDialogue("Thought: I smile, but can't quite force out a chuckle.")
	fnDialogue("Player: [VOICE|Voice|Stacy]If a ghost attacks you, you can probably just seduce it or something.")
	fnDialogue("Mari: [FOCUS|Cellphone][VOICE|Voice|Mari]VERY TRUE, hehehe![P][P] But, are you sure you can't come?")
	fnDialogue("Player: [VOICE|Voice|Stacy]Yeah, I...[P]I have French homework.[P] Can we reschedule?")
	fnDialogue("Mari: [FOCUS|Cellphone][VOICE|Voice|Mari]Yeah, yeah totally! No worries.[P] Okay.")
	fnDialogue("Mari: [FOCUS|Cellphone][VOICE|Voice|Mari]I'm gonna scope out the site today on my own, and I'll tell you if we need to bring anything next time.")
	fnDialogue("Player: [VOICE|Voice|Stacy]Cool.[P] See you.")
	fnDialogue("Mari: [FOCUS|Cellphone][VOICE|Voice|Mari][CRASH]LATER NERDDD!!!")
	fnDialogue("Thought: [CHAREXIT|Cellphone]I hang up and chuck the phone back on the floor. Now I can finally get back to sleep --[P] but the bed doesn't feel too comfortable, it's all hot and muggy.")
	fnDialogue("Thought: Maybe I should've gone with Mari after all.[P] I bet hanging out would make me feel better.[P] But she was also counting on me to be...[P][P]strong.")
	fnDialogue("Thought: And I'm not really strong. I'm just a liar.")
	fnDialogue("Thought: If she knew what I was really like, she'd never want to be my friend.")
	fnDialogue("Thought: She probably doesn't even like me now, especially since I'm always cancelling my plans on her.")
	fnDialogue("Thought: That's why all my other friends stopped hanging out with me.[P] They realized how pathetic I was.[P] Soon [GLITCH]Mari will leave me too, and I'll be all[GLITCH] alone.[P] I-- ")
	fnDialogue("Thought: ugh, stop.[P] Jus[GLITCH][GLITCH]t go to sleep, Stacy.[P] Just sleep.")
	fnDialogue("[HIDEALL][MUSIC|Null][BG|Null]Thought: ...")
	fnDialogue("[BG|Root/Images/Backgrounds/All/WalkHome][CHARENTER|Stacy]Stacy: [E|Shock][PLAYERNAME]! [PLAYERNAME]!!")
	fnDialogue("Player: Stacy.")
	fnDialogue("Stacy: [E|Shock]Oh, thank god you nerd, you had me sweating![P] You just slumped down all of a sudden.")
	fnDialogue("Player: I-I'm sorry.")
	fnDialogue("Stacy: [E|Shock]What, don't apologize for just getting woozy![P][E|Mad] I'm worried about you. A-are you okay, should I call a doctor?")
	fnDialogue("Thought: But I wasn't apologizing for passing out. [P]I didn't really understand what I had seen...")
	fnDialogue("Thought: That couldn't have been [ITALICS]Stacy's[ENDITALICS] memory.[P] She the most strong, courageous, and lighthearted person I know.")
	fnDialogue("Thought: She's holding me up with her arms right now,[P] and I hastily regain my balance. ")
	fnDialogue("Player: I'm fine. ")
	fnDialogue("Thought: She doesn't let go.")
	fnDialogue("Stacy: [E|Sad]I don't know about that.")

    --Rejoin.
    LM_ExecuteScript(LM_GetCallStack(0), "Rejoin")

--Talking. Nobody has done that before!
elseif(sTopicName == "BatterChox") then
	
	--Dialogue.
	fnDialogue("Thought: Maybe I don't have to read Stacy's memories.[P] If I [GLITCH]take my time, and I'm patient... well, I don't mind just talking to her.")
	fnDialogue("Player: Erm... is there something you want me to do?")
	fnDialogue("Stacy: [E|Shock]Huh?")
	fnDialogue("Player: When you lean in, and look at me like that, I... you always seem a little disappointed with me,[P] like I'm not responding the way you want me to.")
	fnDialogue("Player: Is there something I'm supposed to be doing?")
	fnDialogue("Thought: Stacy stands up straight, turning redder than I've ever seen her turn.")
	fnDialogue("Stacy: [E|Shock]Oh, no.[P] Jeez.[P] Shit. I---*ahem*.[E|Mad][P] Sorry, I didn't mean to make you uncomfortable.")
	fnDialogue("Player: No, I-- I think if you just told me what you wanted--I could do it.")
	fnDialogue("Stacy: [E|Blush]Hahaha... geez. It's embarrassing.")
	fnDialogue("Stacy: I'm just... trying to get you to like me. ")
	fnDialogue("Player: !!!")
	fnDialogue("Stacy: [E|Sad]I can get a little intense when I'm trying to make new friends.")
	fnDialogue("Stacy: Sometimes I just kind of do it without even knowing I'm doing it.[P] Turn up the charm and all that.")
	fnDialogue("Player: Oh. Well...[P]I do like you.")
	fnDialogue("Stacy: [E|Shock]Really?[P][P][P][E|Sad] I keep getting the feeling you're weirded out by me.")
	fnDialogue("Player: EHGHPFF.[P] ME??? The coma victim with the mysterious past--[P]weirded out by YOU, someone who can bench press the entire football team?")
	fnDialogue("Stacy: [E|Happy]BAHAHAAHA![E|N] I can't bench press a football team, you dope.[P][E|Mad] But I can definitely bench press more than them.")
	fnDialogue("Thought: Stacy playfully smacks me on the small of my back.[P] For the first time, it doesn't feel so overwhelming.")
	fnDialogue("Player: Okay, I guess if I'm really being honest, your methods are a little intense.[P] But the only thing I'm worried about...")

    --Rejoin.
    LM_ExecuteScript(LM_GetCallStack(0), "Rejoin")

--Rejoin.
elseif(sTopicName == "Rejoin") then
	
	--Dialogue.
	fnDialogue("Thought: We stand there.[P] Time seems to stretch.[P] There's a strange heat creeping up my neck, and a panicky, fluttering feeling in my chest.")
	fnDialogue("Thought: Stacy is standing so close to me, I can't bear to meet her eye.")
	fnDialogue("Player: Stacy.")
	fnDialogue("Stacy: [E|Happy][ITALICS]Oui oui[ENDITALICS]?")
	fnDialogue("Player: What if there was something wrong with me that I wasn't telling you?[P] Something so weird and unnatural... I...[P] would you hate me?")
	fnDialogue("Thought: Her slender hands slide from my back up to my shoulders.")
	fnDialogue("Thought: A shiver runs up my spine, and I try to focus on the thousands of tiny freckles that line her sharp jaw.")
	fnDialogue("Stacy: [E|Blush]Nope. No way.[P] I don't care about what people have, or if they're different.[P][E|Mad] What matters is what's in their heart. ")
	fnDialogue("Player: Um. H-How do you know what's in my heart?[P] Or what's in [ITALICS]anyone's[ENDITALICS] heart? ")
	fnDialogue("Thought: She finally lets go of me and laughs. ")
	fnDialogue("Stacy: [E|N]You've got me there! It's not easy to tell right away, that's for sure. ")
	fnDialogue("Thought: We keep walking, just a short ways now from my house.")
	fnDialogue("Stacy: [P][P][E|Sad]... I think lots of people are afraid of being judged, so they hide what they think are bad parts of themselves.")
	fnDialogue("Stacy: *sigh*, it actually works pretty well;[P] after all, who wants to do the [ITALICS]work[ENDITALICS] to get to know the [ITALICS]worst[ENDITALICS] parts of someone?")
	fnDialogue("Player: Mm.")
	fnDialogue("Stacy: [E|Mad]But the people who do try...[P][E|N] they become great friends.")
	fnDialogue("Stacy: [E|Blush] Which is why [ITALICS]I[ENDITALICS] really want to do the work to get to know all of [ITALICS]vous[ENDITALICS], [PLAYERNAME].[P] So we'll become great friends.")
	fnDialogue("Thought: I blush again.[P] My answer becomes my promise, to myself and to the club co-captain.")
	fnDialogue("Player: Likewise.")
	fnDialogue("Stacy: [E|Shock]*ahem*! Anyhow...[P][E|N]That's your place over there, right?[P] I guess I'll see you around, [PLAYERNAME]!")
	fnDialogue("[HIDEALL]Player: Yeah. [P]Thanks for walking me home, Stacy.")
	fnDialogue("[HIDEALL][BG|Null]Thought: ...")
    
    --Which chapter to jump to.
    local iChapterDestination = VM_GetVar("Root/Variables/System/Player/iChapterDestination", "N")
    if(iChapterDestination == 3.0) then
        LM_ExecuteScript(gsRoot .. "../Chapter 3/Gameplay_SceneD/2 - Arrive At Home.lua", "Start")
        
    elseif(iChapterDestination == 4.0) then
        LM_ExecuteScript(gsRoot .. "../Chapter 4/Gameplay_SceneD/0 - Walk Home.lua", "LimoScene")
    end

end
