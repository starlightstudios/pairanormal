-- |[ ============================= Walking Home With Mari, Scene 0 ============================ ]|
--Walking home with Mari for the first time. This can be called from one of two different places.
-- The chapter variable marks which location to move the script back to when this ends.

-- |[Arg Resolve]|
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

-- |[ ======================================= Execution ======================================== ]|
--Begin.
if(sTopicName == "Start") then

    --Add 5 to friendship score.
    local iMariFriend = VM_GetVar("Root/Variables/System/Romance/iMariFriend", "N")
    VM_SetVar("Root/Variables/System/Romance/iMariFriend", "N", iMariFriend + 50.0)

    --Flag.
    VM_SetVar("Root/Variables/System/Player/iWalkedWithMariA", "N", 1.0)
	
	--Dialogue.
	fnDialogue("[HIDEALL][MUSIC|Love]Thought: Now's my chance to hang out with Mari again.[P] Okay, sidle up to her real casual like. Don't look too excited or desperate.")
	fnDialogue("Player: H-Hey, Mari--")
	fnDialogue("[CHARENTER|Mari]Mari: [E|N]Hiya [PLAYERNAME]![P][E|Shock][CRASH] HEY![P][CRASH] LETS WALK HOME TOGETHER!!!!")
	fnDialogue("Player: ... Yes.[P] Sounds good to me.")
	fnDialogue("[HIDEALL][BG|Null]Thought: ...")
	fnDialogue([[[BG|Root/Images/Backgrounds/All/WalkHome][CHARENTER|Mari]Mari: [E|N]... And he was like, \"this stain is SEARED into the tile floors,[P] how do you expect us to get it out, blah blah blah\",[P] being a total drama queen. ]])
	fnDialogue("Player: Uh huh.")
	fnDialogue("Mari: [E|N]Meanwhile, I'm just looking at Stacy like--[P][E|Shock]this guy has no idea we literally just saved the boy's locker room from a potential supernatural invasion, right?")
	fnDialogue("Mari: So long story short,[P] [CRASH]NEVER purchase a demon summoning kit online.[P][E|Sad] They're a total rip off.")
	fnDialogue("Player: Good to know.")
	fnDialogue("Mari: [E|N]Anyway I've been rambling on. You've barely said a word.[P][E|Blush][CRASH] How was your day?[P][CRASH] What's on your mind?[P][CRASH][E|Shock] TELL ME ALL THE THINGS.")
	fnDialogue("Player: Ah..you know, just the usual.[P] The most exciting bit was the club stuff, and you were there for that.")
	fnDialogue("Thought: Truthfully, I was still mulling over how I could distract Mari long enough to read her mind...")
	fnDialogue("Thought: And find out if I was a member of the club, or an object of investigation.[P] The technique I'd decided on...")
    
	--What to say to Mari:
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (What should I do?)") ]])

	local sDecisionScript = "\"" .. gsRoot .. "../Chapter Walk/Walkhome Mari 0.lua\""
	fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Take her out for a picnic\", " .. sDecisionScript .. ", \"Picnic\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Distract her with ice cream\", " .. sDecisionScript .. ", \"IceCream\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Agree to read tentacle boyfriend\",  " .. sDecisionScript .. ", \"Tentacles\") ")
	fnCutsceneBlocker()

--Picnic.
elseif(sTopicName == "Picnic") then
	
	--Dialogue.
	fnDialogue("Thought: ... Of course, my tool of choice was none other than taking Mari out for a fancy picnic!")
	fnDialogue("Player: Mari,[P] BEHOLD!")
	fnDialogue("Mari: [E|Shock][CRASH]Oh WOW![P] What is that?")
	fnDialogue("Player: ...[P]It's a picnic blanket. ")
	fnDialogue("Mari: [SFX|World|Blink]Really? It looks kind of like a towel.")
	fnDialogue("Player: [P][P][P][SFX|World|Ugh]It is actually a towel.[P] I... I wanted to take you on a picnic. But.[P] I don't know how to prepare food.")
	fnDialogue("Player: Or known exactly what a picnic even is.[P] But [ITALICS]I know there's a blanket involved[ENDITALICS].")
	fnDialogue("Mari: [E|Sad]...[PLAYERNAME]...")
	fnDialogue("Thought: Oh, darn. I'm being totally weird right now, aren't I?[P] I would have looked up how to do this properly if I knew how to read!")
	fnDialogue("Mari: [E|Sad]You want to go on a picnic...[P]with [ITALICS]me[ENDITALICS]?")
	fnDialogue("Player: [CRASH]Hrck!")
	fnDialogue("Thought: The towel and I are crushed as we're encompassed by Mari's arms.[P] It's not the intended result, but you won't hear me complaining.")
	fnDialogue("Mari: [E|Happy]Don't worry, [PLAYERNAME]! [P]I'll teach you everything you need to know!")
	fnDialogue("Thought: Mari guides me to a nearby grassy bank, and I stretch the towel out. ")
	fnDialogue("Mari: [E|Blush]Step one, you need a blanket and a buddy.[P] Step two...")
	fnDialogue("Thought: She rifles around in her backpack until she withdraws a small, beat-up red box.")
	fnDialogue("Mari: [CRASH][E|Blush]Snacks![P] Here, have a Pocky.")
	fnDialogue("Thought: She shakes the packaging at me, and a tiny sliver of chocolate-covered breadstick slides out.")
	fnDialogue("Thought: I take it and try a bite.")
	fnDialogue("Player: Mmm. ")
	fnDialogue("Mari: [E|N]I usually have a box tucked away for emergencies. It's good, isn't it?")
	fnDialogue("Player: Yeah.")
	fnDialogue("Mari: [E|N]Have another one.[P][E|Happy] Heck, have the whole box!")
	fnDialogue("Player: Oh, no,[P] I couldn't possibly--")
	fnDialogue("Mari: [E|MAD][CRASH]ITS MY GIFT TO YOU.")
	fnDialogue("Player: --thank you so much.")

    --Rejoin.
    LM_ExecuteScript(LM_GetCallStack(0), "Rejoin")

--Ice cream.
elseif(sTopicName == "IceCream") then
	
	--Dialogue.
	fnDialogue("Thought: ... Of course, my unbeatable tactic is distracting Mari with ice cream!")
	fnDialogue("Player: Mari, BEHOLD!")
	fnDialogue("Mari: [E|Shock]What is it?")
	fnDialogue("Player: I got us...[P]SOME ICE CREAM.")
	fnDialogue("Mari: [E|Shock]OH MY GOD.[P][CRASH][E|Mad] I LOVE ICE CREAM!!!")
	fnDialogue("Thought: [ITALICS]Of course you do, Mari. I'm new to this world, but even I know--everyone likes ice cream.[ENDITALICS]")
	fnDialogue("Thought: I chuckle inwardly at the sheer genius of the plan. Who could resist it's creamy, refreshing allure?")
	fnDialogue("Thought: Having been exposed to the frosty dairy deliciousness only once, my life has never been the same.")
	fnDialogue("Thought: One time Eilette had to physically restrain me at a Dairy Queen.[P] Anyway, I planned everthing out ahead of time.")
	fnDialogue("Thought: Rifling through my backpack, I pulled out the two ice creams I had purchased for this exact purpose.")
	fnDialogue("Mari: [E|Shock][P][P][E|Blush]Pfffft.[P] You big silly goof.")
	fnDialogue("Player: What? What's wrong?")
	fnDialogue("Mari: [E|N]You know you gotta keep ice cream FROZEN, right? Otherwise it'll turn into a big puddle of goop?")
	fnDialogue("Thought: The lukewarm package in my hand wobbles helplessly, confirming this.[P] [ITALICS]Curses[ENDITALICS]!")
	fnDialogue("Mari: [E|Happy]I'm sorry I'm laughing [P]*giggle*[P] it's just...[P][E|N]sometimes you are kind of adorable, [PLAYERNAME].")
	fnDialogue("Player: D-don't say that![P] It's embarrassing...")
	fnDialogue("Thought: I toss the ice gloop in the trash, and my empty hand is quickly filled with Mari's.")
	fnDialogue("Mari: [|N]Come on. There's a corner store down here that sells ice cream!")
	fnDialogue("Player: Whha... yes![P] This is great!")
	fnDialogue("[HIDEALL][BG|Null]Thought: ...")
	fnDialogue("[BG|Root/Images/Backgrounds/All/WalkHome][CHARENTER|Mari]Mari: [E|Shock]W-wow. When you said you liked ice cream...[P]I had no idea it was [ITALICS]that much[ENDITALICS].")
	fnDialogue("Player: [CRASH][ITALICS]I C E C R E A M[ENDITALICS].")
	fnDialogue("Mari: [P][E|Happy]... HAHAHAHAHAHAHAAAAA!!! ")
	fnDialogue("Thought: Mari is so gleeful she can't help but rap her palms against my shoulder, and I feel like chuckling.[P] This was a lot of fun.")
	fnDialogue("Mari: [E|Blush][PLAYERNAME]... Thanks for doing this with me... ah.")
	fnDialogue("Mari: [E|Sad]After today's craziness--*whew*![P][E|N] I definitely could use the pick me up.")
	fnDialogue("Player: No problem. I mean, anytime!")
	fnDialogue("Mari: [E|Sad]Sometimes, I feel like...")

    --Rejoin.
    LM_ExecuteScript(LM_GetCallStack(0), "Rejoin")

--Hentai.
elseif(sTopicName == "Tentacles") then
	
	--Dialogue.
	fnDialogue("Thought: The best way that I figured to do this was, of course, letting Mari's guard down with the one thing she's always wanted to share.")
	fnDialogue("Player: MARI.")
	fnDialogue("Mari: [E|N]Yeah?")
	fnDialogue("Player: I HAVE AN INTEREST IN...[P]FANFICTION")
	fnDialogue("Mari: [E|Shock][CRASH]YOU DO!?!?![P][CRASH] OH MY GOSH!")
	fnDialogue("Thought: She takes me by the hand, and in moments I'm flying down the streets to the nearest park bench.")
	fnDialogue("Mari: [E|Happy]This proves my theory that even if you're not familiar with the source material,[P][E|Shock][CRASH] EVERYONE can enjoy fanfiction!")
	fnDialogue("Thought: She plops us down and immediately starts tearing through her backpack.")
	fnDialogue("Mari: [E|Blush]The tentacle-covered allure is too much for any one man to resist![P][E|N][CRASH] AHA![P] Here.")
	fnDialogue("Player: Isn't that your science binder?")
	fnDialogue("Mari: [E|N]It's my printouts.[P] Any time I find some really good fiction that I want to revisit--[P]or sometimes revise if it's not spicy enough--")
	fnDialogue("Mari: I print it out and put in a cleverly disguised science binder.[P][E|Blush] [ITALICS]Then I can read it even while I'm sitting in class![ENDITALICS]")
	fnDialogue("Player: Mari...[P]that's genius.")
	fnDialogue("Mari: [E|Happy]Pfffft. Well, anyhow.[P][E|N] I think this one is great for beginners:")
	fnDialogue([[Mari: [E|Shock]\"Mario Died in a Lava Pit:: [P]Now I Can Finally Give My Virginity to Bowser Without Being Interrupted!\"]])
	fnDialogue("Thought: Mari opens the binder, but stops.[P] Her tone gets low.")
	fnDialogue("Mari: [E|Sad]Erm, how's your reading been lately?")
	fnDialogue("Player: Yeah. About that--[P][SFX|World|Ugh]I'm still not very proficient.")
	fnDialogue("Mari: [E|N]Ah. Well, I can just read it to you for now.[P] Is that okay?")
	fnDialogue("Thought: I nod, and with a big grin, Mari's eyes adjust to the first page.")
	fnDialogue([[Mari: [E|Happy]Ahem...\"Chapter 1 - News.[P][E|Shock] \"Princess Peach! Princess Peach![P][E|N] Urgent news from our kingdom's messengers.\"..\"]])
	fnDialogue([[Mari: \"Toad's tiny knuckles bang against the wide doors of my boudoir,[P] the rattles echoing all the way up to my bedpost\"]])
	fnDialogue("Player: Sorry, what's a boudoir?")
	fnDialogue("Mari: [E|N]It's like a fancy way of saying bedroom.")
	fnDialogue("Player: Oh, okay, cool.[P] Continue.")
	fnDialogue([[Mari: [E|Mad]\"\"In a minute!\" I hollered, trying to contain my irritation.[P][E|N] Even though my castle is large, it's hard to get any privacy or a moment to myself.\"]])
	fnDialogue([[Mari: [E|Mad] After all, at that very instant,[P] I was doing my best to thrust a beautiful, plump 1up mushroom into my--\"[P][P][E|Shock] Er.]])
	fnDialogue("Player: What's the matter?")
	fnDialogue("Mari: [E|Blush]I'm going to have to explain...[P]a lot of things to you while I'm reading this [P][ITALICS]out loud[ENDITALICS], aren't I?")
	fnDialogue("Player: Yeah.[P] I'm really sorry if--")
	fnDialogue("Mari: [E|Sad]No, it's not you--It's just, this is totally different from when I'm reading this at home or in class!")
	fnDialogue("Mari: [E|Blush]I'm suddenly very aware of...[P][E|Sad]how kinky this is.")
	fnDialogue("Player: ???")
	fnDialogue("Mari: [E|Sad]And it's not really my kind of kink. Yet, anyway.[P][E|N] Sorry?")
	fnDialogue("Thought: Mari's shoulders sag, and she starts to close the binder.")
	fnDialogue("Player: *ahem*[P] So you can't read the story to me?[P] You won't even tell me the plot?")
	fnDialogue("Mari: [CRASH][E|Shock]AH![P] That's brilliant, [PLAYERNAME]!")
	fnDialogue("Mari: [E|N] I can [ITALICS]absolutely[ENDITALICS] tell you the plot while skipping over the parts that...[P]er.[P][E|Blush] Aren't relevant to the story. ")
	fnDialogue("Thought: Relief floods Mari's cheeks, even though I don't really understand why.")
	fnDialogue("Thought: But at least for the moment, the enthusiastic smile that left her face is back and in my full view.[P] I'm certainly not going to complain.")
	fnDialogue("Mari: [E|Shock]Okay.[P][E|N] So it all starts when the main character, Princess Peach, recieves this terrible news...")
	fnDialogue("[HIDEALL][BG|Null]Thought: ...")
	fnDialogue("[BG|Root/Images/Backgrounds/All/WalkHome][CHARENTER|Mari]Player: ... Hmmm, I'm starting to understand why she kept going back to this 'Bowser' guy,[P] even though technically he is a villain. ")
	fnDialogue("Mari: [E|Happy]Exactly![P][E|N] That's one of the things I really appreciate about the story, it gives context to stuff that happens in the games.")
	fnDialogue("Thought: Mari, who's been talking for a long time now, takes a few seconds to catch her breath.[P] I watch her fingers idly play with the hem of her skirt.")
	fnDialogue("Mari: [E|Happy]Oh man. I've been wanting to talk to someone about this since, like, forever.[P][E|Blush] Thanks for showing an interest, [PLAYERNAME].")
	fnDialogue("Player: You're always so excited about fanfiction.[P] I was curious to know why!")
	fnDialogue("Mari: [E|Sad]Sometimes I get a little carried away.[P] I'm worried people will think that I'm trying to force my interests or my opinion on them.")
	fnDialogue("Mari: [E|Shock] But there used to be a time where--")

    --Rejoin.
    LM_ExecuteScript(LM_GetCallStack(0), "Rejoin")

--Rejoin.
elseif(sTopicName == "Rejoin") then
	
	--Dialogue.
	fnDialogue("Thought: Mari's phone beeps.")
	fnDialogue("Mari: [E|N]Whoops! One second.[P] What--[P][P][E|Shock][CRASH]OH MY GOSH ITS SO LATE!!!![P][CRASH] I GOTTA GET HOME!!!")
	fnDialogue("Player: Oh, r-right!")
	fnDialogue("Mari: [E|Blush]OKAY, I'LL, UHHH!!!! I'LL SEE YOU LATER!!!")
	fnDialogue("Thought: Mari leaps up and hastily gathers all her things together, a few stray papers and pens flying out of her bag.")
	fnDialogue("Player: Oh Mari, you dropped some--")
	fnDialogue("Mari: [E|Mad][CRASH]LEAVE IT.[P][CRASH] ONLY THE STRONG SURVIVE.")
	fnDialogue("[HIDEALL]Thought: With that, Mari waves me goodbye and trots away. I hold her abandoned pen in my hand and smile.")
	fnDialogue("Thought: Silly Mari, always leaving things behind and DANGIT I FORGOT TO READ HER MIND!!! I could slap myself.")
	fnDialogue("Thought: I was prancing about, all doe-eyed, having such a [ITALICS]great[ENDITALICS] time, I didn't do the one thing I meant to!")
	fnDialogue("Thought: I can still see her down the road. I... [P][GLITCH]I can do it, can't I?[P] It's a lit[GLITCH]tle far... only one way to find out.[GLITCH][GLITCH][GLITCH]")
	fnDialogue("[BG|Null][MUSIC|Null]Thought: [P].[GLITCH].[P].[P]")
	fnDialogue("[BG|Root/Images/Backgrounds/All/Hospital]Thought: I'm seated, or rather, Mari [GLITCH] is seated in a hospital bed.[P] This must be the memory of when we met.")
	fnDialogue("[MUSIC|Somber]Thought: On a wall in front of me, a television is showing some old film. In it, a gruesome monster bursts out of a lake to drag two teenagers into the murky depths.")
	fnDialogue("Thought: Funny, I don't remember this playing in the hospital.")
	fnDialogue("???: [VOICE|Voice|Stacy]Hey, you!")
	fnDialogue("Thought: I look to my right, and am astonished to see Stacy Croft sitting in a bed, too.[P][CHARENTER|YStacyHos] Her right arm and leg are locked away in densely scribbled casts.")
	fnDialogue("Player: [VOICE|Voice|Mari]Oh, s-sorry![P] Is this too gruesome? I can change the channel--")
	fnDialogue("Stacy: [FOCUS|YStacyHos][VOICE|Voice|Stacy][E|N]Heck no![P] I was gonna ask if you could turn it up. I can barely hear it from here.")
	fnDialogue("Thought: A smile spreads on my face and I increase the volume.[P] Who knew Stacy Croft, one of the coolest kids in Foster Middle School, was a fan of old horror flicks?")
	fnDialogue("Thought: Granted, she probably doesn't like them because she thinks the evil monsters look kind of hot,[P] like I do.")
	fnDialogue("Thought: I wonder if she knows who I am--we don't share any classes, after all.")
	fnDialogue("Thought: We watch the brutal broadcast for a few more minutes, nothing but the sound of television shrieks filling the hall.")
	fnDialogue("Stacy: [FOCUS|YStacyHos][VOICE|Voice|Stacy][E|Sad]So...[P] what are you in for?")
	fnDialogue("Player: [VOICE|Voice|Mari]I... uh.[P] It's complicated. I drowned.[P] And now there's water in one of my lungs.")
	fnDialogue("Stacy:[FOCUS|YStacyHos][VOICE|Voice|Stacy] [E|Shock]Holy crap. Are you gonna die!?")
	fnDialogue("Thought: I'm so startled by her answer, I can't help but giggle.")
	fnDialogue("Player: [VOICE|Voice|Mari]No! At least I hope not.[P] It would be the most embarrassing funeral ever--")
	fnDialogue("Thought: I glance down at my lap, sighing.")
	fnDialogue("Player: [VOICE|Voice|Mari]'Here lies Mari, who died of a panic attack in her bikini.'")
	fnDialogue("Stacy: [FOCUS|YStacyHos][VOICE|Voice|Stacy][E|Shock]Panic attack? But I thought you said you drowned!")
	fnDialogue("Player: [VOICE|Voice|Mari]I drowned because I was having a panic attack in a pool.[P] Ugh, it's my own [ITALICS]stupid[ENDITALICS] fault.[P] I stopped taking my pills.")
	fnDialogue("Stacy:[FOCUS|YStacyHos][VOICE|Voice|Stacy] Pills?")
	fnDialogue("Player: [VOICE|Voice|Mari]For my OCD. ")
	fnDialogue("Thought: [ITALICS]Why are you telling her this, Mari?[P] For god's sakes stop before your reputation at school becomes totally irreperable![ENDITALICS]")
	fnDialogue("Thought: Now [ITALICS]everyone[ENDITALICS] will know, if they don't already, that I'm a medicated lunatic!")
	fnDialogue("Player: [VOICE|Voice|Mari]I-I was actually fine without them, and my boyfriend Chester Rockford, says that the government gives them to kids to keep them compliant.")
	fnDialogue("Player: [VOICE|Voice|Mari]So naturally, I ditched 'em.")
	fnDialogue("Thought: I could kick myself for name-dropping Chester, a very cute and popular boy at Foster Mid--")
	fnDialogue("Thought: --without mentioning that we'd actually broken up shortly after the whole incident.")
	fnDialogue("Thought: But after spilling my most embarrassing story to Stacy [ITALICS]for some reason[ENDITALICS],[P] I was happy to scrape the bottom of the barrel for any recovering brownie points.")
	fnDialogue("Stacy: [FOCUS|YStacyHos][VOICE|Voice|Stacy][E|N]Well, I guess you did need them after all, huh?")
	fnDialogue("Thought: She snickers like its the funniest thing in the world.[P] I want to die.")
	fnDialogue("Stacy: [FOCUS|YStacyHos][VOICE|Voice|Stacy][E|Shock]So your boyfriend told you to stop taking OCD pills, which triggered a panic attack while you were at a pool, causing you to drown and get water in your lungs?")
	fnDialogue("Thought: There's a pause, and her next words are much softer.")
	fnDialogue("Stacy: [FOCUS|YStacyHos][VOICE|Voice|Stacy][E|Sad]That [ITALICS]is[ENDITALICS] complicated.")
	fnDialogue("Player: [VOICE|Voice|Mari][GLITCH]A-Anyway, how about you?")
	fnDialogue("Stacy: [FOCUS|YStacyHos][VOICE|Voice|Stacy][E|Shock]Oh, these casts?[P][E|Happy] I did a backflip off of my roof.")
	fnDialogue("Stacy: [FOCUS|YStacyHos][VOICE|Voice|Stacy]There's this website on the internet called YouTube, and you can put all your really cool stunts on there for people to see.")
	fnDialogue("Stacy: [FOCUS|YStacyHos][VOICE|Voice|Stacy][E|Sad]I'm definitely not gonna put this one on, though.")
	fnDialogue("Thought: I nod, though it is an empty nod.[P] The divide between us could not be more great.")
	fnDialogue("Thought: Even Stacy's injuries were glamorous, whereas I wound up in hospitals thanks to my incredible impersonation of a Magikarp.")
	fnDialogue("Stacy: [FOCUS|YStacyHos][VOICE|Voice|Stacy][E|Shock]I know Chester Rockefeller, by the way.")
	fnDialogue("Player: You do?")
	fnDialogue("Stacy: [FOCUS|YStacyHos][VOICE|Voice|Stacy][E|N]Yeah. He's a dick. ")
	fnDialogue("Thought: The two of us lock eyes, then burst into laughter.[P] Probably not great for my lung, but I don't care.")
	fnDialogue("Player: [VOICE|Voice|Mari]Heehee, I know he is.[P] Well, I know [ITALICS]now[ENDITALICS]--we broke up.")
	fnDialogue("Stacy: [FOCUS|YStacyHos][VOICE|Voice|Stacy][E|N]Oh, good.")
	fnDialogue("Player: It's just...[P] he was so cute.[P] And he was like, one of the only guys who paid attention to me.[P] I just listened to whatever he had to say.")
	fnDialogue("Stacy: [FOCUS|YStacyHos][VOICE|Voice|Stacy][E|Sad]Yeah, I get it. I've been there a couple times myself.[P][E|N] Hey, have you ever thought about dating girls? ")
	fnDialogue("Thought: [ITALICS]Absolutely, all the time.[ENDITALICS]")
	fnDialogue("Player: [CRASH]N-no! Never.")
	fnDialogue("Stacy: [FOCUS|YStacyHos][VOICE|Voice|Stacy][E|N]Well, I do sometimes, and it's pret-ty great. Give it a shot sometime, you might like it!")
	fnDialogue("Thought: I just shake my head.")
	fnDialogue("Player: Tuh. What's the use, anyway?[P] Nobody's gonna wanna talk to me, let alone wanna date me after this.")
	fnDialogue("Player: I take crazy pills, I was dumped, and I watch 25-year old horror movies for fun.")
	fnDialogue("Thought: Stacy regards me for a long moment.[P] I curl my knees up to my chest, wishing she wouldn't.[P] Finally, she snorts.")
	fnDialogue("Stacy: [FOCUS|YStacyHos][VOICE|Voice|Stacy][E|N]Well, if you're so sure nobody wants to be your friend anyway,[P] why don't you stop trying to impress people and just do what you want?")
	fnDialogue("Thought: My eyebrows arch, and she shrugs like its the most obvious thing in the world.")
	fnDialogue("Stacy: [FOCUS|YStacyHos][VOICE|Voice|Stacy][E|Sad]That's what I do. ")
	fnDialogue("Thought: My instinct is to scoff.[P] How could Stacy Croft [ITALICS]possibly[ENDITALICS] know what it's like to live in dorkdom?[P] And yet, I'm forced to admit that her logic is sound.")
	fnDialogue("Thought: Come to think of it... I don't think I'd ever tried to do things for myself.")
	fnDialogue("Thought: Not since puberty started, and I was suddenly aware-slash-terrified-slash-paranoid that everyone was judging me all the time.")
	fnDialogue("Stacy: [FOCUS|YStacyHos][VOICE|Voice|Stacy][E|N]Hey, check it out. This is the best part!")
	fnDialogue("Thought: The monster on tv, sweeping a pretty blonde teenager into his arms, turns around to face a boy who brandishes a baseball club at him.")
	fnDialogue("Thought: The monster promptly bites off his head, and I find myself [GLITCH]pretending it's Chester's. ")
	fnDialogue("Stacy: [FOCUS|YStacyHos][VOICE|Voice|Stacy][E|Sad]Okay, you're gonna think I'm nuts,[P][E|Shock] but don't you think the monster in this is kinda hot? ")
	fnDialogue("Player: [CRASH]!!!")
	fnDialogue("[HIDEALL][MUSIC|Null][BG|Null]Thought: .[P].[P][GLITCH].[P]")
	fnDialogue("[BG|Root/Images/Backgrounds/All/WalkHome]Player: Mari... Ugh, my head.")
	fnDialogue("Thought: That jump was a stretch.[MUSIC|Love] I sit up from the sidewalk, wiping what I hope is a rotten banana peel off of my elbow.")
	fnDialogue("Thought: So, that's Mari's memory?[P] She's come a long way since being the little girl in that hospital.")
	fnDialogue("Thought: There's no doubt that the club is all her idea--she's doing something that makes her happy.[P] And I realize that I am, too. ")
	fnDialogue("Thought: I'm certain now that the only reason the club members keep me around is because I'm strange...[P]but for all I know, that's what friendship is supposed to be.")
	fnDialogue("Thought: It's not really that bad, is it?[P] I swallow a deep, raw lump in my throat. ")
	fnDialogue("Player: *sigh*... I'd better get home.")
	fnDialogue("[HIDEALL][BG|Null]Thought: ...")
    
    --Which chapter to jump to.
    local iChapterDestination = VM_GetVar("Root/Variables/System/Player/iChapterDestination", "N")
    if(iChapterDestination == 3.0) then
        LM_ExecuteScript(gsRoot .. "../Chapter 3/Gameplay_SceneD/2 - Arrive At Home.lua", "Start")
        
    elseif(iChapterDestination == 4.0) then
        LM_ExecuteScript(gsRoot .. "../Chapter 4/Gameplay_SceneD/0 - Walk Home.lua", "LimoScene")
    end
end
