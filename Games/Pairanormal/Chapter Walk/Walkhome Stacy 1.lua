-- |[ ============================ Walking Home With Stacy, Scene 1 ============================ ]|
--Walking home with Stacy for the second time.

-- |[Arg Resolve]|
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

-- |[ ======================================= Execution ======================================== ]|
--Begin.
if(sTopicName == "Start") then

    --Add 5 to friendship score.
    local iStacyFriend = VM_GetVar("Root/Variables/System/Romance/iStacyFriend", "N")
    VM_SetVar("Root/Variables/System/Romance/iStacyFriend", "N", iStacyFriend + 50.0)
	
	--Dialogue.
	fnDialogue("[HIDEALL][MUSIC|Love]Player: Hey Stacy, wanna walk home together again?")
	fnDialogue("[CHARENTER|Stacy][CHARENTER|Mari]Stacy: [E|N]You bet![P] I'll see you tomorrow, Mari.")
	fnDialogue("Mari: [E|Happy]Hehehe...[P] I'll bet you will.")
	fnDialogue("Player: Why...[P] why is she looking at us with that creepy smile?")
	fnDialogue("Stacy: [E|Mad]I have no idea and I also refuse to acknowledge it.")
	fnDialogue([[Mari: [E|Blush]This is exactly like Chapter 32 of [ITALICS]\"My Best Friends Are Actually My KinGuardians??? And They're in Love!???\"[ENDITALICS] ]])
	fnDialogue("[HIDEALL][BG|Null]Thought: ...")
	fnDialogue([[[BG|Root/Images/Backgrounds/All/WalkHome][CHARENTER|Stacy]Stacy: [E|N]... And that's when I told her, \"look, I'm not gonna join your softball team,[P][E|Happy] but I will ABSOLUTELY go to second base with you if you want to\".]])
	fnDialogue("Player: I think I'm beginning to sense a pattern to a lot of your stories.")
	fnDialogue("Stacy: [E|N]They all do end in a makeout sesh. I can't help that.")
	fnDialogue("Thought: We laugh so hard, tears and snorts start to escape me.[P] I have begun to feel less nervous and just enjoy my time with Stacy.")
	fnDialogue("Thought: She can be boisterous, but it's her way of connecting with people around her.[P] I wonder if someday, I could have the confidence in myself she does.")
	fnDialogue("Stacy: [E|Shock]Oh cool, look!")
	fnDialogue("Thought: The small park we pass by on the way home has a modest smooth cement court which is used for playing basketball.")
	fnDialogue("Thought: But it's always been empty until today.")
	fnDialogue("Stacy: [E|N]Someone left a basketball![P] Want to play?")
	fnDialogue("[HIDEALL]Thought: I nod and follow her to the court, ignoring the fact that I have no idea how.")
	fnDialogue("Thought: I don't have Stacy's confidence, but I'm hoping the time we spend together will rub some off on me. ")
	fnDialogue("[BG|Root/Images/Backgrounds/All/BasketballCourt][CHARENTER|Stacy]Stacy: [E|N]Here we go. How about a little one-on-one?[P] A.. [ITALICS]tete-a-tete[ENDITALICS], if you will?")
	fnDialogue("Thought: My eyes widen as Stacy deftly picks up the ball and starts bouncing it--first from one hand, then the other, then back and forth between her legs.")
	fnDialogue([[Thought: Each contact from ball to court makes a satisfying \"crack\". ]])
	fnDialogue("Thought: This doesn't look too hard![P] I smile, and Stacy tosses it over to me.")
	fnDialogue("Player: Let's see here...")
	fnDialogue("Thought: I drop the ball down, ready for my first foray into becoming a dribbling star.")
	fnDialogue("Thought: The basketball lands on my foot with a soft thud and slowly rolls away from me.")
	fnDialogue("Player: [SFX|World|Ugh]Wow.[P] It didn't even bounce once. ")
	fnDialogue("Thought: Stacy is doubled over with laughter.")
	fnDialogue("Stacy: [E|Happy]Okay, maybe one-on-one isn't a great idea.")
	fnDialogue("Player: This isn't one of the sports I'll have to play in PE, is it?")
	fnDialogue("Stacy: [E|Sad]Ehhhhh... Look, don't worry.[E|N] I can show you a couple tricks.")
	fnDialogue("Stacy: [E|Happy]It doesn't matter HOW bad you are at ol' b-ball, no one will pick on you if you can pull this off.")
	fnDialogue("Thought: She picks up the ball and palms it into a strange position.")
	fnDialogue("Thought: With a flick of her wrist, I'm mesmerized, drifting through space,[P] as I watch a small brown planet spin around the tip of her finger.")
	fnDialogue("Player: Whoah...")
	fnDialogue("Stacy: [E|N]Or how about this!?!")
	fnDialogue("Thought: Stacy darts across the court, sending a gust of wind my way as she breezes by.")
	fnDialogue("[HIDEALL][IMAGE|Root/Images/MajorScenes/All/Stacy]Thought: I blink, and the next thing I know she's stepping on air.")
	fnDialogue("Thought: I could swear she floated for the briefest moment--until her hands come crashing down on the rim of the basketball hoop, slamming the ball through it.")
	fnDialogue("Thought: The impact reverbates through the whole court, and I'm reminded of a fireworks show.")
	fnDialogue("Player: [CRASH]WOW!!!!")
	fnDialogue("[IMAGE|Null]Stacy: *huff* *huff**...[P]What do you think?")
    
	--What to say to Stacy:
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (What should I say?)") ]])

	local sDecisionScript = "\"" .. gsRoot .. "../Chapter Walk/Walkhome Stacy 1.lua\""
	fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Teach me the ball-spinny thing\", " .. sDecisionScript .. ", \"BallySpin\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Teach me the jumping slamming thing!\", " .. sDecisionScript .. ", \"SpaceJamLolTopical\") ")
	fnCutsceneBlocker()

--I played the basketyball.
elseif(sTopicName == "BallySpin") then
	
	--Dialogue.
	fnDialogue("Player: Can you teach me the spinny move?")
	fnDialogue("[CHARENTER|Stacy]Stacy: [E|N]You betcha! Come here.")
	fnDialogue("Thought: Stacy approaches me, ball in tow.[P] Her hand reaches for mine and places the basketball gingerly in my palm.")
	fnDialogue("Stacy: [E|N]First, just turn your wrist, like this.[P] Do it a couple of times slowly.")
	fnDialogue("Player: ...[P]like this?")
	fnDialogue("Stacy: [E|N]Yeah, that's pretty good.")
	fnDialogue("Stacy: [E|Mad] So this is the tricky part--you're gonna do it really fast, and as you spin, point your hand into like,[E|Happy] a little finger gun.[P] Like this.")
	fnDialogue("Thought: She guides each finger into the correct position,[P] and everytime her slightly chilled tips brush against my knuckles,[P] I think I might melt into a puddle.")
	fnDialogue("Stacy: [E|Happy]Okay, now you've just got to give it a shot.")
	fnDialogue("Thought: [P][P]The first attempt is best left unmentioned.")
	fnDialogue("Thought: The second time around, I think I have a better grasp on what I'm supposed to be doing--the ball positions itself correctly but veers away.")
	fnDialogue("Thought: The third time--")
	fnDialogue("Stacy: [CRASH][E|Shock]ahhh!")
	fnDialogue("Player: [CRASH]Haaah! Oh my gosh![P] Did you see that?")
	fnDialogue("Stacy: [E|Shock]You did it!")
	fnDialogue("Player: It was only for a second, but...")
	fnDialogue("Thought: That was really cool.[P] I'll bet if I practiced, I could make it spin for a long time.")
	fnDialogue("Stacy: [E|Happy][ITALICS]Tres bien,[ENDITALICS] [PLAYERNAME]!")
	fnDialogue("Player: Hey, you're a great teacher.")
	fnDialogue("Stacy: [E|Blush]Pshtt...")
	fnDialogue("Player: Club co-captain, part-time model, basketball instructor--[P]is there anything you can't do?")
	fnDialogue("Stacy: [E|Blush]Eheh...[P][E|Sad]All right, cut it out, you.")
	fnDialogue("Thought: She stoops down to pick up the stray basketball.")
	fnDialogue("Player: A more perfect creature never walked the earth.[P] I shudder to think what would happen if that power fell into the wrong hands.")

    --Rejoin.
    LM_ExecuteScript(LM_GetCallStack(0), "Rejoin")

--For you archaeologists visiting in 2145: It was a bad movie from the 90's starring one of the best basketball players ever: Michael Jordan. He teams up with cartoon characters like Bugs Bunny to defeat
-- a bunch of crappy space aliens in a basketball game. It was awful, including showing legitimately disturbing crappy graphics of the guy from Seinfield getting inflated and Bill Murray looking like Bill Murray.
-- The point is it became memetic because of its theme song which was a pseudo-rap song singing about slamming and jamming. We were still making fun of it 20+ years later.
elseif(sTopicName == "SpaceJamLolTopical") then
	
	--Dialogue.
	fnDialogue("Player: THAT WAS AMAZING!!! [P]Can you teach me the dunking thing?")
	fnDialogue("[CHARENTER|Stacy]Stacy: [E|Shock]Huh? Oh, [ITALICS]that[ENDITALICS].[P][E|Sad] Uh... well, I might've actually been overdoing it a bit. It's not exactly a beginner move.")
	fnDialogue("Player: Oh, come on.[P] You can't keep all the cool tricks to yourself![P] If I could do that, I'd be a knockout on the court for SURE.")
	fnDialogue("Stacy: [E|Sad]Hm...[P][E|N]Well, I guess the actual technique isn't terribly difficult in itself.")
	fnDialogue("Stacy: [E|Shock] The most important part is[P]--here, let's lose the ball for a second. How high can you jump?[P][E|N] Do a little demo.")
	fnDialogue("Thought: I nod and steel myself, shaking the muscles loose like the girls at the school track team.")
	fnDialogue("Thought: When I'm ready, I run up to the hoop, trying to match the Paranormal Club captain's form.")
	fnDialogue("Thought: The long post holding up the board seems to grow taller as I get close.[P] I shut my eyes so I don't chicken out.")
	fnDialogue("Thought: [ITALICS]Just jump! jump with all your might!!![ENDITALICS]")
	fnDialogue("Player: Hup---[P][P][P][CRASH]HRAAAARGH! ")
	fnDialogue("Stacy: [E|Shock]!!!")
	fnDialogue("Player: *huff* Ha.[P] ha.[P] ha, how's [ITALICS]THAT[ENDITALICS]!?!")
	fnDialogue("Stacy: [P][P][E|Shock]Oh wow, I was totally wrong.")
	fnDialogue("Stacy: [PLAYERNAME], you've definitely got what it takes--[P][P][E|Happy]to clear a small puddle.")
	fnDialogue("Player: [SFX|World|Ugh]Wahhh...")
	fnDialogue("Stacy: [E|N]Come on, don't pout at me like that! I'm pulling your leg.[P][E|Shock] *gasp* [ITALICS]sacre bleu[ENDITALICS].[P][E|Mad] I have a [ITALICS]brilliant[ENDITALICS] idea.")
	fnDialogue("Player: What, a trampoline?")
	fnDialogue("Stacy: [E|Shock]Close![P][E|N] What if I give you a cheerleader boost? That ought to give you some air.")
	fnDialogue("Stacy: [E|Happy] When you run up to me, step into my hands--and I'll lauch you up!")
	fnDialogue("Player: Uh...[P] I don't know about that.")
	fnDialogue("Stacy: [E|N]Mmm-hmm.[P] Well, it was only a suggestion.")
	fnDialogue("Thought: I nibble at my bottom lip.")
	fnDialogue("Thought: If I ever want to be as exceptional as someone like Stacy--or even vaguely normal, I've got to start having a little more confidence in myself.")
	fnDialogue("Thought: So, I walk back to the center of the court.")
	fnDialogue("Player: Okay.[P] You know what? Let's give it a shot.")
	fnDialogue("Stacy: [E|N]Yeah?")
	fnDialogue("Player: Yeah! Why not try it!?[P] Why should I let fear hold me back!?[P] I bet if I stopped doubting myself and just started DOING stuff, I'd be dunking like a pro in no time!!!")
	fnDialogue("Stacy: [E|Happy][ITALICS]Saikoooooo[ENDITALICS]![P] Hell yeah, [PLAYERNAME]. I like your spunk.")
	fnDialogue("Player: Yeah!!![P][CRASH] I'VE GOT SPUNK!!!")
	fnDialogue("[HIDEALL][BG|Null]Thought: ...")
	fnDialogue("[BG|Root/Images/Backgrounds/All/BasketballCourt]Info: Two minutes later.")
	fnDialogue("Player: I...[P][SFX|World|Ugh]I've got a concussion.")
	fnDialogue("Stacy: Don't worry...[P]*groan* I think my ribcage helped break your fall. *hrk*")
	fnDialogue("Player: W-wait! Don't get up.[P] Oof.[P] I'm not ready to move yet.")
	fnDialogue("Stacy: Hahaha, okay--ooh, ouch.[P] I don't think I'm ready to move, either.")
	fnDialogue("Thought: We lay on the smooth asphalt, a pitiful tangle of limbs, hopefully none of which are split to smithereens.")
	fnDialogue("Thought: In spite of the pain, I find myself giggling.[P] I feel Stacy craning her neck at me.")
	fnDialogue("Stacy: What's so funny?")
	fnDialogue("Player: I'm just glad... haha,[P] not about this.")
	fnDialogue("Thought: I push myself up slowly.[P] A quick inspection reveals no blood or terrible scrapes.")
	fnDialogue("Player: I'm just relieved to know there's actually something you're not good at.[P] Hahaha!")
	fnDialogue("[CHARENTER|Stacy]Stacy: [E|Shock]Heeeey!")
	fnDialogue("Thought: I finally feel brave enough to wobble into a standing position.")
	fnDialogue("Thought: The laughing is definitely not helping, but I can't resist. ")
	fnDialogue("Player: Oh, you know--a club captain, part time model, AND a basketball instructor?")
	fnDialogue("Player: That's a deadly combination.[P] I'd hate to see what happens if that power fell into the wrong hands.")

    --Rejoin.
    LM_ExecuteScript(LM_GetCallStack(0), "Rejoin")

--Rejoin.
elseif(sTopicName == "Rejoin") then
	
	--Dialogue.
	fnDialogue("Stacy: [E|Sad][P][P][P]... Please don't tease me like that.")
	fnDialogue("Player: Sorry, sorry. I got a little carried away there, haha.")
	fnDialogue("Stacy: [E|Mad]I'm not as cool or strong as everyone thinks.[P][E|Sad] Quite the opposite, in fact. ")
	fnDialogue("Thought: Stacy frets with the basketball, a shadow under her brow just like the first time we walked home together.")
	fnDialogue("Thought: I swallow hard. Back then, she said we barely knew each other.[P] But that can't still be true, can it?")
	fnDialogue("Player: Okay, now you're just being silly.[P] You--")
	fnDialogue("Stacy: [E|Mad][CRASH]I was supposed to go with Mari to the clubhouse, okay?[P] You happy? ")
	fnDialogue("Player: ...[SFX|World|Blink] Huh?")
	fnDialogue("Stacy: [E|Sad]... The day that the clubhouse exploded. [P][E|Mad][ITALICS] I was supposed to go there with Mari.[ENDITALICS]")
	fnDialogue("Stacy: [E|Shock]--But I couldn't even get out of BED that day,[SPEEDTEXT] and Mari was alone and she got HURT because I wasn't there and she was COUNTING on me to protect her!!![ENDSPEEDTEXT] ")
	fnDialogue("Thought: Stacy rises indignantly to her feet, the shrill timber of her voice following suit.")
	fnDialogue("Stacy: [E|Shock][SPEEDTEXT]I didn't even find out until a MONTH later that she was in the HOSPITAL, and I'm a terrible friend because I couldn't even get my stupid worthless self OUT OF BED that day--[ENDSPEEDTEXT] ")
	fnDialogue("Thought: Without thinking, my hand reaches out and steadies itself on my frantic companion's shoulder.[P] The contact seems to break her out of a terrible trance.")
	fnDialogue("Stacy: [E|Shock][P][P][E|Sad]I-I promised myself I wouldn't let anyone else down. [P][E|Mad]They [ITALICS]need[ENDITALICS] me to be strong. I h-have to be...")
	fnDialogue("Stacy: [E|Sad] Nevermind. I'm rambling.")
	fnDialogue("Player: Stacy.")
	fnDialogue("Stacy: [E|Sad]Whew, that was kind of weird.[P][E|N] Sorry--let's get out of here. You still need to get home, right?")
	fnDialogue("[HIDEALL]Thought: She makes a beeline for the sidewalk, walking a little too fast for me.")
	fnDialogue("Thought: All the while, she forces a halting, uncomfortable laugh.[P] But I know her outburst was anything but a joke.")
	fnDialogue("[BG|Root/Images/Backgrounds/All/WalkHome]Player: Stacy, can I just say one thing?")
	fnDialogue("[CHARENTER|Stacy]Stacy: [E|Sad][PLAYERNAME], [ITALICS]por favor[ENDITALICS].[P] Let's just change the subject.")
	fnDialogue("Player: Okay, fine.[P] Let's talk about me.")
	fnDialogue("Thought: This is an answer she's pleased with, a flirtatious smile growing on her lips.[P] --A familiar face I now recognize as pretense.")
	fnDialogue("Stacy: [E|Blush]Ahah! Now you have my attention.")
	fnDialogue("Player: [P][P]I lived in a bed for months.[P] I couldn't even walk or remember anything and I was completely alone.")
	fnDialogue("Player: Every day people and doctors would walk by me and none of them knew what I was going through.[P] Do you know when the first day I ever felt truly happy was?")
	fnDialogue("Stacy: [E|Shock]Er.[P] I don't know. ")
	fnDialogue("Player: [ITALICS]The day I met you and Mari.[ENDITALICS]")
	fnDialogue("Stacy: [E|Shock]Oh.[P] Seriously?")
	fnDialogue("Player: You guys didn't protect me or save me--but you were the first two people who cared about how I felt.")
	fnDialogue("Player: All I've ever wanted to be was as good of a friend as you were to me that day. ")
	fnDialogue("Thought: I snatch her wrist and hold it tight.")
	fnDialogue([[Player: You don't have to be \"strong\" or \"helpful\" for anyone to like you.]])
	fnDialogue("Player: Just having you around, and getting to be your friend... that's more than plenty for me. ")
	fnDialogue("Stacy: .[P].[P].[P][E|N]Heh.")
	fnDialogue("Player: What?")
	fnDialogue("Stacy: [E|Sad]*sniff* It's just... didn't you just wake up from a coma a few months ago?[P][E|N] When did you get so dang sage-y and stuff?")
	fnDialogue("Thought: I'm pulled in for a hug; not Stacy's usual intimate lean-to.[P] Even so, my face is getting hot.")
	fnDialogue("Stacy: [E|Blush]Thanks, [PLAYERNAME]. I mean it.[P] Take it from [ITALICS]moi[ENDITALICS], the authority on all things--you're a wicked good pal.")
	fnDialogue("Thought: I nod, my pulse racing.")
	fnDialogue("Thought: Maybe Stacy and I have a ways to go before we're completely open with each other, but today might've closed the gap between us the tiniest bit.")
	fnDialogue("Stacy: [E|N]Whew![P] [ITALICS]Bien[ENDITALICS], in the interest of trying to be more honest, that's enough touchy-feely stuff for me.")
	fnDialogue("Stacy: [E|Blush]..But I want to keep walking you home, if that's okay?")
	fnDialogue("Player: Heh.[P] Heck yeah!!")
	fnDialogue("[HIDEALL][BG|Null][IMAGE|Null]Thought: ...")
    
	--Save the game.
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Starting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
	PairanormalLevel_FlagCheckpoint("Starting")
    
    --Resume.
    LM_ExecuteScript(gsRoot .. "../Chapter 4/Gameplay_SceneD/0 - Walk Home.lua", "LimoScene")

end
