-- |[ ========================== Walking Home With Michelle, Scene 0 =========================== ]|
--Walking home with Michelle for the first time. This can be called from one of two different places.
-- The chapter variable marks which location to move the script back to when this ends.

-- |[Arg Resolve]|
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

-- |[ ======================================= Execution ======================================== ]|
--Begin.
if(sTopicName == "Start") then

    --Add 5 to friendship score.
    local iMichelleFriend = VM_GetVar("Root/Variables/System/Romance/iMichelleFriend", "N")
    VM_SetVar("Root/Variables/System/Romance/iMichelleFriend", "N", iMichelleFriend + 50.0)

    --Flag.
    VM_SetVar("Root/Variables/System/Player/iWalkedWithMichelleA", "N", 1.0)
	
	--Dialogue.
	fnDialogue("[HIDEALL][MUSIC|Love]Player: Michelle!")
	fnDialogue("[CHARENTER|Michelle]Michelle: [E|Shock]Ugh, you again?[P] What do you want, weirdo?")
	fnDialogue("Player: Come on, don't pretend you aren't happy to see me.[P][CRASH] HA!")
	fnDialogue("Thought: I clap my hands and strike a pose like one of the characters in the posters of the music room.")
	fnDialogue("Thought: My heart skips a beat as Michelle bursts into laughter.")
	fnDialogue("Michelle: [E|Happy]Let's go, you absolute specimen.")
	fnDialogue("[HIDEALL][BG|Null]Thought: ...")
	fnDialogue("Thought: It feels so good to walk beside her. I can't believe I was ever intimidated any time she got close.")
	fnDialogue("[BG|Root/Images/Backgrounds/All/WalkHome][CHARENTER|Michelle]Michelle: [E|Blush][P][P]Hey, here.")
	fnDialogue("Player: What?")
	fnDialogue("Michelle: [E|Mad]I'm GIVING you something.")
	fnDialogue("Player: Wh... what is this?[P] p... pop r--")
	fnDialogue("Michelle: [E|Mad]ITS CANDY!!![P] Geez, just put it in your mouth.  ")
	fnDialogue("Thought: Sighing, I tear open the package, and the gentle scent of sour apple hits my nostrils.")
	fnDialogue("Thought: I can't hide my delight; Michelle is actually a big softie.[P] I pour the tiny candy crystals in my mouth.")
	fnDialogue("Thought: ...")
	fnDialogue("Player: !!!! [CRASH]AAAAAAAAAAAAAAHHHHHHHHH!!!!!")
	fnDialogue("Michelle: [E|Shock]What? What's wrong???")
	fnDialogue("Player: THE CANDY *COUGH*[P] *SPUTTER*[P] IS [ITALICS]DISSOLVING THE INSIDE OF MY MOUTH!!!![ENDITALICS]")
	fnDialogue("Michelle: [E|Shock]No it's its not! These are pop rocks, silly.[P][E|Happy] They're just fizzy.[P] I-It's fun!")
	fnDialogue("Player: I'M D Y I N G.")
	fnDialogue("Michelle: [E|Blush]Okay, look, just relax, take slow breaths.[P] One, two...[P]one...[P]er, two.[P] There you go.")
	fnDialogue("Thought: Michelle leads me to a park bench and sits beside me.")
	fnDialogue("Michelle: [E|N] Now hold your mouth open and listen.")
	fnDialogue("Thought: When I do so, I can hear hundreds of pops that spark and bounce with sour apple flavor.[P] [ITALICS]This is normal? People eat this!?[ENDITALICS]")
	fnDialogue("Thought: My cheeks tingle with warmth when I realize I might have panicked a little prematurely.")
	fnDialogue("Player: Oh.")
	fnDialogue("Michelle: [E|Blush]*ahem*... All right, maybe I should'a warned you about 'em before making you try some.")
	fnDialogue("Michelle: [E|Sad] I thought you'd be chuffed! S-sorry.")
	fnDialogue("Player: Mmf.")
	fnDialogue("Thought: I take another dribble of candy rocks, savoring the sensation again.")
	fnDialogue("Player: It is kind of fun.")
	fnDialogue("Michelle: [E|N]Ah, there, see?[E|Happy] I love these things.")
	fnDialogue("Thought: She takes out three from her bookbag and tears them open simultaneously, pouring the whole lot onto her tongue.")
	fnDialogue("Michelle: [E|N]If you ever want more, just let me know.")
	fnDialogue("Michelle: [E|Happy] I swipe 'em all the time from the convenience store down the street--never have to pay a dime for it!")
	fnDialogue("Player: Huh. I thought people had to pay for things from stores.[P] So you can actually just... take it?")
	fnDialogue("Michelle: [E|Happy]Heh...[P]Only if you don't get caught!")
	fnDialogue("Thought: The pop rocks in my mouth slowly fizzle out, and I swallow the flat sugar syrup. ")
	fnDialogue("Player: If you do get caught, w-wouldn't you be in trouble?")
	fnDialogue("Thought: Michelle scoffs.")
	fnDialogue("Michelle: [E|Happy]What are you, a cartoon special?[P] Like I care.")
	fnDialogue("Michelle: [E|Sad] Worst thing they've ever done to me is move--my parents, I mean.")
	fnDialogue("Michelle: [E|N] If they do it again, I'll never see that convenience store owner again.[P] So what's the big deal?")
	fnDialogue("Thought: I toy with the empty candy package in my hands nervously--what should I say?")
    
	--Save the game.
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"RockPops\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
	PairanormalLevel_FlagCheckpoint("RockPops")
    
	--What to say to Michelle:
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (What should I say?)") ]])

	local sDecisionScript = "\"" .. gsRoot .. "../Chapter Walk/Walkhome Michelle 0.lua\""
	fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"What about your friends?\", " .. sDecisionScript .. ", \"Friends\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"You shouldn't run away from your problems\", " .. sDecisionScript .. ", \"NoRun\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Cool\",  " .. sDecisionScript .. ", \"Cool\") ")
	fnCutsceneBlocker()

--Friends are overrated.
elseif(sTopicName == "Friends") then
	
	--Dialogue.
	fnDialogue("Player: Well, you might not see the convenience store owner again, but what about all your friends?")
	fnDialogue("Michelle: [E|Happy]Pfft.[P] What friends are those? ")
	fnDialogue("Thought: Michelle can only shrug, but she might as well have punched a hole in my stomach and thrown pop rocks in.")
	fnDialogue("Player: [P][P]You can't be serious..[GLITCH].the Paranormal Club![P] Don't you care if you'd never get to see us again?[P] Do you really.[P].[P].[P]think so little--")
	fnDialogue("Michelle: [E|Mad]Oh, get off your high horse![P][GLITCH] You and 'em would forget about me just fine, don't you worry.")
	fnDialogue([[Michelle: I've got a bloody cell phone, you know. Do you hear it ringing 'till it bursts with all the \"friends\" I've made moving from town to town!?!]])
	fnDialogue("Michelle: Spoiler alert: [P]NO. ")
	fnDialogue("Thought: She slings her bookbag over her shoulder.[P] It looks [GLITCH][GLITCHTEXT]FORGETABOUTMEFORGETAB[ENDGLITCHTEXT]like our time together here is coming to a close.")
	fnDialogue("Michelle: [E|Mad]Nobody's ever missed me when I've flitted out of their life, and guess what? [P][E|Sad]I never missed them either. ")

    --Rejoin.
    LM_ExecuteScript(LM_GetCallStack(0), "Rejoin")
    
--Stand still.
elseif(sTopicName == "NoRun") then
	
	--Dialogue.
	fnDialogue("Player: I'm serious.[P] People go to jail and stuff for stealing--that's what Eilette says, anyway. ")
	fnDialogue("Thought: [ITALICS]Very pro-incarceration, that woman.[ENDITALICS]")
	fnDialogue("Player: One of these days you won't be able to move away.[P] What then?")
	fnDialogue("Michelle: [E|Mad]Oh, LEAVE IT.[P] What's gotten you so high and mighty all of a sudden?")
	fnDialogue("Michelle: I'm good at what I do, all right?[P] I don't need some teen baby weirdo telling me how to live my life.")
	fnDialogue("Player: I'm just worried, is all.[P] I care[GLITCH][GLITCHTEXT]FORGETABOUTMEFORGETAB[ENDGLITCHTEXT] about what happens to my friends.")
	fnDialogue([[Thought: Michelle opens her mouth, but prompty snaps it shut.[P] I can hear the stern \"click\" of her jaw as teeth collide, and she stands there, stunned.]])
	fnDialogue("Michelle:[E|Blush] W-Whatever, okay? I'm going home.[P][E|Sad] I won't give you pop rocks anymore if you've got such a big problem with them.")
	fnDialogue("Thought: She slings her bookbag over her shoulder.[P] It looks like our time together here is coming to a close.")

    --Rejoin.
    LM_ExecuteScript(LM_GetCallStack(0), "Rejoin")

--COOL!
elseif(sTopicName == "Cool") then
	
	--Dialogue.
	fnDialogue("Player: Oh.[P] Okay, cool.")
	fnDialogue("Michelle: [E|Shock]... what?")
	fnDialogue("Player: I said cool. ")
	fnDialogue("Michelle: [E|Shock]Y-You mean, you don't have a problem with me stealing stuff?")
	fnDialogue("Player: You said its fine, so why would I?")
	fnDialogue("Michelle: [E|Shock]...")
	fnDialogue("Player: Hey, maybe sometime you could show me how to do it! ")
	fnDialogue("Michelle: [E|Mad]NO.[GLITCH] Absolutely not.[P] Y-you'd get caught![P][E|Blush] You'd get in trouble.")
	fnDialogue("Player: Ah, you're probably right. [GLITCH][GLITCHTEXT]FORGETABOUTMEFORGETAB[ENDGLITCHTEXT]I'm not very good at it.")
	fnDialogue("Michelle: [E|Happy]YES,[P][E|Shock] I mean, no--[P][E|Blush]WHY ARE YOU AGREEING WITH EVERYTHING I SAY?!?!")
	fnDialogue([[Player: Can I ask you to \"swipe\" things [ITALICS]for[ENDITALICS] me? Like make a small[GLITCH] wishlist?]])
	fnDialogue([[Player: Mm, although there really isn't anything I [ITALICS]need[ENDITALICS]... Hm, I guess it doesn't matter, scratch that idea.]])
	fnDialogue([[Player: But I should get [GLITCH][GLITCH]some practice in anyway, so I can do it myself, right?]])
	fnDialogue([[Player: I know![P] you can take me \"swiping\" with you.[P] It'll be fun! You'll be my[GLITCH] \"swipe-master\". ]])
	fnDialogue("Michelle: [E|Mad]Oh my god. You are literally making me feel [ITALICS]so[ENDITALICS] uncool right now??[P][E|Sad] And very confused, and questioning my life choices!?!")
	fnDialogue("Thought: She slings her bookbag over her shoulder.[P] It looks like our time together here is coming to a close.")
	fnDialogue("Michelle: [E|Mad]I'm..[GLITCH].[P]I'm outta here.")

    --Rejoin.
    LM_ExecuteScript(LM_GetCallStack(0), "Rejoin")

--Rejoin!
elseif(sTopicName == "Rejoin") then
	
	--Dialogue.
	fnDialogue("[HIDEALL]Player: Wha---Michelle, wait!")
	fnDialogue("Thought: She turns around, almost in[GLITCH] slow motion.")
	fnDialogue("Thought: [GLITCHTEXT]DONT MESS THIS UP I[GLITCH] MESS[GLITCH]E[GLITCH]D IT UP AGAIN[ENDGLITCHTEXT]I reach for her, bu[GLITCH]t my body feels terribly heavy, like I'm trapped on the bench.")
	fnDialogue("Thought: [GLITCHTEXT] I DONT[GLITCH] UNDERSTAND WHAT I DID WRONG.[ENDGLITCHTEXT][ITALICS]Don't go... Don't go![P] I... don'[GLITCH][GLITCH]t understand what I did wrong...[ENDITALICS][Music|Null]")
	fnDialogue("[BG|Null]Thought:... ")
	fnDialogue("[BG|Null]Thought: [ITALICS]FUCK FUCK FUCKKKKK. A bloody tattler, I know it is.[ENDITALICS] ")
	fnDialogue("[BG|Root/Images/Backgrounds/All/Bathroom]Thought: I look around--I'm boxed in by the four walls of the bathroom stall I'm hiding in, but I know whoever's out there saw me.")
	fnDialogue([[Thought: [ITALICS]\"Yew kant dew that heeere\"[ENDITALICS], that's what she'll say when she sees my grafitti on the bathroom mirrors.[P] Like the snooty little witch she is.]])
	fnDialogue("Thought: [MUSIC|Somber] I got nervous, is all.[P] Art relaxes me! It's not easy being the new girl, even though I've done it so many times.")
	fnDialogue("Thought: And when I rightfully clobber her for being a nosy sod, she'll probably run screaming and crying to the principal.")
	fnDialogue("Thought: And I'll have to move again.[P] And my parents will be [ITALICS]pissed[ENDITALICS].")
	fnDialogue("???: [VOICE|Voice|Mari]Scuuuuuuse me?")
	fnDialogue("Player: [VOICE|Voice|Michelle][CRASH]WHAT?")
	fnDialogue("Thought: [ITALICS]Please don't make me move again. I don't want to move again.[ENDITALICS][P] A strand of shimmering black-blue hair peeks around the corner.")
	fnDialogue("[CHARENTER|Mari]Mari: [E|Shock]Hey, sorry to bother you, do you have a tampon?[P] I'm all out.")
	fnDialogue("Player: [VOICE|Voice|Michelle]I-- Oh.[P] Um...[P]here?")
	fnDialogue("Mari: [E|Happy][CRASH]THANK YOU!!![P] You're a freakin' lifesaver, seriously.[P][E|N] I'm Mari, by the way.")
	fnDialogue("Thought: As she takes the tampon I hand her, another girl waves at me from the sinks.")
	fnDialogue("[CHARENTER|Stacy]Mari: [E|N]That's Stacy.")
	fnDialogue("Stacy: [E|Shock][ITALICS]MIERDRE[ENDITALICS]![P] Did you draw this, on the mirrors?")
	fnDialogue("Player: [VOICE|Voice|Michelle]No.")
	fnDialogue("Stacy: [VOICE|Voice|Stacy][E|Mad]It's so cool! I really like the way you draw hair.[P][E|Shock] Oh--Mari hurry up.[P] We gotta get to class before the bell rings.")
	fnDialogue("Mari: [E|Mad]Right!")
	fnDialogue("[CHAREXIT|Mari]Thought: The shorter girl whips into one of the stalls, slamming the door. ")
	fnDialogue("Mari: [VOICE|Voice|Mari][CRASH]GOTTA PEE FAST!")
	fnDialogue("Stacy: [E|Mad]YOU GOT IT, MARI![P] PEE LIKE A CHAMP!")
	fnDialogue("Thought: She does, and it's loud. I stand in my stall, frozen like a wild animal in headlights.")
	fnDialogue("Thought: [ITALICS]Am I dreaming? Is this a weird stale-pizza induced nightmare about school I'm having?[ENDITALICS]")
	fnDialogue([[Stacy: [E|N]--So anyways. I says to her,[P] \"look sis, you're wicked gorgeous, obviously, but like,[P] you can't just BITE people when you're kissing them.\"]])
	fnDialogue([[Stacy: [E|Shock]\"Who told you BITING was an okay thing to do?\"]])
	fnDialogue("Mari: [VOICE|Voice|Mari]What is she, Edward freaking Cullen?")
	fnDialogue("Stacy: [VOICE|Voice|Stacy][E|Happy]I know, seriously![P][E|N] Hey, have you ever read [ITALICS]Twilight[ENDITALICS]?")
	fnDialogue("Thought: I'm surprised that the redheaded girl is directing the question at me.[P] My shoulders instinctively pull back.")
	fnDialogue("Player: [VOICE|Voice|Michelle]Uh... No.[P] N-no way.[P] Vampires are supposed to be terrifying nightcrawlers, not horny sparkling teenagers. ")
	fnDialogue("Stacy: [E|Blush]If you haven't read it yet, how do you know it's full of horny sparkling teenagers?")
	fnDialogue("Thought: She crosses her arms and smirks. I thought the redhead was actually kind of hot at first, but she's starting to get on my nerves.")
	fnDialogue("Thought: By this time, Mari has finished up in her stall and washes up.")
	fnDialogue("[CHARENTER|Mari]Player: [VOICE|Voice|Michelle]I'm not INCOMPETENT, you know!?[P] Besides, I watch all kinds of gory vampire movies that are even HORNIER.[P] Movies you probably haven't even heard of--")
	fnDialogue("Info: [SFX|World|Schoolbell]*RINGRINGRINGRINGRING*")
	fnDialogue("Stacy: [E|Shock]Aw, shoot, the bell! We gotta go. Mari!!")
	fnDialogue("Mari: [E|Shock]Wait wait wait! ")
	fnDialogue("Thought: She rifles furiously through her backpack.[P] Looking for another tampon?")
	fnDialogue("Mari: [E|Happy]Do you know how rare it is for us to find a paranormal enthusiast?!?[P][E|Blush][CRASH] AH, HERE!!!")
	fnDialogue("Thought: A flimsy paper, bleeding through with water from the bubbly girl's still wet hands, is shoved into my chest.")
	fnDialogue("Thought: Before I can push her away, she and her friend are gone.[HIDEALL][P][P][P] I look at the flyer.")
	fnDialogue([[Player: [VOICE|Voice|Michelle]\"Join the Paranormal Club... every Wednesday afterschool in room 305.\"[P][P] tch.]])
	fnDialogue("Thought: I thumb the paper, perusing the unusual artwork.[P] After about a minute, I shove it into my bag and head to class.")
	fnDialogue("Thought: There's a teeny, tiny voice in my head--I've kept it good and quiet all these years but now, its annoying squeak is just loud and hopeful enough I can't ignore it.")
	fnDialogue("Thought: [ITALICS]This time, I'll do it right.[P] Don't mess it up, Michelle.[P] Don't mess it up.[ENDITALICS]")
	fnDialogue("[BG|Null]Thought: ...")
	fnDialogue("[BG|Root/Images/Backgrounds/All/WalkHome]Player: Uuuurrrgh.")
	fnDialogue("Thought: Back to this reality; Michelle is nowhere to be seen.")
	fnDialogue("Thought: I heave myself up to a sitting position, which feels harder to do than getting up in the morning.[P] A pigeon which was sitting on my knee flutters away, startled.")
	fnDialogue("Player: *sigh*...")
	fnDialogue("Thought: I reached into my companion's memories, hoping to debunk the hurtful words she'd told me, somehow.")
	fnDialogue("Thought: But what I saw...[P] what I felt looking through her eyes made me feel worse.")
	fnDialogue("Player: Michelle... one way or another, you'll see.[P] We care about you as much as you care about us.")
	fnDialogue("[HIDEALL][BG|Null]Thought: ...")
    
	--Save the game.
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"DamnIt\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
	PairanormalLevel_FlagCheckpoint("DamnIt")
    
    --Which chapter to jump to.
    local iChapterDestination = VM_GetVar("Root/Variables/System/Player/iChapterDestination", "N")
    if(iChapterDestination == 3.0) then
        LM_ExecuteScript(gsRoot .. "../Chapter 3/Gameplay_SceneD/2 - Arrive At Home.lua", "Start")
        
    elseif(iChapterDestination == 4.0) then
        LM_ExecuteScript(gsRoot .. "../Chapter 4/Gameplay_SceneD/0 - Walk Home.lua", "LimoScene")
    end

end
