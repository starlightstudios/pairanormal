-- |[ ============================= Walking Home With Mari, Scene 1 ============================ ]|
--Walking home with Mari for the second time.

-- |[Arg Resolve]|
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

-- |[ ======================================= Execution ======================================== ]|
--Begin.
if(sTopicName == "Start") then

    --Add 5 to friendship score.
    local iMariFriend = VM_GetVar("Root/Variables/System/Romance/iMariFriend", "N")
    VM_SetVar("Root/Variables/System/Romance/iMariFriend", "N", iMariFriend + 50.0)
	
	--Dialogue.
	fnDialogue("[HIDEALL][MUSIC|Love]Player: Mari, what do you say we head home together?")
	fnDialogue("[CHARENTER|Mari][CHARENTER|Stacy]Mari: [E|Happy]YOU BET! See you later, Stacy!")
	fnDialogue("Stacy: [E|Blush][ITALICS]Arrivederci![ENDITALICS]")
	fnDialogue("Thought: We wave goodbye to the other club members and head out.")
	fnDialogue("[HIDEALL][BG|Null]Thought: ...")
	fnDialogue("[BG|Root/Images/Backgrounds/All/WalkHome]Player: So.[P] Another unconventional Paranormal Club meet down in the books.")
	fnDialogue("[CHARENTER|Mari]Mari: [E|Happy]You sure said it, heehee.[P][E|N] Hey, have you been having fun lately? [P]Not just in the club, but in life and stuff.")
	fnDialogue("Player: Hm...[P] Well, even though I don't really know about... the experiences, that life can offer, or whatever...")
	fnDialogue("Player: I'd say these past few weeks are above and beyond expectations.[P] Except for homework.[P] I'm still not sure about that.")
	fnDialogue("Mari: [E|N]You're not alone there, my friend.")
	fnDialogue("Thought: Mari pats my shoulder, beaming.[P] Something inside me is tickled--I do like being the reason Mari is smiling.")
	fnDialogue("Player: What about you? How do you feel?")
	fnDialogue("Mari: [E|Shock][CRASH]ME?[P][E|N] Gosh, that's nice of you to ask.[P][E|Blush]*gulp* W-well.[P] In general ... I-I'm really happy lately, too. ")
	fnDialogue("Thought: Her fingers find themselves nervously twisting in the long black hair that cascades down each soft shoulder.")
	fnDialogue("Thought: [ITALICS]I wonder what it feels like to touch it--wait, stop thinking that![P] Focus on what she's saying.[ENDITALICS]")
	fnDialogue("Mari: [E|Blush]*ahem*[P] [PLAYERNAME], ever since you've been around...[P]I dunno.")
	fnDialogue("Mari: I kind of seem to get more excited for club meetings.[P] S-Seeing you there and making you happy...[P]I-I really enjoy it. ")
	fnDialogue("Player: Oh, cool.")
	fnDialogue("Mari: [E|Mad][CRASH]DO YOU KNOW WHAT I MEAN!?![P][E|Sad] I mean, n-nevermind. Sorry.[P] I'm just being silly.")
	fnDialogue("Thought: I place my hand on her shoulder, accidentally-but-not-so-accidentally brushing against one of the long feathery wisps of her hair.")
	fnDialogue("Thought: [ITALICS]It's so smooth--I mean, focus! Focus![ENDITALICS]")
	fnDialogue("Player: Not at all, Mari. I totally know what you mean![P] And I agree.")
	fnDialogue("Mari: [E|Shock]E-e-ehhh??? Y-you do!")
	fnDialogue("Player: You and I are totally even.[P] It's a good arrangement.")
	fnDialogue("Mari: ...[P][P][E|N][SFX|World|Ugh]Okay, you've lost me.")
	fnDialogue("Player: I mean, letting me hang out with the club while the club investigates me.[P] It's a good--no, a [ITALICS]great[ENDITALICS] arrangement!")
	fnDialogue("Player: I get to spend time enjoying everyone's company, and the club can learn things about me and investigate my background.")
	fnDialogue("Player: I'm letting you know that I'm okay with that exchange.")
	fnDialogue("Thought: [MUSIC|Null]All at once, the joy leave's the club captain's eyes, replaced with a look of horror.[P] She recoils from my hand.")
	fnDialogue("Mari: [E|Sad]You...")
	fnDialogue("Thought: Her breath starts to rapidly increase.[P] Tears bubble up at the edge of her eyes.")
	fnDialogue("Player: [SFX|World|Blink]Mari, why are you crying!?!?")
	fnDialogue("Mari: [E|Shock][CRASH]AEEUEGHghgaaahhhaahh...")
	fnDialogue("Player: Please, don't be sad! I told you, I don't mind you looking into my background.")
	fnDialogue("Player: If you want to investigate me, that's okay!!![P] I'm just happy as long as we're friends.")
	fnDialogue("Mari: [E|Shock]W-[P][CRASH]WAHHHH!!!!")
	fnDialogue("Player: Wh-what? What's wrong!?!")
	fnDialogue("Mari: [E|Mad]WHAT'S WRONG!?!?[P][SPEEDTEXT][E|Sad][CRASH] I NEVER MEANT TO DO THAT![P] [CRASH]THAT'S SO HORRIBLE OF ME! [P][CRASH]IM A BAD FRIEEEEND.[ENDSPEEDTEXT] ")
	fnDialogue("Thought: Mari starts backing away from me,[P] and it feels like she's pulling my organs out with each step.")
	fnDialogue("Mari: [E|Sad][PLAYERNAME]... please. [P]What I've done to you... Do yourself a favor and never talk to me again!")
	fnDialogue("Mari: I'll ruin your life!!! I don't deserve--")
	fnDialogue("Player: Mari, wait! ")
	fnDialogue("Thought: I grab her arm desperately, and she tugs hard--but I can't let go.")
	fnDialogue("Mari: [E|Sad]*sniff* We were investigating your background to [ITALICS]help you[ENDITALICS]...[P] you have to believe...")
	fnDialogue("Thought: Like I'm climbing a long, tall rope,[P] hand over hand, I try to draw Mari closer to me.")
	fnDialogue("Mari: ... I-I just...[P] Oh god, what was I thinking--")
	fnDialogue("Player: --Mari, please. I'm not mad, exactly.[P][P] I just want to know what's going on here. W-why...[P]why are we hanging out together, really?")
	fnDialogue("Thought: Mari, whose watery gaze was dancing wildly amongst the asphalt beneath us, snaps to attention.")
	fnDialogue("Thought: I've never seen her look so somber. I'm suddenly terrified.")
	fnDialogue("Mari: [E|Sad]We're hanging out because...[P]because I just wanted you to be with someone who understands how you feel!")
	fnDialogue("Player: ...")
	fnDialogue("Mari: I mean, felt.[P] The hospital. When we first met?[P] You just had this expression on your face... it was like I was looking into a mirror.")
	fnDialogue("Player: But I was miserable in the hospital. You had a smile on your face the whole time I saw you.")
	fnDialogue("Mari: [E|Shock]Of course I did. Stacy was so worried about me! I didn't want her to think I was in pain.")
	fnDialogue("Player:[P][P]But, you [ITALICS]were[ENDITALICS] in pain?")
	fnDialogue("Mari: Well. [P][P][E|Neutral]I mean YEAH-- I was in a friggin' explosion!")
	fnDialogue("Thought: She chuckles darkly, but I can only stare.")
	fnDialogue("Mari: Heh. I don't know if you noticed [PLAYERNAME], but living in this town kinda sucks, okay?")
	fnDialogue("Mari: [E|Sad] It's not exactly easy to fit in-- everyone wants you to speak or act a certain way--it is so exhausting.")
	fnDialogue("Mari: [E|Mad]The whole reason I made the Paranormal Club was so there could be a place where nobody would have to feel out of place!")
	fnDialogue("Mari: [E|Sad]I asked you to participate, and made the other members investigate you so we could all do something together.")
	fnDialogue("Mari: [E|Sad] But I never asked you if that was what you wanted. I'm sorry.")
	fnDialogue("Thought: Relief, and a little bit of sadness, washes over me at this confession.[P] It's time to do something Mari has always done for me--comfort a friend in need.")
	fnDialogue("Thought: She bows her head, and somehow it dips naturally in the crook of my neck.[P] I hold her tightly.")
	fnDialogue("Player: I.. I am really sorry you've had a tough time here. I am glad that someone like you understands what I've been going through.")
	fnDialogue("Player: And I'm glad I got to talk about it with you. [P]I guess I was really scared that[P][P][P] I was being used.")
	fnDialogue("Mari: [E|Shock][CRASH]No! No, of course not![P] It doesn't matter what you do, [PLAYERNAME]. You're always going to be our friend.")
	fnDialogue("Mari: [E|Sad] If you ever feel uncomfortable, just let me know! Please!![P] A good friend would never try to control someone else.")
	fnDialogue("Player: Well, the same thing goes to you, Mari.[P] Please let me know if I ever do something to hurt [ITALICS]you.[ENDITALICS]")
	fnDialogue("Mari: [E|Shock]BUT YOU DONT KNOW AS MUCH ABOUT SOCIAL STUFF AS I DO![P] If you mess up, that's okay.")
	fnDialogue("Player: But that doesn't invalidate your feelings and all the things you've been through--")
	fnDialogue("Mari: [E|Shock]But if you--")
	fnDialogue("Thought: Mari stops, then giggles.[P] The hug breaks so she can wipe away some stray tears.")
	fnDialogue("Mari: [E|Blush]Ahh, I think we're just trying to out-nice each other at this point.")
	fnDialogue("Player: Heh, fair enough.[P] Let's just... talk openly from now on and not worry about trying to be or do the right thing.")
	fnDialogue("Mari: [E|Neutral]*sigh*![P] Deal.")
	fnDialogue("Thought: We take a mutual pause; enjoying how the cool air splashes against our warm clothes.[P] Drawing lines along the palms of each other's hands and feeling the roughness of skin. ")
	fnDialogue("Mari: [E|Blush]You know... this is [ITALICS]just[ENDITALICS] like my favorite doujinshi-- ")
	fnDialogue("Mari: Chapter 4 of ''Naruto and Sasuke Both Want to Date Me--but I'm in love with Sakura??'' ")
	fnDialogue("Player: Mari, you must know... even if I end up learning to read...[P]I will never consume any the literature you put in front of me.")
	fnDialogue("Mari: [E|Shock][CRASH]BUT ITS SO GOOD!!![P][CRASH] I SWEAR!!!")
	fnDialogue("Player: Heh.")
	fnDialogue("Thought: When I think of all the misconceptions I had about my new friends just a few moments ago, I could kick myself for being so paranoid.")
	fnDialogue("Thought: Then again, if I hadn't talked about it with Mari, we might never have truly understood each other.")
	fnDialogue("Thought: And Mari... being with her is so [ITALICS]nice[ENDITALICS]. [P]I am suddenly very aware of how fast my heart is beating.")
	fnDialogue("Player: *ahem*... Well.")
	fnDialogue("Mari: [E|N]--ANYWAY. I gotta start heading home.[P][E|Blush] See you soon?")
	fnDialogue("Player: Yes. Absolutely. ")
	fnDialogue("Mari: [E|N]Great.[P][P][P][E|Happy][CRASH] YOU'RE MY FRIEND AND I SUPPORT YOU!!!")
	fnDialogue("Player: Right back at you.")
	fnDialogue("Mari: [E|N]Bye!!!")
	fnDialogue("[HIDEALL]Player: Bye, Mari.")
	fnDialogue("[BG|Null]Thought: ...")
    
	--Save the game.
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"LimoScene\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
	PairanormalLevel_FlagCheckpoint("LimoScene")
    
    --Next script.
    LM_ExecuteScript(gsRoot .. "../Chapter 4/Gameplay_SceneD/0 - Walk Home.lua", "LimoScene")
    

end
