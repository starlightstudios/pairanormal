-- |[ ============================ Walking Home With Todd, Scene 1 ============================= ]|
--Walking home with Todd for the second time. Always dumps the player to chapter 4.

-- |[Arg Resolve]|
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

-- |[ ======================================= Execution ======================================== ]|
--Begin.
if(sTopicName == "Start") then

    --Add 5 to friendship score.
    local iToddFriend = VM_GetVar("Root/Variables/System/Romance/iToddFriend", "N")
    VM_SetVar("Root/Variables/System/Romance/iToddFriend", "N", iToddFriend + 50.0)
    
    --Flag.
    VM_SetVar("Root/Variables/System/Player/iWalkedWithToddB", "N", 1.0)

	--Dialogue.
	fnDialogue("[HIDEALL][MUSIC|Love]Thought: I look around for Todd, but he seems to have left home already.[P] Or maybe he's getting something ready elsewhere.")
	fnDialogue("Thought: I wait a few minutes, but as folks begin to trickle out of the gym, I feel growing embarrassment waiting up for him.")
	fnDialogue("[CHARENTER|Mari][CHARENTER|Stacy]Mari: [E|Blush] Heeeeeeey, [PLAYERNAME]. ")
	fnDialogue("Stacy: [E|Happy] Waiting around for that wicked hot tutor of yours? ")
	fnDialogue("Mari: [E|Blush] *giggle*")
	fnDialogue("Player: [SFX|Blink] Wh-what, no![P] I--no, I was just heading home.[P] On my own.")
	fnDialogue("Mari: [E|Sad] Oh.[P] Okay.")
	fnDialogue("Thought: I grab my backpack and hurry out.")
	fnDialogue("[HIDEALL][BG|Null]Thought: ...")
	fnDialogue("[BG|Root/Images/Backgrounds/All/SchoolExterior]Thought: [P][P]I don't know why I keep making a dork out of myself waiting up for Todd.[P] Haven't I been told enough times I shouldn't try getting so close to him?")
	fnDialogue("[CHARENTER|Todd]Todd: [E|Shock] [PLAYERNAME]! Wait up!")
	fnDialogue("Player: [SFX|World|Blink] Ah! Hey.[P] *ahem* I mean, hey Todd.[P] I was just casually walking home.[P] I didn't wait up for you.")
	fnDialogue("Todd: [E|N] Pffff.[P] Well, do you want some company?")
	fnDialogue("Player: [CRASH]YES! [P][P]... If that's cool with you.")
	fnDialogue("Todd: [E|N] Mmm. ")
	fnDialogue("Thought: He strolls to my side, shifting his bookbag higher up on his shoulder.")
	fnDialogue("Todd: [E|Blush]I wanted to thank you for coming by to help. [P]And bringing the club with you.")
	fnDialogue("Todd: [E|Mad]Like, how did you even know nobody was going to come and we needed the extra hands?")
	fnDialogue("Player: Oh, you know.[P] I'm a mind reader.")
	fnDialogue("Todd: [E|Happy] Pf'Hahaha! Well thank goodness for that.")
	fnDialogue("Player: Heh, yeah. I just happened to overhear it, actually.")
	fnDialogue("Todd: [E|N] Ahh, gotcha.[P] Well, yeah. Thanks for coming.[P] I hope you don't feel--")
	fnDialogue("Thought: He trails off, an uncertain look in his eyes.")
	fnDialogue("Player: What?")
	fnDialogue("Todd: [E|Sad] Ah, I was just gonna say, like,[P] I feel like you've been trying to hang out with me,[P] and I've just been...[P]like, not really attentive of that?")
	fnDialogue("Todd: I'm really sorry, like, ASB just takes up all my time, and--")
	fnDialogue("Player: Todd, you don't have to spend all your time on me.[P] It's okay to do your own thing. ")
	fnDialogue("Todd: [E|Sad]...")
	fnDialogue("Player: I mean, at first, I felt really attached to you,[P] because you're one of the few people who were nice to me,[P] and I needed that. ")
	fnDialogue("Thought: I shut my eyes, pressing my lids together as hard as I can.[P] Maybe if I can't see his face, this'll be easier to say.")
	fnDialogue("Player: But now I'm starting to learn that being a good friend is about giving as much as receiving.[P] And.[P] And, you know.")
	fnDialogue("Player: I don't want to [ITALICS]depend[ENDITALICS] on you.[P] I want to be... your friend.[P] Like an equal and stuff.")
	fnDialogue("Todd: [P][P][P][E|Shock]Whoah. [PLAYERNAME]. ")
	fnDialogue("Thought: I risk a quick peek.[P] Todd is frowning, but he still laughs.")
	fnDialogue("Todd: [E|Blush]There's something about you, [PLAYERNAME], that's so damn...[P][E|Mad]perceptive about how other people feel.[P] It's honestly a little creepy.")
	fnDialogue("Player: I'm sorry--!")
	fnDialogue("Todd: [E|N] Every time we talk, you say stuff that just gets me right here.")
	fnDialogue("Thought: He presses his fingertips against the little crevice at the center of his chest,[P] the fabric of his shirt straining as he does.")
	fnDialogue("Todd: [E|Blush] You... get me.[P] And it's a big relief.")
	fnDialogue("Thought: Our eyes meet.[P] I can only really stand it for a few seconds before I have to look at the trees dotting the sidewalk.")
	fnDialogue("Player: Eheh.[P] You say it kind of like that's a bad thing.")
	fnDialogue("Todd: [E|Mad] No.[P] I mean, it shouldn't be, right?[P][E|Sad] I just...[P] haven't been... [P]*sigh*")
	fnDialogue("Todd: A long time ago, there was this girl...[P]and we dated for a little while.")
	fnDialogue("Player: [SFX|World|Blink]Oh... r-really.")
	
    --Branch.
    local iReadToddsMind = VM_GetVar("Root/Variables/System/Player/iReadToddsMind", "N")
    if(iReadToddsMind == 0.0) then
        fnDialogue("Thought: [ITALICS]Oh god someone is being vulnerable with me.[P] ME![ENDITALICS]")
        fnDialogue("Thought: I stare straight ahead, my face smooth as a stone.[P] [ITALICS]Whatever you do, don't smile, don't look excited.[P] You'll scare him off![ENDITALICS]")
    else
        fnDialogue("Thought: I stare straight ahead, my expression that of someone who definitely [ITALICS]doesn't[ENDITALICS] already know this because they read his mind that one time. ")
    end
    
    --Dialogue.
	fnDialogue("Todd: [E|N]It was all right at first, but I started to kind of ignore all my friends.")
	fnDialogue("Todd: [E|Sad] After it ended...[P]I just got nervous about falling into something like that again.[P][E|Mad] Ugh, like, a gross,[P] co-dependent kind of relationship. ")
	fnDialogue("Player: Right. Totally. ")
	fnDialogue("Todd: [E|Shock]Uh![P] I mean, that's not what I'm saying is happening here.[P] This isn't [ITALICS]that[ENDITALICS] kind of relationship at all.")
	fnDialogue("Player: [SFX|Ugh]Oh yeah.[P] Yeah, for sure. ")
	fnDialogue("[HIDEALL][IMAGE|Root/Images/MajorScenes/All/Todd]Todd: [VOICE|Voice|Todd]You[P] are[P][E|Blush] actually a kind and considerate person.[P] And I think you and I could become really good friends.")
	fnDialogue("Thought: My palms are so sweaty, they'll start dripping at any moment. [P]I wipe them casually against my knees.")
	fnDialogue("Player: Thanks, Todd.[P] That's nice to hear.")
	fnDialogue("Thought: Unable to take anymore, my feet make the executive decision to trudge forward, probably hoping to leave this topic as well as my current location.")
	fnDialogue("Thought: Why does talking about feelings and emotions and stuff feel so awkward nowadays? ")
	fnDialogue("Todd: [E|Blush] Anyway. [E|N]Oh! About that conversation we had earlier--[P]about dating and stuff...")
	fnDialogue("Player: I'm gonna stop you right there. ")
	fnDialogue("Todd: [E|Blush]--were you curious about dating because you kinda sorta had a thing for somebody, or--")
	fnDialogue("Player: NOPE.")
	fnDialogue("Todd: [E|Mad]Oh, come on! You have to tell me.[P][E|Blush] Friends are supposed to spill secrets and stuff, you know.")
	fnDialogue("Player: You [ITALICS]literally[ENDITALICS] told me a few days ago that people shouldn't share everything with each other.")
	fnDialogue("Todd: [E|Happy]Yeah, but now I'm saying screw it, because I'm dying of curiosity!!!")
	fnDialogue("Player: Then [ITALICS]perish[ENDITALICS].")
	fnDialogue("Todd: [E|Happy]Pf'hahahaha![P][P][E|Mad] --okay, who taught you that meme?")
	fnDialogue("Player: [SFX|World|Ugh]I'm never, ever, ever bringing dating up again.[P] With anyone.")
	fnDialogue("[HIDEALL][BG|Null][IMAGE|Null]Thought: ...")
    
	--Save the game.
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Starting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
	PairanormalLevel_FlagCheckpoint("Starting")
    
    --Resume.
    LM_ExecuteScript(gsRoot .. "../Chapter 4/Gameplay_SceneD/0 - Walk Home.lua", "LimoScene")
end
