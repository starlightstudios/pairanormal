-- |[ ============================ Walking Home With Laura, Scene 0 ============================ ]|
--Walking home with Laura for the first time. This can be called from one of two different places.
-- The chapter variable marks which location to move the script back to when this ends.

-- |[Arg Resolve]|
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

-- |[ ======================================= Execution ======================================== ]|
--Begin.
if(sTopicName == "Start") then

    --Add 5 to friendship score.
    local iLauraFriend = VM_GetVar("Root/Variables/System/Romance/iLauraFriend", "N")
    VM_SetVar("Root/Variables/System/Romance/iLauraFriend", "N", iLauraFriend + 50.0)

    --Flag.
    VM_SetVar("Root/Variables/System/Player/iWalkedWithLauraA", "N", 1.0)
	
	--Dialogue.
	fnDialogue("[HIDEALL][MUSIC|Love]Thought: Laura isn't [ITALICS]here[ENDITALICS], per se, but...[P] I look down at the walkie talkie in my hand.")
	fnDialogue("Player: Uhh... hey, Laura.[P] You around? ")
	fnDialogue("[CHARENTER|Walkie]WalkieTalkie: [VOICE|Voice|WalkieTalkie]*cheering*")
	fnDialogue("Thought: ?")
	fnDialogue("Player: Laura, I was wondering if you wanted to hang out today.[P] The club meeting is over, so...[P]yeah.")
	fnDialogue("WalkieTalkie: [VOICE|Voice|WalkieTalkie]...")
	fnDialogue("WalkieTalkie: Oh, y-yeah![P] *rustle* *rustle* Yes, sorry, I totally forgot.[P] I  *ahem* have a match today. *thump* THERE YOU GO ROUGE! GET HER!!!")
	fnDialogue("Player: What's that? It sounds really noisy where you are.")
	fnDialogue("WalkieTalkie: [VOICE|Voice|WalkieTalkie]Sorry, can you meet me at this address?[P] I-I'll explain when you get here.")
	fnDialogue("Thought: I look at the sun dipping dangerously low in the sky.[P] Eilette might get mad at me for coming home so late, and I have a tutoring lesson with Todd.")
	fnDialogue("Player: *sigh*...[P] what the heck. Why not?")
	fnDialogue("[HIDEALL][BG|Null]Thought: .[P][P][P].[P][P][P].")
	fnDialogue("[MUSIC|Tension]Player: ... Okay, this is definitely NOT where I was expecting to end up tonight.")
	fnDialogue("[BG|Root/Images/Backgrounds/All/Gym][IMAGE|Root/Images/MajorScenes/All/RollerDerby][CHARENTER|Laura]Laura:[E|Sad][ITALICS]What's that?[P] Did you *hrm* say something, [PLAYERNAME]?[ENDITALICS]")
	fnDialogue("Thought: I'm sitting in the bleachers of a totally incomrehensible but intense sport.")
	fnDialogue("Thought: Young women ricochet around a painted track, jostling each other, trying to get ahead of the rest of the pack.")
	fnDialogue("Announcer: And the Bloomington Bees are doing an excellent job holding that line, Carcass Lass has her hands full, trying to find--")
	fnDialogue("Thought: Though there aren't many spectators, the ones present fill the gym with their raucus cheers.")
	fnDialogue("Announcer: OH! Razzberry pulling a be-[P]EAUTIFUL spin out of her back pocket, that's the lead--")
	fnDialogue("Thought: Glancing at the court, then back to me, Laura reaches up and gently pulls my shoulders, so my ear is just inches from her lips.")
	fnDialogue("Laura: [ITALICS]G-glad you could make it!!![ENDITALICS]")
	fnDialogue("Thought: I nod, my back stiff. I can tell neither of us are really comfortable being so close to each other.")
	fnDialogue("Thought: I catch a whiff of shampoo off of her signature gradient hair--it smells refreshing, particularly in this hot, occupied gym. ")
	fnDialogue("Laura: [ITALICS]This match is in the *hrm* l-last few minutes. *ahem* W-we can hang out after, if you want!!![ENDITALICS]")
	fnDialogue("Player: [ITALICS]Okay!!![ENDITALICS]")
	fnDialogue("Announcer: Now the ladies are in position once more at the starting line. Hang on to your hats, folks!")
	fnDialogue("Announcer: The Foster Firefoxes are still full of fighting spirit![P] Like I always say, it ain't over till it's over.")
    
	--Save the game.
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Mindread\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
	PairanormalLevel_FlagCheckpoint("Mindread")
    
    --Dialogue.
	fnDialogue("[IMAGE|Null][BG|Null][MUSIC|Null]Thought: ...")
	fnDialogue("[BG|Root/Images/Backgrounds/All/Gym][MUSIC|Love]Player: So, do you play?")
	fnDialogue("Thought: About a half an hour later, Laura and I are on the court.")
	fnDialogue([[Thought: Today's players (of what I now know to be \"Roller Derby\") and organizers slowly pack up the leftover equipment.]])
	fnDialogue("Thought: Laura is laced up in her own skates and swerving slow circles around me--I watch her feet weaving the curious patterns that propel her forward.")
	fnDialogue("[CHARENTER|Laura]Laura: [E|Blush] Heheh, not officially.[P] But I want to *ahem* someday.")
	fnDialogue("Laura: [E|Sad]It's quite a ch-[P]challenge for me, because I get so easily overwhelmed by crowds and coordinating, and body contact and... basically all of it?")
	fnDialogue("Laura: I'm just taking lessons right now but--")
	fnDialogue("Thought: A player with a heavy duffel bag slung around her shoulders waves at us as she steps out of the gymnasium.[P] Laura smiles and returns the gesture.")
	fnDialogue("Laura: [E|Happy]--Everyone is very supportive.")
	fnDialogue("Thought: The future roller derbyist teeters a little bit and grabs my arm, which I offer reactively.")
	fnDialogue("Thought: Regaining balance after a moment, she lets go and continues circling.[P] I secretly enjoy how naturally we reached for each other.")
	fnDialogue("Player: So how come I didn't see you at school today?")
	fnDialogue("Thought: Laura skids, freezing.")
	fnDialogue("Laura: [E|Shock]Oh![P] Uh, I-I was sick.")
	fnDialogue("Player: You're sick?")
	fnDialogue("Laura: [E|Shock]No![P] I mean, yes.[P][E|Sad] I-I-I mean, *ahem* I-I was.[P][E|Blush] I h-had a... stomache-ache... f-fever, and all that.")
	fnDialogue("Player: That sounds terrible.[P] Shouldn't you be at home, resting?")
	fnDialogue("Laura: [E|Shock]No! I-I'm fine.[P] *hrm* Ah, I'm fine [ITALICS]now[ENDITALICS]...[P][P] *sigh*")
	fnDialogue("Thought: She digs the toe of her skate guiltily into the polished wood floor.[P] That's when I realize she's still wearing her school uniform.")
	fnDialogue("Laura: [E|Sad]I-I skipped school on purpose today.[P] So I w... *hrm* so I w-wouldn't have to meet Adam.")
	fnDialogue("Thought: My mouth bobs open and closed.")
	fnDialogue("Laura: [E|Sad]We... we agreed to see each other in the library.[P][E|Mad] B-but today when I woke up *ahem* I couldn't... I...*squeak* oh, I totally panicked!")
	fnDialogue("Player: But... Didn't you say--")
	fnDialogue("Laura: [E|Mad]I know.[P] I know what I s-said![P][E|Sad] *sigh* Look, y-you just wouldn't [GLITCH]understand, all right?")
	fnDialogue("Laura: [E|Mad]I don't want to get teased, I d-don't want to get bullied,[P] or beat up, I [ITALICS]don't[ENDITALICS] want to have my heart smashed into a million tiny pieces, okay?[P][CRASH] a-Argh!!!!")
	fnDialogue("Thought: It may be the first time I've ever heard Laura truly raise her voice.")
	fnDialogue("Thought: Luckily, I'm just too confused to be hurt. She skates to a water fountain at the edge of the court.")
	fnDialogue([[Laura: [E|Mad]Sometimes your whole \"coma\" thing can be really f-f-frusting,[GLITCH] you know?[P] People have to explain [ITALICS]everything[ENDITALICS] to you *hrm*.]])
	fnDialogue("Laura: Gee, Laura what is dating?[GLITCHTEXT] I DONT WANNA BE ALONE I DONT WANNA[ENDGLITCHTEXT] Why are you so pitiful at it?")
	fnDialogue("Laura: Why is it [ITALICS]a million times[ENDITALICS] [GLITCHTEXT]WHAT A FREAK[ENDGLITCHTEXT]harder for [ITALICS]you[ENDITALICS] than it is for e-e-everyone else?")
	fnDialogue("Laura: Explain [GLITCH]that to me, Laura!!! ")
	fnDialogue("Thought: Laura takes a long, rueful drink, wiping her mouth with her arm.")
	fnDialogue("Laura: [E|Sad]Well, I [GLITCH]for one am really, really tired of having to [ITALICS]explain[ENDITALICS][GLITCHTEXT] I DONT WANNA BE ALONE I DONT WANNA[ENDGLITCHTEXT] things all the time for everyone,")
	fnDialogue("Laura: When all I want to do is just have one normal bloody crush that doesn't end in disaster!")
	fnDialogue("Thought: I'm still confused, but now I'm definitely hurt as well.[P] Nobody else near the court seems to have noticed this devastating blow...")
	fnDialogue("Thought: ... And the gym continues with the uncomfortable scraping of chairs and tables, to be gathered up and used for the next roller derby event.")
	fnDialogue("Laura: [E|Shock][PLAYERNAME]!")
	fnDialogue("Thought: She gasps, and I realize tears are running down her cheeks.[P] Mine, too.")
	fnDialogue("Laura: [E|Mad]I'm so sorry-- I...[P][E|Sad] Oh, I feel so awful. I didn't mean that, I was just s-so angry-- [P]not at you!")
	fnDialogue("Player: ... I promise I won't bother you about the Adam thing again.[P] If I can't understand what you're talking about, then I shouldn't try to-- ")
	fnDialogue("Laura: [E|Sad]No. Wait, [PLAYERNAME].")
	fnDialogue("Thought: She skates right back up to me, gently braking against my shoulder.")
	fnDialogue("Laura: [E|Mad]Look.[BR][P][P][P][E|Sad] *sigh*.[P] When I was very very young... Er.")
	fnDialogue("Laura: Doctors[P] and everyone decided[P] I was a boy.")
	fnDialogue("Laura: [E|N]But as I was growing up, I realized I was actually a girl.[P] That's called being trans.")
	fnDialogue("Player: [P][P][P]Oh.")
	fnDialogue("Laura: [E|N]And I had to make a lot of changes.[P][E|Blush] *ahem* I-I wouldn't have it any other way, getting to be myself.")
	fnDialogue("Laura: But... [P]some people in this small town never really understood me.[P][E|Sad] And I get p..picked on, quite a lot, actually.")
	fnDialogue("Laura: [E|N]When you came to school, and everyone was *hrm* obsessed with you [P][E|Happy]-- God, I think I actually f-felt... *sniff* secretly happy.")
	fnDialogue("Laura: [E|Sad]Finally there was someone in Foster Falls with a weirder backstory.[P] *sniff* haha, I'm awful, aren't I?")
	fnDialogue("Thought: Laura's tears fall more freely now.[P] I have a strong urge to brush them away with my fingertips. ")
	fnDialogue("Laura: [E|Blush]When I talk with Adam, I know he likes me for my mind, for who I am.")
	fnDialogue("Laura: [E|Shock] *sniff* But I'm scared, if he meets me...[P] what if he hates me so much,[E|Sad] he tries to hurt me?")
	fnDialogue("Thought: Something deep in me twists and pinches")
	fnDialogue("Thought: --I don't have to be a mind reader,[P] although technically I suppose I am,[P] to see there's a lot of pain in the sniffling skater's past.")
	fnDialogue("Thought: Our hands intertwine, and I squeeze her fingers, hoping somehow she knows how sorry I am.")
	fnDialogue("Player: [CHAREXIT|Laura][IMAGE|Root/Images/MajorScenes/All/LauraCrush]Laura.[P] Um.[P] Thanks for telling me everything.")
	fnDialogue("Thought: She nods.")
	fnDialogue("Laura: [E|N]Thanks for listening.")
	fnDialogue("[HIDEALL][BG|Null][IMAGE|Null]Thought: ...")
	fnDialogue("[BG|Root/Images/Backgrounds/All/WalkHome]Thought: Laura and I leave the gym, a little emotionally rattled but otherwise pretty okay.[P] As we walk home she explains the rules of roller derby to me...")
	fnDialogue("Thought: ... And I manage to tell a pretty good joke about why the school locker rooms always smell like hamburgers in the morning.")
	fnDialogue("Thought: We don't mention Adam or our earlier conversation anymore, but when we at last part ways for the evening...")
	fnDialogue("Thought: ... I make a silent wish that Laura gets to meet someone soon who sees her... who likes her,[P] like I do.")
    
	--Save the game.
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"DollerRerby\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
	PairanormalLevel_FlagCheckpoint("DollerRerby")
    
    --Which chapter to jump to.
    local iChapterDestination = VM_GetVar("Root/Variables/System/Player/iChapterDestination", "N")
    if(iChapterDestination == 3.0) then
        LM_ExecuteScript(gsRoot .. "../Chapter 3/Gameplay_SceneD/2 - Arrive At Home.lua", "Start")
        
    elseif(iChapterDestination == 4.0) then
        LM_ExecuteScript(gsRoot .. "../Chapter 4/Gameplay_SceneD/0 - Walk Home.lua", "LimoScene")
    end
    
end
