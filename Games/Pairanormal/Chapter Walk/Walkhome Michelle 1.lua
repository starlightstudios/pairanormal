-- |[ ========================== Walking Home With Michelle, Scene 1 =========================== ]|
--Walking home with Michelle for the second time.

-- |[Arg Resolve]|
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

-- |[ ======================================= Execution ======================================== ]|
--Begin.
if(sTopicName == "Start") then

    --Add 5 to friendship score.
    local iMichelleFriend = VM_GetVar("Root/Variables/System/Romance/iMichelleFriend", "N")
    VM_SetVar("Root/Variables/System/Romance/iMichelleFriend", "N", iMichelleFriend + 50.0)
	
	--Dialogue.
	fnDialogue("[HIDEALL][MUSIC|Love]Thought: Even though the last time Michelle and I spoke, we got in a fight...[P]I still find myself looking for her bouncing purple bob.")
	fnDialogue("Thought: Someone taps me on the shoulder.")
	fnDialogue("[CHARENTER|Mari][CHARENTER|Stacy]Mari: [E|N]HIIIII")
	fnDialogue("Player: Guh!!![P] Geez, guys, don't sneak up on me like that.")
	fnDialogue("Stacy: [E|Happy]Sorry, hehe.[P][E|N] We're here for Michelle.")
	fnDialogue("[CHARENTER|Michelle]Michelle: [E|Blush]...")
	fnDialogue("Player: Oh! H[P]... Hi Michelle.")
	fnDialogue("Mari: [E|N]Michelle has something she wants to say, don't you?")
	fnDialogue("Michelle: [E|Mad]Oh leave it![E|Blush] I'm not a bloody infant.")
	fnDialogue("Thought: She steps to me, black lips stretched in a tight line.")
	fnDialogue("Michelle: [E|Blush]I... I'm sorry for yelling at you the other day, all right?")
	fnDialogue("Player: ... Did you give back the stuff you stole?")
	fnDialogue("Mari: [E|Shock][CRASH]WHAT!?!? YOU STOLE STUFF!?!?")
	fnDialogue("Stacy: [E|Mad]AND YOU DIDN'T EVEN ASK US TO COME ALONG!?!?")
	fnDialogue("Michelle: [E|Mad]YES, ALL RIGHT!?! I GAVE IT BLOODY BACK.[E|Sad] You... I don't want to get in trouble anymore.")
	fnDialogue("Michelle: I... want to stay here, in Foster Falls for a bit.[P] Pfft. ")
	fnDialogue("Player: ...")
	fnDialogue("Michelle:[E|Happy] Until I get bored of the place, anyway.")
	fnDialogue("Mari: [E|Blush]Oh my gooooddd. Friendshiiiiip!")
	fnDialogue("Stacy: [E|N]This is really sexy energy right here. Just saying.")
	fnDialogue("Michelle: [E|Blush]Yeah, yeah. I'm heading out of here.")
	fnDialogue("Player: Hey, Michelle.[P] Let's walk home together!")
	fnDialogue("Michelle: [E|Blush]...")
	fnDialogue("Thought: Michelle doesn't tell me off, which I think means it's okay.")
	fnDialogue("[HIDEALL][BG|Null]Thought: ...")
	fnDialogue("Player: ... I thought you JUST said you were trying to stay out of trouble.")
	fnDialogue("[BG|Root/Images/Backgrounds/All/Forest]Michelle: [E|Mad]We're not doing anything illegal, you big whiner![E|Blush] I want to [ITALICS]show[ENDITALICS] you something cool. ")
	fnDialogue("Thought: Michelle has lead me down a heavily wooded path, not unlike the one we take to get the clubhouse.[P] A fence cutting across the way abruptly stops the journey.")
	fnDialogue("Thought: I'm playing suspicious, but mostly because it's kind of amusing to watch the towering goth squirm.")
	fnDialogue("Player: What is it you want me to see?")
	fnDialogue("Michelle: [E|N]YOU'LL SEE IN A MOMENT. We just have to cross this fence first.")
	fnDialogue("Player: ...[P]It's a dead body, isn't it? ")
	fnDialogue("Thought: There's a gap under the gate where the fencing meets a post.")
	fnDialogue([[Thought: Not even hesitating at the \"NO TRESPASSERS\" sign,[P] Michelle opts to climb over the gate,[P] using a nearby boulder as a boost.]])
	fnDialogue("[HIDEALL]Michelle: If you [P]*grunt* wimp out on me now [P]*huff* whew, heh.[P] You'll never make it *grunt* to the end! *huff* *huff*.[P] Oh, fuck.[P] My leg's caught on the fucking--")
	fnDialogue("Player: Do you need help?")
	fnDialogue("Michelle: [CRASH]NO![P][CRASH] AND DON'T YOU FUCKING DARE LOOK UNDER MY SKIRT!")
	fnDialogue("Player: I wasn't l--[P]okay, well, now I am.[P] Now that you've mentioned it I can't look at anything else.")
	fnDialogue("Michelle: [CRASH]SHUT YOUR EYES![P][CRASH] [ITALICS]SHUT YOUR BLOODY EYES RIGHT NOW!!![ENDITALICS]")
	fnDialogue("Player: .[P].[P].[P][SFX|World|Blink]Are those teddy bears on your underwear?")
	fnDialogue("Michelle: GYAAAAH!")
	fnDialogue("Thought: I hear a [CRASH]thud.[P] Michelle scrambles to her feet on the other side of the fence...")
	fnDialogue("Thought: ... Dusting herself off and flipping her hair as though she had effortlessly done a backflip off the gate, instead of falling like a sack of potatoes.")
	fnDialogue("[CHARENTER|Michelle]Michelle: [E|N]Right then.[P] Come on.")
    
	--Save the game.
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Fencesitter\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
	PairanormalLevel_FlagCheckpoint("Fencesitter")
    
	--What to say to Michelle:
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (What should I do?)") ]])

	local sDecisionScript = "\"" .. gsRoot .. "../Chapter Walk/Walkhome Michelle 1.lua\""
	fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Climb over the fence\", " .. sDecisionScript .. ", \"Climb\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Climb through the hole\", " .. sDecisionScript .. ", \"Through\") ")
	fnCutsceneBlocker()

--Climb over.
elseif(sTopicName == "Climb") then
	
	--Dialogue.
	fnDialogue("Thought: I decide to follow Michelle's lead and climb over the fence.")
	fnDialogue("Thought: The task seems simpler than she made it out to be, but I realize as soon as I get one leg over the top that the footholds of the lattice are too small for my feet to catch hold going down.")
	fnDialogue("Thought: The longer I stayed up there,[P] the more I felt the gate's top start digging uncomfortably into my thigh.")
	fnDialogue("Michelle: [E|Happy]What's the hold up?")
	fnDialogue("Player: Hang on,[P] hang on.[P][P][P] I'm gonna jump.")
	fnDialogue("Michelle: [E|Shock]What?")
	fnDialogue("Player: [P]Catch me.")
	fnDialogue("Michelle: [E|Mad]If you think I'm gonna bruise myself [ITALICS]again[ENDITALICS] just to-- [P][P]hey--[P][CRASH] UNNFFF!!!")
	fnDialogue("[HIDEALL]Thought: I launch myself into Michelle's general direction.[P] It's painful.")
	fnDialogue("Thought: [IMAGE|Root/Images/MajorScenes/All/CH2_MichelleSquash] We collapse in a puddle of limbs, hair, and steel-toed black boots.")
	fnDialogue("Michelle: [P][P][SFX|World|Ugh][ITALICS]Oooooooowwww.[ENDITALICS] ")
	fnDialogue("Player: M...[P] mistakes were made.")
	fnDialogue("Thought: [P][P][P]Michelle's body shakes beneath me--I'm worried she's about to strangle me, but then I hear her laughter.")
	fnDialogue("Thought: The enormous relief gets me to join in.")
	fnDialogue("Michelle: BAHAHAHAHAAAA!!![P] You launched off that fence like humpty fucking dumpty.")
	fnDialogue("Michelle: [E|Neutral]Not very elegant, but it gets the job done, eh?[P] [IMAGE|Null]Come on.")

    --Rejoin.
    LM_ExecuteScript(LM_GetCallStack(0), "Rejoin")
    
--Go through.
elseif(sTopicName == "Through") then
	
	--Dialogue.
	fnDialogue("Thought: I opt for the safer route and go under the fence.")
	fnDialogue("Thought: I get a little dirty, and some strands of lattice catch my clothes and scratch my exposed skin.")
	fnDialogue("Player: While I'm down here I guess I should start coming up with excuses for Eilette.")
	fnDialogue([[Player: She'll want to know why I'm tracking dirt into her house,[P] and \"breaking and entering\" doesn't sound like an appropriate response.]])
	fnDialogue("Michelle:[E|Happy] Pahh.")
	fnDialogue("Thought: I pass through,[P] and dust the dirt off my knees.")
	fnDialogue("Michelle: [E|Neutral]Not very elegant, but it gets the job done, eh?[P] Come on.")

    --Rejoin.
    LM_ExecuteScript(LM_GetCallStack(0), "Rejoin")

--Rejoin.
elseif(sTopicName == "Rejoin") then
	
	--Dialogue.
	fnDialogue("[HIDEALL][BG|Null][MUSIC|Somber]Thought: ...")
	fnDialogue("[IMAGE|Root/Images/MajorScenes/All/CH2_Shack][BG|Root/Images/Backgrounds/All/Sunset]Thought: Not far from the fence, we reach an abandoned shack patiently waiting to be found under the dense forest foliage.")
	fnDialogue("Player: [P][P]What's this?")
	fnDialogue("Thought: I can feel Michelle's eyes on my neck as I examine it.")
	fnDialogue("[CHARENTER|Michelle]Michelle: I found it last summer... I usually crash here when I play hooky.")
	fnDialogue("Player: Gee,[P] there's an [ITALICS]awful lot[ENDITALICS] of abandoned, hidden buildings in this town,[P] isn't there?")
	fnDialogue("Michelle: [E|Neutral]Yep--[P]Oy, what are you doing?")
	fnDialogue("Thought: I pull my hand away from the shed door.")
	fnDialogue("Player: We're not going inside?")
	fnDialogue("Michelle: [E|Blush]I... um... N-no. Not yet. Sorry. I-I  haven't cleaned up in there yet.")
	fnDialogue("Thought: This elicits a shrug; I don't know that any amount of cleaning is going to make this place look nice.")
	fnDialogue("Thought: [IMAGE|Null]Instead, she guides me around the back of the shed.")
	fnDialogue("Michelle: [E|N] Anyway. This is what I really brought you here for.[P][E|Happy] Pretty bloody fantastic, innit?")
	fnDialogue("Thought: The bushy branches behind the little shack have been torn down, opening up into a partial but beautiful view of town.")
	fnDialogue("Thought: [ITALICS]No wonder I was so out of breath coming here--we must have been climbing uphill![ENDITALICS]")
	fnDialogue("Player: This...[P]is the most beautiful thing I've ever seen.")
	fnDialogue("Michelle: [E|N]Oh, stop. You're not serious?")
	fnDialogue("Thought: I look at her pale face in the golden cast of the evening sunset.[P] Her eyes widen.")
	fnDialogue("Michelle: [E|Shock]... Oh.")
	fnDialogue("Thought: She crumples against the shed's wall, arms crossed.")
	fnDialogue("Michelle: [E|Neutral]Y'know, ever since you first came to the club,[P] I thought you were lying about the whole coma thing.")
	fnDialogue("Michelle: [E|Mad] I mean absolutely none of it makes sense,[P] and everyone dotes on you more than they should.")
	fnDialogue("Player: But I--")
	fnDialogue("Michelle: [E|Mad]I keep thinking--you're DEFINITELY hiding something.")
	fnDialogue("Thought: I flinch under the brief, icy glare she flashes me.")
	fnDialogue("Michelle: [E|Mad]But... [P][E|Sad]for the most part, you're always so bloody HONEST about everything.[P] And you're always talking about your [ITALICS]emotions[ENDITALICS] and all that corny crap.")
	fnDialogue("Player: ...")
	fnDialogue("Michelle: [E|Mad]Either you're the biggest poser I've ever met,[E|Sad] or I was wrong about you.")
	fnDialogue("Michelle: Being honest is hard to do sometimes, innit?[P] It's much easier to flip everyone off and not give a damn what they think.")
	fnDialogue("Thought: She sighs.")
	fnDialogue("Player: Um, about that--")
	fnDialogue("Michelle: [E|Mad][SPEEDTEXT]Forget it![ENDSPEEDTEXT][P][P][E|Neutral] It's getting late.[P] I'm gonna have a vape. Then we oughta get back home.")
	fnDialogue("Player: ... Huh?")
	fnDialogue([[[HIDEALL]Thought: For the next hour or so,[P] I'm exposed the the mesmerizing, slightly nauseating ritual of \"vaping\".]])
	fnDialogue("Player: Can you do the thing where you make a ring with the smoke again?")
	fnDialogue("[CHARENTER|Michelle]Michelle: [E|Mad]I keep telling you, it's [ITALICS]vapor[ENDITALICS].")
	fnDialogue([[Michelle: [E|Happy]Anyway, the ring is nothing compared to this one trick I'm working on called...[P] \"the dragon\"]])
	fnDialogue("Player: Whoah.[P] Yeah, show me that one!")
	fnDialogue("Michelle: [E|Blush]You've got to be patient-- It's only theoretically possible.")
	fnDialogue("Michelle: [E|Happy]I haven't worked out all the logistics yet, but when I do... It's gonna be awesome.")
	fnDialogue("[HIDEALL][BG|Null]Thought: When she's done, we carefully make our way back to somewhat-familiar streets, and from there go our seperate ways.")
	fnDialogue("Thought: The large goth girl doesn't even bother to look back at me as she trudges off, popping a new piece of gum in her mouth.")
	fnDialogue("Thought: But I find myself watching her until the last fleck of violet hair finally disappears beyond some distant hill.")
    
	--Save the game.
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"LimoScene\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
	PairanormalLevel_FlagCheckpoint("LimoScene")
    
    --Next script.
    LM_ExecuteScript(gsRoot .. "../Chapter 4/Gameplay_SceneD/0 - Walk Home.lua", "LimoScene")
    
end
