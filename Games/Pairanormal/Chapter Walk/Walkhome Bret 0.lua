-- |[ ============================ Walking Home With Bret, Scene 0 ============================= ]|
--Walking home with Bret for the first time. This can be called from one of two different places.
-- The chapter variable marks which location to move the script back to when this ends.

-- |[Arg Resolve]|
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

-- |[ ======================================= Execution ======================================== ]|
--Begin.
if(sTopicName == "Start") then

    --Add 5 to friendship score.
    local iBretFriend = VM_GetVar("Root/Variables/System/Romance/iBretFriend", "N")
    VM_SetVar("Root/Variables/System/Romance/iBretFriend", "N", iBretFriend + 50.0)

    --Flag.
    VM_SetVar("Root/Variables/System/Player/iWalkedWithBretA", "N", 1.0)
	
	--Dialogue.
	fnDialogue([[[HIDEALL][MUSIC|Love]Thought: Today feels like another great day to join the club's \"resident thing expert\" for a walk home.]])
	fnDialogue("Player: Hey, Bret.")
	fnDialogue("[CHARENTER|Bret]Bret: [E|N]Howdy doooooo--I mean, [E|Happy]Yes, I'm Bret![P][E|Sad]--Ugh, what am I even saying... [E|N]yeah?")
	fnDialogue("Player: Wanna walk home together?")
	fnDialogue("Bret: [E|N]Heh, Sure.")
	fnDialogue("Thought: We wave goodbye to everyone and set off.")
	fnDialogue("[HIDEALL][BG|Null]Thought: ...")
	fnDialogue("[BG|Root/Images/Backgrounds/All/WalkHome]Thought: ...")
	fnDialogue("[CHARENTER|Bret]Bret: [E|N]... And you got the snoopers,[P] the boopers,[P] the beanie weanies,[P] the waggamiffs,[P] their little triangle ears.")
	fnDialogue("Bret: [E|Mad]Add it all together, okay,[P] and cats are categorically the best pet.[P][E|Happy] Thanks for coming to my TED talk.")
	fnDialogue("Player: A cat wouldn't save your life if you were drowning.")
	fnDialogue("Bret: [E|Mad]Okay but, like he [ITALICS]couldn't[ENDITALICS] anyway.[P][E|N] My cat is too small, and can't swim so...[P]he's already smart, see.")
	fnDialogue("Bret: [E|Happy]He knows not to foolishly risk his own life to save mine.[P][E|Mad] And I respect that about him.")
	fnDialogue("Player: They're okaaaaaay, I guess. ")
	fnDialogue([[Bret: [E|Happy]Did you know I put him on a dating site once?[P] That was wild.[P][E|Shock] By the way, never write \"furry\" in your personal description, even as a joke...]])
	fnDialogue("Player: I have no idea what you're talking about.")
	fnDialogue("Bret: [E|Sad]I saw so many penises.[P][E|Mad] A pound of penii.[P][E|Happy] A bouquet, a veritable plethora, of unsolicited dick.")
	fnDialogue("[HIDEALL][BG|Root/Images/Backgrounds/All/Forest]Thought: [P][P]From there, (somehow) the topic turns to today's club meeting,[P] and batting about theories for what each strange sight could mean.")
	fnDialogue("Thought: The ideas almost become too heavy to carry standing up, so we find ourselves taking a seat on a low hill. ")
	fnDialogue("[CHARENTER|Bret]Bret: [E|Happy]Today was such a blast! No doubt about it![P] [PLAYERNAME], like, seriously.[P] I don't know what it is, but I've gotten so much more enthusiastic these past few weeks.")
	fnDialogue("Bret: [E|N]The club just used to be... you know, kind of fun,[E|Sad] kind of blah, whatever, [E|Shock]but like[P]... I've been having so much [ITALICS]more[ENDITALICS] fun lately.")
	fnDialogue("Player: Only in the Paranormal Club could seeing horrifying sights be considered a bonus.")
	fnDialogue("Bret: [E|Blush]Well, don't forget.[P] We've got a new member, too. ")
	fnDialogue("Player: Ohhhh right, sure.[P] I'm known as a real[GLITCH] live wire around these parts. ")
	fnDialogue("Bret: [E|Happy]I think it's you, really![P][E|Mad] You've inspired us to really get out there and do some serious investigation.")
	fnDialogue("Player: [SFX|World|Ugh]Hmmph, I think you're all just excited 'cuz I'm new.[P] I think [ITALICS]I'll[ENDITALICS] have even more fun when I get to be closer to everyone.")
	fnDialogue("Player: Like, you must've had plenty of good times before me --the folks in Paranormal Club have been your friends for ages!  ")
	fnDialogue("Thought: The smile fades from Bret's lips.[P] He squints at something off in the distance, and his next words are so soft I'm not sure he really said them.")
	fnDialogue("Bret: [E|Sad][GLITCH]... Are they, though?")
	fnDialogue("Player: [P][P]Bret.[P][GLITCH][GLITCHTEXT]I SHOULD BE H[GLITCH]APPY I SHOULD BE HAPPY I SHOULD BE[ENDGLITCHTEXT] H-how could you say that?")
	fnDialogue("Bret: [E|Happy]Ah, haha, don't worry, I'm only kidding.[P][E|N] I don't know where that came from, I guess I'm just loopy from all the crazy excitement from today...")
	fnDialogue("Thought: He trails off, and continues staring at the horizon. [P]I fidget with a piece of grass. I'd give anything to know what he's thinking right now.")
	fnDialogue("Thought: Then I realize--[GLITCH][ITALICS]I know a pretty good way to find out.[ENDITALICS]")
	
	--Save the game.
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Mindread\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
	PairanormalLevel_FlagCheckpoint("Mindread")
    
	--What to say to Bret:
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (What should I do?)") ]])

	local sDecisionScript = "\"" .. gsRoot .. "../Chapter Walk/Walkhome Bret 0.lua\""
	fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Jump into Bret's mind\", " .. sDecisionScript .. ", \"Jump\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Ask him outright\", " .. sDecisionScript .. ", \"Ask\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Force him using karate\",  " .. sDecisionScript .. ", \"Violence\") ")
	fnCutsceneBlocker()

--Mind Reading Time!
elseif(sTopicName == "Jump") then

    --Dialogue.
	fnDialogue("[HIDEALL][BG|Null]Thought: .[GLITCH].[GLITCH].")
	fnDialogue("[BG|Root/Images/Backgrounds/All/Classroom]Thought: I'm standing, or I guess Bret is standing up against a wall with a line of other children.[P] In front of us are five groups.")
	fnDialogue("Thought: Each group takes turns picking classmates out of the lineup.[P] I swallow--[P][ITALICS]please, please don't let me be picked last.[ENDITALICS]")
	fnDialogue("[CHARENTER|YStacy]Stacy: [FOCUS|YStacy][VOICE|Voice|Stacy]I pick Bret!")
	fnDialogue("Thought: I've been chosen! I nod, striding up to my expectant team members.[P] The other leaders groan in disappointment, and it's hard to hide my grin.")
	fnDialogue("Thought: [ITALICS]Yes sirree, Ladies and Gentlemen, the Bretmeister is a hot commodity here in 4th period. [P]No autographs please![ENDITALICS]")
	fnDialogue("Thought: The other children are picked one by one--the last one chosen, I note with some satisfaction, is that jerk Reggie Plenk.")
	fnDialogue("Thought: Better yet, he's not with me. Things are shaping up great today!")
	fnDialogue("[HIDEALL]Thought: ...")
	fnDialogue("[CHARENTER|YStacy][CHARENTER|Kid3][CHARENTER|Kid1][CHARENTER|Kid2]Stacy: [FOCUS|YStacy][VOICE|Voice|Stacy]Okay guys. So the volcano that we got is... ''Mount Rainer.''[P] And... it's in Canada apparently.")
	fnDialogue("Thought: Amazing! Could my luck be any better??")
	fnDialogue("Player: [VOICE|Voice|Bret]I used to live in Canada, don'cha know! Heheh, that's what some Canadians say.[P] Not me though.")
	fnDialogue("Stacy: [FOCUS|YStacy][VOICE|Voice|Stacy]Oh, awesome!")
	fnDialogue("Player: [VOICE|Voice|Bret] I never went to Mount Rainer, actually,[P] but I do know lots of things already about it!")
	fnDialogue("Player: [VOICE|Voice|Bret] [SPEEDTEXT] It's still really active, and it causes lots of earthquakes each year. As a matter of fact, if it ever erupts, Canada could really be in danger of--[ENDSPEEDTEXT] ")
	fnDialogue("Stacy: [FOCUS|YStacy][VOICE|Voice|Stacy]--Tell you what Bret, I'm going to assign you the research portion of the project.")
	fnDialogue("Thought: She hands me a piece of paper with several questions listed on it.")
	fnDialogue("Stacy: [FOCUS|YStacy][VOICE|Voice|Stacy]Make sure you get to a library.[P] You know how to cite stuff correctly, right?")
	fnDialogue("Player: [VOICE|Voice|Bret]Oh, totally! I've got this for sure. Let's just say ''Dewey'' is my middle name.[P] Just kidding, haha, it's not actually--")
	fnDialogue("Student: [FOCUS|Kid3]Nice! Way to go, Bret!")
	fnDialogue("Student: [FOCUS|Kid2]I'm so glad you're on our team.")
	fnDialogue("Stacy: [FOCUS|YStacy][VOICE|Voice|Stacy]Okay, now, who wants to make the volcano?")
	fnDialogue("Player: [VOICE|Voice|Bret]Lots of people put their hand up, including me--I love paper mache!")
	fnDialogue("Student: [FOCUS|Kid2]It's a big job, we should all work together on it.")
	fnDialogue("Stacy: [FOCUS|YStacy][VOICE|Voice|Stacy]Ooh, I know. Let's all come to my house and make it together this Friday!")
	fnDialogue("Thought: The group likes this idea and some people volunteer to get some supplies, or bring snacks.[P] Maybe afterwords they'll order some pizza, too.")
	fnDialogue("Player: [VOICE|Voice|Bret]Oh, wait-- um.[P] Friday actually isn't good for me, I have a doctor's appointment after school.[P] I'm going to get some new glasses.")
	fnDialogue("Stacy: [FOCUS|YStacy][VOICE|Voice|Stacy]That's okay.[P] Tell you what, you can just email us some pictures, and we can use those to make the volcano!")
	fnDialogue("Student: [FOCUS|Kid2]And you can help us present, too.")
	fnDialogue("Student: [FOCUS|Kid1]I have a laptop if anyone needs it!")
	fnDialogue("Student: [FOCUS|Kid2]Ooh![P] Bret should write some jokes for the presentation. Remember the joke he wrote for the tectonic plate project?[P] That was funny.")
	fnDialogue("Stacy: [FOCUS|YStacy][VOICE|Voice|Stacy]Oh my god, dude, yes![P] That'll get us a A+ for sure.")
	fnDialogue("Player: [VOICE|Voice|Bret]Yeah, yeah I could totally do that.[P] I guess, I just bring it up because I have some paper mache stuff at [ITALICS]my[ENDITALICS] house already--")
	fnDialogue("Player: [VOICE|Voice|Bret]I've actually been getting into miniature dioramas a lot lately, so I--")
	fnDialogue("Student: [FOCUS|Kid1]Dioramas![P] Haha, Bret, you're such a nerd.")
	fnDialogue("Stacy: [FOCUS|YStacy][VOICE|Voice|Stacy]Dude, [ITALICS]this[ENDITALICS] is why I love having Bret in the same group as me.[P] Dude's already on [ITALICS]top[ENDITALICS] of it!")
	fnDialogue("Stacy: [FOCUS|YStacy][VOICE|Voice|Stacy]Okay. So I'm thinking... why don't you bring the paper mache stuff to school tomorrow?")
	fnDialogue("Stacy: [FOCUS|YStacy][VOICE|Voice|Stacy]Then the rest of us can use it for the [ITALICS]fete[ENDITALICS],[P] I mean, the study group on Friday.")
	fnDialogue("Player: [VOICE|Voice|Bret]Oh, okay.[P] Sure.")
	fnDialogue("Student: [FOCUS|Kid1]Good idea, Stacy! ")
	fnDialogue("Student: [FOCUS|Kid2]This is gonna be a great project! ")
	fnDialogue("[HIDEALL]Thought: I should be [GLITCH]happy, shouldn't I?[P] Volcanoes are cool. Everybody wants me in their group.[P] Reggie Plenk has to pick on someone else.[P][GLITCH] I [ITALICS]should[ENDITALICS] be happy!")
	fnDialogue("[CHARENTER|YStacy][CHARENTER|Kid1][CHARENTER|Kid2][CHARENTER|Kid3]Student: [FOCUS|Kid2]Did you see [GLITCH][ITALICS]Caramel Crush's Bizarre Adventure[ENDITALICS] the other day?")
	fnDialogue("Stacy: [FOCUS|YStacy][VOICE|Voice|Stacy]I haven't![P] We should make time to see it this weekend. ")
	fnDialogue("Student: [FOCUS|Kid3]Ooh, let's go after [GLITCH]we make the volcano!")
	fnDialogue("Thought: I should be happy.")
	fnDialogue("[HIDEALL][BG|Null]Thought: .[GLITCH][P][P][P].[GLITCH][P][P][P].[GLITCH][P][P][P].[GLITCH][P][P][P].")
	fnDialogue("[BG|Root/Images/Backgrounds/All/Forest][CHARENTER|Bret]Bret: [E|Shock]--[PLAYERNAME], you good? You started to doze off on me for a second.")
	fnDialogue("Player: [CRASH]I-I wasn't dozing off![P] I swear--I was just feeling a little lightheaded.[P][SFX|World|Ugh] Uh...")
	fnDialogue("Thought: I sit forward nervously, and Bret's brows knit in concern.[P] God, here he was, pouring his heart out to me, and I'm frigging projecting into the astral plane.")
	fnDialogue("Player: Uh--I was just thinking.[P] How considerate you are to other people. Always willing to help out and stuff.")
	fnDialogue("Bret: [E|Shock]...?")
	fnDialogue("Player: I... wish I was as smart as you so I could, I dunno, help you out with something like you do for other folks.")
	fnDialogue("Bret: [E|Blush]Oh. [E|N]Psshht. [E|Happy]Whaaat?[P][E|Blush] That's so sweet of you to say, dude.")
	fnDialogue("[HIDEALL]Thought: He sinks back into the grass. I breathe a sigh of relief.")
	fnDialogue("[IMAGE|Root/Images/MajorScenes/All/Bret]Thought: After a pause, I hear him shifting.")
	fnDialogue("Thought: His next words are softer, and they send a shiver down my spine from the moment they tickle my right ear.")
	fnDialogue("Bret: But just so you know.[P] You already have helped me.")

    --Rejoin.
    LM_ExecuteScript(LM_GetCallStack(0), "Rejoin")

--Honesty Time!
elseif(sTopicName == "Ask") then

    --Dialogue.
	fnDialogue("Player: Bret...[P][P] [ITALICS]tell me what you really feel.[ENDITALICS]")
	fnDialogue("Bret: [E|Happy]uh---hahahaha!")
	fnDialogue("Player: Wh-what?[SFX|World|Ugh] I didn't say anything funny.")
	fnDialogue("Bret: [E|N] I know, I know.[P] You just sounded kinda like... Dr. Phil or something.[P][E|Shock] I thought maybe you were quoting him, but then[E|Happy] I realized you don't watch TV.")
	fnDialogue("Player: Ru-ude.")
	fnDialogue("Thought: Bret slowly leans backward until with a conceding grunt, his back lands in the rustling grass. I join him. ")
	fnDialogue("[HIDEALL][IMAGE|Root/Images/MajorScenes/All/Bret]Bret: Do you want to know what I really think, [PLAYERNAME]?")
	fnDialogue("Player: Of course I do.")
	fnDialogue("Bret: A deep dark secret that I've been hiding from you all this time. ")
	fnDialogue("Player: Yes!")
	fnDialogue("Bret: Something SO intense. So [ITALICS]psychologically vulnerable[ENDITALICS] that just speaking the words out loud will make me burst into tears, leading into like,[P] the ultimate emotionally cathartic moment?")
	fnDialogue("Player: [ITALICS]That would be so cool yes[ENDITALICS]")
	fnDialogue("Bret: I think...[P][P][SFX|World|Ugh]that cloud up there looks like a foot.")
	fnDialogue("Player: ...[P][CRASH]BREEEEEET!!!!")
	fnDialogue("Bret: Hahahahahaha! Oh man.[P] I'm sorry dude, but like, saw an opportunity, kind of had to take it.")
	fnDialogue("Player: You're killing me.")
	fnDialogue("Thought: I suppose there are some things people prefer to keep to themselves,[P] no matter what kind of prodding you give them.")
	fnDialogue("Player: *sigh*...[P] It really kind of does look like a foot though.")
	fnDialogue("Bret: Right???")
	fnDialogue("Thought: A few minutes pass, and we observe the Great Foot Cloud as it drifts south, slowly morphing into some other unrecognizable form; a Higher Foot we cannot comprehend.")
	fnDialogue("Player: What are clouds, by the way?")
	fnDialogue("Bret: It's water. ")
	fnDialogue("Player: Get out.[P][SFX|World|Blink] Water?[P] Like out of the water fountains water?")
	fnDialogue("Bret: Yep, yep.[P] Only it's all separated into teeny tiny dots, so it gets light, and it drifts up into the air depending on the temperature, and wind and stuff.")
	fnDialogue("Player: ... Wow.")
	fnDialogue("Bret: ... Yeah.")

    --Rejoin.
    LM_ExecuteScript(LM_GetCallStack(0), "Rejoin")

--RIP AND TEAR!
elseif(sTopicName == "Violence") then

    --Dialogue.
	fnDialogue("Player: Bret, come on.[P] Everyone likes to hang out with you.")
	fnDialogue("Player: You're smart, and funny, and always honest and open about what's going through your head.")
	fnDialogue("Bret: [E|Shock] I wasn't implying--[P] [E|Blush]Seriously, just forget I said anything.")
	fnDialogue("Player: Nnnnope.[P] What's it gonna take to prove that we care about you?")
	fnDialogue("Bret:[E|Sad]...")
	fnDialogue("Player: ... Am I gonna have to wrestle it out of you?[P][CRASH] Armlock![P][CRASH] Headbutt![P][CRASH] Leg pinch![P] Thunder cross split!")
	fnDialogue("Bret: [E|Shock][CRASH]Ah![P][SFX|World|Ugh][E|Happy] Oof, oww, Stacy really needs to stop showing you those wrestling videos on Youtube.")
	fnDialogue("Player: [CRASH]Pile drive! [CRASH]Karate chop! [CRASH]Do you feel my friendship yet!?")
	fnDialogue("Bret: *gak*[P][E|Happy] I'm pretty sure whatever you're doing to me now is not a pile drive.")
	fnDialogue("[HIDEALL]Thought: We collapse onto the grass in giggles.[P] After the playful moment has passed, we remain there, watching the dusky orange sky while we catch our breath.")
	fnDialogue("[IMAGE|Root/Images/MajorScenes/All/Bret]Bret: [PLAYERNAME], *huff* *huff*... Thanks for cheering me up.")
	fnDialogue("Player: Aw, I'm happy to reassure you whenever you need it.")
	fnDialogue([[Bret: If \"reassurance\" involves as many elbows as that last one did, I might have to take a pass.]])
	fnDialogue("Player: He smiles at me from between the damp patches of grass, and goosebump ripple down my arms.")
	fnDialogue("Player: I was fooling around of course, but...[P]I'm not sure I made my point.[P] Does he know how much they care?")
	fnDialogue("Player: How much I care?")

    --Rejoin.
    LM_ExecuteScript(LM_GetCallStack(0), "Rejoin")

--Rejoin.
elseif(sTopicName == "Rejoin") then

	--Dialogue.
	fnDialogue("[IMAGE|Null]Thought: ...")
	fnDialogue("[CHARENTER|Bret]Bret: [E|Shock]Oh, man! Look at the time. I-I should really get going.")
	fnDialogue("Player: *ahem*![P] Yeah, me too.")
	fnDialogue("Thought: We pick up our bags, dusting ourselves off from the ground and getting more ready than I would have liked to go our seperate ways.")
	fnDialogue("Thought: Bret gives me one last parting grin.[P] His smile is infectious--so much better than the sadness in his eyes a few minutes ago.")
	fnDialogue("Bret: [E|N]Later!")
	fnDialogue("[HIDEALL]Thought: I guess... [P]It feels just as good to comfort someone as it does to be comforted.")
    
    --Which chapter to jump to.
    local iChapterDestination = VM_GetVar("Root/Variables/System/Player/iChapterDestination", "N")
    if(iChapterDestination == 3.0) then
        LM_ExecuteScript(gsRoot .. "../Chapter 3/Gameplay_SceneD/2 - Arrive At Home.lua", "Start")
        
    elseif(iChapterDestination == 4.0) then
        LM_ExecuteScript(gsRoot .. "../Chapter 4/Gameplay_SceneD/0 - Walk Home.lua", "LimoScene")
    end
end
