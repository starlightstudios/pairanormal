-- |[ ============================ Walking Home With Todd, Scene 0 ============================= ]|
--Walking home with Todd for the first time. This can be called from one of three different places.
-- The chapter variable marks which location to move the script back to when this ends.

-- |[Arg Resolve]|
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

-- |[ ======================================= Execution ======================================== ]|
--Begin.
if(sTopicName == "Start") then

    --Add 5 to friendship score.
    local iToddFriend = VM_GetVar("Root/Variables/System/Romance/iToddFriend", "N")
    VM_SetVar("Root/Variables/System/Romance/iToddFriend", "N", iToddFriend + 50.0)

    --Flag.
    VM_SetVar("Root/Variables/System/Player/iWalkedWithToddA", "N", 1.0)

	--Dialogue.
	fnDialogue("[HIDEALL][MUSIC|Love]Thought: I think I'll walk home with Todd today.")
	fnDialogue("Thought: Actually, we've got a study meeting later... at his house. [P]I say my hasty goodbyes to the club members and see if he's willing to take me there.")
	fnDialogue("[BG|NULL]Thought: ...")
	fnDialogue("[BG|Root/Images/Backgrounds/All/Kitchen][CHARENTER|Todd]Todd: [E|Mad]It's okay.[P] [E|Neutral]Let's do this one more time.")
	fnDialogue("Player: [SFX|World|Ugh] Arrrrgh, what's the [CRASH]point?")
	fnDialogue("Player: I'm not getting this, please just have someone commit me to a zoo and get it over with.")
	fnDialogue("Thought: If I was bad at reading, I was absolutely hopeless at math.")
	fnDialogue("Thought: Okay, I was sort of okay at basic arithmetic--[P][P]aka the stuff toddlers learn--but the concepts a highschooler had to tackle...")
	fnDialogue("Thought: Like figuring out if a train was going to arrive on time or calculating interest, were totally beyond me...")
	fnDialogue("Player: I just... [GLITCH]why are there all these [ITALICS]extra numbers[ENDITALICS]? It doesn't make any sense.")
	fnDialogue("Todd: [E|Sad]Mmm,[P] as far as I know, theres no superflous figures on this paper.")
	fnDialogue("Player: Haha Mr. big word jokester, yes there is.[P] Look, that one. Why do you have that?")
	fnDialogue("Todd: [E|Mad]...[P]Seven?")
	fnDialogue("Player: YES. [P] And... this one I think.")
	fnDialogue("Todd: [E|Mad]Six.")
	fnDialogue("Player: No, the one that's flipped around.[P] Six plus three.")
	fnDialogue("Todd: [E|Shock][SFX|Blink]Nine.")
	fnDialogue("Player: Yeah, yeah.[P] And which one is six plus two?")
	fnDialogue("Todd: [E|Shock]Eight. [P][E|Happy]Are you saying everything above the number six is too much for you?")
	fnDialogue("Player: No, just those three.[P] And then you go to ten, like [ITALICS]normal[ENDITALICS], and count [GLITCH]again from there;")
	fnDialogue("Player: Eleven, twevel--or twelve or whatever,[P] thirteen, fourteen, fifteen, [ITALICS]sixteen[ENDITALICS], and [GLITCH][GLITCH]then TWENTY.")
	fnDialogue("Player: Why are there three extra numbers!? It's throwing everything off.")
	fnDialogue("Todd: [E|Shock]...")
	fnDialogue("Player: Right? Do you see what I mean?[P][P][P][SFX|Ugh] Oh, no, stop looking at me like that.[P] I hate when you look at me like that.")
	fnDialogue("Todd: [E|Blush]I-I wasn't giving you any kind of look, I swear!")
	fnDialogue("Player: You totally were.[P] It's a little bit of pity mixed with a light pinch of horror and a dash of amusement.")
	fnDialogue("Todd: [E|Happy]Hey, for someone who wants to get committed to a zoo, that reaction could be very profitable--[P][CRASH][E|Shock]ouch![P][E|Neutral] Put that eraser down.")
	fnDialogue("Player: Make me!")
	fnDialogue("Todd: [E|Mad]You dare challenge [ITALICS]me[ENDITALICS]!?")
	fnDialogue("Thought: Todd's reactions are faster than I expected.")
	fnDialogue("Thought: He reaches across the table trying to snatch my weaponized eraser away, but I've got a strong grip on it.")
	fnDialogue("Thought: We thrash around playfully, clawing at each other and laughing. Every time Todd makes a forceful grab at my arm, a thrilling bolt runs through me.")
	fnDialogue("Thought: I don't want it to end--[P]but it does when my knee bumps into the corner of Todd's chair.")
	fnDialogue("Todd: [E|Blush]Oops![P] *huff* *huff* You okay? ")
	fnDialogue("Player: Yeah... hahah...[P] ooh, ow, it does smart a little, though. *gasp* *puff*")
	fnDialogue("Todd: [E|Neutral] Well, *huff* it's official.[P] I say we call it a day for now.")
	fnDialogue("Thought: I nod, and try to focus on the pain radiating from my joint instead of my disappointment.")
	fnDialogue("Thought: I don't suppose I could wrestle him again without a good reason. I can hear him packing our school things away.")
	fnDialogue("Todd: [E|Blush]So...[P] Nice weather we're having today, huh?")
	fnDialogue("Player: Oh yeah.[P] Super nice.")
	fnDialogue("Thought: Ugh, for goodness sake.[P] [ITALICS]Think of something actually interesting to say!!![ENDITALICS]")
	fnDialogue("Player: Um...[P]H-hey, do you believe in paranormal stuff, Todd?")
	fnDialogue("Todd: [E|Shock]Me?[P][E|Mad] Hmm... Not generally, but I try to keep an open mind.[P][E|Neutral] How 'bout you?")
	fnDialogue("Player: Ahah.[P] As an official member of the Paranormal Club, I guess I'm obligated to say yes.")
	fnDialogue("Player: But I mean, everything to me right now is...[P][P][P]not normal, you know?")
	fnDialogue("Todd: [E|Sad]Mmm.")
	fnDialogue("Player: I-if someone were to, for example, say that they could read minds,[P] or something[GLITCH] like that.")
	fnDialogue("Player: Well, who am I to say that they're wrong?[P][P] I don't know any better.")
	fnDialogue("Todd: [P][E|Happy]Oh, THAT would definitely be spurious. People can't read minds--and thank goodness.")
	fnDialogue("Player: [SFX|World|Blink]Wh-what's wrong with reading minds?")
	fnDialogue("Todd: [E|Shock]Dude, can you imagine how disastrous that would be?")
	fnDialogue("Todd: [E|Mad]Every embarrassing thought or feeling you have, right out in the open for everyone to hear?[P] It'd suck.")
	fnDialogue("Player: How could sharing your thoughts be bad?[P] Whenever I share stuff with you, I feel much better.")
	fnDialogue("Todd: [E|Blush]... Pfff, well. I'm glad to hear that.[P][P][P] But you don't really share [ITALICS]everything[ENDITALICS] with me, do you?")
	
	--Save the game.
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Starting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
	PairanormalLevel_FlagCheckpoint("Starting")
    
	--What to say to Todd:
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (What should I say?)") ]])

	local sDecisionScript = "\"" .. gsRoot .. "../Chapter Walk/Walkhome Todd 0.lua\""
	fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"I can read minds\", " .. sDecisionScript .. ", \"PsychoMantis\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"How you feel about him\", " .. sDecisionScript .. ", \"LovingMantis\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"I guess you're right\",  " .. sDecisionScript .. ", \"ShyMantis\") ")
	fnCutsceneBlocker()

--You like Castlevania!
elseif(sTopicName == "PsychoMantis") then

    --Flags.
    VM_SetVar("Root/Variables/System/Player/iReadToddsMind", "N", 1.0)

	--Dialogue.
	fnDialogue("Thought: Todd's right, or at least he was until now.[P] I [ITALICS]had[ENDITALICS] been keeping s[GLITCH]omething from him. [P]There's only one thing I can think to say.")
	fnDialogue("Player: Todd.[P] [CRASH]I'm a mind reader!")
	fnDialogue("Todd:[E|Shock] .[P].[P].[P][E|Happy] PF'HAHAHAHAHA! Nice.")
	fnDialogue("Player: I-It's not funny! [P]At least, I think it isn't.")
	fnDialogue("Todd: [E|Neutral]All right, go on.")
	fnDialogue("Player: Huh?")
	fnDialogue("Todd: [E|Neutral]You're a mind reader. So, what am I thinking?")
	fnDialogue("Player: Well. Uh, it doesn't really work like that. [P]It's more of a memory-type[GLITCH] thing... ")
	fnDialogue("Player: Like a viewfinder that I can sort of [GLITCH]jump in and...[P][P] you know what?[P] Screw it.[GLITCH][GLITCH][P] Here I go.")
    
	--Save the game.
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Admission\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
	PairanormalLevel_FlagCheckpoint("Admission")
    
    --Dialogue.
	fnDialogue("[HIDEALL][BG|Null]Thought: ...")
	fnDialogue("[BG|Root/Images/Backgrounds/All/Cafe]Thought: I'm slumped, or I guess Todd is, in the deep cushions of some cafe couch.")
	fnDialogue("Thought: Around me are all the members of the Associated Student Body,[P] their eyes unwaveringly wide after half an hour's discussion of the same miserable topic.")
	fnDialogue("Thought: This was supposed to be a planning committee for the upcoming Fall Formal Fundraiser--which means free tea and doughnuts,[P] courtesy of Foster High.")
	fnDialogue("Thought: [SFX|World|Ugh] But it's getting late, and we haven't even covered the venue.")
	fnDialogue("[CHARENTER|Student1][CHARENTER|Student4][CHARENTER|Harper][EMOTION|Harper|Sad][CHARENTER|Student3][CHARENTER|Student2]Student: [FOCUS|Student1][VOICE|Student1]I can't believe she would break up with [ITALICS]you.[ENDITALICS][P] TODD.[P][P] [ITALICS]The[ENDITALICS] Todd!!")
	fnDialogue("Student: [FOCUS|Student3][VOICE|Student3]Well that explains why she suddenly dropped out of ASB.")
	fnDialogue("Student: [FOCUS|Student4][VOICE|Student4]I saw her giving her number to Loren a couple weeks ago,[P] but like,[P] I didn't think anything of it,[P] and now I'm like thinking--should I have said something?")
	fnDialogue("Student: [FOCUS|Student1][VOICE|Student1]You're literally the [ITALICS]hottest[ENDITALICS] guy.[P] WHO DROPS the TODDMEISTER?")
	fnDialogue("Thought: I'm surprised Harper hasn't asked me anything, let alone said a word. [P]I only brought the whole subject up because I'd been dying to tell him.")
	fnDialogue("Thought: I need to hear something, anything he has to say.[P] But all he's done is nod and listen.")
	fnDialogue("Student: [FOCUS|Student2][VOICE|Student2]So like, what are you going to do for the Formal? [P]I thought you guys were going together.")
	fnDialogue("Player: [VOICE|Voice|Todd]Not anymore.[P] I mean, I hadn't even asked her yet.")
	fnDialogue("Player: [VOICE|Voice|Todd]I mean, I was going all out with the surprise, you know--[P] I even bought her some escargot from this really fancy French restaurant.")
	fnDialogue("Player: [VOICE|Voice|Todd][CRASH]I paid 60 Bucks for a bucket of snails!!!")
	fnDialogue("Student:[FOCUS|Student1][VOICE|Student1] [P][ITALICS]God.[ENDITALICS]")
	fnDialogue("Student:[FOCUS|Student4][VOICE|Student4] Sixty.[P] Whole.[P] Dollars!")
	fnDialogue("Student:[FOCUS|Student3][VOICE|Student3] I have a bunch of those in my garden, you could've just picked 'em from there.")
	fnDialogue("Player: [VOICE|Voice|Todd]They're Ana-Leah's favorite food. --See, that's what I'm talking about.")
	fnDialogue("Player: [VOICE|Voice|Todd]What the heck is LOREN gonna do for her invitation that'll frickin' top that?")
	fnDialogue("Player: [VOICE|Voice|Todd]I didn't DO anything wrong, [P]I gave her all the care and attention she needed,[P] and she[P] STILL [P]cheats on me.")
	fnDialogue("Player: [VOICE|Voice|Todd]I-I just...[P] don't understand why she doesn't want to be with [ITALICS]me[ENDITALICS].")
	fnDialogue("Student:[FOCUS|Student2][VOICE|Student2] Dude. You gotta get her BACK, bro![P] Invite her to the formal ANYWAY.")
	fnDialogue("Student:[FOCUS|Student4][VOICE|Student4] Omigod yaaaas!")
	fnDialogue("Player: [VOICE|Voice|Todd]Really? Do you think I should go for it?")
	fnDialogue("Student:[FOCUS|Student1][VOICE|Student1] OR get with someone else. Show that missy what she is missing, know what I mean?")
	fnDialogue("Student:[FOCUS|Student4][VOICE|Student4] Yyyyaaaaaaas!")
	fnDialogue("Student:[FOCUS|Student1][VOICE|Student1] [ITALICS] I would like to nominate myself for the role of pretend boyfriend, which as you know in the movies always ends with me turning into the REAL boyfriend.")
	fnDialogue("Student:[FOCUS|Student4][VOICE|Student4] Gaga, ooh-la-la, want your bad rommmmance!")
	fnDialogue("Player: [VOICE|Voice|Todd]I'm gonna pass on that particular plan, Gabriel, but maybe if I--")
	fnDialogue("Thought: Harper finally moves, snapping forward so hard the tea on our coffee table rattles in its kettle.")
	fnDialogue("Harper: [E|Sad]Todd, honey.[P] I'm sorry, but like,[P] [CRASH][E|Shock]WHAT?")
	fnDialogue("Player: [VOICE|Voice|Todd][P][P][P][SFX|Blink]What?")
	fnDialogue("Harper: [E|Shock]You... You're acting like it's some great tragedy that you and Ana-Leah aren't together anymore.")
	fnDialogue("Harper: But frankly, you ES-[P]CAPED. You dodged a bull-et!")
	fnDialogue("Student:[FOCUS|Student4][VOICE|Student4] Ooh, tell it girl!")
	fnDialogue("Harper: [E|Mad] You're not the problem, sweetie, SHE.[P] IS.")
	fnDialogue("Student:[FOCUS|Student4][VOICE|Student4] yyyyaaaaaaas!")
	fnDialogue("Harper: [E|Mad] Christine, not now girl.[P] Todd.[P][E|Shock] I can't believe you're not even... angry???")
	fnDialogue("Player: [VOICE|Voice|Todd]Tuhh.[P] Excuse me?")
	fnDialogue("Harper: [E|Sad]I'm sorry.[P] I just don't understand...[P]do you seriously still wanna get back with her?")
	fnDialogue("Player: [VOICE|Voice|Todd]Of course I do--[P]I mean... I don't know--")
	fnDialogue("Harper: [E|Mad][CRASH]SHE'S LITERALLY THE WORST![P] Like, house Sly-the-rin without a DOUBT.[P] Who the hell's damn favorite food is snails????")
	fnDialogue("Player: [VOICE|Voice|Todd]Uh.[P].. well this is a lot of talk coming from someone who had nothing but nice things to say about her right up until the end!")
	fnDialogue("Thought: The other members clear their throats uncomfortably, but Harper is undeterred,[P] his mouth set in a flat line.")
	fnDialogue("Harper: [E|Sad]*sigh*... Well, I was lying.")
	fnDialogue("Harper: Ana-Leah is kinda a bitch, and I'm sorry I didn't say anything before,[P][E|Neutral] but you had already made up your mind an-y-way,[P] hadn't you?")
	fnDialogue("Player: [VOICE|Voice|Todd]Puh! And you're yelling at [ITALICS]me[ENDITALICS] about this?[P] Some friend you are, dude.")
	fnDialogue("Harper: [E|Happy]No, Todd, some friend YOU are.[P] Do you wanna know the real rea-[P]son why I didn't speak up before?")
	fnDialogue("Harper: [E|Sad] Because I know she would've made you choose between her and us, and WE???[P] would-[P]have-[P]lost.")
	fnDialogue("Player: [VOICE|Voice|Todd]That's not true![P] If you had some kind of disagreement with her, and you had just told me the [ITALICS]truth[ENDITALICS],[P] I would've just hung out with you guys seperately.")
	fnDialogue("Harper: [CRASH][E|Mad]JE-[P]SUS[P] CHRIST!!![P] THIS. [SPEEDTEXT]Is what I am talking about. You lit-er-a-lly just admitted you would ignore me if I--")
	fnDialogue("Player: [VOICE|Voice|Todd][SPEEDTEXT]I wouldn't ignore you--")
	fnDialogue("Harper: [E|Mad][SPEEDTEXT]YOU WOULD IG-NORE ME and any complaint I had, so you could go off gallavanting--")
	fnDialogue("Player: [VOICE|Voice|Todd][SPEEDTEXT]--can't believe you are twisting what I said when I'm the one who was--")
	fnDialogue("Harper: [E|Mad][SPEEDTEXT]--and we don't even SEE you on the weekends, and you are STILL defending her--")
	fnDialogue("Player: [VOICE|Voice|Todd][SPEEDTEXT]--when I needed you the most, you just sat silently and [ITALICS]judged me[ENDITALICS]--")
	fnDialogue("Harper: [E|Mad][SPEEDTEXT]--AND SHE [ENDSPEEDTEXT][CRASH]CHEATED ON YOU![P] AND YOU ARE YELLING AT YOUR [CRASH]FRIEND RIGHT NOW.")
	fnDialogue("Harper: She's not the moron, Todd.[P][E|Sad] You are.")
	fnDialogue("Player: [VOICE|Voice|Todd][ITALICS]Ffffuck[ENDITALICS] you, dude. I'm leaving. ")
	fnDialogue("Harper: [E|Mad]BOY, BYE!!!")
	fnDialogue("[HIDEALL][BG|Null]Thought: I storm off, and thank goodness I didn't forget to grab my backpack before leaving. I'd rather have abandoned it than face the embarrassment of coming back.")
	fnDialogue("[BG|Root/Images/Backgrounds/All/SidewalkNight]Thought: Harper had a reputation for snapping off at people who rubbed him the wrong way,[P] but being on the receiving end was devastating.")
	fnDialogue("Thought: How could he do this to me, when I've been basically trampled on?")
	fnDialogue("Player: [VOICE|Voice|Todd]Doesn't anyone care about me?[P][CRASH] Anyone at all? ")
	fnDialogue("Thought: [P][P]I've never been much of a crier, so I'm grateful to be far away from the cafe, where no one can see my eyeballs at their worst.")
	fnDialogue("Thought: I wander for a couple of hours waiting for the stinging to die down,[P] not caring where I end up, or that I should be home.")
	fnDialogue("Player: [VOICE|Voice|Todd]Ana-Leah. [P]*sniff*. [P]Fucking Ana Fucking Leah. ")
	fnDialogue("Thought: We spent so much time together--I even tutored her after school[P] (though admittedly we didn't get much schoolwork done).")
	fnDialogue("Player: [VOICE|Voice|Todd]Heh. [P]*sniff* [P]Almost makes you wonder when she even found, like,[P] the [ITALICS]time[ENDITALICS] to cheat on me.")
	fnDialogue("Thought: My voice bounces off dark houses into empty air.[P] The ASBees would've found that funny.")
	fnDialogue("Thought: I thought it up last night, just after the breakup. Two hours I begged her not to do it, but in the end she just... hung up.")
	fnDialogue("Thought: I wanted to tell everyone how mad I was.[P] Harper was supposed to say something funny and biting and I would have felt the tiniest bit better.")
	fnDialogue("Thought: But I didn't mention it, because...[P] I was too ashamed.")
	fnDialogue("Thought: I knew Harper didn't like her. [P]I always knew.[P] I just ignored it, hoping it would go away.")
	fnDialogue("Player: [VOICE|Voice|Todd]*sigh*...[P] I [ITALICS]am[ENDITALICS] the moron, aren't I?")
	fnDialogue("[CHARENTER|Harper]Harper: [E|Sad][P][P]Todd.")
	fnDialogue("Player: [VOICE|Voice|Todd][SFX|World|Blink]H-Harper!")
	fnDialogue("Thought: I guess I'd wandered home.[P] My cheeks burned, but before I could open my mouth--")
	fnDialogue("Harper: [E|Blush]You left your phone at the cafe.")
	fnDialogue("Player: [VOICE|Voice|Todd]...[P] [SFX|World|Ugh][ITALICS]Damn it[ENDITALICS].")
	fnDialogue("Harper: [E|Blush]...[P][P]heheh...")
	fnDialogue("Player: [VOICE|Voice|Todd]PF'HAHAHAHAHAHA!")
	fnDialogue("Harper: [E|Happy]AHAHAHAHAHA!")
	fnDialogue("Thought: I take it from his outstretched hand, and we laugh it all out.[P] We laugh till we can't breathe.[P] Then Harper gives me a hug, and I bury my last tear into his shoulder.")
	fnDialogue("Harper: [E|Blush]I'm sorry, Todd.[P] I was trying to splash a little tea, but I just splashed hot steaming piss everywhere instead.")
	fnDialogue("Player: [VOICE|Voice|Todd]It's fine, it's fine.")
	fnDialogue("Harper: [E|Shock] NO,[P] I was so mean to you,[P] and you had just been [E|Mad]hurt by that escargo-sucking [ITALICS]bitch.[ENDITALICS] ")
	fnDialogue("Player: [VOICE|Voice|Todd]Heh.")
	fnDialogue("Harper: [E|Shock]You're my BEST [P]FRIEND.[P][P][E|Sad] I thought... I just didn't want that to change.")
	fnDialogue("Thought: I look him squarely in the eye.")
	fnDialogue("Player: [VOICE|Voice|Todd]Never, dude.[P] Seriously, never.")
	fnDialogue("Thought: He sighs, a tired but satisfied smile on his face.")
	fnDialogue("Harper: [E|Neutral]You hungry?[P] I think there's still some leftover pancake mix in your fridge, right? ")
	fnDialogue("Player: [VOICE|Voice|Todd]Heeeeeck yeah,[P] midnight pancakes!")
	fnDialogue("[HIDEALL][BG|Null]Thought: I let us into the warm house--it feels like a volley of arrows soared past me, and somehow I miraculously survived.")
	fnDialogue("Thought: I don't know who I'm gonna end up with, but I'm sure never gonna let them get in the way of a good-ass midnight pancake.")
	fnDialogue("Thought: And the friends that make 'em.[P] Mm.")
    
	--Save the game.
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"NiceJob\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
	PairanormalLevel_FlagCheckpoint("NiceJob")
    
    --Dialogue.
	fnDialogue("Thought: ...")
	fnDialogue("[CHARENTER|Todd][BG|Root/Images/Backgrounds/All/SidewalkNight]Player: *ahem*.[P] Well, see you around, Todd.")
	fnDialogue("Todd: [E|Shock]Are you sure you're okay?[P][E|Mad] You got a little woozy back there.")
	fnDialogue("Player: Ah, happens all the time.[P] I need to eat more vegetables or something.")
	fnDialogue("Thought: In the end, I couldn't tell my tutor about what I'd seen, and he laughed off the whole thing like it was a joke.")
	fnDialogue("Thought: I feel even more distant from Todd now.[P] We'd spent so much time working and focusing on [ITALICS]me[ENDITALICS]...")
	fnDialogue("Thought: I never really considered what he worries about in life. ")
	fnDialogue("Todd: Well, if you're sure...[P]take it easy, all right?")
	fnDialogue("Player: Yeah.[P] Thanks for your help today.")

    --Rejoin.
    LM_ExecuteScript(LM_GetCallStack(0), "Rejoin")

--Tell Todd you like him.
elseif(sTopicName == "LovingMantis") then

	--Dialogue.
	fnDialogue("Player: Sure I do.[P] I... I think it's really easy to talk to you.")
	fnDialogue("Todd: [E|Neutral]Heh.")
	fnDialogue("Player: And... I don't see anything wrong with saying I like your face, a lot.")
	fnDialogue("Player: I mean, I like to look at it.[P] And I get really excited when I make you laugh.")
	fnDialogue("Todd: [E|Blush]... O-Oh?")
	fnDialogue("Player: And...[P]I think it's annoying when you use big words. I don't really see the point,[P] since I don't know what they mean.")
	fnDialogue("Player: Makes me feel kind of dumb.")
	fnDialogue("Todd: [E|Shock]Oh.[P] *ahem*[P][E|Sad] sorry.")
	fnDialogue("Player: .[P].[P].[P][|SFX|Ugh]Oh, I see what you mean about being honest all the time--[P]I just hurt your feelings, didn't I?")
	fnDialogue("Todd: [E|Sad]Hrm.[P] Yeah, haha, maybe a little.")
	fnDialogue("Todd: [E|Neutral] But at the very least, I'm more aware of my word usage around you.")
	fnDialogue("Todd: ... Can I be honest with you now,[P] [PLAYERNAME]?")
	fnDialogue("Player: *gulp* I... suppose it's only fair.")
	fnDialogue("Todd: [E|Neutral]I[P] also like to look at your face.[P][E|Blush] And make you laugh.[P][E|Happy] And you're very perceptive, even though you don't know too much about life. ")
	fnDialogue("Player: Wow.[P] I-- ")
	fnDialogue("Todd: [E|Blush]And... Uh, nothing,[P] nevermind.")
	fnDialogue("Thought: The way Todd crosses his arms and shifts his shoulders uncomfortably gives me the sense what he was going to say would've hurt [ITALICS]my[ENDITALICS] feelings...")
	fnDialogue("Thought: ... And for that reason I don't press him on it.[P] Maybe it is best to just keep things to ourselves sometimes.")
	fnDialogue("Thought: We gather our things quietly and slowly shuffle to the door.")
	fnDialogue("[HIDEALL][BG|Null]Thought: ...")
	fnDialogue("[CHARENTER|Todd][BG|Root/Images/Backgrounds/All/SidewalkNight]Player: Well, aside from this strange final interaction...[P]I had a great time today, Todd.[P] Thanks for the math lesson.")
	fnDialogue("Todd: [E|Shock]Right, of course![P][E|Neutral] Yo, you're doing great in your studies, [PLAYERNAME].[P] Seriously, keep it up. ")

    --Rejoin.
    LM_ExecuteScript(LM_GetCallStack(0), "Rejoin")

--Cowardice.
elseif(sTopicName == "ShyMantis") then
   
	--Dialogue.
	fnDialogue("Thought: Let's see, there's the whole secret organization that I managed to get myself thrown into, the fact that I can read minds and talk to my dog...")
	fnDialogue("Thought: Oh yeah, and the warm tingly feeling that makes me want to melt anytime he so much as looks at me?[P] Where could I possibly begin?")
	fnDialogue("Player: *gulp* I...[P]I suppose not.")
	fnDialogue("Todd: [E|Neutral]See?[P][E|Blush] I'm not saying keeping secrets is like, [ITALICS]good[ENDITALICS] or anything, but in general people kind of choose their audience for their secrets.")
	fnDialogue("Todd: [E|Happy] Or, I dunno, just get a therapist.[P] Pf'hah!")
	fnDialogue("[HIDEALL]Thought: I nod, but my back slumps petulantly.")
	fnDialogue("Thought: Of course I WANT to tell Todd about all the things going in my head.[P] I don't feel ashamed or anything...")
	fnDialogue("Thought: But I kind of get the feeling that my tutor might not accept me if he knew.[P] After all, I can barely accept it all myself.")
	fnDialogue("Thought: We gather our things quietly and slowly shuffle to the door.")
	fnDialogue("[HIDEALL][BG|Null]Thought: ...")
	fnDialogue("[CHARENTER|Todd][BG|Root/Images/Backgrounds/All/SidewalkNight]Player: *sigh*[P] I guess I'd better be going home.")
	fnDialogue("Todd: Right, of course! Well, uh, you're doing great in your studies, [PLAYERNAME].[P] Seriously, keep it up. ")
	fnDialogue("Player: Yeah, thanks for the lessons. ")

    --Rejoin.
    LM_ExecuteScript(LM_GetCallStack(0), "Rejoin")

--Rejoin.
elseif(sTopicName == "Rejoin") then

	--Dialogue.
	fnDialogue("Thought: He stiffly pats me on the shoulder.[P] There's nothing left to do but turn tail and and start the long walk home.")
	fnDialogue("[HIDEALL][BG|Null]Thought: ...")
    
    --Which chapter to jump to.
    local iChapterDestination = VM_GetVar("Root/Variables/System/Player/iChapterDestination", "N")
    if(iChapterDestination == 3.0) then
        LM_ExecuteScript(gsRoot .. "../Chapter 3/Gameplay_SceneD/2 - Arrive At Home.lua", "Start")
        
    elseif(iChapterDestination == 4.0) then
        LM_ExecuteScript(gsRoot .. "../Chapter 4/Gameplay_SceneD/0 - Walk Home.lua", "LimoScene")
    end
end
