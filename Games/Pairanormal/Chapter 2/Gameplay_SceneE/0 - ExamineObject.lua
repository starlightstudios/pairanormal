--[Investigation of the Classroom]
--The first investigation sequence in the classroom.

--[Arguments]
--Get the name of what we're investigating.
local sExamine = LM_GetScriptArgument(0)

--[Variable Setup]
--Used to simplify the Add Discovered Object calls.
local sBGPath = "Root/Images/Backgrounds/All/Library"
local sScriptPath = gsRoot .. "../Chapter 2/Gameplay_SceneE/0 - ExamineObject.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
local sAutoAdvance = gsRoot .. "../Chapter 2/Gameplay_SceneB/8 - PatchGoingHome.lua"

--Saving
local sSaveExecute = gsRoot .. "../Chapter 2/Gameplay_SceneB/0 - See You At The Club.lua"
local sSaveTopic = "No Topic"
local sSaveFlag = "Investigation"

--[Function Setup]
local fnStdObject = function(iIndex, sLine, sSaveFlag, sSaveExecute, sSaveTopic)
	
    --Argument check.
    if(iIndex == nil) then return end
    
	--Store it in the inventory.
	local i = iIndex
	Dia_SetProperty("Add Discovered Object", gczaObjD[i][1], sBGPath, sScriptPath, gczaObjD[i][1])
	local iExaminedAlready = VM_GetVar("Root/Variables/Chapter2/InvestigationB/iObject|" .. gczaObjD[i][1], "N")
	VM_SetVar("Root/Variables/Chapter2/InvestigationB/iObject|" .. gczaObjD[i][1], "N", 1.0)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction(sLine)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(iExaminedAlready == 0.0) then
		if(fnDecrementTime(1, sAutoAdvance) == false) then
			PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
		end
	end
    
end

--[Examinable Objects]
if(sExamine == "Romance") then
    
	--Store it in the inventory.
	local i = gciInvD_Romance
	Dia_SetProperty("Add Discovered Object", gczaObjD[i][1], sBGPath, sScriptPath, gczaObjD[i][1])
	local iExaminedAlready = VM_GetVar("Root/Variables/Chapter2/InvestigationB/iObject|" .. gczaObjD[i][1], "N")
	VM_SetVar("Root/Variables/Chapter2/InvestigationB/iObject|" .. gczaObjD[i][1], "N", 1.0)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: [ITALICS]'Teh Priate Socrundel'[ENDITALICS]") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: On the cover is a bronze, shirtless man aggressively groping a swooning lady in a billowy white dress.[P] Poor lady!") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(iExaminedAlready == 0.0) then
		if(fnDecrementTime(1, sAutoAdvance) == false) then
			PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
		end
	end
    
elseif(sExamine == "Alien") then
    
	--Store it in the inventory.
	local i = gciInvD_Alien
	Dia_SetProperty("Add Discovered Object", gczaObjD[i][1], sBGPath, sScriptPath, gczaObjD[i][1])
	local iExaminedAlready = VM_GetVar("Root/Variables/Chapter2/InvestigationB/iObject|" .. gczaObjD[i][1], "N")
	VM_SetVar("Root/Variables/Chapter2/InvestigationB/iObject|" .. gczaObjD[i][1], "N", 1.0)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: [ITALICS]'Alenis! It Hppaneed to Me: Ture Adubctoin Sotreis'[ENDITALICS]") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: Funny looking green guy on the cover.[P] What the heck is this book about?") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(iExaminedAlready == 0.0) then
		if(fnDecrementTime(1, sAutoAdvance) == false) then
			PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
		end
	end
    
elseif(sExamine == "DandDee") then
    
	--Store it in the inventory.
	local i = gciInvD_Dandee
	Dia_SetProperty("Add Discovered Object", gczaObjD[i][1], sBGPath, sScriptPath, gczaObjD[i][1])
	local iExaminedAlready = VM_GetVar("Root/Variables/Chapter2/InvestigationB/iObject|" .. gczaObjD[i][1], "N")
	VM_SetVar("Root/Variables/Chapter2/InvestigationB/iObject|" .. gczaObjD[i][1], "N", 1.0)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: [ITALICS]'Dugneons Adn Dargnos'[ENDITALICS]") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: The book has a cool illustration of two fantasy creatures.[P] It's a little older than other books here,[P] and well-worn.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(iExaminedAlready == 0.0) then
		if(fnDecrementTime(1, sAutoAdvance) == false) then
			PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
		end
	end
    
elseif(sExamine == "Computer") then
    
	--Store it in the inventory.
	local i = gciInvD_Computer
	Dia_SetProperty("Add Discovered Object", gczaObjD[i][1], sBGPath, sScriptPath, gczaObjD[i][1])
	local iExaminedAlready = VM_GetVar("Root/Variables/Chapter2/InvestigationB/iObject|" .. gczaObjD[i][1], "N")
	VM_SetVar("Root/Variables/Chapter2/InvestigationB/iObject|" .. gczaObjD[i][1], "N", 1.0)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: This must be the new computer lab I've been hearing so much about.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: Doesn't look quite ready, but it should only be a matter of time now.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(iExaminedAlready == 0.0) then
		if(fnDecrementTime(1, sAutoAdvance) == false) then
			PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
		end
	end
    
elseif(sExamine == "Paranormal") then
    
	--Store it in the inventory.
	local i = gciInvD_Paranormal
	Dia_SetProperty("Add Discovered Object", gczaObjD[i][1], sBGPath, sScriptPath, gczaObjD[i][1])
	local iExaminedAlready = VM_GetVar("Root/Variables/Chapter2/InvestigationB/iObject|" .. gczaObjD[i][1], "N")
	VM_SetVar("Root/Variables/Chapter2/InvestigationB/iObject|" .. gczaObjD[i][1], "N", 1.0)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: Pa..r..a...no...r..mal...[P]K-s-kl...cl..u..b.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: Hey![P] I know those guys.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(iExaminedAlready == 0.0) then
		if(fnDecrementTime(1, sAutoAdvance) == false) then
			PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
		end
	end
    
elseif(sExamine == "OldComputer") then
    
	--Store it in the inventory.
	local i = gciInvD_OldComputer
	Dia_SetProperty("Add Discovered Object", gczaObjD[i][1], sBGPath, sScriptPath, gczaObjD[i][1])
	local iExaminedAlready = VM_GetVar("Root/Variables/Chapter2/InvestigationB/iObject|" .. gczaObjD[i][1], "N")
	VM_SetVar("Root/Variables/Chapter2/InvestigationB/iObject|" .. gczaObjD[i][1], "N", 1.0)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: I understand these computers have been here for a long time.[P] I wonder what'll happen to them when the new lab is open.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(iExaminedAlready == 0.0) then
		if(fnDecrementTime(1, sAutoAdvance) == false) then
			PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
		end
	end
    
elseif(sExamine == "Checkout") then
    
	--Store it in the inventory.
	local i = gciInvD_Checkout
	Dia_SetProperty("Add Discovered Object", gczaObjD[i][1], sBGPath, sScriptPath, gczaObjD[i][1])
	local iExaminedAlready = VM_GetVar("Root/Variables/Chapter2/InvestigationB/iObject|" .. gczaObjD[i][1], "N")
	VM_SetVar("Root/Variables/Chapter2/InvestigationB/iObject|" .. gczaObjD[i][1], "N", 1.0)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: Here's where I can check out books from the library.[P] Since most books are full of words, I haven't made much use of it.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(iExaminedAlready == 0.0) then
		if(fnDecrementTime(1, sAutoAdvance) == false) then
			PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
		end
	end
    
elseif(sExamine == "TV") then
    
	--Store it in the inventory.
	local i = gciInvD_TV
	Dia_SetProperty("Add Discovered Object", gczaObjD[i][1], sBGPath, sScriptPath, gczaObjD[i][1])
	local iExaminedAlready = VM_GetVar("Root/Variables/Chapter2/InvestigationB/iObject|" .. gczaObjD[i][1], "N")
	VM_SetVar("Root/Variables/Chapter2/InvestigationB/iObject|" .. gczaObjD[i][1], "N", 1.0)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: That's a TV.[P] The pairanormal club seemed pretty surprised that I didn't have one in my house.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(iExaminedAlready == 0.0) then
		if(fnDecrementTime(1, sAutoAdvance) == false) then
			PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
		end
	end
    
elseif(sExamine == "School") then
    
	--Store it in the inventory.
	local i = gciInvD_School
	Dia_SetProperty("Add Discovered Object", gczaObjD[i][1], sBGPath, sScriptPath, gczaObjD[i][1])
	local iExaminedAlready = VM_GetVar("Root/Variables/Chapter2/InvestigationB/iObject|" .. gczaObjD[i][1], "N")
	VM_SetVar("Root/Variables/Chapter2/InvestigationB/iObject|" .. gczaObjD[i][1], "N", 1.0)
	Dia_SetProperty("Clear Discovered Object")
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST][IMAGE|Root/Images/MajorScenes/All/CH2_PictureFoster]Thought: Looks like an old photo of the school, when it was being built. ") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: The man in the foreground looks familiar...") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought:[IMAGE|Null] Probably my imagination.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(iExaminedAlready == 0.0) then
		if(fnDecrementTime(1, sAutoAdvance) == false) then
			PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
		end
	end
    
elseif(sExamine == "Library") then
    
	--Store it in the inventory.
	local i = gciInvD_Library
	Dia_SetProperty("Add Discovered Object", gczaObjD[i][1], sBGPath, sScriptPath, gczaObjD[i][1])
	local iExaminedAlready = VM_GetVar("Root/Variables/Chapter2/InvestigationB/iObject|" .. gczaObjD[i][1], "N")
	VM_SetVar("Root/Variables/Chapter2/InvestigationB/iObject|" .. gczaObjD[i][1], "N", 1.0)
	Dia_SetProperty("Clear Discovered Object")
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST][IMAGE|Root/Images/MajorScenes/All/CH2_PictureArroyo]Thought: There's old photos of the library here. High schoolers excitedly unpacking computers from boxes.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: [IMAGE|Null]I guess back in the day they must have been new.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(iExaminedAlready == 0.0) then
		if(fnDecrementTime(1, sAutoAdvance) == false) then
			PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
		end
	end
    
elseif(sExamine == "Security") then
    
	--Store it in the inventory.
	local i = gciInvD_Security
	Dia_SetProperty("Add Discovered Object", gczaObjD[i][1], sBGPath, sScriptPath, gczaObjD[i][1])
	local iExaminedAlready = VM_GetVar("Root/Variables/Chapter2/InvestigationB/iObject|" .. gczaObjD[i][1], "N")
	VM_SetVar("Root/Variables/Chapter2/InvestigationB/iObject|" .. gczaObjD[i][1], "N", 1.0)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: There's a few of these peppered around the school, supposedly for security reasons.[P] I wonder who's watching on the other side?") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(iExaminedAlready == 0.0) then
		if(fnDecrementTime(1, sAutoAdvance) == false) then
			PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
		end
	end
    
--[Laura]
elseif(sExamine == "Laura") then

    --Object dimensions.
	Dia_SetProperty("Add Discovered Object", "Laura", sCharPath, sScriptPath, "Laura", 882, 2, 882 + 438, 2 + 363)
    
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: There's Laura, working away at one of the old school computers.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: She keeps giggling to herself--[P] wonder what she's looking at?") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()

	--Saving.
	PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    
--[Todd]
elseif(sExamine == "Todd") then

    --Object dimensions.
	Dia_SetProperty("Add Discovered Object", "Todd", sCharPath, sScriptPath, "Todd", 2, 367, 2 + 438, 367 + 363)
    
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: Harper is here busy affixing a 'welcome to the lab' sign over the new computer lab's entrance.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: Todd's helping him out. They're probably too busy to talk.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()

	--Saving.
	PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    
--[Ross]
elseif(sExamine == "Ross") then

    --Object dimensions.
	Dia_SetProperty("Add Discovered Object", "Ross", sCharPath, sScriptPath, "Ross", 882, 367, 882 + 438, 367 + 363)
    
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: It's the slacker twin, seated behind the checkout counter, reading one of the books stacked around them.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: Heh, I bet this is what passes for 'working'.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()

	--Saving.
	PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    
--[Terry]
elseif(sExamine == "Terry") then

    --Object dimensions.
	Dia_SetProperty("Add Discovered Object", "Terry", sCharPath, sScriptPath, "Terry", 1332, 367, 1332 + 438, 367 + 363)
    
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: Of course the more responsible twin is amongst the shelves, reorganizing books that have been left on the floor.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: That one always has a no-nonsense look about them.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()

	--Saving.
	PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)

end