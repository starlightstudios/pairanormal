--[Ross Presentation]
--Showing stuff to Ross.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--Variables.
local sScriptPath = gsRoot .. "../Chapter 2/Gameplay_SceneE/1 - Present Laura.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
local sAutoAdvance = gsRoot .. "../Chapter 2/Gameplay_SceneB/8 - PatchGoingHome.lua"

--Saving
local sSaveExecute = gsRoot .. "../Chapter 2/Gameplay_SceneB/5 - A Body In the Library.lua"
local sSaveTopic = "No Topic"
local sSaveFlag = "Investigation"

--[Chat Sequences]
--Chatting.
if(sTopicName == "Chat") then
	
	--Variables.
	local iChattedWithRoss = VM_GetVar("Root/Variables/Chapter2/Investigation/iChattedWithRoss", "N")
	
	--First pass:
	if(iChattedWithRoss == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter2/Investigation/iChattedWithRoss", "N", 1.0)
		
		--Text.
		fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
		fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
		fnCutsceneBlocker()
		fnDialogue([[[HIDEALLFAST][CHARENTER|Ross]Player: Hey you're that twin![P] Ross, right?]])
		fnDialogue([[Ross: [E|Happy]Yaaaaas![P] What's up my good dude?]])
		fnDialogue([[Player: Not much.]])
		fnDialogue([[Ross: [E|Shock]By the way, is it cool if I call you dude?[P] I use it indiscriminately as a gender neutral term, but some people don't like it.]])
		fnDialogue([[Player: Oh, really? I didn't know.[P] Um, it's fine.]])
		fnDialogue([[Ross: [E|Neutral]Righteous.]])
		fnDialogue([[Ross: You can call me a dude too by the way.[P] Or a dudette, I don't mind.[P][E|Shock] Sometimes people get confused.]])
		fnDialogue([[Player: You've got it, dude.[P] So, what are you doing here?]])
		fnDialogue([[Ross: [E|Happy]Weeeell, yours truly and company --]])
		fnDialogue([[Thought: He gestures across the library to his twin, sorting books on the shelves.]])
		fnDialogue([[Ross: [E|Neutral]We work here after school.[P][E|Shock] Sometimes we sort shelves, sometimes we take on more custodial duties.]])
		fnDialogue([[Ross: [E|Neutral] The job doesn't pay at all, buuuuuut--]])
		fnDialogue([[Thought: He lifts his hand from under the desk, producing a large ring of keys.[P] I suspect they go to everything in the school.]])
		fnDialogue([[Ross: [E|Happy] There's definitely some perks.]])
		fnDialogue([[Player: Righteous.]])
		fnDialogue([[Ross: [E|Neutral] MM-HM! Let me know if you want to check out a book or something.[P] Or--]])
		fnDialogue([[Thought: He jangles the keys enticingly.]])
		fnDialogue([[Ross: [E|Happy]You want the homie to hook you up with a free drink from the vending machine!]])
		fnDialogue([[Player: That's very generous of you--]])
		fnDialogue([[Thought: I can't help but wince as I'm reminded of the judgmental onlookers this morning.]])
		fnDialogue([[Player: --Although I don't think I'll ever be drinking something from the vending machine again.]])
		fnDialogue([[Thought: He snorts, breaking into a goofy grin. I don't find it all that funny.]])
		fnDialogue([[Ross: [E|Happy]Ooh, sounds like someone got the Kane Kola Cancel.]])
		fnDialogue([[Ross: [E|Sad] I could get you a snack instead if you want.[P][E|Neutral] I just love to redistribute wealth for the public good. ]])
		fnDialogue([[Player: I'll keep it in mind.[P] See you around, Ross.]])
		fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
		fnCutsceneBlocker()
	
        --Time loss.
        if(fnDecrementTime(5, sAutoAdvance) == false) then
            PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
        end
		
	--Repeat passes.
	else
		fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
		fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
		fnCutsceneBlocker()
		fnDialogue("[HIDEALLFAST][SHOWCHAR|Ross|Neutral]Ross: [E|Neutral]'Sup bro.[P][E|Neutral]Let me know if you've got any questions!")
		fnDialogue("Player: Will do.")
		fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
		fnCutsceneBlocker()
	end
	
--[Objects]
elseif(sTopicName == "Romance") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Ross]Ross: [E|Mad]Hey man, don't make fun of trashy romance novels.")
    fnDialogue("Ross: [E|Sad]Sure, a lot of times the plot is predictable and everything's wrapped up too nicely,[E|Blush] but like, isn't that the whole point of reading 'em?")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "Alien") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Ross]Ross: [E|Mad]Aliens.[P] Are.[P][P] Real. ")
    fnDialogue("Player: You say that with a lot of conviction.")
    fnDialogue("Ross: [E|Neutral]Everyone in Foster Falls knows they landed in the old crater at the edge of town back in the 50s, and they're probably living among us wearing human flesh, [P]dude.")
    fnDialogue("Player: Really?[P] Everyone [ITALICS]knows[ENDITALICS] that?")
    fnDialogue("Ross: [E|Blush]Okay,[P] so not everyone is on board with the theory, but like,[P][E|Mad] I am [ITALICS]very[ENDITALICS] suss about anyone who tries to deny it.")
    fnDialogue("Ross: [E|Neutral]They might be a flesh-wearer, after all.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "DandDee") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Ross]Ross: [E|Blush]I prefer video games to RPGs, honestly. But I like to look at the pictures!")
    fnDialogue("Ross: [E|Happy] The monsters and stuff are a great art reference.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "Computer") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Ross]Ross: [E|Neutral]One of the perks of working at the library is...[P][P][E|Happy] I get the hi-speed wi-fi codes weeks before anyone else does!")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "Paranormal") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Ross]Ross: [E|Happy]DUDE![P][E|Blush] That's my poster.")
    fnDialogue("Ross: [E|Neutral]Well like not [ITALICS]my[ENDITALICS] poster, but...[P] I got paid a sweet couple 'o Jacksons to draw it, bro.")
    fnDialogue("Ross: [E|Shock] I would have done it for free, but the club captain like, INSISTED on paying me.[P][E|Happy] Not like I resisted that hard, heh.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "OldComputer") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Ross]Ross: [E|Happy] All those old computers are good for is a game of solitaire.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "Checkout") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Ross]Ross: [E|Neutral]Welcome to my domain, brah.[P] Here is where you check out books, or return books you've already checked out.")
    fnDialogue("Ross: [E|Shock] Make sure you only check out good books though,[P] because I absolutely judge you based on your selections.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "TV") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Ross]Ross: [E|Neutral]They wheel out the ol' TVs whenever there's a sub.[P] Otherwise, we sometimes use 'em to watch documentaries in here during lunch.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "School") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Ross]Ross: [E|Neutral]Bein' in the library as long as I have,[P] you start to like,[P] really pay attention to all the little details in here.")
    fnDialogue("Ross: [E|Shock] For example, who is that dude in the wheelchair?")
    fnDialogue("Player: I don't know.")
    fnDialogue("Ross: [E|Neutral]I figure he must've been pretty important.")
    fnDialogue("Ross: [E|Shock]This is a construction site, after all.[P] They wouldn't let just any dude roll on in there.")
	fnDialogue("Ross: [E|Sad]Also, doesn't he have kind of an angry look in his eye? [P] I wonder if he was having a bad day.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "Library") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Ross]Ross: [E|Happy]Haha, dude,[P] can you believe people thought those old computers were like,[P] state-of-the-art at one time?")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "Security") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Ross]Ross: [E|Mad]There's a few of these peppered around the school, supposedly for security reasons.")
    fnDialogue("Ross: [E|Shock] You ever wonder, like, who's watching on the other side?")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

--[People]
elseif(sTopicName == "Laura") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Ross]Ross: [E|Neutral] Laura comes into the library on the reg.")
    fnDialogue("Ross: [E|Shock] Sometimes it looks like she's researching stuff, sometimes she looks at cat videos, and one time...")
    fnDialogue("Ross: [E|Neutral]I just walked by, and she was like, [P]just looking at a butt.[P][P][E|Mad] Just a butt.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
elseif(sTopicName == "Todd") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Ross]Ross: [E|Neutral]That's the president and VP over there!")
    fnDialogue("Ross: Haha, did you know that Terry ran for class president two years ago?")
    fnDialogue("Ross: [E|Shock] I won't tell you the final vote tally,[P] but needless to say [E|Happy]she is not quite as full of 'school spirit' these days.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "Ross") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Ross]Ross: [E|Neutral]Aaaaayyy, that's meeee.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "Terry") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Ross]Ross: [E|Neutral]Some people think my sibling can't take criticism. [E|Shock]Meanwhile, those who live with her have seen her drop a brand-new smartphone-- ")
    fnDialogue("Ross: --trust me, she's [E|Happy]veeeery capable of criticising herself.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

end
