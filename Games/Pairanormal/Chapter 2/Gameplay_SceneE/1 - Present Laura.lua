--[Laura Presentation]
--Showing stuff to Laura.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--Variables.
local sScriptPath = gsRoot .. "../Chapter 2/Gameplay_SceneE/1 - Present Laura.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
local sAutoAdvance = gsRoot .. "../Chapter 2/Gameplay_SceneB/8 - PatchGoingHome.lua"

--Saving
local sSaveExecute = gsRoot .. "../Chapter 2/Gameplay_SceneB/5 - A Body In the Library.lua"
local sSaveTopic = "No Topic"
local sSaveFlag = "Investigation"

--[Friendship Increment]
--If this is the first time presenting this object to this character then we increase the friendship tracker for
-- that character.
local fnIncrementFriendship = function(iObjectIndex, iIncrement)
	
    --Arg check.
    if(iObjectIndex == nil) then return end
    if(iIncrement == nil) then return end
    
    --Build strings.
	local i = iObjectIndex
    local p = iLauraIndex
    local sObjectString = string.format("%02i", i)
    local sSubString    = string.format("%02i", p)
    local iIsRepeat = VM_GetVar("Root/Variables/Chapter2/InvestigationB/iObjectPresent|" .. sObjectString .. sSubString, "N")
    if(iIsRepeat == 0.0) then
        
        --Flag
        VM_SetVar("Root/Variables/Chapter2/InvestigationB/iObjectPresent|" .. sObjectString .. sSubString, "N", 1.0)
    
        --Variable gain.
        local iLauraFriend  = VM_GetVar("Root/Variables/System/Romance/iLauraFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iLauraFriend", "N", iLauraFriend + iIncrement)
    end
end

--[Chat Sequences]
--Chatting.
if(sTopicName == "Chat") then
	
	--Variables.
	local iChattedWithLaura = VM_GetVar("Root/Variables/Chapter2/Investigation/iChattedWithLaura", "N")
	
	--First pass:
	if(iChattedWithLaura == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter2/Investigation/iChattedWithLaura", "N", 1.0)
		
		--Text.
		fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
		fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
		fnCutsceneBlocker()
		fnDialogue([[[HIDEALLFAST]Thought: I walk up to Laura.[P] Time to get to the bottom of why she's been avoiding me.]])
		fnDialogue([[Thought: As I get near, I can see she is smiling. And.[P].[P]. her face is all red.]])
		fnDialogue([[Player: Hey, Laura. ]])
		fnDialogue([[[CHARENTER|Laura]Laura: [E|Blush]*giggle*--[P][P][E|Shock]Oh!]])
		fnDialogue([[Player: I see her quickly close a window on the screen.[P] I couldn't see what was on it, just some kind of text.]])
		fnDialogue([[Player: So, um...[P] how goes it here in the library?[P] Are you holding down the fort?]])
		fnDialogue([[Laura: [E|Mad]Yep![P] I just... *ahem*[P][E|Neutral] I usually do some homework on the computers or play some games, or...]])
		fnDialogue([[Thought: Her eyes drift to the screen.]])
		fnDialogue([[Laura: [E|Sad]... Or whatever.]])
		fnDialogue([[Player: Oh, cool, cool.]])
		fnDialogue([[Laura: Yep!]])
		fnDialogue([[Player:[P].[P].[P].]])
		fnDialogue([[Laura: .[P].[P].]])
		fnDialogue([[Player: So! I bet you're excited for the new computer lab.[P] Looks like it's gonna be ready soon!]])
		fnDialogue([[Laura: [E|Happy]Oh yes![P] ... except--]])
		fnDialogue([[Player: Hm?]])
		fnDialogue([[Laura: [E|Sad]uh... it's nothing.]])
		fnDialogue([[Thought: She rests her hands on the bulky screen, stroking it affectionately,[P] like a face.]])
		fnDialogue([[Laura: [E|Sad]I guess I'm just going to miss the old computers, too, in a way.]])
		fnDialogue([[Player: Wh-why's that?]])
		fnDialogue([[Laura: [E|Neutral]Oh, I dunno...[P][E|Happy] they have their charms. ]])
		fnDialogue([[Thought: She giggles.]])
		fnDialogue([[Laura: [E|Happy]For one thing, they're much easier to hack.[P] You won't believe the things I've found in these guys.]])
		fnDialogue([[Player: Such as?]])
		fnDialogue([[Laura: [E|Neutral] Well a bunch of old directories, for one thing.[P] Did you know--]])
		fnDialogue([[Thought: She leans forward, her eyes crinkled in delight.[P] I can feel her excited breath on my cheek.]])
		fnDialogue([[Laura: [E|Neutral]These computers used to belong to Kane Industries back in the 80s.]])
		fnDialogue([[Laura: [E|Happy] And guess what? [P]They never wiped the drives before bringing them over.]])
		fnDialogue([[Thought: [IMAGE|Root/Images/MajorScenes/All/CH2_LauraComputer][CHAREXIT|ALL]She pulls up a new window, and faster than I could possibly comprehend, navigates through the files...]])
		fnDialogue([[Thought: Occasionally typing something strange in before continuing. Suddenly an image appears on the screen.]])
		fnDialogue([[Thought: It looks like a photograph of some kind of old blueprint, complete with handwritten notes.]])
        fnUnlockGallery("Gallery_Scene_LauraComputer")
		fnDialogue([[Laura: [E|Shock]There's shipping invoices, blueprints, and even journals kept by people who worked there.[E|Happy] But do you want to know the best part?]])
		fnDialogue([[Thought: I feel a little breathless.[P] The way Laura's eyes sparkle when she talks about this (even though I have no idea what she means),]])
		fnDialogue([[Thought: It's almost like I'm speaking to a different person.]])
		fnDialogue([[Laura: [E|Shock]I'm not the only person who knew about it, either.]])
		fnDialogue([[Laura: The very first computer club at Foster High kept their journals on these old drives, too. I've read diaries on here that are nearly [ITALICS]thirty years old[ENDITALICS].]])
		fnDialogue([[Laura: [E|Happy] It's absolutely fascinating. ]])
		fnDialogue([[Player: Whoah.]])
		fnDialogue([[Laura: [IMAGE|Null][CHARENTER|Laura]And I've barely scratched the surface![P] I don't know how I'll get through all of it before--]])
		fnDialogue([[Info: [SFX|World|Blink]Bing!]])
		fnDialogue([[Thought: A new window pops up on Laura's screen.[P][P] [CRASH]She spins around, face red as a beet.]])
		fnDialogue([[Player: What's that?]])
		fnDialogue([[Laura: [E|Blush]N-[P]nothing![P] Um, can you give me a moment [PLAYERNAME]?]])
		fnDialogue([[Player: Uh, sure.]])
		fnDialogue([[Thought: I get up, my shoulders comically low.[P] [ITALICS]Dang it, I really thought I was wrong about her for a moment[ENDITALICS].]])
		fnDialogue([[Laura: [E|Mad]U-um![P][E|Blush] If you have any questions *hrm* f-[P]feel free to let me know, okay?]])
		fnDialogue([[Laura: I'll help you out... *ahem*[P] any way I can!]])
		fnDialogue([[Player: ... Sure thing.]])
		fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
		fnCutsceneBlocker()
    
        --Variable gain.
        local iLauraFriend  = VM_GetVar("Root/Variables/System/Romance/iLauraFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iLauraFriend", "N", iLauraFriend + 5.0)
	
        --Time loss.
        if(fnDecrementTime(5, sAutoAdvance) == false) then
            PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
        end
		
	--Repeat passes.
	else
		fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
		fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
		fnCutsceneBlocker()
		fnDialogue("[HIDEALLFAST][SHOWCHAR|Laura|Neutral]Laura: [E|Shock]Oh, hey [PLAYERNAME].[P] G-Give me just a minute will you?")
		fnDialogue("Player: Gotcha.")
		fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
		fnCutsceneBlocker()

	end
	
--[Objects]
elseif(sTopicName == "Romance") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Laura|Neutral]Laura: [E|Mad]Oh, a romance book![P][E|Blush] Makes you wonder, doesn't it?")
    fnDialogue("Player: Wonder what?")
    fnDialogue("Thought: Laura doesn't respond, twirling a lock of hair in her fingers while she gives the computer screen a faraway stare.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvD_Romance, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
elseif(sTopicName == "Alien") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Laura|Neutral]Laura: [E|Neutral]One of the very first club missions we ever had was investigating the old crater on the far side of town.")
    fnDialogue("Laura: [E|Shock]Rumor has it aliens landed there back in the 50s.")
    fnDialogue("Player: Whoah, cool![P] Did you ever find anything?")
    fnDialogue("Thought: Laura shrugs.")
    fnDialogue("Laura: [E|Happy]]Nothing paranormal, I'm afraid.[P][E|Neutral] But there's some beautiful quartz caves around it that you should check out.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvD_Alien, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
elseif(sTopicName == "DandDee") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Laura|Neutral]Laura: [E|Mad]Oooh, that reminds me. [E|Neutral]We simply [ITALICS]have[ENDITALICS] to get a roleplay session together with the club.")
    fnDialogue("Player: What's 'roleplay'?")
    fnDialogue("Laura: [E|Neutral]It's like a game...[P][P][E|Sad]Mmm, it's hard to explain.")
    fnDialogue("Laura: [E|Happy] But trust me, you'll get a big kick out of it.[P] It's fun!")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvD_Dandee, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
elseif(sTopicName == "Computer") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Laura|Neutral]Laura: [E|Neutral]Can you believe it?[P] Look at all that shiny new tech...[P]*sigh!*")
    fnDialogue("Laura: [P][E|Sad] so close, yet so far.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvD_Computer, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "Paranormal") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Laura|Neutral]Laura: [E|Neutral]That's our club poster! [P]Gosh, we must have put it up two years ago.[P][E|Happy] No one's ever bothered to take it down.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvD_Paranormal, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
elseif(sTopicName == "OldComputer") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Laura|Neutral]Laura: *clack* *clack* *clack* Hm? Did you say something, [PLAYERNAME]")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvD_OldComputer, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
elseif(sTopicName == "Checkout") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Laura|Neutral]Laura: Have you checked out any books yet, [PLAYERNAME]?")
    fnDialogue("Player: Ah... no, not just yet. Do you think you could help me figure out, like... how to do that?")
    fnDialogue("Laura: [E|Shock] *clack* *clack* *clack* Hm? Did you say something, [PLAYERNAME]?")
    fnDialogue("Thought: She seems awfully distracted by that computer today.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvD_Checkout, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
elseif(sTopicName == "TV") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Laura|Neutral]Laura: [E|Shock] *clack* *clack* *clack* Hm? Did you say something, [PLAYERNAME]?")
    fnDialogue("Thought: She seems awfully distracted by that computer today.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvD_TV, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
elseif(sTopicName == "School") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Laura|Neutral]Laura: [E|Shock] *clack* *clack* *clack* Hm? Did you say something, [PLAYERNAME]?")
    fnDialogue("Thought: Laura's a tad occupied. I should ask someone else about this.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvD_School, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
elseif(sTopicName == "Library") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Laura|Neutral]Laura: [E|Neutral] Ah! That's the school's old computer club, back when these computers were new.")
    fnDialogue("Laura: [E|Shock]The club isn't around anymore, sadly.[P][E|Happy] But maybe it'll start up again now that there's a new lab here!")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvD_Library, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
elseif(sTopicName == "Security") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Laura|Neutral]Laura: [E|Shock] *clack* *clack* *clack* Hm?[P] Did you say something, [PLAYERNAME]?")
    fnDialogue("Thought: Laura's a tad occupied. I should ask someone else about this.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvD_Security, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

--[People]
elseif(sTopicName == "Laura") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Laura|Neutral]Laura: [E|Neutral]I'm always on call for a club mission;[P] it's nice to be useful even though I'm not there with everyone.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvD_Laura, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
elseif(sTopicName == "Todd") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Laura|Neutral]Laura: [E|Neutral]That's the associated student body president and vice president over there.[P] Didn't you say you were studying with Todd?")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvD_Todd, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "Ross") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Laura|Neutral]Laura: [E|Shock]The twins are almost always working here in the library after school.[P] We don't talk much, though.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvD_Ross, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "Terry") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Laura|Neutral]Laura: [E|Shock]The twins are almost always working here in the library after school. We don't talk much, though.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvD_Terry, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

end
