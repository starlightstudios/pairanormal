--[Todd Presentation]
--What a Todd thing to say.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--Variables.
local sScriptPath = gsRoot .. "../Chapter 2/Gameplay_SceneE/1 - Present Laura.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
local sAutoAdvance = gsRoot .. "../Chapter 2/Gameplay_SceneB/8 - PatchGoingHome.lua"

--Saving
local sSaveExecute = gsRoot .. "../Chapter 2/Gameplay_SceneB/5 - A Body In the Library.lua"
local sSaveTopic = "No Topic"
local sSaveFlag = "Investigation"

--[Friendship Increment]
--If this is the first time presenting this object to this character then we increase the friendship tracker for
-- that character.
local fnIncrementFriendship = function(iObjectIndex, iIncrement)
	
    --Arg check.
    if(iObjectIndex == nil) then return end
    if(iIncrement == nil) then return end
    
    --Build strings.
	local i = iObjectIndex
    local p = iToddIndex
    local sObjectString = string.format("%02i", i)
    local sSubString    = string.format("%02i", p)
    local iIsRepeat = VM_GetVar("Root/Variables/Chapter2/InvestigationB/iObjectPresent|" .. sObjectString .. sSubString, "N")
    if(iIsRepeat == 0.0) then
        
        --Flag
        VM_SetVar("Root/Variables/Chapter2/InvestigationB/iObjectPresent|" .. sObjectString .. sSubString, "N", 1.0)
    
        --Variable gain.
        local iToddFriend = VM_GetVar("Root/Variables/System/Romance/iToddFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iToddFriend", "N", iToddFriend + iIncrement)
    end
end

--[Chat Sequences]
--Chatting.
if(sTopicName == "Chat") then
	
	--Variables.
	local iChattedWithTodd = VM_GetVar("Root/Variables/Chapter2/Investigation/iChattedWithTodd", "N")
	
	--First pass:
	if(iChattedWithTodd == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter2/Investigation/iChattedWithTodd", "N", 1.0)
		
		--Text.
		fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
		fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
		fnCutsceneBlocker()
		fnDialogue([[ [HIDEALLFAST][CHARENTER|Harper][CHARENTER|Todd][FOCUS]Thought: Todd is holding the ladder steady as Harper, at the top, carefully pins the \"WELCOME TO THE LAB\" banner in place.]])
		fnDialogue("Player: How's it going, guys?")
		fnDialogue("Todd: [E|Happy]It's going great![P][E|Neutral] We're just taking care of some ASB business")
		fnDialogue("Player: ASB?")
		fnDialogue("Harper: [E|Neutral]Associated[P] Student[P] Body, hun.[P] Class reps.[P] I'm the pres-i-dent,[E|Happy] and thiii-iiis--")
		fnDialogue("Thought: Harper nudges Todd in the shoulder with his fancy leather shoes,[P] prompting Todd to laugh so hard he snorts a little.")
		fnDialogue("Harper: [E|Neutral]--is the Vice President.")
		fnDialogue("Player: Cool![P] I didn't know you were like... the president of a whole... [P][ITALICS]organization[ENDITALICS].")
		fnDialogue("Harper: [E|Mad]Yeah, he likes to keep secrets like that. [P][E|Happy]For example, he refuses to explain how the two of you are ac-quaint-ed???")
		fnDialogue("Thought: Harper kicks at Todd again,[P] still playfully, but hard enough to knock him into me.")
		fnDialogue("Thought: I feel a strange tingle on my ankles as we detangle from each other.")
		fnDialogue("Todd: [E|Happy]Pf'hahaha![P] Sorry, Harper![P][E|Blush] I should have mentioned it before--")
		fnDialogue("Player: T-Todd is tutoring me.")
		fnDialogue("Thought: At this,[P] Harper stops laughing.")
		fnDialogue("Harper: [E|Shock]Oh, re-[P]ally?[P] As in, [P][E|Blush][ITALICS]private[ENDITALICS] tutoring?")
		fnDialogue("Player: Yes.[P] I'm...[P][P] because of my situation, I'm not very good at schoolwork. ")
		fnDialogue("Thought: Todd straightens up, readjusting his glasses.[P] He regards me with a kind reverence, and I can't help but smile.")
		fnDialogue("Todd: [E|Neutral]But you're a fast learner, [PLAYERNAME].[P] Just because you have a lot to learn doesn't mean you aren't as smart as anyone else here.")
		fnDialogue("Player: Th-[P][P]thanks.")
		fnDialogue("Todd: [E|Neutral] Anyway, what brings you by?")
		fnDialogue("Player: Well... I came by as a representative of the Paranormal Club.[P] I'm here with Laura.")
		fnDialogue("Thought: We turn to Laura, who is so close to the computer screen her nose is practically touching the glass.")
		fnDialogue("Player: Who...[P]er,[P] is busy at the moment.")
		fnDialogue("Harper: [E|Happy]Omigod! That's so-[P]o great that you're part of a club, [PLAYERNAME]! ")
		fnDialogue("Thought: [ITALICS]Hnnnng[ENDITALICS][P]. Why am I weirdly irked whenever this person talks? ")
		fnDialogue("Todd: [E|Happy]And the Paranormal Club, to boot! I bet you get into all kinds of wacky adventures. ")
		fnDialogue("Thought: I want to regale Todd with fantastic stories of the club,[P] but unfortunately all of our recent 'investigative missions' have been to Charlie's Pizza Parlor,")
		fnDialogue("Thought: to discover why the soda fountain flavors taste like cotton candy when you mix them all together.")
		fnDialogue("Thought: It definitely doesn't have anything to do with ghosts.")
		fnDialogue("Player: [P].[P].[P].You bet.")
		fnDialogue("Todd: [E|Neutral]How's Mari treating you? Is she an awesome club president?")
		fnDialogue("Player: [SFX|World|Blink]You know Mari?")
		fnDialogue("Thought: He nods.")
		fnDialogue("Todd: [E|Blush]We've been next-door neighbors since forever.[P][E|Neutral] Grew up together, in a way.")
		fnDialogue("Todd: [E|Happy]Played a lot of video games with her, that's for sure.")
		fnDialogue("Player: Oh, cool.[P] Well, she [ITALICS]is[ENDITALICS] an awesome club president.[P] A-And a friend, too...[P]I think.")
		fnDialogue("Harper: [E|Neutral]Aaaaaaaaaaanyway, hon. Let us know if you need anything!")
		fnDialogue("Harper: [E|Neutral]We're a little bu-[P]sy at the moment, but we can answer any questions you might have about the library!")
		fnDialogue("Player: Oh, gotcha.")
		fnDialogue("Thought: Decision - I don't like that guy.")
		fnDialogue("Todd: [E|Neutral]See you around, [PLAYERNAME]!")
		fnDialogue("Player: See you, Todd.")
		fnDialogue([[Harper: [E|Happy]Byyyyeee!]])
		fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
		fnCutsceneBlocker()
    
        --Variable gain.
        local iToddFriend = VM_GetVar("Root/Variables/System/Romance/iToddFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iToddFriend", "N", iToddFriend + 5.0)
	
        --Time loss.
        if(fnDecrementTime(5, sAutoAdvance) == false) then
            PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
        end
		
	--Repeat passes.
	else
		fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
		fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
		fnCutsceneBlocker()
		fnDialogue("[HIDEALLFAST][SHOWCHAR|Todd|Neutral][SHOWCHAR|Harper|Neutral]Harper: [E|Neutral]Hello again, honey! Do you have any ques-tions about the library?[E|Shock] As the ASB president, I know all the tea.")
		fnDialogue("Player: Tea?")
		fnDialogue("Todd: [E|Neutral] Tea like the letter T. It means the 'Truth'.")
		fnDialogue("Player: Oh, I see.[P] Not yet, but I'll let you know.")
		fnDialogue("Harper: [E|Happy]We'll be here if you need us~")
		fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
		fnCutsceneBlocker()
	end
	
--[Objects]
elseif(sTopicName == "Romance") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Todd][CHARENTER|Harper][FOCUS|Harper]Harper: [E|Happy]Ahh, the Pirate Scoundrel![P] A claaaa-ssic!")
    fnDialogue("Todd: [E|Shock]YOU read tawdry romance novels, Harper?")
    fnDialogue("Harper: [E|Blush]Sh-[P]shut up! you wouldn't understand.[P] A boy's heart is complica-ted.")
    fnDialogue("Todd: [E|Neutral]Pffffff, [P]'Kay.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvD_Romance, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "Alien") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Todd][CHARENTER|Harper][FOCUS|Todd]Todd: [E|Neutral]Have you ever heard of the Fermi Paradox, [PLAYERNAME]?")
    fnDialogue("Player: No, what's that?")
    fnDialogue("Todd: [E|Blush]There's a high probability that alien life abounds in the universe, even within our galaxy.")
    fnDialogue("Todd: [E|Mad] But if that's the case, how come we haven't met any yet?")
    fnDialogue("Player: Uhh, I don't know.")
    fnDialogue("Todd: [E|Happy]Neither does anyone else.[P] It's another fine example of the antipodal nature of our reality.")
    fnDialogue("Harper: [E|Shock]Have you ever considered that human life might be, like,[P] a unique thing?")
    fnDialogue("Thought: Todd deflates.")
    fnDialogue("Todd: [E|Sad]Yeah, but what's the fun in thinking that?")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvD_Alien, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "DandDee") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Todd][CHARENTER|Harper][FOCUS|Todd]Todd: [E|Shock]Oh, man! Dungeons and Dragons! Have you ever role-played, [PLAYERNAME]?")
    fnDialogue("Player: No, what's that?")
    fnDialogue("Thought: Todd becomes very excited.[P] Harper continues working, he appears not to be too interested.")
    fnDialogue("Todd: [E|Blush]It's a type of game that you play in real life with your friends.")
    fnDialogue("Todd: [E|Mad] You make up a character, and you fight monsters and stuff together,[P] and create this whole story![P][E|Happy] It's awesome.")
    fnDialogue("Thought: I chew on this concept for a little bit--[P]what would I be if I weren't me?")
    fnDialogue("Todd: [E|Sad]*sigh*.[P].[P].[P] It's not very popular at the school, though.[P] I've never been able to get a group together.")
    fnDialogue("Thought: Harper looks kind of disappointed.[P] I wonder if Todd ever tried roleplaying with the Associated Student Body.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvD_Dandee, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "Computer") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Todd][CHARENTER|Harper][FOCUS|Harper]Harper: [E|Neutral]Are you excited for the new lab, [PLAYERNAME]?")
    fnDialogue("Player: Sort of.[P] I'm not very good with computers.")
    fnDialogue("Harper: [E|Shock]Oh yea-[P]aaah.[P][P] Sorry, I forgot.")
    fnDialogue("Thought: He sticks a new pin in the banner hanging above the entrance.")
    fnDialogue("Harper: [E|Neutral]Trust me, it's a ve-eeerry big deal![P] You'll love it when you get used to it.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvD_Computer, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "Paranormal") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Todd][CHARENTER|Harper][FOCUS|Harper]Harper: [E|Shock]Todd tells me you're part of the Paranormal Club.[E|Happy] How rad!")
    fnDialogue("Player: Yeah.[P] Um... I like it a lot.")
    fnDialogue("Harper: [E|Mad]Clubs are so-[P]oo important.[P][E|Neutral] They're a great way to learn stuff and make new friends.")
    fnDialogue("Harper: [E|Happy]At ASB we make sure it's as easy as possible to start and join a club!")
    fnDialogue("Thought: Todd winks at me.")
    fnDialogue("Todd: [E|Neutral]We brought the process down from three fill-out forms to one.[P][E|Happy] You're welcome, Pf'hahaha!")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvD_Paranormal, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "OldComputer") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Todd][CHARENTER|Harper][FOCUS|Harper]Harper: [E|Mad]ASB has been fighting so-oo hard to get rid of those dingy old computers.")
    fnDialogue("Harper: How are our schools going to like, prepare us for the modern age if they give us tools from the [ITALICS]stone[ENDITALICS] age?")
    fnDialogue("Player: I don't know.")
    fnDialogue("Harper: [E|Neutral]They can't, is the answer![P][E|Happy] This new lab is gonna be so[p]oo awesome!")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvD_OldComputer, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "Checkout") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Todd][CHARENTER|Harper][FOCUS|Harper]Harper: [E|Neutral]Have you like, checked out any books yet, [PLAYERNAME]?[P] I can give you some great recommendations if you want!")
    fnDialogue("Todd: [E|Sad]Oh, um, Harper.[P].[P].[P] I don't think I mentioned, but [PLAYERNAME] can't re--")
    fnDialogue("Player: Yeah, I would love some![P] Write me out a list sometime.")
    fnDialogue("Harper: [E|Happy]You got it!")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvD_Checkout, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "TV") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Todd][CHARENTER|Harper][FOCUS|Harper]Harper: [E|Shock]Ooh, that TV reminds me![E|Neutral] Have you heard of this thing called [P]'Netflix',[P] Todd?")
    fnDialogue("Todd: [E|Neutral]Uhhh, yeah, I think.[E|Blush] Don't they send DVDs and stuff to your house?")
    fnDialogue("Harper: [E|Shock]Yeah, but no-ow you can watch all their stuff online, too![E|Neutral] I have to send you my login password so we can watch something, like, together.")
    fnDialogue("Todd: [E|Neutral]Hm...[P]watch Netflix and just, like.[P].[P].[P] chill?[E|Happy] Sounds like a good time.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvD_TV, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "School") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Todd][CHARENTER|Harper][FOCUS|Harper]Harper: [E|Happy]That's the school when it was being built. See that man?")
    fnDialogue("Thought: He points to the man with the shovel, 'breaking ground' on a new building.")
    fnDialogue("Harper: [E|Neutral]That's Kane McKay Foster![P] The same as the statue out in front.")
    fnDialogue("Harper: His wife is standing there in the background, too.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvD_School, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "Library") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Todd][CHARENTER|Harper][FOCUS|Harper]Thought: Harper flips his hair excitedly.")
    fnDialogue("Harper: [E|Shock]Oh-em-geeee! You're gonna love this.[P][E|Neutral] You know our Principal, Miss Arroyo, right?")
    fnDialogue("Player: Yeah.")
    fnDialogue("Thought: Harper taps the only woman in the photo.[P] My eyes widen.")
    fnDialogue("Player: Oh, wow.[P] That's her?")
    fnDialogue("Harper: [E|Happy]RIGHT?!?![P] She was the first woman in the Foster High Computer Club.")
    fnDialogue("Todd: [E|Mad]The first [ITALICS]black[ENDITALICS] woman, to boot.")
    fnDialogue("Harper: [E|Happy]Yaaaaaaas.[P] Go [ITALICS]off[ENDITALICS], baby Arroyo!")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvD_Library, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "Security") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Todd][CHARENTER|Harper][FOCUS|Todd]Todd: [E|Neutral]Don't freak out about the security cams. Wanna know a little secret?")
    fnDialogue("Thought: He leans in.")
    fnDialogue("Todd: [E|Blush]Half of them don't even work anyway.")
    fnDialogue("Harper: [E|Shock]Oh-em-geee Todd![P][E|Sad] Don't go blabbing about that!")
    fnDialogue("Todd: [E|Happy]It's fine, it's fine!")
    fnDialogue("Todd: [E|Mad]Look, we've only got one security guy on campus, and he's usually just eating lunch out on the quad.")
    fnDialogue("Todd: [E|Blush]Those things are just to intimidate more than anything else. ")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvD_Security, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

--[People]
elseif(sTopicName == "Laura") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Todd][CHARENTER|Harper][FOCUS|Todd]Todd: [E|Neutral]Laura's usually working away in here, isn't she?")
    fnDialogue("Harper: [E|Blush]Always on that computer.[P][E|Happy] I'll bet she's gonna lo-ove the new lab!")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvD_Laura, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
elseif(sTopicName == "Todd") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Todd][CHARENTER|Harper][FOCUS|Harper]Harper: [E|Happy]As I've said before, if you have any questions about the school, just let the Associated Student Body know!")
    fnDialogue("Harper: [E|Happy]It's ASB's job to make new students wel-come here.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvD_Todd, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "Ross") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Todd][CHARENTER|Harper][FOCUS|Harper]Harper: [E|Neutral]You've got to admire those twins.[P] They work every day after school in the library without pay.")
    fnDialogue("Todd: [E|Shock]Isn't that a little exploitative? ")
    fnDialogue("Harper: [E|Happy]The School Board prefers to call it 'promoting strong work ethic'.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvD_Ross, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "Terry") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Todd][CHARENTER|Harper][FOCUS|Harper]Harper: [E|Neutral]You've got to admire those twins.[P] They work every day after school in the library without pay.")
    fnDialogue("Todd: [E|Shock]Isn't that a little exploitative? ")
    fnDialogue("Harper: [E|Happy]The School Board prefers to call it 'promoting strong work ethic'.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvD_Terry, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

end
