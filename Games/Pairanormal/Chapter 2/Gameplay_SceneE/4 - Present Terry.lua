--[Terry Presentation]
--Showing stuff to Terry.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--Variables.
local sScriptPath = gsRoot .. "../Chapter 2/Gameplay_SceneE/1 - Present Laura.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
local sAutoAdvance = gsRoot .. "../Chapter 2/Gameplay_SceneB/8 - PatchGoingHome.lua"

--Saving
local sSaveExecute = gsRoot .. "../Chapter 2/Gameplay_SceneB/5 - A Body In the Library.lua"
local sSaveTopic = "No Topic"
local sSaveFlag = "Investigation"

--[Chat Sequences]
--Chatting.
if(sTopicName == "Chat") then
	
	--Variables.
	local iChattedWithTerry = VM_GetVar("Root/Variables/Chapter2/Investigation/iChattedWithTerry", "N")
	
	--First pass:
	if(iChattedWithTerry == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter2/Investigation/iChattedWithTerry", "N", 1.0)
		
		--Text.
		fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
		fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
		fnCutsceneBlocker()
		fnDialogue([[[HIDEALLFAST][CHARENTER|Terry]Player: Hey, you're that twin... Terry, right?]])
		fnDialogue([[Thought: Terry sighs, impatiently pulling out, stacking and resorting the books laid out next to them on a wheeled cart.]])
		fnDialogue([[Terry:[E|Neutral] Hot tip;[P] it's kind of a bad idea to refer to me as 'that twin'.[P][E|Shock] It's kiiiiind of objectifying.]])
		fnDialogue([[Player: Sorry.]])
		fnDialogue([[Terry: [E|Neutral] I'll let it slide.[P] Word around these halls is you're kind of new to the whole 'I'm alive' thing.[P] [PLAYERNAME], right?]])
		fnDialogue([[Player: Yeah--]])
		fnDialogue([[Terry: [E|Happy]Just call me 'Terry', or 'her' or 'she'.]])
		fnDialogue([[Thought: She takes a moment to think. ]])
		fnDialogue([[Terry: [E|Shock]I guess 'he' is fine, too.]])
		fnDialogue([[Terry: [E|Happy] Oh, who am I fucking kidding? I don't care what you call me.[P][E|Shock] I've been called worse things by plenty of people.]])
		fnDialogue([[Player: [SFX|World|Ugh]I--]])
		fnDialogue([[Terry: [E|Sad]You must think I'm awfully stuck up, don't you?[P] Everyone does.]])
		fnDialogue([[Terry: [E|Shock]That's why everyone prefers my brother Ross.[P] He's the laid back one.]])
		fnDialogue([[Terry: [E|Happy]I'm trying to like, be laid back, but, I dunno.[P] Every time I sit still its like I can feel the fucking world hurtling through space.]])
		fnDialogue([[Terry: [E|Sad]Like every second I'm not moving I'm just slipping closer and closer to death.]])
		fnDialogue([[Player: [SFX|World|Ugh]--That's.[P].[P].[P].uh.]])
		fnDialogue([[Terry: [E|Shock]And now you must think I'm crazy.[P] So, I think I'll just stop talking now.]])
		fnDialogue([[Terry: [E|Happy]Let me know if you have any questions about books. I work here, by the way,[P] unofficially.]])
		fnDialogue([[Terry: [E|Mad]Not like anyone fucking cares.[P][E|Shock] Nobody, like,[P] visits the library these days.]])
		fnDialogue([[Thought: She shoves a book into the shelf,[P] placing her hands on her hips and nodding in approval, as though something very important and difficult just got taken care of.]])
		fnDialogue([[Terry: [E|Happy]Anyway, bye.]])
		fnDialogue([[Player: B...[P][SFX|World|Ugh]bye.]])
		fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
		fnCutsceneBlocker()
	
        --Time loss.
        if(fnDecrementTime(5, sAutoAdvance) == false) then
            PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
        end
		
	--Repeat passes.
	else
		fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
		fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
		fnCutsceneBlocker()
		fnDialogue("[HIDEALLFAST][SHOWCHAR|Terry|Neutral]Terry: [E|Happy] Let me know if you have any questions about books.[P][E|Shock] Not that anyone ever does--who the fuck visits a library these days?")
		fnDialogue("Player: F-for sure.")
		fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
		fnCutsceneBlocker()
	end
	
--[Objects]
elseif(sTopicName == "Romance") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Terry]Terry: [E|Happy]Pff, those books.[P].[P].[P][E|Sad] Read too many of them and you'll quickly get the wrong idea about love.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "Alien") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Terry]Terry: [E|Mad]Really, I can't believe they would stock such a silly book in a place of knowledge like this.")
    fnDialogue("Thought: Ross calls from across the room.")
    fnDialogue("[CHARENTER|Ross]Ross: [VOICE|Ross][E|Mad]Aliens landed in the crater outside of town in the 50s!")
    fnDialogue("[CHAREXIT|Ross]Terry: [E|Mad][CRASH]NO THEY FUCKING DIDN'T, ROSS!!![P] STOP SHOUTING IN THE FUCKING LIBRARY!!!!")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "DandDee") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Terry]Terry: [E|Neutral]Oooh, I fucking love reading through roleplaying game's rulebooks! All those beautiful interconnected systems.[P].[P].[P]")
    fnDialogue("Terry: [E|Blush] What kind of wonderful people have the imagination to weave it all together?")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "Computer") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Terry]Terry: [E|Sad]I might be the only one not into the new lab.")
    fnDialogue("Player: What? Why not?")
    fnDialogue("Terry: [E|Mad] Duh![P] This library, once a sanctum of peace and wisdom, will soon be flooded by annoying,[P] rule breaking teenagers...")
    fnDialogue("Terry: [ITALICS]Who will talk loudly,[P] spill soda over everything,[P] and do everything but their homework here.[ENDITALICS]")
    fnDialogue("[CHARENTER|Ross]Ross: [E|Neutral]ITS A PUBLIC SPACE, TERRY![P] PEOPLE CAN DO WHAT THEY WANT HERE!")
    fnDialogue("Terry: [E|Mad]WELL [ITALICS]MAYBE[ENDITALICS] IF YOU'D HAD TO CLEAN A DIRTY FUCKING CONDOM FROM BETWEEN THE PAGES OF TWELFTH NIGHT YOU'D FEEL DIFFERENTLY, ROSS!")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "Paranormal") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Terry]Terry: [E|Neutral]I see my brother's handiwork there;[P] those Paranormal Club captains, Mari and Stacy, are the only ones who have actually [ITALICS]paid[ENDITALICS] him for his artwork.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "OldComputer") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Terry]Terry: Hang on, I'm busy.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "Checkout") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Terry]Terry: [E|Shock]Ugh. Ross is over there, doing nothing.")
    fnDialogue("[CHARENTER|Ross]Ross: [E|Neutral]I can hear you guys talking about me!")
    fnDialogue("Terry: [E|Mad]WELL MAYBE IF YOU WERE FUCKING WORKING YOU'D BE TOO BUSY TO LISTEN TO GOSSIP!")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "TV") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Terry]Terry: Hang on, I'm busy.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "School") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Terry]Terry: [E|Neutral]I know, unbeleivable, right?[P] The school is really [ITALICS]that[ENDITALICS] fucking old.")
    fnDialogue("Terry: [E|Sad]I heard a rumor that the two water fountains outside of the gymnasium like, literally used to be segregated.")
    fnDialogue("Player: What does that mean?")
    fnDialogue("Terry: [E|Happy]Oh my god, that's so wild![P] You like, don't even know what racism is yet.")
    fnDialogue("Terry: [E|Shock]Prepare to get really disappointed in humanity.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "Library") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Terry]Terry: [E|Neutral] Back in the day those old computers were all cutting edge... I think they were donated to us from the old McKay Research Institute.")
	fnDialogue("Makes sense, since Kane McKay Foster basically built this school.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "Security") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Terry]Terry: [E|Shock]It [ITALICS]kind[ENDITALICS] of makes sense to have security cameras around valuable objects and stuff, but [E|Mad]hallways?")
    fnDialogue("Terry: Back entrances?[P] Abandoned closets?[P] They're fucking everywhere.")
    fnDialogue("Terry: [E|Sad] Ugh.[P] Gives me the creeps.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

--[People]
elseif(sTopicName == "Laura") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Terry]Terry: [E|Happy]Laura keeps to herself, generally.[P][E|Mad] She might be the only patron who actually follows the 'no loud noises' policy.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
elseif(sTopicName == "Todd") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Terry]Terry: [E|Shock]Ugh, ASB.[P]")
    fnDialogue("Terry: Always breathing down Ross's neck to make poster art for them, or draw something for their next campaign.")
    fnDialogue("Terry: [E|Blush]But do they want to actually pay him for his work? [ITALICS]Noooooo[ENDITALICS].")
    fnDialogue("Thought: From across the room, Ross calls out.")
    fnDialogue("[CHARENTER|Ross]Ross: [E|Happy]You're just jealous cuz because I have a marketable skill!")
    fnDialogue("Terry: [E|Mad][CRASH]ROSS!!![P][CRASH] NO SHOUTING IN THE FUCKING LIBRARY!!!")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "Ross") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Terry]Terry: [E|Neutral]My brother is, like,[P] [ITALICS]so[ENDITALICS] fucking smart and creative.")
    fnDialogue("Terry: [E|Shock] But god, it totally kills me that he won't move his butt to put any of that talent to use.")
    fnDialogue("Terry: [E|Sad]Keeps drawing whatever anyone wants for free.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "Terry") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][CHARENTER|Terry]Terry: [E|Happy]Talk to me whenever you need to speak with a [ITALICS]responsible[ENDITALICS] twin.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

end
