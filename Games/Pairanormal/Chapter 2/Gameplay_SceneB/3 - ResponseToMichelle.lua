--[Response to Michelle]
--Used after walking home with Michelle. Only that path can access this file.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)
	
--Save the game.
if(PairanormalLevel_IsLoading() == false) then
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Starting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

--Set data.
else
	PairanormalLevel_FlagCheckpoint("Starting")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Forest")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Forest")
	fnDialogue([[ [Music|Love][SHOWCHAR|Michelle|Neutral]Thought: [Focus|Michelle]... ]])
end


--[Execution]
--Go over the fence!
if(sTopicName == "Flower") then
	fnDialogue("Thought: I pick the figure I think I can make the most reliably--a flower.")
	fnDialogue("Player: Uhh... Okay.[P] Start with...[P]a circle...")
	fnDialogue("Thought: I've seen pictures of them here and there at school,[P] and a couple on the sidewalk.")
	fnDialogue("Player: Then...[P] some hoopy things around...")
	fnDialogue("Thought: There's someting simple and peaceful about flowers.[P] When I'm done, I nod in satisfaction, then give one last celebratory spurt of paint in the air.")
	fnDialogue("Michelle: [E|Shock]Oy, watch it![P][E|Mad] This stuff ain't cheap, y'know?!")
	fnDialogue("Player: Sorry![P] I-I just got so excited.")
	fnDialogue("Michelle: [E|Mad][P][P][E|Blush]Tuh, you're such a weirdo.[P] Painted a bloody flower!")
	
	
    
--Go THROUGH the fence!
elseif(sTopicName == "Scribble") then
	fnDialogue("Thought: I might as well get a feel for it before I go off trying to actually make a recognizalbe figure.")
	fnDialogue("Thought: I start by making a few tentative spurts on the wall.[P] The paint sprays out with more force than I expected.")
	fnDialogue("Player: whoah.")
	fnDialogue("Thought: Next I advance to making lines;[P] straight, then squiggly.")
	fnDialogue("Player: [P][P][P][SFX|World|Blink]This is kind of fun!")
	fnDialogue("Michelle: [E|Happy]Watch out now, we've got a Pollock in the making.")
	fnDialogue("[CHAREXIT|ALL]")
	fnDialogue("Thought: A line over here... then maybe a loop here![P] Then [ITALICs]scribbles[ENDITALICS]. Scribbles all over everything.")
	fnDialogue("Thought: What if I paint the longest line, from this wall to the other one?")
	fnDialogue("Thought: Ooh, or what if I paint on the ground??[P][ITALICS]why should an artist merely be limited by a single canvas???[ENDITALICS]")
	fnDialogue("I'm a genius, hahaha...[P][CRASH]AN ARTISTIC GENIUS!!!")
	fnDialogue("Player: [P][P][P]... Oh, crap.[P][SFX|World|Ugh] I overdid it, didn't I?")
	fnDialogue("[CHARENTER|Michelle]Michelle: [E|Happy]BAHAHAHAHA! Too right.[P][E|Shock] Christ, there's paint [ITALICs]everywhere[ENDITALICS]. ")
	fnDialogue("Player: It's pretty easy to get carried away with these things.[P] Uhhh, does this wash out of fabric?")
	fnDialogue("Michelle: [E|Shock]Ehh, Better you find out for yourself.[P][E|Neutral] But I'm glad you enjoyed it.")

	
elseif(sTopicName == "Feeling") then
	fnDialogue("Thought: I couldn't hope to create something [ITALICS]realistic[ENDITALICS], but I don't really have to do that, anyway, right?")
	fnDialogue("Player: I...[P]I guess I'll just paint whatever I feel like.")
	fnDialogue("Thought: I tentatively spray a wobbly, large circle.[P] For some reason, I like the idea of painting it around the small patch where parts of the wall have crumbled away.")
	fnDialogue("Michelle: [E|N]Interesting.")
	fnDialogue("Player: The hole... it's sort of like being empty, isn't it?[P] Like there's something missing. ")
	fnDialogue("Player: And... outside...[P] there's lots of activity.[P] Too much activity.")
	fnDialogue("Thought: I paint several tiny, undulating squiggles which radiate out from the circle.")
	fnDialogue("Player: But it's not bad. It's just...[P] a lot?[P] Huh.")
	fnDialogue("Thought: When I'm done, I take a step back.[P] It's like I'm looking at a shape that was in my mind, but I couldn't see before.")
	fnDialogue("Thought: And now that it's in front of me...[P]It's not so scary.")
	fnDialogue("Player: [P]Wow. ")
	fnDialogue("Michelle: [E|Blush][P].[P].[P].I'm glad you enjoyed yourself.")
	
end

--Dialogue rejoins here.
fnDialogue("")
	fnDialogue("Player: Heh. W-what do you think?")
	fnDialogue("Thought: We both regard our new collaborative masterpiece.[P] It's pretty obvious Michelle is much better at this than I am.")
	fnDialogue("Thought: --but something warm stirs in me knowing I got to nestle a small piece of my own work within her impressive, swirling figures.")
	fnDialogue("Michelle: [E|N]Well, you're no Banksy...[E|Happy]but it's all right.")
	fnDialogue("Player: Thanks, I think.")
	fnDialogue("Thought: I catch myself staring at her.[P] She smiles at first, rightly proud of the fresh paint, but all at once it fades.")
	fnDialogue("Player: Is something wrong?")
	fnDialogue("Michelle: [E|Shock]Hm?[P] Er, no--I... I just remembered. It's getting late.[P][E|Sad] You'd better get home. ")
	fnDialogue("Player: Oh crap! Right! ")
	fnDialogue("Michelle:[E|Blush] See you later, weirdo.")
	fnDialogue("[CHAREXIT|ALL][BG|Null]")
	fnDialogue("Thought: I wave goodbye to her and hightail it out of the alley, hoping I can remember how to get out of here--")
	fnDialogue("Thought:partly because I don't want to get lost now that it's getting late, and partly because I kinda want to come back someday.")
	fnDialogue("Thought: [ITALICS][P]... Maybe add a few more marks to the walls.[ENDITALICS]")
	

--Variable tracking.
VM_SetVar("Root/Variables/System/Romance/sLastWalkHome", "S", "Michelle")
VM_SetVar("Root/Variables/System/Romance/iMichelleFriend", "N", 1.0)

--Execute the next script in sequence.
LM_ExecuteScript(gsRoot .. "../Chapter 2/Gameplay_SceneC/0 - Home Time.lua", "Start")
