--[Response to Todd]
--Used after walking home with Todd. Only that path can access this file.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)
	
--Save the game.
if(PairanormalLevel_IsLoading() == false) then
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Starting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

--Set data.
else
	PairanormalLevel_FlagCheckpoint("Starting")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Hallway")
	fnDialogue([[ [Music|Love][SHOWCHAR|Todd|Neutral]Thought: [Focus|Todd]... ]])
end


--[Execution]
--Wait for meeeeee!
if(sTopicName == "Help") then
	fnDialogue("Player: Uh, sure. [P]Thanks.")
	fnDialogue("[BG|Root/Images/Backgrounds/All/Bathroom][CHAREXIT|ALL]Thought: I dart into the bathroom, not entirely sure what I should be doing in there or if I should even come out.")
	fnDialogue("Thought: Why did I feel so different from when I hang out with the paranormal club?[P] I felt...[P]unwelcome.[P] Like an outsider.")
	fnDialogue("Player: *whew*...[P]So what? It probably feels weird because... [P]you're just not used to it, that's all.[P] Yeah.[P] It's not going to stop me!")
	fnDialogue("Thought: After a few deep breaths, I practically kick down the bathroom door into the open, brimming with confidence.")
	fnDialogue("Player: [CRASH] I'm readyyyy!")
	fnDialogue("[CHARENTER|Harper]Thought: [P] Harper alone waits for me on the other side.")
	fnDialogue("Harper: [E|Neutral]Well, I assume you've taken care of ev-[P]ery-[P]thing in there?")
	fnDialogue("Player: Harp... er.[P][SFX|World|Ugh] You weren't supposed to...[P]uh, where is everyone?")
	fnDialogue("Harper: [E|Happy]They're waiting at the corner store a few blocks away.[P] I volunteered to stay behind and...[E|Neutral] show you the way.")
	
	--Save the game.
    fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"HarperTalk\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
    PairanormalLevel_FlagCheckpoint("HarperTalk")
	
	fnDialogue("[CHAREXIT|ALL][BG|NULL]")
	fnDialogue("Thought: ... [P][P][P]")
	fnDialogue("[BG|Root/Images/Backgrounds/All/SchoolExterior][CHARENTER|Harper]Harper: [P][P][P][E|Sad]*siiiiigh*.[BR] [ITALICS]For your in-fo[ENDITALICS],[P] [E|Mad]I can't stand awkward silence.[P] So.[P][E|Happy] Chat it up and tell me about your life, new kid.")
	fnDialogue("Player: Me? [P]I would have thought everyone's heard the rumors by now.")
	fnDialogue("Harper: [E|Neutral]*snort!* [P]Omigosh, Yeah.[P][E|Happy] Thanks by the way, for bringing some [E|Mad][ITALICS]act-[P]u-[P]al[ENDITALICS] juicy gossip to these storied halls.")
	fnDialogue("Harper: [E|Shock] So, you've never actually gone to school?")
	fnDialogue("Player: No,[P] I haven't.")
	fnDialogue("Harper: [E|Shock]Never known the stress of finals?[P][P] Never missed a bus?")
	fnDialogue("Thought: He gives me a sidelong glance.")
	fnDialogue("Harper: [E|Blush]Never had a crush?")
	fnDialogue("Player: Check, [P]check,[P] and... I don't think so?")
	fnDialogue("Harper: [E|Mad]Oooooomigosh.[P][P] Oh-[P]mi-[P][E|Happy]gosh!")
	fnDialogue("Harper: [E|Neutral] Well hun, allow me to impart a few friendly lessons in high school life,[P] courtesy of the ASB.")
	fnDialogue("Thought: We stop.")
	fnDialogue("Harper: [E|Sad]Lesson number 1 -- [P]it is in fact possible avoid another em-bar-rass-ment like [ITALICS]this[ENDITALICS].")
	fnDialogue("Thought: He gestures, I think, at something specific on me but the flourish is so grand it almost seems as he's referring to my whole being.")
	fnDialogue("Harper: For example, [P][E|Mad]NE-[P]VER[P] drink Kane Cola from the vending machines.")
	fnDialogue("Harper: [E|Sad] Let me guess, you tried to buy some and someone screamed at you until you dropped it?")
	fnDialogue("Thought: In a panic, I look down at my pantleg,[P] still stained with remnants of the incident.")
	fnDialogue("Thought: It feels like he's caught me without any pants at all.")
	fnDialogue("Player: h-how did you know?")
	fnDialogue("Harper: [E|Mad]Ooh, honey.[P] It's an oooooold trick passed down from gen-to-gen here at Foster High.")
	fnDialogue("Harper: [E|Neutral] The squirrels in the hills are obsessed with the stuff.")
	fnDialogue("Harper: [E|Shock] [ITALICS]A-[P]pparently[ENDITALICS], they practically jumped a kid walking around with an open bottle once, a long time ago.")
	fnDialogue("Harper: [E|Neutral]So,[P] unofficially,[P] like,[P] you're not really s'posed to be drinking it,")
	fnDialogue("Harper: [E|Mad]even though nobody's bothered to act-[P]ually [ITALICS]take it out[ENDITALICS] of the vending machines.")
	fnDialogue("Player: Seems like a job for ASB.")
	fnDialogue("Harper: [E|Shock]Hey![P][E|Happy] Well, touche.")
	fnDialogue("Thought: He gives me a playful jab in the ribs.")
	fnDialogue("Harper: [E|Happy]for your [ITALICS]in-[P]fo[ENDITALICS],[P] Everybody already knows not to touch the stuff.[P][P][E|Sad] Except for the new kid, I guess.")
	fnDialogue("Harper: He slings his arm around me,[P] and we resume our journey in an awkward step.")
	fnDialogue("Harper: [E|Neutral] Be-cause,[P] if you open up a bottle, every student thinks it's absolutely necessary to get you to drop it.")
	fnDialogue("Harper: In some cases they'll even slam it outta your hands.[P][E|Shock] You've just experienced a tra-dish case of the Kane Kola Cancel.")
	fnDialogue("Player: So that's it?[P] Everyone just cruelly screams at people and embarrasses them because.[P].[P].[P] [CRASH]because of some weird pointless tradition?")
	fnDialogue("Harper: [E|Happy]New kid,[P] you've just summarized the en-[P]tire highschool experience in a sentence.[P][SFX|World|Ugh] Bravo.")
	
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"HarperTalk2\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
    PairanormalLevel_FlagCheckpoint("HarperTalk2")
	
	fnDialogue("[CHAREXIT|ALL]Thought: I can see the rest of the group waiting outside the gate, but they haven't noticed us yet.")
	fnDialogue("Thought: Before I can wave them down, Harper suddenly tightens his grip on my shoulder.")
	fnDialogue("[CHARENTER|Harper]Harper: [E|Neutral]... And lesson number two,[P] do yourself a favor and try not to get[P][P][P][E|Sad] [ITALICS]attached[ENDITALICS] to Todd.")
	fnDialogue("Player: [SFX|World|Blink][P] What do you mean?")
	fnDialogue("Harper: [E|Neutral]If you're spending lots of alone time with him,[P] tu-to-ring and whatnot,")
	fnDialogue("Harper: you might make the mistake of thinking the two of you are closer than you actually are.")
	fnDialogue("Harper: [E|Happy] The truth is,[P] Todd is just really friendly.[P] And handsome.[P] Just about e[P]-veryone he meets likes him.")
	fnDialogue("Harper: [E|Sad] And some of those people like him a little [ITALICS]too[ENDITALICS] much.")
	fnDialogue("Thought: Our footpace slows to a stop, and Harper swings round like he's getting ready to excuse himself.")
	fnDialogue("Thought: Like he knows... I'm no longer quite so confident in joining everyone anymore.")
	fnDialogue("Harper: [E|Shock] NOT that I'm telling you what to feel, [E|Sad]I'm just saying...[P] Well. [P] You've seen what a tough gig highschool is.")
	fnDialogue("Harper: [E|Shock] Keep making doe-eyes at the li-ter-al most popular boy in school, and pretty soon you'll be calling unsavory attention to yourself.")

	--Save the game.
    fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"ToddHang\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
    PairanormalLevel_FlagCheckpoint("HarperThreat")
    
	fnDialogue("Thoughts: I can't tell if Harper is being sincere with me or making some kind of veiled threat. ")
	fnDialogue("Thoughts: To avoid thinking of the dangerous truth to his words, I scramble for an indignant reply")
    
    --TALKING TO HARPER.
    fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (What should I say?)") ]])

    local sDecisionScript = "\"" .. gsRoot .. "../Chapter 2/Gameplay_SceneB/11 - ResponseToHarper.lua" .. "\""
    fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Sounds like you're jealous\", " .. sDecisionScript .. ", \"Jealous\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"You've got it all wrong!\", " .. sDecisionScript .. ", \"Wrong\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Say nothing\", " .. sDecisionScript .. ", \"Nothing\") ")
    fnCutsceneBlocker()
	
	
	
--Go on ahead, go on GIT
else(sTopicName == "GoAhead") then
	fnDialogue("Player: [P][P][P]Nah.[P] Maybe next time.")
	fnDialogue("Todd: .[P].[P].[P][E|Neutral]All right, no problem! [E|Happy]See you around, [PLAYERNAME]!")
	fnDialogue("[CHAREXIT|ALL]Thought: I dart into the bathroom, waiting for their voices to trail off.")
	fnDialogue("[BG|Null]Player: [SFX|World|Ugh]That was.[P].[P].[P] so weird.")
	fnDialogue("Thought: Why did I feel so different from when I hang out with the Pairanormal club?[BR] I felt...[P][P]unwelcome.[P] Like an outsider. ")
	fnDialogue("Thought: [ITALICS] of course.[P] Stupid me.[ENDITALICS][P]I've only just started learning how to make friends.")
	fnDialogue("Thought: Why did I think I could do it with one of the most popular boys in high school?")
	
	--Variable tracking.
VM_SetVar("Root/Variables/System/Romance/sLastWalkHome", "S", "Todd")
VM_SetVar("Root/Variables/System/Romance/iToddFriend", "N", 0.0)

--Execute the next script in sequence.
LM_ExecuteScript(gsRoot .. "../Chapter 2/Gameplay_SceneC/0 - Home Time.lua", "Start")
end





