--[Response to Mari]
--Used after walking home with Mari. Only that path can access this file.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)
	
--Save the game.
if(PairanormalLevel_IsLoading() == false) then
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Starting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

--Set data.
else
	PairanormalLevel_FlagCheckpoint("Starting")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/WalkHome")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/WalkHome")
	fnDialogue([[ [Music|Love][SHOWCHAR|Mari|Neutral]Thought: [Focus|Mari]... ]])
end


--[Execution]
--Ask her outright.
if(sTopicName == "Ask") then
	fnDialogue("Player: Uh... Mari..I w-was wondering...")
	fnDialogue("Mari: [E|Neutral]Yes?")
	fnDialogue("Player: Wondering[GLITCH]...[P] if... you...[P] guhhh [GLITCH][P][P][P]gabbaglasdk??!?")
	fnDialogue("Thought: [SFX|World|Ugh]Aaaand I'm crying. ")
	fnDialogue("[MUSIC|Null][BG|Null]Thought: [P][P][GLITCH]The thought alone of such a horrible thing makes my chest tight,[GLITCH] and the world spins.")
	fnDialogue("Thought: I'm sure I'll collapse at any moment,[GLITCHTEXT]MEHELPYOU I CAN H [GLITCH]ELPYOU[ENDGLITCHTEXT] but Mari's arms quickly encircle me, holding my shaking body steady")
	fnDialogue("Mari: [E|Shock][CRASH]Hey, hey![P] It's okay! I'm right here, [PLAYERNAME]![P][E|Sad] Focus on my voice, okay? Take some deep breaths. In...[P]out. [P]In...[P]out")
	fnDialogue("Thought: I'm obliged to follow her instructions.")
	fnDialogue("Player: In...[P]out.")
	fnDialogue("[BG|Root/Images/Backgrounds/All/WalkHome]Thought: [P].[P].[P].")
	fnDialogue("[CHARENTER|Mari]Mari: [E|Neutral]There you go. You're getting it.")
	fnDialogue("Player: *sigh*... I'm... sorry.[P] I felt like I was... I felt...")
	fnDialogue("Mari: [E|Shock]I'm no doctor or anything, but that looked kinda like a panic attack to me.[P][E|Sad] I get them from time to time, too.")
	fnDialogue("Thought: Mari straightens my collar which had shifted out of place in the commotion, and I'm so red I can barely meet her gentle eyes.")
	fnDialogue("[Music|Love]Thought: Mari, friendly and kind Mari![P] I've never felt used by her before, like her intentions were somehow dishonest.")
	fnDialogue("Thought: If I were ever to question her, the offense caused would [ITALICS]certainly[ENDITALICS] ruin our blossoming friendship.")
	fnDialogue("Thought: I hastily swallow my concerns like the raw lump forming in my throat.")
	fnDialogue("Thought: Making sure I can stand on my own, she starts sidling back down the street.")
	fnDialogue("Mari: [E|Shock]We went through, like, A LOT today, huh?[P] Let's get you home.[P][E|Neutral] Take it easy, and we'll catch up tomorrow, all righty? ")

--Say nothing
elseif(sTopicName == "Nothing") then
	fnDialogue("Player: Oh...[P] It's nothing.[P] Must've been something I ate, is all.")
	fnDialogue("Mari: [E|Sad]OOOh, that's the worst![P] Sorry you're going through that, [PLAYERNAME].")
	fnDialogue("Mari: [E|Neutral] That reminds me of the time I went on this all-seafood diet.[P][E|Mad] I won't bore you with the details, but it sure as hell wasn't pretty.")
	fnDialogue("Player: *gulp*")
	fnDialogue("Thought: Mari, friendly and kind Mari![P] I've never felt used by her before, like her intentions were somehow dishonest.")
	fnDialogue("Thought: If I were ever to question her, the offense caused would [ITALICS]certainly[ENDITALICS] ruin our blossoming friendship.")
	fnDialogue("Mari: [E|Happy]Just remember to get lots of rest, and stay strong![P] I believe in you, and whatever you do[P][CRASH][E|Shock] don't buy sushi from a gas station.")
	fnDialogue("Player: Heh. Noted.")
	fnDialogue("Thought: But now that I've asked the question, it will irritate and itch the back of my mind forever.[P] Because...[P]I don't really know, do I?")
	fnDialogue("Mari: [E|Neutral]You coming?")

    
--change the subject.
elseif(sTopicName == "Change") then
	fnDialogue("Thought: Instead of answering truthfully, I point to the strangest thing I can see, which in this moment is a tall pole strung up with several wires.")
	fnDialogue("Thought: They lead into every house on the block.")
	fnDialogue("Player: What are those? [P]I-I've always wondered.")
	fnDialogue("Thought: Mari smiles and keeps walking, happy to provide an explanation.[P] [ITALICS]Crisis averted.[ENDITALICS]")
	fnDialogue("Mari: [E|Neutral]Oh, those are telephone poles. They're wired up with... electricity? telephone lines, I guess?[P][E|Blush] I'm actually not a hundred percent sure, haha.")
	fnDialogue("Thought: Mari, friendly and kind Mari![P] I've never felt used by her before, like her intentions were somehow dishonest.")
	fnDialogue("Thought: If I were ever to question her, the offense caused would [ITALICS]certainly[ENDITALICS] ruin our blossoming friendship.")
	fnDialogue("Thought: I hastily swallow my concerns like the raw lump forming in my throat.")
	fnDialogue("Mari: [E|Neutral]--But I know they're very important.[P] If one of them falls over in a storm, or breaks,[E|Shock] it could be dangerous!")
	fnDialogue("Mari: [E|Sad] There's power outages, and dudes in vans have to come by to fix them up before anyone..[P].[P] [E|Shock]Hey, wait a minute.")
	fnDialogue("Thought: Mari is frozen, eyes lit up like a tv screen.")
	fnDialogue("Mari: [E|Shock]That sounds an awful lot...[P][P][CRASH][E|Mad]LIKE SOME KIND OF CONSPIRACY!")
	fnDialogue("Player: [SFX|World|Ugh]Huh?")
	fnDialogue("Mari: [E|Happy]I don't actually know what telephone lines are for! Nobody does![P][E|Mad] But they're always there, aren't they?")
	fnDialogue("Mari: [E|Neutral] Transmitting all kinds of stuff.[P] Important stuff![P][CRASH][E|Mad] SECRET STUFF!")
	fnDialogue("Mari: [E|Happy]This is great, [PLAYERNAME]. We've got to do a proper investigation with the club some time.")
	fnDialogue("Mari: [E|Shock]Wait, hang on, I'm gonna get some pictures.[P] That telephone pole over there looks [E|Mad][CRASH][ITALICS]particularly suspicious[ENDITALICS].")
	fnDialogue("[CHAREXIT|Mari]Thought: As Mari scurries over to the 'suspicious' pole, the sickening panic rises in my gut all over again.")
	fnDialogue("Thought: The paranormal Club captain is naturally curious about everything, something I used to find charming.")
	fnDialogue("Thought: But until now I never considered...[P] I have to choke back a whimper.")
	fnDialogue("[CHARENTER|Mari]Mari: [E|Sad]Darnit! It's no good--I'd need some kind of ladder or something to get up there.[P][E|Neutral] I'll make sure to note this so we can go over it with the club.")
end

--Dialogue rejoins here.
fnDialogue("Thought: I walk towards her with the enthusiasm of someone retrieving a dropped cellphone from a toilet bowl.")
fnDialogue("Thought: I wish I could know what she felt about me, but without the embarassment of [ITALICs]asking[ENDITALICS] outright.")
fnDialogue("Thought: [SFX|World|Blink][GLITCH]!!![P] Wait a minute--I totally can![P] I'm a freakin' mind reader![P][ITALICS]There's a way for me to find out[ENDITALICS]")
fnDialogue("Mari: [E|Neutral]Welp, I guess this is where we go our seperate ways!")
fnDialogue("Player: ... Huh?")
fnDialogue("Mari: [E|Neutral]Isn't that your house over there? ")
fnDialogue("Player: Oh.[P][SFX|World|Ugh] Uh, yeah. It is.")
fnDialogue("Mari: [E|Neutral]Oooooookie dokie, I'll be going then.[P][E|Shock] Sorry I'm in such a rush today,[E|Blush] but the club meeting ran a little over,")
fnDialogue("Mari: and I really want to get home to check if the latest chapter of 'Demon Slayer 3: My Motorbike is Actually a Demon, and She's in Love with Me?' came out.")
fnDialogue("[CHAREXIT|ALL]Player: w-wait! [GLITCH]I...")
fnDialogue("Thought: [ITALICS]... I didn't get to read your mind yet![ENDIT] Mari's gone before I can finish that declaration, thank goodness.")
fnDialogue("Player: [SFX|Ugh]--Dangit!")
fnDialogue("Thought: I have no choice but to figure out some way to get close to her again.[P] After all, I don't really have a handle on my abilities yet.")
fnDialogue("I'll need to arrange some kind of one-on-one meeting type of thing; intimate.[P] quiet.[P] without too many distractions so she can't get away. ")
fnDialogue("Player: Like...[P]oh, what do people call thing?[P] kidnapping? No.[P] Uhh... dates! Yeah, that's it.")
fnDialogue("Thought: [P][P][ITALICS]I need to take Mari on a date.[ENDITALICs]")

--Variable tracking.
VM_SetVar("Root/Variables/System/Romance/sLastWalkHome", "S", "Mari")
VM_SetVar("Root/Variables/System/Romance/iMariFriend", "N", 0.0)
VM_SetVar("Root/Variables/System/Romance/iStacyFriend", "N", 0.0)

--Execute the next script in sequence.
LM_ExecuteScript(gsRoot .. "../Chapter 2/Gameplay_SceneC/0 - Home Time.lua", "Start")
