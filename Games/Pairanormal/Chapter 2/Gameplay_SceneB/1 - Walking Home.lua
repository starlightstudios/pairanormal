--[Prototype]
--Quick description of where we are in the scenario, or more likely, a joke only funny to Salty.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)
	
--Save the game.
if(PairanormalLevel_IsLoading() == false) then
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Starting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

--Set data.
else
	PairanormalLevel_FlagCheckpoint("Starting")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/ClubHouse")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/ClubHouse")
    if(sTopicName == "Laura" or sTopicName == "TheGood" or sTopicName == "Todd") then
        PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Library")
        PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Library")
    end
	fnDialogue([[ [Music|DaytimeRedux]Thought: ...]])
end

--Scene setup.
fnCutsceneInstruction([[ Dia_SetProperty("Investigation Time Max", -1) ]])
fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
fnCutsceneInstruction([[ Dia_SetProperty("Clear Investigation") ]])
fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
fnCutsceneBlocker()

-- |[Execution]|
--Mari!
if(sTopicName == "Mari") then
    
    --Variable gain.
    local iMariFriend  = VM_GetVar("Root/Variables/System/Romance/iMariFriend", "N")
    VM_SetVar("Root/Variables/System/Romance/iMariFriend", "N", iMariFriend + 10.0)
    
    --Dialogue.
	fnDialogue("[CHARENTER|Mari][CHARENTER|Stacy]Player: Hey guys.")
	fnDialogue("Mari: [E|Neutral]Yeah!")
	fnDialogue("Player: Wanna walk home together?")
	fnDialogue("Mari: [E|Happy]Do I?[P][CRASH] DO I???[P][E|Neutral] Wait, do I? [P]Haha, psych! just kidding, of course I do--You coming, Stacy?")
	fnDialogue("Stacy: [E|Blush]Aw, thanks for inviting me.[P][E|Neutral] But I think I could use a solo stroll today.")
	fnDialogue("Mari: [E|Neutral]Sure thing.[P] Okay, [E|Shock]let's go!")
	fnDialogue("[CHAREXIT|ALL][BG|NULL]Thought: She waves goodbye to Stacy and the other club members,[P] and we make our way back to the suburban streets.")
	fnDialogue("[BG|Root/Images/Backgrounds/All/WalkHome][MUSIC|Love]Thought: I listen to Mari chatter excitedly about all the things we discovered on our mission.")
	fnDialogue("Thought: I tell myself that my quick pulse is because of the unbelievable encounters we had,")
	fnDialogue("Thought: and not because the captain's energy is so delightful and infectious to me.")
	fnDialogue("[CHARENTER|Mari]Mari: [E|Neutral]Hey, [PLAYERNAME].")
	fnDialogue("Player: Yeah?")
	fnDialogue("Mari: [E|Sad]Do you like being part of the club?")
	fnDialogue("Player: [SFX|World|Blink]O-of course![P] It's a lot of fun going on all these investigations together.")
	fnDialogue("Mari: [E|Happy]Eeeeee, yes! I'm so happy to hear you say that.[P][E|Shock] Of course, the investigations are THE BEST PART of our club!")
	fnDialogue("Mari:[E|Blush]The thrill of the next discovery, the tantalizing promise of [CRASH][E|Shock]demon boyfriends! ")
	fnDialogue("Thought: [ITALICS]Boy, she's really hung up on those demon boyfriends...[ENDITALICS]")
	fnDialogue("Mari: [E|Mad]--No stone is be left unturned,[P] no subject is out of our realm!")
	fnDialogue("Mari: [E|Shock][CRASH] Suspicious places,[P][CRASH][E|Blush] mysterious people![P][E|Neutral] Investigating them makes the club a blast.")
	fnDialogue("Player: Right, right--")
	fnDialogue("Thought: Mysterious people..[P].[P].[P].Mysterious...[P][ITALICS]people?[ENDITALICS]")
	fnDialogue("Thought: [P][P][P] My stomach drops.[P] Of course--but, it can't be, can it?[P] I stop, terrified of the horrible realization I've made.")
	fnDialogue("Player: Mari.")
	fnDialogue("Mari: [E|Neutral]Hm?")
	fnDialogue("Thought: Was I only invited to the paranormal club so...[ITALICS]so you could investigate me[ENDITALICS]?")
	fnDialogue("Thought: That's all I ever was to them, wasn't it? Just like the bacteria we had to examine in science class.")
	fnDialogue("Thought: They were all watching me through a glass, wondering if I would grow hair if they gave me a drop of honey to eat.")
	fnDialogue("Thought: A mystery.[P] A...[P][ITALICS]subject[ENDITALICS].[P] I feel like I'm about to puke.")
	fnDialogue("Mari: [E|Shock]Omigosh, what's the matter, [PLAYERNAME]? You look like you're about to puke.")
	fnDialogue("Thought: My dry lips part, but I can't say it.[P] I can't bear to be proven right. ")
	fnDialogue("Player: Uh...")

    --Decision Set.
    fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (Say something!)") ]])

    local sDecisionScript = "\"" .. gsRoot .. "../Chapter 2/Gameplay_SceneB/2 - ResponseToMari.lua" .. "\""
    fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Ask her outright\", " .. sDecisionScript .. ", \"Ask\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"It's nothing\", " .. sDecisionScript .. ", \"Nothing\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Change the subject\", " .. sDecisionScript .. ", \"Change\") ")
    fnCutsceneBlocker()

--Stasheeee!
elseif(sTopicName == "Stacy") then
    
    --Variable gain.
    local iStacyFriend = VM_GetVar("Root/Variables/System/Romance/iStacyFriend", "N")
    VM_SetVar("Root/Variables/System/Romance/iStacyFriend", "N", iStacyFriend + 10.0)
    
    --Dialogue.
	fnDialogue("Thought: I keep finding myself eyeing Stacy[P]. Maybe she'd like to walk home with me like last time.")
	fnDialogue("Thought: I should ask her--hmm, bout how would I go about it? I'm not exactly--")
	fnDialogue("[CHARENTER|Stacy]Stacy: [E|Neutral]Hey [PLAYERNAME]. What say you and I mosey home together?")
	fnDialogue("Player: Wh--uhh, yes![P] Yes, sounds great. I'll go get Mari.")
	fnDialogue("Stacy: [E|Blush]Ahhh, well, this time.[P] I was kind of hoping it would just be... you and me. Get my drift?")
	fnDialogue("Player: Sure. Yeah, I would like that,[P] only... What does [ITALICs]get my drift[ENDITALICS] mean? ")
	fnDialogue("Stacy: [E|Neutral]Wow, I have so much to teach you about being cool.[P] Come on.")
	fnDialogue("Thought: We say goodbye to Mari and the rest of the club, and start to head back.")
	fnDialogue("Michelle: [E|Shock]Ah![P][P][E|Blush] Oh, it's you, weirdo.")
	fnDialogue("[CHAREXIT|ALL][BG|Root/Images/Backgrounds/All/WalkHome][MUSIC|Love][CHARENTER|Stacy]Stacy: [E|Happy]--And I just finished taking my third semester of French.")
	fnDialogue("Stacy:[E|Neutral]It's actually very easy to learn French if you know Spanish, because they share a Latin root.")
	fnDialogue("Player: What is that?")
	fnDialogue("Stacy: [E|Neutral]It's an older language, one that isn't spoken anymore.[P] But it evolved and became lots of different languages, which share similarities.")
	fnDialogue("Thought: Walking down the suburban sidewalks, I let Stacy do most of the talking.")
	fnDialogue("Thought: She has such a bold personality,[P] I can't help but feel a little shy around her.")
	fnDialogue("Stacy: [E|Neutral]For example,[P][E|Blush] [ITALICS]vous etes belle[ENDITALICS].")
	fnDialogue("Thought: She leans in close, her thin lips curling into a mischievous smile.")
	fnDialogue("Stacy: The French phrase is very similar to the Spanish, [ITALICS]tu eres bella[ENDITALICS].[P] The latin root is [ITALICS]belle[ENDITALICS].")
	fnDialogue("Player: Okay. What does it mean?")
	fnDialogue("Stacy: [E|Blush]'You are beautiful', of course.[P] Isn't that nice?")
	fnDialogue("Player: Hm...[P][SFX|Ugh] I guess so.[P] I think I need to get better at English before I try to tackle any other languages.")
	fnDialogue("Stacy: [SFX|Ugh][E|Sad]R-Right.[P][E|Mad] Of course, silly me. Anyway--[P][E|Shock]agggh!")
	fnDialogue("Player: Whoah! You okay?")
	fnDialogue("Stacy: [E|Neutral] Ahah, I'm fine, honestly.[P] Ngh.[BR] I got a little scratch sliding home when we were playing softball in PE today.")
	fnDialogue("Stacy: No big deal. I-I should probably put a bandaid on it, though!")
	fnDialogue("[CHAREXIT|Stacy]Thought: I'm surprised when Stacy slings off her backpack and pulls one out.[P] Does this sort of thing happen often?")
	fnDialogue("Stacy: Hrrrk... Almost. Got. It!!!")
  
    --Decision Set.
    fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (What should I do?)") ]])

    local sDecisionScript = "\"" .. gsRoot .. "../Chapter 2/Gameplay_SceneB/9 - ResponseToStacy.lua" .. "\""
    fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Help her put it on\", " .. sDecisionScript .. ", \"Help\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Stand there\", " .. sDecisionScript .. ", \"Stand\") ")
    fnCutsceneBlocker()
	
--RAWWWRR!
elseif(sTopicName == "Michelle") then
    
    --Variable gain.
    local iMichelleFriend = VM_GetVar("Root/Variables/System/Romance/iMichelleFriend", "N")
    VM_SetVar("Root/Variables/System/Romance/iMichelleFriend", "N", iMichelleFriend + 10.0)
    
    --Dialogue.
	fnDialogue("Thought: I decide to head home with Michelle.")
	fnDialogue("Thought: She tries to sneak off silently on her own, but I manage to catch up with her in the nick of time.")
	fnDialogue("Player: Hi, Michelle.")
	fnDialogue("[CHARENTER|Michelle]Michelle: [E|Shock]Ah![P][P][E|Blush] Oh, it's you, weirdo.")
    fnDialogue("Michelle: [E|Blush]Well[P][P]... Come on, then.")
    fnDialogue("[CHAREXIT|ALL][BG|BLACK]Thought:...")
	fnDialogue("[BG|Root/Images/Backgrounds/All/WalkHome][MUSIC|Love]Thought: We trudge out of the woods in silence.")
	fnDialogue("Thought: I watch the sun dip behind the rooftops as our sneakers slap against the pavement... [ITALICS][P]*plap* [P]*plap* [P]*plap*.[ENDITALICS]")
	fnDialogue("Thought: I get into a sort of lulled state, walking, watching trees go by.")
	fnDialogue("[CHARENTER|Michelle]Michelle: [CRASH][E|Mad]WELL?")
	fnDialogue("Player: [P][P][P]Huh? [P]What?")
	fnDialogue("Michelle: [E|Mad]Where are you going?")
	fnDialogue("Player: Wh...[P][SFX|World|Blink]what?")
	fnDialogue("Michelle: [E|Mad]Where the hell are you [ITALICS]going,[ENDITALICS][P] weirdo?[P] We're almost near Main Street.")
	fnDialogue("Player: I.[P].[P].[P][SFX|World|Ugh] I'm just following you.")
	fnDialogue("Michelle: [CRASH][E|Shock]WHAT???[P][CRASH][E|Mad] I'VE BEEN FOLLOWING YOU!!!")
	fnDialogue("Player: Me?[P] Why?")
	fnDialogue("Michelle: [E|Neutral]I DUNNO. [E|Sad]You walk like you know where you're going.")
	fnDialogue("Player: I recently awoke from a coma.[P] I never know where I'm going.")
	fnDialogue("Michelle: [E|Shock] Oh, come on then.[P]  Christ.")
	fnDialogue("Player: Wh... where are we headed now?")
	fnDialogue("Michelle: [E|Happy] Don't worry so much.[P]  Have I ever led you wrong?")
	fnDialogue("Player: I mean...[P] are we not counting what just happened a few moments ago?")
	fnDialogue("Thought: She laughs, and I can't help but join in as she deftly ducks in between two town houses.")
	fnDialogue("Thought: [CHAREXIT|ALL] With the sun so low in the sky, it's not long before the shadows slung from the stout brick buildings completely cover us.")
	fnDialogue("Thought: My eyes have a hard time adjusting and my toe keeps tripping on debris,")
	fnDialogue("Thought: but Michelle stomps through the mazelike pathways of backalleys and rotten fences with the confidence of someone who knew all their intricacies by heart.")
	fnDialogue("[BG|Root/Images/Backgrounds/All/Grafitti]Player: a-[P] are we allowed to be back here... do you think?")
	fnDialogue("Michelle: Suuuuure we are. I haven't been told otherwise.")
	fnDialogue("Thought: I can only see her wide shoulders in front of me,[P] but somehow I can picture the cheeky smile on that's definitely on her face.")
	fnDialogue("Player: [CRASH]Oof! ")
	fnDialogue("[CHARENTER|Michelle]Michelle: [E|Blush]Hey! Watch those hands, you perv!")
	fnDialogue("Player: [SFX|World|Blink]Sorry! I was just losing my balance, I didn't mean to bump in to you--[P] Er, what's a ''perv''?")
	fnDialogue("Michelle: [E|Blush]N-nothing! I was just saying--You don't want to fall over.[P] Lots of broken glass on the floor.")
	fnDialogue("Player: Right.")
	fnDialogue("Thought: My attention starts to wander to the walls closed around us.")
	fnDialogue("Thought:Someone has drawn lots of unusual, intricate doodles over the rough surface--I feel the instinct to reach out and touch it.")
	fnDialogue("Thought: Michelles stops here, too.")
	fnDialogue("Thought: Her bookbag spills open on the ground, (it's as messy as I expected it would be inside) and she rummages for something at the bottom.")
	fnDialogue("Michelle: [E|Neutral] Here we are! ")
	fnDialogue("Thought: She proudly holds forward a tall can topped with a nozzle leaking deep purple pigment.")
	fnDialogue("Michelle: [E|Happy]What's an artist without her tools?")
	fnDialogue("Player: [P][P]Wait a minute--you're gonna [ITALICS]draw[ENDITALICS] on... [P]b-but this is someone's house!")
	fnDialogue("Player: They're gonna be mad if[P]--okay, and you're already drawing,[P] you are completely ignoring me and probably some laws..")
	fnDialogue("Thought: A few moments of watching Michelle work, and I can't quite remember what I was saying.")
	fnDialogue("Thought: The can rattles a couple times in her hands, then she holds it to the canvas.[P] A cloud of colored gas bursts out of the tip.")
	fnDialogue("Michelle: [E|Happy]It's not drawing, dope.[P] It's grafitti.")
	fnDialogue("Thought: Turning her arm, and sometimes her whole body, she makes the lines sharp and small, or wide and fuzzy.")
	fnDialogue("Thought: In just a few moments, there's a new intricate figure borne on the brick.")
	fnDialogue("Michelle: [E|Neutral]It's like...[P]art, but deeper.[P] More [ITALICS]raw[ENDITALICS], or whatever. ")
	fnDialogue("Player: Right, right. [ITALICS]Raw.[ENDITALICS][P] Like... meat.[P] *ahem* Art meat.[P] I totally understand this.")
	fnDialogue("Thought: Scoffing, she wipes her hands on the knee of her ripped stockings and chucks the can at me.")
	fnDialogue("Michelle: [E|Neutral]Try it.")
	fnDialogue("Player: ...")
	fnDialogue("Michelle: [E|Mad]It's real simple, yeah?[P] [E|Blush]Just push on the tip there--like this.")
	fnDialogue("Thought: She grabs my hand and finger by finger, arranges it around the cylinder-- [P]which is at first cold, but quickly heated up by the blood racing to all my extremities.")
	fnDialogue("Thought: Once satisfied with my position, she steps back.[P] I look from her to the wall uneasily. ")
	fnDialogue("Michelle: [E|Neutral]Go 'head and paint.[P] Anything that comes to mind.")
	
    
    --Decision Set.
    fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (What should I draw?)") ]])

    local sDecisionScript = "\"" .. gsRoot .. "../Chapter 2/Gameplay_SceneB/3 - ResponseToMichelle.lua" .. "\""
    fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Draw something simple.\", " .. sDecisionScript .. ", \"Flower\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Draw random scribbles.\", " .. sDecisionScript .. ", \"Scribble\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Draw what you're feeling.\", " .. sDecisionScript .. ", \"Feeling\") ")
    fnCutsceneBlocker()

--Bret is well known as the most crushable of the group.
elseif(sTopicName == "Bret") then
    
    --Variable gain.
    local iBretFriend = VM_GetVar("Root/Variables/System/Romance/iBretFriend", "N")
    VM_SetVar("Root/Variables/System/Romance/iBretFriend", "N", iBretFriend + 10.0)
    
    --Dialogue.
	fnDialogue("Thought: I decide to walk home with Bret.")
	fnDialogue("[CHARENTER|Bret]Player: Hey Bret,[P] can I walk home with you?")
	fnDialogue("Bret: [E|Shock]Huh?[P] Me?[P][E|Blush] Me-e?[P] I...")
	fnDialogue("Thought: Bret is fascinated with a bottlecap in his hand,[P] and is so startled by my proposal he loses his grip on it.")
	fnDialogue("Thought: It fumbles and flips through the air as he tries to catch it--")
	fnDialogue("Thought: After a few seconds of this struggle,[P] he finally regains his composure, stuffing the cap in his pocket.")
	fnDialogue("Bret: [E|Happy]Yes! Yes, I would![P][P][E|Mad] I mean, wait, no,[P] I have to pick up my [E|Blush]brother from the doctor--")
	fnDialogue("Bret: [E|Shock]y'know what?[P][E|Neutral] He'll be fine.")
	fnDialogue("Player: Are you sure?")
	fnDialogue("Bret: [P][P][SPEEDTEXT]--I'll text him, he'll be juuuust fine. Let's go before you change your mind please!![ENDSPEEDTEXT]")
	fnDialogue("Player: O-okay.")
	fnDialogue("Thought: Bret and I exit the the forest at a brisk pace.")
	fnDialogue("Thought: I notice he keeps his hand in his pocket the whole time.")
	fnDialogue("[CHAREXIT|ALL][BG|Null]Thought: ...")
	fnDialogue("[CHAREXIT|ALL][BG|Root/Images/Backgrounds/All/WalkHome]Thought: Only when we reach pavement do I see him draw out the bottlecap again.")
    fnDialogue("Player: I notice you've got something there.")
    fnDialogue("[CHARENTER|Bret]Bret: [E|Shock]Oh, this?[P][E|Blush] Y-yeah.[P] It's from my collection.")
    fnDialogue("Bret: [E|Sad]I was looking at it this morning,[E|Shock] accidentally put it in my pocket, and now I'm like, paranoid it's gonna fall out!")
    fnDialogue("Bret: Sorry, I'm just like,[P] totally obsessed with these things.")
    fnDialogue("Player: Must be nice to live in a place that has its own brand of soda.")
    fnDialogue("Player: Even if the only thing people use it for is pranks.")
    fnDialogue("Bret: [E|Sad]Yeah... sorry.")
    fnDialogue("Player: It's not really a big deal.")
    fnDialogue("Bret: [E|Neutral][P][P][P]Um,[P] do you want to know a fun fact about Kane Kola?")
    fnDialogue("Thought: [ITALICS]Nooooooo.[ENDITALICS]")
    fnDialogue("Player: Sure.")
	fnDialogue("Bret: [E|Neutral]There's actually two factories here in town.")
	fnDialogue("Bret: [E|Neutral]One is the old Kane Kola factory,[P][E|Shock] but it got closed down just five years after being built.")
	fnDialogue("Bret: [E|Neutral] When it reopened in 1979,[P] the soda was totally rebranded.")
	fnDialogue("Bret: [E|Shock]Old Charlie from the Pizza place says it used to taste different, too.[P][E|Sad] I guess the formula changed.")
	fnDialogue("Bret: [E|Neutral]Anyway,[P] [E|Mad]that's why those old bottlecaps are so hard to find--[P]there aren't many around!")
	fnDialogue("Player: Oh.")
	fnDialogue("Bret: [E|Blush]... Anyway, that's a--[P][E|Mad]psht![P] Sorry,[P] it's probably not interesting to anyone but me right?")
	fnDialogue("Bret: [SPEEDTEXT][E|Sad] Look at me, getting all giddy over cola formulas![E|Happy] Hmm, yes!")
	fnDialogue("Bret: [E|Mad] Nothing like a specific blend of orange peel, vanilla and cinammon--to splash over the unsuspecting new student's pant leg![ENDSPEEDTEXT][P][E|Sad] So refreshing!")
	fnDialogue("Player: H-heh... haha, you said it all right.")
	fnDialogue("Bret: [E|Neutral][SFX|World|Ugh]Ugh...[P][E|Sad]sorry.[P] You probably hate soda since it got splashed on you.[P] And now I won't shut up about it.[P] Yaaay Bret.")
	fnDialogue("Thought: I bite my lip, trying to ignore the bitter taste in my mouth.[P] For all of Bret's kindness towards me, it's always been underscored by his subtle self-deprecation.")
	fnDialogue("Thought: I don't think I like it, for some reason... but how do I express that?")
    
    
    --Decision Set.
    fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (What should I say to Bret?)") ]])

    local sDecisionScript = "\"" .. gsRoot .. "../Chapter 2/Gameplay_SceneB/4 - ResponseToBret.lua" .. "\""
    fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Try to change the subject\", " .. sDecisionScript .. ", \"Change\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Try to explain how you're feeling\", " .. sDecisionScript .. ", \"Explain\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"I'm fine! keep going!\", " .. sDecisionScript .. ", \"Fine\") ")
    fnCutsceneBlocker()

--Laura time. There's no way this will go badly.
elseif(sTopicName == "Laura" or sTopicName == "TheGood") then
    
    --Variable gain.
    local iLauraFriend = VM_GetVar("Root/Variables/System/Romance/iLauraFriend", "N")
    VM_SetVar("Root/Variables/System/Romance/iLauraFriend", "N", iLauraFriend + 10.0)
    
    --Dialogue.
	fnDialogue("Thought: It's a long shot, but I decide to walk home with Laura.[P] When I find her, she's expertly typing down today's notes on her cellphone.")
	fnDialogue("Player: [BG|Root/Images/Backgrounds/All/Library]Uhh, hey Laura?")
	fnDialogue("[CHARENTER|Laura]Laura: [E|Shock]Yes?")
	fnDialogue("Player: Do you...[P] want to walk home together?")
	fnDialogue("Laura: [E|Shock]Oh![P][P] Y-Yes![P][E|Happy] That sounds *ahem*[P] that sounds fun!")
	fnDialogue("Thought: I breathe a sigh of relief, pushing my backpack further up my shoulder to stable it.")
	fnDialogue("Thought: Laura slings her stylish knapsack over her own shoulder, and we make our way out.")
	fnDialogue("[CHAREXIT|ALL][BG|Root/Images/Backgrounds/All/Hallway][MUSIC|Love]Thought: ...")
	fnDialogue("[CHARENTER|Laura]Laura: [E|Blush]Oh, j-[P]just a moment, [PLAYERNAME].")
	fnDialogue("Player: ?")
	fnDialogue("Thought: We stop outside of what I discover is Laura's locker.")
	fnDialogue("Thought: She quickly enters the combination of the lock,[P] and pulls out a couple of bulky shoes with wheels attached to the bottom.")
	fnDialogue("Laura: [E|Neutral]Do you like them? ")
	fnDialogue("Player: Oh yeah. Those are really cool...[P]uh...[P][SFX|World|Ugh]car shoes.")
	fnDialogue("Laura: [E|Happy]Rollerskates.")
	fnDialogue("Player: mm-er-skates of course, that's also... you know,[P] the common[P]--I was using the [ITALICS]street[ENDITALICS] term.")
	fnDialogue("Laura: [E|Happy]Sure you were. *giggle* I'm just *cough*[P] gonna take them home with me.")
	fnDialogue("[CHAREXIT|ALL][BG|Null]Thought: The atmosphere starts to thicken as the minutes of silent walking continue; neither of us can think of a good topic of conversation.")
	fnDialogue("[BG|Root/Images/Backgrounds/All/WalkHome]Player: Um...[P] Laura?")
	fnDialogue("[CHARENTER|Laura]Laura:[E|Neutral] Mm?")
	fnDialogue("Player: Do you.[P].[P].[P]think I'm annoying,[P] or something?")
	fnDialogue("Laura: [CRASH][P][P][P][E|Mad]Wh-wh-what?")
	fnDialogue("Player: I dunno.[P] I know you said you were shy,[P] but lately you haven't been hanging out with the club at all.")
	fnDialogue("Player: And I'm wondering...[P][SFX|World|Ugh] is it me?[P][P] Do you.[P].[P].[P]not like me?")
	fnDialogue("Thought: Laura gasps, bringing her hands to her lips.")
	fnDialogue("Laura: [E|Mad][PLAYERNAME]... O-[P]o-[P]of course not!")
	fnDialogue("Laura: I...[P] It's just that.[P][E|Blush] I've j-[P]j-[P]just been... *ahem*[P] *hrm*[P] p-preoccupied... [P]with... um...")
	fnDialogue("Thought: The rollerskates she was holding clatter to the ground, and she stumbles over them.")
	fnDialogue("Player: Whoah!")
	fnDialogue("Thought: I reach out to catch her, but its too late.")
	fnDialogue("Thought: She plops down on the ground,[P] the contents of her bag and skates scattered every which way.")
	fnDialogue("Laura: [E|Sad]I'm sorry![P] Oh, I'm sorry, [PLAYERNAME].[P] I've been so preoccupied lately.")
	fnDialogue("Laura: I-[P]I'm such a *ahem*[P] Such a mess...")
	fnDialogue("Player: What's the matter? ")
	fnDialogue("Laura: [E|Sad][PLAYERNAME]... can you keep a secret?")
	fnDialogue("Player: Hm.")
	fnDialogue("Thought: I sit down on the sidewalk with her.[P] She sighs.")
	fnDialogue("Player: I don't know. I've never tried.")

    --Save the game.
    fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"ASecret\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
    PairanormalLevel_FlagCheckpoint("ASecret")

	fnDialogue("Laura: [BG|Root/Images/Backgrounds/All/WalkHome][E|Blush]Ah, right.[P] Well, [PLAYERNAME], the truth is, my head's been a little frazzled, on... on account of...")
	fnDialogue("Laura: [CRASH][E|Mad] I've got a crush on someone!")
	fnDialogue("Thought: When Laura says this, my shirt suddenly feels too small for my body.")
	fnDialogue("Thought: Isn't a crush...[P]having special feelings for someone? [P]I'm at a loss for words.")
	fnDialogue("Player: C...[P]ongratulations?")
	fnDialogue("Thought: Laura shakes her head.")
	fnDialogue("Laura: [E|Sad]No, congratulations are definitely [ITALICS]not[ENDITALICS] in order.[P] It's simply awful, you see.")
	fnDialogue("Thought: She clutches her head, rocking back and forth gently.")
	fnDialogue("Laura: [E|Sad]I met him... on the computer.[P] He's a hacker like me...")--IS HIS NAME ZERO COOL?
	fnDialogue("Laura: We both found an old chat application buried in the school's archived file directories.")
	fnDialogue("Thought: I nod, unsure of what to do with my hands.[P] I decide to help pick up her things while she talks.")
	fnDialogue("Laura: [E|Blush]We talk about school life, things like that. He's got a brother who drives him crazy, and I get bullied sometimes...")
	fnDialogue("Laura: [E|Neutral]We've got -so- much in common. And he's really funny.")
	fnDialogue("Laura: [E|Happy] Sometimes he'll print a picture out for me.[P] A little funny cat, or something. ")
	fnDialogue("Thought: Having never experienced the symptoms of a crush myself, I'm fascinated by the way Laura's demeanor changes just talking about this person.")
	fnDialogue("Laura: [E|Blush]He always knows what to say to cheer me up...[P][E|Sad] Oh, I've got it real bad.")
	fnDialogue("Thought: So, this is what having a crush is like.[P] From my own basic understanding, it's supposed to be a good feeling.")
	fnDialogue("Thought: Looking at Laura, though, she looks pretty miserable.")
	fnDialogue("Player: So...[P] does he not feel the same way?")
	fnDialogue("Laura: [E|Mad]I-[P]I don't know![P][E|Sad] Maybe!!!")
	fnDialogue("Thought: The bracelet around Laura's wrist must be made of reinforced steel.")
	fnDialogue("Thought: Watching her gnaw it the way she does when she's nervous, it's the only way it could withstand such chew.")
	fnDialogue("Laura: [E|Mad]That chat application is going to disappear with the new school computers.")
	fnDialogue("Laura: We won't be able to talk to each other anymore.[P][E|Sad] And... I don't know what to do!")
	fnDialogue("Thought: Laura suddenly stands up.[P] She begins walking again, absentmindedly leaving all her things on the sidewalk with me.")
	fnDialogue("Thought: I hurriedly scoop them up and follow her.")
	fnDialogue("Laura: [E|Mad]He says he doesn't have a phone OR email.")
	fnDialogue("Laura: Isn't that kind of weird?[P][E|Sad] Doesn't he want to stay in contact?")
	fnDialogue("Player: Well,[P] why don't you just meet up with him?")
	fnDialogue("Laura: [E|Shock][P][P][P][SFX|World|Blink]What?")
	fnDialogue("Thought: She stops abruptly.[P] I'm so startled, I drop her bag and skates again.[P] This time, she notices the commotion.")
	fnDialogue("Laura: [E|Shock]Oh, sorry![P][E|Mad][SFX|Ugh] Ugh, look at me, I'm totally scatterbr--*ahem*[P][E|Blush] totally scatterbrained right now.")
	fnDialogue("Thought: She slings on her bookbag and picks up the left skate, which clattered near her.[P] I grab the right.")
	fnDialogue("Laura: [E|Mad]I've already considered that possibility--[P]I [ITALICS]can't[ENDITALICS] meet up with him. ")
	fnDialogue("Player: Are you scared he might not be like what you're expecting?")
	fnDialogue("Laura: [E|Mad]No, no... [P][E|Blush]well, maybe, a little.")
	fnDialogue("Laura: [E|Sad] But I guess I'm more worried he might not like...[P][P][P] [ITALICS]me.[ENDITALICS]")

    --Save the game.
    fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"WhatToSay\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
    PairanormalLevel_FlagCheckpoint("WhatToSay")
    
	fnDialogue("Thought: [BG|Root/Images/Backgrounds/All/WalkHome]I see my house in the distance.[P] If I'm going to say something, it's now or never.")
    
    --Decision Set.
    fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (What should I say?)") ]])

    local sDecisionScript = "\"" .. gsRoot .. "../Chapter 2/Gameplay_SceneB/6 - ResponseToLaura.lua" .. "\""
    fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"You have nothing to worry about\", " .. sDecisionScript .. ", \"DoIt\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"You should find out the truth\", " .. sDecisionScript .. ", \"Truth\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"I guess you're right\", " .. sDecisionScript .. ", \"Nope\") ")
    fnCutsceneBlocker()

--THE TODD COUPLE.
elseif(sTopicName == "Todd") then
    
    --Variable gain.
    local iToddFriend = VM_GetVar("Root/Variables/System/Romance/iToddFriend", "N")
    VM_SetVar("Root/Variables/System/Romance/iToddFriend", "N", iToddFriend + 10.0)
    
    --Dialogue.
	fnDialogue("[CHAREXIT|All]Thought: [BG|Root/Images/Backgrounds/All/Library]I decided to see if Todd wants to walk home.[P] Looking around the library, I notice both him and Harper have disappeared.")
	fnDialogue("[BG|Root/Images/Backgrounds/All/Hallway]Thought: Stepping out, I spot them walking through the halls with some other students. [P] My approach is as casual I could hope for.")
	fnDialogue("Player: Oh, uh, hey![P] Todd!")
	fnDialogue("[CHARENTER|Harper][CHARENTER|Todd][CHARENTER|Student2][CHARENTER|Student3][CHARENTER|Student4]Harper: [E|Happy]Hey, [PLAYERNAME]![E|Neutral] nice to see you again, hun.")
	fnDialogue("Todd: [E|Shock][PLAYERNAME]!! Hey! Everyone, this is [PLAYERNAME], they're the new kid.")
	fnDialogue("Students: Heeeey.")
	fnDialogue("Player: Uh, hi guys.")
	fnDialogue("Player: I was wondering, Todd,[P][P] did you maybe want to walk home together?")
	fnDialogue("Thought: Some of the students make a weird 'ooooh' sound.")
	fnDialogue("Thought: I don't know what it means,[P] but based on Todd's reddening face, maybe I should be embarrassed?")
	fnDialogue("Todd: [E|Shock]Oh, well, sure!")
	fnDialogue("Todd: [E|Mad]I mean, [P]I'd love to,[P] it's just that I've already planned to head out with these guys.")
	fnDialogue("Todd: [E|Neutral] You're welcome to join, though.")
	fnDialogue("Player: Yeah, okay, cool!")
	fnDialogue("Thought: [ITALICS]nicely done[ENDITALICS]. I hop in step with the group, and slowly we begin shuffling back to the exit.")
	fnDialogue("[CHAREXIT|ALL][BG|Null]Thought: ...")
	fnDialogue("[MUSIC|Somber][BG|Root/Images/Backgrounds/All/Hallway][CHARENTER|Student3]Student: [FOCUS|Student3]Student: So, [PLAYERNAME],[P] how do you know Todd?")
    fnDialogue("Player: Todd has been helping me out with school stuff.")
    fnDialogue("[CHARENTER|Todd]Todd: [E|Neutral]Oh, yeah.[P] I've been tutoring them for a while now.")
    fnDialogue("Todd: [E|Happy] How am I doing, [PLAYERNAME]?[P] Am I a good tutor?")
    fnDialogue("Thought:  Todd finds himself swarmed with new questions from every direction--interrupting my response.")
    fnDialogue("Thought: I hadn't realized everyone was listening in.")
    fnDialogue("[CHARENTER|Student2][CHARENTER|Student4][CHARENTER|Harper]Student: [FOCUS|Student3][VOICE|Student3] You're a tutor?")
    fnDialogue("Student: [FOCUS|Student4][VOICE|Student4] Todd, you'd be THE BEST teacher!")
    fnDialogue("Harper: [E|Shock]I didn't know you were taking on students --[P][E|Blush] what are your rates?")
    fnDialogue("Todd: [E|Blush] Uhh.[P].[P].[P][SFX|World|Ugh]They're pretty high, actually.")
    fnDialogue("Thought: A handsome boy to my left leans forward,[P] tapping his lead pencil against Todd's shoulder and mock pouting.")
    fnDialogue("Student: [FOCUS|Student2][VOICE|Student2]Teach me afterschooooooooool, Todd!")
    fnDialogue("Thought: Todd laughs, shaking his head.")
    fnDialogue("Todd: [E|Happy]Pf'hahahaa! [P]Sorry, It's kind of a one-student-at-a-time thing.")
    fnDialogue("Todd: [E|Shock]But, hey![P][E|Neutral] If you want, we can all head to my place and form a little 'study group'.[P] What do you say?")
    fnDialogue("Thought: The group's answer is an enthusiastic cheer.")
    fnDialogue("Harper: [E|Happy] I can't wait to study some Stree-[P]t Fight-[P]er.")
    fnDialogue("Student: [FOCUS|Student4][VOICE|Student4] YAAAAASSSS!")
    fnDialogue("Student: [FOCUS|Student2][VOICE|Student2] You're such a great vice president, Todd.[P] Always thinking of the needs of [ITALICS]the people[ENDITALICS].")
    fnDialogue("Student: [FOCUS|Student3][VOICE|Student3] Tutor me, Todd![P] I will pay you in gum.")
    fnDialogue("[CHAREXIT|ALL]Thought: Is it me, or is the group picking up the pace?[P] I can't seem to keep up with them.")
    fnDialogue("Thought: Even though everyone is walking, I find myself drifting around the edge of the cluster of bodies, not quite able to stay in step.")
    fnDialogue("Thought: Sometimes someone will just say a random string of words--")
    fnDialogue("Student: [VOICE|Student2][FOCUS|Student2]Hey, did you forget to pick up your [CRASH][ITALICS]SALMON SANDWICH~?~?[ENDITALICS]")
    fnDialogue("[CHAREXIT|ALL] Student: [VOICE|Student4][FOCUS|Student4]Pack it in butter,[P] pack it in butter,[P] [CRASH][ITALICS]PACK IT IN BUTTER, GEORGE.[ENDITALICS]")
    fnDialogue("[CHAREXIT|ALL] Thought: Which the group seems to find hilarious;[P] they laugh like they've all heard the funniest thing in their life.[P][P][P] But I don't get it.")
    fnDialogue("Thought: We've just about made it to the exit.[P] There's a bathroom nearby, and I find myself eyeing it longingly. ")
    fnDialogue("Player: Um, Todd?[P] I think I actually forgot something in the library.")
    fnDialogue("[CHARENTER|Harper][CHARENTER|Student2][CHARENTER|Todd][CHARENTER|Student4][CHARENTER|Student3]Todd: [E|Neutral][SFX|World|Blink]Hm?[P][P][E|Shock] Oh uh,[P] do you want us to wait for you?")
    fnDialogue("Thoughts: Todd stops.[P] The group continues to chat idly.")
    fnDialogue("Thoughts: Somewhere a ventilator turns on, blowing dusty cold air around my face like all the conversations I'm not a part of right now.")
    fnDialogue("Thoughts: I have to squint, and Todd's face looks blurry behind this wall of air and words.")
   
     --Save the game.
    fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"ToddHang\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
    PairanormalLevel_FlagCheckpoint("ToddHang")
    
	fnDialogue("Thoughts: I know if I keep going with the group, I won't get to speak with my friend--[P][P]if he even is that--[P]much at all.")
    
    --Decision Set.
    fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (What should I say?)") ]])

    local sDecisionScript = "\"" .. gsRoot .. "../Chapter 2/Gameplay_SceneB/10 - ResponseToTodd.lua" .. "\""
    fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Just wait up a bit\", " .. sDecisionScript .. ", \"Wait\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"You go on ahead\", " .. sDecisionScript .. ", \"GoAhead\") ")
    fnCutsceneBlocker()

end
