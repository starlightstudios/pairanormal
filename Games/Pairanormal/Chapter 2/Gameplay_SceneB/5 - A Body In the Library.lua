--[A Body In the Library]
--The body refers to you, dummy.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)
	
--Save the game.
if(PairanormalLevel_IsLoading() == false) then
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Starting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

--Set data.
else
	PairanormalLevel_FlagCheckpoint("Starting")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Classroom")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Classroom")
	fnDialogue([[ [Music|DaytimeRedux][CHARENTER|Laura][CHARENTER|Bret][CHARENTER|Michelle][CHARENTER|Stacy]Thought: [Focus|Laura]... ]])
end


--[Execution]
--Rock and roll!
fnDialogue("Player: Actually, I think I'll join Laura in the library.")

if(gbSkipDebug == false) then
    fnDialogue("Laura: [E|Shock]Wh-huh?[P] I-[P]I...")
    fnDialogue("Mari: [E|Neutral]All right, up to you! ")
    fnDialogue("Laura: [E|Mad]Um, w-[P]wait.[P][P] I'm actually going to be pretty b--")
    fnDialogue("Mari: [E|Happy]Come on, club![P] [CRASH]READY BREAK!!!")
    fnDialogue("[CHAREXIT|Bret][CHAREXIT|Michelle][CHAREXIT|Mari][CHAREXIT|Stacy]Thought: The club members depart briskly.")
    fnDialogue("Thought: Laura nods hesitantly and we make our way to our destination.")
    fnDialogue("[CHAREXIT|Laura][BG|Root/Images/Backgrounds/All/Hallway][MUSIC|Somber]Thought: Since I don't actually know where the library is, I follow a few paces behind my more knowledgeable clubmate.")
    fnDialogue("Thought: She barely turns to look at me, but chews firmly on her bracelet the whole way.")
    fnDialogue("Thought: I sigh.[P] Maybe she really doesn't like me.")
    fnDialogue("[BG|Root/Images/Backgrounds/All/Library][MUSIC|DaytimeRedux]Thought: We arrive after a few minutes. ")
    fnDialogue("Player: Whoah.")
    fnDialogue("Thought: I'm surprised the beautiful building isn't packed with students browsing the many, many shelves of books--")
    fnDialogue("Thought: Even though I can't read, I have an irresistible urge to dive in myself.")
    fnDialogue("Thought: To my surprise,[P] I do notice the Associated Student Body members Harper and Todd hard at work hanging a banner over a soon-to-be opened computer lab.")
    fnDialogue("Thought: Amongst the shelves,[P] the twins from this morning's assembly casually nod at me.[P] I might have a few questions for them.")
    fnDialogue("[CHARENTER|Laura]Thought: Laura darts to one of the older models of computers nestled against the walls.[P] I notice her face light up with the screen. ")
    fnDialogue("Laura:[P][P][E|Blush] [PLAYERNAME]? ")
    fnDialogue("Thought: She smiles at me politely from her seat.[P] I walk over to her, reaching to pull up a chair.")
    fnDialogue("Laura: [E|Neutral]The club will probably be over at the clubhouse f-[P]for about an hour.")
    fnDialogue("Laura: [E|Blush]*ahem* I-[P]if you get bored of hanging out here,[P] feel free to join them!")
    fnDialogue("Player: [P][P][P]Oh.")
    fnDialogue("Thought: [CHAREXIT|Laura]I'm about to remind Laura that I don't actually know where the clubhouse is...")
    fnDialogue("Thought: But before I know it her face disappears into the computer's enchanting cast once more.[P][P] Oh boy.")
	fnDialogue("[CHARENTER|Todd][CHARENTER|Harper]Todd: [E|Shock] Psst![P][E|Neutral] Hey, [PLAYERNAME]!")
	fnDialogue("Thought: I whip my head around.[P][ITALICS]Oh, that's right. Todd is here to witness my shame.[ENDITALICS] ")
	fnDialogue("Thought: AND he's not alone.[CHARENTER|Harper][FOCUS|Harper] [P] A second boy, tall and neatly dressed peers down his round spectacles at me from the top of a ladder.")
	fnDialogue("Thought: He's hanging up some signage for the new lab while Todd holds various hardware for the job.")
	fnDialogue("Harper: [E|Neutral]Well, well.[P] What have we here?")
	fnDialogue("Todd:[E|Neutral] Harper, this is [PLAYERNAME], the person I was telling you about!")
	fnDialogue("Thought: [SFX|World]T... Todd was telling people about me?[P][ITALICS] What was he saying?[ENDITALICS][P][SPEEDTEXT] Oh god oh god oh god--[ENDSPEEDTEXT]")
	fnDialogue("Todd: [E|Happy][PLAYERNAME], this is Harper, my best bud.")
	fnDialogue("Harper:[E|Neutral] A plea-sure~")
	fnDialogue("Thought: Did he say 'a pleasure' as in 'it's genuinely nice to meet you', or as in 'we've been making fun of your learning disability behind your back'!?!?")
	fnDialogue("Thought: Oh wait, say hi back.")
	fnDialogue("Player: Ah. Hi guys!")
	fnDialogue("[CHARENTER|Terry]Terry: [E|Mad][CRASH]HEY![P][P][CRASH] NO FUCKING SHOUTING IN THE FUCKING LIBRARY!!!")
	fnDialogue("[CHARENTER|Ross]Ross: [E|Neutral] Geez sis, relax.[P] They don't know the rules, remember?")
	fnDialogue("Ross: [E|Shock] They're like,[P] so new to our world,[P] they might not even understand the [ITALICS]concept[ENDITALICS] of rules.")
	fnDialogue("Player: [SFX|World|Blink]I,[P] uh...[P][P][P][SFX|World|Ugh]I'm pretty sure I understand the concept.")
	fnDialogue("Ross: [E|Shock]Oh, okay.[P][E|Happy] Neat! We're the librarians, by the way.[P] Nice to see you again!")
	fnDialogue("Player: Y-yeah.")
    fnDialogue("Thought: Okay, I've got a whole hour here in the library.[P][SFX|World|Ugh] What funnnnn.")
	fnDialogue("Info: [SFX|World|Discovery]Investigation #003 -- The Library")
    fnUnlockGallery("Gallery_Library")
else
    --Save the game.
    fnDialogue("[BG|Root/Images/Backgrounds/All/Library][MUSIC|DaytimeRedux]Thought: We arrive after a few minutes. ")
    fnUnlockGallery("Gallery_Library")
    
    
end

--[Loading the Game]
--If currently loading the game, set up the investigation.
if(PairanormalLevel_IsLoading() == true) then
	
    --Clear checkpoint.
    PairanormalLevel_FlagCheckpoint("Investigation")
	
	--Variables.
	local sBGPath = "Root/Images/Backgrounds/All/Library"
	local sScriptPath = gsRoot .. "../Chapter 2/Gameplay_SceneE/0 - ExamineObject.lua"
	
	--Change the background.
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Library")

	--Boot investigation scripts.
	Dia_SetProperty("Clear Investigation Characters")
	Dia_SetProperty("Clear Investigation")
	Dia_SetProperty("Start Investigation") 
	Dia_SetProperty("Set Investigation Image", "Root/Images/Backgrounds/All/Library")
	Dia_SetProperty("Set Investigation Script", gsRoot .. "../Chapter 2/Gameplay_SceneE/0 - ExamineObject.lua")
	Dia_SetProperty("Set Go Home Strings", gsRoot .. "../Chapter 2/Gameplay_SceneB/8 - PatchGoingHome.lua", "Default")
	
	--Talkable Characters.
	Dia_SetProperty("Add Talkable Character", "Laura", gsRoot .. "../Chapter 2/Gameplay_SceneE/1 - Present Laura.lua")
	Dia_SetProperty("Add Talkable Character", "Todd",  gsRoot .. "../Chapter 2/Gameplay_SceneE/2 - Present Todd.lua")
	Dia_SetProperty("Add Talkable Character", "Ross",  gsRoot .. "../Chapter 2/Gameplay_SceneE/3 - Present Ross.lua")
	Dia_SetProperty("Add Talkable Character", "Terry", gsRoot .. "../Chapter 2/Gameplay_SceneE/4 - Present Terry.lua")
	
	--Talkable characters as inventory objects.
	local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
	Dia_SetProperty("Add Discovered Object", "Laura", sCharPath, sScriptPath, "Laura",  882,   2,  882 + 438,   2 + 363)
	Dia_SetProperty("Add Discovered Object", "Todd",  sCharPath, sScriptPath, "Todd",     2, 367,    2 + 438, 367 + 363)
	Dia_SetProperty("Add Discovered Object", "Ross",  sCharPath, sScriptPath, "Ross",   882, 367,  882 + 438, 367 + 363)
	Dia_SetProperty("Add Discovered Object", "Terry", sCharPath, sScriptPath, "Terry", 1332, 367, 1332 + 438, 367 + 363)
	Dia_SetProperty("Clear Discovered Object")
    
    --SLF file adds the examinable objects.
    Dia_SetProperty("Parse SLF File", gsRoot .. "../Chapter 2/MapData/InvestigationAB.slf")
	
	--Check all the items and re-add them to the inventory.
	for i = 1, gciInvD_Total-1, 1 do
		
		--Variable name.
		local sVarName = "Root/Variables/Chapter2/InvestigationB/iObject|" .. gczaObjD[i][1]
		local iHasItem = VM_GetVar(sVarName, "N")
		
		--Has the item. Add it.
		if(iHasItem == 1.0) then
			Dia_SetProperty("Add Discovered Object", gczaObjD[i][1], sBGPath, sScriptPath, gczaObjD[i][1], gczaObjD[i][2], gczaObjD[i][3], gczaObjD[i][2]+gczaObjD[i][4], gczaObjD[i][3]+gczaObjD[i][5])
		end
		Dia_SetProperty("Clear Discovered Object")
	end

	--Modify the time max/left.
    VM_SetVar("Root/Variables/System/Investigation/sInvestigationPath", "S", "Root/Variables/Chapter2/Investigation/")
	local iTimeMax  = VM_GetVar("Root/Variables/Chapter2/Investigation/iTimeMax",  "N")
	local iTimeLeft = VM_GetVar("Root/Variables/Chapter2/Investigation/iTimeLeft", "N")
	Dia_SetProperty("Investigation Time Max",  iTimeMax)
	Dia_SetProperty("Investigation Time Left", iTimeLeft)

--[Normal Operations]
--Otherwise, flag the game to save and boot the investigation normally.
else
    --Save the game.
    fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Investigation\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

    --Set the time max/left.
    fnCutsceneInstruction([[ VM_SetVar("Root/Variables/Chapter2/Investigation/iTimeMax",  "N", 60.0) ]])
    fnCutsceneInstruction([[ VM_SetVar("Root/Variables/Chapter2/Investigation/iTimeLeft", "N", 60.0) ]])

    --Boot investigation scripts.
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Investigation Characters") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Investigation Time Max", 60) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Investigation Time Left", 60) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Set Investigation Image", "Root/Images/Backgrounds/All/Library") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Set Investigation Script", gsRoot .. "../Chapter 2/Gameplay_SceneE/0 - ExamineObject.lua") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Set Go Home Strings", gsRoot .. "../Chapter 2/Gameplay_SceneB/8 - PatchGoingHome.lua", "Default") ]])

    --Talkable characters.
    fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Laura", gsRoot .. "../Chapter 2/Gameplay_SceneE/1 - Present Laura.lua") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Todd",  gsRoot .. "../Chapter 2/Gameplay_SceneE/2 - Present Todd.lua") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Ross",  gsRoot .. "../Chapter 2/Gameplay_SceneE/3 - Present Ross.lua") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Terry", gsRoot .. "../Chapter 2/Gameplay_SceneE/4 - Present Terry.lua") ]])
    
    --SLF file adds the examinable objects.
    fnCutsceneInstruction([[ Dia_SetProperty("Parse SLF File", gsRoot .. "../Chapter 2/MapData/InvestigationAB.slf") ]])

    --Add the talkable characters as the first examine objects.
    fnCutsceneInstruction([[ Dia_SetProperty("Add Discovered Object", "Laura", "Root/Images/Backgrounds/All/CharPortraits", gsRoot .. "../Chapter 2/Gameplay_SceneE/0 - ExamineObject.lua", "Laura",  882,   2,  882 + 438,   2 + 363) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Discovered Object", "Todd",  "Root/Images/Backgrounds/All/CharPortraits", gsRoot .. "../Chapter 2/Gameplay_SceneE/0 - ExamineObject.lua", "Todd",     2, 367,    2 + 438, 367 + 363) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Discovered Object", "Ross",  "Root/Images/Backgrounds/All/CharPortraits", gsRoot .. "../Chapter 2/Gameplay_SceneE/0 - ExamineObject.lua", "Ross",   882, 367,  882 + 438, 367 + 363) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Discovered Object", "Terry", "Root/Images/Backgrounds/All/CharPortraits", gsRoot .. "../Chapter 2/Gameplay_SceneE/0 - ExamineObject.lua", "Terry", 1332, 367, 1332 + 438, 367 + 363) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])

    fnCutsceneBlocker()
    
    --Time variable.
    VM_SetVar("Root/Variables/System/Investigation/sInvestigationPath", "S", "Root/Variables/Chapter2/Investigation/")
end

--Checkpoint notification.
PairanormalLevel_FlagCheckpoint("Investigation")
fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Investigation\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
