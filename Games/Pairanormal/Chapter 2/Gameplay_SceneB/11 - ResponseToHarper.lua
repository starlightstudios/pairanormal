--[Response to Harper]
--Used after walking home with Todd. Only that path can access this file.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)
	
--Save the game.
if(PairanormalLevel_IsLoading() == false) then
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Starting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

--Set data.
else
	PairanormalLevel_FlagCheckpoint("Starting")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/SchoolExterior")
	fnDialogue([[ [Music|Love][SHOWCHAR|Harper|Neutral]Thought: [Focus|Harper]... ]])
end


--[Execution]
--Sound like you're a jealous hag, harper
if(sTopicName == "Jealous") then
	fnDialogue("Player: That kind of sounds like a threat.[P] You're not actually.[P].[P].[P]jealous?")
	fnDialogue("Thought: Harper laughs.")
	fnDialogue("Harper: [E|Happy]Omigosh,[P] nooooo.[P][E|Neutral] I promise you, I'm not.")
	fnDialogue("Harper: Just...[P][E|Sad] passing on some wisom, which I happen to know from personal experience.")

	
--You're an idiot, Harper
else(sTopicName == "Wrong") then
	fnDialogue("Player: You've got it all wrong![P] I d-dont.[P].[P].[P] It's not like that.")
	fnDialogue("Thought: Harper shrugs.")
	fnDialogue("Harper: [E|Neutral]If you say so.[P] Just keep it in mind.")
	fnDialogue("Thought: I don't respond.[P] How could Harper say something like that so... casually to begin with?")
	fnDialogue("[CHAREXIT|ALL]Thought: I can't help but resent how much easier it is for this sharp-tongued and fashionable boy to handle high school life.")
	fnDialogue("Thought: Like he's in possession of a secret key and no intention of sharing.[P] Even as I grind my teeth over this, Harper has taken his leave.")

	
--I have nothing to say to you, Harper
else(sTopicName == "Nothing") then
	fnDialogue("Thought: I don't respond.[P] How could Harper say something like that so... casually to begin with?")
	fnDialogue("[CHAREXIT|ALL]Thought: I can't help but resent how much easier it is for this sharp-tongued and fashionable boy to handle high school life.")
	fnDialogue("Thought: Like he's in possession of a secret key and no intention of sharing.[P] Even as I grind my teeth over this, Harper has taken his leave.")

end

fnDialogue("[CHAREXIT|ALL]Thought: Thought: And just like that, I'm left standing before the gate, while Harper passes through.")
fnDialogue("Thought:  When everyone sees him, they don't bother looking back for me anymore.")
fnDialogue("[CHARENTER|Todd]Todd: [E|Shock] Isn't [PLAYERNAME] coming?")
fnDialogue("[CHARENTER|Harper]Harper: [E|Neutral]Maybe later.[P] They said they had some stuff to figure out first.")
fnDialogue("[CHAREXIT|ALL][BG|Null] Thought: ...")

--Variable tracking.
VM_SetVar("Root/Variables/System/Romance/sLastWalkHome", "S", "Todd")
VM_SetVar("Root/Variables/System/Romance/iToddFriend", "N", 0.0)

--Execute the next script in sequence.
LM_ExecuteScript(gsRoot .. "../Chapter 2/Gameplay_SceneC/0 - Home Time.lua", "Start")



