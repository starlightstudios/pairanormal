--[Patch Going Home]
--Brief script which is used when going home from the clubhouse.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--[Execution]
--Start.
if(sTopicName == "Default") then
	
	--Question:
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALL]Thought: (Should I head home now?)") ]])

	local sDecisionScript = "\"" .. gsRoot .. "../Chapter 2/Gameplay_SceneB/7 - PatchGoingHome.lua\""
	fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Go Home\", " .. sDecisionScript .. ", \"GoHome\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Not Yet\",  " .. sDecisionScript .. ", \"Dont\") ")
	fnCutsceneBlocker()
	return
	
--Don't go home just yet.
elseif(sTopicName == "Dont") then

	--Resume investigation mode.
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()
	return
end

--Scene setup.
fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
fnCutsceneBlocker()
	
--Save the game.
if(PairanormalLevel_IsLoading() == false) then
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Starting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

--Set data.
else
	PairanormalLevel_FlagCheckpoint("Starting")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/ClubHouse")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/ClubHouse")
	fnDialogue([[ [Music|Daytime]Thought: ... ]])
end

--Flag.
PairanormalLevel_FlagCheckpoint("Starting")
Dia_SetProperty("Investigation Time Max", -1)
fnCutsceneBlocker()

fnDialogue("Thought: The club members are all packing their things up into their backpacks.")
fnDialogue("Thought: Stacy and Mari chat, while Michelle is a short ways off,[P] kicking rocks down the hill.")
fnDialogue("Thought: Bret tries (and fails) to get out of the way of the ricocheting sediment.")
fnDialogue("Thought: Maybe I should ask one of them if they'd like to walk home with me?")

--Decision Set.
fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (Who should I walk home with?)") ]])

local sDecisionScript = "\"" .. gsRoot .. "../Chapter 2/Gameplay_SceneB/1 - Walking Home.lua" .. "\""
fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Ask Mari\", " .. sDecisionScript .. ", \"Mari\") ")
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Ask Stacy\", " .. sDecisionScript .. ", \"Stacy\") ")
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Ask Michelle\", " .. sDecisionScript .. ", \"Michelle\") ")
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Ask Bret\", " .. sDecisionScript .. ", \"Bret\") ")
--fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Ask Clint Eastwood\", " .. sDecisionScript .. ", \"FewDollarsMore\") ")
fnCutsceneBlocker()