--[Patch Going Home]
--Brief script which is used when going home from the clubhouse.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--[Execution]
--Start.
if(sTopicName == "Default") then
	
	--Question:
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALL]Thought: (Should I head home now?)") ]])

	local sDecisionScript = "\"" .. gsRoot .. "../Chapter 2/Gameplay_SceneB/8 - PatchGoingHome.lua\""
	fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Go Home\", " .. sDecisionScript .. ", \"GoHome\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Not Yet\",  " .. sDecisionScript .. ", \"Dont\") ")
	fnCutsceneBlocker()
	return
	
--Don't go home just yet.
elseif(sTopicName == "Dont") then

	--Resume investigation mode.
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()
	return
end

--Scene setup.
fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
fnCutsceneBlocker()
	
--Save the game.
if(PairanormalLevel_IsLoading() == false) then
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Starting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

--Set data.
else
	PairanormalLevel_FlagCheckpoint("Starting")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Library")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Library")
	fnDialogue([[ [Music|Daytime]Thought: ... ]])
end

--Flag.
PairanormalLevel_FlagCheckpoint("Starting")
fnCutsceneBlocker()

--Post investigation
fnDialogue("Thought: Todd seems like he's wrapped up working with Harper; they're chatting and cleaning up.")
fnDialogue("Thought: Laura has stood up from the computer and is packing away her things --[P] she finally reads all the messages the club has been sending her.")
fnDialogue("Thought: Maybe I should ask one of them if they'd like to walk home with me?")

--Decision Set.
fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (Who should I walk home with?)") ]])

local sDecisionScript = "\"" .. gsRoot .. "../Chapter 2/Gameplay_SceneB/1 - Walking Home.lua" .. "\""
fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Ask Laura\", " .. sDecisionScript .. ", \"Laura\") ")
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Ask Todd\", " .. sDecisionScript .. ", \"Todd\") ")
--fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Ask Clint Eastwood\", " .. sDecisionScript .. ", \"TheGood\") ")
fnCutsceneBlocker()
