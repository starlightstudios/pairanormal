-- |[Response to Bret]|
--Accessed when walking home with Bret only.

-- |[Arg Resolve]|
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)
	
--Save the game.
if(PairanormalLevel_IsLoading() == false) then
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Starting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

--Set data.
else
	PairanormalLevel_FlagCheckpoint("Starting")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/WalkHome")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/WalkHome")
	fnDialogue([[ [Music|DaytimeRedux][SHOWCHAR|Bret|Neutral]Thought: [Focus|Bret]... ]])
end

-- |[Flag]|
--Used for later.
VM_SetVar("Root/Variables/System/Romance/iBretMentionedFactory", "N", 1.0)

-- |[Execution]|
--It won't be very hard to trick them.
if(sTopicName == "Change") then
	fnDialogue("Player: Thanks Bret.[P] Let's just change the subject.")
	fnDialogue("Thought: He nods, and I search the clouds above for something new to talk about.")
	fnDialogue("Player:[P][P][P] Uhh,[P] well besides bottlecaps,[P] do you have any other hobbies?")
	fnDialogue("Bret: [E|Happy]Sure do.[P][E|Mad] I'm not just a soda-obsessed maniac, you know.[P][E|Neutral] I like bacon, cats, video games...[P][P]lots of video games.")
	fnDialogue("Player: Like the arcade machines at Charlie's Pizza parlor.[P] How'd you get so good at those things?")
	fnDialogue("Bret: [E|Happy]Oh, you know.[P][E|Neutral] When you spend as much time alone as I do, what else are you gonna do?")
	fnDialogue("Bret: [E|Happy] Haha,[P] only kidding. I...[P]I'm not actually[E|Sad] totally alone.")
	fnDialogue("Player: Pff, I know that.[P] I mean, you're always with the Paranormal Club.")
	fnDialogue("Bret: [E|Neutral]Right.[P] Yes, exactly...")
	fnDialogue("Thought: ...")
	fnDialogue("[P][P]Bret: [E|Neutral]You know, my favorite game isn't even //in// Charlie's pizza parlor.")
	fnDialogue("Bret: It's an old MMORPG called Stonescape [P]--I used to play it all the time, back in my hometown.[P] That was way before my family came to Foster Falls.")
	fnDialogue("Player: What does MMORPG stand for?")
	fnDialogue("Bret: [E|Shock]Hmmm, Massively Multiplayer Online Role Playing Game, I think.")
	fnDialogue("Player: Ah.[P] I see why they shortened it.[BR] Soooo, how come you don't play it anymore?")
	fnDialogue("Bret: [E|Happy]Oh, believe me, I would if I could![P][E|Shock] The appeal of an MMORPG is that you're playing it with a bunch of other people at once, online.")
	fnDialogue("Bret: [E|Neutral] I had so many friends, and we'd go on quests all the time.[P] It was like being in touch with a big community...")
	fnDialogue("Bret: [E|Mad]... Even though they were from all over the world,[E|Shock] and I was in such a small town by myself,[E|Neutral] I could still be connected!")
	fnDialogue("Thought: Bret's steps slow, and he inhales deeply.")
	fnDialogue("Bret: [E|Sad]Anyway.[P] All of the people I used to play it with... well, I guess they moved on to other games.[P][E|Neutral] And I moved here.")
	fnDialogue("Player: Yeah. What luck!")
	fnDialogue("Bret: [E|Shock]... what?")
	fnDialogue("Player: Otherwise we wouldn't have met, right?")
	fnDialogue("Thought: I push a fake pair of spectacles up my nose, just like Bret does right before he makes an astute observation.")
	fnDialogue("Player: Besides, I've deduced that you still play LOTS of video games. [P][CRASH]a-HA![P] See here, based on the indentation on your left thumb--")
	fnDialogue("Thought: I reach for his hand to faux inspect it like a movie detective [P]--but I didn't realize it would be so soft.")
	fnDialogue("Thought: The fingers are pale and long, but the tips are supple, probably great at handling all those video game sticks and buttons.")
	fnDialogue("Thought: Only now, they're brushing against my palm.")
	fnDialogue("Player: *ahem*.[P] I was just kidding.")
	fnDialogue("Bret: [E|Shock]!!! Oh! [E|Sad]uh, yes. [E|Happy]right.[E|Neutral] I mean--you're right.")
	fnDialogue("Thought: He snatches his digits away, holding them close to his abdomen like they were sore and in need of protection.")
	fnDialogue("Bret: [E|Neutral]I don't play Stonescape so much anymore, [E|Mad]but I suppose I don't need to now.[P][E|Happy] My old town was so small.")
	fnDialogue("Bret: [E|Mad] Here in Foster Falls--[E|Shock]well, it's still a pretty small town here too, to be honest.[E|Happy] But you know, there's like more opportunities.")
	fnDialogue("Bret: [E|Blush] To be, you know.[P] C-close.[P] To people.")
	fnDialogue("Thought: We share an awkward but warm smile, and walk on.[P][CHAREXIT|ALL][BG|Null] Even though none of us say anything for a while, for once the silence is envigorating rather than unnerving.")
    
	
--Let's go be reckless ourselves!
elseif(sTopicName == "Explain") then
	fnDialogue("Player: Bret, of course...[P]of course it's not that I hate [ITALICS]soda[ENDITALICS].[P] I just... ")
	fnDialogue("Thought: I look down at my leg where a bottle of Kane Kola had been splashed.[P] The stain isn't there anymore, but somehow I can still feel it.")
	fnDialogue("Player: --It brings up a painful memory right now. ")
	fnDialogue("Bret: [E|Sad]Of course.[P][E|Shock] O-of course it does! What those guys did to you was awful. Such a jerky, jerk thing.")
	fnDialogue("Bret: [E|Sad] It happened to me, too, when I first came here.[P][E|Shock] I was so stunned, [E|Mad]and hurt, [E|Sad]and I felt really crappy for like, a couple weeks, honestly.")
	fnDialogue("Bret: [E|Shock] But then I found out that they do it to everyone, you know?[E|Sad] It's just a typical garbage part of high school life.")
	fnDialogue("Player: Puh, I just don't understand it.")
	fnDialogue("Player: If [ITALICS]everyone[ENDITALICS] goes through such a terrible thing,[P] and understands what it feels like,[P] why on earth would they want to do it to someone else???")
	fnDialogue("Bret: [E|Shock]Oof.[P][E|Neutral] You really...[E|Happy]you really hit the jackpot with that one, my friend.[P] That's the million dollar question, isn't it?")
	fnDialogue("Player: ...")
	fnDialogue("Thought: [ITALICS][SFX|World|Ugh]]Why is it 'good questions' only have a terrible answer or none at all?[ENDITALICS]")
	fnDialogue("Bret: [E|Sad][P][P][P]You know what I think it is? I think everyone's just scared to go through it by themselves.")
	fnDialogue("Bret: [E|Mad]Like, if they had to suffer this dumb, awful thing,[P][E|Shock] but no one else did, what would be the point?")
	fnDialogue("Bret: [E|Mad]] If you get to say you can pass it on somehow, it makes it worth all the embarrassment. [E|Sad]Otherwise... ")
	fnDialogue("Thought: Bret rubs the back of his neck. Even from here I can see the sudden tension rising up his spine.")
	fnDialogue("Bret: [E|Sad]Otherwise.[P] You'd be forced to conclude...[P] you [ITALICS]deserved[ENDITALICS] it somehow.")
	fnDialogue("Bret: [P][P][SFX|World|Blink][E|Shock] Uhh![P] Ah, [PLAYERNAME],[P] w-what are you doing there?")
	fnDialogue("Player: We're hugging.[P] Isn't that what people are supposed to do after they share their feelings?")
	fnDialogue("Bret: [E|Shock]Y-yeah, yes! Of course![P] I mean,[P][SPEEDTEXT][E|Blush] I was just surprised because I guess people don't really hug me that often.")
	fnDialogue("Bret: [E|Mad] But since it is happening,[E|Shock] uh... I guess I'll just put my arms around you too.[E|Blush] Yep. This definitely feels great and is fine.[ENDSPEEDTEXT]")
	fnDialogue("Bret: *ahem*[P] And by the way you are absolutely, like, welcome to do this whenever you want to.")
	fnDialogue("Player: Okay. [P]I will.")
	fnDialogue("Thought: For some reason, it wasn't quite easy getting up close to Bret like this.")
	fnDialogue("Thought: In fact now that we're hugging, instead of feeling more relaxed I find my pulse is actually [ITALICS]increasing[ENDITALICS].[P] It's probably fine.")
	fnDialogue("Thought: [CHAREXIT|ALL][BG|Null]After we let go, the relief I was looking for in a comforting hug finally hits.[P] Bret looks similarly relieved, and we keep walking in a kind of blissful trance.")
    
--No thank you.
elseif(sTopicName == "Fine") then
	fnDialogue("Thought: I really ought to buck up and let Bret speak about his passion.[P][ITALICS] It makes him happy, doesn't it?[ENDITALICS]")
	fnDialogue("Player: Bret, it's not like that![P] Look, I am actually curious about the factory and stuff.")
	fnDialogue("Player: *ahem*! As an official member of the Paranormal Club, IS IT NOT our duty to investigate all unusual circumstances?")
	fnDialogue("Bret: [E|Sad]Yeah.[P][E|Shock] I mean, heh, yes.[E|Blush] t-techincally, actually, I suppose it is.")
	fnDialogue("Player: So tell me, detective--what's the official story on why the soda factory closed down?")
	fnDialogue("Bret: [E|Shock]Aha![P] Well, according to [ITALICS]my[ENDITALICS] extensive research...[P][P][E|Sad][SFX|World|Ugh]OSHA violations???[P] Maybe???")
	fnDialogue("Bret: [E|Mad]I've even had Laura dig into all kinds of archives for me, but I think whoever was aware of what happened was told to keep it quiet.")
	fnDialogue("Bret: [E|Neutral] Only a few reports of a chemical spill, and a fire afterwards.[P][E|Blush] There's just no details.")
	fnDialogue("Bret: [E|Shock] I want to go investigate more, but that place is...[E|Sad]man.[P] It's a place.[P] I'd never wanna be stuck there ALONE.")
	fnDialogue("Player: Let's go together sometime, then!")
	fnDialogue("Bret: [E|Shock]Pfft, no, no.[P][E|Blush] I c-couldn't ask you to... go alone with me...[E|Mad] just the two of us... on a thrilling adventure in an abandoned soda factory.")
	fnDialogue("Bret: [E|Sad]With... just... like, all that muck, and...[E|Blush] and each other.[P] No.[P] Can you even imagine?")
	fnDialogue("Player: I can imagine it would [ITALICS]fun.[ENDITALICs][P] What's the matter, you allergic to my friendship or something?")
	fnDialogue("Thought: Bret instantly reddens, and I plant my hands on my hips. ")
	fnDialogue("Bret: [E|Shock]Uh--I... wh---")
	fnDialogue("Player: Sorry, I shouldn't have assumed--[BR] Are we? [P]F-friends?")
	fnDialogue("Thought: A warm hand tentatively pats me on the back--his.[P] I don't remember standing this close to him.")
	fnDialogue("Bret: [E|Happy]Dude.[P] Oh dude.[P] Of course we're friends, [PLAYERNAME]!")
	fnDialogue("Player: [ITALICS]whew.[ENDITALICS] T-That's good![P] I'm glad.")
	fnDialogue("Bret: [E|Sad]You...[P]what you said...[P]thank you, [PLAYERNAME].[P][P][E|Blush] It felt good to hear.")
	fnDialogue("Player: Really? ")
	fnDialogue("Bret: [E|Neutral]Oh man... yes.")
	fnDialogue("Bret: [E|Mad] I mean, I don't know why you said it so angrily,[P][E|Blush] but I guess it's because you really meant it?")
	fnDialogue("Player: Me neither![P] But I did!")
	fnDialogue("Thought: He chuckles.[P] I never realized how nice it feels to make someone smile.[P][CHAREXIT|ALL] We walk on.")
	fnDialogue("Thought: [BG|Null]Even though none of us say much for a while, for once the silence feels envigorating rather than unnerving.")
	

end

--Dialogue rejoins here.
fnDialogue("[BG|Root/Images/Backgrounds/All/WalkHome]Player: Well, uh,[P] this is me.")
fnDialogue("Thought: I point to Eilette's house a block away as it comes into view.[P] I've already slung my heavy backpack off my shoulders.")
fnDialogue("[CHARENTER|Bret]Bret: [E|Shock]Oh![P] That was--[E|Blush]wow,[P] that felt quick.")
fnDialogue("Player: Yeah, I live pretty close to the school.")
fnDialogue("Bret: [E|Sad][ITALICS]Aww...[ENDITALICS]I mean--[P][E|Shock]Great! That's like, so great, for you.[E|Neutral] Great for everyone actially, yeah.")
fnDialogue("Player: Yep! ")
fnDialogue("Thought: I trot across the street, but don't hear a second pair of footsteps behind me.")
fnDialogue("Turning,[P] I notice Bret is still standing on the sidewalk,[P] rocking from foot to foot like he can't make up his mind up about something.")
fnDialogue("Bret: [E|Blush]You're just like,[P] uh, I would like to...[P][E|Mad]in the future,[P] if you ever are looking for an escort?")
fnDialogue("Bret: [E|Shock] No, wait, not an [ITALICS]escort[ENDITALICS] escort,[P] like, [SPEEDTEXT]an old school escort.[ENDSPEEDTEXT]")
fnDialogue("Bret: [SPEEDTEXT][E|Sad] Just a regular--[E|Mad]what I'm trying to say is...[ENDSPEEDTEXT]")
fnDialogue("Player: [P][P]Bret?")
fnDialogue("Bret: [P][P][SFX|World|Blink]Oh![P][P][CRASH] I gotta pick up my brother from the doctor!!![P] G-gotta go![P] Bye!")
fnDialogue("Player: Wait a minute,[P] you said he would be fine on his own!")
fnDialogue("Thought: But Bret is already a block away and still dashing in the direction of town.")
fnDialogue("Thought: As he disappears into the horizon, I have to stifle a giggle.[P] [ITALICS]What a nerd![ENDITALICS]")

--Variable tracking.
VM_SetVar("Root/Variables/System/Romance/sLastWalkHome", "S", "Bret")
VM_SetVar("Root/Variables/System/Romance/iBretFriend", "N", 1.0)

--Execute the next script in sequence.
LM_ExecuteScript(gsRoot .. "../Chapter 2/Gameplay_SceneC/0 - Home Time.lua", "Start")
