--[Response to Stacy]
--Used after walking home with Stacy. Only that path can access this file.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)
	
--Save the game.
if(PairanormalLevel_IsLoading() == false) then
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Starting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

--Set data.
else
	PairanormalLevel_FlagCheckpoint("Starting")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/WalkHome")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/WalkHome")
	fnDialogue([[ [Music|Love][SHOWCHAR|Stacy|Neutral]Thought: [Focus|Stacy]... ]])
end


--[Execution]
--Ask her outright.
if(sTopicName == "Help") then
	fnDialogue("Player: Um... can I see that?")
	fnDialogue("[CHARENTER|Stacy]Stacy: [E|N]This bandaid? Sure.[P] It's pretty sweet, isn't it?")
	fnDialogue("Stacy: [E|N]I get the 'Athletix Stripz' brand... since what I'm doing is so hardcore sometimes--[P][E|Mad]you need that [ITALICS]adhesive grip[ENDITALICS].")
	fnDialogue("Player: I see.")
	fnDialogue("Stacy: [E|N]Plus, they're self-disinfecting.[P][E|Blush] 'Course it stings [ITALICS]juste un peu[ITALICS] when you put it on, but by now I'm used to--[P][E|Shock][CRASH]OWWWWCCHH!! FUCK!!!")
	fnDialogue("Player: There you go![P] I helped you put it on.")
	fnDialogue("Stacy: [E|Shock]SONOFA--[P][P][E|N][SFX|World|Ugh]I mean, thanks.[P][E|Blush] *ahem*.")
	fnDialogue("Stacy: [E|Blush] You know, [PLAYERNAME]...[P]It actually feels better when you do it.")
	fnDialogue("Player: No problem.")
	fnDialogue("Stacy: [E|Blush]It's not easy putting myself on the line for the club.[P][E|Mad] Our activities are so [ITALICS]fraught[ENDITALICS] with [ITALICS]danger[ENDITALICS].")
	fnDialogue("Stacy: [E|Blush] I'm glad I have someone like you to keep an eye out.")
	fnDialogue("Player: Anytime.")
	fnDialogue("Stacy: [E|Blush].[P].[P].")
	fnDialogue("Player: ...[SFX|World|Blink]?")
	fnDialogue("Stacy: [P][E|Sad][SFX|Ugh]*sigh*, let's just keep walking.")
	fnDialogue("...")
	fnDialogue("...")
	fnDialogue("...")
	fnDialogue("...")
	fnDialogue("...")
	fnDialogue("...")
	fnDialogue("...")

--Say nothing
elseif(sTopicName == "Stand") then
	fnDialogue("Thought: I stand there with the sophistication and grace of a dead tree stump while Stacy nobly struggles with the bandaid.")
	fnDialogue("Stacy: Ugh, this is so hard to do with my [ITALICS]shirt[ENDITALICS] on.[P] Hang tight!")
	fnDialogue("Stacy: It's cool, I've got a sports bra on.")
	fnDialogue("Thought: After an incident at the downtown shopping center, Eilette said I was [ITALICS]never[ENDITALICS] allowed to get naked in public again, or I'd be arrested.")
	fnDialogue("Thought: Even so, I still miss the breezy freedom of my old hospital gown.[P] And here Stacy is, openly disrobing without a care in the world.")
	fnDialogue("Stacy: Got it!")
	fnDialogue("Thought: My exhibitionist companion shows off her freshly dressed ribcage.")
	fnDialogue("Thought: While she puts her shirt back on, I find myself smoothing down a loose corner of the banaid.[P] I feel her muscles tense under my fingertips.")
	fnDialogue("[SHOWCAR|Stacy|Shock][FOCUS|Stacy]Player: Oh, sorry. Did I hurt you?")
	fnDialogue("Stacy: [E|Shock]*wheeze* [ITALICS]it's fine.[P] I'm fine[ENDITALICS]. I mean...[P][SFX|World|Ugh][E|Blush]Let's keep going.")
	fnDialogue("Thought: We do.")

end

--Dialogue rejoins here.
fnDialogue("[CHAREXIT|ALL]Thought: [P][P]Over the course of our walk, Stacy continuously shoots me with a certain look[P]--a single raised eyebrow, a slow, playful smile.")
fnDialogue("Thought: I think she's expecting me to do something,[P] but I haven't the faintest idea what.")
fnDialogue("Thought: The moment always ends in disappointment for both of us. I've got to make up ground with a good topic.")
fnDialogue("[CHARENTER|Stacy]Player: So... oh, yeah.[P] Did you discover anything about the clubhouse? ")
fnDialogue("Stacy: [E|Shock]Hm?")
fnDialogue("Player: Our clubhouse.[P] Why do you think it exploded?")
fnDialogue("Thought: I might have imagined it, but Stacy's eyes suddenly flicker with darkness.[P] She shrugs it off.")
fnDialogue("Stacy: [E|Sad]Mm-mm.")
fnDialogue("Player: Oh come on.")
fnDialogue("Player: The [ITALICS]brilliant[ENDITALICS] captain of the Paranormal Club doesn't at least have SOME theories as to how a giant TANK blew up in the middle of the woods?")
fnDialogue("Stacy: [E|Sad][P][P][P]... Nope.[P] Not this time.")
fnDialogue("Player: Oh I see, you're keeping something from me![P] I can tell.")
fnDialogue("Player: Come on, spill that juicy theory.[P] Give me a piece of that GENIUS co-captain--")
fnDialogue("Stacy: [E|Mad]Look, [PLAYERNAME].[P] No offense, but people aren't always open books.")
fnDialogue("Stacy: There are some things I just don't want to talk about,[P] least of all with someone I barely know!")
fnDialogue("Thought: My mouth clamps shut.[P] Stacy might as well have slapped me across the face.")
fnDialogue("Stacy: [E|Shock]Oh no. [ITALICS]Mierde[ENDITALICS]![P][E|Sad] I'm the one who should be sorry, [PLAYERNAME]. I didn't mean that.")
fnDialogue("Stacy: You are my friend, really you are.[P] I just... as a favor to me, I'd prefer if you didn't ask about that whole situation?[P] The clubhouse?")
fnDialogue("Player: Wh--")
fnDialogue("Thought: I stop myself and nod, hastily clearing the lump in my throat with a forced cough. ")
fnDialogue("Stacy: [E|N]*sigh*...[P][P]Thanks.")
fnDialogue("Stacy: [E|Happy] Now, now! What was I saying about the French language? Oh yeah, so another interesting part is how every noun has a female or male pronoun,")
fnDialogue("Stacy: which English doesn't have.[P] That part was trickiest for me...")
fnDialogue("Thought: It occurs to me for all the talking Stacy does about her accomplishments, I still don't feel like I really [ITALICS]know[ENDITALICS] her,[P] or what she's thinking.")
fnDialogue("Thought: And just what is she thinking about the clubhouse?[P] Why is she suddenly so hesitant to talk about it?")
fnDialogue("Stacy: [E|Shock]... Wonder if those countries will ever come up with a gender neutral pronoun,[P] although that's probably really tough to do...")
fnDialogue("Player: Oh, that's my house over there, Stacy.")
fnDialogue("Stacy: [E|Shock]Ah.[P][E|Happy] I guess so.[P] That was a fast walk, haha!")
fnDialogue("Player: Well, this is goodbye...")
fnDialogue("Stacy: [E|Shock]Uh, [PLAYERNAME], wait![P][E|Blush] I... we should walk home together again soon, okay?")
fnDialogue("Player: [P].[P].[P].")
fnDialogue("Thought: [SFX|World|Ugh][ITALICS]seriously?[ENDITALICS]")
fnDialogue("Player: Sure.[P] I'll see you around.")
fnDialogue("Stacy: [E|Happy]Great! Haha. Super great![P] [ITALICS]Tres bien[ENDITALICS]!!! I mean--[P]See you.")
fnDialogue("[CHAREXIT|Stacy]Thought: ..I REALLY don't understand Stacy.[P] But I guess there'll be the opportunity to try again soon.")


--Variable tracking.
VM_SetVar("Root/Variables/System/Romance/sLastWalkHome", "S", "Stacy")
VM_SetVar("Root/Variables/System/Romance/iStacyFriend", "N", 0.0)

--Execute the next script in sequence.
LM_ExecuteScript(gsRoot .. "../Chapter 2/Gameplay_SceneC/0 - Home Time.lua", "Start")
