--[Response to Laura]
--Accessed when walking home with Laura only.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)
	
--Save the game.
if(PairanormalLevel_IsLoading() == false) then
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Starting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

--Set data.
else
	PairanormalLevel_FlagCheckpoint("Starting")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/WalkHome")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/WalkHome")
	fnDialogue([[ [Music|DaytimeRedux][SHOWCHAR|Bret|Neutral]Thought: [Focus|Bret]... ]])
end


--[Execution]
--DO IT. DO IT NOW.
if(sTopicName == "DoIt") then
	fnDialogue("Player: Hm. I don't think that's possible.")
	fnDialogue("Laura: [E|Shock]What isn't?")
	fnDialogue("Player: That he wouldn't like you. You're a very nice person, and you're smart, and you're very pretty.")
	fnDialogue("Laura: [E|Blush][SFX|World|Blink]A...[P]*ahem*[P] *hrm*[P] I-I...!!!")
	fnDialogue("Player: So you shouldn't worry.[P] If you want to meet him, you should meet him.[P] You owe it to yourself.")
	fnDialogue("Laura: [E|Happy]... T-[P]Thanks, [PLAYERNAME]. It means a lot for you to say that.[P][E|Sad] But this situation is just too...[P]*hrm* complicated.")
	fnDialogue("Thought: Laura gives me a sad smile, and I glower at my shoes in frustration.")
	fnDialogue("Thought: [ITALICS]It doesn't seem all that complicated to me[ENDITALICS]![P] We walk for a while in the usual tense silence. ")
	fnDialogue("Thought: [P][P][P][ITALICS]But then again, What do I know?[ENDITALICS]")
	fnDialogue("Thought: I wish I could say anything to make Laura happy, to understand what she's going through.")
	fnDialogue("Thought: Instead, like the emptyheaded fool I am, I respond::")
    
--ALWAYS FIND OUT THE TRUTH.
elseif(sTopicName == "Truth") then
	fnDialogue("Player: If it were me, I'd want to know the truth about him.")
	fnDialogue("Laura: [E|Shock]The truth?")
	fnDialogue("Player: Yeah.[P] If he doesn't like you when you meet him, then wouldn't all of your interactions have been just a waste of time?")
	fnDialogue("Laura: [E|Blush]I...[P][E|Sad]hm.")
	fnDialogue("Player: I wouldn't want to be with someone who doesn't like me.[P] So, why not just find out and get it over with?")
	fnDialogue("Laura: [E|Sad][P][P][P]hah.")
	fnDialogue("Thought: She fidgets with the edges of her skirt.")
	fnDialogue("Laura: [E|Happy]You make it sound so simple, [PLAYERNAME].")
	fnDialogue("Laura: [E|Sad]And I suppose *ahem*[P] in your experience, it is. [P]But I'm...[P][P]it's just more complicated than that, I'm afraid.")
	fnDialogue("Thought: She looks at me, her sad blue-grey eyes crinkled into a smile.")
	fnDialogue("Thought: A gust of wind picks up, whipping her cool blue hair lazily around her head.[P] The rays of the sun bounce off it, creating a soft halo.")
    --Zoom
	fnDialogue("Laura: [E|Neutral]Maybe someday, you'll get a crush on someone.[P] Then you'll understand.")
    
--No thank you.
elseif(sTopicName == "Nope") then
	fnDialogue("Player: [SFX|World|Ugh]I guess you're right.")
	fnDialogue("Player: It would hurt a lot to find out someone you care about doesn't like you after such a long time.")
	fnDialogue("Laura: [E|Blush]See?[P] That's why I can't meet with him.")
	fnDialogue("Player: Well, I guess you should enjoy the time you have left,[P] since its limited.")
	fnDialogue("Thought: Laura nods.[P] She doesn't look very satisfied with this idea, and I don't feel too good conceding to such a lame point.")
	fnDialogue("Thought: I wish I could say anything to make Laura happy, to understand what she's going through.")
	fnDialogue("Thought: Instead, like the emptyheaded fool I am, I respond:")
    
end

--Dialogue rejoins here.
fnDialogue("Player: Well, this is me.")
fnDialogue("Laura: [E|shock]Oh wow, you live *ahem*[P] nearby to the school.")
fnDialogue("Player: Yep![P] Um...[P] feel free to... um...[P] stop by,[P] sometimes,[P] you know.[P][P][P] If you want to.")
fnDialogue("Laura: [E|Mad]]I-[P]I-[P]I will! ...[P][E|Blush] If I'm in the *hrm* neighborhood.[P] Or... if I want to... s-[P]see you!")
fnDialogue("Player: Yep!")
fnDialogue("Laura: Hrm.")
fnDialogue("Player:[SFX|World|Ugh][P].[P].[P].")
fnDialogue("Laura: [SFX|World|Ugh][P].[P].[P].")
fnDialogue("Thought: She turns to leave, and I reach for the front door.")
fnDialogue("Laura: [E|Shock]Oh, [PLAYERNAME].")
fnDialogue("Player: Yeah?")
fnDialogue("Laura: [E|Neutral]Thanks f-for... walking with me.[P] I'm sorry I made it seem like I was being distant.")
fnDialogue("Laura: [E|Blush] But I-[P]I just want you to know.[P].[P].[P] I'm really happy we're friends now!")
fnDialogue("Thought: She dashes off.")
fnDialogue("Thought: I find myself wishing I had a bracelet I could chew on.[P] Maybe that would keep my blood from racing like it is now, so fast my fingertips tingle a little bit.")

--Variable tracking.
VM_SetVar("Root/Variables/System/Romance/sLastWalkHome", "S", "Laura")
VM_SetVar("Root/Variables/System/Romance/iLauraFriend", "N", 1.0)

--Execute the next script in sequence.
LM_ExecuteScript(gsRoot .. "../Chapter 2/Gameplay_SceneC/0 - Home Time.lua", "Start")
