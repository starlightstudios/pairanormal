-- |[See You At the Club]|
--Aw yeah, Danger 5 jokes.

-- |[Arg Resolve]|
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)
	
--Save the game.
if(PairanormalLevel_IsLoading() == false) then
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Starting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

--Set data.
else
	PairanormalLevel_FlagCheckpoint("Starting")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Classroom")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Classroom")
	fnDialogue([[ [Music|DaytimeRedux][SHOWCHAR|Mari|Neutral][SHOWCHAR|Bret|Neutral][SHOWCHAR|Stacy|Neutral][SHOWCHAR|Laura|Neutral][SHOWCHAR|Michelle|Neutral]Thought: [Focus|Mari]... ]])
end


-- |[Execution]|
--Rock and roll!
fnDialogue("Player: I think I'll tag along with you guys if it's all right.")

if(gbSkipDebug == false) then

    fnDialogue("Mari: [E|Happy][CRASH]HECK YEAH!")
    fnDialogue("Michelle: [E|Neutral]Let's go!")
    fnDialogue("Thought: We all wave goodbye to Laura and depart on foot to the clubhouse.")
    fnDialogue("[CHAREXIT|ALL][BG|Null][P][P]Thought: ...")
    fnDialogue("[BG|Root/Images/Backgrounds/All/SchoolExterior][MUSIC|Theme]Player: I wonder what's keeping Laura at the library.")
    fnDialogue("[CHARENTER|Bret]Bret: [E|Neutral]That's where she always goes on club missions.")
    fnDialogue("Bret: But she used to come to the club house occasionally.[P] It's pretty safe.")
    fnDialogue("Bret: [E|Shock] Nothing up there but squirrels and lawn chairs.")
    fnDialogue("[CHARENTER|Mari][CHARENTER|Stacy][CHARENTER|Michelle]Michelle: [E|Happy]And a recently exploded tank.[P] Maaaaaaybe she's worried about that.")
	fnDialogue("Mari: [E|Neutral]I'm not worried.[P][E|Mad] We've got Stacy and Michelle to bring on the muscle and pain!!!")
	fnDialogue("Michelle:[E|Neutral] Brilliant.[P] If there's another explosion, I'll give it a mean look and Stacy will punch it.[P] That'll totally work.")
	fnDialogue("Thought: I look to Stacy, expecting a spicy retort.")
	fnDialogue("Thought:She has her eyes trained on Mari and for a moment opens her mouth to speak, [P]but clenches her jaw instead and keeps walking.")
	fnDialogue("Thought: [ITALICS]... To be honest, I'm not too keen on getting caught up in an explosion myself.[ENDITALICS]")
    fnDialogue("[CHAREXIT|ALL][BG|Root/Images/Backgrounds/All/Forest]Thought: We reach the edge of a wooded grove several blocks from the school.[P] I can feel the club breathing a collective sigh.")
    fnDialogue("[CHARENTER|Bret]Bret: [E|Neutral]Aaaaanyway,[P] [E|Shock]hey, uh,[P] [PLAYERNAME] so,[P] [E|Blush]you got Kane Kola Cancelled this morning,[P] right?")
    fnDialogue("Player: What?[P] Oh.")
    fnDialogue("Thought: [ITALICS]Bret, observant as usual.[ENDITALICS][P] I kick out my soda-stained pant leg affirmatively.")
    fnDialogue("Player: Yeah.")
    fnDialogue("[CHARENTER|Michelle]Michelle: [E|Mad]Ugh, those pricks still do that stupid ritual?")
    fnDialogue("Bret: [E|Blush]I don't know if it'll ever go away.[P] The seniors got me hard back in '06.[P][E|Mad] Spilled a bottle all over my lap!")
    fnDialogue("[CHARENTER|Mari][CHARENTER|Stacy]Stacy: [E|Sad]'05 over here.")
	fnDialogue("Stacy:[E|Neutral] I was still in junior high at the time but everyone thought I was a freshman because of my youthful energy.")
    fnDialogue("Bret: [E|Mad]I had to go into the bathroom and change,[P] [E|Sad]but I only had one pair of pants,[P][E|Mad] so I held it under the dryer for ages.")
    fnDialogue("Bret: [E|Shock] And then,[SPEEDTEXT] I realized my underwear had soaked through, too, so I-- [ENDSPEEDTEXT] ")
    fnDialogue("Mari: [E|Neutral]I got it freshman year, too.")
    fnDialogue("Mari: My soda spilled on the grass, so the bottle didn't break but... [E|Sad]It's a dollar-fifty down the drain, either way.")
    --Man, the things I could do with a dollar fifty.
    fnDialogue("Player: It's such a mean thing to do.")
    fnDialogue("Michelle: [E|Sad]Meh.[P] It's a rite of passage, babe.[P] Everyone goes through it.")
    fnDialogue("Player: Did it happen to you, too?")
    fnDialogue("Michelle: [E|Happy]Psh.[P] Nah.")

    --Save the game.
    fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"WeAreHere\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
    PairanormalLevel_FlagCheckpoint("WeAreHere")

	fnDialogue("[MUSIC|Null][CHAREXIT|ALL][BG|Null]")
    fnDialogue("Mari: *gasp!* [E|Happy]All right, everyone...[E|Mad][CRASH] prepare!")
    fnDialogue("Thought: We murmur with curiosity.[P] I can see the tops of two tanks through the brush.")
    fnDialogue("Thought: Just a few steps further...")
    fnDialogue("Thought: ...")
    fnDialogue("[BG|Root/Images/Backgrounds/All/ClubHouse][CHARENTER|Bret]Bret: [E|Shock]WHOAH.")
    fnDialogue("[CHAREXIT|Bret][CHARENTER|Stacy]Stacy: [E|Shock]Oh my god.")
    fnDialogue("[CHAREXIT|Stacy][CHARENTER|Michelle]Michelle: [E|Shock]Holy shit.")
    fnDialogue("[CHAREXIT|Michelle]Thought: I can finally see the 'clubhouse' in all it's splendor.")
    fnDialogue("[MUSIC|Somber]Thought: It might have been kind of cool, those two industrial water tanks decorated with strange charms and old patio furniture...")
    fnDialogue("Thought: If it weren't for the giant scorched hole that blew most of it away.")
    fnDialogue("Thought: It's pretty obvious nobody has been back here since the explosion. ")
    fnDialogue("[CHARENTER|Mari][CHARENTER|Stacy]Mari: [E|Happy]All right, folks! [CRASH]Let's EXPLORE.")
    fnDialogue("Stacy: [E|Mad]But... be careful, okay?")
    fnUnlockGallery("Gallery_Clubhouse")

else

    --Save the game.
    fnDialogue("[BG|Root/Images/Backgrounds/All/ClubHouse][CHARENTER|Bret]Bret: [E|Shock]WHOAH.")
    fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"WeAreHere\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
    PairanormalLevel_FlagCheckpoint("WeAreHere")
    fnUnlockGallery("Gallery_Clubhouse")

end

-- |[Loading the Game]|
--If currently loading the game, set up the investigation.
if(PairanormalLevel_IsLoading() == true) then
	
    --Clear checkpoint.
    PairanormalLevel_FlagCheckpoint("Investigation")
    
	--Variables.
	local sBGPath = "Root/Images/Backgrounds/All/ClubHouse"
	local sScriptPath = gsRoot .. "../Chapter 2/Gameplay_SceneD/0 - ExamineObject.lua"
	
	--Change the background.
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/ClubHouse")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/ClubHouse")

	--Boot investigation scripts.
	Dia_SetProperty("Clear Investigation Characters")
	Dia_SetProperty("Clear Investigation")
	Dia_SetProperty("Start Investigation") 
	Dia_SetProperty("Set Investigation Image", "Root/Images/Backgrounds/All/ClubHouse")
	Dia_SetProperty("Set Investigation Script", gsRoot .. "../Chapter 2/Gameplay_SceneD/0 - ExamineObject.lua")
	Dia_SetProperty("Set Go Home Strings", gsRoot .. "../Chapter 2/Gameplay_SceneB/7 - PatchGoingHome.lua", "Default")
	
	--Talkable Characters.
	Dia_SetProperty("Add Talkable Character", "Mari",     gsRoot .. "../Chapter 2/Gameplay_SceneD/1 - Present Mari.lua")
	Dia_SetProperty("Add Talkable Character", "Bret",     gsRoot .. "../Chapter 2/Gameplay_SceneD/3 - Present Bret.lua")
	Dia_SetProperty("Add Talkable Character", "Michelle", gsRoot .. "../Chapter 2/Gameplay_SceneD/5 - Present Michelle.lua")
	
	--Talkable characters as inventory objects.
	local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
	Dia_SetProperty("Add Discovered Object", "Mari",     sCharPath, sScriptPath, "Mari",     1322,   2, 1322 + 438,   2 + 363)
	Dia_SetProperty("Add Discovered Object", "Stacy",    sCharPath, sScriptPath, "Stacy",     442, 367,  442 + 438, 367 + 363)
	Dia_SetProperty("Add Discovered Object", "Bret",     sCharPath, sScriptPath, "Bret",        2,   2,    2 + 438,   2 + 363)
	Dia_SetProperty("Add Discovered Object", "Michelle", sCharPath, sScriptPath, "Michelle", 1762,   2, 1762 + 438,   2 + 363)
	Dia_SetProperty("Clear Discovered Object")
    
    --SLF file adds the examinable objects.
    Dia_SetProperty("Parse SLF File", gsRoot .. "../Chapter 2/MapData/InvestigationAA.slf")
	
	--Check all the items and re-add them to the inventory.
	for i = 1, gciInvC_Total-1, 1 do
		
		--Variable name.
		local sVarName = "Root/Variables/Chapter2/InvestigationA/iObject|" .. gczaObjC[i][1]
		local iHasItem = VM_GetVar(sVarName, "N")
		
		--Has the item. Add it.
		if(iHasItem == 1.0) then
			Dia_SetProperty("Add Discovered Object", gczaObjC[i][1], sBGPath, sScriptPath, gczaObjC[i][1], gczaObjC[i][2], gczaObjC[i][3], gczaObjC[i][2]+gczaObjC[i][4], gczaObjC[i][3]+gczaObjC[i][5])
		end
		Dia_SetProperty("Clear Discovered Object")
	end

	--Modify the time max/left.
    VM_SetVar("Root/Variables/System/Investigation/sInvestigationPath", "S", "Root/Variables/Chapter2/Investigation/")
	local iTimeMax  = VM_GetVar("Root/Variables/Chapter2/Investigation/iTimeMax",  "N")
	local iTimeLeft = VM_GetVar("Root/Variables/Chapter2/Investigation/iTimeLeft", "N")
	Dia_SetProperty("Investigation Time Max",  iTimeMax)
	Dia_SetProperty("Investigation Time Left", iTimeLeft)

-- |[Normal Operations]|
--Otherwise, flag the game to save and boot the investigation normally.
else
    --Save the game.
    fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Investigation\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

    --Set the time max/left.
    fnCutsceneInstruction([[ VM_SetVar("Root/Variables/Chapter2/Investigation/iTimeMax",  "N", 60.0) ]])
    fnCutsceneInstruction([[ VM_SetVar("Root/Variables/Chapter2/Investigation/iTimeLeft", "N", 60.0) ]])

    --Boot investigation scripts.
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Investigation Characters") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Investigation Time Max", 60) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Investigation Time Left", 60) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Set Investigation Image", "Root/Images/Backgrounds/All/ClubHouse") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Set Investigation Script", gsRoot .. "../Chapter 2/Gameplay_SceneD/0 - ExamineObject.lua") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Set Go Home Strings", gsRoot .. "../Chapter 2/Gameplay_SceneB/7 - PatchGoingHome.lua", "Default") ]])

    --Talkable characters.
    fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Mari",     gsRoot .. "../Chapter 2/Gameplay_SceneD/1 - Present Mari.lua") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Bret",     gsRoot .. "../Chapter 2/Gameplay_SceneD/3 - Present Bret.lua") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Michelle", gsRoot .. "../Chapter 2/Gameplay_SceneD/5 - Present Michelle.lua") ]])
    
    --SLF file adds the examinable objects.
    fnCutsceneInstruction([[ Dia_SetProperty("Parse SLF File", gsRoot .. "../Chapter 2/MapData/InvestigationAA.slf") ]])

    --Add the talkable characters as the first examine objects.
    fnCutsceneInstruction([[ Dia_SetProperty("Add Discovered Object", "Mari",     "Root/Images/Backgrounds/All/CharPortraits", gsRoot .. "../Chapter 2/Gameplay_SceneD/0 - ExamineObject.lua", "Mari",     1322,   2, 1322 + 438,   2 + 363) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Discovered Object", "Stacy",    "Root/Images/Backgrounds/All/CharPortraits", gsRoot .. "../Chapter 2/Gameplay_SceneD/0 - ExamineObject.lua", "Stacy",     442, 367,  442 + 438, 367 + 363) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Discovered Object", "Bret",     "Root/Images/Backgrounds/All/CharPortraits", gsRoot .. "../Chapter 2/Gameplay_SceneD/0 - ExamineObject.lua", "Bret",        2,   2,    2 + 438,   2 + 363) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Discovered Object", "Michelle", "Root/Images/Backgrounds/All/CharPortraits", gsRoot .. "../Chapter 2/Gameplay_SceneD/0 - ExamineObject.lua", "Michelle", 1762,   2, 1762 + 438,   2 + 363) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])

    fnCutsceneBlocker()
    
    --Time variable.
    VM_SetVar("Root/Variables/System/Investigation/sInvestigationPath", "S", "Root/Variables/Chapter2/Investigation/")
    
end
PairanormalLevel_FlagCheckpoint("Investigation")
fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Investigation\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

