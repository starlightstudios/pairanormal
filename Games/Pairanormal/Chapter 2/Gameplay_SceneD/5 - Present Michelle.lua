--[Michelle Presentation]
--Showing stuff to Michelle.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--Variables.
local sScriptPath = gsRoot .. "../Chapter 2/Gameplay_SceneD/1 - Present Mari.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
local sAutoAdvance = gsRoot .. "../Chapter 2/Gameplay_SceneB/7 - PatchGoingHome.lua"

--Saving
local sSaveExecute = gsRoot .. "../Chapter 2/Gameplay_SceneB/0 - See You At The Club.lua"
local sSaveTopic = "No Topic"
local sSaveFlag = "Investigation"

--[Friendship Increment]
--If this is the first time presenting this object to this character then we increase the friendship tracker for
-- that character.
local fnIncrementFriendship = function(iObjectIndex, iIncrement)
	
    --Arg check.
    if(iObjectIndex == nil) then return end
    if(iIncrement == nil) then return end
    
    --Build strings.
	local i = iObjectIndex
    local p = iMichelleIndex
    local sObjectString = string.format("%02i", i)
    local sSubString    = string.format("%02i", p)
    local iIsRepeat = VM_GetVar("Root/Variables/Chapter2/InvestigationA/iObjectPresent|" .. sObjectString .. sSubString, "N")
    if(iIsRepeat == 0.0) then
        
        --Flag
        VM_SetVar("Root/Variables/Chapter2/InvestigationA/iObjectPresent|" .. sObjectString .. sSubString, "N", 1.0)
    
        --Variable gain.
        local iMichelleFriend  = VM_GetVar("Root/Variables/System/Romance/iMichelleFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iMichelleFriend", "N", iMichelleFriend + iIncrement)
    end
end

--[Chat Sequences]
--Chatting.
if(sTopicName == "Chat") then
	
	--Variables.
	local iChattedWithMichelle = VM_GetVar("Root/Variables/Chapter2/Investigation/iChattedWithMichelle", "N")
	
	--First pass:
	if(iChattedWithMichelle == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter2/Investigation/iChattedWithMichelle", "N", 1.0)
		
		--Text.
		fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
		fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
		fnCutsceneBlocker()
		fnDialogue([[[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Player: Hey Michelle.]])
		fnDialogue([[Michelle: [E|Neutral]How's it going, weirdo?[P] Sorry about the state of this place.]])
		fnDialogue([[Thought: She thumbs a few of the drawings on the blown out tank thoughtfully.]])
		fnDialogue([[Player: Did you make these?]])
		fnDialogue([[Michelle: [E|Sad]Yeah.]])
		fnDialogue([[Player: They look nice.[P] You're really talented.]])
		fnDialogue([[Michelle: [E|Blush].[P].[P].[P]yeah, thanks.]])
		fnDialogue([[Player: Sorry some of them got blown up.]])
		fnDialogue([[Michelle: [E|Happy]S'allright.[P] Now that the tank is open, I can draw all on the inside of it, too.]])
		fnDialogue([[Player: Good point.[P] We'll have to clean up the glass and stuff first though--Someone could get hurt.]])
		fnDialogue([[Michelle: [E|Happy]Tch![P][E|Neutral] Don't have to worry about me, babe. I can take care of myself, all right?]])
		fnDialogue([[Michelle: A little glass isn't gonna scare--]])
		fnDialogue([[Info: 'DONK']])
		fnDialogue([[Michelle: [E|Mad][CRASH]CHRIST![P][CRASH] FUCK!![P][CRASH] SHIT!!!!]])
		fnDialogue([[Thought: Michelle bends over, clutching her right shin.]])
		fnDialogue([[Thought: She must have accidentally brushed it up against a protruding piece of the tank.]])
		fnDialogue([[Player: [SFX|World|Blink]Oh my gosh, Are you okay?]])
		fnDialogue([[Michelle: [E|Sad]I'm fine...[P][E|Mad]FUCK![P][P] Just...[P] just go walk it off somewhere!]])
		fnDialogue([[Player: [SFX|Blink]W-walk it off?[P] Me?]])
		fnDialogue([[Michelle: [E|Blush]...[P]YES!]])
		fnDialogue([[Player: But I'm not the one--]])
		fnDialogue([[Michelle: [E|Mad][CRASH]GO!!!!]])
		fnDialogue([[Player: O-okay![P] I'm off!]])
		fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
		fnCutsceneBlocker()
    
        --Variable gain.
        local iMichelleFriend  = VM_GetVar("Root/Variables/System/Romance/iMichelleFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iMichelleFriend", "N", iMichelleFriend + 5.0)
	
        --Time loss.
        if(fnDecrementTime(5, sAutoAdvance) == false) then
            PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
        end
        
	--Repeat passes.
	else
		fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
		fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
		fnCutsceneBlocker()
		fnDialogue("[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Player: Hey Michelle.[P] Is your shin any better?")
		fnDialogue("Michelle: [E|Blush]It's fine![P] Now quit bugging me, weirdo![P] Don't you have some paranormal investigating to do, or whatever?")
		fnDialogue("Player: Fair enough.")
		fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
		fnCutsceneBlocker()

	end
	
--Desk.
elseif(sTopicName == "Tank A") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Michelle: [E|Happy]That's the clubhouse, weirdo.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_TankA, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "Tank B") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Michelle: [E|Mad]Bloody hell, that's a huge hole.[P][E|NEutral] Imagine standing next to that thing while it went off.")
    fnDialogue("Player: Do you have any idea why it might have exploded?")
    fnDialogue("Michelle: [E|Happy]Not really, no.[P] As far as I know, this whole place was supposed to be a secret.")
    fnDialogue("Michelle: Nobody's ever come round before.")
    fnDialogue("Thought: She squints at the gaping hole in the tank, leaking water everywhere.")
    fnDialogue("Michelle: [E|Shock]And maybe with good reason, too.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_TankB, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "Rubble") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Michelle: [E|Neutral]That's a mess, all right.[P] I sure as hell won't be digging through there till it dries.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_Rubble, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
elseif(sTopicName == "Grafitti") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Michelle: [E|Blush]Don't tell anyone, but I'm most proud of the work I've done here.")
    fnDialogue("Michelle: [E|Happy] I've never had a whole wall to work on before.")
    fnDialogue("Thought: She looks at me, her brow suddenly creased in suspicion.")
    fnDialogue("Player: What?")
    fnDialogue("Michelle: [E|Blush]Do you... draw?[P][P] Are you any good?")
    fnDialogue("Player: I don't know.[P] I've never tried it.")
    fnDialogue("Michelle: [E|Sad]mm.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_Grafitti, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "DollLeg") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Michelle: [E|Mad]Oh, gross![P] Get that away from me!!")
    fnDialogue("Player: It's just a doll leg.")
    fnDialogue("Thought: Michelle regains her composure,[P] but her nose remains scrunched up in disgust.")
    fnDialogue("Michelle: [E|Shock]What's that brown stain on its leg?")
    fnDialogue("Player: Uh, I dunno.")
    fnDialogue("Michelle: [E|Mad]Looks like the soda stain from yours.[P][P][P] Creepy.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_DollLeg, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "ColaBottle") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Michelle: [E|Happy]You ever chucked a glass bottle at a wall? It's fun.")
    fnDialogue("Michelle: [E|Blush]Just make sure you don't hurt yourself doing it.[P] The glass flies everywhere.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_ColaBottle, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "Trees") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Michelle: [E|Neutral]Lots of trees here.[P] I like how secluded it is.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_Trees, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
elseif(sTopicName == "Chairs") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Michelle: [E|Neutral]We found those last year...[P]*sigh*![P] Looks like we'll have to replace them.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_Chairs, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "Chimes") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Michelle: [E|Neutral]Wind chimes [ITALICS]seem[ENDITALICS] like a nice idea,[P] but Mari is constantly untangling them on account of the wind.")
    fnDialogue("Michelle: [E|Mad] More trouble than its worth, if you ask me.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_Chimes, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "Footprints") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Michelle: [E|Shock]Whose footprints are those?")
    fnDialogue("Player: Beats me.[P] They look to be about your size, though.")
    fnDialogue("Michelle: [E|Mad]Well they're [ITALICS]not[ENDITALICS] mine, babe.[P][E|Happy] I wouldn't be caught dead in pair of penny loafers like those.")
    fnDialogue("Player: 'Penny loafers'?")
    fnDialogue("Michelle: [E|Neutral]Yeah. Can't you see the shape of the heel?[P] Sneaker bottoms don't look like that.")
    fnDialogue("Michelle: [E|Blush] Looks like the heels of a shoe you'd wear to a business meeting.")
    fnDialogue("Player: Huh...[P]that's pretty observant.")
    fnDialogue("Michelle: [E|Mad]Yeah, well I'm part of a paranormal investigation club, aren't I?[P][E|Blush] I can solve mysteries just as good as you lot.")
    fnDialogue("Player: Fair enough.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_Footprints, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "WalkieTalkie") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Michelle: [E|Happy]Hah! been a while since I've seen one of those.[P] Does it work?")
    fnDialogue("Player: Apparently not.")
    fnDialogue("Michelle: [E|Sad]Mm.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_WalkieTalkie, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "Journal") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Player: Erm, Michelle...[P] c-could you read this journal for me?[P] I don't really know how.")
    fnDialogue("Michelle: [E|Blush]... Yeah, all right. Give it.")
    fnDialogue("Thought: She flips it open to a random page and starts reading.")
	fnDialogue("Michelle: [E|Neutral]'It was the most horrifying, sickening experience of my life.'")
	fnDialogue("Michelle: [E|Neutral]'They spoke to me in a manner I can't even describe. [P]And even though I suppose they meant well, I was beside myself with ill.'")
	fnDialogue("Michelle: [E|Neutral]'Oh, if my family saw what's become of me now...[P]I'd be sent to the madhouse for sure.'")
	fnDialogue("Player: ...")
	fnDialogue("Michelle: [E|Shock]Jesus. What kind of creepypasta trite is this?")
	fnDialogue("Michelle: [E|Happy]You have some seriously strange literary taste, babe.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_WalkieTalkie, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
	
--Person: Mari.
elseif(sTopicName == "Mari") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Michelle: [E|Neutral]Mari's quite brave, coming back here.[P][E|Sad] I heard she was actually here when the explosion happened.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_Mari, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
	
--Person: Stacy.
elseif(sTopicName == "Stacy") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Michelle: [E|Sad]I don't like how Stacy's acting all off.[P] She's normally the tough one.")
    fnDialogue("Michelle: [E|Mad]I wonder if she knows something we don't?")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_Stacy, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

    
--Person: Bret.
elseif(sTopicName == "Bret") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Michelle: [E|Happy]Look at that nerd.[P] He's having the time of his life.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_Bret, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end


--Person: Michelle.
elseif(sTopicName == "Michelle") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Michelle|Neutral]Michelle: [E|Blush]Oy, what are you staring at?")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_Michelle, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end


end
