-- |[Investigation of the Clubhouse]|
--The first investigation sequence in the classroom.

-- |[Arguments]|
--Get the name of what we're investigating.
local sExamine = LM_GetScriptArgument(0)

-- |[Variable Setup]|
--Used to simplify the Add Discovered Object calls.
local sBGPath = "Root/Images/Backgrounds/All/ClubHouse"
local sScriptPath = gsRoot .. "../Chapter 2/Gameplay_SceneD/0 - ExamineObject.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
local sAutoAdvance = gsRoot .. "../Chapter 2/Gameplay_SceneB/7 - PatchGoingHome.lua"

--Saving
local sSaveExecute = gsRoot .. "../Chapter 2/Gameplay_SceneB/0 - See You At The Club.lua"
local sSaveTopic = "No Topic"
local sSaveFlag = "Investigation"

-- |[Function Setup]|
local fnStdObject = function(iIndex, sLine, sSaveFlag, sSaveExecute, sSaveTopic)
	
    --Argument check.
    if(iIndex == nil) then return end
    
	--Store it in the inventory.
	local i = iIndex
	Dia_SetProperty("Add Discovered Object", gczaObjC[i][1], sBGPath, sScriptPath, gczaObjC[i][1])
	local iExaminedAlready = VM_GetVar("Root/Variables/Chapter2/InvestigationA/iObject|" .. gczaObjC[i][1], "N")
	VM_SetVar("Root/Variables/Chapter2/InvestigationA/iObject|" .. gczaObjC[i][1], "N", 1.0)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction(sLine)
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(iExaminedAlready == 0.0) then
		if(fnDecrementTime(1, sAutoAdvance) == false) then
			PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
		end
	end
    
end

-- |[Examinable Objects]|
if(sExamine == "Tank A") then
    
	--Store it in the inventory.
	local i = gciInvC_TankA
	Dia_SetProperty("Add Discovered Object", gczaObjC[i][1], sBGPath, sScriptPath, gczaObjC[i][1])
	local iExaminedAlready = VM_GetVar("Root/Variables/Chapter2/InvestigationA/iObject|" .. gczaObjC[i][1], "N")
	VM_SetVar("Root/Variables/Chapter2/InvestigationA/iObject|" .. gczaObjC[i][1], "N", 1.0)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: The tank looks [GLITCH]intact. Inside there's a glass vessel of some kind, but it's [GLITCH]empty.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(iExaminedAlready == 0.0) then
		if(fnDecrementTime(1, sAutoAdvance) == false) then
			PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
		end
	end
    
elseif(sExamine == "Tank B") then
    fnStdObject(gciInvC_TankB, [[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: This [GLITCH][GLITCH]tank might have looked exactly like its counterpart,[P] but now a massive,[GLITCH] mysterious hole has been blown into it.") ]], sSaveFlag, sSaveExecute, sSaveTopic)
    
elseif(sExamine == "Rubble") then
    fnStdObject(gciInvC_Rubble, [[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: Lots of glass shards and rubble are scattered nearby the tank.[P] It must've been opened with a powerful force.") ]], sSaveFlag, sSaveExecute, sSaveTopic)
    
elseif(sExamine == "Grafitti") then
    fnStdObject(gciInvC_Grafitti, [[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: 'I'd nvere be prat of a cubl taht wuold hvae me for a mebmer.'[P] Heh.[P] Kind of clever.") ]], sSaveFlag, sSaveExecute, sSaveTopic)
    
elseif(sExamine == "DollLeg") then
    fnStdObject(gciInvC_DollLeg, [[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: It's a tiny leg.[P] Looks fake, thank goodness.") ]], sSaveFlag, sSaveExecute, sSaveTopic)
    
elseif(sExamine == "ColaBottle") then
    fnStdObject(gciInvC_ColaBottle, [[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: Ugh, I remember pulling one of these out of the vending machines this morning. If I never see another one of these for the rest of my life,[P] it'll be too soon.") ]], sSaveFlag, sSaveExecute, sSaveTopic)
    
elseif(sExamine == "Trees") then
    fnStdObject(gciInvC_Trees, [[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: This clubhouse is situated pretty nicely. A quiet, secluded grove,[P] plenty of fresh air,[P] and a great view of most of the town.") ]], sSaveFlag, sSaveExecute, sSaveTopic)
    
elseif(sExamine == "Chairs") then
    fnStdObject(gciInvC_Chairs, [[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: I see since the Paranormal Club has gotten its hands on these tanks,[P] they've been covered in all kinds of [GLITCH]makeshift furnishings.") ]], sSaveFlag, sSaveExecute, sSaveTopic)
    
elseif(sExamine == "Chimes") then
    fnStdObject(gciInvC_Chimes, [[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: What are these?[P] They make such a peculiar sound when the wind blows against them.") ]], sSaveFlag, sSaveExecute, sSaveTopic)
    
elseif(sExamine == "Footprints") then
    fnStdObject(gciInvC_Footprints, [[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: Footprints on top of the dirt. I wonder whose?") ]], sSaveFlag, sSaveExecute, sSaveTopic)
    
elseif(sExamine == "WalkieTalkie") then
    fnStdObject(gciInvC_WalkieTalkie, [[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: It looks like a phone, but bulkier. When I press the buttons on the side,[P] it makes a low, rustling noise,[P] like someone rifling through a bag of chips.") ]], sSaveFlag, sSaveExecute, sSaveTopic)
    
elseif(sExamine == "Journal") then
    fnStdObject(gciInvC_Journal, [[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: It's a book full of handwritten notes.[P] I should get someone to help me read it.") ]], sSaveFlag, sSaveExecute, sSaveTopic)

-- |[Mari]|
elseif(sExamine == "Mari") then

	Dia_SetProperty("Add Discovered Object", "Mari",     sCharPath, sScriptPath, "Mari",     1322,   2, 1322 + 438,   2 + 363)
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: Mari's talking to Stacy, and they both ocassionally glance at the tank.[P] She has a real worried look on her face.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(fnDecrementTime(1, sAutoAdvance) == false) then
		PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
	end

-- |[Stacy]|
elseif(sExamine == "Stacy") then

	Dia_SetProperty("Add Discovered Object", "Stacy",    sCharPath, sScriptPath, "Stacy",     442, 367,  442 + 438, 367 + 363)
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: Stacy's talking to Mari, and they both ocassionally glance at the tank.[P] She looks angry.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(fnDecrementTime(1, sAutoAdvance) == false) then
		PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
	end

-- |[Bret]|
elseif(sExamine == "Bret") then

	Dia_SetProperty("Add Discovered Object", "Bret",     sCharPath, sScriptPath, "Bret",        2,   2,    2 + 438,   2 + 363)
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: Bret's examining all the stuff around the explosion site.[P] He's quite fascinated with what happened here.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(fnDecrementTime(1, sAutoAdvance) == false) then
		PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
	end

elseif(sExamine == "Michelle") then

	Dia_SetProperty("Add Discovered Object", "Michelle", sCharPath, sScriptPath, "Michelle", 1762,   2, 1762 + 438,   2 + 363)
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: Michelle seems more interested in the grafitti on the tank walls than the actual ruptured container. ") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALLFAST]Thought: It's no wonder-- [P]looks like the beautiful drawings she made were partly blown off. ") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
	if(fnDecrementTime(1, sAutoAdvance) == false) then
		PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
	end
end