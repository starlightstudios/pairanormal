--[Bret Presentation]
--Showing stuff to Bret.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--Variables.
local sScriptPath = gsRoot .. "../Chapter 2/Gameplay_SceneD/1 - Present Mari.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
local sAutoAdvance = gsRoot .. "../Chapter 2/Gameplay_SceneB/7 - PatchGoingHome.lua"

--Saving
local sSaveExecute = gsRoot .. "../Chapter 2/Gameplay_SceneB/0 - See You At The Club.lua"
local sSaveTopic = "No Topic"
local sSaveFlag = "Investigation"

--[Friendship Increment]
--If this is the first time presenting this object to this character then we increase the friendship tracker for
-- that character.
local fnIncrementFriendship = function(iObjectIndex, iIncrement)
	
    --Arg check.
    if(iObjectIndex == nil) then return end
    if(iIncrement == nil) then return end
    
    --Build strings.
	local i = iObjectIndex
    local p = iBretIndex
    local sObjectString = string.format("%02i", i)
    local sSubString    = string.format("%02i", p)
    local iIsRepeat = VM_GetVar("Root/Variables/Chapter2/InvestigationA/iObjectPresent|" .. sObjectString .. sSubString, "N")
    if(iIsRepeat == 0.0) then
        
        --Flag
        VM_SetVar("Root/Variables/Chapter2/InvestigationA/iObjectPresent|" .. sObjectString .. sSubString, "N", 1.0)
    
        --Variable gain.
        local iBretFriend  = VM_GetVar("Root/Variables/System/Romance/iBretFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iBretFriend", "N", iBretFriend + iIncrement)
    end
end

--[Chat Sequences]
--Chatting.
if(sTopicName == "Chat") then
	
	--Variables.
	local iChattedWithBret = VM_GetVar("Root/Variables/Chapter2/Investigation/iChattedWithBret", "N")
	
	--First pass:
	if(iChattedWithBret == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter2/Investigation/iChattedWithBret", "N", 1.0)
		
		--Text.
		fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
		fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
		fnCutsceneBlocker()
		fnDialogue([[[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|Shock]Oh![P] Hi![P] Sup!]])
		fnDialogue([[Player: Hi, Bret.]])
		fnDialogue([[Bret: [E|Happy]Pretty wack-a-doodle stuff, isn't it?]])
		fnDialogue([[Thought: He gestures to the debris scattered around the clubhouse site.]])
		fnDialogue([[Player: Yeah.[P] Kind of scary to think about.]])
		fnDialogue([[Thought: Bret nods.[P] He doesn't seem his usual talkative self;[P] I can tell he's thinking about everything before him.]])
		fnDialogue([[Bret: [E|Neutral]You think so? I find myself kind of...[P] fascinated by it, I guess.]])
		fnDialogue([[Thought: He points to the undamaged tank, with an open door. ]])
		fnDialogue([[Bret: [E|Shock]That second tank has always been empty;[P] and when we knocked on the first one, it sounded hollow, too.]])
		fnDialogue([[Bret: [E|Mad] So why would it explode?[P] Escpecially considering nobody's known about this place for years except us.]])
		fnDialogue([[Thought: Bret shakes his head, pushing his glasses up his nose.]])
		fnDialogue([[Bret: [E|Sad]It's a real mystery all right.[P] Maybe not a paranormal one, but.[P].[P].[P][E|Blush] it's exactly why I signed up for this club.]])
		fnDialogue([[Bret: [E|Happy] What good is high school if you're not having a couple adventures,[P] right?]])
		fnDialogue([[Player: Heh.[P] You know, you're kind of cool sometimes, Bret.]])
		fnDialogue([[Bret: [E|Shock]Wh-what?[P] [E|Blush]Uh... [E|Happy]thanks. [E|Blush]I think you're pretty c-[P]cool too,]])
		fnDialogue([[Bret: [E|Mad] I mean, that is to say it's [E|Blush]cool of you [SPEEDTEXT]to [E|Neutral]say that to someone with a bottlecap collection that fills up four or five mayonnaise jars[P][P][E|Shock]--which are empty, of course![ENDSPEEDTEXT] ]])
		fnDialogue([[Bret: [E|Sad]... uhh...[P][P] I'm gonna go stand over here for a minute. ]])
		fnDialogue([[Player: Okay, Bret.]])
		fnDialogue([[Bret: [E|Shock]Oh, and bring me any interesting things you find!]])
		fnDialogue([[Bret: [E|Neutral] Let's see if we can't get to the bottom of this explosion!]])
		fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
		fnCutsceneBlocker()
    
        --Variable gain.
        local iBretFriend  = VM_GetVar("Root/Variables/System/Romance/iBretFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iBretFriend", "N", iBretFriend + 5.0)
	
        --Time loss.
        if(fnDecrementTime(5, sAutoAdvance) == false) then
            PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
        end
		
	--Repeat passes.
	else
		fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
		fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
		fnCutsceneBlocker()
		fnDialogue("[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|Happy]How's it hanging, home-sauce?[P][E|Mad] Got anything interesting to show the Bretmeister?")
		fnDialogue("Player: I think I'm gonna keep looking around.")
		fnDialogue("Bret: [E|Shock]Right![P] Coolio![P][E|Blush] I'll just be.[P].[P].[P].I'll just be standing here.")
		fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
		fnCutsceneBlocker()
	end
	
--Desk.
elseif(sTopicName == "Tank A") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|Happy]There she is![P] the ooool' club house.[BR] [E|Blush]Pretty sweet digs, don't you think?[P][E|Neutral] Oh yeah, lot of memories in this place...")
    fnDialogue("Thought: We both survey the water spilling out from the exploded tank, creating rivers of mud.")
    fnDialogue("Thought: One of the lawn chairs leans dangerously to the left, its leg decimated by rubble.")
    fnDialogue("Bret:[P][P][E|Sad]... Of course,[P] it's seen better days.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_TankA, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "Tank B") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret:[E|Happy] [PLAYERNAME], have you noticed this?[P] Here's the real kicker.")
    fnDialogue("Player: What?")
    fnDialogue("Bret: [E|Shock]Check out the fragments of the tank shell.[P] They're bending outwards.")
    fnDialogue("Player: Uh huh...")
    fnDialogue("Bret: [E|Mad]I'd betcha a bacon sandwich the explosion came from inside the tank.")
    fnDialogue("Player: ... That doesn't make sense.[P] I thought this tank was sealed shut.")
    fnDialogue("Player: How could someone start an explosion from inside if they were trying to [ITALICS]get[ENDITALICS] in?")
    fnDialogue("Thought: Bret shrugs.")
    fnDialogue("Bret: [E|Sad]I mean,[P][P] who's to say someone was trying to get in?")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_TankB, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "Rubble") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|Blush]Yeah, the clubhouse is a lot... wetter than it usually is.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_Rubble, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
elseif(sTopicName == "Grafitti") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|Mad]I'll say this about Michelle:: She knows how to make the clubhouse look real spiffy.")
    fnDialogue("Bret: [E|Sad] I mean, a year ago that whole tank was bare as a dog bone,[P][E|Mad] know what I mean?")
    fnDialogue("Bret: [E|Neutral]Then, when she joined the club,[P][E|Mad] [ITALICS]boom![ENDITALICS]")
    fnDialogue("Bret: [E|Shock] She hauls in this big 'ol tub of paints and stuff,[P][E|Sad] who knows where she got it--")
    fnDialogue("Bret: [E|Neutral] and the next thing you know, our secret hideaway became a--")
    fnDialogue("Thought: Bret takes on a singsong voice, waving his hands for emphasis.")
    fnDialogue("Bret: [E|Happy][ITALICS]Secret hiiiiiide-awaaaaaay[ENDITALICS]")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_Grafitti, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "DollLeg") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|Shock]I don't believe it.[P] Is that another victorian doll piece?")
    fnDialogue("Player: [SFX|World|Blink][P][P]Another? [P]You mean it happened [CRASH][ITALICS]more than once[ENDITALICS]???")
    fnDialogue("Bret: [E|Neutral]Yep; every now and again you'll find a finger or a leg or something in the woods.")
	fnDialogue("Bret: [E|Neutral]Once Mari discovered an entire doll with a [E|Shock]dead squirrel inside!")
    fnDialogue("Bret: We kept an eye out, and what do you know?[E|Sad] Those things are actually all over the place.")
    fnDialogue("Bret: [E|Happy] Matter of fact, we've been trying to crack that case for a long time.")
    fnDialogue("Bret: [E|Mad] It was number one on the club agenda...[P][P][E|Shock] until this explosion happened, of course. ")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_DollLeg, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "ColaBottle") then
    VM_SetVar("Root/Variables/Chapter2/InvestigationA/iShowedBretBottleCap", "N", 1.0)
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|Neutral]Holy crap.[P][P][E|Shock][CRASH] HOLY CRAP! Wh-[P]where did you find this?")
    fnDialogue("Player: It was on the floor right here.")
    fnDialogue("Bret: [E|Mad]Right here? [P][E|Shock]RIGHT HERE???")
    fnDialogue("Thought: Bret calls out to the members of the club excitedly.")
    fnDialogue("Bret: [E|Shock]HEY, D-DID ANYONE BUY THIS BOTTLE OF SODA? [CRASH]ANYONE???")
    fnDialogue("Thought: Everyone shakes their head no.[P] Bret crouches, his eyes scanning the forest floor.")
    fnDialogue("Player: What is it?")
    fnDialogue("Bret: [E|Blush]This is a bottle of 1956 vintage Kane Kola.[P] You can tell by the shape and curvature of the neck;")
    fnDialogue("Bret: In fact, I bet--[P][E|Mad]on the bottom, it should be stamped.[P].[P].[P]yep![P][E|Happy] Oh my gosh![P] It's real!")
    fnDialogue("Bret: [E|Sad][CRASH]These are like, so hard to find!")
    fnDialogue("Bret: [E|Mad] I've been,[P] I mean,[P][E|Blush] I collect bottlecaps, and this one has been [E|Shock]missing from my collection forever. ")
    fnDialogue("Thought: Bret grabs me by the shoulders tightly.")
    fnDialogue("Bret: [E|Mad][PLAYERNAME],[P] seriously,[P] if you see a bottlecap ANYWHERE,[P][E|Shock] PLEASE bring it to me!")
    fnDialogue("Bret: [E|Neutral] This could be one of the greatest finds of,[P][E|Mad] like,[P][E|Blush] my life!")
    fnDialogue("Player: You can count on me, cap'n.")
    fnDialogue("Thought: Bret nods.[P] Having realized he's still holding my shoulders, he quickly releases me.")
    fnDialogue("Thought: [ITALICS]If he's this happy now, what will he do if I actually -find- that bottlecap?[ENDITALICS]")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_ColaBottle, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "Trees") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|Neutral]MMM-MMM![P][E|Mad] Nothing like...[P][P][E|Blush]some fresh air, and like...[P][P][E|Shock] trees.[P][E|Neutral] mm.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_Trees, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
elseif(sTopicName == "Chairs") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret:[E|Mad] Lawn chairs, oh yeah baby.[P][E|Happy] 34.99 at BoxMart, [E|Shock]But we got' em totally free.[P][P][E|Blush] Found 'em in a dumpster.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_Chairs, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "Chimes") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|Neutral]Wind chimes are awesome.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_Chimes, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "Footprints") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Player: Can you tell me anything about these footprints?")
    fnDialogue("Bret: [E|Blush]Weeeeeeeell [E|Happy][CRASH]THIS IS A STORY ALL ABOUT HOW,[P] MY LIFE GOT FLIP-TURNED UPSIDE-DOWN,")
    fnDialogue("[CHARENTER|Mari]Mari: [E|Mad][CRASH]I'D LIKE TO TAKE A MINUTE")
    fnDialogue("[CHARENTER|Stacy]Stacy: [E|Happy][CRASH]JUST SIT RIGHT THERE")
    fnDialogue("Thought: The group looks at Michelle expectantly.[P] She rolls her eyes.")
    fnDialogue("[CHARENTER|Michelle]Michelle: [E|Happy]*sigh*.[P].[P].[P][CRASH][E|Mad] I'LL TELL YOU HOW I BECAME THE PRINCE OF A TOWN CALLED BEL AIR!")
    fnDialogue("Player:[P][P][P]... What the heck was that?")
    --It's called "Trying too hard"
    fnDialogue("Bret: [E|Neutral]I'm trying to tell you that the prints are fresh! [E|Happy][ITALICS]Fresh prints!![ENDITALICS] Get it!?!?")
    fnDialogue("Player: No.[P][P] Obviously.")
    fnDialogue("Thought: The club members share a laugh while Bret massages his temples in frustration.")
    fnDialogue("Bret: [E|Blush]Oh man, how will I ever get you to like me if you can't understand [ITALICS]literally[ENDITALICS] every pop culture reference I make? ")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_Footprints, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "WalkieTalkie") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|Neutral]Looks like Laura's old walkie talkie. I'll bet she has the other one, somewhere.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_WalkieTalkie, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
	
	elseif(sTopicName == "Journal") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Player: Bret, can you help me read this journal?[P] I found it here.")
	fnDialogue("Bret: [E|Happy]Absolutely![P][E|Mad] I'm twitching with anticipation to discover the clues, cleverly tucked away in the pages. ")
	fnDialogue("Bret: [E|Shock]Or someone's secret self-insert fanfiction,[E|Sad] although I'm really hoping not.")
	fnDialogue("Thought: He opens the journal to a random page and starts reading.")
	fnDialogue("Bret: [E|Neutral]'They say they can't be separated.[P] It's simply not done where they come from.'")
	fnDialogue("Bret: [E|Neutral]'No matter how many times I've proven it with my calculations--[P]being split effectively doubles their power, rather than burning out quickly as a single unit.'")
	fnDialogue("Bret: [E|Neutral] 'Just think of all the glorious accomplishements that could be made using that energy output![P] I have to make them see reason, somehow")
	fnDialogue("Thought: Bret chuckles.")
	fnDialogue("Bret:[E|Happy] Wow, this guy sounds like he's got major third wheel problems.")
	fnDialogue("Player: What's a third wheel?")
	fnDialogue("Bret: [E|Sad] My entire existence, basically.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_WalkieTalkie, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
	end

--Person: Mari.
elseif(sTopicName == "Mari") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|Shock]I'm surprised Mari took us back here.[P] I heard she was actually present when the explosion took place.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_Mari, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
	
--Person: Stacy.
elseif(sTopicName == "Stacy") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|Shock]Stacy sure was hesitant to come back here, eh?")
    fnDialogue("Player: What do you mean? She seems fine. [P] Not as enthusiastic as usual, but... fine.")
    fnDialogue("Bret: [E|Happy]Oh, trust me. [P]'Not as enthusiastic' is Stacy's way of [E|Mad]being totally opposed to something.")
    fnDialogue("Bret: [E|Neutral]You don't know her too well yet to tell the difference, but you will.")
	fnDialogue("Thought: Why did that comment sting like it did?")
	fnDialogue("Player: [P][P]Well...[P]why [ITALICS]is[ENDITALICS] she hesitant to come back here? Is it the explosion?")
    fnDialogue("Bret: [E|Shock]Now [ITALICS]that's[ENDITALICS] hard to say.[P][E|Sad] I guess even I don't know her well enough, either, haha.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_Stacy, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
--Person: Bret.
elseif(sTopicName == "Bret") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|Blush]Uhh, don't mind me![P][E|Mad] if you find anything interesting I'll be happy to take a look at it.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_Bret, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

--Person: Michelle.
elseif(sTopicName == "Michelle") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|Happy]Ah, Michelle.[P][E|Mad] As usual, she's not exactly occupied [E|Sad]with the actual investigation portion of the club.")
    fnDialogue("Bret: [E|Shock] Or any portion, really,[P] [E|Mad]except for sitting in a corner,[P] looking cool,[P] [E|Blush]and calling me a nerd.")
    fnDialogue("Player: Yeah,[P] I noticed that Michelle doesn't really have an interest in paranormal stuff.")
    fnDialogue("Player: Why exactly did she join the club?")
    fnDialogue("Bret: [E|Shock]Beats me. [P][E|Neutral]But she's never missed a meeting, so she must be getting something out of it.")
    fnDialogue("Bret: [E|Mad] Maybe I'm luring her back [E|Happy]with my... my [ITALICS]manly charm.[ENDITALICS] ")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_Michelle, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

end
