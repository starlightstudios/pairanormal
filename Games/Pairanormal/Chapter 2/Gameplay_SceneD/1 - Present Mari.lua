--[Mari Presentation]
--Showing stuff to Mari.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--Variables.
local sScriptPath = gsRoot .. "../Chapter 2/Gameplay_SceneD/1 - Present Mari.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
local sAutoAdvance = gsRoot .. "../Chapter 2/Gameplay_SceneB/7 - PatchGoingHome.lua"

--Saving
local sSaveExecute = gsRoot .. "../Chapter 2/Gameplay_SceneB/0 - See You At The Club.lua"
local sSaveTopic = "No Topic"
local sSaveFlag = "Investigation"

--[Friendship Increment]
--If this is the first time presenting this object to this character then we increase the friendship tracker for
-- that character. In this case, both Mari and Stacy get incremented.
local fnIncrementFriendship = function(iObjectIndex, iIncrement)
	
    --Arg check.
    if(iObjectIndex == nil) then return end
    if(iIncrement == nil) then return end
    
    --Build strings. Use Mari's index for both.
	local i = iObjectIndex
    local p = iMariIndex
    local sObjectString = string.format("%02i", i)
    local sSubString    = string.format("%02i", p)
    local iIsRepeat = VM_GetVar("Root/Variables/Chapter2/InvestigationA/iObjectPresent|" .. sObjectString .. sSubString, "N")
    if(iIsRepeat == 0.0) then
        
        --Flag
        VM_SetVar("Root/Variables/Chapter2/InvestigationA/iObjectPresent|" .. sObjectString .. sSubString, "N", 1.0)
    
        --Variable gain.
        local iMariFriend  = VM_GetVar("Root/Variables/System/Romance/iMariFriend", "N")
        local iStacyFriend = VM_GetVar("Root/Variables/System/Romance/iStacyFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iMariFriend", "N", iMariFriend + iIncrement)
        VM_SetVar("Root/Variables/System/Romance/iStacyFriend", "N", iStacyFriend + iIncrement)
    end
end

--[Chat Sequences]
--Chatting.
if(sTopicName == "Chat") then
	
	--Variables.
	local iChattedWithMari = VM_GetVar("Root/Variables/Chapter2/Investigation/iChattedWithMari", "N")
	
	--First pass:
	if(iChattedWithMari == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter2/Investigation/iChattedWithMari", "N", 1.0)
		
		--Text.
		fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
		fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
		fnCutsceneBlocker()
		fnDialogue([[[HIDEALLFAST][SHOWCHAR|Mari|Neutral][SHOWCHAR|Stacy|Neutral]Thought: I approach Mari and Stacy.]])
		fnDialogue("Thought: My pace slows a little as I near and hear the tone of their conversation.") 
		fnDialogue("Mari: [E|Neutral]--promise you, I'm [ITALICS]fine[ENDITALICS].[P] I want to check this place out!") 
		fnDialogue("Stacy: [E|Sad]Yes, you're fine now, but what if something else happens?")
		fnDialogue("Mari: [E|Neutral]It won't.[P] This time, we're here together, right?")
		fnDialogue("Thought: Mari places a gentle hand on Stacy's shoulder,[P] who pulls away when she notices me.")
		fnDialogue("Stacy: [E|Shock]*ahem*![P][E|Happy][SFX|World|Ugh] Hi, [PLAYERNAME]!")
		fnDialogue("Mari: [E|Shock][SFX|World|Ugh]Heeeeeeeey!")
		fnDialogue("Player: Hey guys.[P] Is everything okay?")
		fnDialogue("Mari: [E|Neutral]Everything is fine.[P] Stacy's just worried because the last time I was here, the tank exploded.")
		fnDialogue("Player: ...[P][CRASH]Wh-what?[P] [ITALICS]You were here[ENDITALICS] when that happened!?")
		fnDialogue("Mari: [E|Blush]Haha, yeah.[P][E|Neutral] Do you remember when we met in the hospital, [PLAYERNAME]?")
		fnDialogue("Player: You mean...")
		fnDialogue("Thought: Mari nods.")
		fnDialogue("Mari: [E|Neutral]I went to the clubhouse to prep it for the coming year.")
		fnDialogue("Mari: [E|Neutral]Rake up the leaves, clean out the cobwebs, you know.[P] And then, suddenly, out of nowhere--[P][P][E|Shock][CRASH]BOOM!!!")
		fnDialogue("Mari: [E|Shock]I'm sent flying.[P][E|Sad] Next thing I know, I wake up in the hospital.")
		fnDialogue("Stacy: [E|Mad]Mari was there for almost a month, totally knocked out.[P] I...[P][E|Sad]None of us even found out until she called.")
		fnDialogue([[Mari: [E|Sad]I'm sorry for making you worry, Stacy.[P][E|Happy] But luckily I had my special quartz amulet!]])
        fnUnlockGallery("Gallery_Scene_MariNecklace")
		fnDialogue([[[CHAREXIT|ALL][IMAGE|Root/Images/MajorScenes/All/CH2_MariNecklace]Thought: Mari withdraws a thin golden chain from under her collar.[P] I recognize the pink stone dangling off it.]])
		fnDialogue([[Player: Oh yeah.[P] You were wearing that in the hospital.]])
		fnDialogue([[Thought: Even though the mineral is muddy with imperfections, [GLITCH] It almost seems to [GLITCH] give off its own light.]])
		fnDialogue("Player: [IMAGE|Null][SHOWCHAR|Mari|Neutral][SHOWCHAR|Stacy|Neutral]Nhh.[GLITCH][GLITCHTEXT]SHOULDHAVE BE ENTHER E[ENDGLICHTEXT][P].[P].[P]S-so after all that, who found you?")
		fnDialogue ("Mari: [E|Blush]Eh?")
		fnDialogue ("Stacy: [E|Shock][ITALICS]Dios mio![ENDITALICS]. That's right--[P]in all the commotion back then I never asked.")
		fnDialogue ("Stacy: [E|Sad]Who found you after the explosion and took you to the hospital?")
		fnDialogue ("Thought: Mari freezes, turning a little pale.")
		fnDialogue ("Mari: [E|Shock]I-I don't know, actually.[P][E|Sad] I never asked either.")
		fnDialogue ("Player: ...")
		fnDialogue ("Stacy: [E|Shock]...")
		fnDialogue ("Mari: [E|Sad]P...[P]robably just a good samaritan, though, right?[P][E|N] someone who heard the explosion who came to help??")
		fnDialogue ("Player: Oh yeah yeah--")
		fnDialogue ("Stacy: [E|Neutral]--definitely totally the most logical answer.")
		fnDialogue ("Mari: [E|Neutral]A-anyway,[E|Shock][CRASH] all the more reason for us to investigate![P] There are a lot of questions that need to be answered.")
		fnDialogue ("Stacy: [E|Mad]One thing's for sure; we aren't the only ones who know about the clubhouse anymore.")
		fnDialogue ("Stacy: [E|Mad] I hate to say it, but we might have to start looking for a new one.[P][E|Happy] Soooo, if you'll [ITALICS]pardonnez moi,[ENDITALICS] I'll be gathering up our heavy furniture for the move")
		fnDialogue([[Mari: [E|Happy]And [ITALICS]I'll[ENDITALICS] be looking for clues![P][E|Neutral] Help us out, won't you, [PLAYERNAME]?]])
		fnDialogue([[Player: Will do.]])
    
        --Variable gain.
        local iMariFriend  = VM_GetVar("Root/Variables/System/Romance/iMariFriend", "N")
        local iStacyFriend = VM_GetVar("Root/Variables/System/Romance/iStacyFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iMariFriend", "N", iMariFriend + 1.0)
        VM_SetVar("Root/Variables/System/Romance/iStacyFriend", "N", iStacyFriend + 1.0)
	
        --Time loss.
        if(fnDecrementTime(5, sAutoAdvance) == false) then
            PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
        end
        
		fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
		fnCutsceneBlocker()
		
	--Repeat passes.
	else
		fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
		fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
		fnCutsceneBlocker()
		fnDialogue("[HIDEALLFAST][SHOWCHAR|Mari|Neutral][SHOWCHAR|Stacy|Neutral]Mari: [E|Shock][CRASH]Hi, [PLAYERNAME]![P][E|Neutral] find anything interesting?")
		fnDialogue("Player: Oh, plenty.[P] I'm still looking around, though.")
		fnDialogue("Stacy: [E|Sad]If you feel uncomfortable or want to leave, just let us know.[P] Safety's a big deal right now, okay?")
		fnDialogue("Player: Roger that.")
        
		fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
		fnCutsceneBlocker()

	end
    
    
--[Objects]
elseif(sTopicName == "Tank A") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnCutsceneInstruction("")
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Mari|Neutral][SHOWCHAR|Stacy|Neutral][FOCUS|Stacy]Stacy: [E|Neutral]Mari and I found this place back in Junior High.[P] Stumbled on it [E|Happy][ITALICS]por accidento[ENDITALICS] while we were just goofing off.")
    fnDialogue("Mari: [E|Shock]It's been our clubhouse ever since.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_TankA, 1.0)
    
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
elseif(sTopicName == "Tank B") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Mari|Neutral][SHOWCHAR|Stacy|Neutral]Mari: [E|Shock][CRASH]Ahah! I knew it![P][E|Neutral] It was a water tank all along.")
    fnDialogue("Stacy: [E|Mad]I mean, yeah, there's water all over the place, but.[P].[P].[P][E|Shock] it didn't fill the tank.")
    fnDialogue("Stacy: It must've just been running through that glass tube there.")
    fnDialogue("Mari: [E|Sad]Hmm,[P] maybe it's some kind of filtering system?")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_TankB, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
elseif(sTopicName == "Rubble") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Mari|Neutral][SHOWCHAR|Stacy|Neutral]Mari: Every time you see an explosion, like in the movies and stuff,[E|Happy] it looks really cool.")
	fnDialogue("Mari: [E|Sad] You don't get a sense of how destructive and dangerous they are until...[P]you know. [P]*gulp*")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_Rubble, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
elseif(sTopicName == "Grafitti") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Mari|Neutral][SHOWCHAR|Stacy|Neutral]Mari: [E|Shock]Isn't Michelle's grafitti cool?[P][E|Neutral] She wrote a bunch of cute sayings around the edges.")
    fnDialogue("Player: I can't read very well.")
    fnDialogue("Mari: [E|Sad][P][P][P]Ah, well.[P][E|Neutral] You'll get a kick out of them someday.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_Grafitti, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "DollLeg") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Mari|Neutral][SHOWCHAR|Stacy|Neutral]Mari: [E|Shock][CRASH]Hey, where'd you find that?[P] Looks like that one victorian doll I found--")
    fnDialogue("Stacy: [E|Shock][CRASH]LALALALA I CANT HEAR THIS!!!")
    fnDialogue("Player: You mean it's not yours?")
    fnDialogue("Mari: [E|Shock]Heck no![P][E|Neutral] I've got a Ouija Board,[P] a bat skeleton,[P] pentagrams,[P] [ITALICS]and[ENDITALICS] a Satan body pillow at home...")
    fnDialogue("Mari: [E|Mad]But I'd [CRASH]NEVER bring a victorian doll into my house.[P][E|Blush] Those things are way too creepy.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_DollLeg, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
elseif(sTopicName == "ColaBottle") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Mari|Neutral][SHOWCHAR|Stacy|Neutral][FOCUS|Stacy]Stacy:[E|Sad] I'm sorry about what happened to you this morning, [PLAYERNAME].")
    fnDialogue("Mari: [E|Mad]Yeah, the Kane Kola Cancel is not one of the finer traditions at Foster High.")
    fnDialogue("Player: I guess it's all right now.[P] My leg is dry.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_ColaBottle, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "Trees") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Mari|Neutral][SHOWCHAR|Stacy|Neutral]Mari: [E|Shock]You wouldn't believe how hard it is to find this place![P] There's no roads or landmarks leading you to it.")
    fnDialogue("Stacy: [E|Neutral]Mari and I had to memorize the way after we first discovered the place;")
    fnDialogue("Stacy: [E|Mad]These trees can hide some pretty big things in 'em.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_Trees, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
elseif(sTopicName == "Chairs") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Mari|Neutral][SHOWCHAR|Stacy|Neutral][FOCUS|Stacy]Stacy: [E|Neutral]Like the furniture?[P] We've been bringing stuff here for years to decorate the clubhouse.")
    fnDialogue("Stacy: [E|Blush]I carried a couple armchairs up all by myself last year.")
    fnDialogue("Mari: [E|Happy]*snicker*")
    fnDialogue("Stacy: [E|Mad]Of course,[P] then this weird moldy stuff started growing in them and I had to carry them back to a dumpster.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_Chairs, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
elseif(sTopicName == "Chimes") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Mari|Neutral][SHOWCHAR|Stacy|Neutral]Mari: [E|Neutral]Instead of dream catchers,[P] I put up wind chimes to keep away the bad spirits.")
    fnDialogue("Mari: [E|Shock] If I'm gonna fight demons, [CRASH]I'm not gonna appropriate someone's culture at the same time!")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_Chimes, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
elseif(sTopicName == "Footprints") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Mari|Neutral][SHOWCHAR|Stacy|Neutral]Player: Whose footprints are these?")
    fnDialogue("Stacy: [E|Shock]Not mine.[P][E|Blush] I've been told my personal gait is naturally very runway-ready")
	fnDialogue("Thought: She demonstrates, [P]exaggerating the swing of her hips.")
	fnDialogue("Player: I don't think I've ever seen you walk like that before.")
	fnDialogue("Stacy:[E|Mad][P][P][P][SFX|World|Ugh]Oh.")
    fnDialogue("Mari: [E|Neutral]These footprints look recent.[P] Maybe ask the other club members?")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_Footprints, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

elseif(sTopicName == "WalkieTalkie") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Mari|Neutral][SHOWCHAR|Stacy|Neutral]Mari: [E|Shock][CRASH]Hey!!![P] The old club walkie-talkies!")
    fnDialogue("Player: What is it?")
    fnDialogue("Stacy: [E|Neutral]It's kind of like a phone.[P] We gave one to Laura so she could communicate with us while she was at the library.")
    fnDialogue("Thought: Stacy presses the side button,[P] and we hear nothing but muffled rustling.")
    fnDialogue("Stacy: [E|Neutral]Hers is probably off right now.[P][E|Happy] Just as well--[P][P]we use cell phones now, like perfectly respectable teens!")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_WalkieTalkie, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
	
elseif(sTopicName == "Journal") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Mari|Neutral][SHOWCHAR|Stacy|Neutral]Mari: [E|Shock][CRASH]WHOAH, AN OLD JOURNAL!!!")
    fnDialogue("Player: Does it belong to anyone here?")
    fnDialogue("Stacy: [E|Neutral]I doubt it. We prefer to share our innermost secret thoughts on public, online blogs.")
    fnDialogue("Thought: Mari flips it open to a random page and begins to read.")
    fnDialogue("Mari: [E|Neutral]'It's extraordinary. Jumping from one homing site to the other, like a frog on a lily pad...'")
	fnDialogue("Mari: [E|Neutral]'... And there isn't any detectable energy being lost. [P]I'm sure the technology could be developed into a quartzified battery transmitter.'")
	fnDialogue("Stacy: [E|Neutral]Nice. sounds like someone was starting to write a cool science fiction novel.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_WalkieTalkie, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

--Person: Mari.
elseif(sTopicName == "Mari") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Mari|Neutral][SHOWCHAR|Stacy|Neutral]Mari: [E|Happy]Don't worry about me.[P] I'm excited to be here!")
    fnDialogue("Stacy: [E|Mad]Really? [P]Even after...[P]everything?")
	fnDialogue("Thought: Mari nods, all seriousness.[P] [ITALICS]Something tells me this isn't just about the explosion.[ENDITALICS]")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_Mari, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
	
--Person: Stacy.
elseif(sTopicName == "Stacy") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Mari|Neutral][SHOWCHAR|Stacy|Neutral]Stacy: I'm being extra vigilant for surprise clubhouse invaders.[P] What do you think about this Karate move?")
    fnDialogue("Stacy:[E|Mad][CRASH] HUAH [CRASH]HYAH [P][CRASH]HAAAAYAAAAAH!!!!!")
	fnDialogue("Mari: [E|Shock]Ooh, [E|Happy]that looks like it would be particularly effective against surprise clubhouse invaders!")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_Stacy, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
--Person: Bret.
elseif(sTopicName == "Bret") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Mari|Neutral][SHOWCHAR|Stacy|Neutral]Stacy: [E|Happy]Just look at Bret! He's real excited that there's an actual mystery to investigate.")
    fnDialogue("Mari: [E|Shock]What do you mean an [ITALICS]actual mystery?[ENDITALICS][P][CRASH] We're always investigating actual mysteries!!!")
	fnDialogue("Stacy: [E|Shock] R-right![P] I mean, I just meant this doesn't really have anything to do with ghosts or demons.")
	fnDialogue("Mari: [E|Neutral]THAT WE KNOW OF. [P]YET.")
	fnDialogue("Stacy: [E|Happy]Of course.")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_Bret, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

--Person: Michelle.
elseif(sTopicName == "Michelle") then
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][SHOWCHAR|Mari|Neutral][SHOWCHAR|Stacy|Neutral]Mari: [E|Sad]Michelle must be bummed about her grafitti. She made our clubhouse really shine with her decorations! ")
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(gciInvC_Michelle, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end

end
