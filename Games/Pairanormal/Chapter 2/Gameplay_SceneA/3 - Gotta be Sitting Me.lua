--[3 - Gotta be Sitting Me]
--Yep, The Thing jokes.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)
	
--Save the game.
if(PairanormalLevel_IsLoading() == false) then
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Starting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

--Set data.
else
	PairanormalLevel_FlagCheckpoint("Starting")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Gym")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Gym")
	fnDialogue([[ [Music|DaytimeRedux]Thought: ...]])
end

--[Execution]
--Choice A.
if(sTopicName == "MariStacy") then
    
    fnDialogue("Thought: [CHAREXIT|ALL]I decide to go sit with Mari and Stacy.[P] So begins my climb up through the packed bleachers.")
	fnDialogue("Player: 'scuse me,[P] sorry.")
	fnDialogue("Thought: Why is figuring out where to plant my butt one of the most anxiety-causing activities in school? ")
	fnDialogue("Player: Coming through,[P] pardon me.")
	fnDialogue("Thought: You'd think just sitting down wouldn't be such a big issue,[P] but there's a million things that can go wrong.")
	fnDialogue("Thought: Several of which,[P] at least in my head,[P] end up with my pants on the ground and everyone in the vicinity pointing and laughing.")
	fnDialogue("Player: Hey guys!")
	fnDialogue("[CHARENTER|Mari][CHARENTER|Stacy]Mari: [E|Happy][CRASH]Heeeeeey!!!!")
	fnDialogue("Thought: As soon as Mari and Stacy notice me,[P] they bring me in for a hug.")
	fnDialogue("Player: Can I sit with you?")
	fnDialogue("Stacy: [E|Happy][ITALICS]But of course![ENDITALICS]")
	fnDialogue("Mari: [CRASH][E|Mad]HECK YEAH!!")
	fnDialogue("Thought: The big grin on my face gradually fades as we all look around for an open seat.")
	fnDialogue("Thought: The bench is actually pretty packed.")
	fnDialogue("Mari: [E|Neutral]I've got an idea--")
	fnDialogue("Mari: [P][P][P][CRASH][E|Happy]SIT ON OUR LAP LIKE A BIG BABY!")
	fnDialogue("Player: [SFX|World|Blink]Wh-what?")
	fnDialogue("Stacy: [E|Happy]Come on![P] Hahaha!")
	fnDialogue("Thought: I feel two arms swoop under me.[P] I scream, partially in terror and delight.")
    fnUnlockGallery("Gallery_Scene_MariGym")
	fnDialogue("Stacy: [E|Neutral][IMAGE|Root/Images/MajorScenes/All/CH2_MariGym][CHAREXIT|All]We've got you![P][E|Blush] Hey, aren't I super strong?")
	fnDialogue("Mari:[E|Mad] [CRASH]Bench![P][CRASH] Press![P][CRASH] Bench![P][CRASH] Press![P]")
	fnDialogue("Thought: I can barely get out my protests, I'm laughing so hard.")
	fnDialogue("Thought: Mari and Stacy's laps are not comfortable seats by any stretch of the imagination,[P] but I'd happily stay there for the duration of the assembly.")
	fnDialogue("[CHARENTER|Teacher2]Teacher: [IMAGE|Null][CHARENTER|Mari][CHARENTER|Stacy]AH--[P][P]LADIES!!")
	fnDialogue("Thought: We all stop.[P] The teacher stands nearby,[P] tapping her foot impatiently.")
	fnDialogue("Teacher: Please put [PLAYERNAME] down.[P] That's very dangerous.")
	fnDialogue("Stacy: [E|Sad]Sorry, Mrs. Ramirez.")
	fnDialogue("Teacher: [PLAYERNAME], I'm sorry dear,[P] but you'll have to find another seat.[P] These benches are pretty packed.")
	fnDialogue("Thought: [ITALICS]Looks like the fun's over[ENDITALICS].[P] I give Mari and Stacy a solemn nod, and unmount.")
	fnDialogue("Mari: [E|Sad]Don't worry[IMAGE|Null], [PLAYERNAME]![P][E|Neutral] We'll see you after school!")
    
--Choice B.
elseif(sTopicName == "LauraBret") then
	fnDialogue("Thought: [CHAREXIT|ALL]I decide to go sit with Laura and Bret.[P] So begins my climb up through the packed bleachers.")
	fnDialogue("Player: 'scuse me,[P] sorry.")
	fnDialogue("Thought: Why is figuring out where to plant my butt one of the most anxiety-causing activities in school? ")
	fnDialogue("Player: Coming through,[P] pardon me.")
	fnDialogue("Thought: You'd think just sitting down wouldn't be such a big issue,[P] but there's a million things that can go wrong.")
	fnDialogue("Thought: Several of which,[P] at least in my head,[P] end up with my pants on the floor and everyone in the vicinity pointing and laughing.")
	fnDialogue("Player: Hey Bret!")
	fnDialogue("[CHARENTER|Bret]Thought: Bret turns to look at me.")
	fnDialogue("Bret: [E|Shock]H-hey![P] [PLAYERNAME]![P][E|Happy] You gonna sit with me?")
	fnDialogue("Player: Yep.[P] And Laura.")
	fnDialogue("Thought: We look to her a few rows back.[CHARENTER|Laura][P] She'd been watching my approach, and gives us a wave.")
	fnDialogue("Thought: We gesture for her to come join us.")
	fnDialogue("Laura: [E|Blush]A...[P] r... right! *ahem*[P][P] E...[P]excuse me.")
	fnDialogue("Thought: She stands and tries to stick her leg over the bench in front.")
	fnDialogue("Thought: Somewhere nearby, a student screams as her friend playfully tries to tickle her.")
	fnDialogue("Thought: Laura's hands fly to her ears, and she plops back down in her seat.")
	fnDialogue("Laura: [SFX|Ugh][E|Sad]I'm... s-sorry,[P] I don't think...[P] I...")
	fnDialogue("Player: What's that?")
	fnDialogue("Laura: [E|Mad]I c-cant![P][P][E|Blush] *ahem* I'm sorry,[P] I can't move from here.")
	fnDialogue("Player: Oh![P][P] Uh...[P]okay!")
	fnDialogue("Thought: I give Bret a confused look.[P] He smiles and shrugs.")
	fnDialogue("[CHAREXIT|Laura]Bret: [E|Blush]Try not to take it personally;[P][E|Neutral] Laura can't stand loud noises,[E|Mad] so when stuff like assemblies happen, it's best to just give her some space.")
	fnDialogue("Bret: [E|Happy] But hey, if you want to sit by me, I'm happy to share!")
	fnDialogue("Thought: I nod, but soon realize that there isn't much space next to Bret after all.")
	fnDialogue("Thought: He helpfully tries to pick up his stuffed backpack,[P] hugging it to his chest.")
	fnDialogue("Thought: I sit down.[P][IMAGE|Root/Images/MajorScenes/All/CH2_BretGym][CHAREXIT|Bret] It's uncomfortably packed.")
    fnUnlockGallery("Gallery_Scene_BretGym")
	fnDialogue("Player: I'm sorry![P] This was a bad idea.")
	fnDialogue("Bret: [E|Sad]Wh-no![P] I'm having a great time![P][E|Blush] I mean, not--not because you and I are...[P][E|Mad] What I mean to say is,[E|Blush] I don't mind being squished up against you--")
	fnDialogue("Bret: That is, [E|Shock]like if there was anyone else to be squished up against,[P] I would prefer--[P][E|Neutral]Okay look,[P] why don't we try--")
	fnDialogue("Player: It's okay,[P] you don't have to go out of your way for me, Bret.")
	fnDialogue("Player: Why don't I just see you at the club meeting this evening?")
	fnDialogue("Bret: [E|Sad] Aww, are you sure?[P] I mean... yeah,[P][E|Neutral] okay, you're probably right.")
	fnDialogue("Player: [IMAGE|Null]I get up and leave.[P] Laura wiggles her fingers at me sadly as I pass by.")

--Choice C.
elseif(sTopicName == "Michelle") then
	fnDialogue("Thought: [CHAREXIT|ALL]I decide to go sit with Michelle.[P] She looks like she could use the company.")
	fnDialogue("Player: 'scuse me,[P] sorry.")
	fnDialogue("Thought: Why is figuring out where to plant my butt one of the most anxiety-causing activities in school? ")
	fnDialogue("Player: Coming through,[P] pardon me.")
	fnDialogue("Thought: You'd think just sitting down wouldn't be such a big issue,[P] but there's a million things that can go wrong.")
	fnDialogue("Thought: Several of which,[P] at least in my head,[P] end up with my pants on the floor and everyone in the vicinity pointing and laughing.")
	fnDialogue("Player: Hey!")
	fnDialogue("Thought: I sidle up to her, but she doesn't look up.")
    fnUnlockGallery("Gallery_Scene_MichelleGym")
	fnDialogue("Thought: [IMAGE|Root/Images/MajorScenes/All/CH2_MichelleGym]Instead,[P] she's hunched over the benches with a thick black marker, carefully carving out interesting patterns and words.")
	fnDialogue("Thought: I can smell the ink fumes from where I'm standing.")
	fnDialogue("Michelle: Piss off, dweeb.[P] Can't you see these benches are occupied?")
	fnDialogue("Player: [SFX|Ugh]Oh, right.[P].[P].[P] I understand.[P] Cool drawings, though!")
	fnDialogue("Player: I-I'll see you at club later!")
	fnDialogue("Thought: I turn to leave, my ears hot.")
	fnDialogue("Thought: Out of the corner of my eye, I see her glance at me.")
	fnDialogue("Michelle: [E|Shock]Oh, wait, it's you![P][E|Mad] Hang on, I-I didn't mean--")
	fnDialogue("Thought: [IMAGE|Null]Whatever she meant to say is drowned out by the voices of chattering students who I pass by as I depart.")
    
--Choice E.
elseif(sTopicName == "Alone") then

--Level skip.
elseif(sTopicName == "Skip") then

    fnDialogue("Thought: (Skipping to which investigation to go to.)")

    --Decision Set.
    fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (Where should I be investigating?)") ]])

    local sDecisionScript = "\"" .. gsRoot .. "../Chapter 2/Gameplay_SceneB/0 - See You At The Club.lua" .. "\""
    fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"The Explosion at the Clubhouse\", " .. sDecisionScript .. ", \"Start\") ")

    sDecisionScript = "\"" .. gsRoot .. "../Chapter 2/Gameplay_SceneB/5 - A Body In the Library.lua" .. "\""
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"What's Keeping Laura in the Library\", " .. sDecisionScript .. ", \"Start\") ")
    fnCutsceneBlocker()
    return
end
	
--Save the game.
fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"AfterDecision\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
PairanormalLevel_FlagCheckpoint("AfterDecision")

--Dialogue rejoins here.
fnDialogue("[MUSIC|Somber][CHAREXIT|ALL][CHARENTER|Teacher3]Principal: [VOICE|Voice|Generic2][FOCUS|Teacher3]All right, all right everyone. Settle down.[SFX|World|Applause]")
fnDialogue("Thought: The din of the makeshift auditorium lowers ever so slightly.")
fnDialogue("Thought: After what feels like wandering for years in a desert and finding a muddy puddle of water...")
fnDialogue("Thought: I sink into an empty seat in the middlemost bench of bleachers set up by the shower rooms.")
fnDialogue("Principal: [VOICE|Voice|Generic2][FOCUS|Teacher3]Thank you for your attention, students.")
fnDialogue("Principal: [VOICE|Voice|Generic2][FOCUS|Teacher3]This assembly is primarily to make some announcements about the upcoming year.")
fnDialogue("Principal: [VOICE|Voice|Generic2][FOCUS|Teacher3]I know it's very early, [P]so we'll try and speed through the news.")
fnDialogue("Thought: Someone several benches down in front of me shouts 'Put coffee in the vending machines!'")
fnDialogue("Principal: [VOICE|Voice|Generic2][FOCUS|Teacher3]First,[P] a quick word about the upcoming soccer season.")
fnDialogue("Principal: [VOICE|Voice|Generic2][FOCUS|Teacher3]Tryouts will be on the following days for rookies...")
fnDialogue("Info: [SFX|World|Blink]*crinkle* [P][P][SFX|World|Blink]*crunch*")
fnDialogue("Principal: [VOICE|Voice|Generic2][FOCUS|Teacher3]... Returning players and cheerleaders will be reporting to the [ITALICS]gymnasium[ENDITALICS] and not the outer fields.")
fnDialogue("Principal: [VOICE|Voice|Generic2][FOCUS|Teacher3]I repeat,[P] this year,[P] tryouts will be in [ITALICS]this[ENDITALICS] very gym.")
fnDialogue("Info: [SFX|World|Ugh]*RRRRRRIIIPPP*")
fnDialogue("[CHAREXIT|ALL]Thought: I look to my right.")
fnDialogue("Thought: [CHARENTER|Ross]A student poorly attempts to open a large bag of chips that peeks out of their open backpack.")
fnDialogue("Thought: When they notice me looking,[P] they smile sheepishly.")
fnDialogue("Student: [Emotion|Ross|Blush][VOICE|Voice|Ross][ITALICS]Don't tell on me![ENDITALICS]")
fnDialogue("Player: Um,[P] okay.")
fnDialogue("Student: [VOICE|Voice|Ross][EMOTION|Ross|Blush].[P].[P].[P][Emotion|Ross|Neutral]Want some?")
fnDialogue("Thought: They offer me the bag.")
fnDialogue("Thought: The hole at the top is unfortunately too small for me to fit my hand in.[P][SFX|World|Ugh] I shrug helplessly.")
fnDialogue("Student: [VOICE|Voice|Ross][Emotion|Ross|Sad]Bummer,[P] dude.")
fnDialogue("[CHAREXIT|ALL][CHARENTER|Teacher3]Principal: [FOCUS|Teacher3][VOICE|Voice|Generic2]And now, [P]I'd like to hand the mic over to Mrs. Candace Foster,[P] one of the chairs of the school board.")
fnDialogue("Thought: [SFX|World|Applause][SFX|World|Applause][SFX|World|Applause]Students clap and cheer as a woman who was waiting at the gym entrance walks onto the court proper.")
fnDialogue("Thought: [CHARENTER|Candace][ITALICS]Awfully enthusiastic for a morning assembly.[ENDITALICS]")
fnDialogue("Thought: Funny...[GLITCH]that woman looks kind of familiar.[GLITCHTEXT]LOOKS KIND OF FAMILIAR[ENDGLITCHTEXT][CHAREXIT|ALL] I feel a hand reach across my chest, fingers wiggling wildly.")

--Save the game.
fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"GiveItHere\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
PairanormalLevel_FlagCheckpoint("GiveItHere")

fnDialogue("[CHARENTER|Terry]Student: [FOCUS|Terry][VOICE|Voice|Terry][EMOTION|Terry|Mad]Fucking quick![P] Give it here, Ross!")
fnDialogue("Thought: [CHARENTER|Ross][FOCUS|Ross]The student to my left gestures as Ross (who bears an uncanny resemblence) hands them the bag of chips.")
fnDialogue("Thought: I watch their hands quickly and ruthlessly rip it open under my nose,[P] the sound of the bag totally undetectable beneath the crowd's cheering.")
fnDialogue("Thought: A puff of cheese-flavored air hits my nostrils.")
fnDialogue("Ross: [EMOTION|Ross|Happy]Thanks, sis.[P] Hey dude, this is my sister Terry.")
fnDialogue("Terry: [E|Happy] You're welcome and nice to meet you, new kid.")
fnDialogue("Thought: The two eerily similar looking students dig their hands hungrily into the bag,[P] which somehow now rests in my lap.")
fnDialogue("[CHAREXIT|ALL][CHARENTER|Candace]Candace: [SFX|World|Applause][E|Neutral]Thank you so much, everyone.[P] Thank you all for your kindness.")
fnDialogue("Candace: Some of these announcements would have come much sooner this year,[P] but as many of you know[P], the Foster family was struck with a recent tragedy.")
fnDialogue("[CHAREXIT|ALL][CHARENTER|Student2][CHARENTER|Student6]Student: [VOICE|Voice|Generic1][FOCUS|Student2]Poor Mrs. Foster.")
fnDialogue("Student: [VOICE|Voice|Generic1][FOCUS|Student6]It must suck to lose a parent.")

--Save the game.
fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"LoseAParent\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
PairanormalLevel_FlagCheckpoint("LoseAParent")

fnDialogue("[HIDEALL][CHARENTER|Candace]Candace: [E|Sad]But, when the Foster family started Foster Oil & Ore in town, [E|Happy]My father made a commitment to the education of his workers and their children.")
fnDialogue("Thought: She gestures to all of us.")
fnDialogue("Candace: [E|Happy]And so [GLITCH]Foster High[GLITCH] was born.")
fnDialogue("Player: Um...[P][GLITCHTEXT]ANDSO F OSTER FALLS WAS B O R N[ENDGLITCHTEXT] c-[P]can I ask [CHAREXIT|ALL][CHARENTER|Ross][CHARENTER|Terry]you guys something?")
fnDialogue("Terry: [E|Sad]*crunch* [P]*crunch*[P] What is it?")
fnDialogue("Ross: [E|Neutral]Are you okay, dude?[P][P] If you want some chips,[P] feel free to dig in, bro.")
fnDialogue("Player: I'm fine.[P] Um,[BR] have the two of you noticed.[P].[P].[P] you look exactly the same?[P] Like, in the face area?")
fnDialogue("Thought: Terry and Ross look at each other.")
fnDialogue("Terry: [E|Shock]You're like,[P] joking, right?")
fnDialogue("Ross: [E|Shock]Whoaaaah sis, I don't think the new kid has ever [GLITCH]seen twins before.")
fnDialogue("[CHAREXIT|ALL][CHARENTER|Candace]Candace: [E|Neutral]He was working on this generous grant right up until his passing.")
fnDialogue("Candace: With it, the school has been able to begin installing a new computer lab,[P] which I'm pleased to announce will be opening after Winter Break.")
fnDialogue("[CHAREXIT|ALL][CHARENTER|Terry][CHARENTER|Ross]Terry: [E|Mad]So like,[P] the rumors are actually fucking [ITALICS]true[ENDITALICS]?[P] You're basically a teenage baby?")
fnDialogue("Ross: [E|Shock]Bro.[P] The new kid was in a coma.")
fnDialogue("Ross: [E|Sad] That's like... such a bummer, dude.")
fnDialogue("Terry: [E|Neutral]Oh, we're twins, by the way.[P] Some humans are born in pairs and they look exactly like each other.")
fnDialogue("Terry: [E|Shock] But sometimes they're not.")
fnDialogue("Ross: [E|Sad]... And [ITALICS]then[ENDITALICS] you have to like, wake up.[P] And you can't remember anything!")
fnDialogue("Ross: Just like a movie, dude,[P] except it's real.")
fnDialogue("Terry: [E|Blush]The ones that don't look like each other are called fraternal twins,[P] and the ones that do are called identical twins.")
fnDialogue("Terry: [E|Shock] There's other types, too,[P] but like,[P] it's way too fucking confusing to explain all at once.")
fnDialogue("Ross: [E|Shock]Do you like, dream in a coma?[P] Or is it just endless, meaningless black?")
fnDialogue("Terry: [E|Mad]Ross, shut the fuck up please.[P][E|Sad] You're gonna give me a fucking panic attack.")
fnDialogue("Ross: [E|Blush][P][P] Sorry.")
fnDialogue("[CHAREXIT|ALL][CHARENTER|Candace]Candace:[E|Happy] We've also put the money from my father's grant towards--")
fnDialogue("Thought:[P][P][P][MUSIC|Null] The woman and I meet eyes.")
fnDialogue("Candace: [P][P][P]T...[P][P][P]towards...")
fnDialogue("Player: .[P].[P].[P]")
fnDialogue("Candace: [E|Shock][P].[P].[P]I...")
fnDialogue("Thought: The whole building is strangely silent.")
fnDialogue("Terry: [ZOOM|150]What's she fucking waiting for?")
fnDialogue("Ross: [ZOOM|200]Is she looking at us, dude?")
fnDialogue("Terry: [ZOOM|500]Hey, new kid. A-are you okay?")
fnDialogue("[BG|Root/Images/Backgrounds/All/CandaceLivingRoom]Player: [GLITCH][GLITCHTEXT]WHY[GLITCH][GLITCH] DID YOU LIE WHY D[GLITCH]ID YOU L[GLITCH]IE WHY DID YOUUUUUUUUUU AAAAAAAAAAAA[GLITCH][BR]Sweetheart, when was the last time you[GLITCH][GLITCH]UUUUUUUUUGGGGGGHAAAAAAAAAAAAAAAAA[ENDGLITCHTEXT]AA[GLITCH]AAAAA")

--abrupt cut to clubhouse!
fnDialogue("[BG|Null][MUSIC|Null][CHAREXIT|ALL][ZOOM|100][BG|Root/Images/Backgrounds/All/Classroom]Info: [CRASH] *PAM* [P][CRASH] *PAM* [P][CRASH] *PAM*")
fnDialogue("[CHARENTER|Mari]Mari:[E|Neutral]Welcome everyone!")


--Save the game.
fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"AnotherMeeting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
PairanormalLevel_FlagCheckpoint("AnotherMeeting")


fnDialogue("Mari: [BG|Root/Images/Backgrounds/All/Classroom][MUSIC|DaytimeRedux][E|Happy]Welcome to another awesome Paranormal Club meeting!!!")
fnDialogue("[CHARENTER|Laura][CHARENTER|Bret][CHARENTER|Michelle][CHARENTER|Stacy]Info: [SFX|World|Applause] *applause*")
fnDialogue("Mari: [E|Neutral]Now, for today's agenda, we'll be going to...[P][E|Happy] [ITALICS]drumroll please![ENDITALICS]")
fnDialogue("Thought: Wait a minute, wait a minute. What happened?[P] Why am I here again?")
fnDialogue("Thought: Wasn't I... wasn't I just at the assembly?")
fnDialogue("Mari: [E|Shock][CRASH]THE CLUBHOUSE![P] We're going to the [CRASH][E|Happy]CLUBHOUSE BABEEEEEEEE!!!")
fnDialogue("Info: [SFX|World|Applause] *applause*")
fnDialogue("Bret: [E|Happy]It's been such a long time!")
fnDialogue("Michelle: [E|Happy]I can't wait to add some new marks to the walls.")
fnDialogue("Mari: [E|Sad]I'm sorry to say you can't paint on the walls anymore,[P] Michelle.[BR][E|Shock]... on account of them being [CRASH]EXPLODED!")
fnDialogue("Mari: [E|Shock][P].[P].[P].")
fnDialogue("Laura: [E|Shock][P].[P].[P].")
fnDialogue("Bret: [E|Shock][P].[P].[P].")
fnDialogue("Michelle: [E|Shock][P].[P].[P].")
fnDialogue("Player: [P][CRASH][ITALICS]Exploded[ENDITALICS]???")
fnDialogue("Mari: [E|Shock]THAT'S RIGHT!!")
fnDialogue("Mari: [E|Neutral] Since the last time we've been at the clubhouse, there's been a mysterious explosion!")
fnDialogue("Mari: [E|Happy] And I'm thinking, what better way to go on our first real mission [CRASH]THAN TO INVESTIGATE the scene of the crime?")
fnDialogue("Mari: [E|Mad][CRASH] WHAT CAUSED THE EXPLOSION? [P][P][CRASH]WHY DID IT HAPPEN?[P][P][E|Blush] And most importantly of all...")
fnDialogue("Thought: The club members lean in, hanging on Mari's every word.")
fnDialogue("Thought: Up until now, club 'missions' mostly involved heading down to Charlie's Pizza Parlor to investigate the mystery of")
fnDialogue("Thought: Why any slice of pizza never seemed to run out of grease, no matter how many times you licked it off or patted it dry with a napkin.")
fnDialogue("Mari: [E|Mad][CRASH]WHAT'S INSIDE TANK NUMBER TWO?!?!")
fnDialogue("Laura: [E|Shock]No way!")
fnDialogue("Michelle: [E|Mad]Tank number two is bloody [ITALICS]open?[ENDITALICS]")
fnDialogue("[CHAREXIT|ALL]Thought: I casually sidle up to the club's co-president.")
fnDialogue("[CHARENTER|Stacy]Player: Um... hey Stacy. [P]Wh-what's the 'clubhouse' everyone's talking about?")
fnDialogue("Stacy: [E|Neutral]Our clubhouse is built inside of this giant empty water tank out in the woods.")
fnDialogue("Stacy:[E|Mad]There's a second one right next to it,[P] but it's always been sealed up tight.")
fnDialogue("Thought: Am I imagining it, or is there the tiniest crinkle of concern on Stacy's brow?")
fnDialogue("[CHARENTER|Michelle][CHARENTER|Mari][CHARENTER|Laura][CHARENTER|Bret]Michelle: [E|Neutral]Whatever caused that explosion was strong enough to blow open reinforced steel!")
fnDialogue("Info: [SFX|World|Applause]*applause*")


--Save the game.
fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"AreWeReadyClubhouse\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
PairanormalLevel_FlagCheckpoint("AreWeReadyClubhouse")

fnDialogue("Stacy: [E|Happy]Well, are we all ready to head out there?")
fnDialogue("Laura: [P][P][P][E|Blush]Um, I can't go,[P] Mari.[P] *ahem* Sorry.[P] I'll hang out in the library.")
fnDialogue("Mari: [E|Shock]What???[P] But you've never had a problem coming to the clubhouse!")
fnDialogue("Stacy: [E|Blush]If you're worried about your safety Laura,[P] don't be![BR][E|Mad] I'll protect you.")
fnDialogue("Laura: [E|Mad]Um, no,[P] i-[P]it's not that.[P][E|Blush] I have...[P] er, a prior engagement.")
fnDialogue("Thought: Laura had missed out on a couple of trips to the pizza parlor as well,[P] even though everyone else assured me...")
fnDialogue("Thought: She never had a problem going before.[P] I study her as she twirls a lock of hair in her fingers bashfully.")
fnDialogue("Thought: Was there another reason she wasn't hanging out with the club? [P][ITALICS]Was it...[P] me?[ENDITALICS]")
fnDialogue("Laura:[E|Blush] I have...[P] well, it's just that...")
fnDialogue("Laura: [SFX|World|Blink][E|Shock] Oh! [P] Uh, Todd and Harper are setting up for the new computer lab!")
fnDialogue("Laura: [E|Blush] Yes, that's right. [P] I h-have to go...[P]S-see how *hrm*[P] h-how it's going!")
fnDialogue("Thought: I feel a prickle behind my ears.")
fnDialogue("Player: [ITALICS]Todd?[ENDITALICs][P] Todd as in my tutor Todd is going to be there?")
fnDialogue("Stacy: [E|Neutral]Yeah, Todd and Harper are the school's ASB club reps.[P] They're usually hanging around a lot of official-type school happenings.")
fnDialogue("Michelle: [E|Blush] What's got you so interested in Todd, anyway?")
fnDialogue("Player: No reason.[P] Nothing! [P][ITALICS]I'm normal[ENDITALICS].")
fnDialogue("Mari: [E|Neutral] Well all right, fine--[P]Laura's going to the library.[P] Anyone else coming to the clubhouse?")
fnDialogue("Michelle: [E|Happy]ME!")
fnDialogue("Bret: [E|Happy]Definitely me!")
fnDialogue("Stacy: [E|Happy][ITALICS]Moi aussi, naturalment[ENDITALICS].")


--Save the game.
fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"WhatDo\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
PairanormalLevel_FlagCheckpoint("WhatDo")

fnDialogue("Mari: [E|Neutral]How 'bout you, [PLAYERNAME]?")

--Decision Set.
fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (Where should I be investigating?)") ]])

local sDecisionScript = "\"" .. gsRoot .. "../Chapter 2/Gameplay_SceneB/0 - See You At The Club.lua" .. "\""
fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"The Explosion at the Clubhouse\", " .. sDecisionScript .. ", \"Start\") ")

sDecisionScript = "\"" .. gsRoot .. "../Chapter 2/Gameplay_SceneB/5 - A Body In the Library.lua" .. "\""
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"What's Keeping Laura in the Library\", " .. sDecisionScript .. ", \"Start\") ")
fnCutsceneBlocker()
