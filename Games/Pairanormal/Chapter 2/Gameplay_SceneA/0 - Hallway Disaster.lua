--[Exec First]
--Executes when the program starts. This will occur with the argument "Start".

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--[Execution]
--Start.
if(sTopicName == "Start") then
    
    --Loading.
    VM_SetVar("Root/Variables/System/Loading/iLoadChapter2", "N", "1.0")
    LM_ExecuteScript(gsRoot .. "../Chapter 2/System/000 Global Variables.lua")
    LM_ExecuteScript(gsRoot .. "../Chapter 2/System/100 Chapter Variables.lua")
    LM_ExecuteScript(gsRoot .. "../Chapter 2/System/200 Dialogue Actors.lua")
        
    --Dialogue.
    fnUnlockGallery("Gallery_Hallway")
	--Save the game.
fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"HallwayDisaster\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
PairanormalLevel_FlagCheckpoint("HallwayDisaster")

	fnDialogue("[MUSIC|DaytimeRedux][BG|Root/Images/Backgrounds/All/Gym]Thought: I lean,[P] or rather am forced up against the wall of Foster High's gymnasium to make way for the oncoming crowds.")
	fnDialogue("Thought: Everyone files into bleachers and hastily set up rows of metal chairs that run alongside the court.")
	fnDialogue("Player: Another school assembly.[P] Didn't we just have one of these?")
	fnDialogue("Thought: Those lucky enough to have made it early had already taken the highly coveted edge seats,")
	fnDialogue("Thought: So the new arrivals search reluctantly for a spot in the middle of the back rows.")
	fnDialogue("Player: Ugh... I do [ITALICS]not[ENDITALICS] have the mental energy for this right now.")
	fnDialogue("Thought: Earlier this morning, I was proudly sipping my most recent accomplishment--[P]a soda that I'd figured out how to buy from a vending machine.")
	fnDialogue("Thought: Just as I was raising it to my lips, a crowd of students screeched at me.[P] I was so startled I dropped the drink on myself.")
	fnDialogue("Thought: I grimace, feeling the still-slightly-damp pant stain brush against my calves.")
	fnDialogue("Thought: Every time it touches me, I'm reminded of how everyone scattered in a fit of giggles while I stood there, soda slowly spreading over my leg.")
	fnDialogue("Thought: And what did these so-called teachers say when I asked for guidance?")
	fnDialogue("Player: [ITALICS]That's called a Kane Kola Cancel.[P] So sorry it happened, now hurry along![ENDITALICS]")
	fnDialogue("Thought:[SFX|World|Ugh]...[P]Better hurry up and find a seat, this assembly is starting.")
	fnDialogue("Thought: I scan the crowd looking for a familiar face.")
	fnDialogue("Player: Oh, there's Todd! I[P][P][P]--Oh,[P] it looks like some other people are sitting with him. ")
	fnDialogue("Thought: [ITALICS]He's probably busy and has no interest in talking to me whatsoever.[P][SFX|World|Blink] Oh crap--did he see me?[ENDITALICS]")
	fnDialogue("Thought: [ITALICS]I'm going to look away and pretend I didn't notice.[P] And laugh. [P][P][SFX|World|Crash] LAUGH!!![P] Like I'm having a great time.[ENDITALICS]")
	fnDialogue("Player: Except I'm alone.[P][P][SFX|World|Ugh][ITALICS] Now it just looks weird.[ENDITALICS]")
	fnDialogue("Thought: One by one I spot Paranormal Club members peppered throughout the gymnasium.")
	fnDialogue("[SHOWCHAR|Mari|Neutral][SHOWCHAR|Stacy|Neutral][FOCUSMOD|Mari|True][FOCUSMOD|Stacy|True] Thought:  Mari and Stacy are seated together in some middle rows.")
	fnDialogue("[HIDEALL][SHOWCHAR|Bret|Neutral][SHOWCHAR|Laura|Neutral][FOCUSMOD|Laura|True][FOCUSMOD|Bret|True] Thought:  Laura and Bret are on the other side,[P] seated near each other,[P] but not speaking.")
	fnDialogue("[HIDEALL][SHOWCHAR|Michelle|Neutral][Focus|Michelle] Thought: Michelle is in one of the back seats.[P] There's a couple of open spots next to her,[P] but nobody seems to be taking them.")
	
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (Who should I sit next to?)") ]])

	local sDecisionScript = "\"" .. gsRoot .. "../Chapter 2/Gameplay_SceneA/3 - Gotta be Sitting Me.lua" .. "\""
	fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Mari and Stacy\", " .. sDecisionScript .. ", \"MariStacy\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Laura and Bret\",  " .. sDecisionScript .. ", \"LauraBret\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Michelle\",  " .. sDecisionScript .. ", \"Michelle\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Sit Alone\",  " .. sDecisionScript .. ", \"Alone\") ")
	fnCutsceneBlocker()
    
--Lets you jump ahead!
elseif(sTopicName == "LevelSkipBoot") then
    
    --Loading.
    VM_SetVar("Root/Variables/System/Loading/iLoadChapter2", "N", "1.0")
    LM_ExecuteScript(gsRoot .. "../Chapter 2/System/000 Global Variables.lua")
    LM_ExecuteScript(gsRoot .. "../Chapter 2/System/100 Chapter Variables.lua")
    LM_ExecuteScript(gsRoot .. "../Chapter 2/System/200 Dialogue Actors.lua")
	
	--Level Skip.
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "(Want to skip ahead?)") ]])

	local sDecisionScript = "\"" .. gsRoot .. "../Chapter 2/Gameplay_SceneA/0 - Hallway Disaster.lua\""
	fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Run First Scene\",      " .. sDecisionScript .. ", \"Start\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Assembly\",             " .. sDecisionScript .. ", \"Assembly\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Choose Investigation\", " .. sDecisionScript .. ", \"ChooseInv\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Go Home\", "              .. sDecisionScript .. ", \"GoHome\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Credits\",              " .. sDecisionScript .. ", \"Credits\") ")
	fnCutsceneBlocker()

--Level skip options.
elseif(sTopicName == "Assembly") then
	LM_ExecuteScript(gsRoot .. "../Chapter 2/Gameplay_SceneA/2 - To Harper I Say.lua", "Skip")
	
elseif(sTopicName == "ChooseInv") then
	LM_ExecuteScript(gsRoot .. "../Chapter 2/Gameplay_SceneA/3 - Gotta be Sitting Me.lua", "Skip")
	
elseif(sTopicName == "GoHome") then
	LM_ExecuteScript(gsRoot .. "../Chapter 2/Gameplay_SceneC/0 - Home Time.lua", "Skip")
	
elseif(sTopicName == "Credits") then
	LM_ExecuteScript(gsRoot .. "../Chapter 2/Gameplay_SceneC/2 - Credits.lua", "Skip")

end
