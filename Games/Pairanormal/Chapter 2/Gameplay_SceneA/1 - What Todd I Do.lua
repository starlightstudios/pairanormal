--[1 - What Todd I Do]
--A level of pun not seen since the late medieval ages.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)
	
--Save the game.
if(PairanormalLevel_IsLoading() == false) then
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Starting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

--Set data.
else
	PairanormalLevel_FlagCheckpoint("Starting")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Hallway")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Hallway")
	fnDialogue([[ [Music|DaytimeRedux][SHOWCHAR|Todd|Neutral][SHOWCHAR|Harper|Neutral]Thought: [Focus|Todd]... ]])
end

--[Execution]
--Todd, you're my only friend.
if(sTopicName == "Greet") then
    VM_SetVar("Root/Variables/ChapterVars/Chapter2/iGreetedToddWarmly", "N", 1.0)
	fnDialogue("Thought: Thought: I smile.")
	fnDialogue("Player: Thanks for coming to my rescue, Todd. [P]I didn't know you were the vice president of something.")
	fnDialogue("Harper: !!!")
	fnDialogue("Harper: [E|Happy]And [P]-I- didn't know you had already both been formally introduced!")
	fnDialogue("Thought: Harper kicks Todd again,[P] still playfully, but hard enough to knock him into my legs.")
	fnDialogue("Thought: I feel a strange tingle on my ankles as we detangle from each other.")
	fnDialogue("Todd: [E|Happy]Pf'hahaha![P] Sorry, Harper![P][E|Blush] I should have mentioned it before--")
	fnDialogue("Player: T-Todd is tutoring me.")
	fnDialogue("Thought: At this,[P] Harper stops laughing.")
	fnDialogue("Harper: [E|Shock]Oh, re-[P]ally?[P] As in, [P][E|Blush][ITALICS]private[ENDITALICS] tutoring?")
	fnDialogue("Player: Yes.[P] I'm...[P][P] because of my situation, I'm not very good at schoolwork.")
	fnDialogue("Thought: Todd stands up, readjusting his glasses.[P] We look at each other reverently.")
	fnDialogue("Todd: [E|Neutral]But you're a fast learner, [PLAYERNAME].")
	fnDialogue("Todd: Just because you have a lot to learn doesn't mean you aren't as smart as anyone else here.")
	fnDialogue("Player: Th-[P][P]thanks.[P] And thanks for not blabbing about it.")
	fnDialogue("Thought: Harper watches us quietly,[P] until some of the students who he had sent out earlier start returning with teachers,[P] janitors,[P] and paper towels.")
	fnDialogue("Harper: [E|Neutral]Well,[P][E|Happy] I'm glad to finally meet you [PLAYERNAME]!")
	fnDialogue("Harper: Todd, head back to class.[P] [E|Neutral]I'll escort the new kid to the nurse's office while this whole me-[P]sssss gets cleaned up.")
	fnDialogue("Thought: With just the kind of authority I'd expect from an Associated Student Body President,")
	fnDialogue("Thought: Harper slings his arm around my shoulder and drags me down the hallways,[P] away from Todd.")
	fnDialogue("[HIDEALL]Thought: We walk for a while in silence.[P] Harper never takes his arm off my shoulder, making sure I stay closer to the edges of the halls,")
	fnDialogue("Thought: Maybe so as not to leave a sticky trail in high-traffic areas.")
	fnDialogue("[SHOWCHAR|Harper|Neutral]Harper: So.[P].[P].[P]you and Todd?[P] Private tu-[P]to-[P]ring.")
	fnDialogue("Harper: [E|Happy] Veeeeeery interesting.")
	fnDialogue("Player: Yeah?")
	fnDialogue("Harper: [E|Blush]As the ASB President,[P] allow me to impart a few friendly lessons myself.")
    
--Always alienate allies!
elseif(sTopicName == "ActDumb") then
	fnDialogue("Player: Hello, Todd.[P] Nice to meet you.")
	fnDialogue("Harper: [E|Shock]Omigosh,[P] Have you guys met before?[P][E|Blush] I couldn't help but notice how friend-[P]ly you are together!")
	fnDialogue("Player: I've seen him around the hallways.[P] He seems like a really friendly guy.")
	fnDialogue("Todd: [E|Shock] Oh,[P] uh yeah![P] That's me.[BR][E|Sad] Welcome to Foster Falls,[P] [PLAYERNAME].")
	fnDialogue("Player: Thanks, Todd.")
	fnDialogue("Thought: Harper raises an eyebrow.")
	fnDialogue("Thought: Some of the students who he'd sent out earlier start returning with teachers,[P] janitors,[P] and paper towels.")
	fnDialogue("Harper: [E|Neutral] Weeeeell, We're glad to finally meet you [PLAYERNAME]!")
	fnDialogue("Harper: [E|Happy] Todd,[P] head back to class.")
	fnDialogue("Harper: I'll escort the new kid to the nurse's office while this whole me-[P]sssss gets cleaned up.")
	fnDialogue("Thought: With just the kind of authority I'd expect from an Associated Student Body President,")
	fnDialogue("Thought: Harper slings his arm around my shoulder and drags me down the hallways,[P] away from a waving Todd.")
	fnDialogue("[HIDEALL]Thought: We walk for a while in silence.")
	fnDialogue("Thought: Harper never takes his arm off my shoulder,[P] making sure I stay closer to the edges of the halls,")
	fnDialogue("Thought: Maybe so as not to leave a sticky trail in high traffic areas.")
	fnDialogue("[SHOWCHAR|Harper|Neutral]Harper: [E|Sad]*siiiiigh*.")
	fnDialogue("Harper: [ITALICS]For your in-fo[ENDITALICS],[P] [E|Mad]I can't stand awkward silence.[P] So.[P][E|Happy] Chat it up and tell me about your life, new kid.")
	fnDialogue("Player: .[P].[P].[P]I would have thought everyone's heard the rumors by now.")
	fnDialogue("Thought: Harper laughs.")
	fnDialogue("Harper: [E|Neutral]Omigosh, Yeah.[P][E|Happy] Thanks by the way,[P] for bringing some [E|Mad][ITALICS]act-[P]u-[P]al[ENDITALICS] juicy gossip to these storied halls.")
	fnDialogue("Harper: [E|Neutral] So, you've never actually gone to school?")
	fnDialogue("Thought: No,[P] I haven't.")
	fnDialogue("Harper: [E|Neutral]Never known the stress of finals?[P][P] Never missed a bus?")
	fnDialogue("Thought: He gives me a sidelong glance.")
	fnDialogue("Harper: [E|Blush]Never had a crush?")
	fnDialogue("Player: Check, [P]check,[P] and check.")
	fnDialogue("Harper: [E|Mad]Oooooomigosh.[P][P] Oh-[P]mi-[P][E|Happy]gosh!")
	fnDialogue("Harper: [E|Neutral] Well honey, allow me to impart a few friendly lessons in high school life,[P] courtesy of the ASB.")
    
end
	
--Save the game.
fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"AfterDecision\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
PairanormalLevel_FlagCheckpoint("AfterDecision")

--Dialogue rejoins here.
fnDialogue("Thought: We stop.")
fnDialogue("Harper: [E|Sad]Lesson number 1 -- ")
fnDialogue("Harper: If you want to avoid another embarrassment like [ITALICS]this[ENDITALICS] again...")
fnDialogue("Thought: He gestures to my soda-covered leg.")
fnDialogue("Harper: [E|Mad]NE-[P]VER[P] drink Kane Cola from the vending machines.")
fnDialogue("Player: H-how did you know?")
fnDialogue("Harper: [E|Mad]Ooh, honey.[P] It's an oooooold trick passed down from \\ngen[P]-to[P]-gen here at Foster High.")
fnDialogue("Harper: [E|Neutral]The squirrels in the hills are obsessed with the stuff.")
fnDialogue("Harper: [E|Shock][ITALICS]Appa-rent-ly[ENDITALICS], they practically attacked a kid walking around with an open bottle once, a long time ago.")
fnDialogue("Harper: [E|Neutral]So,[P] unofficially,[P] like,[P] you're not really s'posed to be drinking it.")
fnDialogue("Harper: [E|Mad]Even though nobody's bothered to act-[P]u-[P]a-[P]lly [ITALICS]take it out[ENDITALICS] of the vending machines.")
fnDialogue("Player: Seems like a job for ASB.")
fnDialogue("Harper: [E|Shock]Hey!")
fnDialogue("Thought: He gives me a playful jab in the ribs.")
fnDialogue("Harper: [E|Happy]for your [ITALICS]in-[P]fo[ENDITALICS],[P] Everybody already knows not to touch the stuff.[P][P][E|Sad] Except for the new kid, I guess.")
fnDialogue("Harper: He slings his arm back around me,[P] and we resume course to the Nurse's office.")
fnDialogue("Harper: [E|Neutral]Be-cause,[P] if you open up a bottle, every student thinks it's absolutely necessary to scream at you until you drop it.")
fnDialogue("Harper: Or worse, slam it from your hands.[P][E|Shock] You've just experienced a traditional case of the Kane Kola Cancel.")
fnDialogue("Player: That's what that was?")
fnDialogue("Player: Everyone just cruelly screamed at me and made me embarrass myself because.[P].[P].[P] [CRASH]because of some weird,[P] pointless tradition?")
fnDialogue("Harper: [E|Happy]New kid,[P] you've just summarized the en-[P]tire highschool experience in a sentence.[P][SFX|Ugh] Bravo.")
fnDialogue("[HIDEALL][IMAGE|Root/Images/MajorScenes/All/CH2_NurseOffice]Thought: We arrive at the nurse's office.")
fnDialogue("Thought: She's busy writing something down,[P] but after spotting my leg, gestures for me to sit.")
fnDialogue("Thought: Harper slides me into a chair,[P] releasing me from his grip at last.")
fnDialogue("[SHOWCHAR|Harper|Neutral]Harper: [E|Neutral]... And lesson number two,[P] do yourself a favor and try not to get[P][P][P][E|Sad] [ITALICS]attached[ENDITALICS] to Todd.")
fnDialogue("Player: [SFX|World|Blink]Wh.[P].[P].Huh?")
if(sTopicName == "Greet") then
    fnDialogue("Harper: [E|Neutral]If you're spending lots of alone time with him,[P] you might make the mistake of thinking the two of you are closer than you actually are.")
    fnDialogue("Harper: [E|Happy] The truth is,[P] he's friendly with everyone.")
else
    fnDialogue("Harper: [E|Neutral]Todd is really friendly.[P] And handsome.[P] Just about everyone he meets likes him.")
    fnDialogue("Harper: [E|Sad] And some of those people like him a little too much.")
end
	
--Save the game.
fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Warning\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
PairanormalLevel_FlagCheckpoint("Warning")

fnDialogue("Thought: He opens the door to the nurse's office,[P] getting ready to excuse himself.")
fnDialogue("Harper: [E|Mad]So unless you're willing to share one of the most popular boys in school with [ITALICS]them[ENDITALICS],[P][E|Neutral] you should probably quit making [ITALICS]doe-[P]eyes[ENDITALICS] at him all the time.")

--Decision Set:
fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (What do I say to this?)") ]])

local sDecisionScript = "\"" .. gsRoot .. "../Chapter 2/Gameplay_SceneA/2 - To Harper I Say.lua" .. "\""
fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Sounds like you're jealous\", " .. sDecisionScript .. ", \"Jealous\") ")
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"You've got it all wrong\",  " .. sDecisionScript .. ", \"Wrong\") ")
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Say nothing\",  " .. sDecisionScript .. ", \"Gordon\") ")
fnCutsceneBlocker()
