--[2 - To Harper I Say]
--So I says to Harper I says - 

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)
	
--Save the game.
if(PairanormalLevel_IsLoading() == false) then
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Starting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

--Set data.
else
	PairanormalLevel_FlagCheckpoint("Starting")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Hallway")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Hallway")
	fnDialogue([[ [Music|DaytimeRedux][SHOWCHAR|Harper|Neutral]Thought: [Focus|Harper]... ]])
end

--[Execution]
--I AIN'T SHARIN'.
if(sTopicName == "Jealous") then
	fnDialogue("Player: That kind of sounds like a threat.[P] You're not actually.[P].[P].[P] jealous?")
	fnDialogue("Thought: Harper laughs.")
	fnDialogue("Harper: [E|Happy]Omigosh,[P] no-[P]o-[P]o.[P][E|Neutral] I promise you, I'm not.")
	fnDialogue("Harper: Just...[P][E|Sad] sharing something I happen to know from personal experience.")

--Todd is not my waifu.
elseif(sTopicName == "Wrong") then
	fnDialogue("Player: You've got it all wrong![P] I d-dont.[P].[P].[P] It's not like that.")
	fnDialogue("Thought: Harper shrugs.")
	fnDialogue("Harper: [E|Neutral]If you say so.[P] Just keep it in mind.")

--Jay of few words.
elseif(sTopicName == "Gordon") then
	fnDialogue("Thought: I don't say anything.[P] Instead, I watch Harper as he shrugs and exits.")
	fnDialogue("Thought: Is it just me, or does he look a little sad?")

--Level skip.
elseif(sTopicName == "Skip") then
	fnDialogue("Thought: (Level skip to who you sit with at the assembly.)")

    --Decision Set.
    fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (Who should I sit next to?)") ]])

    local sDecisionScript = "\"" .. gsRoot .. "../Chapter 2/Gameplay_SceneA/3 - Gotta be Sitting Me.lua" .. "\""
    fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Mari and Stacy\", " .. sDecisionScript .. ", \"MariStacy\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Laura and Bret\",  " .. sDecisionScript .. ", \"LauraBret\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Michelle\",  " .. sDecisionScript .. ", \"Michelle\") ")
    --fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Clint Eastwood\",  " .. sDecisionScript .. ", \"FistfulOfDollars\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Sit Alone\",  " .. sDecisionScript .. ", \"Alone\") ")
    fnCutsceneBlocker()
    return
end
	
--Save the game.
fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"AfterDecision\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
PairanormalLevel_FlagCheckpoint("AfterDecision")

--Dialogue rejoins here.
fnUnlockGallery("Gallery_Gym")
fnDialogue("[HIDEALL][IMAGE|Null][BG|Null][MUSIC|Null]Thought: ...")
fnDialogue("[BG|Root/Images/Backgrounds/All/Gym][Music|Somber]Thought: I lean,[P] or rather am forced up against the wall of Foster High's gymnasium to make way for the oncoming crowds.")
fnDialogue("Thought: Everyone files into bleachers and hastily set up rows of metal chairs that run alongside the court.")
fnDialogue("Player: Another school assembly.[P] Didn't we just have one of these?")
fnDialogue("Thought: Those lucky enough to have made it early had already taken the highly coveted edge seats.")
fnDialogue("Thought: So the new arrivals search reluctantly for a spot in the middle of the back rows.")
fnDialogue("Thought: I scan the crowd looking for a familiar face.[P] One by one I spot Paranormal Club members peppered throughout the gymnasium.")
fnDialogue("Thought: [CHARENTER|Mari][CHARENTER|Stacy][FOCUSMOD|Mari|true][FOCUSMOD|Stacy|true]Mari and Stacy are seated together in some middle rows.")
fnDialogue("Thought: [CHAREXIT|ALL][CHARENTER|Laura][CHARENTER|Bret][FOCUSMOD|Laura|true][FOCUSMOD|Bret|true]Laura and Bret are on the other side,[P] seated near each other,[P] but not speaking.")
fnDialogue("Thought: [CHAREXIT|ALL][CHARENTER|Michelle]Michelle is in one of the back seats.[P] There's a couple of open spots next to her,[P] which to my surprise,[P] nobody is taking.")

--Decision Set.
fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (Who should I sit next to?)") ]])

local sDecisionScript = "\"" .. gsRoot .. "../Chapter 2/Gameplay_SceneA/3 - Gotta be Sitting Me.lua" .. "\""
fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Mari and Stacy\", " .. sDecisionScript .. ", \"MariStacy\") ")
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Laura and Bret\",  " .. sDecisionScript .. ", \"LauraBret\") ")
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Michelle\",  " .. sDecisionScript .. ", \"Michelle\") ")
--fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Clint Eastwood\",  " .. sDecisionScript .. ", \"FistfulOfDollars\") ")
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Sit Alone\",  " .. sDecisionScript .. ", \"Alone\") ")
fnCutsceneBlocker()
