--[Chapter Variables]
--Variables that apply only to this chapter, or are used primarily in it.
DL_AddPath("Root/Variables/Chapter2/")

--[Miscellaneous]
DL_AddPath("Root/Variables/Chapter2/Misc/")

--[Common Investigation]
DL_AddPath("Root/Variables/Chapter2/Investigation/")
if(PairanormalLevel_IsLoading() == false) then
    VM_SetVar("Root/Variables/Chapter2/Investigation/iTimeMax", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter2/Investigation/iTimeLeft", "N", 0.0)
end

--[First Investigation]
--This refers to the clubhouse investigation.
DL_AddPath("Root/Variables/Chapter2/InvestigationA/")
if(PairanormalLevel_IsLoading() == false) then
    VM_SetVar("Root/Variables/Chapter2/InvestigationA/iChattedWithMari", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter2/InvestigationA/iChattedWithStacy", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter2/InvestigationA/iChattedWithBret", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter2/InvestigationA/iChattedWithMichelle", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter2/InvestigationA/iChattedWithLaura", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter2/InvestigationA/iShowedBretBottleCap", "N", 0.0)
end

--Name Lookups
gciInvC_TankA = 1
gciInvC_TankB = 2
gciInvC_ColaBottle = 3
gciInvC_Rubble = 4
gciInvC_Grafitti = 5
gciInvC_DollLeg = 6
gciInvC_Trees = 7
gciInvC_Chairs = 8
gciInvC_Chimes = 9
gciInvC_Footprints = 10
gciInvC_WalkieTalkie = 11
gciInvC_Journal = 12
gciInvC_Total = 13
gciInvC_Mari = 13
gciInvC_Stacy = 14
gciInvC_Bret = 15
gciInvC_Michelle = 16

--Position Data.
gczaObjC = {}
--              [NAME]          [X]   [Y]   [W]   [H]
gczaObjC[ 1] = {"Tank A",       767,  535,  294,  555}
gczaObjC[ 2] = {"Tank B",      1535,  408,  361,  326}
gczaObjC[ 3] = {"ColaBottle",   329, 1314,  127,  107}
gczaObjC[ 4] = {"Rubble",      1568,  760,  502,  516}
gczaObjC[ 5] = {"Grafitti",    1103,  431,  266,  554}
gczaObjC[ 6] = {"DollLeg",     2251, 1271,  141,  122}
gczaObjC[ 7] = {"Trees",         53,  332,  429,  781}
gczaObjC[ 8] = {"Chairs",       518, 1088,  356,  398}
gczaObjC[ 9] = {"Chimes",      2270,  484,  154,  276}
gczaObjC[10] = {"Footprints",   874, 1329,  820,  181}
gczaObjC[11] = {"WalkieTalkie",   0,    0,    1,    1}
gczaObjC[12] = {"Journal",        0,    0,    1,    1}
local iHighestIndex = 12

--Construct script variables.
for i = 1, gciInvC_Total - 1, 1 do
    
    --Script variable.
    if(PairanormalLevel_IsLoading() == false) then
        VM_SetVar("Root/Variables/Chapter2/InvestigationA/iObject|" .. gczaObjC[i][1], "N", 0.0)
    end
    
    --Object string
    local sObjectString = string.format("%02i", i)
    
    --Initialize the object presentation arrays.
    for p = 1, iHighestIndex, 1 do
        
        --Sub string
        local sSubString = string.format("%02i", p)
        
        --Variable creation.
        if(PairanormalLevel_IsLoading() == false) then
            VM_SetVar("Root/Variables/Chapter2/InvestigationA/iObjectPresent|" .. sObjectString .. sSubString, "N", 0.0)
        end
    end
end

--[Second Investgiation]
--Refers to the library investigation.
DL_AddPath("Root/Variables/Chapter2/InvestigationB/")
if(PairanormalLevel_IsLoading() == false) then
    VM_SetVar("Root/Variables/Chapter2/InvestigationB/iChattedWithLaura", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter2/InvestigationB/iChattedWithTodd", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter2/InvestigationB/iChattedWithRoss", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter2/InvestigationB/iChattedWithTerry", "N", 0.0)
end

--Name Lookups
gciInvD_Romance = 1
gciInvD_Alien = 2
gciInvD_Dandee = 3
gciInvD_Computer = 4
gciInvD_Paranormal = 5
gciInvD_OldComputer = 6
gciInvD_Checkout = 7
gciInvD_TV = 8
gciInvD_School = 9
gciInvD_Library = 10
gciInvD_Security = 11
gciInvD_Total = 12
gciInvD_Laura = 12
gciInvD_Todd = 13
gciInvD_Ross = 14
gciInvD_Terry = 15

--Position Data.
gczaObjD = {}
--              [NAME]          [X]   [Y]   [W]   [H]
gczaObjD[ 1] = {"Romance",     1834, 1064, 232,  114}
gczaObjD[ 2] = {"Alien",       1472, 1054, 186,  126}
gczaObjD[ 3] = {"DandDee",        0,    0,   1,    1}
gczaObjD[ 4] = {"Computer",     350,  335, 442,  530}
gczaObjD[ 5] = {"Paranormal",  1965,  703, 159,  214}
gczaObjD[ 6] = {"OldComputer", 1091,  746, 544,  245}
gczaObjD[ 7] = {"Checkout",      49,  893, 578,  298}
gczaObjD[ 8] = {"TV",          1941,  366, 283,  246}
gczaObjD[ 9] = {"School",       657, 1110, 237,  332}
gczaObjD[10] = {"Library",     1390,  528, 240,  208}
gczaObjD[11] = {"Security",    1596,  408, 132,  147}

--Construct script variables.
for i = 1, gciInvD_Total - 1, 1 do
    
    --Script variable.
    if(PairanormalLevel_IsLoading() == false) then
        VM_SetVar("Root/Variables/Chapter2/InvestigationB/iObject|" .. gczaObjD[i][1], "N", 0.0)
    end
    
    --Object string
    local sObjectString = string.format("%02i", i)
    
    --Initialize the object presentation arrays.
    for p = 1, iHighestIndex, 1 do
        
        --Sub string
        local sSubString = string.format("%02i", p)
        
        --Variable creation.
        if(PairanormalLevel_IsLoading() == false) then
            VM_SetVar("Root/Variables/Chapter2/InvestigationB/iObjectPresent|" .. sObjectString .. sSubString, "N", 0.0)
        end
        
    end
end

-- |[Backgrounds]|
--List of backgrounds.
SLF_Open(fnResolvePath() .. "../../Datafiles/Backgrounds.slf")
local saList = {"Hallway", "Gym", "Classroom", "SchoolExterior", "Forest", "ClubHouse", "WalkHome", "Library", "LivingRoom", "Bedroom", "BedroomNight", "Office", "Grafitti"}

for i = 1, #saList, 1 do
    DL_ExtractBitmap(saList[i], "Root/Images/Backgrounds/All/" .. saList[i])
end
SLF_Close()
