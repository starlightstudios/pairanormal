--[ ================================== Dialogue Actor Creation ================================== ]
--Dialogue Actors are the spine objects who appear during gameplay. They are animated and have a set
-- of emotion/expression pairs. They must be created here before they are used.
--All characters use the same set of emotions. They are either just the neutral pose if the character
-- has one emotion, or they are neutral/sad/happy/blush/mad/shocked over 3 poses.
-- The format for emotions is always charname_emotionname, with neutral being 'n'.
--These actors are in addition to the ones in Chapter 1. All actors from Chapter 1 return even if
-- Chapter 1 did not load initially.
local function fnCreateCharacter(saNameList, saLowerList, zaEmotionList, faHalfWidthList)

	--Arg check.
	if(saNameList      == nil) then return end
	if(saLowerList     == nil) then return end
	if(zaEmotionList   == nil) then return end
	if(faHalfWidthList == nil) then return end
	
    --Get the Pairanormal spine path.
    local sPairanormalBasePath = VM_GetVar("Root/Paths/System/Startup/sPairanormalPath", "S")
    local sSpinePath = sPairanormalBasePath .. "Spine/"
	
	--Build the name lookups.
	local saLookupList  = {"Neutral", "Sad", "Happy", "Blush", "Mad", "Shocked"}
	local saCompareList = {"n",       "sad", "happy", "blush", "mad", "shock"}
	
	--Arrays are expected to be parallel.
	local i = 1
	while(saNameList[i] ~= nil and saLowerList[i] ~= nil and zaEmotionList[i] ~= nil and faHalfWidthList[i] ~= nil) do

        --Special case: ReichLeif are two characters who use the exact same sprites.
        local bRenderHFlip = false
        local sUseImgName = saNameList[i]
        local sUseLowerName = saLowerList[i]
        if(sUseImgName == "Reich" or sUseImgName == "Leif") then
            
            --H-flip handler.
            if(sUseImgName == "Reich") then
                bRenderHFlip = true
            end
            
            --Flags.
            sUseImgName = "ReichLeif"
            sUseLowerName = "reich"
        end


		--Create the character using their uppercase name.
		Dia_CreateCharacter(saNameList[i])

			--If the character uses one expression, it's the neutral. The list should only have one entry in that case.
			if(zaEmotionList[i][2] == nil) then
                
                --Special: Don't use the _n.
                if(sUseLowerName == "student1") then
                    DiaChar_SetProperty("Add Expression", "Expression0", sSpinePath .. sUseImgName .. "/Expression0", "Root/Images/Characters/" .. sUseImgName .. "/Expression0", sUseLowerName, "null")
                    DiaChar_SetProperty("Remap Emotion", "Neutral", "Expression0", sUseLowerName)
                
                --Normal:
                else
                    DiaChar_SetProperty("Add Expression", "Expression0", sSpinePath .. sUseImgName .. "/Expression0", "Root/Images/Characters/" .. sUseImgName .. "/Expression0", sUseLowerName .. "_n", "null")
                    DiaChar_SetProperty("Remap Emotion", "Neutral", "Expression0", sUseLowerName .. "_n")
                end
		
			--Character uses all six emotions.
			else
				DiaChar_SetProperty("Add Expression", "Expression0", sSpinePath .. sUseImgName .. "/Expression0", "Root/Images/Characters/" .. sUseImgName .. "/Expression0", sUseLowerName .. "_" .. zaEmotionList[i][1], sUseLowerName .. "_" .. zaEmotionList[i][2])
				DiaChar_SetProperty("Add Expression", "Expression1", sSpinePath .. sUseImgName .. "/Expression1", "Root/Images/Characters/" .. sUseImgName .. "/Expression1", sUseLowerName .. "_" .. zaEmotionList[i][3], sUseLowerName .. "_" .. zaEmotionList[i][4])
				DiaChar_SetProperty("Add Expression", "Expression2", sSpinePath .. sUseImgName .. "/Expression2", "Root/Images/Characters/" .. sUseImgName .. "/Expression2", sUseLowerName .. "_" .. zaEmotionList[i][5], sUseLowerName .. "_" .. zaEmotionList[i][6])
				
				--Remaps
				for e = 1, 3, 1 do
					
					--Slot Lookups
					local iSlot = ((e-1)*2)+1
					
					--Name lookups
					local sLookupA = "Neutral"
					local sLookupB = "Neutral"
					for p = 1, 6, 1 do
						if(saCompareList[p] == zaEmotionList[i][iSlot+0]) then sLookupA = saLookupList[p] end
						if(saCompareList[p] == zaEmotionList[i][iSlot+1]) then sLookupB = saLookupList[p] end
					end
					
					--Set.
					DiaChar_SetProperty("Remap Emotion", sLookupA, "Expression" .. e-1, sUseLowerName .. "_" .. zaEmotionList[i][iSlot+0])
					DiaChar_SetProperty("Remap Emotion", sLookupB, "Expression" .. e-1, sUseLowerName .. "_" .. zaEmotionList[i][iSlot+1])
				
				end
		
			end
		
			--Other character properties.
			DiaChar_SetProperty("Half Width", faHalfWidthList[i] * 0.10)
			
			if(saNameList[i] == "Eilette") then
				DiaChar_SetProperty("Render Offset", 85)
			end
		
            --H-flipper. Must be done after all expressions are created.
            if(bRenderHFlip == true) then
                DiaChar_SetProperty("Render HFlip", true)
            end
        
		DL_PopActiveObject()

		--Next.
		i = i + 1

	end

end

--Get the Pairanormal spine path.
local sPairanormalBasePath = VM_GetVar("Root/Paths/System/Startup/sPairanormalPath", "S")
local sSpinePath = sPairanormalBasePath .. "Spine/"

--Ross! Has a spelling mistake, needs to be done manually.
Dia_CreateCharacter("Ross")
    local sImgName = "Ross"
    local sLower = "ross"
    DiaChar_SetProperty("Add Expression", "Expression0", sSpinePath .. sImgName .. "/Expression0", "Root/Images/Characters/" .. sImgName .. "/Expression0", "ross_blush", "ross_happy")
    DiaChar_SetProperty("Add Expression", "Expression1", sSpinePath .. sImgName .. "/Expression1", "Root/Images/Characters/" .. sImgName .. "/Expression1", "ros_mad",    "ross_shock")
    DiaChar_SetProperty("Add Expression", "Expression2", sSpinePath .. sImgName .. "/Expression2", "Root/Images/Characters/" .. sImgName .. "/Expression2", "ross_sad",   "ross_n")
    DiaChar_SetProperty("Remap Emotion", "Blush",   "Expression0", "ross_blush")
    DiaChar_SetProperty("Remap Emotion", "Happy",   "Expression0", "ross_happy")
    DiaChar_SetProperty("Remap Emotion", "Mad",     "Expression1", "ros_mad")
    DiaChar_SetProperty("Remap Emotion", "Shock",   "Expression1", "ross_shock")
    DiaChar_SetProperty("Remap Emotion", "Sad",     "Expression2", "ross_sad")
    DiaChar_SetProperty("Remap Emotion", "Neutral", "Expression2", "ross_n")
DL_PopActiveObject()

--[Character Listing]
--Build a list of characters and their expression lists.
local csaNameList   = {"Candace", "EiletteO", "Harold", "Harper", "Reich", "Leif", "Student1", "Student2", "Student3", "Student4", "Student5", "Student6", "Teacher1", "Teacher2", "Teacher3", "Terry"}
local csaLowerList  = {"candace", "eiletteo", "harold", "harper", "reich", "leif", "student1", "student2", "student3", "student4", "student5", "student6", "teacher1", "teacher2", "teacher3", "terry"}
local cfaWidthList  = {      293,        319,      330,      338,     380,    380,        261,        261,        285,        271,        271,        270,        270,        270,        270,     270}

--List of the expressions in the order they are stored in the file. It's not the same for each character.
local csaCandaceList = {"happy", "mad", "n", "n", "shock", "sad"}
local csaEiletteOList = {"happy", "mad", "n", "sad", "shock", "blush"}
local csaHaroldList = {"n"}
local csaHarperList = {"n", "happy", "blush", "sad", "mad", "shock"}
local csaReichLeifList = {"happy", "shock", "blush", "mad", "sad", "n"}
local csaStudent1List = {"n"}
local csaStudent2List = {"n"}
local csaStudent3List = {"n"}
local csaStudent4List = {"n"}
local csaStudent5List = {"n"}
local csaStudent6List = {"n"}
local csaTeacher1List = {"n"}
local csaTeacher2List = {"n"}
local csaTeacher3List = {"n"}
local csaTerryList = {"blush", "shock", "happy", "mad", "sad", "n"}
local czaEmotionList = {csaCandaceList, csaEiletteOList, csaHaroldList, csaHarperList, csaReichLeifList, csaReichLeifList, csaStudent1List, csaStudent2List, csaStudent3List, csaStudent4List, csaStudent5List, csaStudent6List, csaTeacher1List, csaTeacher2List, csaTeacher3List, csaTerryList}

--Run it.
fnCreateCharacter(csaNameList, csaLowerList, czaEmotionList, cfaWidthList)
