--[Credits]
--And special thanks of course.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)
	
--Save the game.
if(PairanormalLevel_IsLoading() == false) then
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Starting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

--Set data.
else
	PairanormalLevel_FlagCheckpoint("Starting")
	PairanormalLevel_SetBackground("Null")
	PairanormalLevel_SetBackground("Null")
	fnDialogue([[ [Music|Null][BG|Null]Thought: ... ]])
end
fnDialogue("[CHAPTEREND|Root/Images/Backgrounds/All/EndOfChapterTwo] ")
LM_ExecuteScript(gsRoot .. "../Chapter 3/Gameplay_SceneA/0 - Nerdseum.lua", "Start")


--[Execution]
--Make it happen fellas.
--[=[
fnDialogue("[BG|Null][MUSIC|Null][CHAREXIT|ALL]Credits: [Voice|Null]Chapter 2 End")
fnDialogue("Credits: developed by Salty Justice and Tess Young (@thechicmonster)")
fnDialogue("Credits: Music by Mag.Lo (maglo.bandcamp.com)")
fnDialogue("Credits: Additional music by bensound (bensound.com)")
fnDialogue("Credits: Special thanks to Mari and Stacy of Geek Remix (youtube.com/user/GeekRemix)")
fnDialogue("Credits: Special thanks to Laura Kate Dale (@LauraKBuzz)")
fnDialogue("Credits: Special thanks to Todd Schlickbernd (@Toddly_Enough)")
fnDialogue("Credits: Special thanks to Bret aka HoodiePanda (youtube.com/user/HoodiePanda)")
fnDialogue("Credits: Special thanks to Michelle aka MangaMinx (youtube.com/user/TheRPGMinx)")
fnDialogue("Credits: And thanks to you for playing our game! We paid for this game from our own pockets and fundraising on Patreon. ")
fnDialogue("Credits: If you would like to play future chapters, please consider backing us on patreon.com/chicmonster.")
fnDialogue([[ [TOMAINMENU] ]])]=]















