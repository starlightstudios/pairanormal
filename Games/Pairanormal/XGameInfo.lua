-- |[ ======================================== Game Info ======================================= ]|
--This file is executed at program startup to specify that this is a game the engine can load.
local sBasePath       = fnResolvePath()
local sGameName       = "Pairanormal"
local sGamePath       = "Root/Paths/System/Startup/sPairanormalPath" --Must exist in the C++ listing.
local sButtonText     = "Play Pairanormal"
local iPriority       = gciChicmonster_Set_Start + 0
local sLauncherScript = "YMenuLaunch.lua" --File in the same folder as this file that will launch the program.
local iNegativeLen    = -12 --Indicates length of launcher name. ZLaunch.lua is -12.

--Local variables.
local zLaunchPath = sBasePath .. "ZLaunch.lua"

-- |[Compatibility Check]|
if(LM_IsGameRegistered("Pairanormal") == false) then
    if(gbShowFailedRegistrations == true) then
        io.write("Not registering Pairanormal. Engine is incompatible.\n")
    end
    return
end

--Add the game entry. Does nothing if it already exists.
fnAddGameEntry(sGameName, sGamePath, sButtonText, iPriority, sLauncherScript, iNegativeLen)

--Add a new path to the game. This informs the engine where it can be launched from.
local iGameIndex = fnGetGameIndex(sGameName)
if(iGameIndex == 0) then return end
fnAddGamePath(gsaGameEntries[iGameIndex].sSearchPaths, zLaunchPath)