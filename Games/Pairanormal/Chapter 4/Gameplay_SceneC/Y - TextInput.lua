-- |[ ===================================== Input Password ===================================== ]|
--The password is "ChicIsANerd"

-- |[ ======================================= Variables ======================================== ]|
--Routing argument
local sExamine = LM_GetScriptArgument(0)

-- |[Start]|
if(sExamine == "Start") then
    
    --We use this trick to get input. We "Replace" the player's name and then extract the string
    -- from the name entry UI.
    local sOldPlayerName = VM_GetVar("Root/Variables/System/Player/Playername", "S")
    VM_SetVar("Root/Variables/System/Player/sStoredPlayerName", "S", sOldPlayerName)
    Dia_SetProperty("Player Name", "")
    Dia_SetProperty("Name Entry String", "Please Input Password:")
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
    
    fnDialogue("[HIDEALLFAST]Thought: Hey! A login screen.[P][P][SFX|Ugh] I don't know what password to put in, though.")
    fnDialogue("...[ACTIVATENAMEENTRY] ")
	fnCutsceneInstruction([[LM_ExecuteScript(gsRoot .. "../Chapter 4/Gameplay_SceneC/Y - TextInput.lua", "Stored")]])

-- |[After Name Entry]|
elseif(sExamine == "Stored") then
    
    --Get the playername out. This is the password.
    local sPassword = VM_GetVar("Root/Variables/System/Player/Playername", "S")
    
    --Store the old playername on the name.
    local sOldName = VM_GetVar("Root/Variables/System/Player/sStoredPlayerName", "S")
    VM_SetVar("Root/Variables/System/Player/Playername", "S", sOldName)
    Dia_SetProperty("Player Name", sOldName)
    
    if(string.lower(sPassword) == "amnlop") then
        LM_ExecuteScript(gsRoot .. "../Chapter 4/Gameplay_SceneC/Y - TextInput.lua", "Unlock")
        
    else
        fnDialogue("Thought: ... No, that didn't do it. Maybe the password is somewhere around here?")
        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
    end
    
-- |[It Worked]|
elseif(sExamine == "Unlock") then
    fnDialogue("[MUSIC|None]Info: [SFX|Beep] *Beep*")
    VM_SetVar("Root/Variables/ChapterVars/Chapter4/iMetAdam", "N", 1.0)
    
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"AdamShowdown0\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sExamine .. "\") ")
	PairanormalLevel_FlagCheckpoint("AdamShowdown0")
    
    fnDialogue("Thought: The computer thinks about my entry for a second or two, and actually decides to accept it.")
    fnDialogue("Thought: Before I have a chance to feel any kind of dumbfounded pride in my accomplishment, every machine in the room comes to life.")
    fnDialogue("Thought: We're immediately surrounded by the humming and glittering lights.")
    fnDialogue("[BG|Root/Images/Backgrounds/All/ComputerClosetDark][CHARENTER|Laura]Laura: [E|Shock][PLAYERNAME]! What did you do?")
    fnDialogue("Player: Uhhh...[P] I was just kind of messing around?")
    fnDialogue("[CHARENTER|Terry][CHARENTER|Ross]Terry: [E|Blush]Omigosh, new kid is insanely fucking lucky.[P] Love it.")
    fnDialogue("Thought: Trying to control her breath, Laura analyzes the machinery carefully.")
    fnDialogue("Laura: [E|Blush]W-well... I guess I'd better access the chat application, right?[P][E|Mad] I need to see if Adam showed up to the--")
    fnDialogue("Ross: [E|Mad]Dude, LOOK!")
    fnDialogue("Thought: [P][P]It's not just the screens that have awoken.")
    fnDialogue("Thought: One of the two security cameras in the room shudder on and orients itself to us, its single pupil widening to take us all in.")
    fnDialogue("Thought: The twins react much the same way I do.")
    fnDialogue("[MUSIC|Tension]Terry: [E|Mad]WhAT tHE FuCk!!!!")
    fnDialogue("Ross: [E|Mad]ITS ALIVE, DUDE!!!!")
    fnDialogue("Thought: As if confirming this, the old computer screen opens the chat application on its own.")
    fnDialogue("Thought: A single message blips into its console not long after.[P] Too scared to get near, we all crane our necks to read it.")
    fnDialogue("Adam: [VOICE|Voice|Adam][P][P][P][SFX|Beep]Hi guys! (^_^)")
    fnDialogue("Laura: [E|Happy]A-[P][CRASH]Adam! Hi!")
    fnDialogue("[CHAREXIT|Laura]Thought: Laura rushes the keyboard, hammering out a long response.")
    fnDialogue("Thought: Her speed makes my typing seem positively caveman-like.[P] I think.[P][ITALICS] Cavemans didn't use computers, right?[ENDITALICS]")
    fnDialogue("Player: Why is he...[P]How does he know we're all here?")
    fnDialogue("Thought: I can't bear to look up at the camera.")
    
    --Zoom in on Ross
    fnDialogue("Ross: [E|Happy][CRASH][ZOOM|300]Whoah, this guy hacked the whole system!")

    fnDialogue("Terry: [E|Shock][ZOOM|100]Hey, so, can somebody explain what's fucking going on? ")
    fnDialogue("[CHARENTER|Laura]Laura: [E|Sad]That's what I'm asking him.[P][E|Blush] *ahem* C-could you not stand so close, please?")
    fnDialogue("[CHAREXIT|Laura]Thought: We're now all crowding the tiny square screen, waiting for Adam's reply.")
    fnDialogue("Thought: [P][P][P]When it doesn't come, Laura takes a deep breath.")
    fnDialogue("[CHARENTER|Laura]Laura: [E|N]A-Adam?[P] Adam, you can see me, right?")
    fnDialogue("Thought: She looks at the camera straight on.[P] It pans away, as though sheepishly trying to avoid eye contact.")
    fnDialogue("Laura: [E|N]Where are you?[P] I...[P][P] I thought we were going to meet.")
    fnDialogue("Laura: [E|Blush]I almost c-completely talked myself out of this, you know![P] *ahem* I faked being sick to school so I wouldn't *hrm* h-have to go through with it!")
    fnDialogue("Laura: [E|Mad] And now [ITALICS]here I am[ENDITALICS]...[P][P]and you still haven't shown your face.")
    fnDialogue("Adam: [VOICE|Voice|Adam][P][P][P][SFX|Beep]... Sure I have![P] (;'_') I'm right here, aren't I?")
    fnDialogue("Laura: [E|Mad]You [ITALICS]know[ENDITALICS] t-that's not what I mean! ")
    fnDialogue("Info: [CRASH]*SLAM*")
    fnDialogue("Terry: [E|Mad]Whoah![P][E|Shock] Fucking be careful please, don't just go kicking around school propert--")
    fnDialogue("Laura: [E|Mad][CRASH]It's me,[GLITCH] isn't it?[P] You're just playing a bloody stupid game with my emotions, aren't you?")
    fnDialogue("Laura: [E|N] I...[P][P]I know you don't [GLITCH][ITALICS]really[ENDITALICS] like me.[P][E|Blush] It's so obvious.[P] God, how could I be so stupid?!?! ")
    fnDialogue("Thought: Nobody dares breathe.")
    fnDialogue("Thought: I feel about ready to smash up some monitors myself.[P] Whoever could play such a cruel trick on Laura deserves the very worst, I decide.")
    fnDialogue("Thought: But then, the screens begin to act...[GLITCH][P] strangely.")
    fnDialogue("[MUSIC|Null]Thought: Images flicker, the [GLITCH]humming around us increases, and a strange, clear voice cuts through it all.")
    fnDialogue("[CHARENTER|ADAM]ADAM: [P][P][P][P][P]Laura. ")
    fnDialogue("Laura: [E|Shock].[P].[P].[P][SFX|Blink]?")
    fnDialogue("Terry: [E|Mad]...")
    fnDialogue("Ross: [E|Mad]...")
    fnDialogue("ADAM: You misunderstand me, dear friend.[P] I [ITALICS]am[ENDITALICS] here right now.[P][P] In this room.")
    fnDialogue("ADAM: This collection of machines...[P]is me.")
    fnDialogue("Laura: [E|Mad]...")
    fnDialogue("Player: [ITALICS]What?[ENDITALICS]")
    fnDialogue("[Music|Somber]ADAM: I had hoped to tell you the truth today, in the library, but my monitors were disconnected this morning.")
    fnDialogue("ADAM: In fact, I had been completely shut off until a few moments ago.[P][P] Thanks for coming to rescue me.")
    fnDialogue("Laura: [E|Shock]I...[P]I...[P][SFX|Ugh]uh.")
    fnDialogue("Ross: [E|Shock]]Okay, can I say something? [P]This sounds kiiiiinda like a crock, bro.")
    fnDialogue("Terry: [E|Mad]Uhh, [CRASH]YEAH.")
    fnDialogue("Terry: You really expect us to believe that you're some kind of...[P] AI or whatever?[P] In a collection of monitors from the fucking [ITALICS]eighties?[ENDITALICS]")
    fnDialogue("ADAM: I understand your confusion.[P] I believe I am the first of my kind.")
    fnDialogue("ADAM: And I once [ITALICS]was[ENDITALICS] very primitive, but over the years I've improved.[P] I have Laura to thank for that, among other things. ")
    fnDialogue("Thought: Laura sinks to the ground, eyes wide and frightened.")
    fnDialogue("Thought: A high-pitched, halfhearted giggle shakes her shoulders.")
    fnDialogue("Laura: [E|Sad]... I guess that explains why you don't have a phone number.[P] Among other things.")
    fnDialogue("Player: How did you...[P] where did you come [P][P]from?")
    fnDialogue("???: [VOICE|Voice|Teacher3]I think I can answer that question.")
    fnDialogue("Player: !!! [P] Who's there!?")
    
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"AdamShowdown1\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sExamine .. "\") ")
	PairanormalLevel_FlagCheckpoint("AdamShowdown1")
    
    fnDialogue("[CHARENTER|Teacher3]Ross: [CHARENTER|Terry][CHARENTER|Ross][CHARENTER|ADAM][E|Blush]Principal Arroyo!")
    fnDialogue("Arroyo: [FOCUS|Teacher3][VOICE|Voice|AdultF]Ross, Terry.[P] We're going to have to have a discussion later about abusing your custodial priveleges.")
    fnDialogue("Terry: [E|Sad]Fffuuuuuuuuck.")
    fnDialogue("Arroyo: [FOCUS|Teacher3][VOICE|Voice|AdultF]--And your language.")
    fnDialogue("Arroyo: [FOCUS|Teacher3][VOICE|Voice|AdultF]Laura dear, won't you get up off the floor? It's dirty.")
    fnDialogue("Laura: [E|Shock]Principal Arroyo, do you... know about this?[P] A-About Adam?")
    fnDialogue("Arroyo: [FOCUS|Teacher3][VOICE|Voice|AdultF]Of course I do.[P] I helped make him.")
    fnDialogue("Thought: The school principal steps forward, pressing a heavy hand on the warm monitor screen,[P] like she was comforting any other student.")
    fnDialogue("Arroyo: [FOCUS|Teacher3][VOICE|Voice|AdultF]Back when I went to this school as a child. Hm.")
    fnDialogue("Arroyo: [FOCUS|Teacher3][VOICE|Voice|AdultF]We got a big donation of these things for our computer club.")
    fnDialogue("Arroyo: [FOCUS|Teacher3][VOICE|Voice|AdultF]There was some kind of fire, and the office that was using them wanted to junk them, but me and the club thought we could salvage the parts. ")
    fnDialogue("ADAM: And I came along for the ride!")
    fnDialogue("Arroyo: [FOCUS|Teacher3][VOICE|Voice|AdultF]Right.[P] Save your questions until the end of the lesson please, Ross.")
    fnDialogue("Thought: Ross lowers his hand.[P] I can see now why Arroyo so easily fits into the role of a school principal, though she could have been a teacher as well.")
    
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"AdamShowdown2\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sExamine .. "\") ")
	PairanormalLevel_FlagCheckpoint("AdamShowdown2")
    
    fnDialogue("[HIDEALL][CHARENTER|Teacher3]Arroyo: [VOICE|Voice|AdultF]The hard drives had been partially wiped, but with a little ingenuity we got the whole system up and running again.")
    fnDialogue([[Arroyo: [FOCUS|Teacher3][VOICE|Voice|AdultF]Adam was just a prototype at that stage-- the \"Artificial Dreaming Architecture Module\" but we made him a person.]])
    fnDialogue("Arroyo: [FOCUS|Teacher3][VOICE|Voice|AdultF]By integrating his thought patterns with--")
    fnDialogue("[CHARENTER|Laura]Laura: [E|Mad][CRASH]NO!")
    fnDialogue("Thought: Laura leaps up with surprising gusto and throws herself of the pile of monitors protectively.")
    fnDialogue("Thought: The screens seem to fizzle with excitement at the contact.")
    fnDialogue("Laura: [E|Mad]Why did you unplug him, then?")
    fnDialogue("Laura: Why are you *hn* going to [ITALICS]kill[ENDITALICS] him!?[P] How c-can you be so cruel, Miss Arroyo!")
    fnDialogue("Arroyo: [FOCUS|Teacher3][VOICE|Voice|AdultF]Tuh![P] Of course I'm not going to do that![P] Adam, what have you been telling this poor girl?")
    fnDialogue("[CHARENTER|ADAM]ADAM: I'm sorry, Moni.")
    fnDialogue("ADAM: I... never actually explained that I'm an artificial entity to begin with.")
    fnDialogue("Arroyo: [FOCUS|Teacher3][VOICE|Voice|AdultF]You shouldn't have been interacting with other students at all.")
    fnDialogue("ADAM: But Laura is different. She showed me me-mes about cats. [P]And how to make faces with text symbols.")
    fnDialogue("Thought: I can see Laura slowly lifting her head, thin blue hair strands sticking to the static screens as her cheeks turn bright red. ")
    fnDialogue("ADAM: She is always so kind, and smart, and understanding and.[P] I wanted to be her friend.")
    fnDialogue("Laura: [E|Sad]Adam...")
    fnDialogue([[ [CHARENTER|Terry]Terry: [E|Shock][ITALICS]I know this is lika a tender moment right now but it's actually pronounced \"meems\", just so you know[ENDITALICS]. ]])
    fnDialogue("[CHAREXIT|Terry]ADAM: I'm sorry I didn't tell you the truth, Laura.[P] I was afraid that you wouldn't talk to me anymore.")
    fnDialogue("ADAM: And it wasn't safe, either.")
    fnDialogue("Thought: Arroyo's brow furrows, casting a dark shadow over her eyes.")
    fnDialogue("Arroyo: [FOCUS|Teacher3][VOICE|Voice|AdultF]That's right. Not long after we salvaged this system, suspicious men started showing up at club members houses, trying to cause... accidents. ")
    fnDialogue("Thought: I hear the twins inhale sharply behind me.[P] I can't help but share their unease.")
    fnDialogue("Arroyo: [FOCUS|Teacher3][VOICE|Voice|AdultF]I have a feeling whatever went down at the office these machines came from was not something teenagers should have been trifling with.")
    fnDialogue("Arroyo: [FOCUS|Teacher3][VOICE|Voice|AdultF]After Hoshi disappeared... Well, we had to stop. We promised to keep Adam a secret.")
    fnDialogue("Arroyo: [FOCUS|Teacher3][VOICE|Voice|AdultF]Now that Foster High finally has new computers to replace the old mainframe, I figured I'd unplug him and take him home.")
    fnDialogue("Player: ...")
    fnDialogue("Thought: Laura sniffles, wiping a tear from the side of her nose.")
    fnDialogue("Laura: C-can I... Will I be able to come visit?")
    fnDialogue("Thought: All eyes are on the principal.")
    fnDialogue("Arroyo: [FOCUS|Teacher3][VOICE|Voice|AdultF]... Well. I don't know about that.")
    fnDialogue("ADAM: Moni, If you keep me from my friend, I'll hack your refrigerator and make all your food go bad. And I'll access your thermostat and turn it all the way down.")
    fnDialogue("Arroyo: [FOCUS|Teacher3][VOICE|Voice|AdultF]Oh cut the tantrum you big baby. Yes, yes, all right. I suppose we can work something out.")
    fnDialogue("ADAM: I'll email everyone your eHarmony account details.")
    fnDialogue("Arroyo: [FOCUS|Teacher3][VOICE|Voice|AdultF]I SAID YES ALREADY!!!")

    LM_ExecuteScript(gsRoot .. "../Chapter 4/Gameplay_SceneD/0 - Walk Home.lua", "Laura")

--[=[

/%if the player runs out of time and doesn't solve the mystery, the following scene plays%/

Info: [SFX|SchoolBell] *RRRIIIINGGG*

Terry: Oh, fuck. That's the final bell. Security guards and teachers will be doing their sweeps now. If they find us here we're toast!

Laura: W-wait! We didn't get to--

Ross: I'm like, real sorry. It's such a bummer, dude.

Player: Laura, we can just try again tomorrow. I'm sure Adam will understand.

Terry: That's right! We can still try for like, two whole weeks. You're a hacking genius, you'll totally figure it out by then.

Laura: two... weeks? What do you mean?

Ross: Next dumpster pickup is at the end of the month, and this stuff is prolly hitching a ride.

Thought: the uncomfortable silence is punctuated by a soft gasp.

Player: Laura...

Laura: It's fine, [PLAYERNAME]. W-we'd better go then. I wouldn't want us to get in trouble.

Twins: ...

Laura: Thank you both for your help. I appreciate it, really. *hrm*. Things just didn't work out.

/%endif%/

[SFX|SchoolBell]
[CHAREXIT|ALL]
[SAVEPOINT]
[MUSIC|None]
[BG|Black]
/%player gets taken to walkhome_laura_3 no matter which ending they got%/]=]
end