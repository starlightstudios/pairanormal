--[ ================================== Warehouse Investigation ================================== ]
--I love smelly old buildings!

--[ ========================================= Variables ========================================= ]
--[Arguments]
--Get the name of what we're investigating.
local sExamine = LM_GetScriptArgument(0)

--[Variable Setup]
--Used to simplify the Add Discovered Object calls.
local sBGPath = "Root/Images/Backgrounds/All/ComputerCloset"
local sScriptPath = gsRoot .. "../Chapter 4/Gameplay_SceneC/0 - ExamineObject.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
local sAutoAdvance = gsRoot .. "../Chapter 4/Gameplay_SceneD/7 - PatchGoingHome.lua"

--Saving
gsSaveExecute = gsRoot .. "../Chapter 4/Gameplay_SceneA/3 - Bookakke.lua"
gsSaveTopic = "No Topic"
gsSaveFlag = "InvestigationWarehouse"

--[ ========================================= Functions ========================================= ]
--Stores the object as discovered.
local fnStdStore = function(i)
    
    --Get if we've examined the object before. This will be returned later.
	local iExaminedAlready = VM_GetVar("Root/Variables/Chapter4/InvestigationB/iObject|" .. gczaInv4B_List[i], "N")
    
    --Save it as a discovered object.
	Dia_SetProperty("Add Discovered Object", gczaInv4B_List[i], sBGPath, sScriptPath, gczaInv4B_List[i])
	VM_SetVar("Root/Variables/Chapter4/InvestigationB/iObject|" .. gczaInv4B_List[i], "N", 1.0)
    
    --Pass back.
    return iExaminedAlready
end

--Decrements time. Also saves the game.
local fnStdTime = function()
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(gsSaveFlag, gsSaveExecute, gsSaveTopic)
    end
end

--Standard handler for objects. Saves the object, prints a dialogue, advances time.
local fnStdObject = function(iIndex, saStrings)
	
    --Argument check.
    if(iIndex == nil) then return end
    
	--Store it in the inventory.
	local i = iIndex
	local iExaminedAlready = fnStdStore(i)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
    
    for i = 1, #saStrings, 1 do
		fnDialogue(saStrings[i])
    end
    
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
    if(iExaminedAlready == 0.0) then fnStdTime(sSaveFlag, sSaveExecute, sSaveTopic) end
end

--[ ==================================== Examinable Objects ===================================== ]
if(sExamine == "KolaVault") then
    fnStdObject(gciInv4B_KolaVault, 
    {"Thought: [HIDEFAST]I can see the faint outline of Kane Kola bottles on the other side of the frosted glass.",
     "Thought: The fridge does look comically oversecured, until I notice the hundreds of tiny bite and scratch marks scuffing the corners.",
     "Thought: [ITALICS]Who did that?[ENDITALICS] "})

elseif(sExamine == "SnackStock") then
    fnStdObject(gciInv4B_SnackStock, 
    {"Thought: [HIDEFAST]Looks like standard vending machine options.",
     "Thought: Boring chips,[P] cheesy triangle chips,[P] cheesy crackers, trail mix,[P] and those delicious cookies with the dollop of raspberry jam in the middle."})

elseif(sExamine == "AirConditioning") then
    fnStdObject(gciInv4B_TheAC, 
    {"Thought: [HIDEFAST]There's a cloud on it...[P] and some buttons and knobs, so...[P][SFX|Blink][ITALICS]It must control the weather.[ENDITALICS][P] Cool."})

elseif(sExamine == "CleaningSupplies") then
    fnStdObject(gciInv4B_JanitorBits, 
    {"Thought: [HIDEFAST]I'm not sure I'm comfortable seeing the snacks stored so close to all these cleaning supplies."})

elseif(sExamine == "Yamamoto") then
    fnStdObject(gciInv4B_Yamamoto, 
    {"Thought: [HIDEFAST]Somebody pinned this newspaper article here.[P] Looks like its been here a while."})

elseif(sExamine == "NewComputer") then
    fnStdObject(gciInv4B_NewComputer, 
    {"Thought: [HIDEFAST]This computer has a much wider screen and sleeker look than the old ones in the library."})

elseif(sExamine == "BiminiManual") then
    fnStdObject(gciInv4B_LinuxManual, 
    {"Thought: [HIDEFAST]A big, boring book."})

elseif(sExamine == "Camera") then
    fnStdObject(gciInv4B_Camera, 
    {"Thought: [HIDEFAST]There's two cameras in here, neither which appear to be on."})

elseif(sExamine == "Controls") then

	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
    
    fnDialogue("Thought: [HIDEFAST]There's a lot of buttons and switches on this thing. [P]I don't know if it's a good idea to mess with it.")
    local sDecisionScript = "\"" .. gsRoot .. "../Chapter 4/Gameplay_SceneC/X - Controls.lua\""
    fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Button 1\", " .. sDecisionScript .. ", \"Btn1\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Button 2\", " .. sDecisionScript .. ", \"Btn2\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Button 3\", " .. sDecisionScript .. ", \"Btn3\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Leave It\", " .. sDecisionScript .. ", \"Nope\") ")

elseif(sExamine == "Login") then

    --Variables.
    local iIsLoginOn = VM_GetVar( "Root/Variables/Chapter4/InvestigationB/iIsLoginOn", "N")

    --Not active:
    if(iIsLoginOn == 0.0) then
        fnStdObject(gciInv4B_Login, 
        {[[Info: [HIDEFAST]\"PEALSE NNOCECT PROT TREHE TO WPOER.[P] CTUMPOER WILL ONW SUHT DNWO\" ]],
         "Thought: The blinking text makes this extra hard to read."})

    --Active:
    else
        LM_ExecuteScript(gsRoot .. "../Chapter 4/Gameplay_SceneC/Y - TextInput.lua", "Start")
    end

--[ ======================================== Characters ========================================= ]
elseif(sExamine == "Terry") then
    fnStdObject(gciInv4B_Terry, 
    {"Thought: [HIDEFAST]Terry taps her foot impatiently, but I can tell she's excited to be here.[P] We're probably both anxious to meet this Adam guy."})

elseif(sExamine == "Ross") then
    fnStdObject(gciInv4B_Ross, 
    {"Thought: [HIDEFAST]Ross is already raiding the snack backstock.[P] Whoever put him in charge of restocking the vending machines was not thinking straight."})

elseif(sExamine == "Laura") then
    fnStdObject(gciInv4B_Laura, 
    {"Thought: [HIDEFAST]Laura's cautiously looking over all the wires and buttons. ",
     "Thought: I wish I could help her figure out how to turn this thing on, but I don't know anything about computers."})

elseif(sExamine == "Phone") then
    fnStdObject(gciInv4B_Phone, 
    {"Thought: [HIDEFAST]If typing on a keyboard was tough, typing on a tiny cellphone feels next to impossible."})

end