-- |[ ===================================== Control Panel ====================================== ]|
--Player input on control panel.

--Variables.
local sExamine = LM_GetScriptArgument(0)

--Cases.
if(sExamine == "Btn1") then
    
    fnDialogue("Thought: Hmm, that did something, but I don't know what.")
    VM_SetVar( "Root/Variables/Chapter4/InvestigationB/iIsLoginOn", "N", 0.0)
    
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
    
elseif(sExamine == "Btn2") then
    
    fnDialogue("Thought: Hmm, that did something, but I don't know what.")
    VM_SetVar( "Root/Variables/Chapter4/InvestigationB/iIsLoginOn", "N", 0.0)
    
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
    
elseif(sExamine == "Btn3") then
    
    fnDialogue("Thought: That seems to have activated another computer.")
    VM_SetVar( "Root/Variables/Chapter4/InvestigationB/iIsLoginOn", "N", 1.0)
    
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
    
elseif(sExamine == "Nope") then
    
    fnDialogue("Thought: Better leave it alone for now.")
    
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
end