-- |[Terry Presentation]|
--Showing stuff to Terry.

-- |[Arg Resolve]|
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--Variables.
local sScriptPath = gsRoot .. "../Chapter 4/Gameplay_SceneC/2 - Present Terry.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
local sAutoAdvance = gsRoot .. "../Chapter 4/Gameplay_SceneD/7 - PatchGoingHome.lua"

--Saving
gsSaveExecute = gsRoot .. "../Chapter 4/Gameplay_SceneA/3 - Bookakke.lua"
gsSaveTopic = "No Topic"
gsSaveFlag = "InvestigationWarehouse"

-- |[ ======================================= Functions ======================================== ]|
-- |[Friendship Increment]|
--If this is the first time presenting this object to this character then we increase the friendship tracker for
-- that character.
local fnIncrementFriendship = function(iObjectIndex, iIncrement)
	
    --Arg check.
    if(iObjectIndex == nil) then return end
    if(iIncrement == nil) then return end
    
    --Build strings.
	local i = iObjectIndex
    local p = iTerryIndex
    local sObjectString = string.format("%02i", i)
    local sSubString    = string.format("%02i", p)
    local iIsRepeat = VM_GetVar("Root/Variables/Chapter4/InvestigationB/iObjectPresent|" .. sObjectString .. sSubString, "N")
    if(iIsRepeat == 0.0) then
        VM_SetVar("Root/Variables/Chapter4/InvestigationB/iObjectPresent|" .. sObjectString .. sSubString, "N", 1.0)
    end
end

-- |[Sequence Handler]|
--Almost all sequences can use a standardized template.
local fnStdSequence = function(iFriendshipIndex, saStrings)
    
    --Start.
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    
    for i = 1, #saStrings, 1 do
		fnDialogue(saStrings[i])
    end
    
    --Finish.
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(iFriendshipIndex, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
end

-- |[ ===================================== Chat Sequences ===================================== ]|
--Chatting.
if(sTopicName == "Chat") then
	
	--Variables.
	local iChattedWithTerry = VM_GetVar("Root/Variables/Chapter4/Investigation/iChattedWithTerry", "N")
	
	--First pass:
	if(iChattedWithTerry == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter4/Investigation/iChattedWithTerry", "N", 1.0)
		
		--Text.
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
    
		fnDialogue([[[HIDEALLFAST][SHOWCHAR|Terry][SHOWCHAR|Ross]Terry: [E|Mad][CRASH]AAHHH![P] What!?!?]])
		fnDialogue([[Player: [CRASH]AHHHH!!!]])
		fnDialogue([[Ross: [E|Blush]Whoah, Whoah, whoaaaaah![P][E|N] Take it easy, will you sis? They were talking to me.]])
		fnDialogue([[Terry: [E|Shock]Were not!]])
		fnDialogue([[Ross: [E|Shock]I think so.]])
		fnDialogue([[Player: I was talking to--[P]well, it doesn't really matter. I'm sorry to have startled you, Terry.]])
		fnDialogue([[Terry: [E|Sad]Ugh, yeah, sorry.[P] I'm not a huge fan of confined spaces.]])
		fnDialogue([[Player: Why not?]])
		fnDialogue([[Terry: [E|N]Because--]])
		fnDialogue([[Ross: [E|Sad]She's just not.]])
		fnDialogue([[Terry: [P][P][E|Sad]... Yeah.[GLITCH] ]])
		fnDialogue([[[BG|Root/Images/Backgrounds/All/Hallway][Music|Null][HIDEALLFAST]Thought: [GLITCH][GLITCH][GLITCH]A volley[GLITCH] of painful memories violently intrudes my[BG|ComputerCloset] mind.]])
		fnDialogue([[Thought: It is so forceful,[GLITCH] it takes all my willpower not to[GLITCH] collapse.]])
		fnDialogue([[Player: G-[GLITCH]guhhh...]])
		fnDialogue([[Terry: [BG|Hallway]Whoah,[GLITCH][GLITCHTEXT] are you okay?]])
		fnDialogue([[Ross: [GLITCH]Take a seat bro, [GLITCH]chill out. Hang on, let me get you some c[GLITCH]hips to munch on.[ENDGLITCHTEXT] ]])
		fnDialogue([[Player: I'm [GLITCHTEXT]FFFF[ENDGLITCHTEXT]fine. I]])
		fnDialogue([[[HIDEALLFAST][BG|Null][BG|Null]Thought: [P][P][P]...]])
		fnDialogue([[[BG|Root/Images/Backgrounds/All/Hallway][MUSIC|Somber][CHARENTER|Teacher2][CHARENTER|Ross]Teacher: [FOCUS|Teacher2][VOICE|Voice|AdultF]--can't [GLITCH]have these kinds of rumors spreading around.]])
		fnDialogue([[Player: [VOICE|Voice|Terry]But it really did happen!]])
		fnDialogue([[Ross: [E|Sad]Teach.[P] All due respect.[P] Why would both of us lie about something like this?]])
		fnDialogue([[Teacher: [FOCUS|Teacher2][VOICE|Voice|AdultF]I haven't the faintest clue what's in your heads right now, but I'm laying out the facts.]])
		fnDialogue([[Teacher: [FOCUS|Teacher2][VOICE|Voice|AdultF]The police investigating your claim[P]--and they DID investigate--[P] didn't find [ITALICS]anything[ENDITALICS] corroborating your story.]])
		fnDialogue([[Teacher: [FOCUS|Teacher2][VOICE|Voice|AdultF]If the two of you allow this rumor to persist, you'll have the entire student body in a needless panic.]])
		fnDialogue([[Player: [VOICE|Voice|Terry]They SHOULD be afraid, this could happen to anyone! People aren't SAFE out there.[P] As for the police--]])
		fnDialogue([[Teacher: [FOCUS|Teacher2][VOICE|Voice|AdultF]--And what's more, the story just doesn't make any [ITALICS]sense.[ENDITALICS] ]])
		fnDialogue([[Teacher: [FOCUS|Teacher2][VOICE|Voice|AdultF]You both admit there's no motive,[P] and you can't explain how you came to be standing before us here, completely unharmed.]])
		fnDialogue([[Thought: [VOICE|Voice|Terry]I'm forced to pause.]])
		fnDialogue([[Thought: [VOICE|Voice|Terry]What a wretched world we live in, that surviving something so traumatic without a scratch on me is somehow considered [ITALICS]bad[ENDITALICS] luck.]])
		fnDialogue([[Thought: [VOICE|Voice|Terry]But this is the single most frightening truth of all;[P][P][P] I really can't explain how we escaped.]])
		fnDialogue([[Thought: [VOICE|Voice|Terry]One minute, we were snatched off the sidewalk and thrown into that awful place...[P]and the next, Ross and I woke up on our front lawn. ]])
		fnDialogue([[[CHARENTER|Teacher3]Arroyo: [VOICE|Voice|AdultF][FOCUS|Teacher3]Terry,[P] Ross.[P][P][P] I understand that it [ITALICS]seems[ENDITALICS] like something real, and I believe something maybe did happen.]])
		fnDialogue([[Arroyo: [VOICE|Voice|AdultF]I've personally invited a special counselor over if there's anything the two of you want to talk about with him. ]])
		fnDialogue([[Thought: [VOICE|Voice|Terry]I'm not [GLITCH]fucking delusional.[P] I'm not.[P][P] [ITALICS]I'm not[ENDITALICS].]])
		fnDialogue([[Thought: [VOICE|Voice|Terry]I look to Ross, hoping to find some comfort in his goofy smile or an exaggerated eye roll.[P] But he's completely distracted.]])
		fnDialogue([[Arroyo: [VOICE|Voice|AdultF]... All I ask is that you not speak about this anymore with your fellow students;[P] it's not conducive to a learning environment.]])
		fnDialogue([[Arroyo: [VOICE|Voice|AdultF]If you do, I'll have to revoke your custodial priveleges.[P] Are we clear?]])
		fnDialogue([[Player: [VOICE|Voice|Terry][P].[P].[P].Yes ma'am.]])
		fnDialogue([[[HIDEALLFAST]Player: [VOICE|Voice|Terry]This is such...[GLITCH] fucking bullshit![P] We have to... there's got to be someone we can tell who'll believe us, right?]])
		fnDialogue([[Player: [VOICE|Voice|Terry][P][P][P]Ross?[P][P][CRASH] Are you even paying attention, you dolt!!?]])
		fnDialogue([[[CHARENTER|Ross]Ross: [P][P][P][E|Sad]Mmh, let's face it, sis.[P] We lost this one.]])
		fnDialogue([[Ross: [E|Shock] I say we cut our losses and... try to move on, somehow.]])
		fnDialogue([[Player: [VOICE|Voice|Terry]Move on?[P] Fucking [ITALICS]move on[ENDITALICS]?[P] How are you not burning with rage right now?]])
		fnDialogue([[Player: [VOICE|Voice|Terry]How do you not want to tear apart everyone in the vicinity limb from limb for being heartless, uncaring monsters?]])
		fnDialogue([[Thought: [VOICE|Voice|Terry]My twin--the only other person who remembers this awful event and therefore the rock of sanity to which I've had to cling--shrugs like I've asked him a math question. ]])
		fnDialogue([[Ross: [E|Sad]I dunno.[P][E|Shock] Being kidnapped and watching your whole life, like, flash before your eyes kinda makes you reevaluate a lot of things.]])
		fnDialogue([[Ross: [E|Sad]I suddenly... don't really care what everyone thinks anymore.]])
		fnDialogue([[Thought: [VOICE|Voice|Terry]When we were kids, my brother and I often played mirror; trying to perfectly match each other's movements because everyone said we looked so alike.]])
		fnDialogue([[Thought: [VOICE|Voice|Terry]Ross's face is drawn and haggard now--[P]is that what I look like, too?]])
		fnDialogue([[Ross: [E|Sad]I kinda don't care about anything, actually.[P] I just want to go home and lie down for a bit.]])
		fnDialogue([[Thought: [VOICE|Voice|Terry]I can only stand and watch him do just that.]])
		fnDialogue([[Thought: [VOICE|Voice|Terry]He shrugs,[GLITCH] turns tail, and disappears out the back entrance.[GLITCH][GLITCH] ]])
		fnDialogue([[[HIDEALLFAST][BG|Null][BG|Null]Thought: [VOICE|Voice|Terry]...]])
		fnDialogue([[[BG|Root/Images/Backgrounds/All/ComputerCloset][Music|Investigation][CHARENTER|Terry][CHARENTER|Ross]Terry:[E|Mad] --smell of cheese puffs will NOT suddenly snap them back to reality.[P][P][E|Shock] Oh wait, yes it did.[P] Huh. ]])
		fnDialogue([[Ross: [E|Happy][ITALICS]Told you[ENDITALICS], haha. Snacking will save us all.[P] Here dude, chow it up.]])
		fnDialogue([[Thought: I find myself pushing off of the wall I had slouched against,[P] drawn to the faint crinkle of a Cheese Puff bag Ross holds out to me.]])
		fnDialogue([[Thought: As I grab a handful, I look at the twins' eager faces.]])
		fnDialogue([[Thought: They're a little eccentric, but I'd never pin them as...[P]whatever they were, just then.]])
		fnDialogue([[Terry: [E|Blush]All right, you've had your munch.[P][E|N] Now hurry up and wrap things up here, please?]])
		fnDialogue([[Player: Oh gosh.[P] Right.[P] Laura.]])
		fnDialogue([[Ross: [E|Happy]Or take all the time you like. More snacks for me.]])
        
        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
	
        --Time loss.
        if(fnDecrementTime(5, sAutoAdvance) == false) then
            PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
        end
		
	--Repeat passes.
	else
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()

		fnDialogue([[[HIDEALLFAST][SHOWCHAR|Terry][SHOWCHAR|Ross]Terry: [FOCUS|Terry][E|Blush]Hurry up and wrap things up here, please?]])
		fnDialogue([[Ross: [E|Happy]Or take all the time you like. More snacks for me.]])
		fnDialogue([[Player: Right.]])

        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
	end
	
-- |[ ================================== Object Presentation =================================== ]|
elseif(sTopicName == "KolaVault") then
    fnStdSequence(gciInv4B_KolaVaula, 
    {"[HIDEALLFAST][SHOWCHAR|Terry]Terry: [E|Mad][ITALICS]Genuinely[ENDITALICS] no idea why we even stock Kane Kola.",
     "Terry: If you try to buy one from the vending machines, someone's bound to slam it out of your hands. "})

elseif(sTopicName == "SnackStock") then
    fnStdSequence(gciInv4B_SnackStock, 
    {"[HIDEALLFAST][SHOWCHAR|Terry]Terry: [E|N] If I can be honest, the snack selection here is severely fucking lacking.",
     "Terry: [E|Blush] If [ITALICS]I[ENDITALICS] were in charge of snacks instead of ASB, we'd have Pocky,[P] frozen popsicles,[P] Ice cream sandwich bars,[P] and name brand chips.",
     "[SHOWCHAR|Ross]Ross: [E|N]Sis, you know that would never fly with ASB. None of those snacks are healthy.",
     "Terry: [E|Mad]But they're the snacks everyone wants.[P][E|N] Would ASB say no to [ITALICS]profit?[ENDITALICS]"})

elseif(sTopicName == "AirConditioning") then
    fnStdSequence(gciInv4B_TheAC, 
    {"[HIDEALLFAST][SHOWCHAR|Terry]Terry: [E|Shock]I about how offices have started installing fake thermostats in their offices because people keep fighting over the temperature controls.",
     "Terry: [E|Happy] While it's patently capitalistic and fucking evil, I do have to admire the cleverness."})

elseif(sTopicName == "CleaningSupplies") then
    fnStdSequence(gciInv4B_JanitorBits, 
    {"[HIDEALLFAST][SHOWCHAR|Terry]Terry: [E|Shock]These janitor supplies have probably been here since the first brick was fucking laid on this building.[P][E|Happy] But I'm not allowed to throw them out???"})

elseif(sTopicName == "Yamamoto") then
    fnStdSequence(gciInv4B_Yamamoto, 
    {"[HIDEALLFAST][SHOWCHAR|Terry]Terry: [E|N]Hoshi Yamamoto.[P] Apparently the computer lab is in his honor, or whatever.[P][E|Shock] No idea who he is."})

elseif(sTopicName == "NewComputer") then
    fnStdSequence(gciInv4B_NewComputer, 
    {"[HIDEALLFAST][SHOWCHAR|Terry]Terry: [E|N]Theoretically, a technical marvel much needed at this school.[P][E|Happy] But whenever I look at it, I just see [ITALICS]annoying teens[ENDITALICS] invading the library.",
     "[SHOWCHAR|Ross]Ross: [E|Mad]You.[P] Are.[P] An annoying.[P] Teen."})

elseif(sTopicName == "BiminiManual") then
    fnStdSequence(gciInv4B_LinuxManual, 
    {"[HIDEALLFAST][SHOWCHAR|Terry]Terry: [E|Blush]Ooh, I love 80s corporate manual [ITALICS]aesthetics[ENDITALICS].",
     "Player: This book can't be that old.",
     [[Terry: [E|Happy]Oh yeah? Look at this note on the inside:[P] \"AMNLOP 10188\".]],
     "Terry: Those last numbers are probably the date the computer was manufactured![P][E|Shock] God, these are so fucking ancient."})

elseif(sTopicName == "Camera") then
    fnStdSequence(gciInv4B_Camera, 
    {"[HIDEALLFAST][SHOWCHAR|Terry]Player: Why are there two cameras in here?",
     "Terry: [E|N]Well technically there's pretty valuable stuff in this room.[P][E|Shock] Not much use to anyone if they're not even fucking [ITALICS]on[ITALICS], though."})

elseif(sTopicName == "Controls") then
    fnStdSequence(gciInv4B_Controls, 
    {"[HIDEALLFAST][SHOWCHAR|Terry]Terry:[E|Shock] Hm? You say something?",
     "Player: Yeah.[P] You seem kind of nervous.",
     "Terry: [E|Sad]I'm fine."})

elseif(sTopicName == "Login") then
    fnStdSequence(gciInv4B_Login, 
    {[[[HIDEALLFAST][SHOWCHAR|Terry]Terry: [E|Happy]Hey, try \"1234\" as a password.]],
     "Thought: I type in the numbers and hit the enter key, but the screen simply buzzes angrily at me.[P] Terry pouts.",
     "Terry: [E|Shock]I hear lots of people use 1234 as a password, but I've never actually gotten it to fucking work on anything."})

-- |[ ======================================= Characters ======================================= ]|
elseif(sTopicName == "Terry") then
    fnStdSequence(gciInv4B_Terry, 
    {"[HIDEALLFAST][SHOWCHAR|Terry]Terry:[E|Shock] Hm? You say something?",
     "Player: Yeah.[P] You seem kind of nervous.",
     "Terry: [E|Sad]I'm fine."})

elseif(sTopicName == "Ross") then
    fnStdSequence(gciInv4B_Ross, 
    {"[HIDEALLFAST][SHOWCHAR|Terry]Terry: [E|Mad]Jesus fucking christ, he is already in the snacks.[P][P][E|Shock] Actually, can you ask him to pass some to me?"})

elseif(sTopicName == "Laura") then
    fnStdSequence(gciInv4B_Laura, 
    {"[HIDEALLFAST][SHOWCHAR|Terry]Terry: [E|Blush]I thought this whole secret illicit meeting thing was actually [ITALICS]quite[ENDITALICS] fucking romantic...",
     "Terry: Until of course [ITALICS]this[ENDITALICS] step, where we have to...[P][E|Shock]turn on a computer?[P][E|Mad] Which basically sounds like the start of a true crime episode.",
     [[Player: What's the difference between \"true\" crime and...[P]regular crime?]],
     "Terry: [E|Happy]Severely fucking gruesome, yet somehow more entertaining than depressing."})

elseif(sTopicName == "Phone") then
    fnStdSequence(gciInv4B_Phone, 
    {"[HIDEALLFAST][SHOWCHAR|Terry]Terry: [E|Shock]Can't this boyfriend of hers, just like, text her? What is he, a fucking caveman?"})

end