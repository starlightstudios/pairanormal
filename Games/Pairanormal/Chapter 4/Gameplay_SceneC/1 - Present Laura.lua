--[Laura Presentation]
--Showing stuff to Laura.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--Variables.
local sScriptPath = gsRoot .. "../Chapter 4/Gameplay_SceneC/1 - Present Laura.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
local sAutoAdvance = gsRoot .. "../Chapter 4/Gameplay_SceneD/7 - PatchGoingHome.lua"

--Saving
gsSaveExecute = gsRoot .. "../Chapter 4/Gameplay_SceneA/3 - Bookakke.lua"
gsSaveTopic = "No Topic"
gsSaveFlag = "InvestigationWarehouse"

--[ ========================================= Functions ========================================= ]
--[Friendship Increment]
--If this is the first time presenting this object to this character then we increase the friendship tracker for
-- that character.
local fnIncrementFriendship = function(iObjectIndex, iIncrement)
	
    --Arg check.
    if(iObjectIndex == nil) then return end
    if(iIncrement == nil) then return end
    
    --Build strings.
	local i = iObjectIndex
    local p = iLauraIndex
    local sObjectString = string.format("%02i", i)
    local sSubString    = string.format("%02i", p)
    local iIsRepeat = VM_GetVar("Root/Variables/Chapter4/InvestigationB/iObjectPresent|" .. sObjectString .. sSubString, "N")
    if(iIsRepeat == 0.0) then
        
        --Flag
        VM_SetVar("Root/Variables/Chapter4/InvestigationB/iObjectPresent|" .. sObjectString .. sSubString, "N", 1.0)
    
        --Variable gain.
        local iLauraFriend = VM_GetVar("Root/Variables/System/Romance/iLauraFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iLauraFriend", "N", iLauraFriend + iIncrement)
    end
end

--[Sequence Handler]
--Almost all sequences can use a standardized template.
local fnStdSequence = function(iFriendshipIndex, saStrings)
    
    --Start.
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    
    for i = 1, #saStrings, 1 do
		fnDialogue(saStrings[i])
    end
    
    --Finish.
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(iFriendshipIndex, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
end

--[ ======================================= Chat Sequences ====================================== ]
--Chatting.
if(sTopicName == "Chat") then
	
	--Variables.
	local iChattedWithLaura = VM_GetVar("Root/Variables/Chapter4/Investigation/iChattedWithLaura", "N")
	
	--First pass:
	if(iChattedWithLaura == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter4/Investigation/iChattedWithLaura", "N", 1.0)
		
		--Text.
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
    
		fnDialogue([[[HIDEALLFAST][SHOWCHAR|Laura]Player: Hey Laura.]])
		fnDialogue([[Thought: What I thought was a casual greeting practically makes Laura jump out of her skin. [ITALICS]oops[ENDITALICS].]])
		fnDialogue([[Laura: H-h-hey! Ahem. Hi, [PLAYERNAME]. S-sorry I'm so nervous right now. *hrm* *hnn*. I - I just don't really know... what's going on???]])
		fnDialogue([[Laura: And I want to see Adam, but I don't really want to see him... a-and, er, I'm also... trying to turn this computer on, for some reason???]])
		fnDialogue([[Player: Yeah. I get it--and don't worry, Laura! I'm gonna do whatever I can to help out!]])
		fnDialogue([[Laura: Oh, good. *ahem* glad to hear it. Thanks.]])
		fnDialogue([[Player: ...]])
		fnDialogue([[Laura: ...]])
		fnDialogue([[Player: [SFX|World|Ugh]... hm...]])
		fnDialogue([[Laura: DON'T!!!]])
		fnDialogue([[Thought: Now it's my turn to be severely startled.]])
		fnDialogue([[Player: Wh-haah! What? What's wrong?]])
		fnDialogue([[Laura: D-on't touch t-that wire! You might cause an electrical outage.]])
		fnDialogue([[Player: Right. Sorry.]])
		fnDialogue([[Laura: NOT THAT ONE EITHER!!!]])
		fnDialogue([[Player: Ah! Sorry!]])
		fnDialogue([[Thought: Laura watches me like a hawk, her brow furrowing with every move I make towards the computers. I suppose she can't help it--I have no idea what I'm doing.]])
		fnDialogue([[Player: ... You know what, I'm gonna go over here. Uh, away from the computers.]])
		fnDialogue([[Laura: --well, i-if you think that's best.]])
		fnDialogue([[Player: Mmm. But... I'm still here for [ITALICS]you[ENDITALICS], okay?]])
		fnDialogue([[Laura: ... Y-yeah. Thanks, [PLAYERNAME].]])
        
        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
    
        --Variable gain.
        local iLauraFriend  = VM_GetVar("Root/Variables/System/Romance/iLauraFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iLauraFriend", "N", iLauraFriend + 5.0)
	
        --Time loss.
        if(fnDecrementTime(5, sAutoAdvance) == false) then
            PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
        end
		
	--Repeat passes.
	else
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
        
		fnDialogue([[[HIDEALLFAST][SHOWCHAR|Laura]Player: How you doing, Laura?]])
		fnDialogue([[Laura: j-just fine! I'm sure we'll come up with something.]])
        
        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
	end
	
--[ ==================================== Object Presentation ==================================== ]
elseif(sTopicName == "KolaVault") then
    fnStdSequence(gciInv4B_KolaVaula, 
    {"[HIDEALLFAST][SHOWCHAR|Laura]Thought: Laura: [E|Sad]I've never tried to buy a soda, myself.[P] *hrm* just the th-thought of being pranked makes me ill."})

elseif(sTopicName == "SnackStock") then
    fnStdSequence(gciInv4B_SnackStock, 
    {"[HIDEALLFAST][SHOWCHAR|Laura]Laura: [E|N]I like cheez doodles the most, but they're rarely in stock.",
     "Ross: [E|N]*crnch* *mnch* oh yeah. They're our most popular item, by far.",
     "Thought: [ITALICS]You're eating most of them right now![ENDITALICS]"})

elseif(sTopicName == "AirConditioning") then
    fnStdSequence(gciInv4B_TheAC, 
    {"[HIDEALLFAST][SHOWCHAR|Laura]Thought: Laura: [E|Happy]The best temperature for computers is cold and dry.[P] But it's a-awful for human skin. ",
     "Terry: [E|Happy]Tell me about it![E|Shock] In the winter my elbows flake up like a fucking french pastry."})

elseif(sTopicName == "CleaningSupplies") then
    fnStdSequence(gciInv4B_JanitorBits, 
    {"[HIDEALLFAST][SHOWCHAR|Laura]Laura: [E|Mad]Oh dear.[P] I sh-shudder to think what kind of bacterial communities have made their home in that bucket of water."})

elseif(sTopicName == "Yamamoto") then
    fnStdSequence(gciInv4B_Yamamoto, 
    {"[HIDEALLFAST][SHOWCHAR|Laura]Laura: [E|N]I've heard of Mr. Yamamoto here and there. I think he was part of the old computer lab.[P] I wonder what happened to him?"})

elseif(sTopicName == "NewComputer") then
    fnStdSequence(gciInv4B_NewComputer, 
    {"[HIDEALLFAST][SHOWCHAR|Laura]Laura: y-yes?",
     "Player: Anything I can do to help?",
     "Laura: err... y-you're already doing it!",
     "Thought: Just as I thought--I'm totally useless right now."})

elseif(sTopicName == "BiminiManual") then
    fnStdSequence(gciInv4B_LinuxManual, 
    {"[HIDEALLFAST][SHOWCHAR|Laura]Laura: [E|Shock]Oh my g-goodness! [P]What an incredible find![P][E|Sad] Oof, *ahem*[P] *cough*[P] it's q-quite dusty...",
     "Player: What is it?",
     "Laura: [E|N]This is the user manual for the old school computers.[P] There must a hint in here on how to turn *hrm* the mainframe on.[P][E|Shock] *a--[P][P][CRASH]ACHOO!*",
     "Thought: Maybe I can help Laura read this somehow."})

elseif(sTopicName == "Camera") then
    fnStdSequence(gciInv4B_Camera, 
    {"[HIDEALLFAST][SHOWCHAR|Laura]Thought: There's two cameras in here, neither which appear to be on.",
     "Laura: [E|Blush]not much use, are they?[P] B-being off, and everything.",
     "Player: If they were on, we wouldn't be able to sneak in here.",
     "Laura: [E|N]Hm.[P] Good point!"})

elseif(sTopicName == "Controls") then
    fnStdSequence(gciInv4B_Controls, 
    {"[HIDEALLFAST][SHOWCHAR|Laura]Player: Maybe if I--",
     "Laura: [E|Shock]*squeak*",
     "Player: Ah, i-is that wrong?[P][SFX|Ugh] Sorry!",
     "Laura: [E|Blush]Ahhahh... It's okay, j-[P]just be careful! ",
     "Player: I'm just gonna...[P] stand over in this corner."})

elseif(sTopicName == "Login") then
    fnStdSequence(gciInv4B_Login, 
    {"[HIDEALLFAST][SHOWCHAR|Laura]Laura: [E|Shock]We'll need to enter the right password here,[P] and make sure the mainframe is active and connected so the operating system can communicate with all the towers.",
     "Player: Y-yeah.[P] That's just what I was gonna say."})

--[ ======================================== Characters ========================================= ]
elseif(sTopicName == "Terry") then
    fnStdSequence(gciInv4B_Terry, 
    {"[HIDEALLFAST][SHOWCHAR|Laura]Laura: [E|Blush]It was nice of the twins to let us in here.[P] I *hrm* b-barely even talk to them when I go to the library!",
     "Player: I think they just want to see what happens next.",
     "Laura: [E|Happy]Hah...[P]d-don't we all..."})

elseif(sTopicName == "Ross") then
    fnStdSequence(gciInv4B_Ross, 
    {"[HIDEALLFAST][SHOWCHAR|Laura]Laura: [E|Blush]It was nice of the twins to let us in here.[P] I *hrm* b-barely even talk to them when I go to the library!",
     "Player: I think they just want to see what happens next.",
     "Laura: [E|Happy]Hah...[P]d-don't we all..."})

elseif(sTopicName == "Laura") then
    fnStdSequence(gciInv4B_Laura, 
    {"[HIDEALLFAST][SHOWCHAR|Laura]Laura: [E|N]y-yes?",
     "Player: Anything I can do to help?",
     "Laura: [E|Shock]err...[P][E|Happy] y-you're already doing it!",
     "Thought: [ITALICS]Just as I thought--[P][SFX|Ugh]I'm totally useless right now.[ENDITALICS]"})

elseif(sTopicName == "Phone") then
    fnStdSequence(gciInv4B_Phone, 
    {"[HIDEALLFAST][SHOWCHAR|Laura]Laura: [E|N]Feel free to text the club if you're bored."})

end