--[Phone Presentation]
--Showing stuff to *the club*.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--Variables.
local sScriptPath = gsRoot .. "../Chapter 4/Gameplay_SceneC/4 - Present Phone.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
local sAutoAdvance = gsRoot .. "../Chapter 4/Gameplay_SceneD/7 - PatchGoingHome.lua"

--Saving
gsSaveExecute = gsRoot .. "../Chapter 4/Gameplay_SceneA/3 - Bookakke.lua"
gsSaveTopic = "No Topic"
gsSaveFlag = "InvestigationWarehouse"

--[ ========================================= Functions ========================================= ]
--[Friendship Increment]
--If this is the first time presenting this object to this character then we increase the friendship tracker for
-- that character.
local fnIncrementFriendship = function(iObjectIndex, iIncrement)
	
    --Arg check.
    if(iObjectIndex == nil) then return end
    if(iIncrement == nil) then return end
    
    --Build strings.
	local i = iObjectIndex
    local p = iRossIndex
    local sObjectString = string.format("%02i", i)
    local sSubString    = string.format("%02i", p)
    local iIsRepeat = VM_GetVar("Root/Variables/Chapter4/InvestigationB/iObjectPresent|" .. sObjectString .. sSubString, "N")
    if(iIsRepeat == 0.0) then
        VM_SetVar("Root/Variables/Chapter4/InvestigationB/iObjectPresent|" .. sObjectString .. sSubString, "N", 1.0)
    end
end

--[Sequence Handler]
--Almost all sequences can use a standardized template.
local fnStdSequence = function(iFriendshipIndex, saStrings)
    
    --Start.
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    
    for i = 1, #saStrings, 1 do
		fnDialogue(saStrings[i])
    end
    
    --Finish.
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(iFriendshipIndex, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
end

--[ ======================================= Chat Sequences ====================================== ]
--Chatting.
if(sTopicName == "Chat") then
	
	--Variables.
	local iChattedWithPhone = VM_GetVar("Root/Variables/Chapter4/Investigation/iChattedWithPhone", "N")
	
	--First pass:
	if(iChattedWithPhone == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter4/Investigation/iChattedWithPhone", "N", 1.0)
		
		--Text.
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
    
		fnDialogue([[[HIDEALLFAST][SHOWCHAR|Laura]Laura: [E|Sad]I wish...[P]I wish the club was here.]])
		fnDialogue([[Player: Why didn't you bring your walkie talkie today?[P] I bet if the club heard what was going on, they'd be able to help.]])
		fnDialogue([[Laura: [E|Sad]*hrm*,[P][E|Blush] Well, I was embarrassed, to be p-perfectly frank.[P][E|Sad] But I see now that was a bit of a mistake...]])
		fnDialogue([[Thought: Laura suddenly perks.]])
		fnDialogue([[Laura: [E|Mad]Oh, how silly of me![P] I-I still have a cellphone!]])
		fnDialogue([[Thought: She digs in a pocket for it, and pulling it out, stuffs the bulky blue clamshell device into my hands.]])
		fnDialogue([[Thought: I'm kind of impressed by all the trinkets and stickers she's managed to affix to it.]])
		fnDialogue([[Laura: [E|Shock]Brilliant idea, [PLAYERNAME]![P] Okay, you tell them what's going on while I sort through this whole wire mess.]])
		fnDialogue([[Thought: I try to look reassuring, but...[P][SFX|Ugh][ITALICS]I don't know how to use a cellphone![ENDITALICS] ]])
		fnDialogue([[Thought: But of course, there's no way I'm telling Laura that the one thing she's finally entrusted to me is a no-go.]])
		fnDialogue([[Thought: Taking a deep breath, I start pressing random buttons.]])
		fnDialogue([[[HIDEALLFAST]Player: Whew.[P] Okay.[P] That button on the screen looks like \"chat\"... I think.[P] If I press it... No, that didn't work.[P] maybe this???]])
		fnDialogue([[Info: [SFX|Beep]*Bing*]])
		fnDialogue([[Thought: I nearly tap dance with glee when the graphic \"CybaWitch76 has entered the chat\" pops onto the tiny screen.]])
		fnDialogue([[Thought: Not long after, it's flooded with responses.]])
		fnDialogue([[GhostbabeStarKillerXX52: hi hi laura babeeeee!!! we missed u at the meetingggg ;c]])
		fnDialogue([[Player: hey gais. its [PLAYERNAME] aktualee.]])
		fnDialogue([[GhostbabeStarKillerXX52: ONMG HIIII [PLAYERNAME]! XD XD XD Sorry im so random lol. this is mari!]])
		fnDialogue([[Bretmeister aka Bret Numan: *air horn noise* Hey [PLAYERNAME]. Its Bret!]])
		fnDialogue([[iWriteSins666: r u using laura's phone, then? lol.]])
		fnDialogue([[iWriteSins666: its michelle btw]])
		fnDialogue([[Stac333: its so weird seeing u on the other side, [PLAYERNAME]! lol. Como estas? ;)]])
		fnDialogue([[Player: im doeng good. laura ind I are in a warehous. speling is hard.]])
		fnDialogue([[Player: [P]wat does lol mean]])
		fnDialogue([[Stac333: it means \"laugh out loud!\" its what u write when someone says something funny lol.]])
		fnDialogue([[Player: oh.]])
		fnDialogue([[GhostbabeStarKillerXX52: if u need any help from where ur at, just let us know lol! WE ARE HERE 2 SUPPORT UUUUU <3 <3 <3]])
		fnDialogue([[Player: ok. we ar tryeng to turn on a computer.]])
		fnDialogue([[Stac333: have you tried turning it off and on again? LOL!]])
		fnDialogue([[Bretmeister aka Bret Numan: lolololol!!!]])
		fnDialogue([[Michelle: lol]])
		fnDialogue([[GhostbabeStarKillerXX52: XD lawl.]])
		fnDialogue([[Thought: ... Are they actually laughing out loud every time they say that? ]])
		fnDialogue([[Player: I will text you gais if sometheng coms up.]])
		fnDialogue([[GhostbabeStarKillerXX52: u got it! C:]])

        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()

        --Time loss.
        if(fnDecrementTime(5, sAutoAdvance) == false) then
            PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
        end
		
	--Repeat passes.
	else
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()

		fnDialogue([[[HIDEALLFAST]Player: I will text you gais if sometheng coms up.]])
		fnDialogue([[GhostbabeStarKillerXX52: u got it! C:]])

        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
	end
	
--[ ==================================== Object Presentation ==================================== ]
elseif(sTopicName == "KolaVault") then
    fnStdObject(gciInv4B_KolaVaula, 
    {"Player: thers a kane kola valt here.",
     "Bretmeister aka Bret Numan: IS THERE ANY KIND OF SPECIAL EDITION BOTTLES IN THERE?",
     "Player: sorry no.",
     "Bretmeister aka Bret Numan: :( "})

elseif(sTopicName == "SnackStock") then
    fnStdObject(gciInv4B_SnackStock, 
    {"Player: therss som snacks here for the vending mashins.",
     "GhostbabeStarKillerXX52: OOH GET ME SOME SHORTBREAD COOKIES!!! lol ",
     "Bretmeister aka Bret Numan: If there's any extra cheez doodles can you grab me some? If not the nacho triangles are good too. Or if they're out of that I'm also okay with the fruit gummies. ",
     "Stac333: i want cheez doodles 2.",
     "Thought: [ITALICS]I was asking for some kind of help, not taking your orders![ENDITALICS]"})

elseif(sTopicName == "AirConditioning") then
    fnStdObject(gciInv4B_TheAC, 
    {"Player: wat do you gais think off this?",
     "Thought: No response.[P] Maybe they're busy."})

elseif(sTopicName == "CleaningSupplies") then
    fnStdObject(gciInv4B_JanitorBits, 
    {"Player: wat do you gais think off this?",
     "Thought: No response.[P] Maybe they're busy."})

elseif(sTopicName == "Yamamoto") then
    fnStdObject(gciInv4B_Yamamoto, 
    {"Player: Wat do you gais now about Hoshi Yamamoto",
     "iWriteSins666: sounds like a BMX bike 2 me.",
     "Player: its a person",
     "iWriteSins666: okay WEIRDO. its the person who invented the BMX bike then!!!"})

elseif(sTopicName == "NewComputer") then
    fnStdObject(gciInv4B_NewComputer, 
    {"Player: new komputers?",
     "GhostbabeStarKillerXX52: BEEP BEEP BOOP. Imma computer get it? lol",
     "Bretmeister aka Bret Numan: Have you guys seen that new video on youtube? The one about muffins?",
     "iWriteSins666: boo."})

elseif(sTopicName == "BiminiManual") then
    fnStdObject(gciInv4B_LinuxManual, 
    {"Player: wat do you gais think off this?",
     "Thought: No response.[P] Maybe they're busy."})

elseif(sTopicName == "Camera") then
    fnStdObject(gciInv4B_Camera, 
    {"Player: wat do you gais think off this?",
     "Thought: No response.[P] Maybe they're busy."})

elseif(sTopicName == "Controls") then
    fnStdObject(gciInv4B_Controls, 
    {"Player: thers a kontrol panal hir.",
     "iWriteSins666: push all the buttons and see what happens lol",
     "Bretmeister aka Bret Numan: I concur",
     "Stac333: THIRD",
     "GhostbabeStarKillerXX52: THIRD.",
     "GhostbabeStarKillerXX52: Oh dangit stacyyyyyy!",
     "Stac333: <3"})

elseif(sTopicName == "Login") then
    fnStdObject(gciInv4B_Login, 
    {"Player: Thers a login scrin hir",
     "GhostbabeStarKillerXX52: a login screen? Hm...",
     "Bretmeister aka Bret Numan: Try \"Password\" :D",
     "Thought: I type it in.",
     "Player: didnt work.",
     "Bretmeister aka Bret Numan: Aw man. ",
     "iWriteSins666: try BretIsCool459",
     "Bretmeister aka Bret Numan: HOW DO YOU KNOW MY EMAIL PASSWORD MICHELLE?!?!",
     "iWriteSins666: ;P"})

--[ ======================================== Characters ========================================= ]
elseif(sTopicName == "Terry") then
    fnStdObject(gciInv4B_Terry, 
    {"Player: Wat do you gais no abaut the libreriens?",
     "GhostbabeStarKillerXX52: theyre TWINS!",
     "Bretmeister aka Bret Numan: I haven't had too many interactions with them. Didn't they design our club poster?",
     "Stac333: yeppp. 1 of them anyway."})

elseif(sTopicName == "Ross") then
    fnStdObject(gciInv4B_Ross, 
    {"Player: Wat do you gais no abaut the libreriens?",
     "GhostbabeStarKillerXX52: theyre TWINS!",
     "Bretmeister aka Bret Numan: I haven't had too many interactions with them. Didn't they design our club poster?",
     "Stac333: yeppp. 1 of them anyway."})

elseif(sTopicName == "Laura") then
    fnStdObject(gciInv4B_Laura, 
    {"iWriteSins666: how is laura dong? ",
     "iWriteSins666: DOING.",
     "Player: doing ok. Looks kaind off nervus.",
     "GhostbabeStarKillerXX52: dont give up! help her get that computer on plxxxx! "})

elseif(sTopicName == "Phone") then
    fnStdObject(gciInv4B_Phone, 
    {"Player: oof, typing on this tiny keyboard is a million times harder than a regular computer one."})

end