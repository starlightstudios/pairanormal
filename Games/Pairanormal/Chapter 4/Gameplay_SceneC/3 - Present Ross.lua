--[Ross Presentation]
--Showing stuff to Ross.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--Variables.
local sScriptPath = gsRoot .. "../Chapter 4/Gameplay_SceneC/3 - Present Ross.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
local sAutoAdvance = gsRoot .. "../Chapter 4/Gameplay_SceneD/7 - PatchGoingHome.lua"

--Saving
gsSaveExecute = gsRoot .. "../Chapter 4/Gameplay_SceneA/3 - Bookakke.lua"
gsSaveTopic = "No Topic"
gsSaveFlag = "InvestigationWarehouse"

--[ ========================================= Functions ========================================= ]
--[Friendship Increment]
--If this is the first time presenting this object to this character then we increase the friendship tracker for
-- that character.
local fnIncrementFriendship = function(iObjectIndex, iIncrement)
	
    --Arg check.
    if(iObjectIndex == nil) then return end
    if(iIncrement == nil) then return end
    
    --Build strings.
	local i = iObjectIndex
    local p = iRossIndex
    local sObjectString = string.format("%02i", i)
    local sSubString    = string.format("%02i", p)
    local iIsRepeat = VM_GetVar("Root/Variables/Chapter4/InvestigationB/iObjectPresent|" .. sObjectString .. sSubString, "N")
    if(iIsRepeat == 0.0) then
        VM_SetVar("Root/Variables/Chapter4/InvestigationB/iObjectPresent|" .. sObjectString .. sSubString, "N", 1.0)
    end
end

--[Sequence Handler]
--Almost all sequences can use a standardized template.
local fnStdSequence = function(iFriendshipIndex, saStrings)
    
    --Start.
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    
    for i = 1, #saStrings, 1 do
		fnDialogue(saStrings[i])
    end
    
    --Finish.
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(iFriendshipIndex, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
end

--[ ======================================= Chat Sequences ====================================== ]
--Chatting.
if(sTopicName == "Chat") then
	
	--Variables.
	local iChattedWithTerry = VM_GetVar("Root/Variables/Chapter4/Investigation/iChattedWithTerry", "N")
	
	--First pass:
	if(iChattedWithTerry == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter4/Investigation/iChattedWithTerry", "N", 1.0)
		
		--Text.
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
    
		fnDialogue([[[HIDEALLFAST][SHOWCHAR|Terry][SHOWCHAR|Ross]Terry: [E|Mad][CRASH]AAHHH![P] What!?!?]])
		fnDialogue([[Player: [CRASH]AHHHH!!!]])
		fnDialogue([[Ross: [E|Blush]Whoah, Whoah, whoaaaaah![P][E|N] Take it easy, will you sis? They were talking to me.]])
		fnDialogue([[Terry: [E|Shock]Were not!]])
		fnDialogue([[Ross: [E|Shock]I think so.]])
		fnDialogue([[Player: I was talking to--[P]well, it doesn't really matter. I'm sorry to have startled you, Terry.]])
		fnDialogue([[Terry: [E|Sad]Ugh, yeah, sorry.[P] I'm not a huge fan of confined spaces.]])
		fnDialogue([[Player: Why not?]])
		fnDialogue([[Terry: [E|N]Because--]])
		fnDialogue([[Ross: [E|Sad]She's just not.]])
		fnDialogue([[Terry: [P][P][E|Sad]... Yeah.[GLITCH] ]])
		fnDialogue([[[BG|Root/Images/Backgrounds/All/Hallway][Music|Null][HIDEALLFAST]Thought: [GLITCH][GLITCH][GLITCH]A volley[GLITCH] of painful memories violently intrudes my[BG|ComputerCloset] mind.]])
		fnDialogue([[Thought: It is so forceful,[GLITCH] it takes all my willpower not to[GLITCH] collapse.]])
		fnDialogue([[Player: G-[GLITCH]guhhh...]])
		fnDialogue([[Terry: [BG|Hallway]Whoah,[GLITCH][GLITCHTEXT] are you okay?]])
		fnDialogue([[Ross: [GLITCH]Take a seat bro, [GLITCH]chill out. Hang on, let me get you some c[GLITCH]hips to munch on.[ENDGLITCHTEXT] ]])
		fnDialogue([[Player: I'm [GLITCHTEXT]FFFF[ENDGLITCHTEXT]fine. I]])
		fnDialogue([[[HIDEALLFAST][BG|Null][BG|Null]Thought: [P][P][P]...]])
		fnDialogue([[[BG|Root/Images/Backgrounds/All/Hallway][MUSIC|Somber][CHARENTER|Teacher2][CHARENTER|Ross]Teacher: [FOCUS|Teacher2][VOICE|Voice|AdultF]--can't [GLITCH]have these kinds of rumors spreading around.]])
		fnDialogue([[Player: [VOICE|Voice|Terry]But it really did happen!]])
		fnDialogue([[Ross: [E|Sad]Teach.[P] All due respect.[P] Why would both of us lie about something like this?]])
		fnDialogue([[Teacher: [FOCUS|Teacher2][VOICE|Voice|AdultF]I haven't the faintest clue what's in your heads right now, but I'm laying out the facts.]])
		fnDialogue([[Teacher: [FOCUS|Teacher2][VOICE|Voice|AdultF]The police investigating your claim[P]--and they DID investigate--[P] didn't find [ITALICS]anything[ENDITALICS] corroborating your story.]])
		fnDialogue([[Teacher: [FOCUS|Teacher2][VOICE|Voice|AdultF]If the two of you allow this rumor to persist, you'll have the entire student body in a needless panic.]])
		fnDialogue([[Player: [VOICE|Voice|Terry]They SHOULD be afraid, this could happen to anyone! People aren't SAFE out there.[P] As for the police--]])
		fnDialogue([[Teacher: [FOCUS|Teacher2][VOICE|Voice|AdultF]--And what's more, the story just doesn't make any [ITALICS]sense.[ENDITALICS] ]])
		fnDialogue([[Teacher: [FOCUS|Teacher2][VOICE|Voice|AdultF]You both admit there's no motive,[P] and you can't explain how you came to be standing before us here, completely unharmed.]])
		fnDialogue([[Thought: [VOICE|Voice|Terry]I'm forced to pause.]])
		fnDialogue([[Thought: [VOICE|Voice|Terry]What a wretched world we live in, that surviving something so traumatic without a scratch on me is somehow considered [ITALICS]bad[ENDITALICS] luck.]])
		fnDialogue([[Thought: [VOICE|Voice|Terry]But this is the single most frightening truth of all;[P][P][P] I really can't explain how we escaped.]])
		fnDialogue([[Thought: [VOICE|Voice|Terry]One minute, we were snatched off the sidewalk and thrown into that awful place...[P]and the next, Ross and I woke up on our front lawn. ]])
		fnDialogue([[[CHARENTER|Teacher3]Arroyo: [VOICE|Voice|AdultF][FOCUS|Teacher3]Terry,[P] Ross.[P][P][P] I understand that it [ITALICS]seems[ENDITALICS] like something real, and I believe something maybe did happen.]])
		fnDialogue([[Arroyo: [VOICE|Voice|AdultF]I've personally invited a special counselor over if there's anything the two of you want to talk about with him. ]])
		fnDialogue([[Thought: [VOICE|Voice|Terry]I'm not [GLITCH]fucking delusional.[P] I'm not.[P][P] [ITALICS]I'm not[ENDITALICS].]])
		fnDialogue([[Thought: [VOICE|Voice|Terry]I look to Ross, hoping to find some comfort in his goofy smile or an exaggerated eye roll.[P] But he's completely distracted.]])
		fnDialogue([[Arroyo: [VOICE|Voice|AdultF]... All I ask is that you not speak about this anymore with your fellow students;[P] it's not conducive to a learning environment.]])
		fnDialogue([[Arroyo: [VOICE|Voice|AdultF]If you do, I'll have to revoke your custodial priveleges.[P] Are we clear?]])
		fnDialogue([[Player: [VOICE|Voice|Terry][P].[P].[P].Yes ma'am.]])
		fnDialogue([[[HIDEALLFAST]Player: [VOICE|Voice|Terry]This is such...[GLITCH] fucking bullshit![P] We have to... there's got to be someone we can tell who'll believe us, right?]])
		fnDialogue([[Player: [VOICE|Voice|Terry][P][P][P]Ross?[P][P][CRASH] Are you even paying attention, you dolt!!?]])
		fnDialogue([[[CHARENTER|Ross]Ross: [P][P][P][E|Sad]Mmh, let's face it, sis.[P] We lost this one.]])
		fnDialogue([[Ross: [E|Shock] I say we cut our losses and... try to move on, somehow.]])
		fnDialogue([[Player: [VOICE|Voice|Terry]Move on?[P] Fucking [ITALICS]move on[ENDITALICS]?[P] How are you not burning with rage right now?]])
		fnDialogue([[Player: [VOICE|Voice|Terry]How do you not want to tear apart everyone in the vicinity limb from limb for being heartless, uncaring monsters?]])
		fnDialogue([[Thought: [VOICE|Voice|Terry]My twin--the only other person who remembers this awful event and therefore the rock of sanity to which I've had to cling--shrugs like I've asked him a math question. ]])
		fnDialogue([[Ross: [E|Sad]I dunno.[P][E|Shock] Being kidnapped and watching your whole life, like, flash before your eyes kinda makes you reevaluate a lot of things.]])
		fnDialogue([[Ross: [E|Sad]I suddenly... don't really care what everyone thinks anymore.]])
		fnDialogue([[Thought: [VOICE|Voice|Terry]When we were kids, my brother and I often played mirror; trying to perfectly match each other's movements because everyone said we looked so alike.]])
		fnDialogue([[Thought: [VOICE|Voice|Terry]Ross's face is drawn and haggard now--[P]is that what I look like, too?]])
		fnDialogue([[Ross: [E|Sad]I kinda don't care about anything, actually.[P] I just want to go home and lie down for a bit.]])
		fnDialogue([[Thought: [VOICE|Voice|Terry]I can only stand and watch him do just that.]])
		fnDialogue([[Thought: [VOICE|Voice|Terry]He shrugs,[GLITCH] turns tail, and disappears out the back entrance.[GLITCH][GLITCH] ]])
		fnDialogue([[[HIDEALL][BG|Null][BG|Null]Thought: [VOICE|Voice|Terry]...]])
		fnDialogue([[[BG|Root/Images/Backgrounds/All/ComputerCloset][Music|Investigation][CHARENTER|Terry][CHARENTER|Ross]Terry:[E|Mad] --smell of cheese puffs will NOT suddenly snap them back to reality.[P][P][E|Shock] Oh wait, yes it did.[P] Huh. ]])
		fnDialogue([[Ross: [E|Happy][ITALICS]Told you[ENDITALICS], haha. Snacking will save us all.[P] Here dude, chow it up.]])
		fnDialogue([[Thought: I find myself pushing off of the wall I had slouched against,[P] drawn to the faint crinkle of a Cheese Puff bag Ross holds out to me.]])
		fnDialogue([[Thought: As I grab a handful, I look at the twins' eager faces.]])
		fnDialogue([[Thought: They're a little eccentric, but I'd never pin them as...[P]whatever they were, just then.]])
		fnDialogue([[Terry: [E|Blush]All right, you've had your munch.[P][E|N] Now hurry up and wrap things up here, please?]])
		fnDialogue([[Player: Oh gosh.[P] Right.[P] Laura.]])
		fnDialogue([[Ross: [E|Happy]Or take all the time you like. More snacks for me.]])
        
        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
	
        --Time loss.
        if(fnDecrementTime(5, sAutoAdvance) == false) then
            PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
        end
		
	--Repeat passes.
	else
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()

		fnDialogue([[[HIDEALLFAST][SHOWCHAR|Ross][SHOWCHAR|Terry]Terry: [FOCUS|Terry][E|Blush]Hurry up and wrap things up here, please?]])
		fnDialogue([[Ross: [E|Happy]Or take all the time you like. More snacks for me.]])
		fnDialogue([[Player: Right.]])

        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
	end
	
--[ ==================================== Object Presentation ==================================== ]
elseif(sTopicName == "KolaVault") then
    fnStdSequence(gciInv4B_KolaVaula, 
    {"[HIDEALLFAST][SHOWCHAR|Ross]Ross: [E|N]Check it out, dude. See the scratch marks all up on that thing?[P] Squirrels go absolutely NUTS for this stuff, for some reason.",
     "Ross: [E|Happy]... Oh, heheh--'nuts'.[P] I didn't even realize I did that."})

elseif(sTopicName == "SnackStock") then
    fnStdSequence(gciInv4B_SnackStock, 
    {"[HIDEALLFAST][SHOWCHAR|Ross]Ross: [E|Happy] I think if I could get a job involving snacks somehow, for the rest of my life, I'd be okay."})

elseif(sTopicName == "AirConditioning") then
    fnStdSequence(gciInv4B_TheAC, 
    {"[HIDEALLFAST][SHOWCHAR|Ross]Ross: [E|N]I like it hot in the classrooms, personally.[P][E|Mad] But teachers keep it kind of chilly so kids stay awake."})

elseif(sTopicName == "CleaningSupplies") then
    fnStdSequence(gciInv4B_JanitorBits, 
    {"[HIDEALLFAST][SHOWCHAR|Ross]Ross: [E|Shock]I tried emptying the water out of that bucket once, and then a week later it was full again.[P][E|N] So basically I just gave up on that one."})

elseif(sTopicName == "Yamamoto") then
    fnStdSequence(gciInv4B_Yamamoto, 
    {"[HIDEALLFAST][SHOWCHAR|Ross]Ross: [E|Shock]Oh, Hoshi. I think that guy was in the school's original computer club.[P] There's a picture in the library with that name on it.",
     "Player: How did he die?",
     "Ross: [E|N]Beats me. "})

elseif(sTopicName == "NewComputer") then
    fnStdSequence(gciInv4B_NewComputer, 
    {"[HIDEALLFAST][SHOWCHAR|Ross]Ross: [E|N]*crnch* *crnch* what's that? [P]You say something, dude?"})

elseif(sTopicName == "BiminiManual") then
    fnStdSequence(gciInv4B_LinuxManual, 
    {"[HIDEALLFAST][SHOWCHAR|Ross]Ross: [E|Happy]Dude, isn't it wild that people had to like, [ITALICS]read whole books[ENDITALICS] just to learn how to use a computer? ",
     "Thought: He flips to the first page.",
     [[Ross: [E|Sad]\"AMNLOP 10188\" ... Oof, I'm already tired of this.[P][E|Happy] Heh.]]})

elseif(sTopicName == "Camera") then
    fnStdSequence(gciInv4B_Camera, 
    {"[HIDEALLFAST][SHOWCHAR|Ross]Ross: [E|N]Cameras are really cool.[P] They work a lot like our eyes do, in a kind of robot-y way."})

elseif(sTopicName == "Controls") then
    fnStdSequence(gciInv4B_Controls, 
    {"[HIDEALLFAST][SHOWCHAR|Ross]Ross: [E|Happy]You'd be surprised how many things I've fixed by just poking at stuff until it works!"})

elseif(sTopicName == "Login") then
    fnStdSequence(gciInv4B_Login, 
    {"[HIDEALLFAST][SHOWCHAR|Ross]Ross: [E|Happy]You'd be surprised how many things I've fixed by just poking at stuff until it works!"})

--[ ======================================== Characters ========================================= ]
elseif(sTopicName == "Terry") then
    fnStdSequence(gciInv4B_Terry, 
    {"[HIDEALLFAST][SHOWCHAR|Ross]Player: Your sister seems particularly on edge.",
     "Ross: [E|Sad]Ehh, yeah.[P] Being in enclosed spaces isn't really... our jam."})

elseif(sTopicName == "Ross") then
    fnStdSequence(gciInv4B_Ross, 
    {"[HIDEALLFAST][SHOWCHAR|Ross]Ross: [E|N]*crnch* *crnch* what's that?[P] You say something, dude?"})

elseif(sTopicName == "Laura") then
    fnStdSequence(gciInv4B_Laura, 
    {"[HIDEALLFAST][SHOWCHAR|Ross]Ross: [E|Blush]Oh man, I really hope Miss Hackathon finds her dream dude.[P] Sending good vibes, for sure."})

elseif(sTopicName == "Phone") then
    fnStdSequence(gciInv4B_Phone, 
    {"[HIDEALLFAST][SHOWCHAR|Ross]Ross: [E|Blush]Oh dude, you want my number?[P] Gosh, I'm flattered. Don't know what to say.",
     "Player: It's not my phone, it's Laura's!",
     "Ross: [E|Shock]Ohhhhh.[P][P][P][E|Sad] Now I really don't know what to say--I'm very confused."})

end