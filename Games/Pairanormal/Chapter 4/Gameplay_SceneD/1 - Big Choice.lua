-- |[Walking Home]|
--Ballspgglrr

-- |[Arg Resolve]|
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

-- |[It's like a book! Wait I suck at reading!]|
if(sTopicName == "ReadMind") then
    
    --Variables.
    local sBuddyName = VM_GetVar("Root/Variables/System/Romance/sBuddy", "S")
    
    fnDialogue("Thought: I briefly wonder if the person in the photograph was also able to read minds--if they had to keep it a secret from all their friends.")
    
    -- |[Buddy Sequence]|
    if(sBuddyName == "Mari") then
        fnDialogue("Thought: I look to Mari's disbelieving eyes[P]--can I really do this in front of everyone?[P] What if I pass out!?!")
        fnDialogue("Player: Mari[GLITCH][P][P][P]... Catch me if I fall, okay?")
        fnDialogue("Mari: If you [GLITCH][ITALICS]fall[ENDITALICS]?[P] What are [GLITCH][GLITCHTEXT]you talking about?WHYDIDYOULIEWHYDIDYOULIE[ENDGLITCHTEXT] ")
        
    elseif(sBuddyName == "Stacy") then
        fnDialogue("Thought: I look to Stacy's disbelieving eyes[P]--can I really do this in front of everyone?[P] What if I pass out!?!")
        fnDialogue("Player: Stacy[GLITCH][P][P][P]... Catch me if I fall, okay?")
        fnDialogue("Stacy: If you [GLITCH][ITALICS]fall[ENDITALICS]?[P] What are [GLITCH][GLITCHTEXT]you talking about?WHYDIDYOULIEWHYDIDYOULIE[ENDGLITCHTEXT] ")
        
    elseif(sBuddyName == "Bret") then
        fnDialogue("Thought: I look to Bret's disbelieving eyes[P]--can I really do this in front of everyone?[P] What if I pass out!?!")
        fnDialogue("Player: Bret[GLITCH][P][P][P]... Catch me if I fall, okay?")
        fnDialogue("Bret: If you [GLITCH][ITALICS]fall[ENDITALICS]?[P] What are [GLITCH][GLITCHTEXT]you talking about?WHYDIDYOULIEWHYDIDYOULIE[ENDGLITCHTEXT] ")
        
    elseif(sBuddyName == "Todd") then
        fnDialogue("Thought: I look to Todd's disbelieving eyes[P]--can I really do this in front of everyone?[P] What if I pass out!?!")
        fnDialogue("Player: Todd[GLITCH][P][P][P]... Catch me if I fall, okay?")
        fnDialogue("Todd: If you [GLITCH][ITALICS]fall[ENDITALICS]?[P] What are [GLITCH][GLITCHTEXT]you talking about?WHYDIDYOULIEWHYDIDYOULIE[ENDGLITCHTEXT] ")
        
    elseif(sBuddyName == "Michelle") then
        fnDialogue("Thought: I look to Michelle's disbelieving eyes[P]--can I really do this in front of everyone?[P] What if I pass out!?!")
        fnDialogue("Player: Michelle[GLITCH][P][P][P]... Catch me if I fall, okay?")
        fnDialogue("Michelle: If you [GLITCH][ITALICS]fall[ENDITALICS]?[P] What are [GLITCH][GLITCHTEXT]you talking about?WHYDIDYOULIEWHYDIDYOULIE[ENDGLITCHTEXT] ")
        
    elseif(sBuddyName == "Laura") then
        fnDialogue("Thought: I look to Laura's disbelieving eyes[P]--can I really do this in front of everyone?[P] What if I pass out!?!")
        fnDialogue("Player: Laura[GLITCH][P][P][P]... Catch me if I fall, okay?")
        fnDialogue("Laura: If you [GLITCH][ITALICS]fall[ENDITALICS]?[P] What are [GLITCH][GLITCHTEXT]you talking about?WHYDIDYOULIEWHYDIDYOULIE[ENDGLITCHTEXT] ")
    end
    
    fnDialogue("[HIDEALL][BG|Null][Music|Somber][SHOWCHAR|PlayerFlash][PLAYERNAME]: [VOICE|Voice|Player][CRASH] WHY [GLITCH]DID YOU LIE TO ME!?![P] ANSWER ME, GODDAMMIT!")
    fnDialogue("Candace: [VOICE|Voice|Candace]Look, [PLAYERNAME]...[P] it's [PLAYERNAME], right?[P] I didn't lie to you.[P] I really don't know who you are.")
    fnDialogue("Candace: [VOICE|Voice|Candace]But I am [ITALICS]not[ENDITALICS] your mother.")
    fnDialogue([[ [BG|Root/Images/Backgrounds/All/CandaceLivingRoom]Thought: My security guard gives me a look-- a \"do you want me to deal with this?\" look, but I wave him off.]])
    fnDialogue("Thought: This was definitely not how I wanted to start the week.")
    fnDialogue("Thought: I notice the scuffs on the stranger's jeans,[P] the toes of the shoe all but worn off,[P] the unkempt hair.")
    fnDialogue("Thought: [ITALICS]Dear god, how long has this child been living on the streets?[ENDITALICS] ")
    fnDialogue("Candace: [VOICE|Voice|Candace]Sweetheart, when was the last time you--")
    fnDialogue([[ [PLAYERNAME]: [VOICE|Voice|Player][EMOTION|PlayerFlash|Mad]Don't you \"sweetheart\" me, okay lady?[P] I came here for ANSWERS. I've been fending for myself for 14 years.]])
    fnDialogue("[PLAYERNAME]: [VOICE|Voice|Player]Meanwhile you're out here, sitting pretty,[P] without a care in the world.[P] I don't want anything from you except for you to start tal--")
    fnDialogue("Candace: That's [CRASH][ITALICS]quite[ENDITALICS] enough!")
    fnDialogue("[MUSIC|Theme]Thought: Never thought I'd be dusting off my teacher voice, the one that held classes at attention back in the day.")
    fnDialogue("Thought: The young upstart immediately shuts their mouth, looking sullenly at the ground.")
    fnDialogue("Thought: Part of me wonders if I became a teacher because I never could be a mother. [P][ITALICS]Best to push that thought away for the moment.[ENDITALICS] ")
    fnDialogue("Candace: [VOICE|Voice|Candace]All right.[P] You want answers.[P] Well, take a seat then, I have a story to tell you.")
    fnDialogue("Thought: It feels like a standoff of wills, but I try to remain calm.[P] Eventually they concede, but insist on sitting in Jonathan's chair tucked away in the corner.")
    fnDialogue("Thought: It takes everything in me not to protest, but I can't disrupt whatever tenuous trust is in the room.")
    fnDialogue("Thought: My bodyguard's eye the scruffy teenager suspiciously.[P][ITALICS] Do they really have to be here for this?[ENDITALICS] ")
    fnDialogue("Candace: [VOICE|Voice|Candace]Rodolf, can't you order something for us? A[P]... cheeseburger, or something.[P] God, I've just realized it's been ages since I had one of those.")
    fnDialogue("Thought: I smile at the child, but they're focused on the photos on my wall.[P] The urn underneath Jonathan's photo.")
    fnDialogue("[PLAYERNAME]: [VOICE|Voice|Player][EMOTION|PlayerFlash|N]Who's that?")
    fnDialogue("Candace: [VOICE|Voice|Candace]That was my fiancee.[P] He passed away.")
    fnDialogue("Candace: [VOICE|Voice|Candace]A[P][P]... rogue gunman came into town and he was caught in the crossfire.")
    fnDialogue("Thought: The trauma bubbles up like it happened yesterday.[P] I clamp my throat shut--I certainly don't have the strength to explain what [ITALICS]really[ENDITALICS] happened. ")
    fnDialogue("Candace: [VOICE|Voice|Candace]And I had a child.[P] Jonathan's child.")
    fnDialogue("Candace: [VOICE|Voice|Candace]But... they were only around for about a week.[P] I watched them[P]... die.")
    fnDialogue("Candace: [VOICE|Voice|Candace]I[P]... h-[P]held that baby in my arms...")
    fnDialogue("Thought: [PLAYERNAME]'s shoulders rise, and they look to me with the tiniest bit of panic.")
    fnDialogue("Thought: And in that moment[P]--they do so look like Jonathan.[P] But it's not possible of course. [P]It's not.")
    fnDialogue("Thought: [ITALICS]Get it together, Candace. Do NOT break down.[ENDITALICS] ")
    fnDialogue("Candace: [VOICE|Voice|Candace] *ahem*")
    fnDialogue("[PLAYERNAME]: [VOICE|Voice|Player] [EMOTION|PlayerFlash|N]... Sorry. ")
    fnDialogue("Candace: [VOICE|Voice|Candace] It's all right.[P] It's all right [ITALICS]now,[ENDITALICS] that I've had a few years to... live with it.")
    fnDialogue("Candace: [VOICE|Voice|Candace] But you see, I never tried to start a family again.[P] So I'm afraid what you're claiming just can't be possible.")
    fnDialogue("[PLAYERNAME]: [VOICE|Voice|Player] [EMOTION|PlayerFlash|N]... this doesn't make any sense.[P] I was *born* in that hospital.")
    fnDialogue("[PLAYERNAME]: [VOICE|Voice|Player] [CRASH] My records say so! And it says...[P][EMOTION|PlayerFlash|N] that you're my mother.")
    fnDialogue("Thought: They hand me a couple of sheets of paper that, judging by what's on them,[P] should have been taken much better care of.")
    fnDialogue("Thought: A birth certificate, and some documents--confirming the story.[P] I try to analyze the sullen child as closely as the papers they gave me.")
    fnDialogue("Thought: I don't see how it's possible, but--")
    fnDialogue("Candace: [VOICE|Voice|Candace] Huh.[P] Well, if it's a forgery, it's not a cheap one.")
    fnDialogue("[PLAYERNAME]: [VOICE|Voice|Player] [P][P][EMOTION|PlayerFlash|Mad][CRASH]HOW DARE YOU!?[P] Do I look like some kind of criminal to you!?")
    fnDialogue("Candace: [VOICE|Voice|Candace] I-I'm sorry,[P] I didn't mean--")
    fnDialogue("[PLAYERNAME]: [VOICE|Voice|Player] The only thing I've ever even been able to afford is the bus that took me to this stupid,")
    fnDialogue("[PLAYERNAME]: [VOICE|Voice|Player] Middle of nowhere town to try and find out why my stupid, middle of nowhere mother would ABANDON me!")
    fnDialogue("Candace: [VOICE|Voice|Candace] You're right, that was rude of me.[P] I-- this is all rather a [ITALICS]surprise,[ENDITALICS] you have to admit!")
    fnDialogue("Candace: [VOICE|Voice|Candace] [CRASH] I don't know what to do!")
    fnDialogue("[PLAYERNAME]: [VOICE|Voice|Player] [EMOTION|PlayerFlash|N]...")
    fnDialogue("Candace: [VOICE|Voice|Candace] [P][P]Look, I think...[P][P] I think we should call a case worker and get you back to--")
    fnDialogue("[PLAYERNAME]: [VOICE|Voice|Player] [EMOTION|PlayerFlash|N]Please!")
    fnDialogue("Thought: They reach across the table and snatch my hands. The wild anger is gone,[P] and all I can see through the matt of dark curls is two clear, frightened eyes.")
    fnDialogue("[PLAYERNAME]: [VOICE|Voice|Player] [EMOTION|PlayerFlash|N]Please don't send me back. [P]I hate it there.")
    fnDialogue("[PLAYERNAME]: [VOICE|Voice|Player] Please, I don't want to go back! I'm begging you.[P] There's[GLITCH] nobody there who cares about me.[P][EMOTION|PlayerFlash|Mad] [GLITCH][GLITCH]Nobody!")

    fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Flashback\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
    PairanormalLevel_FlagCheckpoint("Flashback")
    
    fnDialogue("[HIDEALL][BG|Root/Images/Backgrounds/All/SidewalkNight]Thought: ...")
    
    --[Buddy Sequence]
    if(sBuddyName == "Mari") then
        fnDialogue("[CHARENTER|Mari]Mari: --are you talking about?")
        
    elseif(sBuddyName == "Stacy") then
        fnDialogue("[CHARENTER|Stacy]Stacy: --are you talking about?")
        
    elseif(sBuddyName == "Bret") then
        fnDialogue("[CHARENTER|Bret]Bret: --are you talking about?")
        
    elseif(sBuddyName == "Todd") then
        fnDialogue("[CHARENTER|Todd]Todd: --are you talking about?")
        
    elseif(sBuddyName == "Michelle") then
        fnDialogue("[CHARENTER|Michelle]Michelle: --are you talking about?")
        
    elseif(sBuddyName == "Laura") then
        fnDialogue("[CHARENTER|Laura]Laura: --are you talking about?")
    end

    fnDialogue("Thought: I blink away the bitterness in the corner of my eye which threatens to become a tear.")
    fnDialogue("Thought: That was intense[P]... but I didn't pass out this time!")
    fnDialogue("Player: N-nothing.[P] I just...")
    fnDialogue("[CHARENTER|Candace]Candace: [E|Sad][PLAYERNAME].[P] I am offering you a chance to leave this place.[P] No-- [E|Mad]I am literally begging you.")
    fnDialogue("Thought: Miss Foster's hand is still outstretched.[P][P] I really have to make a choice now.")
    fnDialogue("Info: One of the following options leads to a game ending.")
    fnDialogue("Info: You won't be able to go back and choose again, so pick carefully!")

    -- |[Common]|
    fnDialogue("Thought: (What do I do?)")

    local sDecisionScript = "\"" .. LM_GetCallStack(0) .. "\""
    fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Leave with Miss Foster\", " .. sDecisionScript .. ", \"GetOuttaDodge\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Stay in Foster Falls\", " .. sDecisionScript .. ", \"StayInTown\") ")
    fnCutsceneBlocker()

    
-- |[RUN AND DON'T LOOK BACK]|
elseif(sTopicName == "GetOuttaDodge") then

    --Variables.
    local sBuddyName = VM_GetVar("Root/Variables/System/Romance/sBuddy", "S")
    
    fnDialogue("Player: .[P].[P].[P] okay.")
    
    --[Buddy Sequence]
    if(sBuddyName == "Mari") then
        fnDialogue("Mari: [E|Shock]Okay?[P] Okay, [ITALICS]what[ENDITALICS]?")
        fnDialogue("Candace: [E|Sad]Thank you.[P] Oh, thank you [PLAYERNAME]!")
        fnDialogue("Thought: Something deep inside me, something poor and sad and desperate, but true, reaches for Candace Foster's hand.")
        fnDialogue("Thought: It's the only hand that's ever offered me something different, the chance to be free[P]--when everyone else--")
        fnDialogue("Mari: [E|Shock]Miss Foster.[P] You can't just [CRASH]GO!?[P][E|Sad] [PLAYERNAME]!!")
        fnDialogue("Thought: --has asked me to just accept things as they are.")
        fnDialogue("Thought: I shut my eyes tight, holding back any outbursts or tears.[P] It feels crazy, but it's a chance.[P] One I might never have again.")
        fnDialogue("Thought: I step into the car and flinch as Mari bangs on the door feverishly.")
        fnDialogue("Mari: Wait![P] [PLAYERNAME], please wait!!!!")
        fnDialogue("Mari: [CRASH]DON'T GO!!!!")
        
    elseif(sBuddyName == "Stacy") then
        fnDialogue("Stacy: [E|Shock]Okay?[P] Okay, [ITALICS]what[ENDITALICS]?")
        fnDialogue("Candace: [E|Sad]Thank you.[P] Oh, thank you [PLAYERNAME]!")
        fnDialogue("Thought: Something deep inside me, something poor and sad and desperate, but true, reaches for Candace Foster's hand.")
        fnDialogue("Thought: It's the only hand that's ever offered me something different, the chance to be free[P]--when everyone else--")
        fnDialogue("Stacy: [E|Shock]Miss Foster.[P] You can't just [CRASH]GO!?[P][E|Sad] [PLAYERNAME]!!")
        fnDialogue("Thought: --has asked me to just accept things as they are.")
        fnDialogue("Thought: I shut my eyes tight, holding back any outbursts or tears.[P] It feels crazy, but it's a chance.[P] One I might never have again.")
        fnDialogue("Thought: I step into the car and flinch as Stacy bangs on the door feverishly.")
        fnDialogue("Stacy: Wait![P] [PLAYERNAME], please wait!!!!")
        fnDialogue("Stacy: [CRASH]DON'T GO!!!!")
        
    elseif(sBuddyName == "Bret") then
        fnDialogue("Bret: [E|Shock]Okay?[P] Okay, [ITALICS]what[ENDITALICS]?")
        fnDialogue("Candace: [E|Sad]Thank you.[P] Oh, thank you [PLAYERNAME]!")
        fnDialogue("Thought: Something deep inside me, something poor and sad and desperate, but true, reaches for Candace Foster's hand.")
        fnDialogue("Thought: It's the only hand that's ever offered me something different, the chance to be free[P]--when everyone else--")
        fnDialogue("Bret: [E|Shock]Miss Foster.[P] You can't just [CRASH]GO!?[P][E|Sad] [PLAYERNAME]!!")
        fnDialogue("Thought: --has asked me to just accept things as they are.")
        fnDialogue("Thought: I shut my eyes tight, holding back any outbursts or tears.[P] It feels crazy, but it's a chance.[P] One I might never have again.")
        fnDialogue("Thought: I step into the car and flinch as Bret bangs on the door feverishly.")
        fnDialogue("Bret: Wait![P] [PLAYERNAME], please wait!!!!")
        fnDialogue("Bret: [CRASH]DON'T GO!!!!")
        
    elseif(sBuddyName == "Todd") then
        fnDialogue("Todd: [E|Shock]Okay?[P] Okay, [ITALICS]what[ENDITALICS]?")
        fnDialogue("Candace: [E|Sad]Thank you.[P] Oh, thank you [PLAYERNAME]!")
        fnDialogue("Thought: Something deep inside me, something poor and sad and desperate, but true, reaches for Candace Foster's hand.")
        fnDialogue("Thought: It's the only hand that's ever offered me something different, the chance to be free[P]--when everyone else--")
        fnDialogue("Todd: [E|Shock]Miss Foster.[P] You can't just [CRASH]GO!?[P][E|Sad] [PLAYERNAME]!!")
        fnDialogue("Thought: --has asked me to just accept things as they are.")
        fnDialogue("Thought: I shut my eyes tight, holding back any outbursts or tears.[P] It feels crazy, but it's a chance.[P] One I might never have again.")
        fnDialogue("Thought: I step into the car and flinch as Todd bangs on the door feverishly.")
        fnDialogue("Todd: Wait![P] [PLAYERNAME], please wait!!!!")
        fnDialogue("Todd: [CRASH]DON'T GO!!!!")
        
    elseif(sBuddyName == "Michelle") then
        fnDialogue("Michelle: [E|Shock]Okay?[P] Okay, [ITALICS]what[ENDITALICS]?")
        fnDialogue("Candace: [E|Sad]Thank you.[P] Oh, thank you [PLAYERNAME]!")
        fnDialogue("Thought: Something deep inside me, something poor and sad and desperate, but true, reaches for Candace Foster's hand.")
        fnDialogue("Thought: It's the only hand that's ever offered me something different, the chance to be free[P]--when everyone else--")
        fnDialogue("Michelle: [E|Shock]Miss Foster.[P] You can't just [CRASH]GO!?[P][E|Sad] [PLAYERNAME]!!")
        fnDialogue("Thought: --has asked me to just accept things as they are.")
        fnDialogue("Thought: I shut my eyes tight, holding back any outbursts or tears.[P] It feels crazy, but it's a chance.[P] One I might never have again.")
        fnDialogue("Thought: I step into the car and flinch as Michelle bangs on the door feverishly.")
        fnDialogue("Michelle: Wait![P] [PLAYERNAME], please wait!!!!")
        fnDialogue("Michelle: [CRASH]DON'T GO!!!!")
        
    elseif(sBuddyName == "Laura") then
        fnDialogue("Laura: [E|Shock]Okay?[P] Okay, [ITALICS]what[ENDITALICS]?")
        fnDialogue("Candace: [E|Sad]Thank you.[P] Oh, thank you [PLAYERNAME]!")
        fnDialogue("Thought: Something deep inside me, something poor and sad and desperate, but true, reaches for Candace Foster's hand.")
        fnDialogue("Thought: It's the only hand that's ever offered me something different, the chance to be free[P]--when everyone else--")
        fnDialogue("Laura: [E|Shock]Miss Foster.[P] You can't just [CRASH]GO!?[P][E|Sad] [PLAYERNAME]!!")
        fnDialogue("Thought: --has asked me to just accept things as they are.")
        fnDialogue("Thought: I shut my eyes tight, holding back any outbursts or tears.[P] It feels crazy, but it's a chance.[P] One I might never have again.")
        fnDialogue("Thought: I step into the car and flinch as Laura bangs on the door feverishly.")
        fnDialogue("Laura: Wait![P] [PLAYERNAME], please wait!!!!")
        fnDialogue("Laura: [CRASH]DON'T GO!!!!")
    end
    
    fnDialogue("[HIDEALL][BG|Null]Thought: ...")

    fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"ThisIsTheEnd\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
    PairanormalLevel_FlagCheckpoint("ThisIsTheEnd")
    
    fnDialogue("[MUSIC|Somber]Thought: Out the window, I watch the streets I've just started to recognize disappear into the distance; Our car is leaving Foster Falls.")
    fnDialogue("Thought: [BG|Root/Images/Backgrounds/All/Car]Candace Foster has her arms wrapped tightly around me.[P] The tears from her eyes make my shoulder damp.[P] The woman is shaking with sobs.")
    fnDialogue("[CHARENTER|Candace]Candace: [E|Mad]I didn't know about you before.[P] [ITALICS]I swear, I didn't know![ENDITALICS] ")
    fnDialogue("Thought: She pulls away, poorly wiping the stinging salt from her eyes.")
    fnDialogue("Candace: [E|Sad]You came to me almost a year ago, a-and I turned you away.[P][E|Shock] You came to my house telling me the truth--you were an orphan.")
    fnDialogue("Candace: [E|Mad] But I didn't believe you![P][P][P] I'd lost my first child, you see.")
    fnDialogue("Candace: [E|Shock]Then the next day... you ran away.[P] Left a note saying you were going to find out the truth.[P] I n-never saw you again.[P] Until now.")
    fnDialogue("Player: ... So,[P] I'm not really your kid.")
    fnDialogue("Candace: [E|Shock]But you [ITALICS]are[ENDITALICS], [PLAYERNAME]!")
    fnDialogue("Candace: [E|Sad] When you left, there were some hair strands on your bed.")
    fnDialogue("Candace: I had it sent--[E|N]I thought maybe I could help you find who you were looking for[P][E|Shock]--but it was [ITALICS]me[ENDITALICS]. ")
    fnDialogue("Candace: We were a [ITALICS]match[ENDITALICS].[P] I couldn't explain[E|N]--I don't care, to be perfectly honest--! ")
    fnDialogue("Thought: Her cries are briefly interrupted by a hiccup.")
    fnDialogue("Thought: Covering her mouth, she blushes, reaching for a box of tissue buried under her seat.")
    fnDialogue("Candace: [E|Sad]... But by then you were already gone.[P][P][E|Mad] And something [ITALICS]happened[ENDITALICS] to you.")
    fnDialogue("Candace: I don't understand [ITALICS]how you don't remember this,[ENDITALICS] but I'm damn sure who's responsible!")
    fnDialogue("Thought: She tears out a fresh tissue with such fervor that it rips,[P] sending tiny white flying particles scattering in the stale car air.")
    fnDialogue([[Candace: [E|Mad][CRASH]THAT WOMAN, [PLAYERNAME].[P] That awful woman you call a \"caretaker\".]])
    fnDialogue("Candace: She's been lying to you this whole time.[P] It's part of her long and horrid campaign to destroy the Foster legacy!")
    fnDialogue("Candace: Her organization has burned down buildings,[P] totaled businesses, and tried to kill every living Foster for [ITALICS]years[ENDITALICS].")
    fnDialogue("Thought: Something lurches in my ribs.[P] I don't wan't to believe it, but based on what little I've seen...")
    fnDialogue("Thought: Well, I don't have anything that convinces me the story is wrong.")
    fnDialogue("Player: Miss Foster...[P] do you happen to know...[P] [ITALICS]why[ENDITALICS] Eilette is doing all this to you?")

    fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"ButWhy\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
    PairanormalLevel_FlagCheckpoint("ButWhy")
    
    fnDialogue("Candace: [E|Sad]... It happened to my father, a long time ago[P]--Dad never spoke of it much;[P] I can tell it was fairly painful. ")
    fnDialogue("Thought: There's a lost look in Miss Foster's eyes that strikes me as familiar[P]--a look I've seen in the mirrors a time or two. ")
    fnDialogue("Candace: [E|Sad]From what I've put together, I think maybe she and him were business partners, and something went wrong...")
    fnDialogue("Candace: *sigh*,[P] He must have hurt her.[P][E|N] I get the sense, in all the things he did in this town, he was trying to atone.")
    fnDialogue("Candace: He was such a good man, I never saw him treat people with anything but the kindest love!")
    fnDialogue("Candace: [E|Mad] Whatever he did, it never deserved this kind of retaliation.[P] The burnings, the buyouts, the accidents[P][P]... the shootings.")
    fnDialogue("Thought: She grasps my hands tightly.[P] I hope she can't feel how clammy they are.")
    fnDialogue("Candace: [E|Mad]I had to watch my fiancee die.[P] I had to lose everyone I loved.")
    fnDialogue("Candace: This town is poison, and I won't stand for it anymore.[P] I won't let anyone hurt the one miraculously good thing that has happened to me.")
    fnDialogue("Thought: I can't help but flinch as she puts her hand on my cheek.")
    fnDialogue("Candace: [E|Sad]I'm sorry.[P][P] You're probably not used to that.")
    fnDialogue("Guard: Miss Foster--we've arrived at the airport.")
    fnDialogue("Candace: [E|Shock]Oh! O-okay. *whew*. [P]Get the tickets, and have our bags shipped to the address I'll text you.")
    fnDialogue("Candace: [E|N]*sigh*,[P] Are you ready, [PLAYERNAME]?[P] It's time to leave this nightmare behind once and for all.")
    fnDialogue("Thought: All the confusion, the sleepless nights, the questions, the lies.")
    fnDialogue("Thought: Of course I am ready to leave it all behind.[P] I don't want to be miserable anymore.")
    fnDialogue("Thought: I want to lead my own life, and start fresh, and not be weighed down by the past. ")
    fnDialogue("Thought: Staring at the eager hand Miss Foster offers me, I wonder if she can really do all of those things.[P] I guess I have to take that chance.")
    fnDialogue("[HIDEALL][BG|Null][MUSIC|Null]Thought: [P][P][P][P][P]I just wish I could've taken my friends with me. ")

    fnDialogue([[Credits: [P][P][P][Voice|Null][BG|Null][Music|Theme]The End]])
    fnDialogue([[Thought: ...]])

    fnDialogue([[Credits: [P][P][P]Developed by Mike \"Salty Justice\" Upshall and Tess \"Chicmonster\" Young (@thechicmonster)]])
    fnDialogue("Thought: [P][P][P]So, this is the end, huh?")
    fnDialogue("Thought: I hope it didn't come at you too abruptly.")
    fnDialogue("Thought: I've been working on this game for four years.")
    fnDialogue([[Credits: [P][P][P]Music by Mag.Lo (maglo.bandcamp.com)]])
    fnDialogue("Thought: And not just [ITALICS]any four years.")
    fnDialogue("Thought: I'm talking the years of 2016 to 2020. Look it up, man. It was a lot.")
    fnDialogue([[Credits: [P][P][P]Additional score by Bensound.com]])
    fnDialogue("Thought: I'm literally not the same person I was when I started making this. Nobody is!")
    fnDialogue([[Credits: [P][P][P]Special thanks to Mari and Stacy of Geek Remix (youtube.com/user/GeekRemix)]])
    fnDialogue("Thought: Fun fact, all of the people this game was partially based on are no longer gaming youtubers.")
    fnDialogue([[Credits: [P][P][P]Special thanks to Laura Kate Dale (@LauraKBuzz)]])
    fnDialogue("Thought: Not gonna lie, I was hoping to cash in on their quasi-internet-fame at first, but I sort of shot myself in the foot there, didn't I?")
    fnDialogue([[Credits: [P][P][P]Special thanks to Todd Schlickbernd (@Toddly_Enough)]])
    fnDialogue("Thought: Then I realized, well heck, if nobody is going to play this game because of who is in it,")
    fnDialogue([[Credits: [P][P][P]Special thanks to Bret aka HoodiePanda (youtube.com/user/HoodiePanda)]])
    fnDialogue("Thought: In fact there's a very low chance anyone is going to play it at all,")
    fnDialogue([[Credits: [P][P][P]Special thanks to Michelle aka MangaMinx (youtube.com/user/TheRPGMinx)]])
    fnDialogue("Thought: I might as well make it about what's important to me.")
    fnDialogue("Credits: [P][P][P] In memory of dad and Sam")
    fnDialogue("Thought: So thanks to all the friends and family members who are there for me. This game exists because of you.")
    fnDialogue("Thought: And thanks to you for playing this thing! It's not much, but believe me when I say I put my whole heart into it.")
    fnDialogue("Thought: Okay, have a good life![P] I'm going to send you back to the menu.")
    fnDialogue("Thought:[P][P][P](...[P][ITALICS][SFX|World|Ugh]Now that this is done, what am I going to do with all my free time???[ENDITALICS])")
    fnDialogue([[ [TOMAINMENU] ]])

-- |[Watch me take on two evil organizations by myself!]|
elseif(sTopicName == "StayInTown") then
    fnDialogue("[MUSIC|Null][HIDEALL]Player: [P].[P].[P].[P]I'm sorry, but I can't.[P] This place is my home now.")
    
    --Variables.
    local sBuddyName = VM_GetVar("Root/Variables/System/Romance/sBuddy", "S")
    
    --[Buddy Sequence]
    if(sBuddyName == "Mari") then
        fnDialogue("Thought: I find myself leaning towards Mari's shoulder.[P] There's a rush of warmth to my chest as she grabs my arm protectively.")
        fnDialogue("[MUSIC|Null][HIDEALL]Player: [P].[P].[P].[P]I'm sorry, but I can't.[P] This place is my home now.")
        fnDialogue("[CHARENTER|Candace]Candance: [E|Sad]...")
        fnDialogue("Thought: Mari's gaze meets mine. In that moment, I know the feelings I've had for a long time[P]... are reciprocated.[P] It [ITALICS]wasn't[ENDITALICS] just my imagination.")
        fnDialogue("Thought: It's a little scary, but thrilling.[P] What do we do now?")
        fnDialogue("Candace: [E|Sad]I-I'm sorry.[P][P] I just can't accept that.")
        fnDialogue("Thought: Hesitating for a moment, she raises her arm.[P] The driver and a man brandishing a gun step out of the long, black car.")
        fnDialogue("[CHARENTER|Thug1][CHARENTER|Thug2][MUSIC|Tension]Thought: They move towards me, and the hairs on my neck jolt to attention.")
        fnDialogue("Player: [P][P]S-stop![P] Don't get any closer.")
        fnDialogue("Candace: [E|Mad]This is our [ITALICS]only[ENDITALICS] chance, [PLAYERNAME]!!![P][E|Shock] I don't know where that woman is...[P][CRASH]we have to leave before she stops us!")
        fnDialogue("Thought: I would have run, but with Mari beside me, we can only stand and stare;[P] each person hoping the other comes up with a plan.")

    elseif(sBuddyName == "Stacy") then
        fnDialogue("Thought: I find myself leaning towards Stacy's shoulder.[P] There's a rush of warmth to my chest as she grabs my arm protectively.")
        fnDialogue("[MUSIC|Null][HIDEALL]Player: [P].[P].[P].[P]I'm sorry, but I can't.[P] This place is my home now.")
        fnDialogue("[CHARENTER|Candace]Candance: [E|Sad]...")
        fnDialogue("Thought: Stacy's gaze meets mine. In that moment, I know the feelings I've had for a long time[P]... are reciprocated.[P] It [ITALICS]wasn't[ENDITALICS] just my imagination.")
        fnDialogue("Thought: It's a little scary, but thrilling.[P] What do we do now?")
        fnDialogue("Candace: [E|Sad]I-I'm sorry.[P][P] I just can't accept that.")
        fnDialogue("Thought: Hesitating for a moment, she raises her arm.[P] The driver and a man brandishing a gun step out of the long, black car.")
        fnDialogue("[CHARENTER|Thug1][CHARENTER|Thug2][MUSIC|Tension]Thought: They move towards me, and the hairs on my neck jolt to attention.")
        fnDialogue("Player: [P][P]S-stop![P] Don't get any closer.")
        fnDialogue("Candace: [E|Mad]This is our [ITALICS]only[ENDITALICS] chance, [PLAYERNAME]!!![P][E|Shock] I don't know where that woman is...[P][CRASH]we have to leave before she stops us!")
        fnDialogue("Thought: I would have run, but with Stacy beside me, we can only stand and stare;[P] each person hoping the other comes up with a plan.")
        
    elseif(sBuddyName == "Bret") then
        fnDialogue("Thought: I find myself leaning towards Bret's shoulder.[P] There's a rush of warmth to my chest as she grabs my arm protectively.")
        fnDialogue("[MUSIC|Null][HIDEALL]Player: [P].[P].[P].[P]I'm sorry, but I can't.[P] This place is my home now.")
        fnDialogue("[CHARENTER|Candace]Candance: [E|Sad]...")
        fnDialogue("Thought: Bret's gaze meets mine. In that moment, I know the feelings I've had for a long time[P]... are reciprocated.[P] It [ITALICS]wasn't[ENDITALICS] just my imagination.")
        fnDialogue("Thought: It's a little scary, but thrilling.[P] What do we do now?")
        fnDialogue("Candace: [E|Sad]I-I'm sorry.[P][P] I just can't accept that.")
        fnDialogue("Thought: Hesitating for a moment, she raises her arm.[P] The driver and a man brandishing a gun step out of the long, black car.")
        fnDialogue("[CHARENTER|Thug1][CHARENTER|Thug2][MUSIC|Tension]Thought: They move towards me, and the hairs on my neck jolt to attention.")
        fnDialogue("Player: [P][P]S-stop![P] Don't get any closer.")
        fnDialogue("Candace: [E|Mad]This is our [ITALICS]only[ENDITALICS] chance, [PLAYERNAME]!!![P][E|Shock] I don't know where that woman is...[P][CRASH]we have to leave before she stops us!")
        fnDialogue("Thought: I would have run, but with Bret beside me, we can only stand and stare;[P] each person hoping the other comes up with a plan.")
        
    elseif(sBuddyName == "Todd") then
        fnDialogue("Thought: I find myself leaning towards Todd's shoulder.[P] There's a rush of warmth to my chest as she grabs my arm protectively.")
        fnDialogue("[MUSIC|Null][HIDEALL]Player: [P].[P].[P].[P]I'm sorry, but I can't.[P] This place is my home now.")
        fnDialogue("[CHARENTER|Candace]Candance: [E|Sad]...")
        fnDialogue("Thought: Todd's gaze meets mine. In that moment, I know the feelings I've had for a long time[P]... are reciprocated.[P] It [ITALICS]wasn't[ENDITALICS] just my imagination.")
        fnDialogue("Thought: It's a little scary, but thrilling.[P] What do we do now?")
        fnDialogue("Candace: [E|Sad]I-I'm sorry.[P][P] I just can't accept that.")
        fnDialogue("Thought: Hesitating for a moment, she raises her arm.[P] The driver and a man brandishing a gun step out of the long, black car.")
        fnDialogue("[CHARENTER|Thug1][CHARENTER|Thug2][MUSIC|Tension]Thought: They move towards me, and the hairs on my neck jolt to attention.")
        fnDialogue("Player: [P][P]S-stop![P] Don't get any closer.")
        fnDialogue("Candace: [E|Mad]This is our [ITALICS]only[ENDITALICS] chance, [PLAYERNAME]!!![P][E|Shock] I don't know where that woman is...[P][CRASH]we have to leave before she stops us!")
        fnDialogue("Thought: I would have run, but with Todd beside me, we can only stand and stare;[P] each person hoping the other comes up with a plan.")
        
    elseif(sBuddyName == "Michelle") then
        fnDialogue("Thought: I find myself leaning towards Michelle's shoulder.[P] There's a rush of warmth to my chest as she grabs my arm protectively.")
        fnDialogue("[MUSIC|Null][HIDEALL]Player: [P].[P].[P].[P]I'm sorry, but I can't.[P] This place is my home now.")
        fnDialogue("[CHARENTER|Candace]Candance: [E|Sad]...")
        fnDialogue("Thought: Michelle's gaze meets mine. In that moment, I know the feelings I've had for a long time[P]... are reciprocated.[P] It [ITALICS]wasn't[ENDITALICS] just my imagination.")
        fnDialogue("Thought: It's a little scary, but thrilling.[P] What do we do now?")
        fnDialogue("Candace: [E|Sad]I-I'm sorry.[P][P] I just can't accept that.")
        fnDialogue("Thought: Hesitating for a moment, she raises her arm.[P] The driver and a man brandishing a gun step out of the long, black car.")
        fnDialogue("[CHARENTER|Thug1][CHARENTER|Thug2][MUSIC|Tension]Thought: They move towards me, and the hairs on my neck jolt to attention.")
        fnDialogue("Player: [P][P]S-stop![P] Don't get any closer.")
        fnDialogue("Candace: [E|Mad]This is our [ITALICS]only[ENDITALICS] chance, [PLAYERNAME]!!![P][E|Shock] I don't know where that woman is...[P][CRASH]we have to leave before she stops us!")
        fnDialogue("Thought: I would have run, but with Michelle beside me, we can only stand and stare;[P] each person hoping the other comes up with a plan.")
        
    elseif(sBuddyName == "Laura") then
        fnDialogue("Thought: I find myself leaning towards Laura's shoulder.[P] There's a rush of warmth to my chest as she grabs my arm protectively.")
        fnDialogue("[MUSIC|Null][HIDEALL]Player: [P].[P].[P].[P]I'm sorry, but I can't.[P] This place is my home now.")
        fnDialogue("[CHARENTER|Candace]Candance: [E|Sad]...")
        fnDialogue("Thought: Laura's gaze meets mine. In that moment, I know the feelings I've had for a long time[P]... are reciprocated.[P] It [ITALICS]wasn't[ENDITALICS] just my imagination.")
        fnDialogue("Thought: It's a little scary, but thrilling.[P] What do we do now?")
        fnDialogue("Candace: [E|Sad]I-I'm sorry.[P][P] I just can't accept that.")
        fnDialogue("Thought: Hesitating for a moment, she raises her arm.[P] The driver and a man brandishing a gun step out of the long, black car.")
        fnDialogue("[CHARENTER|Thug1][CHARENTER|Thug2][MUSIC|Tension]Thought: They move towards me, and the hairs on my neck jolt to attention.")
        fnDialogue("Player: [P][P]S-stop![P] Don't get any closer.")
        fnDialogue("Candace: [E|Mad]This is our [ITALICS]only[ENDITALICS] chance, [PLAYERNAME]!!![P][E|Shock] I don't know where that woman is...[P][CRASH]we have to leave before she stops us!")
        fnDialogue("Thought: I would have run, but with Laura beside me, we can only stand and stare;[P] each person hoping the other comes up with a plan.")
    end
 
    --Common:
    fnDialogue("Thought: Before we have any chance, the gunman grabs me.")
    fnDialogue("Player: LET ME GOO!!!! URGGH!!!")

    fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"GunTime\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
    PairanormalLevel_FlagCheckpoint("GunTime")

    if(sBuddyName == "Mari") then
        fnDialogue("Thought: Squirming desperately, I feel a second pair of hands wrapping around my waist,[P] and fear that it might be over for me.")
        fnDialogue("Thought: But when I focus my eyes I see it's not the driver who has grabbed me,[P] but Mari.")
        fnDialogue("Player: Mari, what are you doing!?")
        fnDialogue("Mari: [CHARENTER|Mari][E|Happy]I'm going down with you, buddy![P][E|Mad] If they're gonna take you, they're gonna have to wrestle you out of my [CRASH]POWER HUG!")
        fnDialogue("Thought: The only thing worse than being kidnapped is the thought that Mari has decided to be kidnapped too,[P] just for the heck of it.")
        fnDialogue("Thought: I want to cry and tell her to get to safety, but to my shock[P]--the weight of both of us becomes almost too much for the gunman to handle.")
        fnDialogue("Thought: I can feel his grip slipping.[P] The power hug [ITALICS]worked!?[ENDITALICS] ")
        fnDialogue("Player: Mari... start pulling me towards the woods!![P] Grhhh, I think we can get away.")
        fnDialogue("Mari: [E|Shock]Roger that!!!")
        fnDialogue("Thought: But before we have time to enact our plan, the second man catches up to the squabble.")
        fnDialogue("Thought: With two large body-building brutes, we're suddenly powerless.[P] My heart sinks--is this the end for us?")
        fnDialogue("Player: Mari, I... the thought has just occured to me.[P] *gulp*.")
        fnDialogue("Player: I might never see you again after today.")
        fnDialogue("Mari: [E|Shock][PLAYERNAME][P]... D-don't say tha--")
        fnDialogue("Player: I just want you to know, whatever happens.[P] I liked having you around.[P] I liked [ITALICS]you[ENDITALICS].")
        fnDialogue("Player: I'll never forget being your friend.")

        local iFriendVal = VM_GetVar("Root/Variables/System/Romance/iMariFriend", "N")
        if(iFriendVal >= 30) then
            fnDialogue("Mari: [E|Mad][PLAYERNAME]![P] You don't understand. I... I like... I liked--")
        end
       
    elseif(sBuddyName == "Stacy") then
        fnDialogue("Thought: When I was grabbed, a small, silly part of me looked to Stacy.")
        fnDialogue("Thought: The Paranormal Club co-captain was always so brave and direct[P]... I sort of hoped")
        fnDialogue("Thought: ... She would come to my rescue.[P] Like a knight in shining armor from school storybooks.")
        fnDialogue("Thought: [P][P][P]I have to say, I was not disappointed.")
        fnDialogue("Info: [CRASH]*crack*[P][CRASH] *thwack*[P][CRASH] *SLAM*")
        fnDialogue("Stacy: [CHARENTER|Stacy][E|Mad]One down.[P][E|Happy] Do you want a piece, thug number two?")
        fnDialogue("Thought: The driver probably wasn't expecting such a turn of strength from a spry, red-headed teenager.")
        fnDialogue("Thought: Like a bolt of lightning, she slammed her boots into his knees,[P] then kicked him so hard between his legs he collapsed.")
        fnDialogue("Thought: The gunman lets go of me and backs off, but his arm dangles dangerously steady with that firearm.")
        fnDialogue("Thought: There's a plan clearly forming behind his shaded glasses.")
        fnDialogue("Player: Stacy![P] Are you captain of the karate club or something?")
        fnDialogue("Stacy: [E|Sad]Not this year, nah.")
        fnDialogue("Thought: She crooks her arm around my neck protectively.")
        fnDialogue("Stacy: [E|Mad]That was probably just instinct.")
        fnDialogue("Thought: I wonder if the little thrill shooting down my spine is fear of what's to come...[P]or something else.")
        fnDialogue("Thought: It's funny.[P] I've never been more charmed by Stacy, but right now she couldn't be more serious.")
        fnDialogue("Thought: We steel ourselves for the second round, as the fallen thug slowly regains his footing.[P] This time, I knew, they wouldn't underestimate my protector.")
        fnDialogue("Player: Stacy, I... the thought has just occured to me.[P] *gulp*.")
        fnDialogue("Player: I might never see you again after today.")
        fnDialogue("Stacy: [E|Shock][PLAYERNAME][P]... D-don't say tha--")
        fnDialogue("Player: I just want you to know, whatever happens.[P] I liked having you around.[P] I liked [ITALICS]you[ENDITALICS].")
        fnDialogue("Player: I'll never forget being your friend.")

        local iFriendVal = VM_GetVar("Root/Variables/System/Romance/iStacyFriend", "N")
        if(iFriendVal >= 30) then
            fnDialogue("Stacy: [E|Mad][PLAYERNAME]![P] You don't understand. I... I like... I liked--")
        end

    elseif(sBuddyName == "Bret") then
        fnDialogue("Bret: [CHARENTER|Bret][E|Shock][PLAYERNAME], h-hang on!")
        fnDialogue("Thought: The moment that Miss Foster's gunman lays hands on me, Bret springs into action like never before.")
        fnDialogue("Thought: He charges him, awkward arms spinning wildly like a walloping pinwheel of death.")
        fnDialogue("Bret: [E|Mad][CRASH]LET!!![P][CRASH] GO!!![P][CRASH] OF!!![P][CRASH] MY!!! FRIEND!!!!")
        fnDialogue("Player: Ouch!")
        fnDialogue("Bret: [E|Shock]Sorry![P][E|Blush] You guys are kind of tangled up together...")
        fnDialogue("Thought: Despite my violent thrashing and Bret's best efforts, we're quickly overtaken.")
        fnDialogue("Thought: The driver grabs him and pulls him away, and little by little, I'm dragged closer to the car.")
        fnDialogue("Player: NO!!!")
        fnDialogue("Bret: [E|Sad][PLAYERNAME]!!!")
        fnDialogue("Player: Bret, I... the thought has just occured to me.[P] *gulp*.")
        fnDialogue("Player: I might never see you again after today.")
        fnDialogue("Bret: [E|Shock][PLAYERNAME][P]... D-don't say tha--")
        fnDialogue("Player: I just want you to know, whatever happens.[P] I liked having you around.[P] I liked [ITALICS]you[ENDITALICS].")
        fnDialogue("Player: I'll never forget being your friend.")

        local iFriendVal = VM_GetVar("Root/Variables/System/Romance/iBretFriend", "N")
        if(iFriendVal >= 30) then
            fnDialogue("Bret: [E|Mad][PLAYERNAME]![P] You don't understand. I... I like... I liked--")
        end
        
    elseif(sBuddyName == "Todd") then
        fnDialogue("[CHAREXIT|Todd]Thought: It's mad--even though I'm being dragged to my doom, some corner of my conciousness searches for my friend,[P] hoping he's okay.")
        fnDialogue("Player: [CRASH]TODD![P] TODD, GET TO SAFETY!!!")
        fnDialogue("Thought: [P][P]I don't see him anywhere.[P] My heart sinks, but... it's for the best.[P][P] He got away.")
        fnDialogue("[CHARENTER|Todd]Todd: [E|Shock][CRASH]YYYEAEAARARAAAAAGGGGHHH!!!")
        fnDialogue("Thought: Out of nowhere, Todd dives in between my attacker and me, waving the biggest tree branch I've ever seen [P]-- where did he get that thing!?!")
        fnDialogue("Thought: The splintery tendrils bat annoyingly in my face.")
        fnDialogue("Player: WH-what are you doing!?")
        fnDialogue("Todd: [E|Mad]It's called a DISTRACTION!!!")
        fnDialogue("Thought: I don't believe it  at first, but the bulky man who grabbed me does loosen his grip, now occupied with Todd's brandished branch.")
        fnDialogue("Thought: I don't need a second hint--time to wriggle to freedom!")
        fnDialogue("Todd: [E|Shock]Hey--")
        fnDialogue("Thought: The driver enters the fight, wrestling the branch out of Todd's hands.")
        fnDialogue("Thought: Panic rises in me like never before. Now we're done for.")
        fnDialogue("Player: Todd, I... the thought has just occured to me.[P] *gulp*.")
        fnDialogue("Player: I might never see you again after today.")
        fnDialogue("Todd: [E|Shock][PLAYERNAME][P]... D-don't say tha--")
        fnDialogue("Player: I just want you to know, whatever happens.[P] I liked having you around.[P] I liked [ITALICS]you[ENDITALICS].")
        fnDialogue("Player: I'll never forget being your friend.")

        local iFriendVal = VM_GetVar("Root/Variables/System/Romance/iToddFriend", "N")
        if(iFriendVal >= 30) then
            fnDialogue("Todd: [E|Mad][PLAYERNAME]![P] You don't understand. I... I like... I liked--")
        end

    elseif(sBuddyName == "Michelle") then
        fnDialogue("Thought: The moment the gunman lays his hands on me, Michelle springs into action like never before.")
        fnDialogue("Michelle: [CHARENTER|Michelle][E|Mad][CRASH]GRRRRA[GLITCH]AAARGH!!!")
        fnDialogue("Thought: She socks the driver square in the gut.[P] This grown man collapses on the floor like a sack of potatoes.")
        fnDialogue("Thought: Then she makes a beeline for Miss Foster.")
        fnDialogue("Michelle: [E|Sad][ITALICS]Pricks[ENDITALICS].[P][E|Mad] Do you have [ITALICS]any[ENDITALICS] idea who you're messing with?")
        fnDialogue("Candace: [E|Mad]G-get away from[P][P][E|Shock]--agh!")
        fnDialogue("Thought: She seizes the school councilwoman by the hips and lifts her clear of the ground.")
        fnDialogue("Thought: Miss Foster is so startled at the sheer strength, she can only gasp indignantly.")
        fnDialogue("Thought: The gunman raises his weapon, as if to remind her that he has it.[P] Michelle pays no mind.")
        fnDialogue("Michelle: [E|Blush]I'm gonna count to three.[P][E|Mad] If you don't get your grubby men off of [PLAYERNAME] right now, I'm gonna break your legs.")
        fnDialogue("Thought: The woman being hoisted up doesn't really believe the claim, but I can see the rage in Michelle's eyes.[P] It's terrifying, but it might be my only hope.")
        fnDialogue("Candace: [E|Sad]Stay out of this, young lady![P] I am trying to help them, can't you see!?")
        fnDialogue("Candace: If you resort to violence, you'll be arrested for assault.")
        fnDialogue("Michelle: [E|Mad]Well whaddaya bloody know?[P] Finally, I can live up to my school counselor's expectations!")
        fnDialogue("Player: Michelle, stop! Don't hurt anyone.[P] Please, they'll put you in jail!")
        fnDialogue("Thought: Michelle glares at me, almost annoyed that I'm interfering in my own rescue.")
        fnDialogue("Thought: Part of me can't believe it either, pleading even as I'm being dragged away.")
        fnDialogue("Player: If you get in trouble because of me, I'll never be able to forgive myself.")
        fnDialogue("Michelle: [E|Sad][CRASH]You expect me to just let them take you away!?")
        fnDialogue("Player: You've got to get out of here. You're in danger!")
        fnDialogue("Michelle: [E|Mad]WHAT is wrong with you.[P] You stupid...[E|Blush]dumb stupid irritating bloody cockhead! I can't just leave you here.")
        fnDialogue("Michelle: [E|Sad] I[P]--without you,[P][P] I...")
        fnDialogue("Player: Michelle, I... the thought has just occured to me.[P] *gulp*.")
        fnDialogue("Player: I might never see you again after today.")
        fnDialogue("Michelle: [E|Shock][PLAYERNAME][P]... D-don't say tha--")
        fnDialogue("Player: I just want you to know, whatever happens.[P] I liked having you around.[P] I liked [ITALICS]you[ENDITALICS].")
        fnDialogue("Player: I'll never forget being your friend.")

        local iFriendVal = VM_GetVar("Root/Variables/System/Romance/iMichelleFriend", "N")
        if(iFriendVal >= 30) then
            fnDialogue("Michelle: [E|Mad][PLAYERNAME]![P] You don't understand. I... I like... I liked--")
        end

    elseif(sBuddyName == "Laura") then
        fnDialogue("Thought: I don't see my surroundings very clearly once the men start to drag me away.")
        fnDialogue("Thought: Little flashes of asphalt, a scraped knee--who knows who it belongs to.")
        fnDialogue("Thought: Then, for one glorious second I feel the strong hold on me release.[P] I drop to the ground.")
        fnDialogue("Thought: The gunman yelps, and a small pebble clatters beside me.")
        fnDialogue("Thought: Laura stands on the banks of a hill, cell phone in one hand, the other wrapped tightly around a light pole.")
        fnDialogue("Thought: She's shaking so hard the whole beam she's attached to wobbles.")
        fnDialogue("Laura: [CHARENTER|Laura][E|Mad]L-[P]let [PLAYERNAME] go at once![P] I-I-I'm filming this! I'm...")
        fnDialogue("Laura: [E|Sad] I'm filming this! I'm filming...[P]Erm.")
        fnDialogue("Laura: [E|Mad]If you don't drive a-away this INSTANT[P]... I-I'm f--[P]gonna post this to the internet! ")
        fnDialogue("Player: Laura, just run! ")
        fnDialogue("Thought: I scramble to my feet.[P] My desperate cries are enough to snap her out of her fearful trance.")
        fnDialogue("Thought: Throwing one last pebble at my attackers for good measure, she turns tail for the woods.")
        fnDialogue("Laura: [CRASH][E|Shock]Ack!")
        fnDialogue("Thought: [SFX|Ugh]--and almost immediately falls flat on her face.")
        fnDialogue("Thought: My heart nearly stops when I feel hands on me again.[P] Is this the end?")
        fnDialogue("Player: Laura, I... the thought has just occured to me.[P] *gulp*.")
        fnDialogue("Player: I might never see you again after today.")
        fnDialogue("Laura: [E|Shock][PLAYERNAME][P]... D-don't say tha--")
        fnDialogue("Player: I just want you to know, whatever happens.[P] I liked having you around.[P] I liked [ITALICS]you[ENDITALICS].")
        fnDialogue("Player: I'll never forget being your friend.")

        local iFriendVal = VM_GetVar("Root/Variables/System/Romance/iLauraFriend", "N")
        if(iFriendVal >= 30) then
            fnDialogue("Laura: [E|Mad][PLAYERNAME]![P] You don't understand. I... I like... I liked--")
        end
    end

    fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"DragRace\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
    PairanormalLevel_FlagCheckpoint("DragRace")
    
    --Common:
    fnDialogue("[HIDEALL][BG|Null][Music|Null]Thought: I don't see my surroundings very clearly once the men start to drag me away.")
    fnDialogue("Info: [P][P][P][P][P][CRASH][SFX|CarScreech]SCREEEEEEEEEEEEEEEEEEEEEEEEEE")
    fnDialogue("[BG|Root/Images/Backgrounds/All/SidewalkNight]Thought: Off in the distance, the sound of rubber ripping through cement reverberates down the road where we are.[P] A boxy dark vehicle follows. [MUSIC|Tension] ")
    fnDialogue("Thought: Tearing at us like a hungry demon, the car is otherwise pretty non-descript.[P] I recognize it right away.")
    fnDialogue("Thought: [P][P]That's Eilette's car.")
    fnDialogue("Info: [CRASH]CRASH!")
    fnDialogue("Thought: It swerves to a stop just beside us, narrowly clipping the headlights of Miss Kane's sleek ride.")
    fnDialogue("Thought: Like little jack-in-the-box clowns, Reich and Leif spring from the doors, guns drawn.")
    fnDialogue("[CHARENTER|Reich][CHARENTER|Leif]Reich: [E|Happy]Hiya [PLAYERNAME]!")
    fnDialogue("Leif: [E|Blush]Sorry we're late.[P] I lost the car keys for a bit.[P] But it's okay--")
    fnDialogue("Player: ooof!")
    fnDialogue("Thought: Miss Kane's driver and gunman drop everything (including me, apparently)[P] and brace themselves for this new threat. ")
    fnDialogue("Twins: [FOCUS|Reich][FOCUSMOD|Leif|true][EMOTION|Reich|Happy][EMOTION|Leif|Happy]The Frog Society is here!")

    if(sBuddyName == "Mari") then
        fnDialogue("Thought: Not wanting to be caught in whatever is about to go down, I scramble for Mari,[P] and we get the hell out of the way.")
        fnDialogue("Thought: Finally crouched behind Eilette's car, my companion all but rattles me.")
        fnDialogue("[CHARENTER|Mari]Mari: [E|Mad]Oh my [ITALICS]god[ITALICS]. WHAT is happening--a-are they gonna start [ITALICS]shooting[ITALICS]!?!")
        
    elseif(sBuddyName == "Stacy") then
        fnDialogue("Thought: Not wanting to be caught in whatever is about to go down, I scramble for Stacy,[P] and we get the hell out of the way.")
        fnDialogue("Thought: Finally crouched behind Eilette's car, my companion all but rattles me.")
        fnDialogue("[CHARENTER|Stacy]Stacy: [E|Mad]Oh my [ITALICS]god[ITALICS]. WHAT is happening--a-are they gonna start [ITALICS]shooting[ITALICS]!?!")
        
    elseif(sBuddyName == "Bret") then
        fnDialogue("Thought: Not wanting to be caught in whatever is about to go down, I scramble for Bret,[P] and we get the hell out of the way.")
        fnDialogue("Thought: Finally crouched behind Eilette's car, my companion all but rattles me.")
        fnDialogue("[CHARENTER|Bret]Bret: [E|Mad]Oh my [ITALICS]god[ITALICS]. WHAT is happening--a-are they gonna start [ITALICS]shooting[ITALICS]!?!")
        
    elseif(sBuddyName == "Todd") then
        fnDialogue("Thought: Not wanting to be caught in whatever is about to go down, I scramble for Todd,[P] and we get the hell out of the way.")
        fnDialogue("Thought: Finally crouched behind Eilette's car, my companion all but rattles me.")
        fnDialogue("[CHARENTER|Todd]Todd: [E|Mad]Oh my [ITALICS]god[ITALICS]. WHAT is happening--a-are they gonna start [ITALICS]shooting[ITALICS]!?!")
        
    elseif(sBuddyName == "Michelle") then
        fnDialogue("Thought: Not wanting to be caught in whatever is about to go down, I scramble for Michelle,[P] and we get the hell out of the way.")
        fnDialogue("Thought: Finally crouched behind Eilette's car, my companion all but rattles me.")
        fnDialogue("[CHARENTER|Michelle]Michelle: [E|Mad]Oh my [ITALICS]god[ITALICS]. WHAT is happening--a-are they gonna start [ITALICS]shooting[ITALICS]!?!")
        
    elseif(sBuddyName == "Laura") then
        fnDialogue("Thought: Not wanting to be caught in whatever is about to go down, I scramble for Laura,[P] and we get the hell out of the way.")
        fnDialogue("Thought: Finally crouched behind Eilette's car, my companion all but rattles me.")
        fnDialogue("[CHARENTER|Laura]Laura: [E|Mad]Oh my [ITALICS]god[ITALICS]. WHAT is happening--a-are they gonna start [ITALICS]shooting[ITALICS]!?!")
    end

    fnDialogue("Leif: [Voice|Voice|Reich][E|N]Hopefully not.")
    fnDialogue("Reich: [Voice|Voice|Reich][E|Happy]Yeah, we're mostly the distraction.")
    fnDialogue("Player: Distraction for--")
    fnDialogue("Candace: [CRASH]aYEEEEEEEeEEEE!!!!![HIDEALL] ")
    fnDialogue("Thought: What an awful, blood curling scream. [P]Against my better judgement, I risk a quick peek.")
    fnDialogue("Thought: Miss Kane is pinned against her car, cradling her hand.[P] It's bleeding.[P][P] Eilette is behind her.")
    fnDialogue("[IMAGE|Root/Images/MajorScenes/All/EiletteConfrontation]Eilette: [Voice|Voice|Eilette]I have just introduced a trace amount of Epibatidine into your bloodstream.")
    fnDialogue("Eilette: [Voice|Voice|Eilette]In a few moments, your body will start feeling numb.[P] You have about two hours before full paralysis sets in, and you die of asphyxiation.")
    fnDialogue("Thought: I can't believe how flatly she announces this. Candace shrinks like a helpless, abused animal, trembling under my Eilettes's towering figure. ")
    fnDialogue("Candace: H-h-how could you--")
    fnDialogue("Eilette: [ITALICS]Quiet now.[ENDITALICS] ")
    fnDialogue("Leif: Hey, [PLAYERNAME]. Stay under cover, will you?")
    fnDialogue("Thought: One quick, foreceful push, and Miss Kane tumbles into her car.[P] Eilette leans in, arm raised, eliciting a fearful whimper from her prey.")
    fnDialogue("Thought: Even though I'm not really in danger, I feel I've never been more afraid in my whole life--and no, I refuse to look away.")
    fnDialogue("Thought: Even though I know I'm about to watch Eilette kill someone in cold blood.")
    fnDialogue("[MUSIC|Null]Player: ...")
    fnDialogue("Eilette: [VOICE|Voice|Eilette]...")
    fnDialogue("Candace: [VOICE|Voice|Candace]...")
    fnDialogue("Thought: Nothing happens.[P] Wait... what is that in Eilette's hand?[P] Just... a card.")
    fnDialogue("Thought: A little card with some neatly written words.")
    fnDialogue("[Image|Null][CHARENTER|Eilette]Eilette: [E|N]On this paper, I've written down the name of the toxin, along with its antidote.")
    fnDialogue("Eilette: [E|Happy]I recommend you direct your men to the hospital at once, while you still have the use of your mouth,[P] so that someone can administer it.")
    fnDialogue("Thought: The silence is heavy with indecision. [P]Miss Foster's men are frozen, hands fisted, waiting for anything to set them off.")
    fnDialogue("Thought: Candace's eyes dart from Eilette's unmoving face to me, and to her bleeding arm.[P] Finally, she snatches the paper from her assaulter's hand.")
    fnDialogue("[CHARENTER|Candace]Candace: [E|Mad]D-do as she says![P] Quickly!")
    fnDialogue("Thought: in an instant, the driver and gunman pile into the car and peel off. ")
    fnDialogue("[HIDEALL]Thought: [SFX|CarScreech]...")
    fnDialogue("Player: ... Just like that, huh?")
    fnDialogue("Thought: Candace Foster is gone. As I watch the dust clouds disappear into the street, I can't help but feel like I'll never see her again.")
    fnDialogue("Thought: Eilette's narrow eyes watch the horizon, then calmly and methodically survey the perimiter.")
    fnDialogue("[CHARENTER|Eilette]Eilette: [E|N]Well then.[P] Seven, Eight -- bring the car around, will you?")
    fnDialogue("Twins: [CHARENTER|Reich][CHARENTER|Leif][FOCUS|Reich][FOCUSMOD|Leif|true][EMOTION|Reich|Happy][EMOTION|Leif|Happy][Voice|Voice|Reich]Yes Boss!")
    fnDialogue("Thought: [CHAREXIT|Reich][CHAREXIT|Leif]Her tone has no reflection of the horrible thing that just happened;[P] the woman's shown more excitement shopping for overcoats in a department store!!")
    fnDialogue("Thought: Maybe I should have taken Miss Kane's offer while I still had the chance. ")
    fnDialogue("[HIDEALL][BG|Null][Music|Null]Thought: ...")

    fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"JustLikeThat\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
    PairanormalLevel_FlagCheckpoint("JustLikeThat")
    
    fnDialogue("Thought: Somehow, today's survivors all ended up smooshed in Eilette's car.")
    fnDialogue("Thought: I can't look at anything else except the mud on my shoes.[P][ITALICS] Someone tried to kidnap me.[ENDITALICS] ")
    fnDialogue("Thought: Someone [GLITCH]tried to grab me and hold me [GLITCH]against my will!")
    fnDialogue("Thought: The next touch I feel is so sudden and horrifying I instinctively slap it away.")
    fnDialogue("[Music|Love][BG|Root/Images/Backgrounds/All/Car]Thought: ...")

    if(sBuddyName == "Mari") then
        fnDialogue("[CHARENTER|Mari]Mari: [E|Sad]Owwwiee!")
        fnDialogue("Player: Mari![P] I-I'm so sorry. I didn't mean to--")
        fnDialogue("Mari: [E|N]It's okay, you're just nervous.[P] I was gonna give you a hug, that's all.")
        fnDialogue("Player: I-I want a hug![P] I do!")
        fnDialogue("Thought: Eilette gives me a look from the rearview mirror, and I turn red.[P] But Mari's arms wrap around me anyway.")
        fnDialogue("Mari: [E|Blush]I've got lot's of 'em.[P] SQUEEEEEEEZE!")
        fnDialogue("Player: g-gak... [P]can't... breathe.")
        fnDialogue("Mari: [E|Blush]Feel my healing love![P][E|Shock] FEEL IT.")
        fnDialogue("Thought: She finally lets go, and I laugh with relief.[P] We share a giggle, and a fraction of the tension slides away.")
        fnDialogue("Player: Mari...[P]I was so scared.[P] Thanks for saving me back there.")
        fnDialogue("Mari: [E|N]It's no problem. I did what any friend would do. ")
        fnDialogue("Player: I'm... I'm really glad we're friends!")
        fnDialogue("Mari: [E|Blush]... Me too, [PLAYERNAME]!!")
        fnDialogue("Thought: The car slows to a stop.")
        fnDialogue("[CHARENTER|Eilette]Eilette: [E|N][PLAYERNAME], inform your [ITALICS]friend[ENDITALICS] that we have arrived at her house.[P] Make sure she has all her things.")
        fnDialogue("Thought: Mari steps out, giving me one last reassuring look.[P] The car door starts to close.")
        fnDialogue("Mari: [E|Sad]Everything's gonna be okay, [PLAYERNAME]. ")
        fnDialogue("Player: I know.")
        fnDialogue("Mari: [E|Shock]Hey, wait a minute.[P] How did she know where I liv--")
        fnDialogue("[HIDEALL]Info: [CRASH]*SLAM*")
        
    elseif(sBuddyName == "Stacy") then
        fnDialogue("[CHARENTER|Stacy]Stacy: [E|Shock]Ouch!")
        fnDialogue("Player: Stacy![P] Oh man, I-I'm so sorry, I didn't mean to--")
        fnDialogue("Stacy: [E|Sad]D-don't worry about it.[P] You're just a little on edge is all.[P] We all are.")
        fnDialogue("Player: Right![P] Yes!... um, is it all right if you try that again?[P] I won't slap you this time.")
        fnDialogue("Thought: Her cheeks glow red.")
        fnDialogue("Stacy: [E|Shock]Y-yeah! I mean, [ITALICS]oui![ENDITALICS].[P][E|Blush] Very [ITALICS]oui[ENDITALICS].")
        fnDialogue("Thought: Her arms slowly wrap around me.")
        fnDialogue("Thought: I try not to think about how Eilette is watching us in the rearview mirror and just focus on how nice it feels.")
        fnDialogue("Thought: Eventually, The car slows to a stop.")
        fnDialogue("[CHARENTER|Eilette]Eilette: [E|N][PLAYERNAME], inform your friend that we have arrived at her house.[P] Make sure she has all her things.")
        fnDialogue("Stacy: [E|Happy]Let's see... backpack?[P] check.[P] Sanity?[P] jury's still out on that one.")
        fnDialogue("Thought: Stacy steps out, giving me one last reassuring look.[P] The car door starts to close.")
        fnDialogue("Player: Thanks for saving me back there, Stacy.[P] I... hope I can repay the favor someday.")
        fnDialogue("Stacy: [E|Blush]Heh, [ITALICS]no problemo.[ENDITALICS] [P]I look forward to it!")
        fnDialogue("Stacy: [E|Shock]... Hey, wait a minute. How did she know where I liv--")
        fnDialogue("[HIDEALL]Info: [CRASH]*SLAM*")
        
    elseif(sBuddyName == "Bret") then
        fnDialogue("[CHARENTER|Bret]Bret: [E|Shock]Ouch!")
        fnDialogue("Player: B-Bret![P] I'm sorry, I...")
        fnDialogue("Bret: [E|Blush]No, uh, I'm sorry.[P][E|Shock] I shouldn't have tried to--[SPEEDTEXT][E|Mad]I mean, you're obviously nervous, and I'm nervous,")
        fnDialogue("Bret: [E|Shock] and I thought maybe holding your hand would help, [E|Sad]but I've never held anyone's hand before, so honestly it made even MORE nervous,")
        fnDialogue("Bret: [E|Shock] and I think I clenched too hard[ENDSPEEDTEXT][P][E|Sad]... uh...[PLAYERNAME].[P][P]Are you okay?")
        fnDialogue("Player: I will be.[P] *sniff*.[P][P] That was really scary.")
        fnDialogue("Bret: [E|Sad]Yeah. It was.")
        fnDialogue("Bret: [E|N] But I'm really, really glad you're safe.")
        fnDialogue("Thought: I nod.")
        fnDialogue("Player: Thanks for... [P]saving me and all.")
        fnDialogue("Bret: [E|Happy]D-did I save you?[P] I'll be honest, I kind of blacked out a little bit.")
        fnDialogue("Player: Heh.[P][P] Well whatever happened, it was very kind and brave.")
        fnDialogue("Thought: Bret stutters so hard his glasses threaten to fall of his face.[P] The car slows down.")
        fnDialogue("[CHARENTER|Eilette]Eilette: [E|N][PLAYERNAME], inform your friend that we have arrived at his house.[P] Make sure he has all his things.")
        fnDialogue("Thought: Bret steps out; it seems like he wants to say something, but can't.[P] The car door starts to close.")
        fnDialogue("Bret: [E|Blush][PLAYERNAME], uh...[P][SPEEDTEXT]this was really scary but I'm... I'm glad I got to be here for you![ENDSPEETEXT] ")
        fnDialogue("Bret: [E|Shock] You're the most wonderful p--")
        fnDialogue("[HIDEALL]Info: [CRASH]*SLAM*")
        
    elseif(sBuddyName == "Todd") then
        fnDialogue("[CHARENTER|Todd]Todd: [E|Shock][SFX|Blink]Ow!")
        fnDialogue("Player: Todd![P] I'm sorry, I-I didn't mean to...")
        fnDialogue("Todd: [E|Blush]It's fine, it's fine.[P][E|Mad] I was just trying to comfort you, but we're all a little nervous right now.")
        fnDialogue("Player: Yes.[P] Um, thanks for saving me back there.")
        fnDialogue("Thought: He nods, but doesn't reach out for me again.[P] I try to hide my disappointment.")
        fnDialogue("Thought: The car slows to a stop.")
        fnDialogue("[CHARENTER|Eilette]Eilette: [E|N]Todd, we've arrived at your house.[P] Make sure you have all your things.")
        fnDialogue("Todd: [E|N]Sure, sure... let's see.[P] Backpack?[P] Check. Cell phone?[P] Check. Sanity?[P][P][E|Mad] Mm, can't find it anywhere.")
        fnDialogue("Eilette:[E|N][P].[P].[P].")
        fnDialogue("Todd: [SFX|Ugh][E|Sad]Tough crowd.")
        fnDialogue("Thought: He slowly gets out, and I can't think of anything to say.[P] The car door starts to close.")
        fnDialogue("Todd: [E|Shock]Oh, um, [PLAYERNAME]?[P] One last thing?")
        fnDialogue("Player: Y-yeah?")
        fnDialogue("Todd: [E|Sad]I don't think I should be your tutor anymo--")
        fnDialogue("[HIDEALL]Info: [CRASH]*SLAM*")
        
    elseif(sBuddyName == "Michelle") then
        fnDialogue("[CHARENTER|Michelle]Michelle: [E|Shock][CRASH]Jesus! [P]OW!")
        fnDialogue("Player: Michelle,[P] I-I'm sorry, I didn't mean--")
        fnDialogue("Michelle: [E|Blush]Oh, save it. You're fraught with nerves, anyone can see that.[P] I shouldn'a tried to pat your back.")
        fnDialogue("Michelle: [E|Sad]I keep grabbin' people without their permission,[P][P] Stacy says I've got to stop doing that.")
        fnDialogue("Thought: Even though my eyes are watery, I can't help but chuckle.")
        fnDialogue("Player: Y-you do have my permission.")
        fnDialogue("Michelle: [E|Sad]Huh?")
        fnDialogue("Player: I could...[P] if you don't mind, I could use a nice hug, or a...[P] hold my hand, or something?")
        fnDialogue("Thought: Michelle gives me a sidelong look.[P] I think she might scoff in my face, but then she offers me her hand.")
        fnDialogue("Thought: I place mine in it, watching her black-polished fingernails slowly curl around my shaking palm.")
        fnDialogue("Thought: The grip is tight, protective.")
        fnDialogue("Michelle: [E|Blush]I... can't believe what just happened.")
        fnDialogue("Player: Me neither.[P] Thanks for saving me.")
        fnDialogue("Michelle: [E|Blush]Tch.[P] I couldn't just let a bunch of creeps snatch you up, could I?")
        fnDialogue("Thought: My cheeks turn red, and I'm distinctly aware that Eilette is watching us from the rearview mirror.")
        fnDialogue("Thought: The car slows to a stop.")
        fnDialogue("[CHARENTER|Eilette]Eilette: [E|N][PLAYERNAME], inform your friend that we have arrived at her house. [P]Make sure she has all her things.")
        fnDialogue("Thought: Michelle steps out, and I hesitantly let go of her hand.[P] The car door starts to close.")
        fnDialogue("Michelle: [E|Shock]Oy, wait a minute.[P] How did she know where I liv--")
        fnDialogue("[HIDEALL]Info: [CRASH]*SLAM*")
        
    elseif(sBuddyName == "Laura") then
        fnDialogue("[CHARENTER|Laura]Laura: [E|Sad]Eep!")
        fnDialogue("Player: What?[P] Laura, you...")
        fnDialogue("Laura: [E|Shock]I was [P]*hrm*[P] j-just trying to[P]... hnnn...")
        fnDialogue("Thought: Laura's hands, once gently brushing against mine, are now tightly wrapped around her body.")
        fnDialogue("Thought: She's rocking back and forth so violently, if the seatbelt wasn't holding her in she might rock right out of her seat.")
        fnDialogue("Player: Oh no--d-[P]Did I hurt you?[P] I'm so sorry... [P]about all of this. I--")
        fnDialogue("[CHARENTER|Eilette]Eilette: [E|Sad][PLAYERNAME], do not speak to your friend for a little while.[P] Just make sure she doesn't hit her head.")
        fnDialogue("Thought: Eilette's gaze flicks up at me from the rearview mirror. [P]I nod and turn back to Laura.[CHAREXIT|Eilette] ")
        fnDialogue("Thought: We drive for what feels like an eternity, and she keeps rocking.[P] I'm so nervous I find myself rocking along with her.")
        fnDialogue("[CHARENTER|Eilette]Eilette: [E|Sad]Turn here, Seven.[P] Another lap.")
        fnDialogue("[CHAREXIT|Eilette]Thought: Laura's eyes, watery and frantic, finally meet mine.")
        fnDialogue("Thought: We rock together for a little bit.[P] Somehow it distracts me from all the panic, and feels calming.")
        fnDialogue("Thought: Eventually, we slow down together.")
        fnDialogue("Laura: [P][P][E|Sad]I... I'm okay.[P][E|Mad] *hrm* I'm gonna be okay.")
        fnDialogue("Player: Laura![P] I'm so sorry about all of this.")
        fnDialogue("Laura: [E|Sad]Don't be, [PLAYERNAME].[P][E|Blush] I'm just glad...[P]*ahem* n-nothing bad happened to you.")
        fnDialogue("Thought: Laura touches my hand again, and this time I don't pull away.")
        fnDialogue("Thought: My cheeks warm. [P]The car slows to a stop.")
        fnDialogue("[CHARENTER|Eilette]Eilette: [E|N][PLAYERNAME], please inform your friend we've arrived at her house.")
        fnDialogue("Thought: Stepping out, she gives me one last worried look.")
        fnDialogue("Laura: [E|N]Will you be all right?")
        fnDialogue("Player: Y..yeah.[P] Thanks for, uh... thanks for saving me back there.")
        fnDialogue("Thought: We share a tentative smile, and the car door begins to close.")
        fnDialogue("Laura: [E|Shock]W-wait a minute, how does she know where I liv--")
        fnDialogue("[HIDEALL]Info: [CRASH]*SLAM*")

    end

    fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"GoHome\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
    PairanormalLevel_FlagCheckpoint("GoHome")
    
    fnDialogue("[BG|Null][Music|Null]Thought: ...")
    LM_ExecuteScript(gsRoot .. "../Chapter 4/Gameplay_SceneD/2 - Back At Home.lua", "Start")

end
