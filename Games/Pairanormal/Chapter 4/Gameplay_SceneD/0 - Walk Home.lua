-- |[Walking Home]|
--Ballspgglrr

-- |[Arg Resolve]|
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--Flags.
VM_SetVar("Root/Variables/System/Player/iChapterDestination", "N", 4.0)

-- |[Mari]|
if(sTopicName == "Mari") then

    --Variables.
    local iWalkedWithMariA = VM_GetVar("Root/Variables/System/Player/iWalkedWithMariA", "N")
    if(iWalkedWithMariA == 0.0) then
        LM_ExecuteScript(gsRoot .. "../Chapter Walk/Walkhome Mari 0.lua", "Start")
    else
        LM_ExecuteScript(gsRoot .. "../Chapter Walk/Walkhome Mari 1.lua", "Start")
    end
    
-- |[Stacy]|
elseif(sTopicName == "Stacy") then

    --Variables.
    local iWalkedWithStacyA = VM_GetVar("Root/Variables/System/Player/iWalkedWithStacyA", "N")
    if(iWalkedWithStacyA == 0.0) then
        LM_ExecuteScript(gsRoot .. "../Chapter Walk/Walkhome Stacy 0.lua", "Start")
    else
        LM_ExecuteScript(gsRoot .. "../Chapter Walk/Walkhome Stacy 1.lua", "Start")
    end

-- |[Bret]|
elseif(sTopicName == "Bret") then

    --Variables.
    local iWalkedWithBretA = VM_GetVar("Root/Variables/System/Player/iWalkedWithBretA", "N")
    if(iWalkedWithBretA == 0.0) then
        LM_ExecuteScript(gsRoot .. "../Chapter Walk/Walkhome Bret 0.lua", "Start")
    else
        LM_ExecuteScript(gsRoot .. "../Chapter Walk/Walkhome Bret 1.lua", "Start")
    end

-- |[Michelle]|
elseif(sTopicName == "Michelle") then

    --Variables.
    local iWalkedWithMichelleA = VM_GetVar("Root/Variables/System/Player/iWalkedWithMichelleA", "N")
    if(iWalkedWithMichelleA == 0.0) then
        LM_ExecuteScript(gsRoot .. "../Chapter Walk/Walkhome Michelle 0.lua", "Start")
    else
        LM_ExecuteScript(gsRoot .. "../Chapter Walk/Walkhome Michelle 1.lua", "Start")
    end

-- |[Laura]|
elseif(sTopicName == "Laura") then

    --Variables.
    local iWalkedWithLauraA = VM_GetVar("Root/Variables/System/Player/iWalkedWithLauraA", "N")
    if(iWalkedWithLauraA == 0.0) then
        LM_ExecuteScript(gsRoot .. "../Chapter Walk/Walkhome Laura 0.lua", "Start")
    else
        LM_ExecuteScript(gsRoot .. "../Chapter Walk/Walkhome Laura 1.lua", "Start")
    end

-- |[Todd]|
elseif(sTopicName == "Todd") then

    --Variables.
    local iWalkedWithToddA = VM_GetVar("Root/Variables/System/Player/iWalkedWithToddA", "N")
    if(iWalkedWithToddA == 0.0) then
        LM_ExecuteScript(gsRoot .. "../Chapter Walk/Walkhome Todd 0.lua", "Start")
    else
        LM_ExecuteScript(gsRoot .. "../Chapter Walk/Walkhome Todd 1.lua", "Start")
    end

-- |[Big Scene]|
elseif(sTopicName == "LimoScene") then
    fnDialogue("[HIDEALL][BG|Null][Music|Null]Thought: ...")

    fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"LimoScene1\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
    PairanormalLevel_FlagCheckpoint("LimoScene1")
    
    fnDialogue("[HIDEALL][BG|Root/Images/Backgrounds/All/SidewalkNight][Music|Somber]Thought: Player: ... It's a lot later than I thought.")
    fnDialogue("Thought: I eye the last stretch of road before I reach home,[P] contemplating briefly when it stopped becoming just a house I stayed in.")

    --Determine who has the highest friendship value.
    local iMariFriend     = VM_GetVar("Root/Variables/System/Romance/iMariFriend", "N")
    local iStacyFriend    = VM_GetVar("Root/Variables/System/Romance/iStacyFriend", "N")
    local iMichelleFriend = VM_GetVar("Root/Variables/System/Romance/iMichelleFriend", "N")
    local iBretFriend     = VM_GetVar("Root/Variables/System/Romance/iBretFriend", "N")
    local iLauraFriend    = VM_GetVar("Root/Variables/System/Romance/iLauraFriend", "N")
    local iToddFriend     = VM_GetVar("Root/Variables/System/Romance/iToddFriend", "N")
    local zaTable = {}
    zaTable[1] = {iMariFriend, "Mari"}
    zaTable[2] = {iStacyFriend, "Stacy"}
    zaTable[3] = {iMichelleFriend, "Michelle"}
    zaTable[4] = {iBretFriend, "Bret"}
    zaTable[5] = {iLauraFriend, "Laura"}
    zaTable[6] = {iToddFriend, "Todd"}
    local iHighest = -1000
    local sHighest = "Mari"
    for i = 1, 6, 1 do
        if(zaTable[i][1] > iHighest) then
            iHighest = zaTable[i][1]
            sHighest = zaTable[i][2]
        end
    end

    -- |[Mari]|
    if(sHighest == "Mari") then
        VM_SetVar("Root/Variables/System/Romance/sBuddy", "S", "Mari")
        fnDialogue("???: [VOICE|Voice|Mari][PLAYERNAME]! Wait up!")
        fnDialogue("Thought: Who is calling to me? I listen to the murmur of wind through yards, and the low rumbling of a long sleek car as it drives by.")
        fnDialogue("Thought: Its windows are tinted.[P] I crane my neck, drawn to the unusual vehicle--there aren't any cars like that in this neighborhood.")
        fnDialogue("[SHOWCHAR|Mari]Mari: [PLAYERNAME]. There you are; I'm glad I caught you.")
        fnDialogue("Player: Mari! What's the matter, you forget something?")
        fnDialogue("Mari: [E|Sad]Uh. Yes.[P] No.[P] Look. [E|Blush]I'm just gonna come right out and say it before I convince myself this is a bad idea. ")
		fnDialogue("Mari: [E|Blush]I was wondering...")
        fnDialogue("Thought: We're interrupted by an engine cutting off. The car from earlier has stopped in the middle of the road.")
        fnDialogue("Mari: [ITALICS]You've got to be kidding me.[ENDITALICS] ")
        fnDialogue("Thought: The door swings outward.[P] It takes a moment for me to register the woman who runs to me.")
        fnDialogue("Mari: [E|Shock... Miss Foster?")
        fnDialogue("[CHARENTER|Candace]Candace: [E|Shock][PLAYERNAME]!! Oh my god, it's really you.[P][E|Happy] It's really[P]--I can't believe I found you again!")
        fnDialogue("Thought: She pulls me in for a hug, tighter than I'm comfortable with.")
        fnDialogue("Candace: [E|Sad][PLAYERNAME], are you okay?[P] Did they hurt you?")
        fnDialogue([[Player: What are you talking about?[P] Who's \"they\"?]])
        fnDialogue("Candace: [E|Mad]I'm talking about [ITALICS]that woman[ENDITALICS], and her... inhuman organization!")
        fnDialogue("Candace: [E|Sad][PLAYERNAME], please.[P] I know you don't[P][P]... r-remember me, but I... you're not [ITALICS]safe[ENDITALICS] here.")
        fnDialogue("Candace: [E|Mad][P][P]They [ITALICS]did[ENDITALICS] something to you.[P] Please, you have to let me take you away!")
        fnDialogue("Mari: [E|Neutral]Um... what the actual hell is going on???")
        fnDialogue("Player: Look, I'm really sorry, but I...[P]I have to get home, okay?")
        fnDialogue("Candace: [E|Shock]No, please! Whatever you do[P][E|Sad]--You don't understand.[P] They--I...")
        fnDialogue("Thought: Her eyes dart wildly back and forth between me and the woods.[P] It looks like she wants to say something but can't, because Mari is here.")
        fnDialogue("Thought: [MUSIC|NONE]But then she blurts it out anyway--[P]A high pitched, rapid confession.")
        fnDialogue("Thought: So sharp and quiet I'm not even sure I heard her right.")
        fnDialogue("Candace: [E|Sad][PLAYERNAME],[P][P][P][P] I'm your mother.")
        fnDialogue("Mari: [E|Shock]!!!!")
        fnDialogue("Player: I think.[P][P][P] You're mistaken.")
        fnDialogue("Candace: [E|Mad]I'm not! You came to me a y-year ago.")
        fnDialogue("Candace: I did tests.[P][E|Shock] I didn't even think you were alive!")
        fnDialogue("Thought: She hurries back to the car, bringing out a briefcase and snapping it open right on the pavement.")
        fnDialogue("Thought: Papers fly everywhere, and I can see her hands shaking as she rifles through them.")
        fnDialogue("Candace: [E|Shock]I thought you had died, but you were living in an orphanage.[P][E|Happy] Look.[P] Look, here, see!?![P] This is you. ")
        fnDialogue("Thought: She shoves a newspaper in my hands.")
        fnDialogue("Thought: Words on newspapers are still too small for me to read, but I can clearly see the photograph. ")
        fnDialogue("Mari: Holy...![P] That is your face right there.")
        fnDialogue("[HIDEALL][Music|Somber][IMAGE|Root/Images/MajorScenes/All/Missing]Thought: I can't accept it as anything more than a photo of a stranger.")
        fnDialogue("Thought: Someone smiling in a place I've never been, amongst people I don't recognize.[P][P] Awake when I was supposed to be asleep.")
        fnDialogue("Thought: I push the paper back to her.")
        fnDialogue("[IMAGE|Null][CHARENTER|Candace][CHARENTER|Mari]Candace: [VOICE|Voice|Candace][E|Sad]Please, [PLAYERNAME].[P] I know this is a lot to take in. Some crazy random woman on the street, trying to convince you that you're family[P][P]--hahh...")
        fnDialogue("Thought: She lets out a short, sorrowful laugh.")
        fnDialogue("Candace: [E|Sad]Actually, it's quite ironic.[P] You--nevermind, I'll explain later.")
        fnDialogue("Candace: [E|Mad] But you should know now that there are people in this town who play with lives like its a game.")
        fnDialogue("Candace: I've been caught up in it, and I know you are too.")
        fnDialogue("Thought: My face betrays a momentary recognition.[P] I try to look away, but I know Miss Foster saw it.")
        fnDialogue("Thought: She says just what I was afraid she might, the words terrifying and ever so tempting.")
        fnDialogue("Candace: [E|Sad]If you come with me,[P][P][ITALICS] I'll tell you everything.[ENDITALICS] ")
        fnDialogue("Candace: [E|Shock]I know you're desperate for answers.[P] But I have to keep you safe, too.")
        fnDialogue("Candace: [E|Happy]That's all I want, to keep you safe![P] Please, please, listen to me [PLAYERNAME].")
        fnDialogue("Mari: [E|Shock]No way. Miss Foster... [PLAYERNAME], you can't be serious.[P][E|Sad] [PLAYERNAME]?")
        fnDialogue("[MUSIC|NONE]Thought: Miss Foster's hand extends to me.[P] I just stare through it, my whole body numb.")
        fnDialogue("Info: One of the following options leads to a game ending.")
        fnDialogue("Info: You won't be able to go back and choose again, so pick carefully!")
        
    -- |[Stacy]|
    elseif(sHighest == "Stacy") then
        VM_SetVar("Root/Variables/System/Romance/sBuddy", "S", "Stacy")
        fnDialogue("???: [VOICE|Voice|Stacy][PLAYERNAME]! Wait up!")
        fnDialogue("Thought: Who is calling to me? I listen to the murmur of wind through yards, and the low rumbling of a long sleek car as it drives by.")
        fnDialogue("Thought: Its windows are tinted.[P] I crane my neck, drawn to the unusual vehicle--there aren't any cars like that in this neighborhood.")
        fnDialogue("[SHOWCHAR|Stacy]Stacy: [PLAYERNAME]. There you are; I'm glad I caught you.")
        fnDialogue("Player: Stacy! What's the matter, you forget something?")
        fnDialogue("Stacy: Uh. Yes. No. Look. I'm just gonna come right out and say it before I convince myself this is a bad idea. I was wondering...")
        fnDialogue("Thought: We're interrupted by an engine cutting off. The car from earlier has stopped in the middle of the road.")
        fnDialogue("Stacy: [ITALICS]You've got to be kidding me.[ENDITALICS] ")
        fnDialogue("Thought: The door swings outward.[P] It takes a moment for me to register the woman who runs to me.")
        fnDialogue("Stacy: ... Miss Foster?")
        fnDialogue("[CHARENTER|Candace]Candace: [E|Shock][PLAYERNAME]!! Oh my god, it's really you.[P][E|Happy] It's really[P]--I can't believe I found you again!")
        fnDialogue("Thought: She pulls me in for a hug, tighter than I'm comfortable with.")
        fnDialogue("Candace: [E|Sad][PLAYERNAME], are you okay?[P] Did they hurt you?")
        fnDialogue([[Player: What are you talking about?[P] Who's \"they\"?]])
        fnDialogue("Candace: [E|Mad]I'm talking about [ITALICS]that woman[ENDITALICS], and her... inhuman organization!")
        fnDialogue("Candace: [E|Sad][PLAYERNAME], please.[P] I know you don't[P][P]... r-remember me, but I... you're not [ITALICS]safe[ENDITALICS] here.")
        fnDialogue("Candace: [E|Mad][P][P]They [ITALICS]did[ENDITALICS] something to you.[P] Please, you have to let me take you away!")
        fnDialogue("Stacy: What the actual hell is going on???")
        fnDialogue("Player: Look, I'm really sorry, but I...[P]I have to get home, okay?")
        fnDialogue("Candace: [E|Shock]No, please! Whatever you do[P][E|Sad]--You don't understand.[P] They--I...")
        fnDialogue("Thought: Her eyes dart wildly back and forth between me and the woods.[P] It looks like she wants to say something but can't, because Stacy is here.")
        fnDialogue("Thought: [MUSIC|NONE]But then she blurts it out anyway--[P]A high pitched, rapid confession.")
        fnDialogue("Thought: So sharp and quiet I'm not even sure I heard her right.")
        fnDialogue("Candace: [E|Sad][PLAYERNAME],[P][P][P][P] I'm your mother.")
        fnDialogue("Stacy: !!!!")
        fnDialogue("Player: I think.[P][P][P] You're mistaken.")
        fnDialogue("Candace: [E|Mad]I'm not! You came to me a y-year ago.")
        fnDialogue("Candace: I did tests.[P][E|Shock] I didn't even think you were alive!")
        fnDialogue("Thought: She hurries back to the car, bringing out a briefcase and snapping it open right on the pavement.")
        fnDialogue("Thought: Papers fly everywhere, and I can see her hands shaking as she rifles through them.")
        fnDialogue("Candace: [E|Shock]I thought you had died, but you were living in an orphanage.[P][E|Happy] Look.[P] Look, here, see!?![P] This is you. ")
        fnDialogue("Thought: She shoves a newspaper in my hands.")
        fnDialogue("Thought: Words on newspapers are still too small for me to read, but I can clearly see the photograph. ")
        fnDialogue("Stacy: Holy...! That is your face right there.")
        fnDialogue("[HIDEALL][Music|Somber][IMAGE|Root/Images/MajorScenes/All/Missing]Thought: I can't accept it as anything more than a photo of a stranger.")
        fnDialogue("Thought: Someone smiling in a place I've never been, amongst people I don't recognize.[P][P] Awake when I was supposed to be asleep.")
        fnDialogue("Thought: I push the paper back to her.")
        fnDialogue("[CHARENTER|Candace][CHARENTER|Stacy]Candace: [VOICE|Voice|Candace]Please, [PLAYERNAME].[P] I know this is a lot to take in. Some crazy random woman on the street, trying to convince you that you're family[P][P]--hahh...")
        fnDialogue("[IMAGE|Null]Thought: She lets out a short, sorrowful laugh.")
        fnDialogue("Candace: [E|Sad]Actually, it's quite ironic.[P] You--nevermind, I'll explain later.")
        fnDialogue("Candace: [E|Mad] But you should know now that there are people in this town who play with lives like its a game.")
        fnDialogue("Candace: I've been caught up in it, and I know you are too.")
        fnDialogue("Thought: My face betrays a momentary recognition.[P] I try to look away, but I know Miss Foster saw it.")
        fnDialogue("Thought: She says just what I was afraid she might, the words terrifying and ever so tempting.")
        fnDialogue("Candace: [E|Sad]If you come with me,[P][P][ITALICS] I'll tell you everything.[ENDITALICS] ")
        fnDialogue("Candace: [E|Shock]I know you're desperate for answers.[P] But I have to keep you safe, too.")
        fnDialogue("Candace: [E|Happy]That's all I want, to keep you safe![P] Please, please, listen to me [PLAYERNAME].")
        fnDialogue("Stacy: No way. Miss Foster... [PLAYERNAME], you can't be serious. [PLAYERNAME]?")
        fnDialogue("Thought: Miss Foster's hand extends to me.[P] I just stare through it, my whole body numb.")
        fnDialogue("Info: One of the following options leads to a game ending.")
        fnDialogue("Info: You won't be able to go back and choose again, so pick carefully!")
        
    -- |[Michelle]|
    elseif(sHighest == "Michelle") then
        VM_SetVar("Root/Variables/System/Romance/sBuddy", "S", "Michelle")
        fnDialogue("???: [VOICE|Voice|Michelle][PLAYERNAME]! Wait up!")
        fnDialogue("Thought: Who is calling to me? I listen to the murmur of wind through yards, and the low rumbling of a long sleek car as it drives by.")
        fnDialogue("Thought: Its windows are tinted.[P] I crane my neck, drawn to the unusual vehicle--there aren't any cars like that in this neighborhood.")
        fnDialogue("[SHOWCHAR|Michelle]Michelle: [PLAYERNAME]. There you are; I'm glad I caught you.")
        fnDialogue("Player: Michelle! What's the matter, you forget something?")
        fnDialogue("Michelle: Uh. Yes. No. Look. I'm just gonna come right out and say it before I convince myself this is a bad idea. I was wondering...")
        fnDialogue("Thought: We're interrupted by an engine cutting off. The car from earlier has stopped in the middle of the road.")
        fnDialogue("Michelle: [ITALICS]You've got to be kidding me.[ENDITALICS] ")
        fnDialogue("Thought: The door swings outward.[P] It takes a moment for me to register the woman who runs to me.")
        fnDialogue("Michelle: ... Miss Foster?")
        fnDialogue("[CHARENTER|Candace]Candace: [E|Shock][PLAYERNAME]!! Oh my god, it's really you.[P][E|Happy] It's really[P]--I can't believe I found you again!")
        fnDialogue("Thought: She pulls me in for a hug, tighter than I'm comfortable with.")
        fnDialogue("Candace: [E|Sad][PLAYERNAME], are you okay?[P] Did they hurt you?")
        fnDialogue([[Player: What are you talking about?[P] Who's \"they\"?]])
        fnDialogue("Candace: [E|Mad]I'm talking about [ITALICS]that woman[ENDITALICS], and her... inhuman organization!")
        fnDialogue("Candace: [E|Sad][PLAYERNAME], please.[P] I know you don't[P][P]... r-remember me, but I... you're not [ITALICS]safe[ENDITALICS] here.")
        fnDialogue("Candace: [E|Mad][P][P]They [ITALICS]did[ENDITALICS] something to you.[P] Please, you have to let me take you away!")
        fnDialogue("Michelle: What the actual hell is going on???")
        fnDialogue("Player: Look, I'm really sorry, but I...[P]I have to get home, okay?")
        fnDialogue("Candace: [E|Shock]No, please! Whatever you do[P][E|Sad]--You don't understand.[P] They--I...")
        fnDialogue("Thought: Her eyes dart wildly back and forth between me and the woods.[P] It looks like she wants to say something but can't, because Michelle is here.")
        fnDialogue("Thought: [MUSIC|NONE]But then she blurts it out anyway--[P]A high pitched, rapid confession.")
        fnDialogue("Thought: So sharp and quiet I'm not even sure I heard her right.")
        fnDialogue("Candace: [E|Sad][PLAYERNAME],[P][P][P][P] I'm your mother.")
        fnDialogue("Michelle: !!!!")
        fnDialogue("Player: I think.[P][P][P] You're mistaken.")
        fnDialogue("Candace: [E|Mad]I'm not! You came to me a y-year ago.")
        fnDialogue("Candace: I did tests.[P][E|Shock] I didn't even think you were alive!")
        fnDialogue("Thought: She hurries back to the car, bringing out a briefcase and snapping it open right on the pavement.")
        fnDialogue("Thought: Papers fly everywhere, and I can see her hands shaking as she rifles through them.")
        fnDialogue("Candace: [E|Shock]I thought you had died, but you were living in an orphanage.[P][E|Happy] Look.[P] Look, here, see!?![P] This is you. ")
        fnDialogue("Thought: She shoves a newspaper in my hands.")
        fnDialogue("Thought: Words on newspapers are still too small for me to read, but I can clearly see the photograph. ")
        fnDialogue("Michelle: Holy...! That is your face right there.")
        fnDialogue("[HIDEALL][Music|Somber][IMAGE|Root/Images/MajorScenes/All/Missing]Thought: I can't accept it as anything more than a photo of a stranger.")
        fnDialogue("Thought: Someone smiling in a place I've never been, amongst people I don't recognize.[P][P] Awake when I was supposed to be asleep.")
        fnDialogue("Thought: I push the paper back to her.")
        fnDialogue("[CHARENTER|Candace][CHARENTER|Michelle]Candace: [VOICE|Voice|Candace]Please, [PLAYERNAME].[P] I know this is a lot to take in. Some crazy random woman on the street, trying to convince you that you're family[P][P]--hahh...")
        fnDialogue("[IMAGE|Null]Thought: She lets out a short, sorrowful laugh.")
        fnDialogue("Candace: [E|Sad]Actually, it's quite ironic.[P] You--nevermind, I'll explain later.")
        fnDialogue("Candace: [E|Mad] But you should know now that there are people in this town who play with lives like its a game.")
        fnDialogue("Candace: I've been caught up in it, and I know you are too.")
        fnDialogue("Thought: My face betrays a momentary recognition.[P] I try to look away, but I know Miss Foster saw it.")
        fnDialogue("Thought: She says just what I was afraid she might, the words terrifying and ever so tempting.")
        fnDialogue("Candace: [E|Sad]If you come with me,[P][P][ITALICS] I'll tell you everything.[ENDITALICS] ")
        fnDialogue("Candace: [E|Shock]I know you're desperate for answers.[P] But I have to keep you safe, too.")
        fnDialogue("Candace: [E|Happy]That's all I want, to keep you safe![P] Please, please, listen to me [PLAYERNAME].")
        fnDialogue("Michelle: No way. Miss Foster... [PLAYERNAME], you can't be serious. [PLAYERNAME]?")
        fnDialogue("Thought: Miss Foster's hand extends to me.[P] I just stare through it, my whole body numb.")
        fnDialogue("Info: One of the following options leads to a game ending.")
        fnDialogue("Info: You won't be able to go back and choose again, so pick carefully!")
        
    -- |[Bret]|
    elseif(sHighest == "Bret") then
        VM_SetVar("Root/Variables/System/Romance/sBuddy", "S", "Bret")
        fnDialogue("???: [VOICE|Voice|Bret][PLAYERNAME]! Wait up!")
        fnDialogue("Thought: Who is calling to me? I listen to the murmur of wind through yards, and the low rumbling of a long sleek car as it drives by.")
        fnDialogue("Thought: Its windows are tinted.[P] I crane my neck, drawn to the unusual vehicle--there aren't any cars like that in this neighborhood.")
        fnDialogue("[SHOWCHAR|Bret]Bret: [PLAYERNAME]. There you are; I'm glad I caught you.")
        fnDialogue("Player: Bret! What's the matter, you forget something?")
        fnDialogue("Bret: Uh. Yes. No. Look. I'm just gonna come right out and say it before I convince myself this is a bad idea. I was wondering...")
        fnDialogue("Thought: We're interrupted by an engine cutting off. The car from earlier has stopped in the middle of the road.")
        fnDialogue("Bret: [ITALICS]You've got to be kidding me.[ENDITALICS] ")
        fnDialogue("Thought: The door swings outward.[P] It takes a moment for me to register the woman who runs to me.")
        fnDialogue("Bret: ... Miss Foster?")
        fnDialogue("[CHARENTER|Candace]Candace: [E|Shock][PLAYERNAME]!! Oh my god, it's really you.[P][E|Happy] It's really[P]--I can't believe I found you again!")
        fnDialogue("Thought: She pulls me in for a hug, tighter than I'm comfortable with.")
        fnDialogue("Candace: [E|Sad][PLAYERNAME], are you okay?[P] Did they hurt you?")
        fnDialogue([[Player: What are you talking about?[P] Who's \"they\"?]])
        fnDialogue("Candace: [E|Mad]I'm talking about [ITALICS]that woman[ENDITALICS], and her... inhuman organization!")
        fnDialogue("Candace: [E|Sad][PLAYERNAME], please.[P] I know you don't[P][P]... r-remember me, but I... you're not [ITALICS]safe[ENDITALICS] here.")
        fnDialogue("Candace: [E|Mad][P][P]They [ITALICS]did[ENDITALICS] something to you.[P] Please, you have to let me take you away!")
        fnDialogue("Bret: What the actual hell is going on???")
        fnDialogue("Player: Look, I'm really sorry, but I...[P]I have to get home, okay?")
        fnDialogue("Candace: [E|Shock]No, please! Whatever you do[P][E|Sad]--You don't understand.[P] They--I...")
        fnDialogue("Thought: Her eyes dart wildly back and forth between me and the woods.[P] It looks like she wants to say something but can't, because Bret is here.")
        fnDialogue("Thought: [MUSIC|NONE]But then she blurts it out anyway--[P]A high pitched, rapid confession.")
        fnDialogue("Thought: So sharp and quiet I'm not even sure I heard her right.")
        fnDialogue("Candace: [E|Sad][PLAYERNAME],[P][P][P][P] I'm your mother.")
        fnDialogue("Bret: !!!!")
        fnDialogue("Player: I think.[P][P][P] You're mistaken.")
        fnDialogue("Candace: [E|Mad]I'm not! You came to me a y-year ago.")
        fnDialogue("Candace: I did tests.[P][E|Shock] I didn't even think you were alive!")
        fnDialogue("Thought: She hurries back to the car, bringing out a briefcase and snapping it open right on the pavement.")
        fnDialogue("Thought: Papers fly everywhere, and I can see her hands shaking as she rifles through them.")
        fnDialogue("Candace: [E|Shock]I thought you had died, but you were living in an orphanage.[P][E|Happy] Look.[P] Look, here, see!?![P] This is you. ")
        fnDialogue("Thought: She shoves a newspaper in my hands.")
        fnDialogue("Thought: Words on newspapers are still too small for me to read, but I can clearly see the photograph. ")
        fnDialogue("Bret: Holy...! That is your face right there.")
        fnDialogue("[HIDEALL][Music|Somber][IMAGE|Root/Images/MajorScenes/All/Missing]Thought: I can't accept it as anything more than a photo of a stranger.")
        fnDialogue("Thought: Someone smiling in a place I've never been, amongst people I don't recognize.[P][P] Awake when I was supposed to be asleep.")
        fnDialogue("Thought: I push the paper back to her.")
        fnDialogue("[CHARENTER|Candace][CHARENTER|Bret]Candace: [VOICE|Voice|Candace]Please, [PLAYERNAME].[P] I know this is a lot to take in. Some crazy random woman on the street, trying to convince you that you're family[P][P]--hahh...")
        fnDialogue("[IMAGE|Null]Thought: She lets out a short, sorrowful laugh.")
        fnDialogue("Candace: [E|Sad]Actually, it's quite ironic.[P] You--nevermind, I'll explain later.")
        fnDialogue("Candace: [E|Mad] But you should know now that there are people in this town who play with lives like its a game.")
        fnDialogue("Candace: I've been caught up in it, and I know you are too.")
        fnDialogue("Thought: My face betrays a momentary recognition.[P] I try to look away, but I know Miss Foster saw it.")
        fnDialogue("Thought: She says just what I was afraid she might, the words terrifying and ever so tempting.")
        fnDialogue("Candace: [E|Sad]If you come with me,[P][P][ITALICS] I'll tell you everything.[ENDITALICS] ")
        fnDialogue("Candace: [E|Shock]I know you're desperate for answers.[P] But I have to keep you safe, too.")
        fnDialogue("Candace: [E|Happy]That's all I want, to keep you safe![P] Please, please, listen to me [PLAYERNAME].")
        fnDialogue("Bret: No way. Miss Foster... [PLAYERNAME], you can't be serious. [PLAYERNAME]?")
        fnDialogue("Thought: Miss Foster's hand extends to me.[P] I just stare through it, my whole body numb.")
        fnDialogue("Info: One of the following options leads to a game ending.")
        fnDialogue("Info: You won't be able to go back and choose again, so pick carefully!")
        
    -- |[Todd]|
    elseif(sHighest == "Todd") then
        VM_SetVar("Root/Variables/System/Romance/sBuddy", "S", "Todd")
        fnDialogue("???: [VOICE|Voice|Todd][PLAYERNAME]! Wait up!")
        fnDialogue("Thought: Who is calling to me? I listen to the murmur of wind through yards, and the low rumbling of a long sleek car as it drives by.")
        fnDialogue("Thought: Its windows are tinted.[P] I crane my neck, drawn to the unusual vehicle--there aren't any cars like that in this neighborhood.")
        fnDialogue("[SHOWCHAR|Todd]Todd: [PLAYERNAME]. There you are; I'm glad I caught you.")
        fnDialogue("Player: Todd! What's the matter, you forget something?")
        fnDialogue("Todd: Uh. Yes. No. Look. I'm just gonna come right out and say it before I convince myself this is a bad idea. I was wondering...")
        fnDialogue("Thought: We're interrupted by an engine cutting off. The car from earlier has stopped in the middle of the road.")
        fnDialogue("Todd: [ITALICS]You've got to be kidding me.[ENDITALICS] ")
        fnDialogue("Thought: The door swings outward.[P] It takes a moment for me to register the woman who runs to me.")
        fnDialogue("Todd: ... Miss Foster?")
        fnDialogue("[CHARENTER|Candace]Candace: [E|Shock][PLAYERNAME]!! Oh my god, it's really you.[P][E|Happy] It's really[P]--I can't believe I found you again!")
        fnDialogue("Thought: She pulls me in for a hug, tighter than I'm comfortable with.")
        fnDialogue("Candace: [E|Sad][PLAYERNAME], are you okay?[P] Did they hurt you?")
        fnDialogue([[Player: What are you talking about?[P] Who's \"they\"?]])
        fnDialogue("Candace: [E|Mad]I'm talking about [ITALICS]that woman[ENDITALICS], and her... inhuman organization!")
        fnDialogue("Candace: [E|Sad][PLAYERNAME], please.[P] I know you don't[P][P]... r-remember me, but I... you're not [ITALICS]safe[ENDITALICS] here.")
        fnDialogue("Candace: [E|Mad][P][P]They [ITALICS]did[ENDITALICS] something to you.[P] Please, you have to let me take you away!")
        fnDialogue("Todd: What the actual hell is going on???")
        fnDialogue("Player: Look, I'm really sorry, but I...[P]I have to get home, okay?")
        fnDialogue("Candace: [E|Shock]No, please! Whatever you do[P][E|Sad]--You don't understand.[P] They--I...")
        fnDialogue("Thought: Her eyes dart wildly back and forth between me and the woods.[P] It looks like she wants to say something but can't, because Todd is here.")
        fnDialogue("Thought: [MUSIC|NONE]But then she blurts it out anyway--[P]A high pitched, rapid confession.")
        fnDialogue("Thought: So sharp and quiet I'm not even sure I heard her right.")
        fnDialogue("Candace: [E|Sad][PLAYERNAME],[P][P][P][P] I'm your mother.")
        fnDialogue("Todd: !!!!")
        fnDialogue("Player: I think.[P][P][P] You're mistaken.")
        fnDialogue("Candace: [E|Mad]I'm not! You came to me a y-year ago.")
        fnDialogue("Candace: I did tests.[P][E|Shock] I didn't even think you were alive!")
        fnDialogue("Thought: She hurries back to the car, bringing out a briefcase and snapping it open right on the pavement.")
        fnDialogue("Thought: Papers fly everywhere, and I can see her hands shaking as she rifles through them.")
        fnDialogue("Candace: [E|Shock]I thought you had died, but you were living in an orphanage.[P][E|Happy] Look.[P] Look, here, see!?![P] This is you. ")
        fnDialogue("Thought: She shoves a newspaper in my hands.")
        fnDialogue("Thought: Words on newspapers are still too small for me to read, but I can clearly see the photograph. ")
        fnDialogue("Todd: Holy...! That is your face right there.")
        fnDialogue("[HIDEALL][Music|Somber][IMAGE|Root/Images/MajorScenes/All/Missing]Thought: I can't accept it as anything more than a photo of a stranger.")
        fnDialogue("Thought: Someone smiling in a place I've never been, amongst people I don't recognize.[P][P] Awake when I was supposed to be asleep.")
        fnDialogue("Thought: I push the paper back to her.")
        fnDialogue("[CHARENTER|Candace][CHARENTER|Todd]Candace: [VOICE|Voice|Candace]Please, [PLAYERNAME].[P] I know this is a lot to take in. Some crazy random woman on the street, trying to convince you that you're family[P][P]--hahh...")
        fnDialogue("[IMAGE|Null]Thought: She lets out a short, sorrowful laugh.")
        fnDialogue("Candace: [E|Sad]Actually, it's quite ironic.[P] You--nevermind, I'll explain later.")
        fnDialogue("Candace: [E|Mad] But you should know now that there are people in this town who play with lives like its a game.")
        fnDialogue("Candace: I've been caught up in it, and I know you are too.")
        fnDialogue("Thought: My face betrays a momentary recognition.[P] I try to look away, but I know Miss Foster saw it.")
        fnDialogue("Thought: She says just what I was afraid she might, the words terrifying and ever so tempting.")
        fnDialogue("Candace: [E|Sad]If you come with me,[P][P][ITALICS] I'll tell you everything.[ENDITALICS] ")
        fnDialogue("Candace: [E|Shock]I know you're desperate for answers.[P] But I have to keep you safe, too.")
        fnDialogue("Candace: [E|Happy]That's all I want, to keep you safe![P] Please, please, listen to me [PLAYERNAME].")
        fnDialogue("Todd: No way. Miss Foster... [PLAYERNAME], you can't be serious. [PLAYERNAME]?")
        fnDialogue("Thought: Miss Foster's hand extends to me.[P] I just stare through it, my whole body numb.")
        fnDialogue("Info: One of the following options leads to a game ending.")
        fnDialogue("Info: You won't be able to go back and choose again, so pick carefully!")
        
    -- |[Laura]|
    elseif(sHighest == "Laura") then
        VM_SetVar("Root/Variables/System/Romance/sBuddy", "S", "Laura")
        fnDialogue("???: [VOICE|Voice|Laura][PLAYERNAME]! Wait up!")
        fnDialogue("Thought: Who is calling to me? I listen to the murmur of wind through yards, and the low rumbling of a long sleek car as it drives by.")
        fnDialogue("Thought: Its windows are tinted.[P] I crane my neck, drawn to the unusual vehicle--there aren't any cars like that in this neighborhood.")
        fnDialogue("[SHOWCHAR|Laura]Laura: [PLAYERNAME]. There you are; I'm glad I caught you.")
        fnDialogue("Player: Laura! What's the matter, you forget something?")
        fnDialogue("Laura: Uh. Yes. No. Look. I'm just gonna come right out and say it before I convince myself this is a bad idea. I was wondering...")
        fnDialogue("Thought: We're interrupted by an engine cutting off. The car from earlier has stopped in the middle of the road.")
        fnDialogue("Laura: [ITALICS]You've got to be kidding me.[ENDITALICS] ")
        fnDialogue("Thought: The door swings outward.[P] It takes a moment for me to register the woman who runs to me.")
        fnDialogue("Laura: ... Miss Foster?")
        fnDialogue("[CHARENTER|Candace]Candace: [E|Shock][PLAYERNAME]!! Oh my god, it's really you.[P][E|Happy] It's really[P]--I can't believe I found you again!")
        fnDialogue("Thought: She pulls me in for a hug, tighter than I'm comfortable with.")
        fnDialogue("Candace: [E|Sad][PLAYERNAME], are you okay?[P] Did they hurt you?")
        fnDialogue([[Player: What are you talking about?[P] Who's \"they\"?]])
        fnDialogue("Candace: [E|Mad]I'm talking about [ITALICS]that woman[ENDITALICS], and her... inhuman organization!")
        fnDialogue("Candace: [E|Sad][PLAYERNAME], please.[P] I know you don't[P][P]... r-remember me, but I... you're not [ITALICS]safe[ENDITALICS] here.")
        fnDialogue("Candace: [E|Mad][P][P]They [ITALICS]did[ENDITALICS] something to you.[P] Please, you have to let me take you away!")
        fnDialogue("Laura: What the actual hell is going on???")
        fnDialogue("Player: Look, I'm really sorry, but I...[P]I have to get home, okay?")
        fnDialogue("Candace: [E|Shock]No, please! Whatever you do[P][E|Sad]--You don't understand.[P] They--I...")
        fnDialogue("Thought: Her eyes dart wildly back and forth between me and the woods.[P] It looks like she wants to say something but can't, because Laura is here.")
        fnDialogue("Thought: [MUSIC|NONE]But then she blurts it out anyway--[P]A high pitched, rapid confession.")
        fnDialogue("Thought: So sharp and quiet I'm not even sure I heard her right.")
        fnDialogue("Candace: [E|Sad][PLAYERNAME],[P][P][P][P] I'm your mother.")
        fnDialogue("Laura: !!!!")
        fnDialogue("Player: I think.[P][P][P] You're mistaken.")
        fnDialogue("Candace: [E|Mad]I'm not! You came to me a y-year ago.")
        fnDialogue("Candace: I did tests.[P][E|Shock] I didn't even think you were alive!")
        fnDialogue("Thought: She hurries back to the car, bringing out a briefcase and snapping it open right on the pavement.")
        fnDialogue("Thought: Papers fly everywhere, and I can see her hands shaking as she rifles through them.")
        fnDialogue("Candace: [E|Shock]I thought you had died, but you were living in an orphanage.[P][E|Happy] Look.[P] Look, here, see!?![P] This is you. ")
        fnDialogue("Thought: She shoves a newspaper in my hands.")
        fnDialogue("Thought: Words on newspapers are still too small for me to read, but I can clearly see the photograph. ")
        fnDialogue("Laura: Holy...! That is your face right there.")
        fnDialogue("[HIDEALL][Music|Somber][IMAGE|Root/Images/MajorScenes/All/Missing]Thought: I can't accept it as anything more than a photo of a stranger.")
        fnDialogue("Thought: Someone smiling in a place I've never been, amongst people I don't recognize.[P][P] Awake when I was supposed to be asleep.")
        fnDialogue("Thought: I push the paper back to her.")
        fnDialogue("[CHARENTER|Candace][CHARENTER|Laura]Candace: [VOICE|Voice|Candace]Please, [PLAYERNAME].[P] I know this is a lot to take in. Some crazy random woman on the street, trying to convince you that you're family[P][P]--hahh...")
        fnDialogue("[IMAGE|Null]Thought: She lets out a short, sorrowful laugh.")
        fnDialogue("Candace: [E|Sad]Actually, it's quite ironic.[P] You--nevermind, I'll explain later.")
        fnDialogue("Candace: [E|Mad] But you should know now that there are people in this town who play with lives like its a game.")
        fnDialogue("Candace: I've been caught up in it, and I know you are too.")
        fnDialogue("Thought: My face betrays a momentary recognition.[P] I try to look away, but I know Miss Foster saw it.")
        fnDialogue("Thought: She says just what I was afraid she might, the words terrifying and ever so tempting.")
        fnDialogue("Candace: [E|Sad]If you come with me,[P][P][ITALICS] I'll tell you everything.[ENDITALICS] ")
        fnDialogue("Candace: [E|Shock]I know you're desperate for answers.[P] But I have to keep you safe, too.")
        fnDialogue("Candace: [E|Happy]That's all I want, to keep you safe![P] Please, please, listen to me [PLAYERNAME].")
        fnDialogue("Laura: No way. Miss Foster... [PLAYERNAME], you can't be serious. [PLAYERNAME]?")
        fnDialogue("Thought: Miss Foster's hand extends to me.[P] I just stare through it, my whole body numb.")
        fnDialogue("Info: One of the following options leads to a game ending.")
        fnDialogue("Info: You won't be able to go back and choose again, so pick carefully!")
    end

    -- |[Common]|
    fnDialogue("Thought: (What do I do?)")

    local sDecisionScript = "\"" .. gsRoot .. "../Chapter 4/Gameplay_SceneD/1 - Big Choice.lua\""
    fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Read Foster's Mind\", " .. sDecisionScript .. ", \"ReadMind\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Leave with Miss Foster\", " .. sDecisionScript .. ", \"GetOuttaDodge\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Stay in Foster Falls\", " .. sDecisionScript .. ", \"StayInTown\") ")
    fnCutsceneBlocker()
end
