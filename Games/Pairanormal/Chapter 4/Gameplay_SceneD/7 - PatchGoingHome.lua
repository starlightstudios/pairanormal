-- |[Patch Going Home]|
--Brief script which is used when going home from the cemetary.

-- |[Arg Resolve]|
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

-- |[Execution]|
--Start.
if(sTopicName == "Default") then
	
	--Question:
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALL]Thought: (Should I head home now?)") ]])

	local sDecisionScript = "\"" .. gsRoot .. "../Chapter 4/Gameplay_SceneD/7 - PatchGoingHome.lua\""
	fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Go Home\", " .. sDecisionScript .. ", \"GoHome\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Not Yet\",  " .. sDecisionScript .. ", \"Dont\") ")
	fnCutsceneBlocker()
	return
	
--Don't go home just yet.
elseif(sTopicName == "Dont") then

	--Resume investigation mode.
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneBlocker()
	return
end

--Scene setup.
fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
fnCutsceneBlocker()
	
--Save the game.
if(PairanormalLevel_IsLoading() == false) then
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Starting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

--Set data.
else
	PairanormalLevel_FlagCheckpoint("Starting")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Museum")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Museum")
	fnDialogue([[ [Music|Daytime]Thought: ... ]])
end

--Flag.
Dia_SetProperty("Investigation Time Max",  -1)
PairanormalLevel_FlagCheckpoint("Starting")
fnCutsceneBlocker()

fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (Who do I walk home with?)") ]])

local sDecisionScript = "\"" .. gsRoot .. "../Chapter 4/Gameplay_SceneD/0 - Walk Home.lua\""
fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Mari\", " .. sDecisionScript .. ", \"Mari\") ")
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Stacy\", " .. sDecisionScript .. ", \"Stacy\") ")
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Bret\", " .. sDecisionScript .. ", \"Bret\") ")
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Michelle\", " .. sDecisionScript .. ", \"Michelle\") ")
fnCutsceneBlocker()