--[Chapter Variables]
--Variables that apply only to this chapter, or are used primarily in it.
DL_AddPath("Root/Variables/Chapter4/")
io.write("Booting chapter 4 variables.\n")

--[Miscellaneous]
DL_AddPath("Root/Variables/Chapter4/Misc/")

--[Common Investigation]
DL_AddPath("Root/Variables/Chapter4/Investigation/")
if(PairanormalLevel_IsLoading() == false) then
    VM_SetVar("Root/Variables/Chapter4/Investigation/iTimeMax", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter4/Investigation/iTimeLeft", "N", 0.0)
end

--[First Investigation]
--This refers to the museum investigation
DL_AddPath("Root/Variables/Chapter4/InvestigationA/")
if(PairanormalLevel_IsLoading() == false) then
    VM_SetVar( "Root/Variables/Chapter4/InvestigationA/iUnlockedDoor", "N", 0.0)
end

--Name Lookups
gciInv4A_Amethyst = 1
gciInv4A_OldPhotos = 2
gciInv4A_BigTownModel = 3
gciInv4A_DeerGod = 4
gciInv4A_JournalPages = 5
gciInv4A_KaneFoster = 6
gciInv4A_Wheelchair = 7
gciInv4A_NewspaperFire = 8
gciInv4A_NewspaperMeteorite = 9
gciInv4A_NewspaperClinic = 10
gciInv4A_Door = 11
gciInv4A_Mari = 12
gciInv4A_Bret = 13
gciInv4A_Michelle = 14
gciInv4A_Laura = 15
gciInv4A_Variable_Total = 15

--Position Data.
gczaInv4A_List = {}
gczaInv4A_List[gciInv4A_Amethyst]           = "Amethyst"
gczaInv4A_List[gciInv4A_OldPhotos]          = "OldPhotos"
gczaInv4A_List[gciInv4A_BigTownModel]       = "BigTownModel"
gczaInv4A_List[gciInv4A_DeerGod]            = "DeerGod"
gczaInv4A_List[gciInv4A_JournalPages]       = "JournalPages"
gczaInv4A_List[gciInv4A_KaneFoster]         = "KaneFoster"
gczaInv4A_List[gciInv4A_Wheelchair]         = "Wheelchair"
gczaInv4A_List[gciInv4A_NewspaperFire]      = "NewspaperFire"
gczaInv4A_List[gciInv4A_NewspaperMeteorite] = "NewspaperMeteorite"
gczaInv4A_List[gciInv4A_NewspaperClinic]    = "NewspaperClinic"
gczaInv4A_List[gciInv4A_Door]               = "Door"
gczaInv4A_List[gciInv4A_Mari]               = "Mari"
gczaInv4A_List[gciInv4A_Bret]               = "Bret"
gczaInv4A_List[gciInv4A_Michelle]           = "Michelle"
gczaInv4A_List[gciInv4A_Laura]              = "Laura"

--[Second Investigation]
--This refers to the soda factory investigation.
DL_AddPath("Root/Variables/Chapter4/InvestigationB/")
if(PairanormalLevel_IsLoading() == false) then
    VM_SetVar( "Root/Variables/Chapter4/InvestigationB/iIsLoginOn", "N", 0.0)
    VM_SetVar( "Root/Variables/Chapter4/InvestigationB/iIsLoginDone", "N", 0.0)
end

--Name Lookups
gciInv4B_KolaVault = 1
gciInv4B_SnackStock = 2
gciInv4B_TheAC = 3
gciInv4B_JanitorBits = 4
gciInv4B_Yamamoto = 5
gciInv4B_NewComputer = 6
gciInv4B_LinuxManual = 7
gciInv4B_Camera = 8
gciInv4B_Controls = 9
gciInv4B_Login = 10
gciInv4B_Terry = 11
gciInv4B_Ross = 12
gciInv4B_Laura = 13
gciInv4B_Phone = 14
gciInv4B_Variable_Total = 14

--Position Data.
gczaInv4B_List = {}
gczaInv4B_List[gciInv4B_KolaVault] = "KolaVault"
gczaInv4B_List[gciInv4B_SnackStock] = "SnackStock"
gczaInv4B_List[gciInv4B_TheAC] = "AirConditioning"
gczaInv4B_List[gciInv4B_JanitorBits] = "CleaningSupplies"
gczaInv4B_List[gciInv4B_Yamamoto] = "Yamamoto"
gczaInv4B_List[gciInv4B_NewComputer] = "NewComputer"
gczaInv4B_List[gciInv4B_LinuxManual] = "BiminiManual"
gczaInv4B_List[gciInv4B_Camera] = "Camera"
gczaInv4B_List[gciInv4B_Controls] = "Controls"
gczaInv4B_List[gciInv4B_Login] = "Login"
gczaInv4B_List[gciInv4B_Terry] = "Terry"
gczaInv4B_List[gciInv4B_Ross] = "Ross"
gczaInv4B_List[gciInv4B_Laura] = "Laura"
gczaInv4B_List[gciInv4B_Phone] = "Phone"

--[Backgrounds]
--List of backgrounds.
SLF_Open(fnResolvePath() .. "../../Datafiles/Backgrounds.slf")
local saList = {"Cliff", "Hallway", "Classroom", "Hallway", "Gym", "Cafe", "SchoolExterior", "SidewalkNight", "Gate", "Museum", "ComputerCloset", "ComputerClosetDark", "MuseumOffice", "WalkHome", "CandaceLivingRoom", "Car", "LivingRoomNight", "BedroomNight", "FlashbackCafe", "LibraryClear"}

for i = 1, #saList, 1 do
    DL_ExtractBitmap(saList[i], "Root/Images/Backgrounds/All/" .. saList[i])
end
SLF_Close()