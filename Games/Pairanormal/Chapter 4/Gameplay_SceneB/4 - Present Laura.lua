--[Laura Presentation]
--Showing stuff to Laura.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--Variables.
local sScriptPath = gsRoot .. "../Chapter 4/Gameplay_SceneB/4 - Present Laura.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
local sAutoAdvance = gsRoot .. "../Chapter 4/Gameplay_SceneD/7 - PatchGoingHome.lua"

--Saving
gsSaveExecute = gsRoot .. "../Chapter 4/Gameplay_SceneA/2 - Pizza Time.lua"
gsSaveTopic = "No Topic"
gsSaveFlag = "InvestigationMuseum"

--[ ========================================= Functions ========================================= ]
--[Friendship Increment]
--If this is the first time presenting this object to this character then we increase the friendship tracker for
-- that character.
local fnIncrementFriendship = function(iObjectIndex, iIncrement)
	
    --Arg check.
    if(iObjectIndex == nil) then return end
    if(iIncrement == nil) then return end
    
    --Build strings.
	local i = iObjectIndex
    local p = iLauraIndex
    local sObjectString = string.format("%02i", i)
    local sSubString    = string.format("%02i", p)
    local iIsRepeat = VM_GetVar("Root/Variables/Chapter4/InvestigationA/iObjectPresent|" .. sObjectString .. sSubString, "N")
    if(iIsRepeat == 0.0) then
        
        --Flag
        VM_SetVar("Root/Variables/Chapter4/InvestigationA/iObjectPresent|" .. sObjectString .. sSubString, "N", 1.0)
    
        --Variable gain.
        local iLauraFriend = VM_GetVar("Root/Variables/System/Romance/iLauraFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iLauraFriend", "N", iLauraFriend + iIncrement)
    end
end

--[Sequence Handler]
--Almost all sequences can use a standardized template.
local fnStdSequence = function(iFriendshipIndex, saStrings)
    
    --Start.
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    
    for i = 1, #saStrings, 1 do
		fnDialogue(saStrings[i])
    end
    
    --Finish.
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(iFriendshipIndex, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
end

--[ ======================================= Chat Sequences ====================================== ]
--Chatting.
if(sTopicName == "Chat") then
	
	--Variables.
	local iChattedWithLaura = VM_GetVar("Root/Variables/Chapter4/Investigation/iChattedWithLaura", "N")
	
	--First pass:
	if(iChattedWithLaura == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter4/Investigation/iChattedWithLaura", "N", 1.0)
		
		--Text.
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
    
		fnDialogue([[[HIDEALLFAST]Thought: I look solemly at the basically dead walkie talkie in my hands.]])
		fnDialogue([[[CHARENTER|Mari][CHARENTER|Stacy]Mari: [E|N]What's the matter, [PLAYERNAME]?]])
		fnDialogue([[Player: Oh, nothing...[P]Laura just hasn't been responding on here. ]])
		fnDialogue([[Stacy: [E|N]She probably left hers at home.[P] Don't worry, she's a big girl...[P][E|Blush] in case you're worried about how she's doing, hehe!]])
		fnDialogue([[Thought: [ITALICS]Nobody[ENDITALICS] seems to be all that worried that Laura was acting particularly unusual today.]])
		fnDialogue([[Thought: What could she have been up to?[P] Still, my cheeks are warm.]])
		fnDialogue([[Player: I was just...[P] you know, curious about stuff in the museum. ]])
		fnDialogue([[Mari: [E|Shock]Here, try texting her!]])
		fnDialogue([[Thought: Mari pulls out her phone and presses it into my free hand. It slides open with a satisfying *click* and the screen turns on.]])
		fnDialogue([[Mari: [E|N]Don't worry, it's got a full keyboard and text correct.[P] Just take your time.]])
		fnDialogue([[Player: ...]])
		fnDialogue([[[HIDEALLFAST]Thought: It takes a little experimentation and several presses of the tiny \"back\" key,[P] but I do manage to type out a message.]])
		fnDialogue([[Player: Hey Laura. How are you?]])
		fnDialogue([[Info: ...]])
		fnDialogue([[Laura: [SFX|World|Cellphone]oh woops lol hi mari! just saw this.]])
		fnDialogue([[Laura: [SFX|World|Cellphone]im doing great.]])
		fnDialogue([[Player: its [PLAYERNAME]. wish you were here!]])
		fnDialogue([[Thought: Laura doesn't respond...[P]but I guess she's okay after all.[P] And around if I need to ask questions.]])
        
        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
    
        --Variable gain.
        local iLauraFriend  = VM_GetVar("Root/Variables/System/Romance/iLauraFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iLauraFriend", "N", iLauraFriend + 5.0)
	
        --Time loss.
        if(fnDecrementTime(5, sAutoAdvance) == false) then
            PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
        end
		
	--Repeat passes.
	else
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
        
		fnDialogue([[[HIDEALLFAST]Player: Chicmonster didn't write this part.]])
        
        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
	end
	
--[ ==================================== Object Presentation ==================================== ]
elseif(sTopicName == "Amethyst") then
    fnStdObject(gciInv4A_Amethyst, 
    {"[HIDEALLFAST]Player: thers a gaint amathest hir.",
     "Laura: [SFX|World|Cellphone]the amethyst! its the largest [GLITCH]1 extracted from the mines. ",
     "Player: Ugh, the phone keeps bugging out when I get near this thing... "})

elseif(sTopicName == "OldPhotos") then
    fnStdObject(gciInv4A_OldPhotos, 
    {"[HIDEALLFAST]Player: charlies piza parler.",
     "Laura: [SFX|World|Cellphone]every1s favorite small business opened in 1972. their website is so old its still written purely in html. LOL!",
     "Thought: ...[P]what?"})

elseif(sTopicName == "BigTownModel") then
    fnStdObject(gciInv4A_BigTownModel, 
    {"[HIDEALLFAST]Player: beg toun littel model",
     "Laura: [SFX|World|Cellphone]???",
     "Thought: ...[P]I think I need to work on my spelling."})

elseif(sTopicName == "DeerGod") then
    fnStdObject(gciInv4A_DeerGod, 
    {"[HIDEALLFAST]Player: staffd deer hed",
     "Laura: [SFX|World|Cellphone]u must be talking about deerick! its good luck if u pat him on the head.",
     "Thought: I look at the glazed-over expression on the stuffed carcass.[P][P] [ITALICS]pass[ENDITALICS]."})

elseif(sTopicName == "JournalPages") then
    fnStdObject(gciInv4A_JournalPages, 
    {"[HIDEALLFAST]Player: jernal payg",
     "Laura: [SFX|World|Cellphone]???",
     "Thought: argh.[P] I think I need to work on my spelling."})

elseif(sTopicName == "KaneFoster") then
    fnStdObject(gciInv4A_KaneFoster, 
    {"[HIDEALLFAST]Player: foster painteng",
     "Laura: [SFX|World|Cellphone]u must be talking about that family portrait in the museum.",
     "Laura: [SFX|World|Cellphone]the museum website says it was painted by a lady named annabelle richardson in 1955."})

elseif(sTopicName == "Wheelchair") then
    fnStdObject(gciInv4A_Wheelchair, 
    {"[HIDEALLFAST]Player: weelchare",
     "Laura: [SFX|World|Cellphone]oh george foster jrs wheelchair?",
     "Laura: [SFX|World|Cellphone]1 member of the foster family was born paraplegic. but he didn't live 2 long anyway."})

elseif(sTopicName == "NewspaperFire") then
    fnStdObject(gciInv4A_NewspaperFire, 
    {"[HIDEALLFAST]Player: artekel",
     "Laura: [SFX|World|Cellphone]ask someone 2 read articles 2 u if u have questions."})

elseif(sTopicName == "NewspaperMeteorite") then
    fnStdObject(gciInv4A_NewspaperMeteorite, 
    {"[HIDEALLFAST]Player: meteorite?",
     "Laura: [SFX|World|Cellphone]u must mean the meteorite that fell in town.",
     "Thought: Wow, [P]I can't believe I actually spelled it right.",
     "Laura: [SFX|World|Cellphone]if I were able 2 see a shooting star up close like that... id wish 4 an intel 5 processor LOL.",
     "Laura: [SFX|World|Cellphone]My parents will never buy it 4 me ); ",
     "Player: ...[P]what?"})

elseif(sTopicName == "NewspaperClinic") then
    fnStdObject(gciInv4A_NewspaperClinic, 
    {"[HIDEALLFAST]Player: artekel",
     "Laura: [SFX|World|Cellphone]ask some1 2 read articles 2 u if u have Qs about it."})

elseif(sTopicName == "Door") then
    fnStdObject(gciInv4A_Door, 
    {"[HIDEALLFAST]Player: lkoked dor.",
     "Laura: [SFX|World|Cellphone]if a door is locked u should find the key silly!",
     "Thought: Good point.[P] Laura definitely can't help on this particular problem."})

--[ ======================================== Characters ========================================= ]
elseif(sTopicName == "Mari") then
    fnStdObject(gciInv4A_Mari, 
    {"[HIDEALLFAST]Player: mari",
     "Laura: [SFX|World|Cellphone]hey you spelled her name right!"})

elseif(sTopicName == "Bret") then
    fnStdObject(gciInv4A_Bret, 
    {"[HIDEALLFAST]Player: bret",
     "Laura: [SFX|World|Cellphone]he's a nurd jk"})

elseif(sTopicName == "Michelle") then
    fnStdObject(gciInv4A_Michelle, 
    {"[HIDEALLFAST]Player: michel",
     "Laura: [SFX|World|Cellphone]nah it's michelle lol!"})

elseif(sTopicName == "Laura") then
    fnStdObject(gciInv4A_Laura, 
    {"[HIDEALLFAST]Player: lorra",
     "Laura: [SFX|World|Cellphone]close enough!"})

end