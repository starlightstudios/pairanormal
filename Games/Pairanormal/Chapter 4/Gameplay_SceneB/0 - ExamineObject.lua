-- |[ ================================== Museum Investigation ================================== ]|
--The plot thickens.

-- |[ ======================================= Variables ======================================== ]|
-- |[Arguments]|
--Get the name of what we're investigating.
local sExamine = LM_GetScriptArgument(0)

-- |[Variable Setup]
--Used to simplify the Add Discovered Object calls.
local sBGPath = "Root/Images/Backgrounds/All/Museum"
local sScriptPath = gsRoot .. "../Chapter 4/Gameplay_SceneB/0 - ExamineObject.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
local sAutoAdvance = gsRoot .. "../Chapter 4/Gameplay_SceneD/7 - PatchGoingHome.lua"

--Saving
gsSaveExecute = gsRoot .. "../Chapter 4/Gameplay_SceneA/2 - Pizza Time.lua"
gsSaveTopic = "No Topic"
gsSaveFlag = "InvestigationMuseum"

-- |[ ======================================= Functions ======================================== ]|
--Stores the object as discovered.
local fnStdStore = function(i)
    
    --Get if we've examined the object before. This will be returned later.
	local iExaminedAlready = VM_GetVar("Root/Variables/Chapter4/InvestigationA/iObject|" .. gczaInv4A_List[i], "N")
    
    --Save it as a discovered object.
	Dia_SetProperty("Add Discovered Object", gczaInv4A_List[i], sBGPath, sScriptPath, gczaInv4A_List[i])
	VM_SetVar("Root/Variables/Chapter3/InvestigationA/iObject|" .. gczaInv4A_List[i], "N", 1.0)
    
    --Pass back.
    return iExaminedAlready
end

--Decrements time. Also saves the game.
local fnStdTime = function()
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(gsSaveFlag, gsSaveExecute, gsSaveTopic)
    end
    
end

--Standard handler for objects. Saves the object, prints a dialogue, advances time.
local fnStdObject = function(iIndex, saStrings)
	
    --Argument check.
    if(iIndex == nil) then return end
    
	--Store it in the inventory.
	local i = iIndex
	local iExaminedAlready = fnStdStore(i)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
    
    for i = 1, #saStrings, 1 do
		fnDialogue(saStrings[i])
    end
    
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
    if(iExaminedAlready == 0.0) then fnStdTime(sSaveFlag, sSaveExecute, sSaveTopic) end
end

-- |[ =================================== Examinable Objects =================================== ]|
if(sExamine == "Amethyst") then
    fnStdObject(gciInv4A_Amethyst, 
    {"[HIDEALLFAST]Thought: This thing is huge.[P][GLITCH] I feel kind of... drawn[GLITCH][GLITCH] to it.",
     "Thought: Hard to believe they[GLITCH][GLITCHTEXT]hA R D T O BELIEVE TH[ENDGLITCHTEXT][GLITCH] just dig these things out of the ground."})

elseif(sExamine == "OldPhotos") then
    fnStdObject(gciInv4A_OldPhotos, 
    {"[HIDEALLFAST]Thought: I'm sure these all have some significance to the town.[P] But not meeeeee."})

elseif(sExamine == "BigTownModel") then
    fnStdObject(gciInv4A_BigTownModel, 
    {"[HIDEALLFAST]Thought: Everything is so tiny.[P][P] [ITALICS]I love it.[ENDITALICS]"})

elseif(sExamine == "DeerGod") then
    fnStdObject(gciInv4A_DeerGod, 
    {"[HIDEALLFAST]Thought: Is...[P]is the body on the other side of the wall?"})

elseif(sExamine == "JournalPages") then
    fnStdObject(gciInv4A_JournalPages, 
    {"[HIDEALLFAST]Thought: A journal page, but it's behind a frame."})

elseif(sExamine == "KaneFoster") then
    fnStdObject(gciInv4A_KaneFoster, 
    {"[HIDEALLFAST]Thought: [GLITCH]Looks just like him. "})

elseif(sExamine == "Wheelchair") then
    fnStdObject(gciInv4A_Wheelchair, 
    {"[HIDEALLFAST]Thought: Weird thing to have in a museum, but what do I know?"})

elseif(sExamine == "NewspaperFire") then
    fnStdObject(gciInv4A_NewspaperFire, 
    {"[HIDEALLFAST]Thought: This probably has something to do with the old factory in town."})

elseif(sExamine == "NewspaperMeteorite") then
    fnStdObject(gciInv4A_NewspaperMeteorite, 
    {"[HIDEALLFAST]Thought: A big meteorite falling must have been a sight to see if you were alive at the time!"})

elseif(sExamine == "NewspaperClinic") then
    fnStdObject(gciInv4A_NewspaperClinic, 
    {"[HIDEALLFAST]Thought: This is about some kind of opening ceremony, but I can't tell what for."})

elseif(sExamine == "Door") then
    LM_ExecuteScript(fnResolvePath() .. "Z - Door.lua", "Check")

-- |[ ======================================= Characters ======================================= ]|
elseif(sExamine == "Mari") then
    fnStdObject(gciInv4A_Mari, 
    {"Thought: [HIDEALLFAST]Mari went right to the trashcan and started digging. I don't think she's going to find anything like that.",
	"Thought: Meanwhile, Stacy's looking kinda restless. There's nothing to jump over or climb in a museum, after all."})

elseif(sExamine == "Bret") then
    fnStdObject(gciInv4A_Bret, 
    {"Thought: [HIDEALLFAST]Bret seems pretty fascinated with that tiny town.[P] I'd have thought he'd have seen it plenty of times already."})

elseif(sExamine == "Michelle") then
    fnStdObject(gciInv4A_Michelle, 
    {"Thought: [HIDEALLFAST]Michelle is totally stealing the quartz on display."})

elseif(sExamine == "Laura") then
    fnStdObject(gciInv4A_Laura, 
    {"Thought: [HIDEALLFAST]Laura's off alone at the library, and hasn't answered the walkie talkie.[P] Is she okay?"})

end