--[Michelle Presentation]
--Showing stuff to Michelle.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--Variables.
local sScriptPath = gsRoot .. "../Chapter 4/Gameplay_SceneB/5 - Present Michelle.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
local sAutoAdvance = gsRoot .. "../Chapter 4/Gameplay_SceneD/7 - PatchGoingHome.lua"

--Saving
gsSaveExecute = gsRoot .. "../Chapter 4/Gameplay_SceneA/2 - Pizza Time.lua"
gsSaveTopic = "No Topic"
gsSaveFlag = "InvestigationMuseum"

--[ ========================================= Functions ========================================= ]
--[Friendship Increment]
--If this is the first time presenting this object to this character then we increase the friendship tracker for
-- that character.
local fnIncrementFriendship = function(iObjectIndex, iIncrement)
	
    --Arg check.
    if(iObjectIndex == nil) then return end
    if(iIncrement == nil) then return end
    
    --Build strings.
	local i = iObjectIndex
    local p = iMichelleIndex
    local sObjectString = string.format("%02i", i)
    local sSubString    = string.format("%02i", p)
    local iIsRepeat = VM_GetVar("Root/Variables/Chapter4/InvestigationA/iObjectPresent|" .. sObjectString .. sSubString, "N")
    if(iIsRepeat == 0.0) then
        
        --Flag
        VM_SetVar("Root/Variables/Chapter4/InvestigationA/iObjectPresent|" .. sObjectString .. sSubString, "N", 1.0)
    
        --Variable gain.
        local iMichelleFriend  = VM_GetVar("Root/Variables/System/Romance/iMichelleFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iMichelleFriend", "N", iMichelleFriend + iIncrement)
    end
end

--[Sequence Handler]
--Almost all sequences can use a standardized template.
local fnStdSequence = function(iFriendshipIndex, saStrings)
    
    --Start.
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    
    for i = 1, #saStrings, 1 do
		fnDialogue(saStrings[i])
    end
    
    --Finish.
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(iFriendshipIndex, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
end

--[ ======================================= Chat Sequences ====================================== ]
--Chatting.
if(sTopicName == "Chat") then
	
	--Variables.
	local iChattedWithMichelle = VM_GetVar("Root/Variables/Chapter4/Investigation/iChattedWithMichelle", "N")
	
	--First pass:
	if(iChattedWithMichelle == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter4/Investigation/iChattedWithMichelle", "N", 1.0)
		
		--Text.
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
    
		fnDialogue([[[HIDEALLFAST][CHARENTER|Michelle]Player: Hey Michelle.]])
		fnDialogue([[Thought: She takes her eyes off the dimly lit walls for a moment.[P] Her usually strong voice only whispers to me, and her eyes caution me to do the same.]])
		fnDialogue([[Michelle: [E|N][ITALICS]Hey.[ENDITALICS] ]])
		fnDialogue([[Thought: Being loud would obviously start a dangerous ripple in the air, muddying the solemn spell of concentration around us, making it harder to see clearly.]])
		fnDialogue([[Player: [ITALICS]Find anything interesting yet?[ENDITALICS] ]])
		fnDialogue([[Michelle: [E|N][ITALICS]Nah.[P] But I'm still looking[ENDITALICS].]])
		fnDialogue([[Thought: We stand together, listening to quiet crinkles as the club gently rifles through the museum's artifacts.]])
		fnDialogue([[Thought: Something about this atmosphere sinks Michelle's broad shoulders low, and the tiniest quirk plays up on the corners of her full, dark mouth.]])
		fnDialogue([[Thought: Our eyes accidentally meet.]])
		fnDialogue([[Michelle: [E|N][ITALICS]What?[ITALICS] ]])
		fnDialogue([[Player: [ITALICS]What're you smiling for?[ENDITALICS] ]])
		fnDialogue([[Michelle: [E|N][P][P][E|Shock][P][P][E|Blush][ITALICS]I-[P]I wasn't smiling![ENDITALICS] ]])
		fnDialogue([[Player: [ITALICS]I thought you wanted to go to the arcade.[P] Buy one get two free pizza.[ENDITALICS] ]])
		fnDialogue([[Michelle: [E|Sad][ITALICS]I was grimacing.[P] It was a grimace.[ITALICS] ]])
		fnDialogue([[Player: [ITALICS]You're having a good time, admit it.[ENDITALICS] ]])
		fnDialogue([[Michelle: [E|Mad][ITALICS]I'll admit my boot's about to go up your ass if you don't leave me alone![ENDITALICS] ]])
		fnDialogue([[Player: [ITALICS]Fine, fine.[P][P] But I saw what I saw.[ENDITALICS] ]])
		fnDialogue([[Thought: For all her posturing, Michelle's a member of the club as much as anyone else.[P] I'll leave her to her investigation. ]])

        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
    
        --Variable gain.
        local iMariFriend  = VM_GetVar("Root/Variables/System/Romance/iMariFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iMariFriend", "N", iMariFriend + 5.0)
	
        --Time loss.
        if(fnDecrementTime(5, sAutoAdvance) == false) then
            PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
        end
		
	--Repeat passes.
	else
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
        
		fnDialogue("[HIDEALLFAST][CHARENTER|Michelle]Michelle: [E|N]Okay. What did you find?")
		fnDialogue("Player: There's that smile again. [P]You're totally enjoying this!")
		fnDialogue("Michelle: [E|Blush]Oh I can't get a bloody break with you.[P] Show me something cool or piss off!")

        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
	end
	
--[ ==================================== Object Presentation ==================================== ]
elseif(sTopicName == "Amethyst") then
    fnStdSequence(gciInv4A_Amethyst, 
    {"[HIDEALLFAST][SHOWCHAR|Michelle]Michelle: [E|Happy]That's a pretty sick rock.[P] I still haven't figured out how to steal it yet."})

elseif(sTopicName == "OldPhotos") then
    fnStdSequence(gciInv4A_OldPhotos, 
    {"[HIDEALLFAST][SHOWCHAR|Michelle]Michelle: [E|N]Heh, that's the parlor, back before they got grease in the carpets, I'll bet."})

elseif(sTopicName == "BigTownModel") then
    fnStdSequence(gciInv4A_BigTownModel, 
    {"[HIDEALLFAST][SHOWCHAR|Michelle]Thought: Michelle stares at me with a severity that makes me burn.",
     "Michelle: [E|Blush]You ever...[P] get the urge?",
     "Player: Wh-what?[P] What urge?",
     "Michelle: [E|Blush]You know, the [ITALICS]urge.[ENDITALICS][P][P][E|Happy] To stomp on it.",
     "Player: Oh.[P][SFX|Ugh] Uh, no.[P] Can't say that I have."})

elseif(sTopicName == "DeerGod") then
    fnStdSequence(gciInv4A_DeerGod, 
    {"[HIDEALLFAST][SHOWCHAR|Michelle]Michelle: [E|Mad]NOT.[P] a fan of hunting."})

elseif(sTopicName == "JournalPages") then
    fnStdSequence(gciInv4A_JournalPages, 
    {"[HIDEALLFAST][SHOWCHAR|Michelle]Michelle: [E|Blush]You've got that sad look in your eye again--all right, calm down.[P] I'll read the bloody thing.",
     "Thought: Michelle leans towards one of the framed sheets, lips pursed in concentration.",
     [[ Michelle: [E|N]\"Another classic Foster family argument tonight. [P]Does Kane really think he can build an empire with a wink and a smile?\" ]],
     [[Michelle: \"Who on earth would want to buy land here, especially when the mines are doing so poorly.\" ]],
     [[Michelle: \"Even more dismaying, Father has stopped dismissing him outright.[P] In fact, he's started listening.\" ]],
     "Player: Sibling rivalries.[P] Are they really that bad?",
     "Michelle: [E|Happy]I wouldn't know. I was told I might've had a brother, but I ate him in the womb."})

elseif(sTopicName == "KaneFoster") then
    fnStdSequence(gciInv4A_KaneFoster, 
    {"[HIDEALLFAST][SHOWCHAR|Michelle]Michelle: [E|N]I wonder if that's what he actually looked like, or if he asked the painter to...[P] you know.[P][E|Happy] Shave off a few pounds.",
     "Player: Oh, believe me, it's accurate.",
     "Thought: Her eyes narrow.",
     "Michelle: [E|Mad]How do you know?",
     "Player: Ah--[P]j-just a guess![P] Ooh, what's that over there?[P][CRASH] Shiny!"})

elseif(sTopicName == "Wheelchair") then
    fnStdSequence(gciInv4A_Wheelchair, 
    {"[HIDEALLFAST][SHOWCHAR|Michelle]Player: Should we sit on it?[P] There's no one here to stop us.",
     "Michelle: [E|Mad]Are you mental? That's downright disrespectful.",
     "Player: [SFX|Ugh]Oh, right, right.[P] Sorry.",
     "Michelle: [E|Blush]Also, there's no [ITALICS]way[ENDITALICS] I'm putting my ass where some old fart DIED. "})

elseif(sTopicName == "NewspaperFire") then
    fnStdSequence(gciInv4A_NewspaperFire, 
    {"[HIDEALLFAST][SHOWCHAR|Michelle]Michelle: [E|Happy]Mmm, whatever it is, I don't care."})

elseif(sTopicName == "NewspaperMeteorite") then
    fnStdSequence(gciInv4A_NewspaperMeteorite, 
    {"[HIDEALLFAST][SHOWCHAR|Michelle]Michelle: [E|Happy]If I could make a wish on a falling star, it would obviously be for super strength.[P] Then nobody could mess with me.",
     "Player: That doesn't make any sense.[P] You're already super strong.",
     "Michelle: [P][P][E|Blush]Oy, q-quit poking my arms!!"})

elseif(sTopicName == "NewspaperClinic") then
    fnStdSequence(gciInv4A_NewspaperClinic, 
    {[[ [HIDEALLFAST][SHOWCHAR|Michelle]Michelle: [E|Happy]\"Women's Clinic\", BAHAHAHA! What a load of bullshit.[P] You know it's still open, right? ]],
     "Player: Uhhh...",
     "Michelle: [E|N]And it's the only one in town.[P] If you need [ITALICS]actual[ENDITALICS] care, you'd have to drive two counties out to see a real bloody doctor.",
     "Michelle: [E|Happy] Imagine how many lives have been ruined because someone couldn't be bothered to go on a road trip.[P] Hilarious!"})

elseif(sTopicName == "Door") then
    fnStdSequence(gciInv4A_Door, 
    {"[HIDEALLFAST][SHOWCHAR|Michelle]Michelle: [E|Shock]Who drew a pizza on the door?",
     "Player: Oh, is that what it is?",
     "Thought: She shrugs."})

--[ ======================================== Characters ========================================= ]
elseif(sTopicName == "Mari") then
    fnStdSequence(gciInv4A_Mari, 
    {"[HIDEALLFAST][SHOWCHAR|Michelle]Michelle: Leader of the geek squad. Which unfortunately makes me a geek."})

elseif(sTopicName == "Bret") then
    fnStdSequence(gciInv4A_Bret, 
    {"[HIDEALLFAST][SHOWCHAR|Michelle]Michelle: [E|Happy]Did I ever tell you about the time I choked him unconscious? He doesn't remember it for some reason."})

elseif(sTopicName == "Michelle") then
    fnStdSequence(gciInv4A_Michelle, 
    {"[HIDEALLFAST][SHOWCHAR|Michelle]Michelle: Yeah, what is it?"})

elseif(sTopicName == "Laura") then
    fnStdSequence(gciInv4A_Laura, 
    {"[HIDEALLFAST][SHOWCHAR|Michelle]Michelle: Hope she's doing all right."})

end