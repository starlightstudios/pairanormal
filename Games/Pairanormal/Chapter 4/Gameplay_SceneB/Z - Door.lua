--[Archives Door]
--Input the code to advance.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--[Check]
--First time the player inputs something.
if(sTopicName == "Check") then
	
    --If the player has unlocked the gate:
    local iUnlockedDoor = VM_GetVar("Root/Variables/Chapter4/InvestigationA/iUnlockedDoor", "N")
    if(iUnlockedDoor == 1.0) then

        fnDialogue([[[HIDEALLFAST]Thought: (Go through the door?)]])
        local sDecisionScript = "\"" .. gsRoot .. "../Chapter 4/Gameplay_SceneB/Z - Door.lua\""
        fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Keep Exploring\", " .. sDecisionScript .. ", \"Explore\") ")
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Go Inside\", " .. sDecisionScript .. ", \"Inside\") ")
        fnCutsceneBlocker()
    
    --Prompt them to enter the code:
    else

        --Setup.
        gsCodeSoFar = ""
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
        fnDialogue([[[HIDEALLFAST]Thought: It's locked with a number key. There's a weird drawing taped over the handle.]])
        fnDialogue([[Thought: (First Number?)]])

        local sDecisionScript = "\"" .. gsRoot .. "../Chapter 4/Gameplay_SceneB/Z - Door.lua\""
        fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"0\", " .. sDecisionScript .. ", \"First 0\") ")
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"1\", " .. sDecisionScript .. ", \"First 1\") ")
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"2\", " .. sDecisionScript .. ", \"First 2\") ")
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"3\", " .. sDecisionScript .. ", \"First 3\") ")
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"4\", " .. sDecisionScript .. ", \"First 4\") ")
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"5\", " .. sDecisionScript .. ", \"First 5\") ")
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"6\", " .. sDecisionScript .. ", \"First 6\") ")
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"7\", " .. sDecisionScript .. ", \"First 7\") ")
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"8\", " .. sDecisionScript .. ", \"First 8\") ")
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"9\", " .. sDecisionScript .. ", \"First 9\") ")
        fnCutsceneBlocker()
    end

--[First Key Input]
elseif(string.sub(sTopicName, 1, 5) == "First") then

    --Storage
    gsCodeSoFar = gsCodeSoFar .. string.sub(sTopicName, 7, 7)

    --Dialogue
    fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "*Click*") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (Second Number?)") ]])

    local sDecisionScript = "\"" .. gsRoot .. "../Chapter 4/Gameplay_SceneB/Z - Door.lua\""
    fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"0\", " .. sDecisionScript .. ", \"Second 0\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"1\", " .. sDecisionScript .. ", \"Second 1\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"2\", " .. sDecisionScript .. ", \"Second 2\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"3\", " .. sDecisionScript .. ", \"Second 3\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"4\", " .. sDecisionScript .. ", \"Second 4\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"5\", " .. sDecisionScript .. ", \"Second 5\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"6\", " .. sDecisionScript .. ", \"Second 6\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"7\", " .. sDecisionScript .. ", \"Second 7\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"8\", " .. sDecisionScript .. ", \"Second 8\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"9\", " .. sDecisionScript .. ", \"Second 9\") ")
    fnCutsceneBlocker()

--[Second Key Input]
elseif(string.sub(sTopicName, 1, 6) == "Second") then

    --Storage
    gsCodeSoFar = gsCodeSoFar .. string.sub(sTopicName, 8, 8)

    --Dialogue
    fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "*Click*") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (Third Number?)") ]])

    local sDecisionScript = "\"" .. gsRoot .. "../Chapter 4/Gameplay_SceneB/Z - Door.lua\""
    fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"0\", " .. sDecisionScript .. ", \"Third 0\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"1\", " .. sDecisionScript .. ", \"Third 1\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"2\", " .. sDecisionScript .. ", \"Third 2\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"3\", " .. sDecisionScript .. ", \"Third 3\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"4\", " .. sDecisionScript .. ", \"Third 4\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"5\", " .. sDecisionScript .. ", \"Third 5\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"6\", " .. sDecisionScript .. ", \"Third 6\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"7\", " .. sDecisionScript .. ", \"Third 7\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"8\", " .. sDecisionScript .. ", \"Third 8\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"9\", " .. sDecisionScript .. ", \"Third 9\") ")
    fnCutsceneBlocker()

--[Third Key Input]
elseif(string.sub(sTopicName, 1, 5) == "Third") then

    --Storage
    gsCodeSoFar = gsCodeSoFar .. string.sub(sTopicName, 7, 7)

    --Dialogue
    fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "*Click*") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (Fourth Number?)") ]])

    local sDecisionScript = "\"" .. gsRoot .. "../Chapter 4/Gameplay_SceneB/Z - Door.lua\""
    fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"0\", " .. sDecisionScript .. ", \"Fourth 0\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"1\", " .. sDecisionScript .. ", \"Fourth 1\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"2\", " .. sDecisionScript .. ", \"Fourth 2\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"3\", " .. sDecisionScript .. ", \"Fourth 3\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"4\", " .. sDecisionScript .. ", \"Fourth 4\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"5\", " .. sDecisionScript .. ", \"Fourth 5\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"6\", " .. sDecisionScript .. ", \"Fourth 6\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"7\", " .. sDecisionScript .. ", \"Fourth 7\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"8\", " .. sDecisionScript .. ", \"Fourth 8\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"9\", " .. sDecisionScript .. ", \"Fourth 9\") ")
    fnCutsceneBlocker()

--[Fourth Key Input]
elseif(string.sub(sTopicName, 1, 6) == "Fourth") then

    --Storage
    gsCodeSoFar = gsCodeSoFar .. string.sub(sTopicName, 8, 8)

    --Dialogue
    fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "*Click*") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: Let's see...") ]])
    
    --Correct!
    if(gsCodeSoFar == "1972") then
        fnDialogue([[[MUSIC|Null]Info: [SFX|Beep]Thought: The door clicks open.]])
        fnDialogue([[Player: Uh...[P] guys?]])
        fnDialogue([[Thought: Everyone seems invested in their own thing at the moment--[P]I peer in.]])
        fnDialogue([[Thought: There's nothing in here but a boring hallway, capped by a single small office.]])
        fnDialogue([[Thought: (Go inside?)]])

        local sDecisionScript = "\"" .. gsRoot .. "../Chapter 4/Gameplay_SceneB/Z - Door.lua\""
        fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Keep Exploring\", " .. sDecisionScript .. ", \"Explore\") ")
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Go Inside\", " .. sDecisionScript .. ", \"Inside\") ")
        fnCutsceneBlocker()
        
    --NOPE!
    else
        fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: Huh, nope. That didn't work.") ]])
        fnCutsceneBlocker()
        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
    
    end

--[Don't Go Inside]
elseif(sTopicName == "Explore") then
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALL]Thought: I'll keep looking around before going in there, if at all.") ]])
	fnCutsceneBlocker()
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()

--[Go Inside, The Suspense Is Killing Me]
elseif(sTopicName == "Inside") then
    LM_ExecuteScript(fnResolvePath() .. "Z - Office of Mystery.lua", "Start")
end
