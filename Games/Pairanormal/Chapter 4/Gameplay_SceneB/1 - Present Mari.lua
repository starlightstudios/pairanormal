-- |[Mari Presentation]|
--Showing stuff to Mari.

-- |[Arg Resolve]|
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--Variables.
local sScriptPath = gsRoot .. "../Chapter 4/Gameplay_SceneB/1 - Present Mari.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
local sAutoAdvance = gsRoot .. "../Chapter 4/Gameplay_SceneD/7 - PatchGoingHome.lua"

--Saving
gsSaveExecute = gsRoot .. "../Chapter 4/Gameplay_SceneA/2 - Pizza Time.lua"
gsSaveTopic = "No Topic"
gsSaveFlag = "InvestigationMuseum"

-- |[ ======================================= Functions ======================================== ]|
-- |[Friendship Increment]|
--If this is the first time presenting this object to this character then we increase the friendship tracker for
-- that character.
local fnIncrementFriendship = function(iObjectIndex, iIncrement)
	
    --Arg check.
    if(iObjectIndex == nil) then return end
    if(iIncrement == nil) then return end
    
    --Build strings.
	local i = iObjectIndex
    local p = iMariIndex
    local sObjectString = string.format("%02i", i)
    local sSubString    = string.format("%02i", p)
    local iIsRepeat = VM_GetVar("Root/Variables/Chapter4/InvestigationA/iObjectPresent|" .. sObjectString .. sSubString, "N")
    if(iIsRepeat == 0.0) then
        
        --Flag
        VM_SetVar("Root/Variables/Chapter4/InvestigationA/iObjectPresent|" .. sObjectString .. sSubString, "N", 1.0)
    
        --Variable gain.
        local iMariFriend  = VM_GetVar("Root/Variables/System/Romance/iMariFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iMariFriend", "N", iMariFriend + iIncrement)
    end
end

-- |[Sequence Handler]|
--Almost all sequences can use a standardized template.
local fnStdSequence = function(iFriendshipIndex, saStrings)
    
    --Start.
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    
    for i = 1, #saStrings, 1 do
		fnDialogue(saStrings[i])
    end
    
    --Finish.
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(iFriendshipIndex, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
end

-- |[ ===================================== Chat Sequences ===================================== ]|
--Chatting.
io.write("Mari chat: " .. sTopicName .. "\n")
if(sTopicName == "Chat") then
	
	--Variables.
	local iChattedWithMari = VM_GetVar("Root/Variables/Chapter4/Investigation/iChattedWithMari", "N")
	
	--First pass:
	if(iChattedWithMari == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter4/Investigation/iChattedWithMari", "N", 1.0)
		
		--Text.
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
    
		fnDialogue([[[HIDEALLFAST][CHARENTER|Mari][CHARENTER|Stacy]Mari: [E|Happy]Stacy,[P] remind me NEVER to take us Charlie's Pizza Parlor again!]])
		fnDialogue([[Thought: Mari's practically vibrating with excitement;[P] her wide eyes scan every dark corner like they hold the key to unlocking the ultimate paranormal mystery.]])
		fnDialogue([[Thought: I'm not sure my own racing pulse is the exact same feeling...]])
		fnDialogue([[Thought: --any minute now, someone's bound to catch us breaking in to this museum, and then what?[P] Will we be shot?[P] Hung?[P] Tied to a rail and fed to wild animals?]])
		fnDialogue([[Thought: That's what they did to all the criminals in our history lessons.]])
		fnDialogue([[Stacy: [E|Shock]Well technically, we wouldn't have ended up in here unless we hadn't planned on going to Charlie's.]])
		fnDialogue([[Mari: [E|N]Ah. Good point.[P] Then Stacy, [P][CRASH][E|Shock]REMIND ME TO ALWAYS TAKE US TO CHARLIES PIZZA PARLOR!]])
		fnDialogue([[Stacy: [E|N]Haha, sure.]])
		fnDialogue([[Player: So, we're looking for documents about \"missing people\".[P] Who are they?]])
		fnDialogue([[Thought: Mari and Stacy search each other, but both seem to come up empty.]])
		fnDialogue([[Mari: [E|N]I [P]don't [P]know.[P][P][P][E|Shock] I mean, a missing persons case is HUGE![P][E|Sad] Why haven't we about it from previous paranormal investigations?]])
		fnDialogue([[Thought: Like water draining from a deep well, all the joy seems to sink away from Mari's eyes.]])
		fnDialogue([[Mari: [E|Sad] E-Everything is connected, as you know. I... we're a very thorough organization.]])
		fnDialogue([[Stacy: [E|Shock]I got it![P][E|Happy] It must've happened recently, so no one's found out about it yet.]])
		fnDialogue([[Player: I thought that museum lady said she's been working on this story for years, though.]])
		fnDialogue([[Mari: [E|Sad]*sigh*...[PLAYERNAME]'s right.[P] Besides, we know everyone in this town.]])
		fnDialogue([[Mari: I think it'd be pretty noticeable if one of them just [ITALICS]went missing[ENDITALICS].]])
		fnDialogue([[Stacy: [E|Sad]Mm, good point.]])
		fnDialogue([[Player: [SFX|Ugh]Hmm...]])
		fnDialogue([[Stacy: [E|N]Don't worry about it Mari.[P] There's only one way to find out the truth.]])
		fnDialogue([[Stacy: We just have to look at the documents and see for ourselves!]])
		fnDialogue([[Thought: Mari takes a deep breath, the worried crease in her brow relaxing.]])
		fnDialogue([[Mari: [E|N]Right!]])
		fnDialogue([[Player: I'll do my best to help, then.[P] We'll get to the bottom of it!]])

        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
    
        --Variable gain.
        local iMariFriend  = VM_GetVar("Root/Variables/System/Romance/iMariFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iMariFriend", "N", iMariFriend + 5.0)
	
        --Time loss.
        if(fnDecrementTime(5, sAutoAdvance) == false) then
            PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
        end
		
	--Repeat passes.
	else
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
        
		fnDialogue("[HIDEALLFAST][CHARENTER|Mari][CHARENTER|Stacy]Player: Don't worry, team.[P] We'll find those documents and learn the truth.")
		fnDialogue("Stacy: [E|Happy]That's the spirit, [PLAYERNAME]!")
		fnDialogue("Mari: [E|Blush]*sniff*[P][P] My little baby paranormal investigator is all grown up!!!")
        
        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
	end
	
-- |[ ================================== Object Presentation =================================== ]|
elseif(sTopicName == "Amethyst") then
    fnStdSequence(gciInv4A_Amethyst, 
    {"[HIDEALLFAST][SHOWCHAR|Mari][SHOWCHAR|Stacy]Mari: [E|N]God, I LOVE.[P][E|Shock] This amethyst!!!",
     "Player: It's pretty, all right.",
     "Mari: [E|Mad][CRASH]DO YOU KNOW HOW MUCH SPIRITUAL ENERGY I COULD CHARGE INTO THIS SUCKER?",
     "Stacy: [E|N]I bet if we got it under a full moon, I'd be SO powerful I could like,[P] kill a man with just a look.",
     "Player: ... Ah."})

elseif(sTopicName == "OldPhotos") then
    fnStdSequence(gciInv4A_OldPhotos, 
    {"[HIDEALLFAST][SHOWCHAR|Mari][SHOWCHAR|Stacy]Stacy: [E|Happy]Awww, Charlie's pizza! I really like this photo."})

elseif(sTopicName == "BigTownModel") then
    fnStdSequence(gciInv4A_BigTownModel, 
    {"[HIDEALLFAST][SHOWCHAR|Mari][SHOWCHAR|Stacy]Stacy: [E|N]Rock..",
     "Mari: [E|N]Paper...",
     "Stacy and Mari: [E|Mad][CRASH]SCISSORS!",
     "Thought: [ITALICS]Hmm, I don't think they're paying attention.[ENDITALICS]"})

elseif(sTopicName == "DeerGod") then
    fnStdSequence(gciInv4A_DeerGod, 
    {"[HIDEALLFAST][SHOWCHAR|Mari][SHOWCHAR|Stacy]Stacy: [E|N]*snicker* Oh god.",
     "Mari: [E|Happy]It's Deerick![P] *giggle*",
     "Stacy: [E|N]Good old Deerick.",
     "Player: Is that his name?",
     "Stacy: [E|N]Unofficially, yeah.[P][E|Happy] He's infamous for having a booger hanging out of his nose. "})

elseif(sTopicName == "JournalPages") then
    fnStdSequence(gciInv4A_JournalPages, 
    {[[ [HIDEALLFAST][SHOWCHAR|Mari][SHOWCHAR|Stacy]Stacy: Aha... \"Even in my rather pitiful preliminary tests,\" ]],
     [[ Stacy: \"the quartzified engine has outperformed the efficiency of the generator that runs father's mines by nearly 34 percent!\" ]],
     [[Stacy: \"My hands are practically shaking with excitement just writing such a claim down on paper.\" ]],
     [[Stacy: \"If I can make improvements to my model, Foster Oil and Ore may be saved after all.\" ]],
     "Player: What does all that mean?",
     "Stacy: [E|N][P][P][P][SFX|Ugh]I have no idea."})

elseif(sTopicName == "KaneFoster") then
    fnStdSequence(gciInv4A_KaneFoster, 
    {"[HIDEALLFAST][SHOWCHAR|Mari][SHOWCHAR|Stacy]Stacy: [E|N]Rock..",
     "Mari: [E|N]Paper...",
     "Stacy and Mari: [E|Mad][CRASH]SCISSORS!",
     "Thought: [ITALICS]Hmm, I don't think they're paying attention.[ENDITALICS]"})

elseif(sTopicName == "Wheelchair") then
    fnStdSequence(gciInv4A_Wheelchair, 
    {"[HIDEALLFAST][SHOWCHAR|Mari][SHOWCHAR|Stacy]Mari: [E|N]That belonged to George Foster, the older brother, I think?",
     "Stacy: [E|Shock]No, I think he's the younger one.",
     "Mari: [E|N]Ah welp, same difference. ",
     "Stacy: [E|Shock]No it's not![P] It's totally different.",
     "Mari: [E|Shock]I know it's not actually the same difference,[P] I just meant, like,[P][E|Sad] they're the same to me? ",
     "Stacy: [E|Shock]Like how?",
     "Mari: [E|N]Like I don't... really care about who was older.",
     "Stacy: [E|Sad]Mm. Me neither, now that you mention it. "})

elseif(sTopicName == "NewspaperFire") then
    fnStdSequence(gciInv4A_NewspaperFire, 
    {"[HIDEALLFAST][SHOWCHAR|Mari][SHOWCHAR|Stacy]Stacy: [E|N]Rock..",
     "Mari: [E|N]Paper...",
     "Stacy and Mari: [E|Mad][CRASH]SCISSORS!",
     "Thought: [ITALICS]Hmm, I don't think they're paying attention.[ENDITALICS]"})

elseif(sTopicName == "NewspaperMeteorite") then
    fnStdSequence(gciInv4A_NewspaperMeteorite, 
    {"[HIDEALLFAST][SHOWCHAR|Mari][SHOWCHAR|Stacy]Mari: [E|Shock]Oh man, can you imagine seeing a frickin' [CRASH]ASTEROID landing in town!?",
     "Stacy: [E|N]It's actually called a Meteor if it enters earth's atmosphere.[P][E|Blush] What would you wish for?",
     "Mari: [E|Mad][ITALICS]How about a friend who lets me make mistakes!?![ENDITALICS]",
     "Stacy: [E|Happy]Ahh, I'm sorry, Mari!!",
     "Mari: [E|Blush]I know, I'm just teasing.",
     "Mari: [E|N] Anyway it would DEFINITELY be to have Satan rise up from the underworld and like...[P] date me."})

elseif(sTopicName == "NewspaperClinic") then
    fnStdSequence(gciInv4A_NewspaperClinic, 
    {"[HIDEALLFAST][SHOWCHAR|Mari][SHOWCHAR|Stacy]Mari: [E|Mad]UGH!",
     "Player: I guess this article makes you pretty mad...[P]somehow?",
     "Stacy: [E|Mad]Double ugh.",
     "Mari: [E|Mad][CRASH]Triple UGH!",
     "Player: Can you...[P] read it to me?",
     "Mari: [E|Sad]To sum it up, the Foster Family hasn't [ITALICS]always[ENDITALICS] helped out the town.[P] Sometimes they've just sucked a big bag of patriarchal dicks.",
     "Thought: That did not summarize anything."})

elseif(sTopicName == "Door") then
    fnStdSequence(gciInv4A_Door, 
    {"[HIDEALLFAST][SHOWCHAR|Mari][SHOWCHAR|Stacy]Mari: [E|N]It's locked![P][E|Shock] Quick Stacy, kick it down!",
     "Stacy: [E|Mad]Alas, I'm not wearing my steel-toed Doc Martins today.[P][E|Blush] Even though my toes are wicked strong--",
     "Thought: She winks at me (???). ",
     "Stacy: [E|Happy]--they can't penetrate a metal door.",
     "Mari: [E|Sad]Mm, drat.[P] We'll have to find another way in."})

-- |[ ======================================= Characters ======================================= ]|
elseif(sTopicName == "Mari") then
    fnStdSequence(gciInv4A_Mari, 
    {"[HIDEALLFAST][SHOWCHAR|Mari][SHOWCHAR|Stacy]Mari: I'll try not to make too much noise, I promise!"})

elseif(sTopicName == "Bret") then
    fnStdSequence(gciInv4A_Bret, 
    {"[HIDEALLFAST][SHOWCHAR|Mari][SHOWCHAR|Stacy]Mari: [E|N]This seems to be Bret's element.",
     "Stacy: [E|N]Not as cool as my element. [E|Mad]LEAD.",
     "Mari: [E|N]I knew that was coming and I let it happen anyway."})

elseif(sTopicName == "Michelle") then
    fnStdSequence(gciInv4A_Michelle, 
    {"[HIDEALLFAST][SHOWCHAR|Mari][SHOWCHAR|Stacy]Stacy: [E|N]I can hear her stomach gurgling from here."})

elseif(sTopicName == "Laura") then
    fnStdSequence(gciInv4A_Laura, 
    {"[HIDEALLFAST][SHOWCHAR|Mari][SHOWCHAR|Stacy]Mari: [E|N]Careful with that walkie-talkie. Make sure she doesn't call us at a bad time."})

end