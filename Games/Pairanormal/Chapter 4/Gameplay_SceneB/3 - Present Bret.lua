--[Bret Presentation]
--Showing stuff to Bret.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--Variables.
local sScriptPath = gsRoot .. "../Chapter 4/Gameplay_SceneB/3 - Present Bret.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
local sAutoAdvance = gsRoot .. "../Chapter 4/Gameplay_SceneD/7 - PatchGoingHome.lua"

--Saving
gsSaveExecute = gsRoot .. "../Chapter 4/Gameplay_SceneA/2 - Pizza Time.lua"
gsSaveTopic = "No Topic"
gsSaveFlag = "InvestigationMuseum"

--[ ========================================= Functions ========================================= ]
--[Friendship Increment]
--If this is the first time presenting this object to this character then we increase the friendship tracker for
-- that character.
local fnIncrementFriendship = function(iObjectIndex, iIncrement)
	
    --Arg check.
    if(iObjectIndex == nil) then return end
    if(iIncrement == nil) then return end
    
    --Build strings.
	local i = iObjectIndex
    local p = iBretIndex
    local sObjectString = string.format("%02i", i)
    local sSubString    = string.format("%02i", p)
    local iIsRepeat = VM_GetVar("Root/Variables/Chapter4/InvestigationA/iObjectPresent|" .. sObjectString .. sSubString, "N")
    if(iIsRepeat == 0.0) then
        
        --Flag
        VM_SetVar("Root/Variables/Chapter4/InvestigationA/iObjectPresent|" .. sObjectString .. sSubString, "N", 1.0)
    
        --Variable gain.
        local iBretFriend  = VM_GetVar("Root/Variables/System/Romance/iBretFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iBretFriend", "N", iBretFriend + iIncrement)
    end
end

--[Sequence Handler]
--Almost all sequences can use a standardized template.
local fnStdSequence = function(iFriendshipIndex, saStrings)
    
    --Start.
    fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    fnCutsceneBlocker()
    
    for i = 1, #saStrings, 1 do
		fnDialogue(saStrings[i])
    end
    
    --Finish.
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneBlocker()
    
    --Friendship handler.
    fnIncrementFriendship(iFriendshipIndex, 1.0)
	
    --Time loss.
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
    end
    
end

--[ ======================================= Chat Sequences ====================================== ]
--Chatting.
if(sTopicName == "Chat") then
	
	--Variables.
	local iChattedWithBret = VM_GetVar("Root/Variables/Chapter4/Investigation/iChattedWithBret", "N")
	
	--First pass:
	if(iChattedWithBret == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter4/Investigation/iChattedWithBret", "N", 1.0)
		
		--Text.
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
    
		fnDialogue([[[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Player: Hey, Bret.]])
		fnDialogue([[Bret: [E|Happy]IT'S YO BOIIIII.[P][E|N] What's up, [PLAYERNAME]?]])
		fnDialogue([[Player: Just taking a look around.[P] Man, this place looks really different when it's closed down.]])
		fnDialogue([[Bret: [E|Happy]Heh. It sure does.[P] But I like it--[E|Mad]it kinda like, gives you space, you know?[P][E|N] You get a chance to focus on the little details.]])
		fnDialogue([[Player: Uhhh, sure.[P] I guess.]])
		fnDialogue([[Bret: [E|N]There's always something interesting happening, or that [ITALICS]has[ENDITALICS] happened in a room.[P] Look at that coffee stain, for example.]])
		fnDialogue([[Thought: He points to a tiny, almost imperceptible discoloration on the far wall.]])
		fnDialogue([[Bret: [E|Happy]Miss Archer spilled her coffee there when we were in fifth grade.[P] Still hasn't been cleaned.]])
		fnDialogue([[Thought: I watch Bret's brows scrunch as he concentrates on the corner, his green-flecked eyes darting from tiny detail to detail.]])
		fnDialogue([[Thought: He had the look of someone remembering something important, from a long time ago.]])
		fnDialogue([[Player: So, what's the deal with you and the curator?]])
		fnDialogue([[Bret: [E|Shock]NOTHING![P][E|Blush] A-absolutely nothing.[P] I mean, sure I spend a lot of time at the museum outside of field trips...]])
		fnDialogue([[Bret: *ahem*! I admire her, is all.[P][E|Mad] What's wrong with that?]])
		fnDialogue([[Bret: [P][P][P][E|Sad]When I came here for the first time... I think it was like, the first grade field trip?[P][E|N] She was talking about the town's old Soda Factory,]])
		fnDialogue([[Bret: and the fire that put it out of commission.[P][E|Sad] I asked her if she knew anyone who had died in it.]])
		fnDialogue([[Thought: He chuckles to himself.]])
		fnDialogue([[Bret: [E|Happy]Everybody scolded me for asking such an insensitive question, but she stopped them--[P][E|Blush] \"There's nothing wrong with asking questions...\"]])
		fnDialogue([[Bret: \"The truth is the first step to becoming a better person.\"[P][P] or, something like that.]])
		fnDialogue([[Player: ... So she [ITALICS]did[ENDITALICS] know of someone who died?]])
		fnDialogue([[Bret: [E|Shock]She never answered me, actually. But I think so.[P][E|Sad] The reason I asked in the first place was...[P][P] she looked so sad when she was talking about it.]])
		fnDialogue([[Thought: I watch the wistful smile that perks up on Bret's lips, the shy way he averts his eyes.]])
		fnDialogue([[Thought: [ITALICS]Pffft, he totally likes her[ENDITALICS].]])

        --If player has walked home with Bret at least once:
        if(true) then
            fnDialogue([[Thought: Odd--why does seeing him think about another person in this way make me uncomfortable?]])
            fnDialogue([[Thought: I bat away the thought before it can get a foothold in my mind.]])
        end
        
		fnDialogue([[Player: Wise words.[P] In that case, I'm gonna see if we can find out the truth from these archives.]])
		fnDialogue([[Bret: [E|N]I'm standing by![P] Let me know if you've got any questions.]])
		fnDialogue([[Player: Mm.]])

        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
    
        --Variable gain.
        local iBretFriend  = VM_GetVar("Root/Variables/System/Romance/iBretFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iBretFriend", "N", iBretFriend + 5.0)
	
        --Time loss.
        if(fnDecrementTime(5, sAutoAdvance) == false) then
            PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
        end
		
	--Repeat passes.
	else
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
        
		fnDialogue("[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|N]I'm standing by![P] Let me know if you've got any questions.")
		fnDialogue("Player: Mm.")
        
        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
	end
	
--[ ==================================== Object Presentation ==================================== ]
elseif(sTopicName == "Amethyst") then
    fnStdSequence(gciInv4A_Amethyst, 
    {"[HIDEALLFAST][SHOWCHAR|Bret]Bret: [E|N]Mmmm, that sure is a big 'ol mineral."})

elseif(sTopicName == "OldPhotos") then
    fnStdSequence(gciInv4A_OldPhotos, 
    {"[HIDEALLFAST][SHOWCHAR|Bret]Bret: [E|N]Opened up in 1972, expanded to include an arcade about 10 years later, and the rest is history.",
     "Bret: [E|Happy] Where would we be without Charlie's?[P][E|Sad] Bored.[P] Definitely very, very bored."})

elseif(sTopicName == "BigTownModel") then
    fnStdSequence(gciInv4A_BigTownModel, 
    {"[HIDEALLFAST][SHOWCHAR|Bret]Bret: [E|Mad]I love this model.[P] The detail...[P] the intricacy...",
     "Player: Uh, Bret?[P] You okay?",
     "Bret: ...[P][E|Blush][ITALICS]it's so tiny[ENDITALICS]."})

elseif(sTopicName == "DeerGod") then
    fnStdSequence(gciInv4A_DeerGod, 
    {"[HIDEALLFAST][SHOWCHAR|Bret]Bret: [E|N]Deerick's okay as long as you don't make too much eye contact."})

elseif(sTopicName == "JournalPages") then
    fnStdSequence(gciInv4A_JournalPages, 
    {"[HIDEALLFAST][SHOWCHAR|Bret]Bret: [E|Blush]Need someone to read this for you?",
     "Thought: I nod.",
     [[ Bret: [E|Mad]\"... He can't do this to me! I am the eldest, damn them all.[P] I am the eldest.\" ]],
     [[ Bret: \"It is my right! My birthright![P] Must this cold world really take everything, when I already have so little, and give so much?\" ]],
     "Player: The author sounds...[P] really upset by something.",
     "Bret: [E|Sad]Yeah.[P] Too bad he doesn't say about what."})

elseif(sTopicName == "KaneFoster") then
    fnStdSequence(gciInv4A_KaneFoster, 
    {"[HIDEALLFAST][SHOWCHAR|Bret]Bret: [E|N]Would you ever have a large portrait of yourself made?",
     "Player: I don't think so.[P] Feels creepy."})

elseif(sTopicName == "Wheelchair") then
    fnStdSequence(gciInv4A_Wheelchair, 
    {"[HIDEALLFAST][SHOWCHAR|Bret]Bret: [E|N]Not a lot of people talk about George Foster Jr. He was Kane McKay Foster's younger brother, lived his whole life in a wheelchair, then he died in an explosion.",
     [[ Bret: [E|Shock] The town's whole mining thing was his idea, but that ended up [P][P][E|Happy] not \"panning\" out! ]],
     "Player: Heh.",
     "Bret: ...[P][P][E|Sad]But seriously, his life was super depressing."})

elseif(sTopicName == "NewspaperFire") then
    fnStdSequence(gciInv4A_NewspaperFire, 
    {"[HIDEALLFAST][SHOWCHAR|Bret]Bret: [E|N]Here's what the old Kola Factory looked like when it wasn't...[P][E|Sad]you know, burnt to a crisp."})

elseif(sTopicName == "NewspaperMeteorite") then
    fnStdSequence(gciInv4A_NewspaperMeteorite, 
    {"[HIDEALLFAST][SHOWCHAR|Bret]Bret: [E|Shock]If I ever saw a shooting star, I'd wish for money.",
     "Player: Huh.[P] That's surprisingly practical.[P] Are you saving up for something important, or--",
     "Bret: [E|Mad]--Which I'd use to buy a lifetime supply of pizza,[P] Frog Fighter 7,[P] a statuette of Krystal from Star Fox,",
     "Bret: [SPEEDTEXT]The limited edition silver Crystal Pepsi bottle cap set, A Sega Dreamcast that I used to have, but it got stolen by this dick neighbor of ours,[ENDSPEEDTEXT]",
     "Bret: [E|N] and a popcorn machine."})

elseif(sTopicName == "NewspaperClinic") then
    fnStdSequence(gciInv4A_NewspaperClinic, 
    {"[HIDEALLFAST][SHOWCHAR|Bret]Player: What's this article about?",
     "Bret: [E|N]Janice Foster, Mr. Foster's wife. She opened up a new women's clinic in town a long time ago.",
     "Bret: It's still running today.[P][E|Shock] For some reason Mari and Stacy, like,[P] HATE it.",
     "Player: Hate is a pretty strong word.",
     "Bret: [E|Sad]Yeah, I don't really get it either.[P] A women's clinic should be a good thing, right?"})

elseif(sTopicName == "Door") then
    fnStdSequence(gciInv4A_Door, 
    {"[HIDEALLFAST][SHOWCHAR|Bret]Player: How do you think we should get through?",
     "Bret: [E|Blush]Well, in an ideal world, I would be on great terms with the museum curator,",
     "Bret: [E|Mad] and she would trust me [ITALICS]so[ENDITALICS] much,[P][E|N] seeing as I'm here all the time,[P][E|Blush] that I'd already know the password.",
     "Player: ...",
     "Bret: ...[P][P][E|Sad]So, I guess we're probably gonna have to break in somehow.[P] *sigh*."})

--[ ======================================== Characters ========================================= ]
elseif(sTopicName == "Mari") then
    fnStdSequence(gciInv4A_Bret, 
    {"[HIDEALLFAST][SHOWCHAR|Bret]Bret: Chicmonster didn't write this part yet."})

elseif(sTopicName == "Bret") then
    fnStdSequence(gciInv4A_Bret, 
    {"[HIDEALLFAST][SHOWCHAR|Bret]Bret: Chicmonster didn't write this part yet."})

elseif(sTopicName == "Michelle") then
    fnStdSequence(gciInv4A_Michelle, 
    {"[HIDEALLFAST][SHOWCHAR|Bret]Bret: Chicmonster didn't write this part yet."})

elseif(sTopicName == "Laura") then
    fnStdSequence(gciInv4A_Laura, 
    {"[HIDEALLFAST][SHOWCHAR|Bret]Bret: Chicmonster didn't write this part yet."})

end