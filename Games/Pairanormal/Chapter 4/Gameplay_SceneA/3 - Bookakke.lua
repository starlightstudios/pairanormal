-- |[I will never apologize for the filename of this script]|
-- |[Never]|

-- |[Arg Resolve]|
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)
    
--Dialogue.
fnDialogue("Player: I just remembered, I have somewhere to go.")
fnDialogue("[CHARENTER|Mari][CHARENTER|Stacy]Mari: [E|Sad]Aww, really?")
fnDialogue("Stacy: [E|Sad][ITALICS]Ne me quitte pas.[ENDITALICS] ")
fnDialogue("Thought: Stacy reaches for me dramatically,[P] then lets her arm dangle limply at her side.")
fnDialogue("Player: Pff.[P] You guys will be fine without me, I think. Enjoy the pizza, okay?")
fnDialogue("[CHARENTER|Michelle][CHARENTER|Bret]Michelle: [E|Blush]Stay safe, you idjit.")
fnDialogue("Bret: [E|Shock]Watch out for anything paranormal!")
fnDialogue("[HIDEALL]Thought: I nod, then head dutifully out the door. It's time to find Laura.")

fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Idjit\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
PairanormalLevel_FlagCheckpoint("Idjit")

fnDialogue("[BG|Null][Music|Null]Thought: ...")
fnDialogue("[BG|Root/Images/Backgrounds/All/LibraryClear][Music|Somber]Player: Psst... Laura[BR] Laura?")
fnDialogue("Thought: I scan the room for that telling fleck of cyan hair.[P] Spotting it behind some stacks, I move to join my companion.")
fnDialogue("Player: [SFX|Blink] There you are![P] I hope I'm not too late...")
fnDialogue("[CHARENTER|Laura]Thought: [EMOTION|Laura|Sad]Laura whirls around, her eyes wide and wet.")
fnDialogue("Thought: Before I have a chance to put down my backpack, she's reaching for me.")
fnDialogue("Player: [CRASH]!!!")
fnDialogue("Thought: With a tiny squeak, her arms are entwined tightly around my shoulders.[P] We've never hugged like this before. ")
fnDialogue("Player: [ITALICS]Whoah, you're really soft.[ENDITALICS] ")
fnDialogue("Laura: [E|Sad]*sniff*[P]*sob*[P]... What?")
fnDialogue("Player: Nothing![P] I-is everything okay?")
fnDialogue("Laura: [E|Mad]Adam [GLITCH]is LATE!")
fnDialogue("Player: Okay, well maybe he just got caught up with--")
fnDialogue("Laura: [E|Mad]What if I've m-m-[P]met him before?")

--Special effect to zoom in on Laura's torso

fnDialogue("Laura:[E|Shock] What if he's my mortal enemy, Barton Ashley--[P][E|Sad]*ahem*[P] *hrm* he moved away last year, but s-[E|Mad]still!")
fnDialogue("Player: [ITALICS]You have a mortal enemy?[ENDITALICS] ")

--Special effect to zoom in on Laura's face

fnDialogue("Laura: [E|Shock]What... what if he showed up, took one look at me, and left!?!? ")
fnDialogue("Player: Laura.")

--Normal zoom

fnDialogue("[MUSIC|Null]Thought: I hold her shoulders squarely in front of me, [P]locking onto those crystal blue eyes the same reassuring way so many others have done for me.")
fnDialogue("Player: That would be impossible.[P] You're one of the most likeable people I've met--[P][P] and I've met like, 25 people total so I know what I'm talking about.")
fnDialogue("[MUSIC|Theme]Thought: [P][P][P]A faint pink tint creeps up Laura's pale cheeks.[P] The wild panic in her eyes begins to disappear.")
fnDialogue("Laura: [E|Shock]Hmph.[P][E|Blush] Yhanks for the objective opinion, comrade.")
fnDialogue("Thought: I nod[P]-- I can't believe that worked, but I'm happy it did.")
fnDialogue("Laura: [E|Sad]... I just wish the computers were here.[P] Then I might be able to [ITALICS]ask him[ENDITALICS] where he is!!")
fnDialogue("Thought: She points to the space in the library where the old tan boxes used to sit. ")
fnDialogue("Player: Ah. I guess they finally got unplugged, huh?")
fnDialogue("Thought: They'd been there for so long, the dust that accumulated on the walls around them created a peculiar shadow.[P] It's so noticeably bare now.")
fnDialogue("Laura: [E|Sad]They've [GLITCH][GLITCHTEXT]t O O KOnelOOkA tMEand L E F T[ENDGLITCHTEXT]probably all been locked up in the warehouse where the mainframe is")
fnDialogue("Laura: [E|Mad]--Argh! *ahem* [P]W-Why didn't they tell me they were gonna do this ahead of time?!")
fnDialogue("Player: Right.[P] right, right right...")
fnDialogue("Laura: [E|Sad][SFX|World|Ugh]...")
fnDialogue("Player: [SFX|Ugh]...")
fnDialogue("Player: [P][P][P]Oh my gosh.[P][P][P] I might have an idea.")

fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Komputer\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
PairanormalLevel_FlagCheckpoint("Komputer")

fnDialogue("[HIDEALL][BG|Null]Thought: [P][P][P]...")
fnDialogue("[BG|Root/Images/Backgrounds/All/SchoolExterior]Player: [P][P]This was a bad idea.")
fnDialogue("[CHARENTER|Ross]Ross:[E|N] Don't be ridonk, dude.[P] Look, for sure it's this key.[P] We already tried the other four, so it's gotta be this one, right?")
fnDialogue("[CHARENTER|Terry]Terry: [E|Mad][CRASH]WILL YOU PLEASE.[P] GIVE IT FUCKING HERE ROSS.[P][P][CRASH] I KNOW WHICH ONE--")
fnDialogue("Ross: [E|N]Shh, hold on.[P][P][E|Shock] Oh dang.[P] Now I lost track of which keys I already tried.")
fnDialogue("Ross: [E|N]So Laura, this is gonna help you find your dude buddy, right?")
fnDialogue("[CHARENTER|Laura]Laura: [E|Blush]A-Adam.[P] His name is Adam.")
fnDialogue("Ross: [E|Shock]You mean, like, Adam from AP Math Adam,[P] Adam Popovich,[P] or Dancing Adam?")
fnDialogue("Terry: [E|Mad][SPEEDTEXT]Adam Popovich [ITALICS]is[ENDITALICS] Dancing Adam---[P][P]just fucking pay attention to what you're fucking doing![ENDSPEEDTEXT] ")
fnDialogue("Thought: Terry, Ross, Laura and I stand as inconspicuously as possible outside a door behind the gym building.")
fnDialogue("Thought: I must've passed by it every day, as did other students,[P] but the twins informed me it currently houses the school's old computer network.")
fnDialogue("Player: We're supposed to meet at the library, but this is the only way to get in contact with him.")
fnDialogue("Ross: [E|Happy]Raaaadical.")
fnDialogue("Ross: Hm. Okay, that key didn't actually work. So then it's gotta be [ITALICS]this[ENDITALICS] one--")
fnDialogue("Terry: [E|Mad]G O D .")
fnDialogue("Thought: Terry finally manages to snatch the giant ring of custodial keys away from her brother, and now mashes them all impatiently into the lock.")
fnDialogue("Laura: [E|Sad][PLAYERNAME] is right.[P] This *ahem* [ITALICS]is[ENDITALICS] a bad idea, actually.")
fnDialogue("Terry: [E|Happy]Don't be silly, I'll get it open.")
fnDialogue("Ross: [E|N]They key on the left.")
fnDialogue("Terry: [E|Mad]SHUT UP ROSS!")
fnDialogue("Ross: [E|Shock]I'm just saying, it's the key on the left for sure.")
fnDialogue("Laura: [E|N]I-I'll just leave a note in the library e-explaining that I got sick again, and I couldn't make it.[P] *hrm* I came down with a sudden case of... of...[P][P][E|Sad] Oh, what if we get caught?")
fnDialogue("Terry: [E|Happy][CRASH] A-FUCKING-HA!")
fnDialogue("Thought: The librarian finally rams a key into the lock without it jamming right away.")
fnDialogue("Thought: It turns with a quick, satisfying click, and the door swings inward.")

fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Harewouse\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
PairanormalLevel_FlagCheckpoint("Harewouse")

fnDialogue("[HIDEALL][Music|Investigation][BG|Root/Images/Backgrounds/All/ComputerCloset]Thought: [P][P][P]...")
fnDialogue("Info: [SFX|Discovery]Investigation[SFX|World|Investigation] #005 -- The Mainframe")
fnDialogue("[CHARENTER|Terry]Terry: [E|Blush]Behold -- the last vestiges of Foster High's former computer mainframe.")
fnDialogue("[CHARENTER|Ross]Ross: [E|N]We also keep extra janitorial supplies in here.[P][E|Shock] Plus the entrance to the security office,[P] uhh, old records,[E|Happy] and [ITALICS]backup snacks[ENDITALICS] for the vending machine.")
fnDialogue("Player: That explains why I always see you with a bag of chips.")
fnDialogue("Ross: [E|Mad]Hey, snack custodian is a very important job.[P][E|N] We actually have to keep our Kane Kola in a vault, otherwise the squirrels sniff it out and try to--")
fnDialogue("[CHARENTER|Laura]Laura: [E|N]Excuse me, but...[P][E|Shock][SFX|Ugh]*hrm* How are we going to turn this on?")
fnDialogue("Thought: Only then do we really behold the mass of definitely-dead computers, discarded and tangled wires, and general disorder.[P] Terry only shrugs.")
fnDialogue("Terry: [E|N]Aren't [ITALICS]you[ENDITALICS] supposed to be like, the computer whiz? Whatever you do, just do it fast.")
fnDialogue("Laura: [E|Sad]... R-[P]right.")

-- |[Loading the Game]|
--If currently loading the game, set up the investigation.
if(PairanormalLevel_IsLoading() == true) then
    
    --Clear checkpoint.
    PairanormalLevel_FlagCheckpoint("InvestigationWarehouse")
    fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"InvestigationWarehouse\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
    
    --Variables.
    local sBGPath = "Root/Images/Backgrounds/All/ComputerCloset"
    local sScriptPath = gsRoot .. "../Chapter 4/Gameplay_SceneC/0 - ExamineObject.lua"

    --Change the background.
    PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/ComputerCloset")
    PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/ComputerCloset")

    --Boot investigation scripts.
    Dia_SetProperty("Clear Investigation Characters")
    Dia_SetProperty("Clear Investigation")
    Dia_SetProperty("Start Investigation") 
    Dia_SetProperty("Set Investigation Image", "Root/Images/Backgrounds/All/ComputerCloset")
    Dia_SetProperty("Set Investigation Script", gsRoot .. "../Chapter 4/Gameplay_SceneC/0 - ExamineObject.lua")
    Dia_SetProperty("Set Go Home Strings", gsRoot .. "../Chapter 4/Gameplay_SceneD/8 - PatchGoingHome.lua", "Default")
    
    --Talkable Characters.
    Dia_SetProperty("Add Talkable Character", "Laura", gsRoot .. "../Chapter 4/Gameplay_SceneC/1 - Present Laura.lua")
    Dia_SetProperty("Add Talkable Character", "Terry", gsRoot .. "../Chapter 4/Gameplay_SceneC/2 - Present Terry.lua")
    Dia_SetProperty("Add Talkable Character", "Ross",  gsRoot .. "../Chapter 4/Gameplay_SceneC/3 - Present Ross.lua")
    Dia_SetProperty("Add Talkable Character", "Phone", gsRoot .. "../Chapter 4/Gameplay_SceneC/4 - Present Phone.lua")
    
    --Talkable characters as inventory objects.
    local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
    Dia_SetProperty("Add Discovered Object", "Laura", sCharPath, sScriptPath, "Laura",  882,   2,  882 + 438,   2 + 363)
    Dia_SetProperty("Add Discovered Object", "Ross",  sCharPath, sScriptPath, "Ross",   882, 367,  882 + 438, 367 + 363)
    Dia_SetProperty("Add Discovered Object", "Terry", sCharPath, sScriptPath, "Terry", 1332, 367, 1332 + 438, 367 + 363) 
    Dia_SetProperty("Clear Discovered Object")
    
    --SLF file adds the examinable objects.
    Dia_SetProperty("Parse SLF File", gsRoot .. "../Chapter 4/MapData/InvestigationAB.slf")
    
    --Check all the items and re-add them to the inventory.
    for i = 1, gciInv4B_Variable_Total-1, 1 do
        
        --Variable name.
        local sVarName = "Root/Variables/Chapter4/InvestigationB/iObject|" .. gczaInv4B_List[i]
        local iHasItem = VM_GetVar(sVarName, "N")
        
        --Has the item. Add it.
        if(iHasItem == 1.0) then
            Dia_SetProperty("Add Discovered Object", gczaInv4B_List[i], sBGPath, sScriptPath, gczaInv4B_List[i], 0, 0, 1, 1)
        end
        Dia_SetProperty("Clear Discovered Object")
    end

    --Modify the time max/left.
    VM_SetVar("Root/Variables/System/Investigation/sInvestigationPath", "S", "Root/Variables/Chapter4/Investigation/")
    local iTimeMax  = VM_GetVar("Root/Variables/Chapter4/Investigation/iTimeMax",  "N")
    local iTimeLeft = VM_GetVar("Root/Variables/Chapter4/Investigation/iTimeLeft", "N")
    Dia_SetProperty("Investigation Time Max",  iTimeMax)
    Dia_SetProperty("Investigation Time Left", iTimeLeft)

-- |[Normal Operations]|
--Otherwise, flag the game to save and boot the investigation normally.
else

    --Save the game.
    fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"InvestigationWarehouse\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

    --Set the time max/left.
    fnCutsceneInstruction([[ VM_SetVar("Root/Variables/Chapter4/Investigation/iTimeMax",  "N", 60.0) ]])
    fnCutsceneInstruction([[ VM_SetVar("Root/Variables/Chapter4/Investigation/iTimeLeft", "N", 60.0) ]])

    --Boot investigation scripts.
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Investigation Characters") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Investigation Time Max", 60) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Investigation Time Left", 60) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Set Investigation Image", "Root/Images/Backgrounds/All/ComputerCloset") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Set Investigation Script", gsRoot .. "../Chapter 4/Gameplay_SceneC/0 - ExamineObject.lua") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Set Go Home Strings", gsRoot .. "../Chapter 4/Gameplay_SceneD/8 - PatchGoingHome.lua", "Default") ]])

    --Talkable characters.
    fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Laura", gsRoot .. "../Chapter 4/Gameplay_SceneC/1 - Present Laura.lua") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Terry", gsRoot .. "../Chapter 4/Gameplay_SceneC/2 - Present Terry.lua") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Ross",  gsRoot .. "../Chapter 4/Gameplay_SceneC/3 - Present Ross.lua") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Phone", gsRoot .. "../Chapter 4/Gameplay_SceneC/4 - Present Phone.lua") ]])
    
    --SLF file adds the examinable objects.
    fnCutsceneInstruction([[ Dia_SetProperty("Parse SLF File", gsRoot .. "../Chapter 4/MapData/InvestigationAB.slf") ]])

    --Add the talkable characters as the first examine objects.
    fnCutsceneInstruction([[ Dia_SetProperty("Add Discovered Object", "Laura", "Root/Images/Backgrounds/All/CharPortraits", gsRoot .. "../Chapter 4/Gameplay_SceneC/0 - ExamineObject.lua", "Laura",  882,   2,  882 + 438,   2 + 363) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Discovered Object", "Ross",  "Root/Images/Backgrounds/All/CharPortraits", gsRoot .. "../Chapter 4/Gameplay_SceneC/0 - ExamineObject.lua", "Ross",   882, 367,  882 + 438, 367 + 363) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Discovered Object", "Terry", "Root/Images/Backgrounds/All/CharPortraits", gsRoot .. "../Chapter 4/Gameplay_SceneC/0 - ExamineObject.lua", "Terry", 1332, 367, 1332 + 438, 367 + 363) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])

    fnCutsceneBlocker()
    
    --Time variable.
    VM_SetVar("Root/Variables/System/Investigation/sInvestigationPath", "S", "Root/Variables/Chapter4/Investigation/")
    
end
