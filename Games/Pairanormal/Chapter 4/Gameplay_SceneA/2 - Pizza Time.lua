-- |[Pizza Time]|
--MINKI MING

-- |[Arg Resolve]|
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)
    
--Dialogue.
fnDialogue("Thought: I have to concede with Mari.[P] All the excitement from the past two days has kind of worn me out. ")
fnDialogue("Player: Ah, what the heck,[P] let's go to the pizza parlor. ")
fnDialogue("[CHARENTER|Michelle]Michelle: [E|Happy][CRASH]HA! Yes!!!")
fnDialogue("[CHARENTER|Bret]Bret:[E|Sad] Well, all right.[P][E|Happy] Never hurts to try my hand at another high score.")
fnDialogue("Thought: Maybe relaxing would be nice for a change.[P] We don't always have to do an investigation, right?")

fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"KONKEYDONG\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
PairanormalLevel_FlagCheckpoint("KONKEYDONG")

fnDialogue("[HIDEALL][BG|Null][Music|Null]...")
fnDialogue("[HIDEALL][BG|Root/Images/Backgrounds/All/WalkHome][Music|Daytime2][CHARENTER|Bret]Bret: [E|Mad]...[P]and then you have to do a double jump to get to the next platform. ")
fnDialogue("Thought: I'd finally agreed to play a couple of games at the arcade, instead of just watching like I normally do.")
fnDialogue("Thought: While the club members kick stones lazily down the street, Bret breaks down the nuances of his favorite cabinets.")
fnDialogue("Thought: Every time I look up at him, he smiles wide.")
fnDialogue("Bret: [E|Shock]It's a tricky maneuver, but don't worry--[P][E|N]You'll be wall-hopping into triple backflips in no time!")
fnDialogue("Thought: I smile.[P] [ITALICS]I think you give me too much credit, Bret.[ENDITALICS]")
fnDialogue("Bret: [E|Shock]Oh! And make sure to punch all the walls when you can.[P] Sometimes there's chicken in there.")
fnDialogue("Player: [SFX|Blink]Pffft! I can't have heard that right.")
fnDialogue("[CHARENTER|Stacy]Stacy: [E|N]Ah yes, a delicious oven-roasted chicken.[P][E|Happy] Even I know about that one!")
fnDialogue("[CHARENTER|Michelle]Michelle: [E|Happy]Since it's in the walls, would that make it [ITALICS]brick[ITALICS] oven roasted?")
fnDialogue("Player: You guys... [P][SFX|Ugh]are so weird.")
fnDialogue("Michelle: [E|N]Takes one to know one, weirdo.")
fnDialogue("Stacy: [CRASH][E|Shock]Shh, shh![P] You guys![P] look!")

fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Spookseum\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
PairanormalLevel_FlagCheckpoint("Spookseum")

fnDialogue("[Music|Null]Thought: We halt; not far off is Foster Falls Museum.[P] It's just a few blocks away from the arcade.[P] But now, pizza and games are last thing on our minds.")
fnDialogue("[HIDEALL]Thought: We can see two adult figures arguing at the entrance of the museum.")
fnDialogue("[CHARENTER|Director][CHARENTER|Three]???: [FOCUS|Director]-can't afford to keep them here. ")
fnDialogue("Curator: [FOCUS|Three][VOICE|Voice|Three][EMOTION|Three|Mad]BULLSHIT!")
fnDialogue("???: [FOCUS|Director]Serena, please...")
fnDialogue("[HIDEALL][CHARENTER|Mari][CHARENTER|Stacy][CHARENTER|Michelle][CHARENTER|Bret]Mari: [E|N]Oooh, look Bret.[P][E|Blush] It's [ITALICS]your girlfriend![ENDITALICS] ")
fnDialogue("Bret: [E|Shock]W-Wha---I-I already told you guys,[E|Mad] [ITALICS]it's not like that[ENDITALICS]!")
fnDialogue("Bret: [E|Blush]Miss Freeman is a grown woman,[SPEEDTEXT] She said [ITALICS]one[ENDITALICS] nice thing to me during a school field trip - [ENDSPEEDTEXT] ")
fnDialogue("Stacy: [E|Mad]It looks like a pretty heated argument.[P][E|Happy] LETS GET CLOSER!")
fnDialogue("Thought: The four of us nonchalantly skirt the corner around a wall of bushes,[P] ears pricked for every scrap of the conversation we can salvage.")
fnDialogue("[CHAREXIT|ALL][CHARENTER|Three][CHARENTER|Director]Curator: [FOCUS|Three][VOICE|Voice|Three][EMOTION|Three|Neutral]--must be joking![P] What did the board of trustees say?")
fnDialogue("[CHAREXIT|ALL][CHARENTER|Mari][CHARENTER|Stacy][CHARENTER|Michelle][CHARENTER|Bret]Player: Hey, that lady is awful familiar...")
fnDialogue("Bret:[E|Blush] It's the curator of the museum.")
fnDialogue("Michelle: [E|Happy]AKA Bret's lady lover.")
fnDialogue("Bret: [E|Mad]--AND THE OTHER GUY, Mr. Felicio,[P] the museum director.[P][P][E|N] He's probably her boss.")
fnDialogue("Mari: [E|Shock]Okay Bret, [ITALICS]how[ENDITALICS] do you know the name of the museum director?")
fnDialogue("Bret: [E|Happy]*snort*! How do you [ITALICS]forget[ENDITALICS] the name Mr. Felicio?[P][E|Sad] I mean it sounds exactly like...[P] you know.")

fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"2Spookseum\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
PairanormalLevel_FlagCheckpoint("2Spookseum")

fnDialogue("[CHAREXIT|ALL][CHARENTER|Director][CHARENTER|Three]Director: They're certainly not happy about it, but what choice do we have, dear?")
fnDialogue("Director: We're running out of storage space, and these boxes have never seen the light of day.")
fnDialogue("Curator: [FOCUS|Three][VOICE|Voice|Three][EMOTION|Three|Mad]They would have, if you'd let me put together the exhibit I've been proposing for the past decade! ")
fnDialogue("Director: This is an institution of knowledge, Selena.[P] Not some fear-mongering sensationalist minstrel show!")
fnDialogue("Curator: [FOCUS|Three][VOICE|Voice|Three][EMOTION|Three|Shock]MISTER FELICIO.[P][MUSIC|Tension][EMOTION|Three|Sad] People.[P] Have been missing in this town for decades.")
fnDialogue("Curator: [FOCUS|Three][VOICE|Voice|Three][EMOTION|Three|Neutral]--and the circumstances surrounding their disappearances [ITALICS]warrants[ENDITALICS] attention.[P] It warrants investigation![P][E|Mad] It--")
fnDialogue("Director: Frankly, I don't think you've managed to put together enough solid evidence, Selena.[P] But if you think you can by next Wednesday, by all means.")
fnDialogue("Director: In the meantime, those documents are being prepped to ship to the county courthouse. ")
fnDialogue("[HIDEALL]Thought: The Paranormal Investigation Club's jaw drops collectively.")
fnDialogue("Thought: Something fiery is igniting in my chest,[P] a surging tingle that spreads to my cheeks and fingers.")
fnDialogue("Thought: Without looking around, I know exactly what everyone here is thinking... [P]because I feel the same way.")
fnDialogue("[CHARENTER|Mari][CHARENTER|Stacy][CHARENTER|Michelle][CHARENTER|Bret]Mari: [E|Shock]A... missing person's case?")
fnDialogue("Bret: [E|N][ITALICS]Several[ENDITALICS] missing people, apparently.")
fnDialogue("Stacy: [E|Mad]It looks like they're about to close the museum.[P][E|N] There won't be anyone there until tomorrow morning!")
fnDialogue("Bret: [E|Shock]I hear the museum doesn't even keep their archives in an air conditioned ready-room.[P][E|Shock] Just a locked up closet in the back.")
fnDialogue("Michelle: [E|Sad]...[P]But, what about buy-one-get-two-free pizza?[P][E|Mad] Mari, didn't you say you were tired?")
fnDialogue("Mari: [E|CRASH][E|Shock]QUICK EVERYONE, see if you can find something that'll give us a boost over the back gate!")
fnDialogue("Michelle: [E|Sad][P][P]Uuuuuuuu...")
fnDialogue("[HIDEALL][BG|Null][Music|Null]...")

fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"3Spookseum\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
PairanormalLevel_FlagCheckpoint("3Spookseum")

fnDialogue("[BG|Root/Images/Backgrounds/All/Gate][Music|Investigation][CHARENTER|Michelle][CHARENTER|Stacy][CHARENTER|Mari][CHARENTER|Bret]Michelle: [E|Sad]--uuuuuuuuuuugh.")
fnDialogue("Thought: Once everyone in the museum went home for the day, The club was eager to get inside, but how?[P] After all, breaking in isn't as easy as it sounds.")
fnDialogue("Thought: We resorted to poking around the doorways that looked the least secure.")
fnDialogue("Mari: [E|N]Bret, you're on lookout.[P] Michelle, can you drag that bin over?")
fnDialogue("Stacy: [E|Mad]Good thinking, Mari. I'm gonna try to climb!")
fnDialogue("[CHAREXIT|ALL]")
fnDialogue("Thought: In all the commotion, the other club members seemed to have forgotten to give me a role.")
fnDialogue("Thought: I meander along the bars of this gate, inspecting the lock that holds it shut.")
fnDialogue("Bret: It's starting to get dark...[P] I don't think I can see very far into the distance.")
fnDialogue("Michelle: Oy, this bin won't budge!")
fnDialogue("Michelle: Don't try to lift it Stacy, I already gave it my best shot so its obviously impossible.")
fnDialogue("Thought: The lock looks an awful lot like the one I practiced on with the twins earlier.")
fnDialogue("Thought: [ITALICS]I...[P]don't suppose I could, could I?[P] Let's see.[ENDITALICS]")
fnDialogue("Mari: There's only one option...[P][CRASH] WE'VE GOT TO FORM A HUMAN PYRAMID![P][CRASH] I CALL THE TOP!")
fnDialogue("Stacy: I can form the base.[P] Supporting all of you guys shouldn't be a problem.")
fnDialogue("Michelle: Fat chance of that![P] I'm gonna get up there first, just you wait.[P] Stacy, give me a boost.")
fnDialogue("Bret: I'm the lightest! Boost me!")
fnDialogue("Thought: A twist there, a turn here.[P] Then... I think I stick something pointy in here, don't I?")
fnDialogue("Info: *click*")
fnDialogue("[HIDEALL]Player: [SFX|World|Blink]Holy crap.[P] It actually worked![P] Hey, guys--")
fnDialogue("[IMAGE|Root/Images/MajorScenes/All/Pyramid]Player: Uhh...[P] the gate is open.")
fnDialogue("Mari: [VOICE|Voice|Mari][CRASH]WOW![P] How'd you do that, [PLAYERNAME]!?")
fnDialogue("Player: Let's just say...[P] I found it like this?")

fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"4Spookseum\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
PairanormalLevel_FlagCheckpoint("4Spookseum")

fnDialogue("[BG|Null][Music|Null][IMAGE|Null]...")
fnDialogue("[BG|Root/Images/Backgrounds/All/Museum][Music|Investigation]...")
fnDialogue("Info: [SFX|Discovery]Investigation[SFX|World|Investigation] #005 -- The Museum")
fnDialogue("[CHARENTER|Stacy][CHARENTER|Mari][CHARENTER|Bret][CHARENTER|Michelle]Thought: Once inside the museum, everyone quickly finds something that takes their interest.")
fnDialogue("Stacy: [E|Mad] Don't forget, guys.[P] We're trying to find the archives.")
fnDialogue("Bret: [E|N]--and [ITALICS]please[ENDITALICS] don't mess with anything!! Miss Freeman worked so hard on these exhibits, and--[P][E|Shock] H-Hey!")
fnDialogue("Michelle: [E|Mad]What're you looking at me for?")
fnDialogue("Bret: [E|Shock][SFX|Ugh] You literally just put a handful of quartz in your pocket AS I was saying my thing.")
fnDialogue("Michelle: [E|Happy]All right, all right.[P] Just 'cuz you fancy her don't mean you can order me around.")
fnDialogue("Michelle: [E|Blush][ITALICS]Don't see what's so bloody great about some stuffy old lady, anyway.[ENDITALICS]")
fnDialogue("Bret: [E|Shock]What was that?")
fnDialogue("Michelle: [E|Blush]n-NOTHING!")

-- |[Loading the Game]|
--If currently loading the game, set up the investigation.
if(PairanormalLevel_IsLoading() == true) then
    
    --Clear checkpoint.
    PairanormalLevel_FlagCheckpoint("InvestigationMuseum")
    fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"InvestigationMuseum\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
    
    --Variables.
    local sBGPath = "Root/Images/Backgrounds/All/Museum"
    local sScriptPath = gsRoot .. "../Chapter 4/Gameplay_SceneB/0 - ExamineObject.lua"

    --Change the background.
    PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Museum")
    PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Museum")

    --Boot investigation scripts.
    Dia_SetProperty("Clear Investigation Characters")
    Dia_SetProperty("Clear Investigation")
    Dia_SetProperty("Start Investigation") 
    Dia_SetProperty("Set Investigation Image", "Root/Images/Backgrounds/All/Museum")
    Dia_SetProperty("Set Investigation Script", gsRoot .. "../Chapter 4/Gameplay_SceneB/0 - ExamineObject.lua")
    Dia_SetProperty("Set Go Home Strings", gsRoot .. "../Chapter 4/Gameplay_SceneD/7 - PatchGoingHome.lua", "Default")
    
    --Talkable Characters.
    Dia_SetProperty("Add Talkable Character", "Mari",     gsRoot .. "../Chapter 4/Gameplay_SceneB/1 - Present MariStacy.lua")
    Dia_SetProperty("Add Talkable Character", "Bret",     gsRoot .. "../Chapter 4/Gameplay_SceneB/3 - Present Bret.lua")
    Dia_SetProperty("Add Talkable Character", "Michelle", gsRoot .. "../Chapter 4/Gameplay_SceneB/5 - Present Michelle.lua")
    
    --Talkable characters as inventory objects.
    local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
    Dia_SetProperty("Add Discovered Object", "Mari",     sCharPath, sScriptPath, "Mari",     1322,   2, 1322 + 438,   2 + 363)
    Dia_SetProperty("Add Discovered Object", "Bret",     sCharPath, sScriptPath, "Bret",        2,   2,    2 + 438,   2 + 363)
    Dia_SetProperty("Add Discovered Object", "Laura",    sCharPath, sScriptPath, "Laura",     882,   2,  882 + 438,   2 + 363)
    Dia_SetProperty("Add Discovered Object", "Michelle", sCharPath, sScriptPath, "Michelle", 1762,   2, 1762 + 438,   2 + 363)
    Dia_SetProperty("Clear Discovered Object")
    
    --SLF file adds the examinable objects.
    Dia_SetProperty("Parse SLF File", gsRoot .. "../Chapter 4/MapData/InvestigationAA.slf")
    
    --Check all the items and re-add them to the inventory.
    for i = 1, gciInv4A_Variable_Total-1, 1 do
        
        --Variable name.
        local sVarName = "Root/Variables/Chapter4/InvestigationA/iObject|" .. gczaInv4A_List[i]
        local iHasItem = VM_GetVar(sVarName, "N")
        
        --Has the item. Add it.
        if(iHasItem == 1.0) then
            Dia_SetProperty("Add Discovered Object", gczaInv4A_List[i], sBGPath, sScriptPath, gczaInv4A_List[i], 0, 0, 1, 1)
        end
        Dia_SetProperty("Clear Discovered Object")
    end

    --Modify the time max/left.
    VM_SetVar("Root/Variables/System/Investigation/sInvestigationPath", "S", "Root/Variables/Chapter4/Investigation/")
    local iTimeMax  = VM_GetVar("Root/Variables/Chapter4/Investigation/iTimeMax",  "N")
    local iTimeLeft = VM_GetVar("Root/Variables/Chapter4/Investigation/iTimeLeft", "N")
    Dia_SetProperty("Investigation Time Max",  iTimeMax)
    Dia_SetProperty("Investigation Time Left", iTimeLeft)

-- |[Normal Operations]|
--Otherwise, flag the game to save and boot the investigation normally.
else

    --Set the time max/left.
    fnCutsceneInstruction([[ VM_SetVar("Root/Variables/Chapter4/Investigation/iTimeMax",  "N", 60.0) ]])
    fnCutsceneInstruction([[ VM_SetVar("Root/Variables/Chapter4/Investigation/iTimeLeft", "N", 60.0) ]])

    --Boot investigation scripts.
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Investigation Characters") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Investigation Time Max", 60) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Investigation Time Left", 60) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Set Investigation Image", "Root/Images/Backgrounds/All/Museum") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Set Investigation Script", gsRoot .. "../Chapter 4/Gameplay_SceneB/0 - ExamineObject.lua") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Set Go Home Strings", gsRoot .. "../Chapter 4/Gameplay_SceneD/7 - PatchGoingHome.lua", "Default") ]])

    --Talkable characters.
    fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Mari",     gsRoot .. "../Chapter 4/Gameplay_SceneB/1 - Present Mari.lua") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Bret",     gsRoot .. "../Chapter 4/Gameplay_SceneB/3 - Present Bret.lua") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Michelle", gsRoot .. "../Chapter 4/Gameplay_SceneB/5 - Present Michelle.lua") ]])
    
    --SLF file adds the examinable objects.
    fnCutsceneInstruction([[ Dia_SetProperty("Parse SLF File", gsRoot .. "../Chapter 4/MapData/InvestigationAA.slf") ]])

    --Add the talkable characters as the first examine objects.
    fnCutsceneInstruction([[ Dia_SetProperty("Add Discovered Object", "Mari",     "Root/Images/Backgrounds/All/CharPortraits", gsRoot .. "../Chapter 4/Gameplay_SceneB/0 - ExamineObject.lua", "Mari",     1322,   2, 1322 + 438,   2 + 363) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Discovered Object", "Bret",     "Root/Images/Backgrounds/All/CharPortraits", gsRoot .. "../Chapter 4/Gameplay_SceneB/0 - ExamineObject.lua", "Bret",        2,   2,    2 + 438,   2 + 363) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Discovered Object", "Laura",    "Root/Images/Backgrounds/All/CharPortraits", gsRoot .. "../Chapter 4/Gameplay_SceneB/0 - ExamineObject.lua", "Laura",     882,   2,  882 + 438,   2 + 363) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Discovered Object", "Michelle", "Root/Images/Backgrounds/All/CharPortraits", gsRoot .. "../Chapter 4/Gameplay_SceneB/0 - ExamineObject.lua", "Michelle", 1762,   2, 1762 + 438,   2 + 363) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])

    fnCutsceneBlocker()
    
    --Time variable.
    VM_SetVar("Root/Variables/System/Investigation/sInvestigationPath", "S", "Root/Variables/Chapter4/Investigation/")
    
    --Save the game.
    fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"InvestigationMuseum\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
    
end

