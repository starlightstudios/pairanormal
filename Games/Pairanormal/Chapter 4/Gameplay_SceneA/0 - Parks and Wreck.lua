--[Exec First]
--Executes when the program starts. This will occur with the argument "Start".

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--[Execution]
--Start.
if(sTopicName == "Start") then
	
    --Loading.
    VM_SetVar("Root/Variables/System/Loading/iLoadChapter2", "N", "1.0")
    VM_SetVar("Root/Variables/System/Loading/iLoadChapter3", "N", "1.0")
    VM_SetVar("Root/Variables/System/Loading/iLoadChapter4", "N", "1.0")
    LM_ExecuteScript(gsRoot .. "../Chapter 4/System/000 Global Variables.lua")
    LM_ExecuteScript(gsRoot .. "../Chapter 4/System/100 Chapter Variables.lua")
    LM_ExecuteScript(gsRoot .. "../Chapter 2/System/200 Dialogue Actors.lua")
    LM_ExecuteScript(gsRoot .. "../Chapter 3/System/200 Dialogue Actors.lua")
    LM_ExecuteScript(gsRoot .. "../Chapter 4/System/200 Dialogue Actors.lua")
    
	--Save the game.
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"ParkA\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
	PairanormalLevel_FlagCheckpoint("ParkA")
    
    --Dialogue.
    fnDialogue("Info: [BG|Null]*Click*")
    fnDialogue("Player: H-Hey![P] I did it!")
    fnDialogue([[Thought: [BG|Root/Images/Backgrounds/All/Cliff]After two hours of what barely qualifies as a \"lockpicking lesson\" and more of a \"nagging session\" from Reich and Leif,]])
    fnDialogue("Thought: I do feel actually pretty triumphant when I hear the sound of a lock giving up to the little tool in my hands.")
    fnDialogue("[CHARENTER|Reich][CHARENTER|Leif]Reich: [E|Sad]... Hm? Did you say something?")
    fnDialogue("Player: [P][P][P][SFX|UGH][ITALICS]You've got to be kidding me[ENDITALICS].[P] The one time I get this right, and you're not even paying attention.")
    fnDialogue("[CHARENTER|AJ]Dog: [E|Neutral][SFX|Dog]It is JUST like deg saying.[P][E|Sad] time being WASTTED on metal, when.[P] Could be GIVING me--[E|Happy]trets.")
    fnDialogue("Player: I don't even want to be here, you know.[P][CRASH] This whole lesson was your guys' idea!!!")
    fnDialogue("Leif: [E|Neutral]Yeah yeah yeah, but look!")
    fnDialogue("Thought: I follow Leif's finger to two young women sitting on a picnic blanket out on the park's rolling hills.[P] They're kissing.")
    fnDialogue("Twins: [FOCUS|Reich][FOCUSMOD|Leif|true][EMOTION|Reich|Happy][EMOTION|Leif|Happy]They're [ITALICS]kissing[ENDITALICS].")
    fnDialogue("Player: I know.")
    fnDialogue("Reich: [E|Blush]Isn't it weird?")
    fnDialogue("Player: Uh... [P][P]Todd says that two girls can be girlfriends, sometimes,[P] or two boys--")
    fnDialogue("Reich: [E|Shock]No, the [ITALICS]kissing[ENDITALICS].[P][E|Blush] Isn't that weird?")
    fnDialogue("Leif: [E|Blush]Splitbodies do it all the time.[P] And they go on [ITALICS]dates[ENDITALICS], and all kinds of kooky things like that.")
    fnDialogue("Thought: I consider the ladies on the picnic blanket--the activity of mashing your damp face against someone else's actually does look kind of bizarre,")
    fnDialogue("Thought: But you can't deny they're having a great time doing it.")
    fnDialogue("Thought: Romance is a new concept to me.[P] I hear about it in little bits and pieces from people at school.")
    fnDialogue("Thought: But up until lately, I hadn't really thought about it for myself.")
    
    --[Branching Stuff]
    --What do?
    fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (What should I say?)") ]])

    local sDecisionScript = "\"" .. gsRoot .. "../Chapter 4/Gameplay_SceneA/0 - Parks and Wreck.lua\""
    fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Have you  tried dating?\", " .. sDecisionScript .. ", \"Tried\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Romance would be nice...\", " .. sDecisionScript .. ", \"Nice\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"You're right. That's weird.\", " .. sDecisionScript .. ", \"Weird\") ")
    fnCutsceneBlocker()

--[Have you tried dating?]
elseif(sTopicName == "Tried") then
    fnDialogue("Player: Have you guys, uh, ever tried dating?")
    fnDialogue("Thought: The twins exchange curious looks.[P] Then they burst into giggles.")
    fnDialogue("Player: Okay, sorry I asked.")
    fnDialogue("Thought: The two of them stifle their laughter in that uncanny, synchronized way they do.")
    fnDialogue("Leif: [E|Blush]One time, we tried to go to a restaurant with a girl...")
    fnDialogue("Reich: [E|Happy]But she thought we were the same person!")
    fnDialogue("Leif: [E|Happy]We had to keep switching in the bathroom so we could each get something to eat.")
    fnDialogue("Reich: [E|Sad]But Eilette said we couldn't do that anymore.")
    fnDialogue("Dog: [E|Mad]SEE?[P] typical humans-- [SFX|Dog]always tryeng a DECEIT.")
    fnDialogue("Thought: [ITALICS]Believe me, they're anything but typical humans[ENDITALICS].")
    fnDialogue("Reich: [E|Blush]Then this other time, we tried to meet a guy online.")
    fnDialogue("Leif: [E|Mad]He was actually [ITALICS]looking[ENDITALICS] for twins, but when we got to his house, all he wanted to do was put his--")
    fnDialogue("Reich: [E|Shock][SFX|World|Blink]WAIT![P] Eilette said we can't talk about that.")
    fnDialogue("Leif: [E|Sad]Oh, right.")
    fnDialogue("Twins: [FOCUS|Reich][FOCUSMOD|Leif|True][EMOTION|Reich|Shock][EMOTION|Leif|Shock]Anyway, the internet is bad.[P] Don't do it!")
    fnDialogue("Player: ... For crying out loud.")
    fnDialogue("Leif: [E|Neutral]In the end, we just stopped trying to date.[P] After all, why would I need to?")
    fnDialogue("Reich: [E|Neutral]Yeah, dating is for lonely people.")
    fnDialogue("Twins: [FOCUS|Reich][FOCUSMOD|Leif|True][EMOTION|Reich|Happy][EMOTION|Leif|Happy]And I'm never lonely without my other!")
    fnDialogue("Thought: My teeth grind silently. The twins don't seem to be aware that most of us don't have the much-glorified 'other'.")
    fnDialogue("Thought: After a pause, I feel a reassuring hand on each of my shoulders.[P] I look up.[P] The twins are unusually sober.")
    fnDialogue("Reich: [E|Sad]NineorTen, our advice is-- maybe don't try so hard to fit in with strangers.")
    fnDialogue("Leif: [E|Sad]If you put yourself out there, you could get hurt.")
    fnDialogue("Player: I know that! But I still...")
    fnDialogue("Reich: [E|Blush]--one minute, you're happily sipping your double-shot espresso across from the woman you hope to marry,")
    fnDialogue("Leif: [E|Shock]The next minute you're shot down by an amateur hitman in a romantic open-air cafe where you're left to bleed to death over your complimentary biscotti.")
    fnDialogue("Dog: [E|Shock][P][P][SFX|World|Ugh]... what?")
    LM_ExecuteScript(LM_GetCallStack(0), "Resume")

--[Romance? FUCK YEAH!!!!]
elseif(sTopicName == "Nice") then
    fnDialogue("Player: Uhh... I don't know.[P] I think romance might be kind of nice, actually.")
    fnDialogue("Twins: [FOCUS|Reich][FOCUSMOD|Leif|True][EMOTION|Reich|Happy][EMOTION|Leif|Happy]Oh REALLY?")
    fnDialogue("Thought: My shoulders slide al the way up to my red ears.[P] two pairs of piercing green eyes look like they can see right through me.")
    fnDialogue("Player: Y-yeah.[P] I don't know.[P] Having somebody care about you, and... and caring about someone else.[P] And touching you where... wh-when... I mean, uhh--")
    fnDialogue("Reich: [E|Blush]All right then, who is it?")
    fnDialogue("Leif: [E|Blush]You've obviously got your mind on someone.")
    fnDialogue("Player: I do NOT![P] I just--[P]as a concept,[P] it seems like not a bad idea.")
    fnDialogue("Thought: My chaperones shrug in concert; hopefully the matter is closed.")
    fnDialogue("Thought: I try to banish the image of the person who [ITALICS]is[ENDITALICS] on my mind, and the now steadily growing idea of what... being with them could be like.")
    fnDialogue("Thought: After a pause, I feel a reassuring hand on each of my shoulders.[P] I look up.[P] The twins are unusually sober.")
    fnDialogue("Reich: [E|Sad]NineorTen, our advice is-- maybe don't try so hard to fit in with strangers.")
    fnDialogue("Leif: [E|Sad]If you put yourself out there, you could get hurt.")
    fnDialogue("Player: I know that! But I still...")
    fnDialogue("Reich: [E|Blush]--one minute, you're happily sipping your double-shot espresso across from the woman you hope to marry,")
    fnDialogue("Leif: [E|Shock]the next minute you're shot down by an amateur hitman in a romantic open-air cafe where you're left to bleed to death over your complimentary biscotti.")
    fnDialogue("Dog: [E|Shock]... what?")
    LM_ExecuteScript(LM_GetCallStack(0), "Resume")

--[The cooties are coming]
elseif(sTopicName == "Weird") then
    fnDialogue("Thought: I sigh.")
    fnDialogue("Player: You're right.[P] It is kind of weird.")
    fnDialogue("Thought: Who needs romance, anyway? I've got enough on my mind these days:")
    fnDialogue("Thought: Homework, walking a dog whose mind I can read, and dealing with twins from a creepy shadow organization I somehow belong to.")
    fnDialogue("Thought: Everyday stuff, but still--[P]It adds up.")
    fnDialogue("Dog: [E|Sad]Do nott.[P] being sad, hyuman.")
    fnDialogue("Player: Sad? what?[P] I'm not sad. ")
    fnDialogue("Dog: [E|Mad]NOT can decieving dog.[P][E|Shock] dog SEEing it plain on the FaCe.")
    fnDialogue("Dog: [E|Neutral]You are ugly AND dumb human, that very much true--[P][E|Happy]but this one still believing you can find mate.")
    fnDialogue("Player: Please.[P] I'm just really busy right now, okay?[P] I can't think about dating.")
    fnDialogue("Dog: [E|Happy]Dog decide, it is nNot lost cause![P][E|Shock] I can be techingk humman seCrett dogue Tecknique for attracting mate.")
    fnDialogue("Player: Pass.[P] I'm gonna pass on that.")
    fnDialogue("Leif: [E|Neutral]Hm?[P] Did you say something, [PLAYERNAME]?")
    fnDialogue("Player: Nope! just...[P] thinking out loud.")
    LM_ExecuteScript(LM_GetCallStack(0), "Resume")

--[Resume Here]
elseif(sTopicName == "Resume") then
    fnDialogue("Thought: I huff dramatically, hoping to clear the air and this demoralizing topic.")
    fnDialogue("Player: ... Okay, then.[P] What were you telling me about lock picking?")
    fnDialogue("[HIDEALL][Music|Null][BG|Null]...")
    
	--Save the game.
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"ParkB\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
	PairanormalLevel_FlagCheckpoint("ParkB")
    
    fnDialogue("[BG|Root/Images/Backgrounds/All/Hallway][Music|Theme]Thought: School.[P] Its strange how quickly I've been able to fall into a routine here.")
    fnDialogue("Thought: Things were once so new, and now I can put in the combination for my locker and open it without thinking twice.")
    fnDialogue("Thought: [ITALICS]Of course, after Rech and Leif's lockpicking lessons, I can now also open everyone else's locker in theory.[ENDITALICS] ")
    fnDialogue("[CHARENTER|Laura]Laura: [E|Neutral]Oh, [PLAYERNAME].[P] T-There you are.")
    fnDialogue("Player: Laura! Hey, good to see you.")
    fnDialogue("Laura: [E|Neutral]*Hrm* I just was stopping by to... erm.[P] C-can you tell the club I won't be able to make the meeting today?")
    fnDialogue("Laura: [E|Mad]There's something I have to--I mean, I'm just going to be doing some homework in the library.")
    fnDialogue("Laura: [E|Shock] Yes![P] Homework, that's it.")
    fnDialogue("Player: Sure.[P] You'll still be on the walkie talkie, right?")
    fnDialogue("Laura: [E|Shock]Huh?")
    fnDialogue("Player: Cuz you're going to the library.[P] But you'll be on your walkie talkie.")
    fnDialogue("Laura: [E|Mad]YES![P][E|Blush] I-I mean, no.[P] I'm...[P]*whew*")
    fnDialogue("Thought: Laura's shaking so hard, she has to hold onto the lockers for support.")
    fnDialogue("Player: Laura, you look a little pale.[P] Do you need to take a seat--")
    fnDialogue("Laura: [E|Sad]I'm fine, I'm just a really bad liar, okay!?!?")
    fnDialogue("Laura: [E|Mad]YES, I'LL ADMIT IT![P] I LIED TO YOU, ALL RIGHT?")
    fnDialogue("Laura: I'M NOT GOING TO THE LIBRARY, I MEAN I AM BUT NOT FOR HOMEWORK I'M GOING TO MEET--")
    fnDialogue("Thought: She catches herself at the last minute, clapping her hand to her mouth.")
    fnDialogue("Laura: [E|Blush]I-I'm going to meet someone.[P] You know...[ITALICS]alone.[ENDITALICS] ")
    fnDialogue("Thought: [CHAREXIT|Laura]I mean, I'm pretty sure I understand ''alone'', but not...[P][P][ITALICS]alone[ENDITALICS] ")
    fnDialogue("Player: Oh, c-cool.[P] Do you want anyone to come with you?")
    fnDialogue("Thought: I hadn't realized Laura had already scurried off.[P] [ITALICS]sigh[ENDITALICS].")
    
    if(true) then
        fnDialogue("Thought: I'm about to try and search for her to get more answers, but I see a familiar face in my sightline, quickly approaching.")
    end
    
	--Save the game.
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"HallwayA\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
	PairanormalLevel_FlagCheckpoint("HallwayA")

    fnDialogue("Player: [P][P]Todd!")
    fnDialogue("Thought: I'm so excited to see him, I slam the locker door shut.")
    fnDialogue("Thought: I immediately regret it, of course, because I've now removed the only small barrier between us.")
    fnDialogue("[CHARENTER|Todd]Todd: [E|Shock][PLAYERNAME]![E|Neutral] How's your day going?")
    fnDialogue("Player: Great![P] I-I was just thinking... how I don't have to think about everything I do anymore.")
    fnDialogue("Player: Some stuff comes naturally to me now.[P] It's nice.")
    fnDialogue("Todd: [E|Happy]Hah. Gosh.")
    fnDialogue("Player: What is it?")
    fnDialogue("Todd: [E|Mad]Routine. Fundamental to a human's everyday operation, but almost inherently unnoticable.")
    fnDialogue("Todd: [E|Happy] Except by a fresh pair of eyes, like yours.")
    fnDialogue("Player: Big words.")
    fnDialogue("Todd: [E|Shock]Ack, sorry. [P][E|Blush]I'm just always pleasantly surprised by your perspective on things.")
    fnDialogue("Player: Thanks?")
    fnDialogue("Todd: [E|Neutral] Don't mention it.")
    fnDialogue("Thought: He leans against the neighboring locker and exhales, indicating it's my turn to talk.[P][ITALICS] Why did I start a conversation I have no idea how to continue!??[ENDITALICS] ")
    fnDialogue("Thought: I just got carried away...[P] Todd almost never approaches me at school.")
    fnDialogue("Thought: It feels weird making casual conversation in the hallway, but he's obviously making an effort today for some reason.")
    fnDialogue("Thought: The only thing on -my- mind recently has been[P].[P].[P].")
    fnDialogue("Player: Uhhh[P] *cough* hey.[P] I've been thinking about this thing lately.")
    fnDialogue("Player: About... ''romance'' and ''dating'' and stuff, and how it works.")
    fnDialogue("Thought: Todd's eyes widen, and a curious grin spreads across his face.")
    fnDialogue("Todd: [E|Neutral]Oh, yeah?")
    fnDialogue("Thought: [ITALICS]You've got nothing to be embarrassed about![P] He... he is my tutor, after all.[P] I can ask him anything, right?[ENDITALICS] ")
    fnDialogue("Player: well...")
    
    --[Branching Stuff]
    --What do?
    fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (What should I say?)") ]])

    local sDecisionScript = "\"" .. gsRoot .. "../Chapter 4/Gameplay_SceneA/0 - Parks and Wreck.lua\""
    fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Ask about dating\", " .. sDecisionScript .. ", \"AskAbout\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Ask Todd out!\", " .. sDecisionScript .. ", \"NiceDayToddate\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Nevermind\", " .. sDecisionScript .. ", \"Cowardice\") ")
    fnCutsceneBlocker()

--[WHAT IS A DATING???]
elseif(sTopicName == "AskAbout") then
    fnDialogue("Player: There's someone I'm... interested in dating.")
    fnDialogue("Player: And I have a lot of questions about how to go about doing that.")
    fnDialogue("Todd: [E|Happy]Ohhhhh.[P] [E|Blush] For exaaaaample?")
    fnDialogue("Player: Like do I just start kissing them and then we're dating, or do I have to ask them first?")
    fnDialogue("Player: Do I have to kiss them sitting down, or can I stand up?")
    fnDialogue("Todd: [E|Shock][P].[P].[P].[P][CRASH][E|Happy]Pf'hahahahahaaha!")
    fnDialogue("Player: D-dude! I'm being serious!")
    fnDialogue("Todd: [E|Happy]I know, know--ahahah.[P] I am just REALLY glad you asked me instead of trying it on your own.")
    fnDialogue("Todd: [E|Blush] First of all, definitely don't kiss anyone without their permission.")
    fnDialogue("Player: Got it.")
    fnDialogue("Todd: [E|Neutral]Secondly, I kinda have to ask now,[P] like what do you think romance even means?")
    fnDialogue("Player: Well, I see it as someone caring about you.[P] And I want to be cared about.")
    fnDialogue("Player: Also, there's kissing, which I don't really know why I have to do it,[P] but I am willing to do it I guess and [CRASH]WILL YOU STOP LAUGHING.")
    fnDialogue("Todd: [E|Happy]Pffff--I'm sorry, I'm sorry. *whew*.[P] Okay.")
    fnDialogue("Todd: [E|Neutral] I wouldn't say you're totally wrong, but romance is lowkey [ITALICS]much[ENDITALICS] more complicated than that.")
    fnDialogue("Thought: I nod, waiting for him to continue.")
    fnDialogue("Todd: [E|Neutral]Well, when two people...[E|Mad]mm.[P] If you like a person, but they--[P] Huh.")
    fnDialogue("Todd: This is actually super hard to explain.[P] Okay.[P] So you said you want someone to care about you.")
    fnDialogue("Player: Yes.")
    fnDialogue("Todd: That person will like,[P] expect the same level of care from you.[P] Like a balance.")
    fnDialogue("Player: Okay.")
    fnDialogue("Todd: [E|Sad]Right.")
    fnDialogue("Todd: If I can be blunt, [PLAYERNAME], ask yourself if you have even the time or energy to give care to someone else right now.")
    fnDialogue("Todd: I mean, you're so busy just trying to figure out how everything works here.")
    fnDialogue("Player: ...[P][SFX|Ugh] Oh. ")
    fnDialogue("Todd: [E|Shock]But there's nothing wrong with that! Romance is a very time and energy consuming ordeal.")
    fnDialogue("Todd: [E|Neutral] Take all the time you need now to like, focus on yourself.")
    fnDialogue("Thought: I nod, trying not to look too devastated.[P] Todd gives me a sympathetic pat on the shoulder.")
    fnDialogue("Todd: [E|Shock]But h-hey, this is just [ITALICS]my[ENDITALICS] crazy opinion--and I'm not exactly an expert on the subject,[P][E|Mad] [ITALICS]exibit A, my love life is a disaster[ENDITALICS]. ")
    fnDialogue("Player: What?")
    fnDialogue("Todd: [E|Shock]Nothing![P][E|Neutral] Look, if you really like this person, start thinking about what they could need from you instead of what you want from them.")
    fnDialogue("Todd: At least it'll lead to something dope, even if that's not a romance.")
    fnDialogue("Player: ... *Sigh*.[P] Okay.[P] Thanks Todd.")
    fnDialogue("Todd: [E|Neutral]Hey.[P] I feel really honored that you wanted to know my opinion.[P][E|Mad] It's one of the ways [ITALICS]you show me you care.[ENDITALICS] ")
    fnDialogue("Player: I know you're trying to be nice right now but the extra sappy comments make me want to kick your shins.")
    fnDialogue("Todd: [E|Happy]That's fair.[P][E|Neutral] I retract my earlier statement.")
    LM_ExecuteScript(LM_GetCallStack(0), "PostTodd")

--[Ask Todd Out!]
elseif(sTopicName == "NiceDayToddate") then
    fnDialogue("Player: Todd![P][CRASH] I WANT YOU TO DATE ME.")
    fnDialogue("Todd: [SFX|World|Blink][E|Shock]pshdfkl--[P] eh, what?")
    fnDialogue("Player: I don't know how it works but I want to try it I think.[P] So can you date me, please?")
    fnDialogue("Todd: [E|Blush]Ahh... well, I...[P][E|Shock] *ahem*, I don't think that's such a good idea, [PLAYERNAME].")
    fnDialogue("Player: Why not?")
    fnDialogue("Todd:[P][P][P][E|Sad] Mmmnh.[P] Well I'm your tutor, for a start.[P] that means there's kind of an imbalance in the relationship.")
    fnDialogue("Todd: [E|Mad]I personally believe that imbalance is bad between two people who are dating.")
    fnDialogue("Todd: It's about giving and taking, like, care.[P] kindness.[P] [E|Blush] Love, I guess, technically.")
    fnDialogue("Todd: *ahem* Ehh,[P][SFX|World|Ugh] is this making any sense to you?")
    fnDialogue("Player: ...[P]I see, you're saying because you're tutoring me, we can't date.")
    fnDialogue("Todd: [E|Neutral]Yes.[P] I'd recommend waiting on trying that out with [ITALICS]anyone[ENDITALICS] for a while--")
    fnDialogue("Player: --So once I get caught up in school we can start dating.")
    fnDialogue("Todd: [E|Shock][CRASH]!!![P][SFX|World|Ugh] Th-that's not quite--")
    LM_ExecuteScript(LM_GetCallStack(0), "PostTodd")

--[WUSS]
--Literally nobody will see this dialogue line.
elseif(sTopicName == "Cowardice") then
    fnDialogue("Info: hello, this is Salty, the coder of this game.")
	fnDialogue("Info: I wanted to see if anyone would pick this dialogue route.")
	fnDialogue("Info: If you're seeing this, congradulations on your cowardice.")
	fnDialogue("Info: Anyone would ask Todd out or for advice. Why did Chic even bother to write this option?")
	fnDialogue("Info: I guess, specifically for you. Write in if you picked this on your first pass.")
	fnDialogue("Info: Inquiring minds want to know.")
    fnDialogue("Info: Okay, game resumes now.")
    fnDialogue("Player: I...[P]if one person were to...[P]eh.[P] You know what? Nevermind.")
    fnDialogue("Thought: Todd nods.")
    fnDialogue("Todd: [E|Blush]Dating is really hard.[P] I mean I'm not exactly an expert on the subject, either.[P][E|Mad][SFX|Ugh] [ITALICS]Exibit A, my love life is a disaster[ENDITALICS].")
    fnDialogue("Player: [SFX|World|Blink]What?")
    fnDialogue("Todd: [E|Shock]Nothing![P][E|Blush] Look, I know it's awkward but...[P]I'm here if you have any questions or whatever.")
    fnDialogue("Player: Cool. Thanks, Todd.")
    LM_ExecuteScript(LM_GetCallStack(0), "PostTodd")
    
--[Resume Again]
elseif(sTopicName == "PostTodd") then

	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"PostToddly\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
	PairanormalLevel_FlagCheckpoint("PostToddly")
    
    fnDialogue("[CHARENTER|Harper]Harper: [E|Shock]Todd![P] There you are.")
    fnDialogue("Todd: [E|Neutral]Hey man. What's up?")
    fnDialogue("Harper: [E|Mad]Hell on earth, that's what's up.[P] Alexa and Rodrigo's gang to-ta-lly bailed on us for the fundraiser setup tonight.")
    fnDialogue("Todd: [E|Mad]You're kidding.")
    fnDialogue("Harper: [E|Mad]I wish!!![P] They're going to catch a movie instead, the fun-loving [ITALICS]bastards[ENDITALICS].")
    fnDialogue("Harper: [E|Sad] Can you help me move supplies into the gym so we can get a head start tonight?")
    fnDialogue("Todd: [E|Sad]Sure-- ugh, just what we need.[P][E|Neutral] I'll see you around, [PLAYERNAME].")
    fnDialogue("Player: Bye.")
    fnDialogue("[HIDEALL]Thought: I suppose they were referring to the Winter Fundraiser event happening this weekend.[P] My eyes drift to the poster hanging not far from my locker advertising it.")
    fnDialogue("Thought: [P][P][P]I never bothered to read the big handwritten letters but try to now, hoping I can take my mind off of romance once and for all.")
    fnDialogue("[BG|Root/Images/Backgrounds/All/Classroom][Music|Daytime]Thought: It's finally time for my favorite part of the day.")
    fnDialogue("Info: [CRASH] *PAM*")
    fnDialogue("[CHARENTER|Mari]Mari: [E|Sad]Hey guys.")
    fnDialogue("[SHOWCHAR|Bret|Sad][SHOWCHAR|Michelle|Sad][SHOWCHAR|Stacy|Sad]Club: [FOCUSMOD|Bret|True][FOCUSMOD|Michelle|true][FOCUSMOD|Stacy|true][SFX|Ugh] Hey.")
    fnDialogue("Thought: [FOCUSMOD|Mari|False][FOCUSMOD|Bret|False][FOCUSMOD|Michelle|False][FOCUSMOD|Stacy|False]Huh.[P] Today's meeting doesn't seem quite as grand as all the others.")
    fnDialogue("Thought: Everyone slumps into desks, fidgeting and letting their eyes wander to any vague point of interest.")
    fnDialogue("Mari: [E|Neutral]Today we will be investigating...[P]uh...[P][ITALICS]Charlie's Pizza Parlor[ENDITALICS]!")
    fnDialogue("Michelle: [E|Shock]Oh, no.[P] Not again.")
    fnDialogue("Mari: [E|Happy]There's a buy 1 get 2 special on Pizza!")
    fnDialogue("Michelle: [E|Blush][CRASH]Well, when you put it that way--")
    fnDialogue("Stacy: [E|Mad]Hang on people, what about all the amazing places we've been going to?")
    fnDialogue("Stacy: How come we aren't doing any follow up investigations?")
    fnDialogue("Mari: [E|Neutral]Ahhh.[P][E|Sad] To be perfectly honest Stacy, I've been in the mood for something a little more...[P]chill these days.")
    fnDialogue("Mari: I hope that's okay.")
    fnDialogue("Michelle: [E|Mad]There is NOTHING wrong with buy 1 get 2 pizzas, love.[P][CRASH] NOTHING.")
    fnDialogue("Bret: [E|Sad]Yeah, but... the parlor?[P][E|Shock] We've already been there so many times,")
    fnDialogue("Bret: [SPEEDTEXT]and while its true[E|Happy] the brand new Monkey King cabinet got a lot of my attention when [E|Mad]they put it in,")
    fnDialogue("Bret: [SPEEDTEXT]there's only so many tokens you can put in [E|Sad]before[ENDSPEEDTEXT] you get kind of bored...[P] and[P] stuff.")
    fnDialogue("Thought: The classroom echoes with a soft groan,[P] the murmurs of 25-year old desks squeaking while we shift lazily in our chairs.")

	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"MANKEYKANG\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
	PairanormalLevel_FlagCheckpoint("MANKEYKANG")
    
    fnDialogue("Stacy: [E|Shock]Anyone know where Laura is?")
    fnDialogue("Player: [SFX|Blink]Ah! S-she can't make the meeting today.[P] She's in the library.[P] [ITALICS]Alone.[ENDITALICS] ")
    fnDialogue("Stacy: [E|Neutral]Oh, cool.")
    fnDialogue("Player: You know... [ITALICS]Alone[ENDITALICS].")
    fnDialogue("Mari: [E|Neutral]Mmm.")
    fnDialogue("Player: Sh...[P][SFX|Ugh]shouldn't somebody check on her?")
    fnDialogue("Michelle: [E|Neutral]Why, is she in danger of being crushed by a pile of books?")
    fnDialogue("Thought: Nobody moves from their seats.[P] What happened to all the energy, the excitement? The conviction?")
    fnDialogue("Thought: [GLITCH]I shake my head;[P] Looks like the club [GLITCH][GLITCH]members won't[GLITCHTEXT]WONT BE DOING M U C H INV[ENDGLITCHTEXT] be doing much investigating today.[GLITCH] ")
    fnDialogue("[BG|Root/Images/Backgrounds/All/Hallway][BG|Root/Images/Backgrounds/All/Hallway][HIDEALL][CHARENTER|Harper][CHARENTER|Todd]Harper: [E|Mad] They're going to [GLITCH]catch a movie instead, the fun-loving [ITALICS]bastards[ENDITALICS].")
    fnDialogue("[BG|Root/Images/Backgrounds/All/Classroom][BG|Root/Images/Backgrounds/All/Classroom][HIDEALL][CHARENTER|Mari][CHARENTER|Bret][CHARENTER|Michelle][CHARENTER|Stacy]Thought: Nggh...[P] Oh, that's right.[P] Todd might need some help setting up for the Winter Carnival.")
    fnDialogue("Thought: I look around at my companions, who appear only slightly more animated than their heavy schoolbags tossed on the floor.[P] Should I suggest it to them?")
    
    --[Branching Stuff]
    --What do?
    fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (What should I suggest?)") ]])

    local sDecisionScript = "\"" .. gsRoot .. "../Chapter 4/Gameplay_SceneA/0 - Parks and Wreck.lua\""
    fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Pizza sounds good\", " .. sDecisionScript .. ", \"Pizzat\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Let's help with the fundraiser\", " .. sDecisionScript .. ", \"Funds\") ")
    
    local iWalkedWithLauraA = VM_GetVar("Root/Variables/System/Player/iWalkedWithLauraA", "N")
    if(iWalkedWithLauraA == 1.0) then
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"I'll go to the library\", " .. sDecisionScript .. ", \"Library\") ")
    end
    fnCutsceneBlocker()

--[PIZZAT]
elseif(sTopicName == "Pizzat") then
    LM_ExecuteScript(gsRoot .. "../Chapter 4/Gameplay_SceneA/2 - Pizza Time.lua", "Start")

--[Let's Raise Money]
elseif(sTopicName == "Funds") then
    LM_ExecuteScript(gsRoot .. "../Chapter 4/Gameplay_SceneA/1 - Fundraiser.lua", "Start")

--[Buk]
elseif(sTopicName == "Library") then
    LM_ExecuteScript(gsRoot .. "../Chapter 4/Gameplay_SceneA/3 - Bookakke.lua", "Start")

-- |[Skipper]|
elseif(sTopicName == "Arrive Home") then
	LM_ExecuteScript(gsRoot .. "../Chapter 4/Gameplay_SceneD/0 - Walk Home.lua", "Mari")

--[Level Skip]
--Lets you jump ahead!
elseif(sTopicName == "LevelSkipBoot") then
	
    --Loading.
    VM_SetVar("Root/Variables/System/Loading/iLoadChapter2", "N", "1.0")
    VM_SetVar("Root/Variables/System/Loading/iLoadChapter3", "N", "1.0")
    VM_SetVar("Root/Variables/System/Loading/iLoadChapter4", "N", "1.0")
    LM_ExecuteScript(gsRoot .. "../Chapter 4/System/000 Global Variables.lua")
    LM_ExecuteScript(gsRoot .. "../Chapter 4/System/100 Chapter Variables.lua")
    LM_ExecuteScript(gsRoot .. "../Chapter 2/System/200 Dialogue Actors.lua")
    LM_ExecuteScript(gsRoot .. "../Chapter 3/System/200 Dialogue Actors.lua")
    LM_ExecuteScript(gsRoot .. "../Chapter 4/System/200 Dialogue Actors.lua")
	
	--Level Skip.
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "(Want to skip ahead?)") ]])

	local sDecisionScript = "\"" .. gsRoot .. "../Chapter 4/Gameplay_SceneA/0 - Parks and Wreck.lua\""
	fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Run First Scene\",        " .. sDecisionScript .. ", \"Start\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Start Fundraiser\",       " .. sDecisionScript .. ", \"Funds\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Go to Library\",          " .. sDecisionScript .. ", \"Library\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Museum Madness\",         " .. sDecisionScript .. ", \"Pizzat\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Arrive Home\",            " .. sDecisionScript .. ", \"Arrive Home\") ")
	fnCutsceneBlocker()

-- |[ ==================================== Walkhome Handler ==================================== ]|
--Beta test the walkhome cases.
elseif(sTopicName == "WalkhomeBoot") then
	
    --Loading.
    VM_SetVar("Root/Variables/System/Loading/iLoadChapter2", "N", "1.0")
    VM_SetVar("Root/Variables/System/Loading/iLoadChapter3", "N", "1.0")
    VM_SetVar("Root/Variables/System/Loading/iLoadChapter4", "N", "1.0")
    LM_ExecuteScript(gsRoot .. "../Chapter 4/System/000 Global Variables.lua")
    LM_ExecuteScript(gsRoot .. "../Chapter 4/System/100 Chapter Variables.lua")
    LM_ExecuteScript(gsRoot .. "../Chapter 2/System/200 Dialogue Actors.lua")
    LM_ExecuteScript(gsRoot .. "../Chapter 3/System/200 Dialogue Actors.lua")
    LM_ExecuteScript(gsRoot .. "../Chapter 4/System/200 Dialogue Actors.lua")
	
	--Level Skip.
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "(Which Character?)") ]])

	local sDecisionScript = "\"" .. gsRoot .. "../Chapter 4/Gameplay_SceneA/0 - Parks and Wreck.lua\""
	fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Bret\", "     .. sDecisionScript .. ", \"BretWalkSel\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Laura\", "    .. sDecisionScript .. ", \"LauraWalkSel\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Mari\", "     .. sDecisionScript .. ", \"MariWalkSel\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Michelle\", " .. sDecisionScript .. ", \"MichelleWalkSel\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Stacy\", "    .. sDecisionScript .. ", \"StacyWalkSel\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Todd\", "     .. sDecisionScript .. ", \"ToddWalkSel\") ")
	fnCutsceneBlocker()

-- |[Bret Cases]|
elseif(sTopicName == "BretWalkSel") then
	
	--Level Skip.
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "(Which version?)") ]])

	local sDecisionScript = "\"" .. gsRoot .. "../Chapter 4/Gameplay_SceneA/0 - Parks and Wreck.lua\""
	fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Version 1 (Ch 3)\", " .. sDecisionScript .. ", \"Bret1Ch3\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Version 1 (Ch 4)\", " .. sDecisionScript .. ", \"Bret1Ch4\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Version 2 (Ch 4)\", " .. sDecisionScript .. ", \"Bret2Ch4\") ")
	fnCutsceneBlocker()
    
elseif(sTopicName == "Bret1Ch3") then
    VM_SetVar("Root/Variables/System/Player/iChapterDestination", "N", 3.0)
    LM_ExecuteScript(gsRoot .. "../Chapter Walk/Walkhome Bret 0.lua", "Start")
    
elseif(sTopicName == "Bret1Ch4") then
    VM_SetVar("Root/Variables/System/Player/iChapterDestination", "N", 4.0)
    LM_ExecuteScript(gsRoot .. "../Chapter Walk/Walkhome Bret 0.lua", "Start")
    
elseif(sTopicName == "Bret2Ch4") then
    LM_ExecuteScript(gsRoot .. "../Chapter Walk/Walkhome Bret 1.lua", "Start")

-- |[Laura Cases]|
elseif(sTopicName == "LauraWalkSel") then
	
	--Level Skip.
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "(Which version?)") ]])

	local sDecisionScript = "\"" .. gsRoot .. "../Chapter 4/Gameplay_SceneA/0 - Parks and Wreck.lua\""
	fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Version 1 (Ch 3)\", " .. sDecisionScript .. ", \"Laura1Ch3\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Version 1 (Ch 4)\", " .. sDecisionScript .. ", \"Laura1Ch4\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Version 2 (Ch 4)\", " .. sDecisionScript .. ", \"Laura2Ch4\") ")
	fnCutsceneBlocker()
    
elseif(sTopicName == "Laura1Ch3") then
    VM_SetVar("Root/Variables/System/Player/iChapterDestination", "N", 3.0)
    LM_ExecuteScript(gsRoot .. "../Chapter Walk/Walkhome Laura 0.lua", "Start")
    
elseif(sTopicName == "Laura1Ch4") then
    VM_SetVar("Root/Variables/System/Player/iChapterDestination", "N", 4.0)
    LM_ExecuteScript(gsRoot .. "../Chapter Walk/Walkhome Laura 0.lua", "Start")
    
elseif(sTopicName == "Laura2Ch4") then
    LM_ExecuteScript(gsRoot .. "../Chapter Walk/Walkhome Laura 1.lua", "Start")

-- |[Mari Cases]|
elseif(sTopicName == "MariWalkSel") then
	
	--Level Skip.
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "(Which version?)") ]])

	local sDecisionScript = "\"" .. gsRoot .. "../Chapter 4/Gameplay_SceneA/0 - Parks and Wreck.lua\""
	fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Version 1 (Ch 3)\", " .. sDecisionScript .. ", \"Mari1Ch3\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Version 1 (Ch 4)\", " .. sDecisionScript .. ", \"Mari1Ch4\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Version 2 (Ch 4)\", " .. sDecisionScript .. ", \"Mari2Ch4\") ")
	fnCutsceneBlocker()
    
elseif(sTopicName == "Mari1Ch3") then
    VM_SetVar("Root/Variables/System/Player/iChapterDestination", "N", 3.0)
    LM_ExecuteScript(gsRoot .. "../Chapter Walk/Walkhome Mari 0.lua", "Start")
    
elseif(sTopicName == "Mari1Ch4") then
    VM_SetVar("Root/Variables/System/Player/iChapterDestination", "N", 4.0)
    LM_ExecuteScript(gsRoot .. "../Chapter Walk/Walkhome Mari 0.lua", "Start")
    
elseif(sTopicName == "Mari2Ch4") then
    LM_ExecuteScript(gsRoot .. "../Chapter Walk/Walkhome Mari 1.lua", "Start")

-- |[Michelle Cases]|
elseif(sTopicName == "MichelleWalkSel") then
	
	--Level Skip.
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "(Which version?)") ]])

	local sDecisionScript = "\"" .. gsRoot .. "../Chapter 4/Gameplay_SceneA/0 - Parks and Wreck.lua\""
	fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Version 1 (Ch 3)\", " .. sDecisionScript .. ", \"Michelle1Ch3\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Version 1 (Ch 4)\", " .. sDecisionScript .. ", \"Michelle1Ch4\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Version 2 (Ch 4)\", " .. sDecisionScript .. ", \"Michelle2Ch4\") ")
	fnCutsceneBlocker()
    
elseif(sTopicName == "Michelle1Ch3") then
    VM_SetVar("Root/Variables/System/Player/iChapterDestination", "N", 3.0)
    LM_ExecuteScript(gsRoot .. "../Chapter Walk/Walkhome Michelle 0.lua", "Start")
    
elseif(sTopicName == "Michelle1Ch4") then
    VM_SetVar("Root/Variables/System/Player/iChapterDestination", "N", 4.0)
    LM_ExecuteScript(gsRoot .. "../Chapter Walk/Walkhome Michelle 0.lua", "Start")
    
elseif(sTopicName == "Michelle2Ch4") then
    LM_ExecuteScript(gsRoot .. "../Chapter Walk/Walkhome Michelle 1.lua", "Start")

-- |[Stacy Cases]|
elseif(sTopicName == "StacyWalkSel") then
	
	--Level Skip.
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "(Which version?)") ]])

	local sDecisionScript = "\"" .. gsRoot .. "../Chapter 4/Gameplay_SceneA/0 - Parks and Wreck.lua\""
	fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Version 1 (Ch 3)\", " .. sDecisionScript .. ", \"Stacy1Ch3\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Version 1 (Ch 4)\", " .. sDecisionScript .. ", \"Stacy1Ch4\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Version 2 (Ch 4)\", " .. sDecisionScript .. ", \"Stacy2Ch4\") ")
	fnCutsceneBlocker()
    
elseif(sTopicName == "Stacy1Ch3") then
    VM_SetVar("Root/Variables/System/Player/iChapterDestination", "N", 3.0)
    LM_ExecuteScript(gsRoot .. "../Chapter Walk/Walkhome Stacy 0.lua", "Start")
    
elseif(sTopicName == "Stacy1Ch4") then
    VM_SetVar("Root/Variables/System/Player/iChapterDestination", "N", 4.0)
    LM_ExecuteScript(gsRoot .. "../Chapter Walk/Walkhome Stacy 0.lua", "Start")
    
elseif(sTopicName == "Stacy2Ch4") then
    LM_ExecuteScript(gsRoot .. "../Chapter Walk/Walkhome Stacy 1.lua", "Start")

-- |[Todd Cases]|
elseif(sTopicName == "ToddWalkSel") then
	
	--Level Skip.
	fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "(Which version?)") ]])

	local sDecisionScript = "\"" .. gsRoot .. "../Chapter 4/Gameplay_SceneA/0 - Parks and Wreck.lua\""
	fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Version 1 (Ch 3)\", " .. sDecisionScript .. ", \"Todd1Ch3\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Version 1 (Ch 4)\", " .. sDecisionScript .. ", \"Todd1Ch4\") ")
	fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Version 2 (Ch 4)\", " .. sDecisionScript .. ", \"Todd2Ch4\") ")
	fnCutsceneBlocker()
    
elseif(sTopicName == "Todd1Ch3") then
    VM_SetVar("Root/Variables/System/Player/iChapterDestination", "N", 3.0)
    LM_ExecuteScript(gsRoot .. "../Chapter Walk/Walkhome Todd 0.lua", "Start")
    
elseif(sTopicName == "Todd1Ch4") then
    VM_SetVar("Root/Variables/System/Player/iChapterDestination", "N", 4.0)
    LM_ExecuteScript(gsRoot .. "../Chapter Walk/Walkhome Todd 0.lua", "Start")
    
elseif(sTopicName == "Todd2Ch4") then
    LM_ExecuteScript(gsRoot .. "../Chapter Walk/Walkhome Todd 1.lua", "Start")
end
