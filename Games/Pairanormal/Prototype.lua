--[Prototype]
--Quick description of where we are in the scenario, or more likely, a joke only funny to Salty.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)
	
--Save the game.
if(PairanormalLevel_IsLoading() == false) then
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Starting\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

--Set data.
else
	PairanormalLevel_FlagCheckpoint("Starting")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Hallway")
	PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/Hallway")
	fnDialogue([[ [Music|DaytimeRedux][SHOWCHAR|Harper|Neutral]Thought: [Focus|Harper]... ]])
end


--[Execution]
--Choice A.
if(sTopicName == "Choice A") then
	fnDialogue("Player: Dialogue goes here.")

--Choice B.
elseif(sTopicName == "Choice B") then
	fnDialogue("Player: Dialogue goes here.")

--Choice C.
elseif(sTopicName == "Choice C") then
	fnDialogue("Player: Dialogue goes here.")

end

--Dialogue rejoins here.
fnDialogue("Player: More dialogue.")

--Decision Set.
fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (Reminder question.)") ]])

local sDecisionScript = "\"" .. gsRoot .. "PairanormalLevel_SceneA/0 - This is the next script.lua" .. "\""
fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Next Choice A\", " .. sDecisionScript .. ", \"ChoiceA\") ")
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Next Choice B\", " .. sDecisionScript .. ", \"ChoiceB\") ")
fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Next Choice C\", " .. sDecisionScript .. ", \"ChoiceC\") ")
fnCutsceneBlocker()
