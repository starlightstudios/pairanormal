-- |[ ====================================== Menu Launcher ===================================== ]|
--Script called when this game is selected from the main menu.
local iGameIndex = fnGetGameIndex("Pairanormal")
if(iGameIndex == 0 or gsaGameEntries[iGameIndex].sActivePath == "Null") then return end

--Execute the file in question.
LM_ExecuteScript(gsaGameEntries[iGameIndex].sActivePath)
