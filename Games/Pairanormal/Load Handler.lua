-- |[ ====================================== Load Handler ====================================== ]|
--This script is called after the Chapter 1/ZLaunch.lua is called and the program has overwritten
-- any created variables.
--After execution, the appropriate script is called. All this script needs to do is handle loading
-- and any special cases.
local iLoadChapter2 = VM_GetVar("Root/Variables/System/Loading/iLoadChapter2", "N")
local iLoadChapter3 = VM_GetVar("Root/Variables/System/Loading/iLoadChapter3", "N")
local iLoadChapter4 = VM_GetVar("Root/Variables/System/Loading/iLoadChapter4", "N")
local iLoadChapter5 = VM_GetVar("Root/Variables/System/Loading/iLoadChapter5", "N")
if(iLoadChapter2 == 1.0) then
    LM_ExecuteScript(gsRoot .. "../Chapter 2/System/000 Global Variables.lua")
    LM_ExecuteScript(gsRoot .. "../Chapter 2/System/100 Chapter Variables.lua")
    LM_ExecuteScript(gsRoot .. "../Chapter 2/System/200 Dialogue Actors.lua")
end
if(iLoadChapter3 == 1.0) then
    LM_ExecuteScript(gsRoot .. "../Chapter 3/System/000 Global Variables.lua")
    LM_ExecuteScript(gsRoot .. "../Chapter 3/System/100 Chapter Variables.lua")
    LM_ExecuteScript(gsRoot .. "../Chapter 3/System/200 Dialogue Actors.lua")
end
if(iLoadChapter4 == 1.0) then
    LM_ExecuteScript(gsRoot .. "../Chapter 4/System/000 Global Variables.lua")
    LM_ExecuteScript(gsRoot .. "../Chapter 4/System/100 Chapter Variables.lua")
    LM_ExecuteScript(gsRoot .. "../Chapter 4/System/200 Dialogue Actors.lua")
end
if(iLoadChapter5 == 1.0) then
    LM_ExecuteScript(gsRoot .. "../Chapter 5/System/000 Global Variables.lua")
    LM_ExecuteScript(gsRoot .. "../Chapter 5/System/100 Chapter Variables.lua")
    LM_ExecuteScript(gsRoot .. "../Chapter 5/System/200 Dialogue Actors.lua")
end
Debug_DropEvents()

--Set last background. It may have needed to load.
local sLastBackground = VM_GetVar("Root/Variables/System/Player/LastBackground", "S")
PairanormalLevel_SetBackground(sLastBackground)
