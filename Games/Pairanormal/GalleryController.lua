-- |[ ====================================== Load Gallery ====================================== ]|
--Script responsible for the gallery menu. Executes whenever it gets opened.
local sBasePath = fnResolvePath()
local ciMaxUnlocks = 32

-- |[Background Loader]|
--List of backgrounds. Load them into memory.
local saList = {}
saList[1] = {"BasketballCourt", "Bathroom", "Bedroom", "BedroomNight", "BedroomNightSacked", "Burning", "Cafe", "CandaceLivingRoom", "Car", "Cave", "Classroom", "ClassroomNoStuff", "Cliff"}
saList[2] = {"ClubHouse", "ComputerCloset", "ComputerClosetDark", "FlashbackCafe", "Forest", "Gate", "Grafitti", "Graveyard", "Gym", "GymFair", "Hallway", "Hospital", "KaneKolaFactory", "Kitchen"}
saList[3] = {"Lab", "Library", "LibraryClear", "LivingRoom", "LivingRoomEvening", "LivingRoomNight", "MineAgentRoom", "MineLobby", "MineQuartzRoom", "MineStorage", "MineTwinRoom"}
saList[4] = {"Museum", "MuseumOffice", "Office", "QuartzBubble", "RunHome", "SchoolExterior", "SchoolExteriorGrad", "ShadowBuilding", "SidewalkNight", "SodaFactory", "SquirrelCouncil", "StacyRoom", "Sunset", "Thunderstorm", "WalkHome"}
SLF_Open(sBasePath .. "Datafiles/Backgrounds.slf")
for i = 1, #saList, 1 do
    for p = 1, #saList[i], 1 do
        DL_ExtractBitmap(saList[i][p], "Root/Images/Backgrounds/All/" .. saList[i][p])
    end
end
SLF_Close()

-- |[Variable Check]|
--First, run through the variables to determine which images have been unlocked.
local iTotalUnlocks = 0
for i = 0, ciMaxUnlocks, 1 do
	
	--Get the option.
    local iVarExists = VM_GetVar("Root/Variables/Gallery/All/" .. csGalleryNames[i], "N")
	
	--Count as unlocked.
	if(iVarExists == 1) then iTotalUnlocks = iTotalUnlocks + 1 end
	
end

--Flag how many were unlocked.
PairanormalMenu_SetProperty("Gallery Images Total", iTotalUnlocks)

-- |[List Assembly]|
--Run through the variables again and unlock as needed.
local iUnlocksSoFar = 0
for i = 0, ciMaxUnlocks, 1 do
	
	--Get the option.
    local iVarExists = VM_GetVar("Root/Variables/Gallery/All/" .. csGalleryNames[i], "N")
	
	--Unlock the image.
	if(iVarExists == 1) then
		PairanormalMenu_SetProperty("Gallery Image", iUnlocksSoFar, csGalleryPaths[i])
		iUnlocksSoFar = iUnlocksSoFar + 1
	end
end

