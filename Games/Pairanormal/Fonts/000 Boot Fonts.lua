--[ ========================================= Boot Fonts ======================================== ]
--Boots fonts used by Pairanormal
if(gbBootedPairanormalFonts == true) then return end
gbBootedPairanormalFonts = true

--[Setup]
--Paths.
local sFontPath = "Data/"
local sKerningPath = "Data/Scripts/Fonts/"
local sLocalPath = fnResolvePath()

--Special Flags
local ciFontNoFlags = 0
local ciFontNearest  = Font_GetProperty("Constant Precache With Nearest")
local ciFontEdge     = Font_GetProperty("Constant Precache With Edge")
local ciFontDownfade = Font_GetProperty("Constant Precache With Downfade")
local ciFontSpecialS = Font_GetProperty("Constant Precache With Special S")

--[ ======================================= Font Registry ======================================= ]
--[Swagger]
Font_Register("Swagger 20", sLocalPath .. "Swagger.ttf", sLocalPath .. "Swagger 20.lua", 20, ciFontNoFlags)
Font_Register("Swagger 40", sLocalPath .. "Swagger.ttf", sLocalPath .. "Swagger 40.lua", 40, ciFontNoFlags)
Font_Register("Swagger 60", sLocalPath .. "Swagger.ttf", sLocalPath .. "Swagger 60.lua", 60, ciFontNoFlags)

--[OpenDyslexicMono]
Font_Register("DyslexicMono 40", sLocalPath .. "DyslexicMono.otf", sLocalPath .. "DyslexicMono 40.lua", 40, ciFontNoFlags)

--[Oxygen]
Font_Register("Oxygen 40", sLocalPath .. "Oxygen-Regular.ttf", sLocalPath .. "Oxygen-Regular 40.lua", 40, ciFontNoFlags)

--[ ========================================== Aliases ========================================== ]
--Swagger 20
Font_SetProperty("Add Alias", "Swagger 20", "Pairanormal System")

--Swagger 40
Font_SetProperty("Add Alias", "Swagger 40", "Pairanormal Disclaimer")
Font_SetProperty("Add Alias", "Swagger 40", "Pairanormal Dialogue Normal")
Font_SetProperty("Add Alias", "Swagger 40", "Pairanormal Options Normal")
Font_SetProperty("Add Alias", "Swagger 40", "Pairanormal Options Swagger")

--Swagger 60
Font_SetProperty("Add Alias", "Swagger 60", "Pairanormal Heading")

--OpenDyslexicMono 40
Font_SetProperty("Add Alias", "DyslexicMono 40", "Pairanormal Dialogue Heading")

--Oxygen 40
Font_SetProperty("Add Alias", "Oxygen 40", "Pairanormal Dialogue Special")
Font_SetProperty("Add Alias", "Oxygen 40", "Pairanormal Options Oxygen")



--Mister Pixel 120 DFO
Font_SetProperty("Add Alias", "Mister Pixel 120 DFO", "Adventure Combat Damage Effect")

--Mister Pixel 90 DFO
Font_SetProperty("Add Alias", "Mister Pixel 90 DFO", "Adventure Combat Header")

--Mister Pixel 20 DFO
Font_SetProperty("Add Alias", "Mister Pixel 20 DFO", "Adventure Combat Description")
Font_SetProperty("Add Alias", "Mister Pixel 20 DFO", "Adventure Menu Small")
Font_SetProperty("Add Alias", "Mister Pixel 20 DFO", "Adventure Menu Costume Small")
Font_SetProperty("Add Alias", "Mister Pixel 20 DFO", "Adventure Menu Doctor Bag Small")
Font_SetProperty("Add Alias", "Mister Pixel 20 DFO", "Adventure Combat Exp")
Font_SetProperty("Add Alias", "Mister Pixel 20 DFO", "Adventure Combat VicItem")

--Mister Pixel 16 DFO
Font_SetProperty("Add Alias", "Mister Pixel 16 DFO", "Adventure Combat Ally Bar")
Font_SetProperty("Add Alias", "Mister Pixel 16 DFO", "Adventure Combat Portrait UI")
Font_SetProperty("Add Alias", "Mister Pixel 16 DFO", "Adventure Combat Small")
Font_SetProperty("Add Alias", "Mister Pixel 16 DFO", "Adventure Menu Status Small")
Font_SetProperty("Add Alias", "Mister Pixel 16 DFO", "Adlev Generator Small")
Font_SetProperty("Add Alias", "Mister Pixel 16 DFO", "Adventure Level UI")
Font_SetProperty("Add Alias", "Mister Pixel 16 DFO", "Context Menu Main")
Font_SetProperty("Add Alias", "Mister Pixel 16 DFO", "Classic Level Small")
Font_SetProperty("Add Alias", "Mister Pixel 16 DFO", "Dance Score")
Font_SetProperty("Add Alias", "Mister Pixel 16 DFO", "Adventure Menu Status Equipment")
Font_SetProperty("Add Alias", "Mister Pixel 16 DFO", "Adventure Level Reinforcement")

--Mister Pixel 8 O
Font_SetProperty("Add Alias", "Mister Pixel 16 DFO", "Adventure Mug")

--Oxygen 27 DF
Font_SetProperty("Add Alias", "Oxygen 27 DF", "World Dialogue Main")
Font_SetProperty("Add Alias", "Oxygen 27 DF", "World Dialogue Decision")

--Sanchez 60
Font_SetProperty("Add Alias", "Sanchez 60 DFO", "Adventure Combat New Turn Font")
Font_SetProperty("Add Alias", "Sanchez 60 DFO", "Adventure Combat Confirmation Big")
Font_SetProperty("Add Alias", "Sanchez 60 DFO", "Adventure Menu Skills Confirmation Big")

--Sanchez 35
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "World Dialogue Heading")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Combat Acting Name Font")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Combat Confirmation Small")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Combat Target Cluster Selection")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Combat Ability Title")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Campfire Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Campfire Main")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Costume Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Equipment Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu File Select Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Gemcutter Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Inventory Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Options Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Status Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Doctor Bag Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Vendor Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Skills Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Skills Character")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Skills Confirmation Small")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Menu Field Abilities Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "String Entry Header")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Classic Level Main")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Combat Inspector Heading")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Combat Inspector Names")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Combat Platina")
Font_SetProperty("Add Alias", "Sanchez 35 DFO", "Adventure Combat Doctor")

--Sanchez 22
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu Equipment Main")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu File Select Main")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu Gemcutter Main")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu Inventory Main")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu Options Main")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu Status Main")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu Doctor Bag Main")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu Vendor Main")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu Skills Job")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu Skills Skills")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adventure Menu Field Abilities Main")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "String Entry Main")
Font_SetProperty("Add Alias", "Sanchez 22 DFO", "Adlev Generator Main")

--Trigger 28 DFO
Font_SetProperty("Add Alias", "Trigger 28 DFO", "Adventure Combat Main")
Font_SetProperty("Add Alias", "Trigger 28 DFO", "Adventure Combat Prediction")
Font_SetProperty("Add Alias", "Trigger 28 DFO", "Adventure Menu Main")
Font_SetProperty("Add Alias", "Trigger 28 DFO", "Adventure Menu Mainline")
Font_SetProperty("Add Alias", "Trigger 28 DFO", "Adventure Menu Status Resistance")
Font_SetProperty("Add Alias", "Trigger 28 DFO", "Adventure Menu Status Health")
Font_SetProperty("Add Alias", "Trigger 28 DFO", "Adventure Menu Status Statistic")
Font_SetProperty("Add Alias", "Trigger 28 DFO", "Adventure Menu Effect")
Font_SetProperty("Add Alias", "Trigger 28 DFO", "Adventure Combat Inspector Effects")

--Trigger 50 DFO
Font_SetProperty("Add Alias", "Sanchez 50 DFO", "Adventure Combat Description Header")
