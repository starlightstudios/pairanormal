--[ ===================================== Oxygen 40 Kerning ===================================== ]
--Set the main scaler.
SugarFont_SetKerning(1.0)

--[Letters groupings]
--SugarFont_SetKerning(string.byte("i"), -1, 3.0)
--SugarFont_SetKerning(-1, string.byte("i"), 2.0)

--[Punctuation groupings]
--SugarFont_SetKerning(-1, string.byte(","), 2.0)
--SugarFont_SetKerning(-1, string.byte("."), 2.0)
--SugarFont_SetKerning(-1, string.byte("'"), 2.0)
--SugarFont_SetKerning(-1, string.byte("!"), 2.0)
--SugarFont_SetKerning(-1, string.byte(":"), 2.0)
--SugarFont_SetKerning(string.byte("'"), -1, 2.0)

--[Specific punctuations]
--SugarFont_SetKerning(string.byte("."), string.byte("."), 0.0)

--[Numbers]
--SugarFont_SetKerning(string.byte("1"), string.byte("."), 3.0)

--[Specific letters]
--SugarFont_SetKerning(string.byte("a"), string.byte("t"), 3.0)

--[Letter-to-punctuation]
--SugarFont_SetKerning(string.byte("t"), string.byte("."), 0.0)
--SugarFont_SetKerning(string.byte(" "), string.byte("i"), 4.0)