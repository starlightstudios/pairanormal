--[ ===================================== Swagger 20 Kerning ==================================== ]
--Set the main scaler.
SugarFont_SetKerning(1.0)


--[Special Characters]
SugarFont_SetKerning(string.byte(","), -1, 1.0)
SugarFont_SetKerning(string.byte("."), -1, 1.0)
SugarFont_SetKerning(string.byte("'"), -1, 2.5)
SugarFont_SetKerning(string.byte("!"), -1, 3.0)
SugarFont_SetKerning(string.byte("*"), -1, 1.0)
SugarFont_SetKerning(-1, string.byte("-"), 2.0)
SugarFont_SetKerning(string.byte("\""), -1 , 3.0)

SugarFont_SetKerning(string.byte(" "), -1, 6.0)
SugarFont_SetKerning(-1, string.byte(" "), 6.0)

--[Individual Letters]
SugarFont_SetKerning(string.byte("A"), -1, 0.5)
SugarFont_SetKerning(string.byte("a"), -1, -0.5)
SugarFont_SetKerning(string.byte("B"), -1, 2.0)
SugarFont_SetKerning(string.byte("b"), -1, 1.0)
SugarFont_SetKerning(string.byte("C"), -1, 1.0)
SugarFont_SetKerning(string.byte("c"), -1, 1.0)
SugarFont_SetKerning(string.byte("D"), -1, 1.0)
SugarFont_SetKerning(string.byte("d"), -1, 1.5)
SugarFont_SetKerning(string.byte("E"), -1, 0.0)
SugarFont_SetKerning(string.byte("e"), -1, 0.5)
SugarFont_SetKerning(string.byte("F"), -1, 1.0)
SugarFont_SetKerning(string.byte("f"), -1, -0.5)
SugarFont_SetKerning(string.byte("G"), -1, 0.0)
SugarFont_SetKerning(string.byte("g"), -1, 1.5)
SugarFont_SetKerning(string.byte("H"), -1, .5)
SugarFont_SetKerning(string.byte("h"), -1, 2.5)
SugarFont_SetKerning(string.byte("i"), -1, 1.5)
SugarFont_SetKerning(string.byte("I"), -1, 2.0)
SugarFont_SetKerning(string.byte("J"), -1, 0.0)
SugarFont_SetKerning(string.byte("j"), -1, -1.0)
SugarFont_SetKerning(string.byte("K"), -1, 1.0)
SugarFont_SetKerning(string.byte("k"), -1, 1.0)
SugarFont_SetKerning(string.byte("L"), -1, 0.5)
SugarFont_SetKerning(string.byte("l"), -1, 1.5)
SugarFont_SetKerning(string.byte("M"), -1, 2.0)
SugarFont_SetKerning(string.byte("m"), -1, 1.5)
SugarFont_SetKerning(string.byte("N"), -1, 1.0)
SugarFont_SetKerning(string.byte("n"), -1, 1.5)
SugarFont_SetKerning(string.byte("O"), -1, 2.0)
SugarFont_SetKerning(string.byte("o"), -1, 0.5)
SugarFont_SetKerning(string.byte("P"), -1, -0.5)
SugarFont_SetKerning(string.byte("p"), -1, 1.5)
SugarFont_SetKerning(string.byte("Q"), -1, 1.0)
SugarFont_SetKerning(string.byte("q"), -1, 1.0)
SugarFont_SetKerning(string.byte("R"), -1, 1.0)
SugarFont_SetKerning(string.byte("r"), -1, 0.5)
SugarFont_SetKerning(string.byte("S"), -1, 0.5)
SugarFont_SetKerning(string.byte("s"), -1, 0.5)
SugarFont_SetKerning(string.byte("T"), -1, -0.5)
SugarFont_SetKerning(string.byte("t"), -1, 0.0)
SugarFont_SetKerning(string.byte("U"), -1, 2.0)
SugarFont_SetKerning(string.byte("u"), -1, 1.5)
SugarFont_SetKerning(string.byte("V"), -1, 0.0)
SugarFont_SetKerning(string.byte("v"), -1, 0.0)
SugarFont_SetKerning(string.byte("W"), -1, 0.5)
SugarFont_SetKerning(string.byte("w"), -1, 1.0)
SugarFont_SetKerning(string.byte("X"), -1, 1.0)
SugarFont_SetKerning(string.byte("x"), -1, 1.0)
SugarFont_SetKerning(string.byte("Y"), -1, 1.0)
SugarFont_SetKerning(string.byte("y"), -1, 1.0)
SugarFont_SetKerning(string.byte("Z"), -1, 1.0)
SugarFont_SetKerning(string.byte("z"), -1, 1.0)

--Just A
SugarFont_SetKerning(-1, string.byte("A"), -0.5)
SugarFont_SetKerning(-1, string.byte("a"), 2.0)



--special cases
SugarFont_SetKerning(string.byte("e"), string.byte("v"), 0.0)
SugarFont_SetKerning(string.byte("W"), string.byte("e"), -0.5)
SugarFont_SetKerning(string.byte("r"), string.byte("s"), -0.5)
SugarFont_SetKerning(string.byte("r"), string.byte("g"), -1.5)
SugarFont_SetKerning(string.byte("r"), string.byte("e"), -1.5)
SugarFont_SetKerning(string.byte("r"), string.byte("a"), -1.5)
SugarFont_SetKerning(string.byte("r"), string.byte("o"), 0.0)
SugarFont_SetKerning(string.byte("P"), string.byte("A"), -2.0)
SugarFont_SetKerning(string.byte("V"), string.byte("E"), -1.0)
SugarFont_SetKerning(string.byte("M"), string.byte("A"), 1.0)
SugarFont_SetKerning(string.byte("Y"), string.byte("o"), -1.0)
SugarFont_SetKerning(string.byte("Y"), string.byte("O"), -1.0)
SugarFont_SetKerning(string.byte("I"), string.byte("A"), 2.0)
SugarFont_SetKerning(string.byte("u"), string.byte("r"), 2.0)
SugarFont_SetKerning(string.byte("f"), string.byte("a"), -1.0)
SugarFont_SetKerning(string.byte("t"), string.byte("a"), -1.0)


--special cases + characters
SugarFont_SetKerning(string.byte("f"), string.byte("."), -3.0)
SugarFont_SetKerning(string.byte("t"), string.byte("."), -3.0)
SugarFont_SetKerning(string.byte("r"), string.byte("."), -3.0)
SugarFont_SetKerning(string.byte(" "), string.byte("A"), 6.0)
SugarFont_SetKerning(string.byte(" "), string.byte("a"), 6.0)
--SugarFont_SetKerning(string.byte("-"), string.byte("-"), 2.0)
