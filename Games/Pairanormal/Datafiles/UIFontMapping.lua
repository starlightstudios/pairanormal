--[UI Font: Small]
--Set lookup information for the large half of the font.

--Local Functions
local fnCreateLetter = function(sLetterString, iLft, iTop, iWid, iHei, iStdSpacing)
	iWid = iWid + 2
	iHei = iHei + 1
	StarlightFont_SetBitmapCharacter(string.byte(sLetterString), iLft, iTop, iLft + iWid, iTop + iHei, iWid + iStdSpacing)
end

--Letters
fnCreateLetter("A", 1299, 0, 12, 20, 1)
fnCreateLetter("B", 1319, 0, 10, 20, 1)
fnCreateLetter("C", 1339, 0,  9, 20, 1)
fnCreateLetter("D", 1359, 0, 11, 20, 1)
fnCreateLetter("E", 1379, 0, 10, 20, 1)
fnCreateLetter("F", 1399, 0,  9, 20, 1)
fnCreateLetter("G", 1419, 0, 11, 20, 1)
fnCreateLetter("H", 1439, 0, 12, 20, 1)
fnCreateLetter("I", 1459, 0,  4, 20, 1)
fnCreateLetter("J", 1479, 0,  5, 20, 1)
fnCreateLetter("K", 1499, 0, 11, 20, 1)
fnCreateLetter("L", 1519, 0,  9, 20, 1)
fnCreateLetter("M", 1539, 0, 14, 20, 1)
fnCreateLetter("N", 1559, 0, 11, 20, 1)
fnCreateLetter("O", 1579, 0, 11, 20, 1)
fnCreateLetter("P", 1599, 0,  9, 20, 1)
fnCreateLetter("Q", 1619, 0, 12, 20, 1)
fnCreateLetter("R", 1639, 0, 11, 20, 1)
fnCreateLetter("S", 1659, 0,  8, 20, 1)
fnCreateLetter("T", 1679, 0, 10, 20, 1)
fnCreateLetter("U", 1699, 0, 10, 20, 1)
fnCreateLetter("V", 1719, 0, 12, 20, 1)
fnCreateLetter("W", 1739, 0, 18, 20, 1)
fnCreateLetter("X", 1759, 0, 11, 20, 1)
fnCreateLetter("Y", 1779, 0, 12, 20, 1)
fnCreateLetter("Z", 1799, 0,  9, 20, 1)

fnCreateLetter("a", 1939, 0,  9, 20, 1)
fnCreateLetter("b", 1959, 0,  9, 20, 1)
fnCreateLetter("c", 1979, 0,  7, 20, 1)
fnCreateLetter("d", 1999, 0,  9, 20, 1)
fnCreateLetter("e", 2019, 0,  8, 20, 1)
fnCreateLetter("f", 2039, 0,  7, 20, 1)
fnCreateLetter("g", 2059, 0,  9, 20, 1)
fnCreateLetter("h", 2079, 0, 10, 20, 1)
fnCreateLetter("i", 2099, 0,  4, 20, 1)
fnCreateLetter("j", 2119, 0,  4, 20, 1)
fnCreateLetter("k", 2139, 0, 10, 20, 1)
fnCreateLetter("l", 2159, 0,  4, 20, 1)
fnCreateLetter("m", 2179, 0, 15, 20, 1)
fnCreateLetter("n", 2199, 0, 10, 20, 1)
fnCreateLetter("o", 2219, 0,  9, 20, 1)
fnCreateLetter("p", 2239, 0,  9, 20, 1)
fnCreateLetter("q", 2259, 0, 10, 20, 1)
fnCreateLetter("r", 2279, 0,  7, 20, 1)
fnCreateLetter("s", 2299, 0,  7, 20, 1)
fnCreateLetter("t", 2319, 0,  6, 20, 1)
fnCreateLetter("u", 2339, 0, 10, 20, 1)
fnCreateLetter("v", 2359, 0, 10, 20, 1)
fnCreateLetter("w", 2379, 0, 15, 20, 1)
fnCreateLetter("x", 2399, 0, 10, 20, 1)
fnCreateLetter("y", 2419, 0, 10, 20, 1)
fnCreateLetter("z", 2439, 0,  7, 20, 1)

--Numbers
fnCreateLetter("0",  959, 0,  9, 20, 1)
fnCreateLetter("1",  979, 0,  9, 20, 1)
fnCreateLetter("2",  999, 0,  8, 20, 1)
fnCreateLetter("3", 1019, 0,  8, 20, 1)
fnCreateLetter("4", 1039, 0,  9, 20, 1)
fnCreateLetter("5", 1059, 0,  8, 20, 1)
fnCreateLetter("6", 1079, 0,  9, 20, 1)
fnCreateLetter("7", 1099, 0,  9, 20, 1)
fnCreateLetter("8", 1119, 0,  9, 20, 1)
fnCreateLetter("9", 1139, 0,  9, 20, 1)

--Misc
fnCreateLetter(" ",    0, 0,  4,  1, 1)
fnCreateLetter("/",  941, 0,  8, 20, 1)
fnCreateLetter("'",  780, 0,  4, 20, 1)
fnCreateLetter("\"", 679, 0,  5, 20, 1)
fnCreateLetter(";", 1179, 0,  3, 20, 1)
fnCreateLetter(":", 1159, 0,  2, 20, 1)
fnCreateLetter("[", 1819, 0,  5, 20, 1)
fnCreateLetter("]", 1859, 0,  5, 20, 1)
fnCreateLetter("-",  899, 0,  5, 20, 1)
fnCreateLetter("+",  859, 0,  9, 20, 1)
fnCreateLetter("_",  191, 0,  7, 20, 1)
fnCreateLetter("=", 1219, 0,  9, 20, 1)
fnCreateLetter("!",  659, 0,  2, 20, 1)
fnCreateLetter("@", 1279, 0, 15, 20, 1)
fnCreateLetter("#",  699, 0,  9, 20, 1)
fnCreateLetter("$",  719, 0,  8, 20, 1)
fnCreateLetter("%",  739, 0, 16, 20, 1)
fnCreateLetter("^", 1879, 0,  7, 20, 1)
fnCreateLetter("&",  759, 0, 13, 20, 1)
fnCreateLetter("*",  839, 0,  6, 20, 1)
fnCreateLetter("(",  799, 0,  6, 20, 1)
fnCreateLetter(")",  819, 0,  6, 20, 1)
fnCreateLetter("?", 1259, 0,  6, 20, 1)

fnCreateLetter(".",  919, 0,  2, 20, 1)
fnCreateLetter(",",  879, 0,  3, 20, 1)







