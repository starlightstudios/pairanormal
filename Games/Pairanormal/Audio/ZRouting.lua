--[Audio Routing]
--Load all music/sfx here
local sBasePath = fnResolvePath()

--[Path Setup]
local sMusicPath = sBasePath .. "Music/"
local sSoundPath = sBasePath .. "Sound/"
local sVoicePath = sBasePath .. "Voice/"
local csaList = {}

--[ ====================================== Effect Function ====================================== ]
local fnLoadEffects = function(psPath, pcsaList, psPrefix)
	
	--Arg check.
	if(psPath   == nil) then return end
	if(pcsaList == nil) then return end
	if(psPrefix == nil) then return end
	
	--Iterate.
	local i = 1
	while(pcsaList[i] ~= nil) do
		AudioManager_Register(psPrefix .. pcsaList[i], "AsSound", "AsSample", psPath .. pcsaList[i] .. ".ogg")
		i = i + 1
	end
end

--[ ======================================== Music Themes ======================================= ]
AudioManager_Register("Daytime",       "AsMusic", "AsStream", sMusicPath .. "Daytime.ogg",       60 + 53.403, 180 + 35.522)
AudioManager_Register("DaytimeRedux",  "AsMusic", "AsStream", sMusicPath .. "DaytimeRedux.ogg", 120 + 37.693, 300 +  0.798)
AudioManager_Register("Jazzy",         "AsMusic", "AsStream", sMusicPath .. "Jazzy.ogg",              12.614, 180 +  7.018)
AudioManager_Register("Love",          "AsMusic", "AsStream", sMusicPath .. "Love.ogg",               16.130, 240 + 27.198)
AudioManager_Register("Somber",        "AsMusic", "AsStream", sMusicPath .. "Somber.ogg",              8.259, 120 + 26.850)
AudioManager_Register("Tension",       "AsMusic", "AsStream", sMusicPath .. "Tension.ogg",            15.274, 180 +  9.593)
AudioManager_Register("Theme",         "AsMusic", "AsStream", sMusicPath .. "Theme.ogg",              52.191, 120 + 55.676)
AudioManager_Register("Investigation", "AsMusic", "AsStream", sMusicPath .. "Investigation.ogg", 60 + 12.035, 120 + 24.062)

--[ ======================================= Sound Effects ======================================= ]
--[World Effects]
csaList = {"Applause", "Blink", "Crash", "Gunshot", "Investigation", "Ugh", "Cellphone"}
fnLoadEffects(sSoundPath, csaList, "World|")

--[UI Effects]
csaList = {"MenuIn", "MenuOut", "MouseOver", "Select"}
fnLoadEffects(sSoundPath, csaList, "UI|")

--[Glitch]
csaList = {"Glitch00", "Glitch01", "Glitch02", "Glitch03", "Glitch04"}
fnLoadEffects(sSoundPath, csaList, "Glitch|")

--[Special]
--Used for unlocking cheats. You cheater.
AudioManager_Register("Special|CheatTone", "AsSound", "AsSample", sSoundPath .. "CheatTone.wav")

--[ =======================================  Voice Ticks ======================================== ]
csaList = {"Adam", "AdultF", "AdultM", "AJ", "Bret", "Candace", "Doll", "Eilette", "EiletteO", "Generic1", "Generic2", "Harold", "Harper", "Kane", "KidF", "KidM", "KidM1", "Laura", "Mari", "Michelle", "Mistress", "Nelson", "Nora", "Player", "Police", "Reich", "Ronnie", "Ross", "Six", "Squirrel", "Stacy", "Terry", "Three", "Thought", "Thug", "Todd", "WalkieTalkie"}
fnLoadEffects(sVoicePath, csaList, "Voice|")

--Special: Leif and Reich use the same voice.
AudioManager_Register("Voice|Leif", "AsSound", "AsSample", sVoicePath .. "Reich.ogg")
