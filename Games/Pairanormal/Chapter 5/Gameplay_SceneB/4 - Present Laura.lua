-- |[ =================================== Laura Presentation =================================== ]|
--Showing stuff to Laura.

-- |[Arg Resolve]|
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variable Setup]|
--Used to simplify the Add Discovered Object calls.
local sBGPath = "Root/Images/Backgrounds/All/MineLobby"
local sScriptPath = gsRoot .. "../Chapter 5/Gameplay_SceneB/0 - ExamineObject.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
local sAutoAdvance = gsRoot .. "../Chapter 5/Gameplay_SceneC/7 - PatchGoingHome.lua"

--Saving
gsSaveExecute = gsRoot .. "../Chapter 5/Gameplay_SceneA/3 - Quartz.lua"
gsSaveTopic = "No Topic"
gsSaveFlag = "InvestigationMine"

-- |[ ========================================= Functions ========================================= ]|
-- |[Storage Handler]|
--Stores the object as discovered.
local fnStdStore = function(i)
    
    --Get if we've examined the object before. This will be returned later.
	local iExaminedAlready = VM_GetVar("Root/Variables/Chapter5/InvestigationA/iObject|" .. gczaInv5A_List[i], "N")
    
    --Save it as a discovered object.
	Dia_SetProperty("Add Discovered Object", gczaInv5A_List[i], sBGPath, sScriptPath, gczaInv5A_List[i])
	VM_SetVar("Root/Variables/Chapter5/InvestigationA/iObject|" .. gczaInv5A_List[i], "N", 1.0)
    
    --Pass back.
    return iExaminedAlready
end

-- |[Friendship Increment]|
--If this is the first time presenting this object to this character then we increase the friendship tracker for
-- that character.
local fnIncrementFriendship = function(iObjectIndex, iIncrement)
	--Does nothing in this chapter.
end

-- |[Time Handler]|
--Decrements time. Also saves the game.
local fnStdTime = function()
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(gsSaveFlag, gsSaveExecute, gsSaveTopic)
    end
end

-- |[Object Handler]|
--Standard handler for objects. Saves the object, prints a dialogue, advances time.
local fnStdObject = function(iIndex, saStrings)
	
    --Argument check.
    if(iIndex == nil) then return end
    
	--Store it in the inventory.
	local i = iIndex
	local iExaminedAlready = fnStdStore(i)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
    
    --Append.
    for i = 1, #saStrings, 1 do
		fnDialogue(saStrings[i])
    end
    
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
    if(iExaminedAlready == 0.0) then fnStdTime(sSaveFlag, sSaveExecute, sSaveTopic) end
end

-- |[ ======================================= Chat Sequences ====================================== ]|
--Chatting.
if(sTopicName == "Chat") then
	
	--Variables.
	local iChattedWithMari = VM_GetVar("Root/Variables/Chapter5/Investigation/iChattedWithMari", "N")
	
	--First pass:
	if(iChattedWithMari == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Investigation/iChattedWithMari", "N", 1.0)
		
		--Text.
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
        
		fnDialogue([[[HIDEALLFAST][CHARENTER|Laura]Laura: [E|Shock]Here I am... I-in the storage room.[P] *cough*]])
		fnDialogue([[Laura: [E|Sad]Creepy, cold storage room.[P] Not nervous at all--[CRASH][E|Mad]HAAAH!![P] What was that?]])
		fnDialogue([[Laura: [E|Blush] Oh... That's my hair b-blowing in my face. ]])
		fnDialogue([[Player: You okay, Laura?]])
		fnDialogue([[Laura: [E|Blush]G-great! yyyyep.[P][E|Happy] Er, let me know if you need me to look at anything.]])
		fnDialogue([[Thought: I guess I should focus on the investigation.]])

        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
	
        --Time loss.
        if(fnDecrementTime(1, sAutoAdvance) == false) then
            PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
        end
		
	--Repeat passes.
	else
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
        
		fnDialogue([[[HIDEALLFAST][CHARENTER|Laura]Laura: [E|Shock]Here I am... I-in the storage room.[P] *cough*]])
		fnDialogue([[Laura: [E|Sad]Creepy, cold storage room.[P] Not nervous at all--[CRASH][E|Mad]HAAAH!![P] What was that?]])
		fnDialogue([[Laura: [E|Blush] Oh... That's my hair b-blowing in my face. ]])
		fnDialogue([[Player: You okay, Laura?]])
		fnDialogue([[Laura: [E|Blush]G-great! yyyyep.[P][E|Happy] Er, let me know if you need me to look at anything.]])
		fnDialogue([[Thought: I guess I should focus on the investigation.]])
        
        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
	end

-- |[ ======================================= Characters ======================================= ]|
elseif(sTopicName == "Bret") then
    fnStdObject(gciInv5A_Bret, 
    {"[HIDEALLFAST][CHARENTER|Laura]Laura: [E|Sad]I-I wish Bret was here.[P] *hrm* He could probably explain the significance of all these things.", 
     "Player: Well, at least I'm here.",
     "Laura: [E|Shock]O-of course![P][E|Blush] And that's [ITALICS]great[ENDITALICS]."})

elseif(sTopicName == "Mari") then
    fnStdObject(gciInv5A_Mari, 
    {"[HIDEALLFAST][CHARENTER|Laura]Laura: [E|Sad]I-I wish Mari was here. [P]*hrm* She could tell some jokes to ease the tension.", 
     "Player: Well, I'm here!",
     "Laura: [E|Shock]O-of course![P][E|Blush] And that's [ITALICS]great[ENDITALICS]."})

elseif(sTopicName == "Stacy") then
    fnStdObject(gciInv5A_Stacy, 
    {"[HIDEALLFAST][CHARENTER|Laura]Laura: [E|Sad]I-I wish Stacy was here.[P] *hrm* She'd fight any baddies who try to stop me from peeking around.", 
     "Thought: [CRAHS][ITALICS]Wh--[P]are we expecting baddies to show up!?[ENDITALICS]"})

elseif(sTopicName == "Michelle") then
    fnStdObject(gciInv5A_Michelle, 
    {"[HIDEALLFAST][CHARENTER|Laura]Laura: [E|Sad]I-I wish Michelle was here.[P] *hrm* She'd keep a lookout while I dig through all these things.", 
     "Thought: [CRASH][ITALICS]I'm standing right here, you know![ENDITALICS]"})

elseif(sTopicName == "Laura") then
    fnStdObject(gciInv5A_Laura, 
    {"[HIDEALLFAST][CHARENTER|Laura]Laura: [E|Sad]A storage room is...[P]where things go to be forgotten, don't you think?[P][E|Happy] Ooh, that sounds like a cool *ahem* s-song lyric."})

elseif(sTopicName == "Todd") then
    fnStdObject(gciInv5A_Todd, 
    {"[HIDEALLFAST][CHARENTER|Laura]Laura: [E|N]So, Todd's tagged along on this adventure.[P] I'm glad the two of you are becoming so close.", 
     "Player: a--psh--w-well,[P] you know... [P]*Ahem*... y-yeah.",
     "Laura: [E|Happy]*giggle* f-for once, I'm not the most flustered person i-in the conversation!"})

-- |[ ======================================= Main Room ======================================== ]|
elseif(sTopicName == "ReceptionDesk") then
    fnStdObject(gciInv5A_Reception, 
    {"Thought: No dialogue."})

elseif(sTopicName == "GeorgeFosterPortrait") then
    fnStdObject(gciInv5A_GeorgeFosterPortrait, 
    {"Thought: No dialogue."})

elseif(sTopicName == "BiminiRecording") then
    fnStdObject(gciInv5A_BiminiRecording, 
    {"Thought: No dialogue."})

elseif(sTopicName == "Mural") then
    fnStdObject(gciInv5A_Mural, 
    {"Thought: No dialogue."})

-- |[ ======================================= Twin Room ======================================== ]|
elseif(sTopicName == "TankMaps") then
    fnStdObject(gciInv5A_TankMaps, 
    {"Thought: No dialogue."})

elseif(sTopicName == "PhotoPlayer") then
    fnStdObject(gciInv5A_PhotoPlayer, 
    {"Thought: No dialogue."})

elseif(sTopicName == "Photo34") then
    fnStdObject(gciInv5A_Photo34, 
    {"Thought: No dialogue."})

elseif(sTopicName == "Photo56") then
    fnStdObject(gciInv5A_Photo56, 
    {"Thought: No dialogue."})

elseif(sTopicName == "Photo78") then
    fnStdObject(gciInv5A_Photo78, 
    {"Thought: No dialogue."})

elseif(sTopicName == "Photo910") then
    fnStdObject(gciInv5A_Photo910, 
    {"Thought: No dialogue."})

elseif(sTopicName == "DoubleStorageTubes") then
    fnStdObject(gciInv5A_DoubleStorageTubes, 
    {"Thought: No dialogue."})

elseif(sTopicName == "KaneKola") then
    fnStdObject(gciInv5A_KaneKola, 
    {"Thought: No dialogue."})

elseif(sTopicName == "DoubleSecureTable") then
    fnStdObject(gciInv5A_DoubleSecureTable, 
    {"Thought: No dialogue."})

elseif(sTopicName == "HeavyMachinery") then
    fnStdObject(gciInv5A_HeavyMachinery, 
    {"Thought: No dialogue."})

-- |[ ==================================== Agent's Quarters ==================================== ]|
elseif(sTopicName == "Paperwork") then
    fnStdObject(gciInv5A_Paperwork, 
    {"Thought: No dialogue."})

elseif(sTopicName == "Personnel") then
    fnStdObject(gciInv5A_Personnel, 
    {"Thought: No dialogue."})

elseif(sTopicName == "EiletteFile") then
    fnStdObject(gciInv5A_EiletteFile, 
    {"Thought: No dialogue."})

elseif(sTopicName == "CandaceFoster") then
    fnStdObject(gciInv5A_CandaceFoster, 
    {"Thought: No dialogue."})

elseif(sTopicName == "CoffeeCup") then
    fnStdObject(gciInv5A_CoffeeCup, 
    {"Thought: No dialogue."})

elseif(sTopicName == "FrogSociety") then
    fnStdObject(gciInv5A_FrogSociety, 
    {"Thought: No dialogue."})

-- |[ ====================================== Quartz Room ======================================= ]|
elseif(sTopicName == "Notes") then
    fnStdObject(gciInv5A_Notes, 
    {"Thought: No dialogue."})

elseif(sTopicName == "PhotoCrater") then
    fnStdObject(gciInv5A_PhotoCrater, 
    {"Thought: No dialogue."})

elseif(sTopicName == "Chamber") then
    fnStdObject(gciInv5A_Chamber, 
    {"Thought: No dialogue."})

elseif(sTopicName == "KanePhoto") then
    fnStdObject(gciInv5A_KanePhoto, 
    {"Thought: No dialogue."})

elseif(sTopicName == "JaniceCandacePhoto") then
    fnStdObject(gciInv5A_JaniceCandacePhoto, 
    {"Thought: No dialogue."})

elseif(sTopicName == "BrokenStorageTube") then
    fnStdObject(gciInv5A_BrokenStorageTube, 
    {"Thought: No dialogue."})

elseif(sTopicName == "StorageTube") then
    fnStdObject(gciInv5A_StorageTube, 
    {"Thought: No dialogue."})

-- |[ ======================================== Storage ========================================= ]|
elseif(sTopicName == "OldSodaRecording") then
    fnStdObject(gciInv5A_OldSodaRecording, 
    {"[HIDEALLFAST][CHARENTER|Laura][CHARENTER|Todd]Todd: [E|Mad]Wow, this recording gives me [ITALICS]MAD[ENDITALICS] Fallout vibes.",
     "Laura: [E|Happy]I know, right?[E|Blush] I-I was just going to say that.[P][E|Shock] Oh--[PLAYERNAME], that's a video game by the way.",
     "Player: Cool.[P] Can I play it sometime?",
     "Laura: [E|Sad]Mmm, I would probably start you off on something easier.[P][E|Mad] *hrm* Something like...[P] like...",
     "Todd: [E|Mad]What's a good video game for someone who's never used a computer before?",
     "Thought: Laura and Todd meet eyes.",
     "Both: [FOCUS|Todd][FOCUSMOD|Laura|true][EMOTION|Todd|Shock][EMOTION|Laura|Shock]'Putt Putt Saves the Zoo'!"})

elseif(sTopicName == "ComputerParts") then
    fnStdObject(gciInv5A_ComputerParts, 
    {"[HIDEALLFAST][CHARENTER|Laura]Laura: [E|Happy]A computer virus was such a n-nebulous concept back in the 80s!",
     "Laura: Basically, if your PC wasn't working *cough* properly, it 'had a virus'...[P][E|Mad] My parents still think that way, honestly."})

elseif(sTopicName == "PhotoKaneAdam") then
    fnStdObject(gciInv5A_PhotoKaneAdam, 
    {"[HIDEALLFAST][CHARENTER|Laura]Laura: [E|Sad]Adam... ",
     "Player: You okay, Laura?",
     "Laura: [E|Shock]Y-yep![P][E|Happy] I was just reminded of something unrelated."})

elseif(sTopicName == "MessyFileCabinet") then
    fnStdObject(gciInv5A_MessyFileCabinet, 
    {"[HIDEALLFAST][CHARENTER|Laura]Player: I bet you know what this means.",
     "Laura: [E|Sad]*sigh*... Looks like a program.[P] I can understand some of the commands, *hrm* but I d-don't know what the language is.",
     "Thought: She gestures at the categories in the file cabinet.",
     "Laura: [E|Shock]But each of these...[P]seem to indicate they were trying to interconnect a *ahem*[P] a-a bunch of different electronic systems.",
     "Laura: [E|Happy] Heh--k-kind of like a universal remote."})

elseif(sTopicName == "DirtyMattress") then
    fnStdObject(gciInv5A_DirtyMattress, 
    {"[HIDEALLFAST][CHARENTER|Laura]Laura: [E|Mad]Eeep![P] Wh-why is there a bloody mattress here!?[P] Literally, it has b-b-blood on it!",
     "Player: Oh.[P] Ew."})

elseif(sTopicName == "Grafitti") then
    fnStdObject(gciInv5A_Grafitti, 
    {"[HIDEALLFAST][CHARENTER|Laura]Laura: [E|Mad]These markings are... so strange.[P][E|Blush] *hrm* I-it looks kinda like you, doesn't it,[PLAYERNAME]?",
     "Player: I don't...[P]see a resemblance, personally.",
     "Thought: [ITALICS]Really, I'd rather not see one.[ENDITALICS]"})
end