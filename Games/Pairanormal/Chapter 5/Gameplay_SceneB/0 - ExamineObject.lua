-- |[ ================================== Bimini Investigation ================================== ]|
--The plot thickens.

-- |[ ======================================= Variables ======================================== ]|
-- |[Arguments]|
--Get the name of what we're investigating.
local sExamine = LM_GetScriptArgument(0)

-- |[Variable Setup]|
--Used to simplify the Add Discovered Object calls.
local sBGPath = "Root/Images/Backgrounds/All/MineLobby"
local sScriptPath = gsRoot .. "../Chapter 5/Gameplay_SceneB/0 - ExamineObject.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
local sAutoAdvance = gsRoot .. "../Chapter 5/Gameplay_SceneC/7 - PatchGoingHome.lua"

--Saving
gsSaveExecute = gsRoot .. "../Chapter 5/Gameplay_SceneA/3 - Quartz.lua"
gsSaveTopic = "No Topic"
gsSaveFlag = "InvestigationMine"

-- |[ ======================================= Functions ======================================== ]|
-- |[Object Saving]|
--Stores the object as discovered.
local fnStdStore = function(i)
    
    --Get if we've examined the object before. This will be returned later.
	local iExaminedAlready = VM_GetVar("Root/Variables/Chapter5/InvestigationA/iObject|" .. gczaInv5A_List[i], "N")
    
    --BG path is resolved dynamically!
    if(i >= gciInv5LobbyStart and i <= gciInv5LobbyEnd) then
        sBGPath = "Root/Images/Backgrounds/All/MineLobby"
    elseif(i >= gciInv5TwinsStart and i <= gciInv5TwinsEnd) then
        sBGPath = "Root/Images/Backgrounds/All/MineTwinRoom"
    elseif(i >= gciInv5AgentsStart and i <= gciInv5AgentsEnd) then
        sBGPath = "Root/Images/Backgrounds/All/MineAgentRoom"
    elseif(i >= gciInv5QuartzStart and i <= gciInv5QuartzEnd) then
        sBGPath = "Root/Images/Backgrounds/All/MineQuartzRoom"
    elseif(i >= gciInv5StorageStart and i <= gciInv5StorageEnd) then
        sBGPath = "Root/Images/Backgrounds/All/MineStorage"
    end
    
    --Save it as a discovered object.
	Dia_SetProperty("Add Discovered Object", gczaInv5A_List[i], sBGPath, sScriptPath, gczaInv5A_List[i])
	VM_SetVar("Root/Variables/Chapter5/InvestigationA/iObject|" .. gczaInv5A_List[i], "N", 1.0)
    
    --Pass back.
    return iExaminedAlready
end

-- |[Time Handler]|
--Decrements time. Also saves the game.
local fnStdTime = function()
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(gsSaveFlag, gsSaveExecute, gsSaveTopic)
    end
end

-- |[Object Examination Handler]|
--Standard handler for objects. Saves the object, prints a dialogue, advances time.
local fnStdObject = function(iIndex, saStrings)
	
    --Argument check.
    if(iIndex == nil) then return end
    
	--Store it in the inventory.
	local i = iIndex
	local iExaminedAlready = fnStdStore(i)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
    
    --First string has this appended:
    saStrings[1] = "[HIDEALLFAST]" .. saStrings[1]
    
    --Append.
    for i = 1, #saStrings, 1 do
		fnDialogue(saStrings[i])
    end
    
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
    if(iExaminedAlready == 0.0) then fnStdTime(sSaveFlag, sSaveExecute, sSaveTopic) end
end

-- |[Object Removal]|
--Used when transitioning between rooms. Removes all discovered objects (except characters).
local function fnRemoveDiscovered()
    for i = gciInv5AAllObjectStart, gciInv5AAllObjectEnd, 1 do
        local sString = "Dia_SetProperty(\"Rem Discovered Object\", \"" .. gczaInv5A_List[i] .. "\")"
        fnCutsceneInstruction(sString)
    end
end

-- |[ ======================================= Transitions ====================================== ]|
-- |[To Quartz Room]|
--These change the background and change the loaded map, but do not consume time.
if(sExamine == "ToQuartz") then
    
    --Transition
    Dia_SetProperty("Set Investigation Image", "Root/Images/Backgrounds/All/MineQuartzRoom")
    Dia_SetProperty("Clear Investigation Objects")
    Dia_SetProperty("Parse SLF File", gsRoot .. "../Chapter 5/MapData/InvestigationAB.slf")
    
	--Dialogue
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][BG|Root/Images/Backgrounds/All/MineQuartzRoom]Thought: I head through the door...")
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    
    --Characters
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Investigation Characters") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Michelle", gsRoot .. "../Chapter 5/Gameplay_SceneB/5 - Present Michelle.lua") ]])
    
    --Remove all discovered objects. Subroutine handles this.
    fnRemoveDiscovered()
    
    --Replace all objects for this area.
    for i = gciInv5QuartzStart, gciInv5QuartzEnd, 1 do
        
        --Variable name.
        local sVarName = "Root/Variables/Chapter5/InvestigationA/iObject|" .. gczaInv5A_List[i]
        local iHasItem = VM_GetVar(sVarName, "N")
        local sBGPath = "Root/Images/Backgrounds/All/MineQuartzRoom"
        
        --Has the item. Add it.
        if(iHasItem == 1.0) then
            local sString = "Dia_SetProperty(\"Add Discovered Object\", \"" .. gczaInv5A_List[i] .. "\", \"" .. sBGPath .. "\", \"" .. sScriptPath .. "\", \"" .. gczaInv5A_List[i] .. "\")"
            fnCutsceneInstruction(sString)
        end
    end
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])

    --Finish Up
	fnCutsceneBlocker()
    
-- |[To Twins Room]|
elseif(sExamine == "ToTwins") then

    --Transition
    Dia_SetProperty("Set Investigation Image", "Root/Images/Backgrounds/All/MineTwinRoom")
    Dia_SetProperty("Clear Investigation Objects")
    Dia_SetProperty("Parse SLF File", gsRoot .. "../Chapter 5/MapData/InvestigationAD.slf")
    
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][BG|Root/Images/Backgrounds/All/MineTwinRoom]Thought: I head through the door...")
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    
    --Remove all discovered objects. Subroutine handles this.
    fnRemoveDiscovered()
    
    --Replace all objects for this area.
    for i = gciInv5TwinsStart, gciInv5TwinsEnd, 1 do
        
        --Variable name.
        local sVarName = "Root/Variables/Chapter5/InvestigationA/iObject|" .. gczaInv5A_List[i]
        local iHasItem = VM_GetVar(sVarName, "N")
        local sBGPath = "Root/Images/Backgrounds/All/MineTwinRoom"
        
        --Has the item. Add it.
        if(iHasItem == 1.0) then
            local sString = "Dia_SetProperty(\"Add Discovered Object\", \"" .. gczaInv5A_List[i] .. "\", \"" .. sBGPath .. "\", \"" .. sScriptPath .. "\", \"" .. gczaInv5A_List[i] .. "\")"
            fnCutsceneInstruction(sString)
        end
    end
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    
    --Characters
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Investigation Characters") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Bret",     gsRoot .. "../Chapter 5/Gameplay_SceneB/3 - Present Bret.lua") ]])
    
    --Finish Up
	fnCutsceneBlocker()

-- |[To Storage Room]|
elseif(sExamine == "ToStorage") then

    --Transition
    Dia_SetProperty("Set Investigation Image", "Root/Images/Backgrounds/All/MineStorage")
    Dia_SetProperty("Clear Investigation Objects")
    Dia_SetProperty("Parse SLF File", gsRoot .. "../Chapter 5/MapData/InvestigationAC.slf")
    
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][BG|Root/Images/Backgrounds/All/MineStorage]Thought: I head through the door...")
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    
    --Characters
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Investigation Characters") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Laura",    gsRoot .. "../Chapter 5/Gameplay_SceneB/4 - Present Laura.lua") ]])
    local iDecoratedGym = VM_GetVar("Root/Variables/ChapterVars/Chapter4/iDecoratedGym", "N")
    if(iDecoratedGym == 1.0) then
        fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Todd",     gsRoot .. "../Chapter 5/Gameplay_SceneB/6 - Present Todd.lua") ]])
    end
    
    --Remove all discovered objects. Subroutine handles this.
    fnRemoveDiscovered()
    
    --Replace all objects for this area.
    for i = gciInv5StorageStart, gciInv5StorageEnd, 1 do
        
        --Variable name.
        local sVarName = "Root/Variables/Chapter5/InvestigationA/iObject|" .. gczaInv5A_List[i]
        local iHasItem = VM_GetVar(sVarName, "N")
        local sBGPath = "Root/Images/Backgrounds/All/MineStorage"
        
        --Has the item. Add it.
        if(iHasItem == 1.0) then
            local sString = "Dia_SetProperty(\"Add Discovered Object\", \"" .. gczaInv5A_List[i] .. "\", \"" .. sBGPath .. "\", \"" .. sScriptPath .. "\", \"" .. gczaInv5A_List[i] .. "\")"
            fnCutsceneInstruction(sString)
        end
    end
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    
    --Finish Up
	fnCutsceneBlocker()

-- |[To Agents Room]|
elseif(sExamine == "ToAgents") then

    --Transition
    Dia_SetProperty("Set Investigation Image", "Root/Images/Backgrounds/All/MineAgentRoom")
    Dia_SetProperty("Clear Investigation Objects")
    Dia_SetProperty("Parse SLF File", gsRoot .. "../Chapter 5/MapData/InvestigationAE.slf")
    
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][BG|Root/Images/Backgrounds/All/MineAgentRoom]Thought: I head through the door...")
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    
    --Characters
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Investigation Characters") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Stacy",    gsRoot .. "../Chapter 5/Gameplay_SceneB/2 - Present Stacy.lua") ]])
    
    --Remove all discovered objects. Subroutine handles this.
    fnRemoveDiscovered()
    
    --Replace all objects for this area.
    for i = gciInv5AgentsStart, gciInv5AgentsEnd, 1 do
        
        --Variable name.
        local sVarName = "Root/Variables/Chapter5/InvestigationA/iObject|" .. gczaInv5A_List[i]
        local iHasItem = VM_GetVar(sVarName, "N")
        local sBGPath = "Root/Images/Backgrounds/All/MineAgentRoom"
        
        --Has the item. Add it.
        if(iHasItem == 1.0) then
            local sString = "Dia_SetProperty(\"Add Discovered Object\", \"" .. gczaInv5A_List[i] .. "\", \"" .. sBGPath .. "\", \"" .. sScriptPath .. "\", \"" .. gczaInv5A_List[i] .. "\")"
            fnCutsceneInstruction(sString)
        end
    end
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    
    --Finish Up
	fnCutsceneBlocker()

-- |[To Lobby]|
elseif(sExamine == "ToLobby") then

    --Transition
    Dia_SetProperty("Set Investigation Image", "Root/Images/Backgrounds/All/MineLobby")
    Dia_SetProperty("Clear Investigation Objects")
    Dia_SetProperty("Parse SLF File", gsRoot .. "../Chapter 5/MapData/InvestigationAA.slf")
    
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
    fnDialogue("[HIDEALLFAST][BG|Root/Images/Backgrounds/All/MineLobby]Thought: I head back to the lobby...")
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    
    --Characters
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Investigation Characters") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Mari",     gsRoot .. "../Chapter 5/Gameplay_SceneB/1 - Present Mari.lua") ]])
    
    --Remove all discovered objects. Subroutine handles this.
    fnRemoveDiscovered()
    
    --Replace all objects for this area.
    for i = gciInv5LobbyStart, gciInv5LobbyEnd, 1 do
        
        --Variable name.
        local sVarName = "Root/Variables/Chapter5/InvestigationA/iObject|" .. gczaInv5A_List[i]
        local iHasItem = VM_GetVar(sVarName, "N")
        local sBGPath = "Root/Images/Backgrounds/All/MineLobby"
        
        --Has the item. Add it.
        if(iHasItem == 1.0) then
            local sString = "Dia_SetProperty(\"Add Discovered Object\", \"" .. gczaInv5A_List[i] .. "\", \"" .. sBGPath .. "\", \"" .. sScriptPath .. "\", \"" .. gczaInv5A_List[i] .. "\")"
            fnCutsceneInstruction(sString)
        end
    end
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
    
    --Finish Up
	fnCutsceneBlocker()
    
-- |[ ======================================= Characters ======================================= ]|
elseif(sExamine == "Bret") then
    fnStdObject(gciInv5A_Bret, 
    {"Thought: Bret says he's gonna check out the door on the left."})

elseif(sExamine == "Mari") then
    fnStdObject(gciInv5A_Mari, 
    {"Thought: Mari's keeping an eye out in the foyer."})

elseif(sExamine == "Stacy") then
    fnStdObject(gciInv5A_Stacy, 
    {[[Thought: Stacy said she was headed to the room marked \"Surveillance\"[P]... whichever one that is.]]})

elseif(sExamine == "Michelle") then
    fnStdObject(gciInv5A_Michelle, 
    {"Thought: Michelle is in the room upstairs."})

elseif(sExamine == "Laura") then
    fnStdObject(gciInv5A_Laura, 
    {"Thought: Laura said she was going to the storage room...[P] wherever that is."})

elseif(sExamine == "Todd") then
    fnStdObject(gciInv5A_Todd, 
    {"Thought: Todd said he was going to the storage room...[P] wherever that is."})

-- |[ ======================================= Main Room ======================================== ]|
elseif(sExamine == "ReceptionDesk") then
    fnStdObject(gciInv5A_Reception, 
    {"Thought: An abandoned check-in desk.[P] Hard to believe this place had a lot of visitors back in the day."})

elseif(sExamine == "GeorgeFosterPortrait") then
    fnStdObject(gciInv5A_GeorgeFosterPortrait, 
    {"Thought: The face is unmistakable. Looks like Kane McKay Foster.[P] Except he's in a wheelchair."})

elseif(sExamine == "BiminiRecording") then
    fnStdObject(gciInv5A_BiminiRecording, 
    {"Thought: I press a button under the run-down logo.[P] An age-old recording crackles to life.",
     [[Voice: [ITALICS]\"Welcome to the Bimini Foundation, ...*ksssht* to you by the NEW Foster Industries.\"[ENDITALICS] ]],
     [[Voice: [ITALICS]Bimini is at the forefront of innov*kssshhht*... Together, we'll crack the fountain of youth.\"[ENDITALICS] ]]})

elseif(sExamine == "Mural") then
    fnStdObject(gciInv5A_Mural, 
    {"Thought: I press a button by the futuristic mural.[P] I hear a recording of someone--presumably Kane Foster, narrating to me. ",
     "Voice: [ITALICS]''If you could *kssshht*... someone else's life, would you still be yourself?[P] Would you ...*ksht*self?[P] Or would you be someone new?[ENDITALICS]'' ",
     "Thought: The recording winds down abruptly.[P] It's probably broken down over the years."})

-- |[ ======================================= Twin Room ======================================== ]|
elseif(sExamine == "TankMaps") then
    fnStdObject(gciInv5A_TankMaps, 
    {"Thought: Here's a map of town with various cryptic markings in random locations.[P] Some of them are pairs of numbers."})

elseif(sExamine == "PhotoPlayer") then
    fnStdObject(gciInv5A_PhotoPlayer, 
    {"Thought: I feel the hairs on the back of my neck prickle up.",
     "Thought: [ITALICS]That's me...[ENDITALICS] photographs of me while I was in my coma.",
     "Thought: It's unsettling to see myself like that, diagrammed up like a piece of meat."})

elseif(sExamine == "Photo34") then
    fnStdObject(gciInv5A_Photo34, 
    {"Thought: ... They're just kids.[P][P] They look kind of familiar."})

elseif(sExamine == "Photo56") then
    fnStdObject(gciInv5A_Photo56, 
    {"Thought: Twins, but one is more developed than the other.",
     "Thought: There's no mistaking it; that's agent Six from Eilette's organization."})

elseif(sExamine == "Photo78") then
    fnStdObject(gciInv5A_Photo78, 
    {"Thought: I recognize the two figures in the photo immediately.",
	 "Thought: Numbersevenrech.[P] Numbereightleif.[P] Even as kids, they had an eerie synchronicity."})

elseif(sExamine == "Photo910") then
    fnStdObject(gciInv5A_Photo910, 
    {"Thought: A cold sweat breaks out as I look at the final handful of photos.",
     "Thought: [ITALICs]That's Terry and Ross...[ENDITALICS]They were obviously taken without the twin's knowledge."})

elseif(sExamine == "DoubleStorageTubes") then
    fnStdObject(gciInv5A_DoubleStorageTubes, 
    {"Thought: Two storage tubes with a dark brown liquid inside them.[P] Whatever was in them before is long gone."})

elseif(sExamine == "KaneKola") then
    fnStdObject(gciInv5A_KaneKola, 
    {"Thought: I give the bottle a tentative sniff. It's sticky sweet, like soda.",
     "Thought: Kind of like the liquid inside the nearby storage tubes.[P][ITALICS] That can't actually be soda???[ENDITALICS] "})

elseif(sExamine == "DoubleSecureTable") then
    fnStdObject(gciInv5A_DoubleSecureTable, 
    {"Thought: Two cold metal tables with straps, to hold down... [P]I'd rather not picture it, actually."})

elseif(sExamine == "HeavyMachinery") then
    fnStdObject(gciInv5A_HeavyMachinery, 
    {"Thought: Looks like these machines have corroded slightly over time, not that they were much to look at to begin with."})

-- |[ ==================================== Agent's Quarters ==================================== ]|
elseif(sExamine == "Paperwork") then
    fnStdObject(gciInv5A_Paperwork, 
    {"Thought: Reams of paperwork here. [P]I don't pity whoever sat at this desk."})

elseif(sExamine == "Personnel") then
    fnStdObject(gciInv5A_Personnel, 
    {"Thought: All of Kane's former agents appear to be here.[P] Are they still active, now that he's dead?"})

elseif(sExamine == "EiletteFile") then
    fnStdObject(gciInv5A_EiletteFile, 
    {"Thought: This is a photo of Eilette![P] She has a different name here, as well--",
     "Thought: It's circled.. .cop... comp...[P] *sigh*,[P] I'm too stressed to read it."})

elseif(sExamine == "CandaceFoster") then
    fnStdObject(gciInv5A_CandaceFoster, 
    {"Thought: Photos of surveillance of Ms. Foster.[P] There's a man here who I recognize from my dreams."})

elseif(sExamine == "CoffeeCup") then
    fnStdObject(gciInv5A_CoffeeCup, 
    {"Thought: Of all the places in this facility, this office looks to be the most recently abandoned.",
     "Thought: There's still traces of coffee in this mug!"})

elseif(sExamine == "FrogSociety") then
    fnStdObject(gciInv5A_FrogSociety, 
    {"Thought: All the members of the frog society, including the woman from my dreams--",
     "Thought: I recognize almost all of them, except for the one at the top."})

-- |[ ====================================== Quartz Room ======================================= ]|
elseif(sExamine == "Notes") then
    fnStdObject(gciInv5A_Notes, 
    {"Thought: These notes are extensive.[P] I don't have the skills to scan through them, though. "})

elseif(sExamine == "PhotoCrater") then
    fnStdObject(gciInv5A_PhotoCrater, 
    {"Thought: When I look at this photo, [GLITCH][GLITCH]something hums inside me.[P] I feel an urge to stroke the image fondly, wistfully."})

elseif(sExamine == "Chamber") then
    fnStdObject(gciInv5A_Chamber, 
    {"Thought: [GLITCH][GLITCHTEXT]ESCAPEE S C A P EESCAPEES C APE[ENDGLITCHTEXT][GLITCH]I-I'm gonna be sick.[P][GLITCH] I can't even look at it."})

elseif(sExamine == "KanePhoto") then
    fnStdObject(gciInv5A_KanePhoto, 
    {"Thought: Kind of weird to keep a photo of yourself on your desk-- maybe that's just the kind of man Kane was."})

elseif(sExamine == "JaniceCandacePhoto") then
    fnStdObject(gciInv5A_JaniceCandacePhoto, 
    {"Thought: A woman and a kid...[P] and another woman."})

elseif(sExamine == "BrokenStorageTube") then
    fnStdObject(gciInv5A_BrokenStorageTube, 
    {"Thought: There's an unsettling amount of completely smashed vessels in this facility."})

elseif(sExamine == "StorageTube") then
    fnStdObject(gciInv5A_StorageTube, 
    {"Thought: A storage tube with water running inside it. Looks a lot like[GLITCH] the ones from the clubhouse."})

-- |[ ======================================== Storage ========================================= ]|
elseif(sExamine == "OldSodaRecording") then
    fnStdObject(gciInv5A_OldSodaRecording, 
    {"Thought: I press the button at the base of the discarded mascot.[P] A garbled voice weakly parrots some ancient recording to me... ",
     [[ [ITALICS]\"Under the bimini project we developed a new formula f...*ksssht*[P] stronger than ever, helping your cells to regenerate...\"[ENDITALICS] ]],
     [[ [ITALICS]\"*ksht* *ksht*... Drink it on the job, and you've never felt better!... [P]kane kola![P] It's good fer ya!\"[ENDITALICS] ]]})

elseif(sExamine == "ComputerParts") then
    fnStdObject(gciInv5A_ComputerParts, 
    {[[ Thought: \"DO NOT LOOT! ab-so-lu...[P]absolutely DO NOT PulG IN! Tehse parts have been... in-fec-ted with a virus!\" ]],
     "Thought: Whew, handwriting is tough to read."})

elseif(sExamine == "PhotoKaneAdam") then
    fnStdObject(gciInv5A_PhotoKaneAdam, 
    {"Thought: I press the button at the base of the photo. The recording that plays back is fuzzy, but I can hear most of it.",
     [[ Voice: [ITALICS]\"What would our world look like if we *ksssht*... live on in our computers?[P] You don't have to wonder anymore.\"[ENDITALICS] ]],
     [[ Voice: [ITALICS]\"I want you to meet the Bimini Project's new*ksht*... technology...\"[ENDITALICS] ]],
     [[ Voice: [ITALICS]\"The Artificial Dreaming Architecture Module--of course, we just like to call him a simpler name--*ksssshhhht*\"[ENDITALICS] ]]})

elseif(sExamine == "MessyFileCabinet") then
    fnStdObject(gciInv5A_MessyFileCabinet, 
    {"Thought: these cabinets have lots of notes in them, but I can't read well enough to flip through them easily."})

elseif(sExamine == "DirtyMattress") then
    fnStdObject(gciInv5A_DirtyMattress, 
    {"Thought: God, was someone sleeping on that? [P][GLITCH]Looking at it makes me feel... sick."})

elseif(sExamine == "Grafitti") then
    fnStdObject(gciInv5A_Grafitti, 
    {"Thought: I'm pretty sure I know who that is, but I don't want to believe it."})

end