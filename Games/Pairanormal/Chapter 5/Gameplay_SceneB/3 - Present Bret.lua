-- |[ =================================== Bret Presentation ==================================== ]|
--Showing stuff to Bret.

-- |[Arg Resolve]|
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variable Setup]|
--Used to simplify the Add Discovered Object calls.
local sBGPath = "Root/Images/Backgrounds/All/MineLobby"
local sScriptPath = gsRoot .. "../Chapter 5/Gameplay_SceneB/0 - ExamineObject.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
local sAutoAdvance = gsRoot .. "../Chapter 5/Gameplay_SceneC/7 - PatchGoingHome.lua"

--Saving
gsSaveExecute = gsRoot .. "../Chapter 5/Gameplay_SceneA/3 - Quartz.lua"
gsSaveTopic = "No Topic"
gsSaveFlag = "InvestigationMine"

-- |[ ========================================= Functions ========================================= ]|
-- |[Storage Handler]|
--Stores the object as discovered.
local fnStdStore = function(i)
    
    --Get if we've examined the object before. This will be returned later.
	local iExaminedAlready = VM_GetVar("Root/Variables/Chapter5/InvestigationA/iObject|" .. gczaInv5A_List[i], "N")
    
    --Save it as a discovered object.
	Dia_SetProperty("Add Discovered Object", gczaInv5A_List[i], sBGPath, sScriptPath, gczaInv5A_List[i])
	VM_SetVar("Root/Variables/Chapter5/InvestigationA/iObject|" .. gczaInv5A_List[i], "N", 1.0)
    
    --Pass back.
    return iExaminedAlready
end

-- |[Friendship Increment]|
--If this is the first time presenting this object to this character then we increase the friendship tracker for
-- that character.
local fnIncrementFriendship = function(iObjectIndex, iIncrement)
	--Does nothing in this chapter.
end

-- |[Time Handler]|
--Decrements time. Also saves the game.
local fnStdTime = function()
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(gsSaveFlag, gsSaveExecute, gsSaveTopic)
    end
end

-- |[Object Handler]|
--Standard handler for objects. Saves the object, prints a dialogue, advances time.
local fnStdObject = function(iIndex, saStrings)
	
    --Argument check.
    if(iIndex == nil) then return end
    
	--Store it in the inventory.
	local i = iIndex
	local iExaminedAlready = fnStdStore(i)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
    
    --Append.
    for i = 1, #saStrings, 1 do
		fnDialogue(saStrings[i])
    end
    
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
    if(iExaminedAlready == 0.0) then fnStdTime(sSaveFlag, sSaveExecute, sSaveTopic) end
end

-- |[ ======================================= Chat Sequences ====================================== ]|
--Chatting.
if(sTopicName == "Chat") then
	
	--Variables.
	local iChattedWithBret = VM_GetVar("Root/Variables/Chapter4/Investigation/iChattedWithBret", "N")
	
	--First pass:
	if(iChattedWithBret == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter4/Investigation/iChattedWithBret", "N", 1.0)
		
		--Text.
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
    
		fnDialogue([[[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|N]I solemly swear to put all my powers of observation to use for you, [PLAYERNAME]![P][E|Mad] Bring me anything of interest.]])
		fnDialogue([[Player: Will do.]])
		fnDialogue([[Thought: Even Bret's not as talkative this time around.[P] I should focus on the investigation.]])

        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
	
        --Time loss.
        if(fnDecrementTime(1, sAutoAdvance) == false) then
            PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
        end
		
	--Repeat passes.
	else
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
    
		fnDialogue([[[HIDEALLFAST][SHOWCHAR|Bret|Neutral]Bret: [E|N]I solemly swear to put all my powers of observation to use for you, [PLAYERNAME]![P][E|Mad] Bring me anything of interest.]])
		fnDialogue([[Player: Will do.]])
		fnDialogue([[Thought: Even Bret's not as talkative this time around.[P] I should focus on the investigation.]])
        
        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
	end
	
-- |[ ======================================= Characters ======================================= ]|
elseif(sTopicName == "Bret") then
    fnStdObject(gciInv5A_Bret, 
    {"[HIDEALLFAST][CHARENTER|Bret]Bret: [E|Shock]Wow...[E|Mad]when you said this might be the most important investigation we've ever conducted.[P][E|Sad] Y-you weren't kidding, huh?"})

elseif(sTopicName == "Mari") then
    fnStdObject(gciInv5A_Mari, 
    {"[HIDEALLFAST][CHARENTER|Bret]Player: Mari's in the foyer if you need her.", 
     "Bret: [E|Shock]I think I'm gonna need, like, therapy after this,[P] not Mari."})

elseif(sTopicName == "Stacy") then
    fnStdObject(gciInv5A_Stacy, 
    {"[HIDEALLFAST][CHARENTER|Bret]Player: Stacy's in the surveillance room.",
     "Bret: [E|Shock]Ah, what? There's a surveillance room?",
     "Bret: [E|Sad]Why didn't I go in there, instead of...[P][E|Mad]the room with all the creepy twin photographs.",
     "Bret: [E|N]What's the name of this room, anyway?[E|Happy] 'Creepy twin photograph room'?"})

elseif(sTopicName == "Michelle") then
    fnStdObject(gciInv5A_Michelle, 
    {"[HIDEALLFAST][CHARENTER|Bret]Bret: [E|N]I think I can hear Michelle stomping around somewhere nearby.",
     "Player: She's upstairs.",
     "Bret: [E|Happy]Heh, figures.[P] Michelle says she stole her steel-toed boots off some dead guy, but there wasn't any scuffing.[P] [E|Mad]I knew she bought them for full retail price.",
     "Bret: [E|Blush]Her secret is safe with me, of course."})

elseif(sTopicName == "Laura") then
    fnStdObject(gciInv5A_Laura, 
    {"[HIDEALLFAST][CHARENTER|Bret]Bret: Where's Laura?",
     "Player: Somewhere quiet, I imagine.",
     "Bret: [E|Shock]Hm.[P] That's a great educated guess!",
     "Player: Learning from the best.",
     "Bret: [E|Blush]Pshaw---w-I mean, Well, I'm--",
     "Player: The Paranormal Club.",
     "Bret: [E|Sad]--Paranormal Club, is who you were referring to, [E|N]and so was I.[P] Yes.",
     "Thought: [ITALICS]Oh Bret.[P] You are so easy to tease.[ENDITALICS]"})

elseif(sTopicName == "Todd") then
    fnStdObject(gciInv5A_Todd, 
    {"[HIDEALLFAST][CHARENTER|Bret]Bret: [E|Shock]You know, [PLAYERNAME],[P] I've been giving it a lot of thought,",
     "Bret: [E|Blush]And I'd be happy to take up the ol' tutoring torch now that [E|Mad]Todd isn't...[P] potentially,[P][E|Shock] you know,[P][E|Blush] filling that role.",
     "Player: Thanks Bret, but I'd rather we just stay friends.",
     "Bret: [E|Shock]Yeaaaah, cool.[P] Probably for the best."})

-- |[ ======================================= Main Room ======================================== ]|
elseif(sTopicName == "ReceptionDesk") then
    fnStdObject(gciInv5A_Reception, 
    {"Thought: No dialogue."})

elseif(sTopicName == "GeorgeFosterPortrait") then
    fnStdObject(gciInv5A_GeorgeFosterPortrait, 
    {"Thought: No dialogue."})

elseif(sTopicName == "BiminiRecording") then
    fnStdObject(gciInv5A_BiminiRecording, 
    {"Thought: No dialogue."})

elseif(sTopicName == "Mural") then
    fnStdObject(gciInv5A_Mural, 
    {"Thought: No dialogue."})

-- |[ ======================================= Twin Room ======================================== ]|
elseif(sTopicName == "TankMaps") then
    fnStdObject(gciInv5A_TankMaps, 
    {"[HIDEALLFAST][CHARENTER|Bret]Thought: Bret taps one section of the map,[P] not circled in red but discreetly marked with a thin, black line.",
     "Bret: [E|Mad]That's where our clubhouse is.[P] Why isn't it circled?",
     "Player: Maybe it wasn't as important as the others."})

elseif(sTopicName == "PhotoPlayer") then
    fnStdObject(gciInv5A_PhotoPlayer, 
    {"[HIDEALLFAST][CHARENTER|Bret]Bret: [E|Shock]That's n-not... uh,[P] that can't be.",
     "Player: It's me.[P] When I was in a coma.[P] Kinda messed up, isn't it?",
     "Thought: Bret takes the photo off the wall and gingerly places it face down on the desk.",
     "Thought: .[P][P].[P][P].",
     "Bret: [E|Sad]Only because it doesn't do the real you justice."})

elseif(sTopicName == "Photo34") then
    fnStdObject(gciInv5A_Photo34, 
    {"[HIDEALLFAST][CHARENTER|Bret]Player: Who knows who these kids might be?",
     "Bret: [E|N]Well, they'd be adults now.",
     "Player: Huh?",
     "Bret: [E|N]This photo was taken in,[P] like, the 60s,[P] 70s?[P] Nobody dresses like that now--",
     "Bret: [E|Mad]--well, maybe Rogelio in period four,[P] [E|Blush]and [ITALICS]me[ENDITALICS], for that one Halloween I went as Lucille Ball--",
     "Bret: [E|Shock]*ahem!* A-anyway.[P][E|Sad] these kids would be fully grown by now.",
     "Player: If they're still alive."})

elseif(sTopicName == "Photo56") then
    fnStdObject(gciInv5A_Photo56, 
    {"[HIDEALLFAST][CHARENTER|Bret]Bret: [E|Sad]Its so messed up... I always imagined photographs to represent...[P] [E|Mad]like, some kind of happy memory.",
     "Bret: Something people should like,[P] cherish and stuff,[P] y'know?",
     "Bret: [E|Shock]But all the photographs in this room...[P]there's just something really undignified about it.[P][E|Sad] It's kind of ruining it for me."})

elseif(sTopicName == "Photo78") then
    fnStdObject(gciInv5A_Photo78, 
    {"[HIDEALLFAST][CHARENTER|Bret]Bret: [E|Sad]Its so messed up... I always imagined photographs to represent...[P] [E|Mad]like, some kind of happy memory.",
     "Bret: Something people should like,[P] cherish and stuff,[P] y'know?",
     "Bret: [E|Shock]But all the photographs in this room...[P]there's just something really undignified about it.[P][E|Sad] It's kind of ruining it for me."})

elseif(sTopicName == "Photo910") then
    fnStdObject(gciInv5A_Photo910, 
    {"[HIDEALLFAST][CHARENTER|Bret]Bret: [E|Shock]No way.[P][CRASH] NO WAY.[P] That's Terry and Ross!!!",
     "Player: I thought it was just my imagination, but...",
     "Bret: [E|Shock]They're just kids.[P] Someone's been watching them. Wh...[P][E|Sad]Ugh, this doesn't make any sense. "})

elseif(sTopicName == "DoubleStorageTubes") then
    fnStdObject(gciInv5A_DoubleStorageTubes, 
    {"[HIDEALLFAST][CHARENTER|Bret]Bret: [E|Mad]*sniff* *sniff*.[P][E|Shock] Well, I do declare.",
     "Bret: [E|N] Those tubes are filled with Kane Kola...[P][E|Mad]mixed with something else, I think.[P][E|N] But yeah, definitely Kane Kola."})

elseif(sTopicName == "KaneKola") then
    fnStdObject(gciInv5A_KaneKola, 
    {"[HIDEALLFAST][CHARENTER|Bret]Player: Look, Bret.[P] Do you have this bottle in your collection?",
     "Bret: [E|Mad]Hah! As if.[P][E|N] I only look for pristine samples, thank you very much.",
     "Bret: [E|Happy] Mostly because shattered glass would surely cut up my beautiful, delicate collector's hands.",
     "Player: Pshh."})

elseif(sTopicName == "DoubleSecureTable") then
    fnStdObject(gciInv5A_DoubleSecureTable, 
    {"[HIDEALLFAST][CHARENTER|Bret]Bret: [E|Sad]Mannnnn, just like, no.[P] You know?[P] no.",
     "Bret: No, no, no.[P][E|Mad] Y-you don't even have to tell me--like, I know,[P] okay?",
     "Bret: [E|Shock]Photos of twins, on the wall, first of all,[P] okay?[P][E|Sad] And then, two identical tables.",
     "Bret: With identical straps, to hold down...[P]so just...[P]no!",
     "Bret: I'll say it, okay? [P][CRASH][E|Mad]FUCK.[P] NO.",
     "Player: My feelings exactly."})

elseif(sTopicName == "HeavyMachinery") then
    fnStdObject(gciInv5A_HeavyMachinery, 
    {"Thought: No dialogue."})

-- |[ ==================================== Agent's Quarters ==================================== ]|
elseif(sTopicName == "Paperwork") then
    fnStdObject(gciInv5A_Paperwork, 
    {"Thought: No dialogue."})

elseif(sTopicName == "Personnel") then
    fnStdObject(gciInv5A_Personnel, 
    {"Thought: No dialogue."})

elseif(sTopicName == "EiletteFile") then
    fnStdObject(gciInv5A_EiletteFile, 
    {"Thought: No dialogue."})

elseif(sTopicName == "CandaceFoster") then
    fnStdObject(gciInv5A_CandaceFoster, 
    {"Thought: No dialogue."})

elseif(sTopicName == "CoffeeCup") then
    fnStdObject(gciInv5A_CoffeeCup, 
    {"Thought: No dialogue."})

elseif(sTopicName == "FrogSociety") then
    fnStdObject(gciInv5A_FrogSociety, 
    {"Thought: No dialogue."})

-- |[ ====================================== Quartz Room ======================================= ]|
elseif(sTopicName == "Notes") then
    fnStdObject(gciInv5A_Notes, 
    {"Thought: No dialogue."})

elseif(sTopicName == "PhotoCrater") then
    fnStdObject(gciInv5A_PhotoCrater, 
    {"Thought: No dialogue."})

elseif(sTopicName == "Chamber") then
    fnStdObject(gciInv5A_Chamber, 
    {"Thought: No dialogue."})

elseif(sTopicName == "KanePhoto") then
    fnStdObject(gciInv5A_KanePhoto, 
    {"Thought: No dialogue."})

elseif(sTopicName == "JaniceCandacePhoto") then
    fnStdObject(gciInv5A_JaniceCandacePhoto, 
    {"Thought: No dialogue."})

elseif(sTopicName == "BrokenStorageTube") then
    fnStdObject(gciInv5A_BrokenStorageTube, 
    {"Thought: No dialogue."})

elseif(sTopicName == "StorageTube") then
    fnStdObject(gciInv5A_StorageTube, 
    {"Thought: No dialogue."})

-- |[ ======================================== Storage ========================================= ]|
elseif(sTopicName == "OldSodaRecording") then
    fnStdObject(gciInv5A_OldSodaRecording, 
    {"Thought: No dialogue."})

elseif(sTopicName == "ComputerParts") then
    fnStdObject(gciInv5A_ComputerParts, 
    {"Thought: No dialogue."})

elseif(sTopicName == "PhotoKaneAdam") then
    fnStdObject(gciInv5A_PhotoKaneAdam, 
    {"Thought: No dialogue."})

elseif(sTopicName == "MessyFileCabinet") then
    fnStdObject(gciInv5A_MessyFileCabinet, 
    {"Thought: No dialogue."})

elseif(sTopicName == "DirtyMattress") then
    fnStdObject(gciInv5A_DirtyMattress, 
    {"Thought: No dialogue."})

elseif(sTopicName == "Grafitti") then
    fnStdObject(gciInv5A_Grafitti, 
    {"Thought: No dialogue."})

end