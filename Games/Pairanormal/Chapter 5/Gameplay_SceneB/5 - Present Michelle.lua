-- |[ ================================= Michelle Presentation ================================== ]|
--Showing stuff to Todd.

-- |[Arg Resolve]|
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variable Setup]|
--Used to simplify the Add Discovered Object calls.
local sBGPath = "Root/Images/Backgrounds/All/MineLobby"
local sScriptPath = gsRoot .. "../Chapter 5/Gameplay_SceneB/0 - ExamineObject.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
local sAutoAdvance = gsRoot .. "../Chapter 5/Gameplay_SceneC/7 - PatchGoingHome.lua"

--Saving
gsSaveExecute = gsRoot .. "../Chapter 5/Gameplay_SceneA/3 - Quartz.lua"
gsSaveTopic = "No Topic"
gsSaveFlag = "InvestigationMine"

-- |[ ========================================= Functions ========================================= ]|
-- |[Storage Handler]|
--Stores the object as discovered.
local fnStdStore = function(i)
    
    --Get if we've examined the object before. This will be returned later.
	local iExaminedAlready = VM_GetVar("Root/Variables/Chapter5/InvestigationA/iObject|" .. gczaInv5A_List[i], "N")
    
    --Save it as a discovered object.
	Dia_SetProperty("Add Discovered Object", gczaInv5A_List[i], sBGPath, sScriptPath, gczaInv5A_List[i])
	VM_SetVar("Root/Variables/Chapter5/InvestigationA/iObject|" .. gczaInv5A_List[i], "N", 1.0)
    
    --Pass back.
    return iExaminedAlready
end

-- |[Friendship Increment]|
--If this is the first time presenting this object to this character then we increase the friendship tracker for
-- that character.
local fnIncrementFriendship = function(iObjectIndex, iIncrement)
	--Does nothing in this chapter.
end

-- |[Time Handler]|
--Decrements time. Also saves the game.
local fnStdTime = function()
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(gsSaveFlag, gsSaveExecute, gsSaveTopic)
    end
end

-- |[Object Handler]|
--Standard handler for objects. Saves the object, prints a dialogue, advances time.
local fnStdObject = function(iIndex, saStrings)
	
    --Argument check.
    if(iIndex == nil) then return end
    
	--Store it in the inventory.
	local i = iIndex
	local iExaminedAlready = fnStdStore(i)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
    
    --Append.
    for i = 1, #saStrings, 1 do
		fnDialogue(saStrings[i])
    end
    
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
    if(iExaminedAlready == 0.0) then fnStdTime(sSaveFlag, sSaveExecute, sSaveTopic) end
end

-- |[ ======================================= Chat Sequences ====================================== ]|
--Chatting.
if(sTopicName == "Chat") then
	
	--Variables.
	local iChattedWithMichelle = VM_GetVar("Root/Variables/Chapter5/Investigation/iChattedWithMichelle", "N")
	
	--First pass:
	if(iChattedWithMichelle == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Investigation/iChattedWithMichelle", "N", 1.0)
		
		--Text.
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
        
		fnDialogue([[[HIDEALLFAST][CHARENTER|Michelle]Michelle: [E|Sad]This is the place where we'll learn about the man behind it all, I s'pose.[P][E|N] I'll keep sharp.]])
		fnDialogue([[Player: Yeah.[P] Thanks. ]])
		fnDialogue([[Thought: Seems like nobody's in the mood for chitchat this time. I should focus on the investigation.]])

        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
	
        --Time loss.
        if(fnDecrementTime(1, sAutoAdvance) == false) then
            PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
        end
		
	--Repeat passes.
	else
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
        
		fnDialogue([[[HIDEALLFAST][CHARENTER|Michelle]Michelle: [E|Sad]This is the place where we'll learn about the man behind it all, I s'pose.[P][E|N] I'll keep sharp.]])
		fnDialogue([[Player: Yeah.[P] Thanks. ]])
		fnDialogue([[Thought: Seems like nobody's in the mood for chitchat this time. I should focus on the investigation.]])

        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
	end
	
-- |[ ======================================= Characters ======================================= ]|
elseif(sTopicName == "Bret") then
    fnStdObject(gciInv5A_Bret, 
    {"[HIDEALLFAST][CHARENTER|Michelle]Michelle: [E|Blush]Where's the nerd?",
     "Player: I think we went to check out some other room.",
     "Michelle: [E|Shock]Ugh, someone should keep an eye on that dolt.[P] He's likely to trip some kind of alarm, or something."})

elseif(sTopicName == "Mari") then
    fnStdObject(gciInv5A_Mari, 
    {"[HIDEALLFAST][CHARENTER|Michelle]Michelle: [E|Shock]D-does Mari need any company? ",
     "Player: She might.[P] She's in the lobby.",
     "Michelle: [E|Blush]Hmmm.[P] Maybe I'll stop by later.[P] Someone ought to stay with you."})

elseif(sTopicName == "Stacy") then
    fnStdObject(gciInv5A_Stacy, 
    {"[HIDEALLFAST][CHARENTER|Michelle]Player: Apparently Stacy's in the surveillance room.",
     "Michelle: [E|Shock]Oh yeah?",
     "Thought: She scans the ceiling for a camera, and spotting it, smiles wickedly while flipping it off.",
     "Michelle: [E|Happy]Heh.[P] hope she caught that."})

elseif(sTopicName == "Michelle") then
    fnStdObject(gciInv5A_Michelle, 
    {"[HIDEALLFAST][CHARENTER|Michelle]Player: How are you, Michelle?",
     "Michelle: [E|Blush]I-I'll say it. I'm pretty used to weird expeditions...[P][E|Sad] but this place is on a whole other level."})

elseif(sTopicName == "Laura") then
    fnStdObject(gciInv5A_Laura, 
    {"[HIDEALLFAST][CHARENTER|Michelle]Michelle: [E|N] Where's Laura?",
     "Player: Storage room, I think.",
     "Michelle: [E|Shock]Huh. I'm surprised she tagged along, to be honest.",
     "Michelle: [E|Happy]If I were her, I'd be scared out of my wits.[P][E|Blush] Which I'm not, obviously."})

elseif(sTopicName == "Todd") then
    fnStdObject(gciInv5A_Todd, 
    {"[HIDEALLFAST][CHARENTER|Michelle]Michelle: [E|Mad]So what's the deal with this tutor of yours? You necking or what?",
     "Player: What?",
     "Michelle: [E|Blush]Ugh, nevermind."})
 
-- |[ ======================================= Main Room ======================================== ]|
elseif(sTopicName == "ReceptionDesk") then
    fnStdObject(gciInv5A_Reception, 
    {"Thought: No dialogue."})

elseif(sTopicName == "GeorgeFosterPortrait") then
    fnStdObject(gciInv5A_GeorgeFosterPortrait, 
    {"Thought: No dialogue."})

elseif(sTopicName == "BiminiRecording") then
    fnStdObject(gciInv5A_BiminiRecording, 
    {"Thought: No dialogue."})

elseif(sTopicName == "Mural") then
    fnStdObject(gciInv5A_Mural, 
    {"Thought: No dialogue."})

-- |[ ======================================= Twin Room ======================================== ]|
elseif(sTopicName == "TankMaps") then
    fnStdObject(gciInv5A_TankMaps, 
    {"Thought: No dialogue."})

elseif(sTopicName == "PhotoPlayer") then
    fnStdObject(gciInv5A_PhotoPlayer, 
    {"Thought: No dialogue."})

elseif(sTopicName == "Photo34") then
    fnStdObject(gciInv5A_Photo34, 
    {"Thought: No dialogue."})

elseif(sTopicName == "Photo56") then
    fnStdObject(gciInv5A_Photo56, 
    {"Thought: No dialogue."})

elseif(sTopicName == "Photo78") then
    fnStdObject(gciInv5A_Photo78, 
    {"Thought: No dialogue."})

elseif(sTopicName == "Photo910") then
    fnStdObject(gciInv5A_Photo910, 
    {"Thought: No dialogue."})

elseif(sTopicName == "DoubleStorageTubes") then
    fnStdObject(gciInv5A_DoubleStorageTubes, 
    {"Thought: No dialogue."})

elseif(sTopicName == "KaneKola") then
    fnStdObject(gciInv5A_KaneKola, 
    {"Thought: No dialogue."})

elseif(sTopicName == "DoubleSecureTable") then
    fnStdObject(gciInv5A_DoubleSecureTable, 
    {"Thought: No dialogue."})

elseif(sTopicName == "HeavyMachinery") then
    fnStdObject(gciInv5A_HeavyMachinery, 
    {"Thought: No dialogue."})

-- |[ ==================================== Agent's Quarters ==================================== ]|
elseif(sTopicName == "Paperwork") then
    fnStdObject(gciInv5A_Paperwork, 
    {"Thought: No dialogue."})

elseif(sTopicName == "Personnel") then
    fnStdObject(gciInv5A_Personnel, 
    {"Thought: No dialogue."})

elseif(sTopicName == "EiletteFile") then
    fnStdObject(gciInv5A_EiletteFile, 
    {"Thought: No dialogue."})

elseif(sTopicName == "CandaceFoster") then
    fnStdObject(gciInv5A_CandaceFoster, 
    {"Thought: No dialogue."})

elseif(sTopicName == "CoffeeCup") then
    fnStdObject(gciInv5A_CoffeeCup, 
    {"Thought: No dialogue."})

elseif(sTopicName == "FrogSociety") then
    fnStdObject(gciInv5A_FrogSociety, 
    {"Thought: No dialogue."})

-- |[ ====================================== Quartz Room ======================================= ]|
elseif(sTopicName == "Notes") then
    fnStdObject(gciInv5A_Notes, 
    {"[HIDEALLFAST][CHARENTER|Michelle]Michelle: [E|Sad]Hmm... well, this is just disturbing.",
     "Player: What? What is it?",
     "Michelle: [E|Happy]This dork signs every note he makes.",
     "Michelle: It's not a letter, you idiot! You don't have to put your name at the bottom of every page.",
     "Player: But [CRASH][ITALICS]what does it actually say???[ENDITALICS]",
     "Michelle: [E|Blush]Ah, right...[P][E|Sad] ugh, his handwriting's abominable.",
     "Michelle: [E|N] Found some kind of pod. He's... recording its dimensions and other boring stuff, some rocks he found inside.",
     "Michelle: Funny... he doesn't say how he happened to stumble upon it.[P] Not so great at your job, are you, 'George'???"})

elseif(sTopicName == "PhotoCrater") then
    fnStdObject(gciInv5A_PhotoCrater, 
    {"[HIDEALLFAST][CHARENTER|Michelle]Michelle: [E|Mad]Bloody hell.[P] Is that the thing that caused the crater outside? ",
     "Player: I've never seen as asteroid before.",
     "Michelle: [E|Shock]That's no asteroid, babe.[P] Looks like something out of H.R. Geiger's wet dreams."})

elseif(sTopicName == "Chamber") then
    fnStdObject(gciInv5A_Chamber, 
    {"[HIDEALLFAST][CHARENTER|Michelle]Michelle: [E|Sad]Eh, you all right [PLAYERNAME]??? You look blue.[P] Take a seat and rest for a bit.",
     "Player: I'm fine.",
     "Michelle: [E|Mad][CRASH]It [ITALICS]wasn't[ENDITALICS] a question, dolt!",
     "Player: Eeep."})

elseif(sTopicName == "KanePhoto") then
    fnStdObject(gciInv5A_KanePhoto, 
    {"[HIDEALLFAST][CHARENTER|Michelle]Michelle: [E|N]This was the old man's desk, eh?[P] He's even messier than I am, if you can believe it."})

elseif(sTopicName == "JaniceCandacePhoto") then
    fnStdObject(gciInv5A_JaniceCandacePhoto, 
    {"[HIDEALLFAST][CHARENTER|Michelle]Michelle: [E|Happy]Mmm, methinks there was a bit more going on with ol' mister Foster than he let on.",
     "Player: What do you mean?",
     "Michelle: [E|Happy]I've seen tons of detective movies, all right?",
     "Michelle: Anytime you find a picture in a desk drawer, it means that person is a secret lover, or dead...[P][E|Shock] or both."})

elseif(sTopicName == "BrokenStorageTube") then
    fnStdObject(gciInv5A_BrokenStorageTube, 
    {"[HIDEALLFAST][CHARENTER|Michelle]Michelle: [E|Shock]Wasn't this place closed down in the 50s?",
     "Player: Yes, I think so.",
     "Michelle: [E|Sad]Huh.[P] I didn't know they made machinery like that back then."})

elseif(sTopicName == "StorageTube") then
    fnStdObject(gciInv5A_StorageTube, 
    {"[HIDEALLFAST][CHARENTER|Michelle]Michelle: [E|Shock]Wasn't this place closed down in the 50s?",
     "Player: Yes, I think so.",
     "Michelle: [E|Sad]Huh.[P] I didn't know they made machinery like that back then."})

-- |[ ======================================== Storage ========================================= ]|
elseif(sTopicName == "OldSodaRecording") then
    fnStdObject(gciInv5A_OldSodaRecording, 
    {"Thought: No dialogue."})

elseif(sTopicName == "ComputerParts") then
    fnStdObject(gciInv5A_ComputerParts, 
    {"Thought: No dialogue."})

elseif(sTopicName == "PhotoKaneAdam") then
    fnStdObject(gciInv5A_PhotoKaneAdam, 
    {"Thought: No dialogue."})

elseif(sTopicName == "MessyFileCabinet") then
    fnStdObject(gciInv5A_MessyFileCabinet, 
    {"Thought: No dialogue."})

elseif(sTopicName == "DirtyMattress") then
    fnStdObject(gciInv5A_DirtyMattress, 
    {"Thought: No dialogue."})

elseif(sTopicName == "Grafitti") then
    fnStdObject(gciInv5A_Grafitti, 
    {"Thought: No dialogue."})

end