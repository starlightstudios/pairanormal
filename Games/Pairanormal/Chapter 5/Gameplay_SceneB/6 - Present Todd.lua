-- |[ =================================== Todd Presentation ==================================== ]|
--Showing stuff to Todd.

-- |[Arg Resolve]|
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variable Setup]|
--Used to simplify the Add Discovered Object calls.
local sBGPath = "Root/Images/Backgrounds/All/MineLobby"
local sScriptPath = gsRoot .. "../Chapter 5/Gameplay_SceneB/0 - ExamineObject.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
local sAutoAdvance = gsRoot .. "../Chapter 5/Gameplay_SceneC/7 - PatchGoingHome.lua"

--Saving
gsSaveExecute = gsRoot .. "../Chapter 5/Gameplay_SceneA/3 - Quartz.lua"
gsSaveTopic = "No Topic"
gsSaveFlag = "InvestigationMine"

-- |[ ========================================= Functions ========================================= ]|
-- |[Storage Handler]|
--Stores the object as discovered.
local fnStdStore = function(i)
    
    --Get if we've examined the object before. This will be returned later.
	local iExaminedAlready = VM_GetVar("Root/Variables/Chapter5/InvestigationA/iObject|" .. gczaInv5A_List[i], "N")
    
    --Save it as a discovered object.
	Dia_SetProperty("Add Discovered Object", gczaInv5A_List[i], sBGPath, sScriptPath, gczaInv5A_List[i])
	VM_SetVar("Root/Variables/Chapter5/InvestigationA/iObject|" .. gczaInv5A_List[i], "N", 1.0)
    
    --Pass back.
    return iExaminedAlready
end

-- |[Friendship Increment]|
--If this is the first time presenting this object to this character then we increase the friendship tracker for
-- that character.
local fnIncrementFriendship = function(iObjectIndex, iIncrement)
	--Does nothing in this chapter.
end

-- |[Time Handler]|
--Decrements time. Also saves the game.
local fnStdTime = function()
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(gsSaveFlag, gsSaveExecute, gsSaveTopic)
    end
end

-- |[Object Handler]|
--Standard handler for objects. Saves the object, prints a dialogue, advances time.
local fnStdObject = function(iIndex, saStrings)
	
    --Argument check.
    if(iIndex == nil) then return end
    
	--Store it in the inventory.
	local i = iIndex
	local iExaminedAlready = fnStdStore(i)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
    
    --Append.
    for i = 1, #saStrings, 1 do
		fnDialogue(saStrings[i])
    end
    
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
    if(iExaminedAlready == 0.0) then fnStdTime(sSaveFlag, sSaveExecute, sSaveTopic) end
end

-- |[ ======================================= Chat Sequences ====================================== ]|
--Chatting.
if(sTopicName == "Chat") then
	
	--Variables.
	local iChattedWithTodd = VM_GetVar("Root/Variables/Chapter5/Investigation/iChattedWithTodd", "N")
	
	--First pass:
	if(iChattedWithTodd == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Investigation/iChattedWithTodd", "N", 1.0)
		
		--Text.
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()

		fnDialogue([[[HIDEALLFAST][CHARENTER|Todd]Todd: [E|N]Hey, you.[P] Let me know if you find something cool.]])
		fnDialogue([[Thought: Seems like nobody's in the mood for chitchat this time. I should focus on the investigation.]])

        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
	
        --Time loss.
        if(fnDecrementTime(1, sAutoAdvance) == false) then
            PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
        end
		
	--Repeat passes.
	else
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()

		fnDialogue([[[HIDEALLFAST][CHARENTER|Todd]Todd: [E|N]Hey, you.[P] Let me know if you find something cool.]])
		fnDialogue([[Thought: Seems like nobody's in the mood for chitchat this time. I should focus on the investigation.]])
        
        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
	end
	
-- |[ ======================================= Characters ======================================= ]|
elseif(sTopicName == "Bret") then
    fnStdObject(gciInv5A_Bret, 
    {"[HIDEALLFAST][CHARENTER|Todd]Player: Bret's in another room.",
     "Todd: [E|N]... Who?",
     "Player: Bret![P] You know, Bret.",
     "Todd: [E|Mad]...",
     "Player: Haven't you gone to school with this guy for like, 15 years now?",
     "Todd: [E|N]I don't know... have I?"})

elseif(sTopicName == "Mari") then
    fnStdObject(gciInv5A_Mari, 
    {"[HIDEALLFAST][CHARENTER|Todd]Player: Mari's in the lobby.",
     "Toddy: [E|N] Ghosts are her hobby."})

elseif(sTopicName == "Stacy") then
    fnStdObject(gciInv5A_Stacy, 
    {"[HIDEALLFAST][CHARENTER|Todd]Player: Stacy's in the surveillance room.",
     "Todd: Hope she doesn't meet her doom.",
     "Thought: [ITALICS]Dang it![P] I thought for sure that rhyme would stump him.[ENDITALICS]"})

elseif(sTopicName == "Michelle") then
    fnStdObject(gciInv5A_Michelle, 
    {"[HIDEALLFAST][CHARENTER|Todd]Todd: [E|N]Michelle's on the second floor.",
     "Player: She's...[P] really into blood and gore.",
     "Todd: [E|Happy]Nice, nice."})

elseif(sTopicName == "Laura") then
    fnStdObject(gciInv5A_Laura, 
    {"[HIDEALLFAST][CHARENTER|Todd]Player: Laura's over there.",
     "Todd: [E|Mad]With a... faraway stare.[P][E|Happy] Hah!",
     "Laura: [E|Shock]wh-what?"})

elseif(sTopicName == "Todd") then
    fnStdObject(gciInv5A_Todd, 
    {"[HIDEALLFAST][CHARENTER|Todd]Player: Okay, stop rhyming now,[P] I mean it!",
     "Todd: [P][E|Happy]... Does anybody want a peanut?",
     "Thought: [ITALICS]That doesn't even make sense!!![ENDITALICS]"})

-- |[ ======================================= Main Room ======================================== ]|
elseif(sTopicName == "ReceptionDesk") then
    fnStdObject(gciInv5A_Reception, 
    {"Thought: No dialogue."})

elseif(sTopicName == "GeorgeFosterPortrait") then
    fnStdObject(gciInv5A_GeorgeFosterPortrait, 
    {"Thought: No dialogue."})

elseif(sTopicName == "BiminiRecording") then
    fnStdObject(gciInv5A_BiminiRecording, 
    {"Thought: No dialogue."})

elseif(sTopicName == "Mural") then
    fnStdObject(gciInv5A_Mural, 
    {"Thought: No dialogue."})

-- |[ ======================================= Twin Room ======================================== ]|
elseif(sTopicName == "TankMaps") then
    fnStdObject(gciInv5A_TankMaps, 
    {"Thought: No dialogue."})

elseif(sTopicName == "PhotoPlayer") then
    fnStdObject(gciInv5A_PhotoPlayer, 
    {"Thought: No dialogue."})

elseif(sTopicName == "Photo34") then
    fnStdObject(gciInv5A_Photo34, 
    {"Thought: No dialogue."})

elseif(sTopicName == "Photo56") then
    fnStdObject(gciInv5A_Photo56, 
    {"Thought: No dialogue."})

elseif(sTopicName == "Photo78") then
    fnStdObject(gciInv5A_Photo78, 
    {"Thought: No dialogue."})

elseif(sTopicName == "Photo910") then
    fnStdObject(gciInv5A_Photo910, 
    {"Thought: No dialogue."})

elseif(sTopicName == "DoubleStorageTubes") then
    fnStdObject(gciInv5A_DoubleStorageTubes, 
    {"Thought: No dialogue."})

elseif(sTopicName == "KaneKola") then
    fnStdObject(gciInv5A_KaneKola, 
    {"Thought: No dialogue."})

elseif(sTopicName == "DoubleSecureTable") then
    fnStdObject(gciInv5A_DoubleSecureTable, 
    {"Thought: No dialogue."})

elseif(sTopicName == "HeavyMachinery") then
    fnStdObject(gciInv5A_HeavyMachinery, 
    {"Thought: No dialogue."})

-- |[ ==================================== Agent's Quarters ==================================== ]|
elseif(sTopicName == "Paperwork") then
    fnStdObject(gciInv5A_Paperwork, 
    {"Thought: No dialogue."})

elseif(sTopicName == "Personnel") then
    fnStdObject(gciInv5A_Personnel, 
    {"Thought: No dialogue."})

elseif(sTopicName == "EiletteFile") then
    fnStdObject(gciInv5A_EiletteFile, 
    {"Thought: No dialogue."})

elseif(sTopicName == "CandaceFoster") then
    fnStdObject(gciInv5A_CandaceFoster, 
    {"Thought: No dialogue."})

elseif(sTopicName == "CoffeeCup") then
    fnStdObject(gciInv5A_CoffeeCup, 
    {"Thought: No dialogue."})

elseif(sTopicName == "FrogSociety") then
    fnStdObject(gciInv5A_FrogSociety, 
    {"Thought: No dialogue."})

-- |[ ====================================== Quartz Room ======================================= ]|
elseif(sTopicName == "Notes") then
    fnStdObject(gciInv5A_Notes, 
    {"Thought: No dialogue."})

elseif(sTopicName == "PhotoCrater") then
    fnStdObject(gciInv5A_PhotoCrater, 
    {"Thought: No dialogue."})

elseif(sTopicName == "Chamber") then
    fnStdObject(gciInv5A_Chamber, 
    {"Thought: No dialogue."})

elseif(sTopicName == "KanePhoto") then
    fnStdObject(gciInv5A_KanePhoto, 
    {"Thought: No dialogue."})

elseif(sTopicName == "JaniceCandacePhoto") then
    fnStdObject(gciInv5A_JaniceCandacePhoto, 
    {"Thought: No dialogue."})

elseif(sTopicName == "BrokenStorageTube") then
    fnStdObject(gciInv5A_BrokenStorageTube, 
    {"Thought: No dialogue."})

elseif(sTopicName == "StorageTube") then
    fnStdObject(gciInv5A_StorageTube, 
    {"Thought: No dialogue."})

-- |[ ======================================== Storage ========================================= ]|
elseif(sTopicName == "OldSodaRecording") then
    fnStdObject(gciInv5A_OldSodaRecording, 
    {"[HIDEALLFAST][CHARENTER|Todd][CHARENTER|Laura]Todd: [E|Mad]Wow, this recording gives me [ITALICS]MAD[ENDITALICS] Fallout vibes.",
     "Laura: [E|Happy]I know, right?[E|Blush] I-I was just going to say that.[P][E|Shock] Oh--[PLAYERNAME], that's a video game by the way.",
     "Player: Cool.[P] Can I play it sometime?",
     "Laura: [E|Sad]Mmm, I would probably start you off on something easier.[P][E|Mad] *hrm* Something like...[P] like...",
     "Todd: [E|Mad]What's a good video game for someone who's never used a computer before?",
     "Thought: Laura and Todd meet eyes.",
     "Both: [FOCUS|Todd][FOCUSMOD|Laura|true][EMOTION|Todd|Shock][EMOTION|Laura|Shock]'Putt Putt Saves the Zoo'!"})

elseif(sTopicName == "ComputerParts") then
    fnStdObject(gciInv5A_ComputerParts, 
    {"[HIDEALLFAST][CHARENTER|Todd]Todd: [E|N]I kiiiinda want to plug it in, just to see what would happen.",
     "Player: You? Goody-two shoes, class president Todd?",
     "Todd: [E|HAppy]YES. [P]The spirit of rebellion courses though my veins once more!",
     "Player: Was it a bad idea to bring here? I feel like I've created a monster."})

elseif(sTopicName == "PhotoKaneAdam") then
    fnStdObject(gciInv5A_PhotoKaneAdam, 
    {"[HIDEALLFAST][CHARENTER|Todd]Todd: [E|N]Hah.[P] I love how in the 80s everyone thought technology was basically magic.",
     "[E|Mad]I wonder what ADAM could actually do--[P]maybe write a canned response in a terminal or something."})

elseif(sTopicName == "MessyFileCabinet") then
    fnStdObject(gciInv5A_MessyFileCabinet, 
    {"[HIDEALLFAST][CHARENTER|Todd]Todd: [E|Mad]Just a bunch of code...[E|Happy]Ah, haha.[P] These notes are hilarious.",
     "Player: ???",
     "Todd: [E|Happy]'Hey Rico, It's Teddy again.[P] this thing is DEFINITELY haunted! I'm freakin' telling you, man...'",
     "Todd: '... The office is always tits cold when I get in, The lights are flickerin' on and off all the time, and the printer sends me dumb jokes!'",
     "Todd: '... I'm quittin' unless you unplug it.[P] QUITTING, you hear me?'"})

elseif(sTopicName == "DirtyMattress") then
    fnStdObject(gciInv5A_DirtyMattress, 
    {"[HIDEALLFAST][CHARENTER|Todd]Todd: [E|Mad]Oh man. That thing looks like bedbug city.[P] Give it clearance of at LEAST six feet! "})

elseif(sTopicName == "Grafitti") then
    fnStdObject(gciInv5A_Grafitti, 
    {"[HIDEALLFAST][CHARENTER|Todd]Todd: [E|Sad]It seems someone was doodling it up in here. Uhhh...[P]and they weren't too happy about it?"})

end