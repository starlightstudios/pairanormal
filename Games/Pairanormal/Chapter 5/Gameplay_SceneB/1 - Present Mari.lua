-- |[ =================================== Mari Presentation ==================================== ]|
--Showing stuff to Mari.

-- |[Arg Resolve]|
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variable Setup]|
--Used to simplify the Add Discovered Object calls.
local sBGPath = "Root/Images/Backgrounds/All/MineLobby"
local sScriptPath = gsRoot .. "../Chapter 5/Gameplay_SceneB/0 - ExamineObject.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
local sAutoAdvance = gsRoot .. "../Chapter 5/Gameplay_SceneC/7 - PatchGoingHome.lua"

--Saving
gsSaveExecute = gsRoot .. "../Chapter 5/Gameplay_SceneA/3 - Quartz.lua"
gsSaveTopic = "No Topic"
gsSaveFlag = "InvestigationMine"

-- |[ ========================================= Functions ========================================= ]|
-- |[Storage Handler]|
--Stores the object as discovered.
local fnStdStore = function(i)
    
    --Get if we've examined the object before. This will be returned later.
	local iExaminedAlready = VM_GetVar("Root/Variables/Chapter5/InvestigationA/iObject|" .. gczaInv5A_List[i], "N")
    
    --Save it as a discovered object.
	Dia_SetProperty("Add Discovered Object", gczaInv5A_List[i], sBGPath, sScriptPath, gczaInv5A_List[i])
	VM_SetVar("Root/Variables/Chapter5/InvestigationA/iObject|" .. gczaInv5A_List[i], "N", 1.0)
    
    --Pass back.
    return iExaminedAlready
end

-- |[Friendship Increment]|
--If this is the first time presenting this object to this character then we increase the friendship tracker for
-- that character.
local fnIncrementFriendship = function(iObjectIndex, iIncrement)
	--Does nothing in this chapter.
end

-- |[Time Handler]|
--Decrements time. Also saves the game.
local fnStdTime = function()
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(gsSaveFlag, gsSaveExecute, gsSaveTopic)
    end
end

-- |[Object Handler]|
--Standard handler for objects. Saves the object, prints a dialogue, advances time.
local fnStdObject = function(iIndex, saStrings)
	
    --Argument check.
    if(iIndex == nil) then return end
    
	--Store it in the inventory.
	local i = iIndex
	local iExaminedAlready = fnStdStore(i)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
    
    --Append.
    for i = 1, #saStrings, 1 do
		fnDialogue(saStrings[i])
    end
    
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
    if(iExaminedAlready == 0.0) then fnStdTime(sSaveFlag, sSaveExecute, sSaveTopic) end
end

-- |[ ======================================= Chat Sequences ====================================== ]|
--Chatting.
if(sTopicName == "Chat") then
	
	--Variables.
	local iChattedWithMari = VM_GetVar("Root/Variables/Chapter5/Investigation/iChattedWithMari", "N")
	
	--First pass:
	if(iChattedWithMari == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Investigation/iChattedWithMari", "N", 1.0)
		
		--Text.
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
        
		fnDialogue([[[HIDEALLFAST][CHARENTER|Mari]Mari: [E|N]I'm gonna hang back here, since someone needs to coordinate the team.[CRASH][E|Mad] BUT BE ASSURED, I'M INVESTIGATING EVERY INCH OF THIS LOBBY!!!]])
		fnDialogue([[Player: Thanks.]])
		fnDialogue([[Thought: Seems like nobody's in the mood for talking this time. I should focus on the investigation.]])

        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
	
        --Time loss.
        if(fnDecrementTime(1, sAutoAdvance) == false) then
            PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
        end
		
	--Repeat passes.
	else
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()
        
		fnDialogue([[[HIDEALLFAST][CHARENTER|Mari]Mari: [E|N]I'm gonna hang back here, since someone needs to coordinate the team.[CRASH][E|Mad] BUT BE ASSURED, I'M INVESTIGATING EVERY INCH OF THIS LOBBY!!!]])
		fnDialogue([[Player: Thanks.]])
		fnDialogue([[Thought: Seems like nobody's in the mood for talking this time. I should focus on the investigation.]])
        
        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
	end
	
-- |[ ======================================= Characters ======================================= ]|
elseif(sTopicName == "Bret") then
    fnStdObject(gciInv5A_Bret, 
    {"[HIDEALLFAST][SHOWCHAR|Mari]Mari: [E|Shock]Where did Bret go?[P] Is he okay!?!?", 
     "Player: I'm sure he's fine.",
     "Mari: [E|N]Go check on him when you get the chance. I'd appreciate it a lot!"})

elseif(sTopicName == "Mari") then
    fnStdObject(gciInv5A_Mari, 
    {"Mari: [E|N]Hmm hmm hmm~", 
     "Player: [CRASH]MARI! WHAT ARE YOU DOING WITH THAT LIGHTER!?!?",
     "Mari: [E|Happy]J-[P]just lighting a little bit of sage.[P] There's lots of bad vibes in here![E|Sad] What, do you think I shouldn't?"})

elseif(sTopicName == "Stacy") then
    fnStdObject(gciInv5A_Stacy, 
    {"Mari: [E|N]If you get into any trouble, make sure to use a secret bird call! Stacy and I will leap into action.",
     "Player: What's the bird call?",
     "Mari: [E|N]It's like... [P][E|Blush]well, I can't just do it right now!",
     "Mari: [E|N]Just make one up. I'm sure we'll know it's you."})

elseif(sTopicName == "Michelle") then
    fnStdObject(gciInv5A_Michelle, 
    {"Mari:[E|N] Make sure to check in on Michelle.[P] She's tough, but I bet she'd appreciate the company."})

elseif(sTopicName == "Laura") then
    fnStdObject(gciInv5A_Laura, 
    {"Mari: [E|N]I'm glad Laura came along.[P][E|Shock] [ITALICS]With our powers combined, our team is unstoppable[ENDITALICS]."})

elseif(sTopicName == "Todd") then
    fnStdObject(gciInv5A_Todd, 
    {"Mari: [E|N]So...[P]Todd tagged along for the adventure.[P] Is this going to be...[P][E|Blush]a [ITALICS]regular[ENDITALICS] thing between the two of you?",
     "Player: I don't know.[P] He's so busy with all his other activities.",
     "Mari: [E|Blush]Oh, I dunno, hee heeee~.[P][E|Happy] I'm sure he could make a little bit of time now and then.",
     "Player: What?[P] What's with that weird smile on your face?",
     "Mari: [E|Happy][CRASH]HEE HEEEE",
     "Player: [SFX|Ugh]Stop it! You're creeping me out.",
     "Mari: [E|N]... [P][P][E|Happy]heeeeee."})

-- |[ ======================================= Main Room ======================================== ]|
elseif(sTopicName == "ReceptionDesk") then
    fnStdObject(gciInv5A_Reception, 
    {"Mari: [E|N]Step right up! Make sure to sign your name in our guest register and qualify for a free sticker:",
     "Mari: 'I WENT TO THIS CREEPY QUARTZ MINE AND ALL I GOT WAS PERMANENT PSYCHOLOGICAL TRAUMA[P][E|Happy] (and a free sticker)'"})

elseif(sTopicName == "GeorgeFosterPortrait") then
    fnStdObject(gciInv5A_GeorgeFosterPortrait, 
    {"Mari: [E|Sad]The plaque underneath says 'In honor of George Foster Jr. -- Your spirit lives on.'[P] This must've been Kane Foster's brother."})

elseif(sTopicName == "BiminiRecording") then
    fnStdObject(gciInv5A_BiminiRecording, 
    {"Mari: [E|N]Bimini, Bimini... what a weird name for a company.[P][E|Shock] Is it short for something?",
     "Player: Hmmm...",
     "Mari: [E|N]Bitter Intellectual Men Interested in Naughty Inches.",
     "Player: You came up with that [ITALICS]really[ENDITALICS] fast."})

elseif(sTopicName == "Mural") then
    fnStdObject(gciInv5A_Mural, 
    {"Mari: [E|Shock]'who would you be with someone else's memories'[P][E|Sad]... Oof.[SFX|UGh] That's awfully existential for a reception area.",
     "Player: It doesn't make any sense to me.[P] Then again, I don't have many memories at all.",
     "Mari: [E|Sad]OOF.[P] Ooooooof."})

-- |[ ======================================= Twin Room ======================================== ]|
elseif(sTopicName == "TankMaps") then
    fnStdObject(gciInv5A_TankMaps, 
    {"Thought: No dialogue."})

elseif(sTopicName == "PhotoPlayer") then
    fnStdObject(gciInv5A_PhotoPlayer, 
    {"Thought: No dialogue."})

elseif(sTopicName == "Photo34") then
    fnStdObject(gciInv5A_Photo34, 
    {"Thought: No dialogue."})

elseif(sTopicName == "Photo56") then
    fnStdObject(gciInv5A_Photo56, 
    {"Thought: No dialogue."})

elseif(sTopicName == "Photo78") then
    fnStdObject(gciInv5A_Photo78, 
    {"Thought: No dialogue."})

elseif(sTopicName == "Photo910") then
    fnStdObject(gciInv5A_Photo910, 
    {"Thought: No dialogue."})

elseif(sTopicName == "DoubleStorageTubes") then
    fnStdObject(gciInv5A_DoubleStorageTubes, 
    {"Thought: No dialogue."})

elseif(sTopicName == "KaneKola") then
    fnStdObject(gciInv5A_KaneKola, 
    {"Thought: No dialogue."})

elseif(sTopicName == "DoubleSecureTable") then
    fnStdObject(gciInv5A_DoubleSecureTable, 
    {"Thought: No dialogue."})

elseif(sTopicName == "HeavyMachinery") then
    fnStdObject(gciInv5A_HeavyMachinery, 
    {"Thought: No dialogue."})

-- |[ ==================================== Agent's Quarters ==================================== ]|
elseif(sTopicName == "Paperwork") then
    fnStdObject(gciInv5A_Paperwork, 
    {"Thought: No dialogue."})

elseif(sTopicName == "Personnel") then
    fnStdObject(gciInv5A_Personnel, 
    {"Thought: No dialogue."})

elseif(sTopicName == "EiletteFile") then
    fnStdObject(gciInv5A_EiletteFile, 
    {"Thought: No dialogue."})

elseif(sTopicName == "CandaceFoster") then
    fnStdObject(gciInv5A_CandaceFoster, 
    {"Thought: No dialogue."})

elseif(sTopicName == "CoffeeCup") then
    fnStdObject(gciInv5A_CoffeeCup, 
    {"Thought: No dialogue."})

elseif(sTopicName == "FrogSociety") then
    fnStdObject(gciInv5A_FrogSociety, 
    {"Thought: No dialogue."})

-- |[ ====================================== Quartz Room ======================================= ]|
elseif(sTopicName == "Notes") then
    fnStdObject(gciInv5A_Notes, 
    {"Thought: No dialogue."})

elseif(sTopicName == "PhotoCrater") then
    fnStdObject(gciInv5A_PhotoCrater, 
    {"Thought: No dialogue."})

elseif(sTopicName == "Chamber") then
    fnStdObject(gciInv5A_Chamber, 
    {"Thought: No dialogue."})

elseif(sTopicName == "KanePhoto") then
    fnStdObject(gciInv5A_KanePhoto, 
    {"Thought: No dialogue."})

elseif(sTopicName == "JaniceCandacePhoto") then
    fnStdObject(gciInv5A_JaniceCandacePhoto, 
    {"Thought: No dialogue."})

elseif(sTopicName == "BrokenStorageTube") then
    fnStdObject(gciInv5A_BrokenStorageTube, 
    {"Thought: No dialogue."})

elseif(sTopicName == "StorageTube") then
    fnStdObject(gciInv5A_StorageTube, 
    {"Thought: No dialogue."})

-- |[ ======================================== Storage ========================================= ]|
elseif(sTopicName == "OldSodaRecording") then
    fnStdObject(gciInv5A_OldSodaRecording, 
    {"Thought: No dialogue."})

elseif(sTopicName == "ComputerParts") then
    fnStdObject(gciInv5A_ComputerParts, 
    {"Thought: No dialogue."})

elseif(sTopicName == "PhotoKaneAdam") then
    fnStdObject(gciInv5A_PhotoKaneAdam, 
    {"Thought: No dialogue."})

elseif(sTopicName == "MessyFileCabinet") then
    fnStdObject(gciInv5A_MessyFileCabinet, 
    {"Thought: No dialogue."})

elseif(sTopicName == "DirtyMattress") then
    fnStdObject(gciInv5A_DirtyMattress, 
    {"Thought: No dialogue."})

elseif(sTopicName == "Grafitti") then
    fnStdObject(gciInv5A_Grafitti, 
    {"Thought: No dialogue."})

end