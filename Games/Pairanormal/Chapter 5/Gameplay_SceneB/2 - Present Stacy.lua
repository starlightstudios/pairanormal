-- |[ =================================== Stacy Presentation =================================== ]|
--Showing stuff to Stacy.

-- |[Arg Resolve]|
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

-- |[Variable Setup]|
--Used to simplify the Add Discovered Object calls.
local sBGPath = "Root/Images/Backgrounds/All/MineLobby"
local sScriptPath = gsRoot .. "../Chapter 5/Gameplay_SceneB/0 - ExamineObject.lua"
local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
local sAutoAdvance = gsRoot .. "../Chapter 5/Gameplay_SceneC/7 - PatchGoingHome.lua"

--Saving
gsSaveExecute = gsRoot .. "../Chapter 5/Gameplay_SceneA/3 - Quartz.lua"
gsSaveTopic = "No Topic"
gsSaveFlag = "InvestigationMine"

-- |[ ========================================= Functions ========================================= ]|
-- |[Storage Handler]|
--Stores the object as discovered.
local fnStdStore = function(i)
    
    --Get if we've examined the object before. This will be returned later.
	local iExaminedAlready = VM_GetVar("Root/Variables/Chapter5/InvestigationA/iObject|" .. gczaInv5A_List[i], "N")
    
    --Save it as a discovered object.
	Dia_SetProperty("Add Discovered Object", gczaInv5A_List[i], sBGPath, sScriptPath, gczaInv5A_List[i])
	VM_SetVar("Root/Variables/Chapter5/InvestigationA/iObject|" .. gczaInv5A_List[i], "N", 1.0)
    
    --Pass back.
    return iExaminedAlready
end

-- |[Friendship Increment]|
--If this is the first time presenting this object to this character then we increase the friendship tracker for
-- that character.
local fnIncrementFriendship = function(iObjectIndex, iIncrement)
	--Does nothing in this chapter.
end

-- |[Time Handler]|
--Decrements time. Also saves the game.
local fnStdTime = function()
    if(fnDecrementTime(1, sAutoAdvance) == false) then
        PairanormalLevel_SaveCheckpoint(gsSaveFlag, gsSaveExecute, gsSaveTopic)
    end
end

-- |[Object Handler]|
--Standard handler for objects. Saves the object, prints a dialogue, advances time.
local fnStdObject = function(iIndex, saStrings)
	
    --Argument check.
    if(iIndex == nil) then return end
    
	--Store it in the inventory.
	local i = iIndex
	local iExaminedAlready = fnStdStore(i)
	
	--Dialogue.
	fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
	fnCutsceneBlocker()
    
    --Append.
    for i = 1, #saStrings, 1 do
		fnDialogue(saStrings[i])
    end
    
	fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
	fnCutsceneBlocker()
	
	--Time loss.
    if(iExaminedAlready == 0.0) then fnStdTime(sSaveFlag, sSaveExecute, sSaveTopic) end
end

-- |[ ======================================= Chat Sequences ====================================== ]|
--Chatting.
if(sTopicName == "Chat") then
	
	--Variables.
	local iChattedWithStacy = VM_GetVar("Root/Variables/Chapter5/Investigation/iChattedWithStacy", "N")
	
	--First pass:
	if(iChattedWithStacy == 0.0) then
		
		--Flag.
		VM_SetVar("Root/Variables/Chapter5/Investigation/iChattedWithStacy", "N", 1.0)
		
		--Text.
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()

		fnDialogue([[[HIDEALLFAST][CHARENTER|Stacy]Stacy: [E|Blush][ITALICS]Bonjour[ENDITALICS]. Let me know if anything strikes your fancy!]])
		fnDialogue([[Player: In this place?[P] I don't think so.]])
		fnDialogue([[Stacy: [E|Sad][P][P]... Right.]])
		fnDialogue([[Thought: [ITALICS]Can't flirt now, Stacy[ENDITALICS].[P] I should focus on the investigation.]])

        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
    
        --Variable gain.
        local iMariFriend  = VM_GetVar("Root/Variables/System/Romance/iMariFriend", "N")
        VM_SetVar("Root/Variables/System/Romance/iMariFriend", "N", iMariFriend + 5.0)
	
        --Time loss.
        if(fnDecrementTime(5, sAutoAdvance) == false) then
            PairanormalLevel_SaveCheckpoint(sSaveFlag, sSaveExecute, sSaveTopic)
        end
		
	--Repeat passes.
	else
        fnCutsceneInstruction([[ Dia_SetProperty("Stop Investigation") ]])
        fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])
        fnCutsceneBlocker()

		fnDialogue([[[HIDEALLFAST][CHARENTER|Stacy]Stacy: [E|Blush][ITALICS]Bonjour[ENDITALICS]. Let me know if anything strikes your fancy!]])
		fnDialogue([[Player: In this place?[P] I don't think so.]])
		fnDialogue([[Stacy: [E|Sad][P][P]... Right.]])
		fnDialogue([[Thought: [ITALICS]Can't flirt now, Stacy[ENDITALICS].[P] I should focus on the investigation.]])
        
        fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
        fnCutsceneBlocker()
	end
	
-- |[ ======================================= Characters ======================================= ]|
elseif(sTopicName == "Bret") then
    fnStdObject(gciInv5A_Bret, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Stacy: [E|Sad]Mmh, I really wish Bret was in here.[P] I'd bet he'd know lots of stuff about what's in this place.",
     "Player: We'll just have to figure it out together."})

elseif(sTopicName == "Mari") then
    fnStdObject(gciInv5A_Mari, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Stacy: [E|N]Mari doesn't realize she's being watched by me right now... [P][E|Happy][CRASH]MWAHAHAHAHA. "})

elseif(sTopicName == "Stacy") then
    fnStdObject(gciInv5A_Stacy, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Stacy:  [E|Shock]Can you believe all this stuff in here? I don't even know where to start."})

elseif(sTopicName == "Michelle") then
    fnStdObject(gciInv5A_Michelle, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Player: Michelle's somewhere upstairs.",
     "Stacy: [E|Shock][ITALICS]C'est vrai?[ENDITALICS] Well if she isn't around,[P][E|Blush] I'll have to be the one to protect you."})

elseif(sTopicName == "Laura") then
    fnStdObject(gciInv5A_Laura, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Stacy: [E|Sad]I really wish Laura was in here.[P] Maybe she'd be able to help me figure out all this tech-y stuff."})

elseif(sTopicName == "Todd") then
    fnStdObject(gciInv5A_Todd, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Stacy: [E|N]So... the tutor came along, did he?[P] Wanted to get some scary alone adventure time with you, did he?",
     "Player: I guess.[P] You make it seem like that's bad.",
     "Stacy: [E|N]Not at all.",
     "Stacy: [E|Mad]Don't worry, [PLAYERNAME]. If he tries anything suss...[P]I'll handle it with a quick kick to the groin."})

-- |[ ======================================= Main Room ======================================== ]|
elseif(sTopicName == "ReceptionDesk") then
    fnStdObject(gciInv5A_Reception, 
    {"Thought: No dialogue."})

elseif(sTopicName == "GeorgeFosterPortrait") then
    fnStdObject(gciInv5A_GeorgeFosterPortrait, 
    {"Thought: No dialogue."})

elseif(sTopicName == "BiminiRecording") then
    fnStdObject(gciInv5A_BiminiRecording, 
    {"Thought: No dialogue."})

elseif(sTopicName == "Mural") then
    fnStdObject(gciInv5A_Mural, 
    {"Thought: No dialogue."})

-- |[ ======================================= Twin Room ======================================== ]|
elseif(sTopicName == "TankMaps") then
    fnStdObject(gciInv5A_TankMaps, 
    {"Thought: No dialogue."})

elseif(sTopicName == "PhotoPlayer") then
    fnStdObject(gciInv5A_PhotoPlayer, 
    {"Thought: No dialogue."})

elseif(sTopicName == "Photo34") then
    fnStdObject(gciInv5A_Photo34, 
    {"Thought: No dialogue."})

elseif(sTopicName == "Photo56") then
    fnStdObject(gciInv5A_Photo56, 
    {"Thought: No dialogue."})

elseif(sTopicName == "Photo78") then
    fnStdObject(gciInv5A_Photo78, 
    {"Thought: No dialogue."})

elseif(sTopicName == "Photo910") then
    fnStdObject(gciInv5A_Photo910, 
    {"Thought: No dialogue."})

elseif(sTopicName == "DoubleStorageTubes") then
    fnStdObject(gciInv5A_DoubleStorageTubes, 
    {"Thought: No dialogue."})

elseif(sTopicName == "KaneKola") then
    fnStdObject(gciInv5A_KaneKola, 
    {"Thought: No dialogue."})

elseif(sTopicName == "DoubleSecureTable") then
    fnStdObject(gciInv5A_DoubleSecureTable, 
    {"Thought: No dialogue."})

elseif(sTopicName == "HeavyMachinery") then
    fnStdObject(gciInv5A_HeavyMachinery, 
    {"Thought: No dialogue."})

-- |[ ==================================== Agent's Quarters ==================================== ]|
elseif(sTopicName == "Paperwork") then
    fnStdObject(gciInv5A_Paperwork, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Stacy: [E|N]I thought filling out school forms online was a nightmare, but [ITALICS]this[ENDITALICS]? I'd go out of my mind.",
     "Player: Anything interesting in here?",
     "Stacy: [E|Sad]I'm seeing a lot of expense reports... and this one is a bulleted list of someone's 'movements'.",
     "Player: !!!",
     "Thought: She holds a sheet up up to the light.",
     "Stacy: [E|Shock]March 12th, 1991:: 7::08 PM 'Erstwhile' returns from Pine Cineplex with 'Sparrow' in tow. [P]Both appeared to have had a good time.",
     "Stacy: 7::10, 'Erstwhile' Rounding maple street. Intentions seem clear to the observer.",
     "Stacy: Note:: Activate auditory bug in 'Sparrow''s house? Note:: 'Louhi' and 'Lilith' nowhere in sight."})

elseif(sTopicName == "Personnel") then
    fnStdObject(gciInv5A_Personnel, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Stacy: [E|Shock]I don't recognize any of these [ITALICS]nommes[ENDITALICS].[P][E|N] Of course, I'm pretty sure they're made up.",
     "Stacy: [E|Mad] Spy names usually are."})

elseif(sTopicName == "EiletteFile") then
    fnStdObject(gciInv5A_EiletteFile, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Player: I need to know what this says!",
     "Stacy: [E|Mad]'Compromised.'[P] In spy movies and stuff, 'compromised' usually means something went very, very wrong.",
     "Thought: Stacy thumbs through the notes attached around the portrait.",
     "Stacy: [E|Shock]Hey, doesn't this woman look kind of familia--",
     "Player: [CRASH]H-hey, I bet you can't do 100 situps.[P][CRASH] I tried to do just 20 in gym class the other day, and it was way too hard!", 
     "Stacy: [E|Happy]You kidding me? 100 situps is a piece of cake.", 
     "Stacy: [E|Mad] Check this out--1, 2...", 
     "Thought: [ITALICS]Whew! I think I distracted her.[ENDITALICS]"})

elseif(sTopicName == "CandaceFoster") then
    fnStdObject(gciInv5A_CandaceFoster, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Stacy: [E|Mad]I [ITALICS]knew[ENDITALICS] it. Look there! Miss Foster MUST be evil![P] They were tracking her movements.",
     "Player: I kinda figured that out when she tried to kidnap me.",
     "Stacy: [E|Shock]YES.[P][E|Sad] Also that."})

elseif(sTopicName == "CoffeeCup") then
    fnStdObject(gciInv5A_CoffeeCup, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Stacy: [E|Happy]Check it out. I got eyes [ITALICS]all over[ENDITALICS] this facility.[P] And a few other weird random corners of town--",
     "Thought: Stacy's eyes widen, and I'm grabbed by her long, tense fingers.",
     "Stacy: [E|Shock]No.[P] Way.[P][P][CRASH] LOOK! ",
     "Thought: She taps one of the images just before it manages to switch over.[P] I recognize it instantly.",
     "Player: The clubhouse. ",
     "Thought: I really hope nobody was around by the time the paranormal club made their home here."})

elseif(sTopicName == "FrogSociety") then
    fnStdObject(gciInv5A_FrogSociety, 
    {"[HIDEALLFAST][CHARENTER|Stacy]Player: Do you happen to know who that woman at the top is?",
     "Stacy: [E|Sad]Not a clue[P]--but based on that X on her face, I get the feeling we won't meet her any time soon."})

-- |[ ====================================== Quartz Room ======================================= ]|
elseif(sTopicName == "Notes") then
    fnStdObject(gciInv5A_Notes, 
    {"Thought: No dialogue."})

elseif(sTopicName == "PhotoCrater") then
    fnStdObject(gciInv5A_PhotoCrater, 
    {"Thought: No dialogue."})

elseif(sTopicName == "Chamber") then
    fnStdObject(gciInv5A_Chamber, 
    {"Thought: No dialogue."})

elseif(sTopicName == "KanePhoto") then
    fnStdObject(gciInv5A_KanePhoto, 
    {"Thought: No dialogue."})

elseif(sTopicName == "JaniceCandacePhoto") then
    fnStdObject(gciInv5A_JaniceCandacePhoto, 
    {"Thought: No dialogue."})

elseif(sTopicName == "BrokenStorageTube") then
    fnStdObject(gciInv5A_BrokenStorageTube, 
    {"Thought: No dialogue."})

elseif(sTopicName == "StorageTube") then
    fnStdObject(gciInv5A_StorageTube, 
    {"Thought: No dialogue."})

-- |[ ======================================== Storage ========================================= ]|
elseif(sTopicName == "OldSodaRecording") then
    fnStdObject(gciInv5A_OldSodaRecording, 
    {"Thought: No dialogue."})

elseif(sTopicName == "ComputerParts") then
    fnStdObject(gciInv5A_ComputerParts, 
    {"Thought: No dialogue."})

elseif(sTopicName == "PhotoKaneAdam") then
    fnStdObject(gciInv5A_PhotoKaneAdam, 
    {"Thought: No dialogue."})

elseif(sTopicName == "MessyFileCabinet") then
    fnStdObject(gciInv5A_MessyFileCabinet, 
    {"Thought: No dialogue."})

elseif(sTopicName == "DirtyMattress") then
    fnStdObject(gciInv5A_DirtyMattress, 
    {"Thought: No dialogue."})

elseif(sTopicName == "Grafitti") then
    fnStdObject(gciInv5A_Grafitti, 
    {"Thought: No dialogue."})

end