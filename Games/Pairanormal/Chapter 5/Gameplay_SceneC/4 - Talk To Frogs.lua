--[Exec First]
--Frogs.

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--[Execution]
--Start.
if(sTopicName == "Six") then
    VM_SetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToSix", "N", 1.0)
    fnDialogue("[HIDEALL]Player: Six!")
    fnDialogue("Thought: It's hard not to find Six and her other in a crowd[P]--I just have to follow the uncomfortable gazes of onlookers.")
    fnDialogue("Thought: Catching her attention, she pulls up to me in that handy motorized chair.")
    fnDialogue("Player: You're here!")
    fnDialogue("[CHARENTER|Six]Six: [E|Happy]Wouldn't miss this *hiss* for all the fruitcakes in the world.")
    fnDialogue("Thought: Six usually stays in her bunker, or at least comes out at night.")
	fnDialogue("Thought: I used to think it was so she could avoid gawkers, but it turns out those are the only hours her favorite nightclub is open.")
    fnDialogue("Six: [E|Sad]What's with *huff*[P] everyone giving me weird looks.[P] *hiss* [P]You'd think they've never seen...")
    fnDialogue("Six: ... A beautiful girl *huff* [P]in 6-inch heels.[P][E|Happy] EHYEEEH HYEEE HEEEEEH.")
    fnDialogue("Thought: Her smile is infectious.[P] Something about Six's warmth and skill with a computer always reminded me of Laura.")
	fnDialogue("Thought: I push back the tiniest of pangs. Laura and I haven't seen each other for a while... but I hope she's doing well.")
    fnDialogue("Player: So... [P]I'm going to be an official member of the Frog Society.")
    fnDialogue("Six: [E|N]Oh no. [P]*huff*[P] Who invited you?")
    fnDialogue("Player: I'm serious![P] Do you have any... tips?")
    fnDialogue("Six: She rolls up to me, knocking my shins with her motorized chair.")
    fnDialogue("Player: [CRASH] Ouch![P][SFX|Ugh] Wh-what'd you do that for?")
    fnDialogue([[Six: [E|Sad]Because. *huff*[P] You said \"I'm serious\". [P]And my tip to you is *hisss*[P][SFX|Blink][E|Happy] Relax!]])
    fnDialogue("Thought: Her hand reaches out to touch my arm affectionately.")
    fnDialogue("Six: You're doing great. [P]*huff*[P] And you're not alone anymore.")
    fnDialogue("Player: ... Right.[P] Thanks, Six.")
    fnDialogue("Six: [E|Happy]See you around, grad. [P]*hiss*[P] Five says congrats!")
    fnDialogue("[HIDEALL]Thought: I can talk to someone else, too...")

    --Variables
    local iTalkedToThree = VM_GetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToThree", "N")
    local iTalkedToSix   = VM_GetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToSix", "N")
    local iTalkedToDog   = VM_GetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToDog", "N")
    local iTalkedToTwins = VM_GetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToTwins", "N")

    --Decision
    local sDecisionScript = "\"" .. gsRoot .. "../Chapter 5/Gameplay_SceneC/4 - Talk To Frogs.lua\""
    fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
    if(iTalkedToThree == 0.0) then
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Three\", " .. sDecisionScript .. ", \"Three\") ")
    end
    if(iTalkedToSix == 0.0) then
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Six\", " .. sDecisionScript .. ", \"Six\") ")
    end
    if(iTalkedToDog == 0.0) then
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Dog\", " .. sDecisionScript .. ", \"Dog\") ")
    end
    if(iTalkedToTwins == 0.0) then
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Twins\", " .. sDecisionScript .. ", \"Twins\") ")
    end
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Eilette (End Sequence)\", " .. sDecisionScript .. ", \"Eilette\") ")
    fnCutsceneBlocker()


elseif(sTopicName == "Three") then
    VM_SetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToThree", "N", 1.0)
    
    fnDialogue("[HIDEALL][CHARENTER|Three]Serena: [E|Blush]Well well well. The graduate.[P][E|N] How you doing, kid? ")
    fnDialogue("Player: Pretty great! Thanks for coming.")
    fnDialogue([[Thought: Serena shoves me small teddy bear with \"congrats graduate\" stitched onto it's tummy.[P] Why...[P][SFX|Ugh]why is it dirty?]])
    fnDialogue("Player: Uhhhh.[P] So.[P] How are things at the museum?")
    fnDialogue("Serena: [E|Blush]Business is booming, so naturally I've got my hands full.")
    fnDialogue("Serena: Bret has been a useful addition to our staff.")
    fnDialogue("Thought: As if on cue, Bret appears behind her, tightly clutching a clipboard.")
    fnDialogue("[CHARENTER|BretAdult][FOCUS|BretAdult]Bret: [EMOTION|BretAdult|Blush]Oh, gosh,[P] I mean, you're too kind Serena, I mean Miss Serena,[P] uh, I mean Miss Freeman,")
    fnDialogue("[FOCUS|BretAdult]Bret: [SPEEDTEXT][E|Sad] ah, all I'm saying is, like, I just do a [E|Mad]little simple clerical work here and there... it's [E|Blush]all thanks to your brilliant exhibition ideas, really--[ENDSPEEDTEXT] ")
    fnDialogue("Player: New exhibitions at the museum?[P] I never thought the manager would approve of something like that.")
    fnDialogue("Serena: [E|Blush]Well [ITALICS]I'm[ENDITALICS] the manager now, and I think my performance speaks for itself.")
    fnDialogue("[FOCUS|BretAdult]Bret: [EMOTION|BretAdult|Neutral]Yeah--[E|Happy]isn't it fortunate the previous curator decided to move to Tanzania?")
    fnDialogue("[FOCUS|BretAdult]Bret: [EMOTION|BretAdult|Shock]Although it was kinda weird how he disappeared without telling anyone and [EMOTION|BretAdult|Sad]only left behind a typewritten note,")
    fnDialogue("[FOCUS|BretAdult]Bret: [EMOTION|BretAdult|Mad]AND he left his cat behind,[P][EMOTION|BretAdult|Sad] but that's fine because I took it in, but still--")
    fnDialogue("Serena: [E|Shock]Bret, er, would you mind getting me some cold water?[P] I'm about ready to pass out in this heat.")
    fnDialogue("[FOCUS|BretAdult]Bret: [EMOTION|BretAdult|Shock]Oh, yeah, right![P] Eh...*ahem*.[P][EMOTION|BretAdult|Neutral] See you around, [PLAYERNAME].")
    fnDialogue("[CHAREXIT|BretAdult]Thought: I wave goodbye to him wistfully.")
    fnDialogue("Thought: I don't hang out often with the old Paranormal Club members. And, after today...[P]probably not ever.")
    fnDialogue("Thought: I try to push the disappointment out of my mind.")
    fnDialogue("Serena: [E|N]Hey.")
    fnDialogue("Player: Y-yeah?")
    fnDialogue("Serena: [E|Sad]What you did... what you're doing.[P] It ain't easy kid.")
    fnDialogue("Serena: You, uh.[P] You have a lot to be proud of, giving up stuff in the name of justice. I...[P]I owe you thanks.")
    fnDialogue("Thought: She pats me gently on the shoulder, her expression softening for the first time since I've ever talked to her.")
    fnDialogue("Serena: [E|N]I know us frogs can't replace the friends you've lost, but...[P]we [ITALICS]are[ENDITALICS] here for you, when you need us.")
    fnDialogue("Player: Thanks, Serena.")
    fnDialogue("Thought: She nods, and takes her leave.")
    fnDialogue("[HIDEALL]Thought: I can talk to someone else, too...")

    --Variables
    local iTalkedToThree = VM_GetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToThree", "N")
    local iTalkedToSix   = VM_GetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToSix", "N")
    local iTalkedToDog   = VM_GetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToDog", "N")
    local iTalkedToTwins = VM_GetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToTwins", "N")

    --Decision
    local sDecisionScript = "\"" .. gsRoot .. "../Chapter 5/Gameplay_SceneC/4 - Talk To Frogs.lua\""
    fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
    if(iTalkedToThree == 0.0) then
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Three\", " .. sDecisionScript .. ", \"Three\") ")
    end
    if(iTalkedToSix == 0.0) then
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Six\", " .. sDecisionScript .. ", \"Six\") ")
    end
    if(iTalkedToDog == 0.0) then
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Dog\", " .. sDecisionScript .. ", \"Dog\") ")
    end
    if(iTalkedToTwins == 0.0) then
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Twins\", " .. sDecisionScript .. ", \"Twins\") ")
    end
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Eilette (End Sequence)\", " .. sDecisionScript .. ", \"Eilette\") ")
    fnCutsceneBlocker()

elseif(sTopicName == "Dog") then
    VM_SetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToDog", "N", 1.0)
    
    fnDialogue("[CHARENTER|AJ]Dog: [E|Happy]Dog has being arrived, yes! Let the cebleration -  commennse.")
    fnDialogue("Thought: My furry friend leaps into my arms, and I spin him around playfully.")
    fnDialogue("Player: Dog![P] You came along for the ride!")
    fnDialogue("[CHARENTER|Eilette]Eilette: [E|Shock]Not intentionally.[P] It was a stowaway in the car.")
    fnDialogue("Player: I'm surprised a little animal like this was able to escape your notice, Eilette.")
    fnDialogue("Thought: She shifts her shoulders non-commitally.")
    fnDialogue("Eilette: [E|N]Perhaps he's been trained too well.")
    fnDialogue("Thought: Dog has lived with us for a long time now (we still haven't technically named him).")
    fnDialogue("Thought: I used to think Eilette couldn't stand him being around all the time...[P]until she started taking the afternoons off to do some training.")
    fnDialogue("Player: For the last time, I'm not letting you turn him into an animal operative.")
    fnDialogue("Eilette: [E|Mad]Then you refuse to see reason.[P] He would be the perfect undercover tool.")
    fnDialogue("Dog: [E|N]Dog is irrersistable, true.[P] Have a face of angel.[BR][E|Shock] Who could suspect!? I do a deceive.[P] ")
    fnDialogue("Dog: [E|N]Someday, dog have power to steal the meats.[P][E|Mad][CRASH] DIRECT FROM FRIDGE!")
    fnDialogue("Thought: I scratch his head and he melts, tail wagging madly.")
    fnDialogue("Player: He's way too easy to bribe to be effective.")
    fnDialogue("Dog: [E|N]I must go now. Manny things needing sniff here!!!")
    fnDialogue("Dog: [E|Blush]Deg also[P] everyone must pet me.")
    fnDialogue("Player: Okay, see you around.")
    fnDialogue("[HIDEALL]Thought: I can talk to someone else, too...")

    --Variables
    local iTalkedToThree = VM_GetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToThree", "N")
    local iTalkedToSix   = VM_GetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToSix", "N")
    local iTalkedToDog   = VM_GetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToDog", "N")
    local iTalkedToTwins = VM_GetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToTwins", "N")

    --Decision
    local sDecisionScript = "\"" .. gsRoot .. "../Chapter 5/Gameplay_SceneC/4 - Talk To Frogs.lua\""
    fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
    if(iTalkedToThree == 0.0) then
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Three\", " .. sDecisionScript .. ", \"Three\") ")
    end
    if(iTalkedToSix == 0.0) then
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Six\", " .. sDecisionScript .. ", \"Six\") ")
    end
    if(iTalkedToDog == 0.0) then
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Dog\", " .. sDecisionScript .. ", \"Dog\") ")
    end
    if(iTalkedToTwins == 0.0) then
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Twins\", " .. sDecisionScript .. ", \"Twins\") ")
    end
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Eilette (End Sequence)\", " .. sDecisionScript .. ", \"Eilette\") ")
    fnCutsceneBlocker()

elseif(sTopicName == "Twins") then
    VM_SetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToTwins", "N", 1.0)
    
    fnDialogue("[CHARENTER|Reich][CHARENTER|Leif]Rech: [E|N][PLAYERNAME]!")
    fnDialogue("Leif: [E|N]YOU DID IT!")
    fnDialogue("Twins: [FOCUS|Reich][FOCUSMOD|Leif|true][EMOTION|Reich|Happy][EMOTION|Leif|Happy][CRASH]You graduated!!!")
    fnDialogue("Leif: [E|N]It only took you five years to do it![P][P][E|Mad] Wait a minute, aren't high-schoolers supposed to graduate in four years?")
    fnDialogue("Rech: [E|Blush]Duh! It took [PLAYERNAME] longer because they[SFX|Ugh] didn't know anything at first, silly.")
    fnDialogue("Player: G-[P]give me a break![P] I learned everything a high schooler needed to know in 12 years.")
    fnDialogue("Player: If anything, that means I'm [ITALICS]extra smart.[ENDITALICS] ")
    fnDialogue("Twins: [FOCUS|Reich][FOCUSMOD|Leif|true][EMOTION|Reich|Shock][EMOTION|Leif|Shock]Whoah... you've got a point.")
    fnDialogue("Rech: [E|Blush]But school doesn't teach you the important stuff, anyway.")
    fnDialogue("Leif: [E|Mad]Yeah, talk to us when your classmates can assemble a revolver in 45 seconds.")
    fnDialogue("Leif: [E|N] It would take them [ITALICS]at least[ENDITALICS] twice as long.")
    fnDialogue("Player: At least.")
    fnDialogue("Rech: [P][P][E|Sad]Well...[P]then.")
    fnDialogue("Leif: [E|Sad]Y-[P]yeah...")
    fnDialogue("Player: What's the matter? You guys look like you have something to say--")
    fnDialogue("Thought: In one big swoop, Rech and Leif dive for me.[P] The next thing I know, I'm tangled in a tight coil of long limbs.")
    fnDialogue("Rech: [E|Blush]We're giving you a hug!")
    fnDialogue("Leif: [E|Sad]We wanted to practice ahead of time, but Six and Serena wouldn't let us...")
    fnDialogue("Leif: [E|Blush]But it looked like a lot of fun!")
    fnDialogue("Twins: [FOCUS|Reich][FOCUSMOD|Leif|true][EMOTION|Reich|Happy][EMOTION|Leif|Happy][CRASH]Are we doing it right!?!?")
    fnDialogue("Thought: Its hard to laugh considering how tightly they're squeezing me, but I draw the two of them in closer.")
    fnDialogue("Player: You could've just hugged each other if you wanted to try it so bad, you dopes.")
    fnDialogue("Thought: Sighing happily, I'm patted on the head by a pair of perfectly synchronized hands.")
    fnDialogue("Thought: Rech and Leif always acted like they never needed anybody but each other,")
    fnDialogue("Thought: But I get the impression they're actually willing to make room for a few more.")
    fnDialogue("Player: I have to say, its not bad for your first time. ")
    fnDialogue("Twins: [FOCUS|Reich][FOCUSMOD|Leif|true][EMOTION|Reich|Happy][EMOTION|Leif|Happy]Thanks! And congradulations, [PLAYERNAME]!!")
    fnDialogue("Thought: Released at last, I wave them off while they head for the snack table.")
    fnDialogue("[HIDEALL]Thought: I can talk to someone else, too...")

    --Variables
    local iTalkedToThree = VM_GetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToThree", "N")
    local iTalkedToSix   = VM_GetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToSix", "N")
    local iTalkedToDog   = VM_GetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToDog", "N")
    local iTalkedToTwins = VM_GetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToTwins", "N")

    --Decision
    local sDecisionScript = "\"" .. gsRoot .. "../Chapter 5/Gameplay_SceneC/4 - Talk To Frogs.lua\""
    fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
    if(iTalkedToThree == 0.0) then
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Three\", " .. sDecisionScript .. ", \"Three\") ")
    end
    if(iTalkedToSix == 0.0) then
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Six\", " .. sDecisionScript .. ", \"Six\") ")
    end
    if(iTalkedToDog == 0.0) then
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Dog\", " .. sDecisionScript .. ", \"Dog\") ")
    end
    if(iTalkedToTwins == 0.0) then
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Twins\", " .. sDecisionScript .. ", \"Twins\") ")
    end
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Eilette (End Sequence)\", " .. sDecisionScript .. ", \"Eilette\") ")
    fnCutsceneBlocker()

elseif(sTopicName == "Eilette") then
    
    fnDialogue("[CHARENTER|Eilette]Eilette: [E|N]Ah, [PLAYERNAME]. [E|Shock]Finished already, are you?")
    fnDialogue("Player: Uhh, yeah. I'd honestly rather get out of here as soon as possible. ")
    fnDialogue("Eilette: [E|N]For once, I agree.[P][E|Mad] That outdoor ceremony was so hot,[P] I nearly had to [ITALICS]remove[ENDITALICS] my coat.")
    fnDialogue("Thought: Eilette takes out a pocketbook, glances at the pages, and snaps it shut. [P]This is the signal for the rest of the team to congregate.")
    fnDialogue("[CHARENTER|Reich][CHARENTER|Leif][CHARENTER|AJ][CHARENTER|Three][CHARENTER|Six]Rech: [E|N]Yes boss?")
    fnDialogue("Leif: [E|N]Are we ready?")
    fnDialogue("Eilette: [E|N]Indeed.[P] Come along, everyone.")
    fnDialogue("Dog: [E|Mad]I'M protest![P][E|Sad] Dog was so closse to--ccatching the squirrel!?!")
    
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"DegCredits\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
	PairanormalLevel_FlagCheckpoint("DegCredits")
    
    fnDialogue("[HIDEALL][Music|Null][BG|Root/Images/Backgrounds/All/Gym]Player: [P][SFX|Ugh]The gymnasium. [P]We're doing this [P]here.")
    fnDialogue("[CHARENTER|Reich][CHARENTER|Leif][CHARENTER|AJ][CHARENTER|Three][CHARENTER|Six][CHARENTER|Eilette]Eilette: [E|N]... I believe than any form of induction requires the proper setting.")
    fnDialogue("Eilette: [E|Mad] Unfortunately, it has been several years since a new member has joined us,[P] and our former cermonial hall was compromised.")
    fnDialogue("Player: [ITALICS]Ceremonial hall?[ENDITALICS] ")
    fnDialogue("Six: [E|N]Old pencil warehouse we used to own. [P]*hiss*[P] It got blown up.")
    fnDialogue("Player: *gulp*")
    fnDialogue("Eilette: [E|N]So for today, this will have to do.")
    fnDialogue("[MUSIC|Somber]Thought: Eilette strides to what she probably believes is a more authoritative position on the gym floor.")
    fnDialogue("Thought: She begins reciting what I believe to be a practiced speech, which I wasn't really anticipating.")
    fnDialogue([[Eilette: [E|Happy]Today, we welcome [PLAYERNAME] as a new member to our organization[P][E|Mad]--[ITALICS]informally[ENDITALICS] known as \"The Frog Society\"--]])
    fnDialogue("Twins: [FOCUS|Reich][FOCUSMOD|Leif|true][EMOTION|Leif|Happy][EMOTION|Reich|Happy] yessss.")
    fnDialogue("Eilette: [E|Blush]--Whose mission and purpose is to dismantle the Foster Family Empire.")
    fnDialogue("Eilette: [E|N]Their actions have done immeasurable harm to the citizens of Foster Falls and surrounding counties,[P] and we intend to bring them to justice.")
    fnDialogue("Thought: I lower my head.[P] The din of the graduation festivities outside slowly fades away, and I let these words center my thoughts.")
    fnDialogue("Thought: I was worried before about how I would be able to leave my old highschool life behind, but it's actually not too hard.")
    fnDialogue("Thought: I have to let justice be my guide.")
    fnDialogue("Thought: After all, what Eilette says is true.[P] I've seen the pain the Foster Family caused with my own eyes.")
    fnDialogue("Eilette: [E|Mad]The highest ordinances of our organization are determined by each member.[P][E|N] You will now recite.")
    fnDialogue("Six: [E|N]Never judge a book by its cover.")
    fnDialogue("Twins: [FOCUS|Reich][FOCUSMOD|Leif|true][EMOTION|Leif|Neutral][EMOTION|Reich|Neutral]Stick together, or else!")
    fnDialogue("Serena: [E|N]No more breakthroughs.[P] Only break outs!")
    fnDialogue("Dog: [E|N]Dog's rule is. If you ssee dog,[P][E|Happy] You must pet.")
    fnDialogue("Eilette: [E|N]... Don't smile unless you really mean it.")
    fnDialogue("Eilette: [P][P][P][E|N][PLAYERNAME]. To be inducted into the organization, you must declare your very own ordinance.[P] Please recite it now.")
    fnDialogue("Player: [SFX|World|Ugh][P]... I really don't know.")
    fnDialogue("Serena: [P][P][P][E|Mad][CRASH]You've got to be kidding me.")
    fnDialogue("Player: No, I mean, wh--[P] come on--nobody told me this was going to be a thing![P] I didn't have time to prepare!")
    fnDialogue("Six: [E|N] You don't have to prepare. *hiss*[P] It's just the truth.")
    fnDialogue("[FOCUS|Reich][FOCUSMOD|Leif|true]Twins: [E|N]The thing you've learned.")
    fnDialogue("Serena: [E|Mad]The thing you believe at the root of everything.")
    fnDialogue("[HIDEALL]Player: ... Hm. Uh, I guess...")
    
    --We use this trick to get input. We "Replace" the player's name and then extract the string
    -- from the name entry UI.
    local sOldPlayerName = VM_GetVar("Root/Variables/System/Player/Playername", "S")
    VM_SetVar("Root/Variables/System/Player/sStoredPlayerName", "S", sOldPlayerName)
    Dia_SetProperty("Player Name", "")
    Dia_SetProperty("Name Entry String", "Ordinance:")
    fnDialogue("...[ACTIVATENAMEENTRY] ")
	fnCutsceneInstruction([[LM_ExecuteScript(gsRoot .. "../Chapter 5/Gameplay_SceneC/4 - Talk To Frogs.lua", "Ordinance")]])
    
elseif(sTopicName == "Ordinance") then
    
    --Get the playername out. This is the ordinance the player entered.
    local sOrdinance = VM_GetVar("Root/Variables/System/Player/Playername", "S")
    
    --Store the old playername on the name.
    local sOldName = VM_GetVar("Root/Variables/System/Player/sStoredPlayerName", "S")
    VM_SetVar("Root/Variables/System/Player/Playername", "S", sOldName)
    Dia_SetProperty("Player Name", sOldName)
    
    --Presumably Chic will want some sort of recognition here.
    
    LM_ExecuteScript(gsRoot .. "../Chapter 5/Gameplay_SceneC/5 - Credits.lua", "Start")

end