--[Exec First]
--Executes when the program starts. This will occur with the argument "Start".

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--[Execution]
--Start.
if(sTopicName == "Todd") then
    VM_SetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToTodd", "N", 1.0)
    fnDialogue("[HIDEALL][CHARENTER|ToddAdult]Todd: [FOCUS|ToddAdult][VOICE|Voice|Todd][EMOTION|ToddAdult|N]Nicely done, [PLAYERNAME].[P] How do you feel?")
    fnDialogue("Player: Todd!!! You made it!")
    fnDialogue("Todd: [FOCUS|ToddAdult][VOICE|Voice|Todd][EMOTION|ToddAdult|Mad]Are you kidding? Getting to see my first pupil graduate, entirely thanks to me--[P]major ego boost.[P] Couldn't miss it.")
    fnDialogue("Player: Shut up, you dork.")
    fnDialogue("Thought: He squeezes my shoulder affectionately.")
    fnDialogue("Thought: Todd and Mari both work at the same local television studio, but he also does some part time assistant teacher work at Foster Mid. ")
    fnDialogue("Todd: [FOCUS|ToddAdult][VOICE|Voice|Todd][EMOTION|ToddAdult|Shock]Oh, God, is that Penny from my video production class?[P] Geez, I can't catch a break. ")
    fnDialogue("Player: Is that the one who keeps trying to follow you on Twitter?")
    fnDialogue("Todd: [FOCUS|ToddAdult][EMOTION|ToddAdult|Shock]Shit! She saw me.[P][E|Mad] Quick, pretend you're talking to me.")
    fnDialogue("Player: I [ITALICS]am[ENDITALICS] talking to you.")
    fnDialogue("Thought: He's [ITALICS]very[ENDITALICS] popular with students.")
    fnDialogue("Thought: Thankfully, there's usually someone around to help scare them off.")
    fnDialogue("[CHARENTER|Harper]Harper: [E|Happy]Excuuuuuse me, little girl.[P][E|N] There's a teeny tiny stain on the back of your dress.")
    fnDialogue("Harper: [E|Mad] I mean, it's tiny, but actually very noticeable.[P][E|N] Might wanna have that tak-en care of.")
    fnDialogue("Todd: [FOCUS|ToddAdult][VOICE|Voice|Todd][EMOTION|ToddAdult|N]Oh, there you are Harper.[P] Come say congradulations to [PLAYERNAME]!")
    fnDialogue("Harper: [E|Happy]Dar-liiiing! You did it! Well done.")
    fnDialogue("Harper: [E|N]Now, if you'll excuse me, I have to write a series of tweets about how lame graduation ceremonies are.")
    fnDialogue("Player: Haha, nice to see you too, Harper.[P] And you, Todd.")
    fnDialogue("Todd: [FOCUS|ToddAdult][VOICE|Voice|Todd][EMOTION|ToddAdult|N]I'll be around, avoiding Penny.[P] Don't be a stranger!")
    fnDialogue("[HIDEALL]Thought: I can talk to some else, too...")

    --Variables
    local iTalkedToTodd     = VM_GetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToTodd", "N")
    local iTalkedToBret     = VM_GetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToBret", "N")
    local iTalkedToLaura    = VM_GetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToLaura", "N")
    local iTalkedToMichelle = VM_GetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToMichelle", "N")

    --Decision
    local sDecisionScript = "\"" .. gsRoot .. "../Chapter 5/Gameplay_SceneC/3 - Talk To Teens.lua\""
    fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
    if(iTalkedToTodd == 0.0) then
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Todd\", " .. sDecisionScript .. ", \"Todd\") ")
    end
    if(iTalkedToMichelle == 0.0) then
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Michelle\", " .. sDecisionScript .. ", \"Michelle\") ")
    end
    if(iTalkedToBret == 0.0) then
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Bret\", " .. sDecisionScript .. ", \"Bret\") ")
    end
    if(iTalkedToLaura == 0.0) then
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Laura\", " .. sDecisionScript .. ", \"Laura\") ")
    end
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Mari and Stacy (End Sequence)\", " .. sDecisionScript .. ", \"MariStacy\") ")
    fnCutsceneBlocker()
    
elseif(sTopicName == "Bret") then
    VM_SetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToBret", "N", 1.0)
    
    fnDialogue("[HIDEALL][CHARENTER|BretAdult]Bret: [FOCUS|BretAdult][VOICE|Voice|Bret][EMOTION|BretAdult|Shock][PLAYERNAME]![P] Oh my god![P] You did it!")
    fnDialogue("Bret: [FOCUS|BretAdult][VOICE|Voice|Bret][EMOTION|BretAdult|Happy] I'm so proud of you.[P][EMOTION|BretAdult|Mad] I mean, you worked so hard, [EMOTION|BretAdult|N]and you did it, and you're amazing.[P][EMOTION|BretAdult|Blush] Am I showering you with too many compliments!?")
    fnDialogue("Player: Not nearly enough.[P] Come here, you.")
    fnDialogue("Thought: We hug, and Bret procures a rather ostentatious bouquet of celebratory flowers for me.")
    fnDialogue("Thought: After high school,[P] Bret took an accelerated college course and is now working part time at the museum,")
    fnDialogue("Thought: Where he can fawn over the curator to his (but definitely not her) heart's content.")
    fnDialogue("[CHARENTER|Three]Serena: [E|N]Hey, kid. Congradulations on your graduation. ")
    fnDialogue("Player: Thanks.[P] Uh, how are things at the museum?")
    fnDialogue("Serena: [E|Blush]Business is booming, so naturally I've got my hands full.")
    fnDialogue("Serena: Bret has been a useful addition to our staff.")
    fnDialogue("Bret: [FOCUS|BretAdult][VOICE|Voice|Bret][EMOTION|BretAdult|Blush]Oh, gosh,[P] I mean, you're too kind Serena, I mean Miss Serena,[P] uh, I mean Miss Freeman,")
    fnDialogue("Bret: [FOCUS|BretAdult][VOICE|Voice|Bret][SPEEDTEXT][EMOTION|BretAdult|Sad]Ah, all I'm saying is, like, I just do a [EMOTION|BretAdult|Mad]little simple clerical work here and there... it's [EMOTION|BretAdult|Blush]all thanks to your brilliant exhibition ideas, really--[ENDSPEEDTEXT] ")
    fnDialogue("Player: New exhibitions at the museum?[P] I never thought the manager would approve of something like that.")
    fnDialogue("Serena: [E|Blush]Well [ITALICS]I'm[ENDITALICS] the manager now, and I think my performance speaks for itself.")
    fnDialogue("Bret: [FOCUS|BretAdult][VOICE|Voice|Bret][EMOTION|BretAdult|N]Yeah--[EMOTION|BretAdult|Happy]isn't it fortunate the previous curator decided to move to Tanzania?")
    fnDialogue("Bret: [FOCUS|BretAdult][VOICE|Voice|Bret][EMOTION|BretAdult|Shock]Although it was kinda weird how he disappeared without telling anyone and [EMOTION|BretAdult|Sad]only left behind a typewritten note,")
    fnDialogue("Bret: [FOCUS|BretAdult][VOICE|Voice|Bret][EMOTION|BretAdult|Mad]AND he left his cat behind,[P][EMOTION|BretAdult|Sad] but that's fine because I took it in, but still--")
    fnDialogue("Serena: [E|Shock]Bret, er, don't be long now.[P] I'll be waiting in the car.")
    fnDialogue("Bret: [FOCUS|BretAdult][VOICE|Voice|Bret][EMOTION|BretAdult|Blush]Oh, yeah, right! Eh...*ahem*.[P][EMOTION|BretAdult|N] I'll see you around, [PLAYERNAME].")
    fnDialogue("Player: See you, Bret.")

    --Variables
    local iTalkedToTodd     = VM_GetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToTodd", "N")
    local iTalkedToBret     = VM_GetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToBret", "N")
    local iTalkedToLaura    = VM_GetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToLaura", "N")
    local iTalkedToMichelle = VM_GetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToMichelle", "N")

    --Decision
    local sDecisionScript = "\"" .. gsRoot .. "../Chapter 5/Gameplay_SceneC/3 - Talk To Teens.lua\""
    fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
    if(iTalkedToTodd == 0.0) then
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Todd\", " .. sDecisionScript .. ", \"Todd\") ")
    end
    if(iTalkedToMichelle == 0.0) then
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Michelle\", " .. sDecisionScript .. ", \"Michelle\") ")
    end
    if(iTalkedToBret == 0.0) then
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Bret\", " .. sDecisionScript .. ", \"Bret\") ")
    end
    if(iTalkedToLaura == 0.0) then
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Laura\", " .. sDecisionScript .. ", \"Laura\") ")
    end
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Mari and Stacy (End Sequence)\", " .. sDecisionScript .. ", \"MariStacy\") ")
    fnCutsceneBlocker()

elseif(sTopicName == "Michelle") then
    VM_SetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToMichelle", "N", 1.0)
    
    fnDialogue("[HIDEALL][CHARENTER|MichelleAdult]Michelle: [FOCUS|MichelleAdult][VOICE|Voice|Michelle][EMOTION|MichelleAdult|N]Well, well, well.[P] Look who we have here.")
    fnDialogue("Player: Michelle! You made it![P] I thought you were working on your gallery opening.")
    fnDialogue("Michelle: [FOCUS|MichelleAdult][VOICE|Voice|Michelle][EMOTION|MichelleAdult|N]Pff, oh that thing?")
    fnDialogue("Michelle: [FOCUS|MichelleAdult][VOICE|Voice|Michelle][EMOTION|MichelleAdult|Blush]I thought I could just use a stroll, you know.[P] Clear my head.")
    fnDialogue("Michelle: [FOCUS|MichelleAdult][VOICE|Voice|Michelle]And you know, you happened to be graduating on the same day.[P] So I thought I'd come by.[P] No big deal.")
    fnDialogue("Thought: I wrap my arms tightly around her waist and squeeze, to show her it's a very big deal for me.")
    fnDialogue("Michelle: [FOCUS|MichelleAdult][VOICE|Voice|Michelle][EMOTION|MichelleAdult|Shock]!!![P] O-oy!")
    fnDialogue("Michelle: [FOCUS|MichelleAdult][VOICE|Voice|Michelle][EMOTION|MichelleAdult|Blush]Quit getting all lovey dovey and stuff...[ITALICS]geez[ENDITALICS].[P] *hrm*. Here. ")
    fnDialogue("Thought: She hands me a tiny square canvas.")
    fnDialogue("Thought: It's got Michelle's trademark abstract shapes, but much smaller and detailed than I've seen before.")
    fnDialogue("Thought: Little gold flecks of paint run through it like veins.")
    fnDialogue("Player: This is beautiful.[P] Thank you so much Michelle.")
    fnDialogue("Michelle: [FOCUS|MichelleAdult][VOICE|Voice|Michelle][EMOTION|MichelleAdult|Sad]... Yeah, well, you deserve it.[P] You did a good job.")
    fnDialogue("Michelle: [FOCUS|MichelleAdult][VOICE|Voice|Michelle][EMOTION|MichelleAdult|Mad]Anyway! That's enough mushy stuff for me.[P][E|N] I'll be around, [PLAYERNAME].")

    --Variables
    local iTalkedToTodd     = VM_GetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToTodd", "N")
    local iTalkedToBret     = VM_GetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToBret", "N")
    local iTalkedToLaura    = VM_GetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToLaura", "N")
    local iTalkedToMichelle = VM_GetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToMichelle", "N")

    --Decision
    local sDecisionScript = "\"" .. gsRoot .. "../Chapter 5/Gameplay_SceneC/3 - Talk To Teens.lua\""
    fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
    if(iTalkedToTodd == 0.0) then
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Todd\", " .. sDecisionScript .. ", \"Todd\") ")
    end
    if(iTalkedToMichelle == 0.0) then
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Michelle\", " .. sDecisionScript .. ", \"Michelle\") ")
    end
    if(iTalkedToBret == 0.0) then
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Bret\", " .. sDecisionScript .. ", \"Bret\") ")
    end
    if(iTalkedToLaura == 0.0) then
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Laura\", " .. sDecisionScript .. ", \"Laura\") ")
    end
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Mari and Stacy (End Sequence)\", " .. sDecisionScript .. ", \"MariStacy\") ")
    fnCutsceneBlocker()

elseif(sTopicName == "Laura") then
    VM_SetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToLaura", "N", 1.0)
    
    fnDialogue("[HIDEALL][CHARENTER|LauraAdult]Laura: [FOCUS|LauraAdult][VOICE|Voice|Laura][EMOTION|LauraAdult|Neutral]E-e-er... *ahem*...*hrm*.[P] [PLAYERNAME]![P] C-congratulations.")
    fnDialogue("Player: Laura. ")
    fnDialogue("Thought: She grasps my hand tightly, and I can tell she's shaking a little bit.")
    fnDialogue("Thought: --I hope with excitement more than nervousness from all the noise. ")
    fnDialogue("Player: I should have asked to borrow your headphones.[P] It's such a ruckus out here, right?")
    fnDialogue("Laura: [FOCUS|LauraAdult][VOICE|Voice|Laura][EMOTION|LauraAdult|Happy]R-right! But...[P][EMOTION|LauraAdult|Mad] who cares!?![P] We have every right to be celebrating!")
    fnDialogue("Laura: [FOCUS|LauraAdult][VOICE|Voice|Laura]Oof, if I'm going to get my doctorate before I turn 25, I'm going to have to go through like...[P]th-three more of these, anyway.")
    fnDialogue("Thought: We giggle and share an embrace.[P] Laura's been going to college on a full ride, because of course she is.")
    fnDialogue("Thought: But somehow between all her midterms and finals,[P] she's found the time to help fix my computer when I inevitably manage to mess it up. ")

    local iMetAdam = VM_GetVar("Root/Variables/ChapterVars/Chapter4/iMetAdam", "N")
    if(iMetAdam == 1.0) then
        fnDialogue("Thought: My cellphone vibrates, separating us, and I take a quick peek.")
        fnDialogue("[CHARENTER|Cellphone]ADAM: [VOICE|Voice|ADAM]~*~*~*~!!!C O N G R A T S!!!~*~*~*~ ")
        fnDialogue("Player: ADAM![P] Wait minute, I just got this phone.[P] How does he know--")
        fnDialogue("Laura: [FOCUS|LauraAdult][VOICE|Voice|Laura][EMOTION|LauraAdult|Happy]Well, since he's been plugged into the internet...[P][EMOTION|LauraAdult|N]H-He knows everyone's phone number.")
        fnDialogue("Laura: [FOCUS|LauraAdult][VOICE|Voice|Laura]*hrm*[P] In the whole world. ")
        fnDialogue("Player: !!!")
        fnDialogue("Laura: [FOCUS|LauraAdult][VOICE|Voice|Laura][EMOTION|LauraAdult|Shock]Don't worry![P][EMOTION|LauraAdult|Happy] *ahem* he's actually very polite!")
        fnDialogue("Laura: [FOCUS|LauraAdult][VOICE|Voice|Laura]Never calls anyone without permission--[P][E|N]u-unless he's ordering a pizza or something.")
        fnDialogue("ADAM: [VOICE|Voice|ADAM]<3 <3 <3 THERE'S FIVE BOXES OF CHARLIE'S WAITING FOR YOU WHEN YOU GET HOME, CHAMP! \\\\('3<)")
        fnDialogue("Player: You have no idea how happy I am that he didn't turn out evil.")
    end

    fnDialogue("Laura: [FOCUS|LauraAdult][VOICE|Voice|Laura][EMOTION|LauraAdult|Happy]*giggle* I-I'll see you around,[P] [PLAYERNAME].")

    --Variables
    local iTalkedToTodd     = VM_GetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToTodd", "N")
    local iTalkedToBret     = VM_GetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToBret", "N")
    local iTalkedToLaura    = VM_GetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToLaura", "N")
    local iTalkedToMichelle = VM_GetVar("Root/Variables/ChapterVars/Chapter5/iTalkedToMichelle", "N")

    --Decision
    local sDecisionScript = "\"" .. gsRoot .. "../Chapter 5/Gameplay_SceneC/3 - Talk To Teens.lua\""
    fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
    if(iTalkedToTodd == 0.0) then
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Todd\", " .. sDecisionScript .. ", \"Todd\") ")
    end
    if(iTalkedToMichelle == 0.0) then
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Michelle\", " .. sDecisionScript .. ", \"Michelle\") ")
    end
    if(iTalkedToBret == 0.0) then
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Bret\", " .. sDecisionScript .. ", \"Bret\") ")
    end
    if(iTalkedToLaura == 0.0) then
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Laura\", " .. sDecisionScript .. ", \"Laura\") ")
    end
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Mari and Stacy (End Sequence)\", " .. sDecisionScript .. ", \"MariStacy\") ")
    fnCutsceneBlocker()

elseif(sTopicName == "MariStacy") then

    fnDialogue("[HIDEALL]Thought: I look around for two specific faces in the crowd.[P] The ones who this definitely wouldn't be possible without.")
    fnDialogue("Thought: But when I feel a familiar embrace around my shoulders, I know they've found me first.")
    fnDialogue("[CHARENTER|MariAdult]Mari: [FOCUS|MariAdult][VOICE|Voice|Mari][EMOTION|MariAdult|Shock]GUESS WHAT!?!?")
    
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"NewsTalkMari\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
	PairanormalLevel_FlagCheckpoint("NewsTalkMari")
    
    fnDialogue("[CHARENTER|StacyAdult]Stacy: [FOCUS|StacyAdult][VOICE|Voice|Stacy][EMOTION|StacyAdult|N]You did it, [PLAYERNAME]!!!")
    fnDialogue("Both: [FOCUS|MariAdult][FOCUSMOD|StacyAdult|True][CRASH][EMOTION|MariAdult|Happy][EMOTION|StacyAdult|Happy]TRES BIEEEENNNNN!!!!")
    fnDialogue("Player: You guys maaaaade it![P] Hahaha! ")
    fnDialogue("Mari: [FOCUS|MariAdult][VOICE|Voice|Mari][EMOTION|MariAdult|Shock][CRASH]BREAKING NEWS!!")
    fnDialogue("Mari: [FOCUS|MariAdult][VOICE|Voice|Mari][EMOTION|MariAdult|N]THIS JUST IN,[P] the coolest person in the whole world, our friend [PLAYERNAME],[EMOTION|MariAdult|Mad] who overcame like, a ton of challenges like it was [ITALICS]nothing[ENDITALICS]--")
    fnDialogue("Mari: [FOCUS|MariAdult][VOICE|Voice|Mari][EMOTION|MariAdult|Happy]--just graduated highschool after 5 SOLID years!")
    fnDialogue("Thought: Mari works at the local television station, and quickly climbed the ranks from intern to on-the-scene journalist.")
    fnDialogue([[Thought: The director still won't let her run regular \"paranormal activity\" segments,[P] but I'm sure its only a matter of time.]])
    fnDialogue("Mari: [FOCUS|MariAdult][VOICE|Voice|Mari][EMOTION|MariAdult|N]So!?!? [CRASH][EMOTION|MariAdult|Shock][ITALICS]How do you feel?[ENDITALICS] ")
    fnDialogue("Player: Oh man, are you kidding? [P]I'm on top of the world.")
    fnDialogue("Player: I-I'm so happy.")
    fnDialogue("Mari: [FOCUS|MariAdult][VOICE|Voice|Mari][P][P][P][EMOTION|MariAdult|N]... Except?")
    fnDialogue("Player: Wh-what?[P] There's no except.[P] I'm happy, I swear!")
    fnDialogue("Stacy: [FOCUS|StacyAdult][VOICE|Voice|Stacy][EMOTION|StacyAdult|Blush]There's something you're keeping from us.[P][EMOTION|StacyAdult|Mad] I can see it in your deep, soulful eyes.")
    fnDialogue("Stacy: [FOCUS|StacyAdult][VOICE|Voice|Stacy][EMOTION|StacyAdult|N]COME ON, spit it out [ITALICS]henceforth[ENDITALICS].")
    fnDialogue("Player: Ack!![P] Oh god, not the arm bar--")
    fnDialogue("Thought: Stacy has become so much more deadly (and fit) since she became a top social media influencer[P]-slash- personal trainer.")
    fnDialogue("Player: All right, all right![P] I concede.")
    fnDialogue("Mari: [FOCUS|MariAdult][VOICE|Voice|Mari][EMOTION|MariAdult|Happy]YES??? Give me that sweet, sweet scoop.")
    fnDialogue("Player: I'm...[P][P]well, I'm going to miss the Paranormal Club.")
    fnDialogue("[CHARENTER|Ronnie][CHARENTER|Nelson][CHARENTER|Elizabeth]Ronnie: [CRASH]CAPTAIN!!!")
    fnDialogue("Nelson: We... we're going to miss you too,[P] captain.")
    fnDialogue("Elizabeth: *louder, agreeing blubbering noises*")
    fnDialogue("Player: Don't worry, guys. I know it's in good hands now.")
    fnDialogue("Player: But... [P]we sure did have a lot of fun, didn't we?")
    fnDialogue("Thought: Stacy and Mari smile wide, the relief on their faces apparent. [P]I don't really understand.")
    fnDialogue("Stacy: [FOCUS|StacyAdult][VOICE|Voice|Stacy]The club [ITALICS]was[ENDITALICS] a lot of fun. But in a way, it's already served its purpose.")
    fnDialogue("Stacy: [FOCUS|StacyAdult][VOICE|Voice|Stacy]After all, our motto was--")
    fnDialogue("Ronnie: [CRASH]FINDING DEMONS TO DATE VIA VARIOUS PARANORMAL TECHNIQUES--")
    fnDialogue("Player: No!!![P] Jeez Ronnie.[P] I told you no.")
    fnDialogue("Ronnie: Mmh.")
    fnDialogue("[HIDEALL][CHARENTER|ToddAdult]Todd: [FOCUS|ToddAdult][ITALICS]Are highschoolers seriously this horny?[ENDITALICS] ")
    fnDialogue("Player: [P]... The mission of the club has always been to investigate the paranormal. ")
    fnDialogue("[CHARENTER|BretAdult]Bret: [FOCUS|BretAdult][VOICE|Voice|Bret][P][EMOTION|BretAdult|Happy]To uncover the truth where others may prefer to turn a blind eye!")
    fnDialogue("[CHARENTER|LauraAdult]Laura: [FOCUS|LauraAdult][VOICE|Voice|Laura][EMOTION|LauraAdult|Happy]To forge lifelong friendships!")
    fnDialogue("[CHARENTER|MichelleAdult]Michelle: [FOCUS|MichelleAdult][VOICE|Voice|Michelle][E|Neutral]To do something to pass the time in that hell we call highschool.")
    fnDialogue("[CHARENTER|MariAdult][CHARENTER|StacyAdult][FOCUSMOD|MariAdult|True][FOCUSMOD|StacyAdult|True]Captains: [EMOTION|MariAdult|Happy][EMOTION|StacyAdult|Happy]So that when we are finally liberated from the chains of obligatory education...")
    fnDialogue("Player: [FOCUSMOD|StacyAdult|False][P][P][P][P][P]... We'll be okay.")
    fnDialogue("[HIDEALL][Image|Root/Images/MajorScenes/All/FriendEnd]Thought: I take a delicious deep breath, and even though the autumn air is biting and brisk,[P] I'm filled with warmth from fingertip to fingertip.")
    fnDialogue("Thought: It's finally time for the next adventure...")
    fnDialogue("Thought: [Image|Null]... And I think I have a good idea about where it should start.")
    LM_ExecuteScript(gsRoot .. "../Chapter 5/Gameplay_SceneC/5 - Credits.lua", "Start")
    
end
