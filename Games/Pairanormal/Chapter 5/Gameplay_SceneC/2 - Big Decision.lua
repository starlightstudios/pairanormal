--[Exec First]
--Executes when the program starts. This will occur with the argument "Start".

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--[Execution]
--Start.
if(sTopicName == "Life") then
    fnDialogue("[HIDEALL]Player: Well,[P] if I get to have a choice... I want to try and live my life.")
    fnDialogue("Player: I don't... I don't want to get caught up in this past that I never really had.")
    fnDialogue("Player: I want to... build something new.")
    fnDialogue("Thought: The Frog Society processes this in silence for several moments.")
    fnDialogue("Thought: I can see flecks of lots of different emotions in their eyes.[P] But in Eilette's...")
    fnDialogue("Thought: She places a hand on my shoulder.")
    fnDialogue("[CHARENTER|Eilette]Eilette: [E|N]... Well said, [PLAYERNAME].")
    LM_ExecuteScript(gsRoot .. "../Chapter 5/Gameplay_SceneC/2 - Big Decision.lua", "JoinLife")
    
elseif(sTopicName == "Truth") then
    fnDialogue("[HIDEALL]Player: Well,[P] if I get to have a choice...[P]of course I want to find out the truth!")
    fnDialogue("Player: Somebody already took my whole life from me, a-and I had NO say in it.")
    fnDialogue("Player: It doesn't matter what I have to give up...[P]I want to make sure this never happens to anyone else again.")
    fnDialogue("Thought: Everyone in the room processes this in silence for several moments.")
    fnDialogue("Thought: I can see flecks of lots of different emotions in their eyes.[P] But in Eilette's...")
    fnDialogue("[CHARENTER|Eilette]Eilette: [E|N]... If that is your decision, I will respect it.")
    fnDialogue("Eilette: [P][P][PLAYERNAME], from this day forward, you must prepare to work harder than you ever have before.")
    fnDialogue("Thought: She rises from the couch.")
    fnDialogue("Eilette: [E|Sad] And you must, in your own way,[P] say goodbye to your friends.")
    fnDialogue("Eilette: Because someday,[P] you'll have to disappear from their lives forever.")
    fnDialogue("Player: .[P].[P].[P]Yeah.")
    LM_ExecuteScript(gsRoot .. "../Chapter 5/Gameplay_SceneC/2 - Big Decision.lua", "JoinTruth")

elseif(sTopicName == "JoinLife" or sTopicName == "JoinTruth") then

	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"DecisionMade\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
	PairanormalLevel_FlagCheckpoint("DecisionMade")
    
    fnDialogue("[HIDEALL][BG|Null][Music|Null]Thought: .[P][P][P][P][P].[P][P][P][P][P].[P][P][P][P][P].[P][P][P][P][P].")
    fnDialogue("Info: Five years later[P].[P].[P].")
    fnDialogue("[BG|Root/Images/Backgrounds/All/SchoolExteriorGrad][Music|Love][CHARENTER|Teacher3]Arroyo: [VOICE|Voice|Generic2][FOCUS|Teacher3][EMOTION|Teacher3|Neutral]In the words of our Founder, Kane McKay Foster; ")
    fnDialogue("Arroyo: [VOICE|Voice|Generic2][FOCUS|Teacher3][EMOTION|Teacher3|Neutral]''No goal is too great to accomplish.[P] No man is too small to accomplish it.''")
    fnDialogue("Arroyo: [VOICE|Voice|Generic2][FOCUS|Teacher3][EMOTION|Teacher3|Happy] Congratulations, to all my graduates.[P] Ladies and gentlemen, I now present to you the class of 2014! ")
    fnDialogue("Thought: [CHAREXIT|Teacher3]The small but enthusiastic crowd cheers, but not as much as those of us sitting in the bleachers,[P] waiting two hours under the hot sun for this very moment.")
    fnDialogue("Thought: We were told not to throw our caps in the air because last year lots of them got stuck on the roof,[P] but none of us listened.")
    fnDialogue("Thought: I feel like my cap flew the highest of them all.[P] Maybe because I was the happiest to throw it.")
    fnDialogue("Thought: A beautiful, driving orchestral score plays, and I clutch the sturdy certificate in my hand.")
    fnDialogue("Thought: Not so long ago, I would have had to sit for hours to read the simple message printed at the top.")
	fnDialogue("Thought: But the words are clearer now than anything else I've ever read--[P] ''[PLAYERNAME] Mira,[P][P] Graduate.''")
    fnDialogue("[CHARENTER|Ronnie][CHARENTER|Elizabeth][CHARENTER|Nelson]Ronnie: CAPTAIN!!!!")
    fnDialogue("Thought: A handful of teenagers crowd around me, their faces enthusiastic but clearly holding back tears.[P] Except for Elizabeth.")
    fnDialogue("Elizabeth: aaaaeeeeeugggghhhhaaaaaa...")
    fnDialogue("Player: Heh. Guys, you came out to my graduation? You're supposed to be off losing your minds for summer vacation, or something.")
    fnDialogue("Ronnie: Captain.[P][P][CRASH] oF cOuRsE wE'D cOMe tO yOuR GraDuAtIon!!!!")
    fnDialogue("Nelson: The truth is captain, we had to know...[P]while we're all very happy for you in this moment,")
    fnDialogue("Nelson: ... What's going to happen to the Paranormal Club from here on out?")
    fnDialogue("Player: Uhh...[P]I guess it's up to you guys.[P] Do you like doing it?")
    fnDialogue("Ronnie: [CRASH]HoW cAn yOU eVeN aSk That qUeStION!!?!?")
    fnDialogue("Ronnie: Hunting down ghosts and breaking and entering abandoned factories is the only reason I come to school!")
    fnDialogue("Elizabeth: [SFX|Ugh]*enthusiastic blubbering*")
    fnDialogue("Player: Well then.[P] You should keep it going.")
    fnDialogue("Player: Figure out who's gonna be the captain next year and... sign up just like you would for any other club![P] Here.")
    fnDialogue("Thought: I reach into my pocket and toss Ronnie a set of keys.")
    fnDialogue("Nelson: Those can't be... the [ITALICS]keys[ENDITALICS].[P][P] [ITALICS]To the clubhouse[ENDITALICS]!!")
    fnDialogue("Thought: Seeing as the clubhouse was always situated in the open woods, the keys are more symbolic than actually useful.")
    fnDialogue("Player: I think...[P]the original captains would be really happy to hear you're keeping their traditions alive.")
    fnDialogue("Player: Take good care of that clubhouse, okay?")
    fnDialogue("Elizabeth: [SFX|Ugh]*emotional hyperventilating*")

	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Spylette\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
	PairanormalLevel_FlagCheckpoint("Spylette")
    
    fnDialogue("[HIDEALL][CHARENTER|Eilette]Eilette: [E|N]There you are, [PLAYERNAME].")
    fnDialogue("Thought: Eilette approaches, and my former classmates scatter, having learned not to get in her way.")
    fnDialogue("Eilette: [E|Shock]How is your body temperature?")
    fnDialogue("Eilette: [E|Blush]Prolonged exposure to the sun in these dark robes creates a high likelihood for heatstroke.")
    fnDialogue("Player: I think I'm okay.[P] I'm mostly just excited to be done with highschool.")
    fnDialogue("Eilette: [E|N]Ah, yes.[P][E|Happy] That's certainly an accomplishment you should be proud of.")
    fnDialogue("Eilette: [E|Mad] Although I'm not certain it warrants an entire [ITALICS]four-hour mandatory ceremony[ENDITALICS].")
    fnDialogue("Eilette: I believe a small trophy or perhaps quick dinner festivities would have been more appropriate.[P][E|Shock] Anyway, here.")
    fnDialogue("Thought: Eilette quickly presses a large bouquet and something small and papery into my hands.")
    fnDialogue("Thought: I struggle to hold onto both the gifts and my certificate.")
    fnDialogue("Eilette: [E|N]The flowers are a customary commemorative item, feel free to discard them.")
    fnDialogue("Eilette: I've also given you some money.")
    fnDialogue("Eilette: Since you seem unwilling to discuss your personal tastes with me, I believe it's easier for you to just purchase something for yourself as a gift.")
    fnDialogue("Eilette: [E|Mad]It's also a helpful reminder that from now on, outside of school,[P] you need to ensure all your labor is compensated.")
    fnDialogue("Player: Th...[P][SFX|Ugh]thanks?")
    fnDialogue("Eilette: [E|Happy]Well that's enough pleasantries.[P] You have others who are anxious to speak with you.")
    fnDialogue("Thought: I nod, spotting several familiar faces coming closer.[P] I'm anxious to speak with them too.")

    if(sTopicName == "JoinLife") then
        
        fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (Who do I talk to?)") ]])

        local sDecisionScript = "\"" .. gsRoot .. "../Chapter 5/Gameplay_SceneC/3 - Talk To Teens.lua\""
        fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Todd\", " .. sDecisionScript .. ", \"Todd\") ")
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Michelle\", " .. sDecisionScript .. ", \"Michelle\") ")
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Bret\", " .. sDecisionScript .. ", \"Bret\") ")
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Laura\", " .. sDecisionScript .. ", \"Laura\") ")
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Mari and Stacy (End Sequence)\", " .. sDecisionScript .. ", \"MariStacy\") ")
        fnCutsceneBlocker()
        
    elseif(sTopicName == "JoinTruth") then
        
        fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "Thought: (Who do I talk to?)") ]])

        local sDecisionScript = "\"" .. gsRoot .. "../Chapter 5/Gameplay_SceneC/4 - Talk To Frogs.lua\""
        fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Six and Five\", " .. sDecisionScript .. ", \"Six\") ")
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Serena\", " .. sDecisionScript .. ", \"Three\") ")
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Doggo\", " .. sDecisionScript .. ", \"Dog\") ")
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Rech and Leif\", " .. sDecisionScript .. ", \"Twins\") ")
        fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Eilette (End Sequence)\", " .. sDecisionScript .. ", \"Eilette\") ")
        fnCutsceneBlocker()
    end
end
