--[Exec First]
--Executes when the program starts. This will occur with the argument "Start".

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--[Execution]
--Start.
if(sTopicName == "Start") then
	
    --Variables
    local sBuddy = VM_GetVar("Root/Variables/System/Romance/sBuddy", "S")
    local iToddAlong = VM_GetVar("Root/Variables/ChapterVars/Chapter5/iToddAlong", "N")
    
    --Dialogue.
    fnDialogue("Thought: I feel a strange buzzing in both ears, like something just blew past me.")
    fnDialogue("Thought: In the corner of the room, a previously unnoticed camera-like device beeps to life. It's red eye focuses to me.")
    
    --Special effect: Screen flashes red periodically
    
    fnDialogue("[MUSIC|Tension]Intercom: CONTAINMENT BREACH.[P] CONTAINMENT BREACH")
    fnDialogue("Intercom: ALL PERSONNEL, REPORT TO EMERGENCY STATIONS.")
    fnDialogue("Intercom: ACTIVATING RE-ATTAINMENT PROCEDURES.")
    fnDialogue("Player: What...?")
    fnDialogue("Thought: The building roars to life like an ancient, slumbering beast;[P] battered and worse for wear, but still deadly.")
    fnDialogue("Thought: Gates that I hadn't seen before start to descend over the exit doors.")
    fnDialogue("Thought: Oh damn.[P][ITALICS] oh damn!!![ENDITALICS]")
    
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"LockDaun\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
	PairanormalLevel_FlagCheckpoint("LockDaun")
    
    fnDialogue("[HIDEALL][BG|Root/Images/Backgrounds/All/MineLobby]Player: Guys! Is everyone okay!?")
    
    if(iToddAlong == 0.0) then
        fnDialogue("[CHARENTER|Mari][CHARENTER|Stacy][CHARENTER|Bret][CHARENTER|Michelle][CHARENTER|Laura]Stacy: [FOCUS|Stacy][E|Shock]The building is gonna go into like, a full lockdown mode any second.")
    else
        fnDialogue("[CHARENTER|Mari][CHARENTER|Stacy][CHARENTER|Bret][CHARENTER|Michelle][CHARENTER|Laura][CHARENTER|Todd]Stacy: [E|Shock]The building is gonna go into like, a full lockdown mode any second.")
    end
    fnDialogue("Mari: [E|Sad]Everyone's here except--[P][E|Shock][CRASH]BRET!!!")
    fnDialogue("Bret: [E|Shock]Yeah, sorry, I'm coming.[P][E|N] I just couldn't help picking up a souvenir.")
    fnDialogue("Bret: [E|Happy]Isn't this helmet cool, you guys?")
    fnDialogue("Michelle: [E|Mad][CRASH]We'll see how bloody cool it is when you've trapped us here for eternity, you knob!")
    fnDialogue("Thought: My breath catches.[P][ITALICS] Shoot![ENDITALICS]")
    fnDialogue("Thought: That's right[P]--I wanted to bring something back to Eilette to prove that I've come here,[P] and that I'm ready for more answers. ")
    fnDialogue("Player: Head for the exit, everyone.")
    if(iToddAlong == 1.0) then
        fnDialogue("Todd: [E|Shock]Come on, guys!")
        fnDialogue("[CHAREXIT|Todd][CHAREXIT|Michelle][CHAREXIT|Laura][CHAREXIT|Bret]Mari: [E|Shock][PLAYERNAME], wait, it's this way![P] Where are you going!?")
    else
        fnDialogue("[CHAREXIT|Michelle][CHAREXIT|Laura][CHAREXIT|Bret]Mari: [E|Shock][PLAYERNAME], wait, it's this way![P] Where are you going!?")
    end
    fnDialogue("Player: I'll be right back.")
    fnDialogue("Stacy: [E|Shock][PLAYERNAME]!!!")
    fnDialogue("")

    fnCutsceneInstruction([[ Dia_SetProperty("Set Dialogue", "[HIDEALL]Thought: (What do I take with me?)") ]])

    local sDecisionScript = "\"" .. gsRoot .. "../Chapter 5/Gameplay_SceneC/1 - Go Home.lua\""
    fnCutsceneInstruction([[ Dia_SetProperty("Activate Decisions") ]])
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Bimini Logo\", " .. sDecisionScript .. ", \"Bimini\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Frog Society Photo\", " .. sDecisionScript .. ", \"FrogSociety\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Kane Kola\", " .. sDecisionScript .. ", \"KaneKola\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Pod Photograph\", " .. sDecisionScript .. ", \"Podograph\") ")
    fnCutsceneInstruction(" Dia_SetProperty(\"Append Decision\", \"Photo of Kane and ADAM\", " .. sDecisionScript .. ", \"KaneAndADAM\") ")
    fnCutsceneBlocker()



end
