-- |[ ================================ Dialogue Actor Creation ================================= ]|
--Creates dialogue actors for the finale.

--Get the Pairanormal spine path.
local sPairanormalBasePath = VM_GetVar("Root/Paths/System/Startup/sPairanormalPath", "S")
local sSpinePath = sPairanormalBasePath .. "Spine/"

-- |[Bret, Adult]|
Dia_CreateCharacter("BretAdult")
    sImgName = "BretAdult"
    sLower = "bretadult"
    DiaChar_SetProperty("Add Expression", "Expression0", sSpinePath .. sImgName .. "/Expression0", "Root/Images/Characters/" .. sImgName .. "/Expression0", "bret_mad", "bret_shock")
    DiaChar_SetProperty("Add Expression", "Expression1", sSpinePath .. sImgName .. "/Expression1", "Root/Images/Characters/" .. sImgName .. "/Expression1", "bret_happy", "bret_n")
    DiaChar_SetProperty("Add Expression", "Expression2", sSpinePath .. sImgName .. "/Expression2", "Root/Images/Characters/" .. sImgName .. "/Expression2", "bret_sad", "bret_blush")
    DiaChar_SetProperty("Remap Emotion", "Neutral", "Expression0", "bret_n")
    DiaChar_SetProperty("Remap Emotion", "Sad",     "Expression0", "bret_sad")
    DiaChar_SetProperty("Remap Emotion", "Mad",     "Expression1", "bret_mad")
    DiaChar_SetProperty("Remap Emotion", "Shock",   "Expression1", "bret_shock")
    DiaChar_SetProperty("Remap Emotion", "Shocked", "Expression1", "bret_shock")
    DiaChar_SetProperty("Remap Emotion", "Happy",   "Expression2", "bret_happy")
    DiaChar_SetProperty("Remap Emotion", "Blush",   "Expression2", "bret_blush")
DL_PopActiveObject()

-- |[Laura, Adult]|
Dia_CreateCharacter("LauraAdult")
    sImgName = "LauraAdult"
    sLower = "lauraadult"
    DiaChar_SetProperty("Add Expression", "Expression0", sSpinePath .. sImgName .. "/Expression0", "Root/Images/Characters/" .. sImgName .. "/Expression0", "laura_blush", "laura_happy")
    DiaChar_SetProperty("Add Expression", "Expression1", sSpinePath .. sImgName .. "/Expression1", "Root/Images/Characters/" .. sImgName .. "/Expression1", "laura_mad", "laura_n")
    DiaChar_SetProperty("Add Expression", "Expression2", sSpinePath .. sImgName .. "/Expression2", "Root/Images/Characters/" .. sImgName .. "/Expression2", "laura_sad", "laura_shock")
    DiaChar_SetProperty("Remap Emotion", "Neutral", "Expression0", "laura_n")
    DiaChar_SetProperty("Remap Emotion", "Sad",     "Expression0", "laura_sad")
    DiaChar_SetProperty("Remap Emotion", "Mad",     "Expression1", "laura_mad")
    DiaChar_SetProperty("Remap Emotion", "Shock",   "Expression1", "laura_shock")
    DiaChar_SetProperty("Remap Emotion", "Shocked", "Expression1", "laura_shock")
    DiaChar_SetProperty("Remap Emotion", "Happy",   "Expression2", "laura_happy")
    DiaChar_SetProperty("Remap Emotion", "Blush",   "Expression2", "laura_blush")
DL_PopActiveObject()

-- |[Mari, Adult]|
Dia_CreateCharacter("MariAdult")
    sImgName = "MariAdult"
    sLower = "mariadult"
    DiaChar_SetProperty("Add Expression", "Expression0", sSpinePath .. sImgName .. "/Expression0", "Root/Images/Characters/" .. sImgName .. "/Expression0", "mari_blush", "mari_happy")
    DiaChar_SetProperty("Add Expression", "Expression1", sSpinePath .. sImgName .. "/Expression1", "Root/Images/Characters/" .. sImgName .. "/Expression1", "mari_mad", "mari_shock")
    DiaChar_SetProperty("Add Expression", "Expression2", sSpinePath .. sImgName .. "/Expression2", "Root/Images/Characters/" .. sImgName .. "/Expression2", "mari_n", "mari_sad")
    DiaChar_SetProperty("Remap Emotion", "Neutral", "Expression0", "mari_n")
    DiaChar_SetProperty("Remap Emotion", "Sad",     "Expression0", "mari_sad")
    DiaChar_SetProperty("Remap Emotion", "Mad",     "Expression1", "mari_mad")
    DiaChar_SetProperty("Remap Emotion", "Shock",   "Expression1", "mari_shock")
    DiaChar_SetProperty("Remap Emotion", "Shocked", "Expression1", "mari_shock")
    DiaChar_SetProperty("Remap Emotion", "Happy",   "Expression2", "mari_happy")
    DiaChar_SetProperty("Remap Emotion", "Blush",   "Expression2", "mari_blush")
DL_PopActiveObject()

-- |[Michelle, Adult]|
Dia_CreateCharacter("MichelleAdult")
    sImgName = "MichelleAdult"
    sLower = "michelleadult"
    DiaChar_SetProperty("Add Expression", "Expression0", sSpinePath .. sImgName .. "/Expression0", "Root/Images/Characters/" .. sImgName .. "/Expression0", "michelle_shock", "michelle_happy")
    DiaChar_SetProperty("Add Expression", "Expression1", sSpinePath .. sImgName .. "/Expression1", "Root/Images/Characters/" .. sImgName .. "/Expression1", "michelle_mad", "michelle_blush")
    DiaChar_SetProperty("Add Expression", "Expression2", sSpinePath .. sImgName .. "/Expression2", "Root/Images/Characters/" .. sImgName .. "/Expression2", "michelle_n", "michelle_sad")
    DiaChar_SetProperty("Remap Emotion", "Neutral", "Expression0", "michelle_n")
    DiaChar_SetProperty("Remap Emotion", "Sad",     "Expression0", "michelle_sad")
    DiaChar_SetProperty("Remap Emotion", "Mad",     "Expression1", "michelle_mad")
    DiaChar_SetProperty("Remap Emotion", "Shock",   "Expression1", "michelle_shock")
    DiaChar_SetProperty("Remap Emotion", "Shocked", "Expression1", "michelle_shock")
    DiaChar_SetProperty("Remap Emotion", "Happy",   "Expression2", "michelle_happy")
    DiaChar_SetProperty("Remap Emotion", "Blush",   "Expression2", "michelle_blush")
DL_PopActiveObject()

-- |[Stacy, Adult]|
Dia_CreateCharacter("StacyAdult")
    sImgName = "StacyAdult"
    sLower = "stacyadult"
    DiaChar_SetProperty("Add Expression", "Expression0", sSpinePath .. sImgName .. "/Expression0", "Root/Images/Characters/" .. sImgName .. "/Expression0", "stacy_blush", "stacy_happy")
    DiaChar_SetProperty("Add Expression", "Expression1", sSpinePath .. sImgName .. "/Expression1", "Root/Images/Characters/" .. sImgName .. "/Expression1", "stacy_mad", "stacy_shock")
    DiaChar_SetProperty("Add Expression", "Expression2", sSpinePath .. sImgName .. "/Expression2", "Root/Images/Characters/" .. sImgName .. "/Expression2", "stacy_n", "stacy_sad")
    DiaChar_SetProperty("Remap Emotion", "Neutral", "Expression0", "stacy_n")
    DiaChar_SetProperty("Remap Emotion", "Sad",     "Expression0", "stacy_sad")
    DiaChar_SetProperty("Remap Emotion", "Mad",     "Expression1", "stacy_mad")
    DiaChar_SetProperty("Remap Emotion", "Shock",   "Expression1", "stacy_shock")
    DiaChar_SetProperty("Remap Emotion", "Shocked", "Expression1", "stacy_shock")
    DiaChar_SetProperty("Remap Emotion", "Happy",   "Expression2", "stacy_happy")
    DiaChar_SetProperty("Remap Emotion", "Blush",   "Expression2", "stacy_blush")
DL_PopActiveObject()

-- |[Todd, Adult]|
Dia_CreateCharacter("ToddAdult")
    sImgName = "ToddAdult"
    sLower = "toddadult"
    DiaChar_SetProperty("Add Expression", "Expression0", sSpinePath .. sImgName .. "/Expression0", "Root/Images/Characters/" .. sImgName .. "/Expression0", "todd_blush", "todd_sad")
    DiaChar_SetProperty("Add Expression", "Expression1", sSpinePath .. sImgName .. "/Expression1", "Root/Images/Characters/" .. sImgName .. "/Expression1", "todd_mad", "todd_shock")
    DiaChar_SetProperty("Add Expression", "Expression2", sSpinePath .. sImgName .. "/Expression2", "Root/Images/Characters/" .. sImgName .. "/Expression2", "todd_n", "todd_happy")
    DiaChar_SetProperty("Remap Emotion", "Neutral", "Expression0", "todd_n")
    DiaChar_SetProperty("Remap Emotion", "Sad",     "Expression0", "todd_sad")
    DiaChar_SetProperty("Remap Emotion", "Mad",     "Expression1", "todd_mad")
    DiaChar_SetProperty("Remap Emotion", "Shock",   "Expression1", "todd_shock")
    DiaChar_SetProperty("Remap Emotion", "Shocked", "Expression1", "todd_shock")
    DiaChar_SetProperty("Remap Emotion", "Happy",   "Expression2", "todd_happy")
    DiaChar_SetProperty("Remap Emotion", "Blush",   "Expression2", "todd_blush")
DL_PopActiveObject()
