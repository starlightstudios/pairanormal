-- |[Chapter Variables]|
--Variables that apply only to this chapter, or are used primarily in it.
DL_AddPath("Root/Variables/Chapter5/")

-- |[Miscellaneous]|
DL_AddPath("Root/Variables/Chapter5/Misc/")

-- |[Common Investigation]|
DL_AddPath("Root/Variables/Chapter5/Investigation/")
if(PairanormalLevel_IsLoading() == false) then
    VM_SetVar("Root/Variables/Chapter5/Investigation/iTimeMax", "N", 0.0)
    VM_SetVar("Root/Variables/Chapter5/Investigation/iTimeLeft", "N", 0.0)
end

-- |[First Investigation]|
--This refers to the museum investigation
DL_AddPath("Root/Variables/Chapter5/InvestigationA/")

--Name Lookups
gciInv5A_Bret = 1
gciInv5A_Mari = 2
gciInv5A_Stacy = 3
gciInv5A_Michelle = 4
gciInv5A_Laura = 5
gciInv5A_Todd = 6

gciInv5AAllObjectStart = 7
gciInv5LobbyStart = 7
gciInv5A_Reception = 7
gciInv5A_GeorgeFosterPortrait = 8
gciInv5A_BiminiRecording = 9
gciInv5A_Mural = 10
gciInv5LobbyEnd = 10

gciInv5TwinsStart = 11
gciInv5A_TankMaps = 11
gciInv5A_PhotoPlayer = 12
gciInv5A_Photo34 = 13
gciInv5A_Photo56 = 14
gciInv5A_Photo78 = 15
gciInv5A_Photo910 = 16
gciInv5A_DoubleStorageTubes = 17
gciInv5A_KaneKola = 18
gciInv5A_DoubleSecureTable = 19
gciInv5A_HeavyMachinery = 20
gciInv5TwinsEnd = 20

gciInv5AgentsStart = 21
gciInv5A_Paperwork = 21
gciInv5A_Personnel = 22
gciInv5A_EiletteFile = 23
gciInv5A_CandaceFoster = 24
gciInv5A_CoffeeCup = 25
gciInv5A_FrogSociety = 26
gciInv5AgentsEnd = 26

gciInv5QuartzStart = 27
gciInv5A_Notes = 27
gciInv5A_PhotoCrater = 28
gciInv5A_Chamber = 29
gciInv5A_KanePhoto = 30
gciInv5A_JaniceCandacePhoto = 31
gciInv5A_BrokenStorageTube = 32
gciInv5A_StorageTube = 33
gciInv5QuartzEnd = 33

gciInv5StorageStart = 34
gciInv5A_OldSodaRecording = 34
gciInv5A_ComputerParts = 35
gciInv5A_PhotoKaneAdam = 36
gciInv5A_MessyFileCabinet = 37
gciInv5A_DirtyMattress = 38
gciInv5A_Grafitti = 39
gciInv5StorageEnd = 39

gciInv5A_ToQuartz = 40
gciInv5A_ToTwins = 41
gciInv5A_ToStorage = 42
gciInv5A_ToAgents = 43
gciInv5A_ToLobby = 44
gciInv5AAllObjectEnd = 44

gciInv5A_Variable_Total = 44

--Position Data.
gczaInv5A_List = {}

gczaInv5A_List[gciInv5A_Bret] = "Bret"
gczaInv5A_List[gciInv5A_Mari] = "Mari"
gczaInv5A_List[gciInv5A_Stacy] = "Stacy"
gczaInv5A_List[gciInv5A_Michelle] = "Michelle"
gczaInv5A_List[gciInv5A_Laura] = "Laura"
gczaInv5A_List[gciInv5A_Todd] = "Todd"

gczaInv5A_List[gciInv5A_Reception] = "ReceptionDesk"
gczaInv5A_List[gciInv5A_GeorgeFosterPortrait] = "GeorgeFosterPortrait"
gczaInv5A_List[gciInv5A_BiminiRecording] = "BiminiRecording"
gczaInv5A_List[gciInv5A_Mural] = "Mural"

gczaInv5A_List[gciInv5A_TankMaps] = "TankMaps"
gczaInv5A_List[gciInv5A_PhotoPlayer] = "PhotoPlayer"
gczaInv5A_List[gciInv5A_Photo34] = "Photo34"
gczaInv5A_List[gciInv5A_Photo56] = "Photo56"
gczaInv5A_List[gciInv5A_Photo78] = "Photo78"
gczaInv5A_List[gciInv5A_Photo910] = "Photo910"
gczaInv5A_List[gciInv5A_DoubleStorageTubes] = "DoubleStorageTubes"
gczaInv5A_List[gciInv5A_KaneKola] = "KaneKola"
gczaInv5A_List[gciInv5A_DoubleSecureTable] = "DoubleSecureTable"
gczaInv5A_List[gciInv5A_HeavyMachinery] = "HeavyMachinery"

gczaInv5A_List[gciInv5A_Paperwork] = "Paperwork"
gczaInv5A_List[gciInv5A_Personnel] = "Personnel"
gczaInv5A_List[gciInv5A_EiletteFile] = "EiletteFile"
gczaInv5A_List[gciInv5A_CandaceFoster] = "CandaceFoster"
gczaInv5A_List[gciInv5A_CoffeeCup] = "CoffeeCup"
gczaInv5A_List[gciInv5A_FrogSociety] = "FrogSociety"

gczaInv5A_List[gciInv5A_Notes] = "Notes"
gczaInv5A_List[gciInv5A_PhotoCrater] = "PhotoCrater"
gczaInv5A_List[gciInv5A_Chamber] = "Chamber"
gczaInv5A_List[gciInv5A_KanePhoto] = "KanePhoto"
gczaInv5A_List[gciInv5A_JaniceCandacePhoto] = "JaniceCandacePhoto"
gczaInv5A_List[gciInv5A_BrokenStorageTube] = "BrokenStorageTube"
gczaInv5A_List[gciInv5A_StorageTube] = "StorageTube"

gczaInv5A_List[gciInv5A_OldSodaRecording] = "OldSodaRecording"
gczaInv5A_List[gciInv5A_ComputerParts] = "ComputerParts"
gczaInv5A_List[gciInv5A_PhotoKaneAdam] = "PhotoKaneAdam"
gczaInv5A_List[gciInv5A_MessyFileCabinet] = "MessyFileCabinet"
gczaInv5A_List[gciInv5A_DirtyMattress] = "DirtyMattress"
gczaInv5A_List[gciInv5A_Grafitti] = "Grafitti"

gczaInv5A_List[gciInv5A_ToQuartz] = "ToQuartz"
gczaInv5A_List[gciInv5A_ToTwins] = "ToTwins"
gczaInv5A_List[gciInv5A_ToStorage] = "ToStorage"
gczaInv5A_List[gciInv5A_ToAgents] = "ToAgents"
gczaInv5A_List[gciInv5A_ToLobby] = "ToLobby"

-- |[Backgrounds]|
--List of backgrounds.
SLF_Open(fnResolvePath() .. "../../Datafiles/Backgrounds.slf")
local saList = {"Classroom", "Thunderstorm", "Cliff", "Forest", "Hallway", "LivingRoom", "LivingRoomNight", "Burning", "Library", "Lab", "SchoolExterior", "Cave", "QuartzBubble", "MineLobby", "MineQuartzRoom", "MineTwinRoom", "MineStorage", "MineAgentRoom", "MineLobby", "MuseumOffice", "Museum", "Gate", "RunHome", "SchoolExteriorGrad", "Gym"}

for i = 1, #saList, 1 do
    DL_ExtractBitmap(saList[i], "Root/Images/Backgrounds/All/" .. saList[i])
end
SLF_Close()
