--[Exec First]
--Quartz! Everywhere!

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--[Execution]
--Start.
fnDialogue("Thought: It's as though I'm connected to the thoughts and feelings of everyone, at once.")
fnDialogue("Thought: It feels... wonderful.")
fnDialogue("Thought: [P][GLITCH][ITALICS]... A journey the likes of which no one has ever undertaken before!..[GLITCH][ENDITALICS] ")
fnDialogue("Thought: [P][GLITCH][ITALICS]... The most beautiful crystals I've ever seen...[GLITCH][ENDITALICS] ")
fnDialogue("Thought: [P][GLITCH][ITALICS].. Have to keep everyone safe. My friends are the most important thing to me in the world...[GLITCH][ENDITALICS] ")
fnDialogue("Thought: [P][GLITCH][ITALICS]... Everything is gonna be okay...[GLITCH][ENDITALICS] ")

local sBuddy = VM_GetVar("Root/Variables/System/Romance/sBuddy", "S")
if(sBuddy == "Mari") then
    fnDialogue("Thought: As the motion inside me swells, I feel drawn to Mari.")
    fnDialogue("Thought: To my surprise, she's looking right back at me. Can she feel... what I'm feeling?")
    fnDialogue("[CHARENTER|Mari]Mari: [E|Sad][PLAYERNAME]... I'm sorry I don't always know what the right thing to do is.")
    fnDialogue("Mari: Sometimes I just make stuff up, because I don't want you to be hurt like I was. [P]I'm trying, I really am.")
    fnDialogue("Mari: I just[P]... well, I'm trying.")
    fnDialogue("Thought: Did she say that out loud? [P]Her lips weren't really moving.")
    fnDialogue("Thought: But as our eyes meet, I get the sense that the thoughts somehow were understood.")
    fnDialogue("Thought: I hope she hears my response.")
    fnDialogue("Player: Mari... I appreciate everything you've done for me.")
    fnDialogue("Player: I hope you know it's more than enough.[P] I promise if we get out of this... no, [ITALICS]when[ENDITALICS] we get out of this,")
    fnDialogue("Player: I'll tell you everything.[P] I'll tell you how much you mean to me.")
    fnDialogue("Player: I... like you.")

elseif(sBuddy == "Stacy") then
    fnDialogue("Thought: As the motion inside me swells, I feel drawn to Stacy.")
    fnDialogue("Thought: To my surprise, she's looking right back at me. Can she feel... what I'm feeling?")
    fnDialogue("[CHARENTER|Stacy]Stacy: [E|Sad][PLAYERNAME]... don't look at me like that.")
    fnDialogue("Stacy: [E|Mad]I'm not your hero.[P][E|Sad] I'm scared all the time.")
    fnDialogue("Stacy: Someday, I'm going to fail the people who depend on me.[P] And I couldn't stand to fail...[P] the person I care about most.")
    fnDialogue("Thought: My heart is hammering.[P] Did she say that out loud? Her lips weren't really moving.")
    fnDialogue("Thought: But as our eyes meet, I get the sense that the thoughts somehow were understood.[P] I hope she hears my response.")
    fnDialogue("Player: Stacy.[P] You don't have to be anything other than honest with me.")
    fnDialogue("Player: I want to be there for you as much as you've been for me. If we get out of here... no, when.")
    fnDialogue("Player: I'll tell you how much you've meant.[P] I... like you.")

elseif(sBuddy == "Bret") then
    fnDialogue("Thought: As the motion inside me swells, I feel drawn to Bret.")
    fnDialogue("Thought: To my surprise, he's looking right back at me. Can he feel...[P]what I'm feeling?")
    fnDialogue("[CHARENTER|Bret]Bret: [E|Blush][PLAYERNAME]... I've never felt more excited to be around a person in my life.")
    fnDialogue("Bret: [E|Sad]I'm trying not to be desperate, I'm trying not to be overbearing.[P] [E|Blush]Because...")
    fnDialogue("Bret: [E|Sad]... What if you don't like me that way?")
    fnDialogue("Bret: [E|Mad]What if I'm being a big idiot all over again and ruining the best friendship I've ever had?")
    fnDialogue("Thought: My heart is hammering.[P] Did he say that out loud? His lips weren't really moving.")
    fnDialogue("Thought: But as our eyes meet, I get the sense that the thoughts somehow were understood.")
    fnDialogue("Thought: I hope somehow, Bret can hear my response.")
    fnDialogue("Player: Stop worrying about being yourself, you dope.")
    fnDialogue("Player: I know you've always been self conscious about making friends, and you couldn't be more wrong.")
    fnDialogue("Player: If we get out of here... no, when.[P] I'll tell you how much you mean to everyone.")
    fnDialogue("Player: I'll tell you how much I...[P] like you.")

elseif(sBuddy == "Michelle") then
    fnDialogue("Thought: As the motion inside me swells, I feel drawn to Michelle.")
    fnDialogue("Thought: To my surprise, she's looking right back at me. Can she feel... what I'm feeling?")
    fnDialogue("[CHARENTER|Michelle]Michelle: [E|Mad]You damn weirdo.[P] Why are you always hanging around?")
    fnDialogue("Michelle: [E|Sad]Can't you see... everyone who gets close to me regrets it eventually.")
    fnDialogue("Michelle: [E|Blush]Even this dumb club will probably do it someday.[P][E|Mad] Just... just leave me alone!")
    fnDialogue("Michelle: [E|Sad]I don't ever want to hear you say[P]... you're finally sick being my friend.")
    fnDialogue("Thought: My heart is hammering. [P]Did she say that out loud? Her lips weren't really moving.")
    fnDialogue("Thought: But as our eyes meet, I get the sense that the thoughts somehow were understood.")
    fnDialogue("Thought: I hope she hears my response.")
    fnDialogue("Player: Michelle.[P] I am... continually frustrated by your efforts to push me away!")
    fnDialogue("Player: But you're just plain wrong if you think it's going to work!")
    fnDialogue("Player: Like it or not, you're my friend--[P]more than that, you're a good friend.")
    fnDialogue("Player: If we get out of here... no, when.[P] I'll tell you how much I...[P]like you.")

elseif(sBuddy == "Laura") then
    fnDialogue("Thought: As the motion inside me swells, I feel drawn to Laura.")
    fnDialogue("Thought: To my surprise, she's looking right back at me. Can she feel... what I'm feeling?")
    fnDialogue("[CHARENTER|Laura]Laura: [E|Mad][PLAYERNAME]... Wh-why are you looking at me like that?[P][E|Sad] Oh, I feel all dizzy inside.")
    fnDialogue("Laura: [E|Blush]Every time I think of our friendship, and how great it's been--[P][E|Mad]oh I could kick myself!")
    fnDialogue("Laura: Here I was, blathering on about all my crushes and nonsense, and I never even thought about being there for you.")
    fnDialogue("Laura: [E|Sad]I've ruined everything, haven't I?")
    fnDialogue("Laura: [E|Blush]Why didn't I just say from the beginning... how much you've meant to me?")
    fnDialogue("Thought: My heart is hammering.[P] Did she say that out loud? Her lips weren't really moving.")
    fnDialogue("Thought: But as our eyes meet, I get the sense that the thoughts somehow were understood.[P] I hope she hears my response.")
    fnDialogue("Player: Laura.[P] It's not too late!")
    fnDialogue("Player: If we get out of here... no, when.[P] I'll tell you everything.[P] I... like you.")

elseif(sBuddy == "Todd") then
    fnDialogue("Thought: As the motion inside me swells, I feel drawn to Todd.")
    fnDialogue("Thought: To my surprise, he's looking right back at me.[P] Can he feel... what I'm feeling?")
    fnDialogue("[CHARENTER|Todd]Todd: [E|Sad][PLAYERNAME]... I'm sorry I can't be there for you when you need me.")
    fnDialogue("Todd: [E|Shock]Damnit![P][E|Mad] I want to, I really do, but...[P][E|Sad]this stuff is really scary.")
    fnDialogue("Todd: [E|Shock]I don't know what to do! I don't know if I can even help you!")
    fnDialogue("Todd: You're...[P][E|Sad]better off with someone you can count on. ")
    fnDialogue("Thought: My heart is hammering.[P] Did he say that out loud? His lips weren't really moving.")
    fnDialogue("Thought: But as our eyes meet, I get the sense that the thoughts somehow were understood.[P] I feel so heavy, now that I know the truth.")
    fnDialogue("Thought: Maybe it's time to take some of the weight off.")
    fnDialogue("Player: I [P][P]owe you an apology, Todd[BR]. You said it yourself, a relationship is a give and take.[P] And I've been taking a lot.")
    fnDialogue("Player: I can't really be your friend by demanding you care for me. [P]And I don't want you to take care of me... I want to be your equal.")
    fnDialogue("Player: If we get out of here... no, when.[P] I'll make sure you know how I feel.")


end

fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"QuartzFromTheOffice\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
PairanormalLevel_FlagCheckpoint("QuartzFromTheOffice")

fnDialogue("[HIDEALL][BG|Null]Thought: [P][P][P].[P][P][P].[P][P][P].")
fnDialogue("Stacy: [P][P][P][VOICE|Voice|Stacy]Hey guys![P] I found the exit!")
fnDialogue("Thought: Well.[P][MUSIC|None] I take a deep breath.")
fnDialogue("Player: Okay.[P][P][P] Let's go.")
fnDialogue("Thought:...")
fnDialogue("[BG|Root/Images/Backgrounds/All/MineLobby]Thought:...")
fnDialogue("Info: [SFX|World|Investigation]Final investigation -- The Mining Facility")
fnDialogue("[CHARENTER|Mari]Mari: [E|Shock]Well.[P] It's... pretty big in here.")
fnDialogue("Thought: Nobody has a lighthearted response, which makes me feel immediately queasy. ")
fnDialogue("[CHAREXIT|Mari][CHARENTER|Michelle]Michelle: [E|N]Just to clarify. What exactly are we looking for in here again?")
fnDialogue("[HIDEALL]Player: The truth about the Foster Family.[P] The truth about this town.")
fnDialogue("Player: I guess also, the truth about...[P]me?")
fnDialogue("Thought: Somebody gives my hand a quick squeeze, but before I can see who it is, the club members have filed away quietly on their noble mission.")
fnDialogue("Thought: I feel myself being tugged along by my own morbid curiosity, as well.")
fnDialogue("Player: *Sigh*.[P] Let's do this, [PLAYERNAME].")
fnDialogue("Info: [MUSIC|Jazzy]Doors that are red lead to other rooms.")
fnDialogue("Info: Investigate what you can and get out quickly!")


--[Loading the Game]
--If currently loading the game, set up the investigation.
if(PairanormalLevel_IsLoading() == true) then
    
    --Clear checkpoint.
    PairanormalLevel_FlagCheckpoint("InvestigationMine")
    
    --Variables.
    local sBGPath = "Root/Images/Backgrounds/All/MineLobby"
    local sScriptPath = gsRoot .. "../Chapter 5/Gameplay_SceneB/0 - ExamineObject.lua"

    --Change the background.
    PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/MineLobby")
    PairanormalLevel_SetBackground("Root/Images/Backgrounds/All/MineLobby")

    --Boot investigation scripts.
    Dia_SetProperty("Clear Investigation Characters")
    Dia_SetProperty("Clear Investigation")
    Dia_SetProperty("Start Investigation") 
    Dia_SetProperty("Set Investigation Image", "Root/Images/Backgrounds/All/MineLobby")
    Dia_SetProperty("Set Investigation Script", gsRoot .. "../Chapter 5/Gameplay_SceneB/0 - ExamineObject.lua")
    Dia_SetProperty("Set Go Home Strings", gsRoot .. "../Chapter 5/Gameplay_SceneC/7 - PatchGoingHome.lua", "Default")
    
    --Talkable Characters. Only Mari is in the lobby.
	Dia_SetProperty("Add Talkable Character", "Mari",     gsRoot .. "../Chapter 5/Gameplay_SceneB/1 - Present Mari.lua")
	--Dia_SetProperty("Add Talkable Character", "Stacy",    gsRoot .. "../Chapter 5/Gameplay_SceneB/2 - Present Stacy.lua")
	--Dia_SetProperty("Add Talkable Character", "Bret",     gsRoot .. "../Chapter 5/Gameplay_SceneB/3 - Present Bret.lua")
    --Dia_SetProperty("Add Talkable Character", "Laura",    gsRoot .. "../Chapter 5/Gameplay_SceneB/4 - Present Laura.lua")
	--Dia_SetProperty("Add Talkable Character", "Michelle", gsRoot .. "../Chapter 5/Gameplay_SceneB/5 - Present Michelle.lua")
	--Dia_SetProperty("Add Talkable Character", "Todd",     gsRoot .. "../Chapter 5/Gameplay_SceneB/6 - Present Todd.lua")
    
    --Talkable characters as inventory objects.
    local sCharPath = "Root/Images/Backgrounds/All/CharPortraits"
	Dia_SetProperty("Add Discovered Object", "Mari",     sCharPath, sScriptPath, "Mari",     1322,   2, 1322 + 438,   2 + 363)
	Dia_SetProperty("Add Discovered Object", "Stacy",    sCharPath, sScriptPath, "Stacy",     442, 367,  442 + 438, 367 + 363)
	Dia_SetProperty("Add Discovered Object", "Bret",     sCharPath, sScriptPath, "Bret",        2,   2,    2 + 438,   2 + 363)
    Dia_SetProperty("Add Discovered Object", "Laura",    sCharPath, sScriptPath, "Laura",     882,   2,  882 + 438,   2 + 363)
	Dia_SetProperty("Add Discovered Object", "Michelle", sCharPath, sScriptPath, "Michelle", 1762,   2, 1762 + 438,   2 + 363)
    
    --Todd only appears if you helped with the gym in chapter 4.
    local iDecoratedGym = VM_GetVar("Root/Variables/ChapterVars/Chapter4/iDecoratedGym", "N")
    if(iDecoratedGym == 1.0) then
        Dia_SetProperty("Add Discovered Object", "Todd",     sCharPath, sScriptPath, "Todd",        2, 367,    2 + 438, 367 + 363)
    end
    Dia_SetProperty("Clear Discovered Object")
    
    --SLF file adds the examinable objects. All of the SLFs need to be parsed! This is because objects in different rooms 
    -- need to have their coordinate data passed in when re-setting their discovered objects.
    Dia_SetProperty("Parse SLF File", gsRoot .. "../Chapter 5/MapData/InvestigationAA.slf")
    Dia_SetProperty("Parse SLF File", gsRoot .. "../Chapter 5/MapData/InvestigationAB.slf")
    Dia_SetProperty("Parse SLF File", gsRoot .. "../Chapter 5/MapData/InvestigationAC.slf")
    Dia_SetProperty("Parse SLF File", gsRoot .. "../Chapter 5/MapData/InvestigationAD.slf")
    Dia_SetProperty("Parse SLF File", gsRoot .. "../Chapter 5/MapData/InvestigationAE.slf")
    
    --Check all the items and re-add them to the inventory. We only check the lobby here.
    for i = gciInv5LobbyStart, gciInv5LobbyEnd, 1 do
        
        --Variable name.
        local sVarName = "Root/Variables/Chapter5/InvestigationA/iObject|" .. gczaInv5A_List[i]
        local iHasItem = VM_GetVar(sVarName, "N")
    
        --BG path is resolved dynamically!
        if(i >= gciInv5LobbyStart and i <= gciInv5LobbyEnd) then
            sBGPath = "Root/Images/Backgrounds/All/MineLobby"
        elseif(i >= gciInv5TwinsStart and i <= gciInv5TwinsEnd) then
            sBGPath = "Root/Images/Backgrounds/All/MineTwinRoom"
        elseif(i >= gciInv5AgentsStart and i <= gciInv5AgentsEnd) then
            sBGPath = "Root/Images/Backgrounds/All/MineAgentRoom"
        elseif(i >= gciInv5QuartzStart and i <= gciInv5QuartzEnd) then
            sBGPath = "Root/Images/Backgrounds/All/MineQuartzRoom"
        elseif(i >= gciInv5StorageStart and i <= gciInv5StorageEnd) then
            sBGPath = "Root/Images/Backgrounds/All/MineStorage"
        else
            sBGPath = "Root/Images/Backgrounds/All/MineLobby"
        end
        
        --Has the item. Add it.
        if(iHasItem == 1.0) then
            Dia_SetProperty("Add Discovered Object", gczaInv5A_List[i], sBGPath, sScriptPath, gczaInv5A_List[i])
        end
        Dia_SetProperty("Clear Discovered Object")
    end

    --Now clear the objects are re-parse the first investigation.
    Dia_SetProperty("Clear Investigation Objects")
    Dia_SetProperty("Parse SLF File", gsRoot .. "../Chapter 5/MapData/InvestigationAA.slf")

    --Modify the time max/left.
    VM_SetVar("Root/Variables/System/Investigation/sInvestigationPath", "S", "Root/Variables/Chapter5/Investigation/")
    local iTimeMax  = VM_GetVar("Root/Variables/Chapter5/Investigation/iTimeMax",  "N")
    local iTimeLeft = VM_GetVar("Root/Variables/Chapter5/Investigation/iTimeLeft", "N")
    Dia_SetProperty("Investigation Time Max",  iTimeMax)
    Dia_SetProperty("Investigation Time Left", iTimeLeft)

--[Normal Operations]
--Otherwise, flag the game to save and boot the investigation normally.
else
    --Save the game.
    fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"InvestigationMine\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")

    --Set the time max/left.
    fnCutsceneInstruction([[ VM_SetVar("Root/Variables/Chapter5/Investigation/iTimeMax",  "N", 60.0) ]])
    fnCutsceneInstruction([[ VM_SetVar("Root/Variables/Chapter5/Investigation/iTimeLeft", "N", 60.0) ]])

    --Boot investigation scripts.
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Investigation Characters") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Start Investigation") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Investigation Time Max", 60) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Investigation Time Left", 60) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Set Investigation Image", "Root/Images/Backgrounds/All/MineLobby") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Set Investigation Script", gsRoot .. "../Chapter 5/Gameplay_SceneB/0 - ExamineObject.lua") ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Set Go Home Strings", gsRoot .. "../Chapter 5/Gameplay_SceneC/7 - PatchGoingHome.lua", "Default") ]])

    --Talkable characters. Only Mari is in the lobby.
	fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Mari",     gsRoot .. "../Chapter 5/Gameplay_SceneB/1 - Present Mari.lua") ]])
	--fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Stacy",    gsRoot .. "../Chapter 5/Gameplay_SceneB/2 - Present Stacy.lua") ]])
	--fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Bret",     gsRoot .. "../Chapter 5/Gameplay_SceneB/3 - Present Bret.lua") ]])
    --fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Laura",    gsRoot .. "../Chapter 5/Gameplay_SceneB/4 - Present Laura.lua") ]])
	--fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Michelle", gsRoot .. "../Chapter 5/Gameplay_SceneB/5 - Present Michelle.lua") ]])
	--fnCutsceneInstruction([[ Dia_SetProperty("Add Talkable Character", "Todd",     gsRoot .. "../Chapter 5/Gameplay_SceneB/6 - Present Todd.lua") ]])
    
    --SLF file adds the examinable objects.
    fnCutsceneInstruction([[ Dia_SetProperty("Parse SLF File", gsRoot .. "../Chapter 5/MapData/InvestigationAA.slf") ]])

    --Add the talkable characters as the first examine objects.
	fnCutsceneInstruction([[ Dia_SetProperty("Add Discovered Object", "Mari",     "Root/Images/Backgrounds/All/CharPortraits", gsRoot .. "../Chapter 5/Gameplay_SceneB/0 - ExamineObject.lua", "Mari",     1322,   2, 1322 + 438,   2 + 363) ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Add Discovered Object", "Stacy",    "Root/Images/Backgrounds/All/CharPortraits", gsRoot .. "../Chapter 5/Gameplay_SceneB/0 - ExamineObject.lua", "Stacy",     442, 367,  442 + 438, 367 + 363) ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Add Discovered Object", "Bret",     "Root/Images/Backgrounds/All/CharPortraits", gsRoot .. "../Chapter 5/Gameplay_SceneB/0 - ExamineObject.lua", "Bret",        2,   2,    2 + 438,   2 + 363) ]])
    fnCutsceneInstruction([[ Dia_SetProperty("Add Discovered Object", "Laura",    "Root/Images/Backgrounds/All/CharPortraits", gsRoot .. "../Chapter 5/Gameplay_SceneB/0 - ExamineObject.lua", "Laura",     882,   2,  882 + 438,   2 + 363) ]])
	fnCutsceneInstruction([[ Dia_SetProperty("Add Discovered Object", "Michelle", "Root/Images/Backgrounds/All/CharPortraits", gsRoot .. "../Chapter 5/Gameplay_SceneB/0 - ExamineObject.lua", "Michelle", 1762,   2, 1762 + 438,   2 + 363) ]])
    
    if(iDecoratedGym == 1.0) then
        fnCutsceneInstruction([[ Dia_SetProperty("Add Discovered Object", "Todd",     "Root/Images/Backgrounds/All/CharPortraits", gsRoot .. "../Chapter 5/Gameplay_SceneB/0 - ExamineObject.lua", "Todd",        2, 367,    2 + 438, 367 + 363) ]])
    end
	
    fnCutsceneInstruction([[ Dia_SetProperty("Clear Discovered Object") ]])

    fnCutsceneBlocker()
    
    --Time variable.
    VM_SetVar("Root/Variables/System/Investigation/sInvestigationPath", "S", "Root/Variables/Chapter5/Investigation/")
    
end

PairanormalLevel_FlagCheckpoint("InvestigationMine")
fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"InvestigationMine\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")