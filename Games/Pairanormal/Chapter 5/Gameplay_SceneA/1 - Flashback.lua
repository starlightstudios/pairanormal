--[Exec First]
--Executes when the program starts. This will occur with the argument "Start".

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--[Execution]
--Start.
if(sTopicName == "Start") then
	
	--Save the game.
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Flash0\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
	PairanormalLevel_FlagCheckpoint("Flash0")
    
    --Dialogue.
    fnDialogue("[HIDEALL][BG|Root/Images/Backgrounds/All/LivingRoomNight][MUSIC|Jazzy]Thought: It's the morning after the [ITALICS]incident[ENDITALICS].[P] I stumble out of bed.")
    fnDialogue("Thought: Somehow, I find myself seated on the living room couch, a blanket wrapped around my shoulders.")
    fnDialogue("Thought: For a little while, the previous day's events don't stir my memory.[P] But I feel activity around me, and the sunshine starting to warm my fingertips.")
    fnDialogue("Thought: When I come to my senses, [CHARENTER|Reich][CHARENTER|Leif][FOCUSMOD|Leif|true][FOCUSMOD|Reich|true][EMOTION|Leif|Sad][EMOTION|Reich|Sad]I realize Rech and Leif are seated beside me, wearing two perfectly identical worried expressions.")
    fnDialogue("Player: Oh.[P] Hey, guys.")
    fnDialogue("[CHARENTER|AJ]Dog: It is dog, for making you fel better.")
    fnDialogue("Thought: Eilette emerges from the kitchen, where I think she was carrying on a conversation with someone.[P] Two others are with her.")
    fnDialogue("[CHARENTER|Eilette][CHARENTER|Three][CHARENTER|Six]Eilette: [E|N]Excellent work, agents.[P] Thank you for securing [PLAYERNAME] before it was too late.")
    fnDialogue("Eilette: [PLAYERNAME], meet our additional operatives.[P] This is Three, and this is Five and Six.")
    fnDialogue("Serena: [E|N]I prefer to be called Serena.")
    fnDialogue("Six: [SFX|Mask][E|N]Ah, it's all [P]*huff*[P] part of the job...")
    fnDialogue("Eilette: [E|Shock]How are you feeling today?")
    fnDialogue("Thought: I pull the blanket tighter around me.[P] Pretty sure there aren't any words to describe it.")
    fnDialogue("Player: I just want...[P] someone to tell me the truth.")
    fnDialogue("Six: [E|Happy]join the *huf* club, kid *huf* EHYE EHYE EHYE.")
    fnDialogue("Player: H-[P][P]how can you laugh at that?")
    fnDialogue("Player: [CRASH]I WAS ATTACKED![P] SOMEBODY TRIED TO KIDNAP ME![P] If they had taken me--[GLITCH]I-I---")
    fnDialogue("[HIDEALL][MUSIC|Null]Thought: I shut my [GLITCH]eyes tight[BG|Null] and am wracked with a new terrifying sensation.[P] My[BG|Root/Images/Backgrounds/All/Burning][GLITCH][GLITCH] bones shake uncontrollably and my vision blurs.")
    fnDialogue("Thought: I... want to run for a thousand[GLITCH][GLITCH] miles.[P] I'm about to explode[GLITCH][GLITCHTEXT][BG|Null][BG|Root/Images/Backgrounds/All/Burning] I want to explode out of my skin.[ENDGLITCHTEXT] out of [GLITCH]my skin.")
	
	--Save the game.
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Flash1\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
	PairanormalLevel_FlagCheckpoint("Flash1")
    
    fnDialogue("[BG|Null]Player: [SPEEDTEXT][GLITCH][GLITCHTEXT] I WANT TO[P] RIP MY HAIR OUT AND EAT IT. IM TRAPPED IN MY BODY WITH[GLITCH][ENDGLITCHTEXT]some[P]bod")
    fnDialogue("Player: y hel[GLITCH][GLITCHTEXT] NO ESCAPE.[P] NO END TO THE AGONY.[P][ENDGLITCHTEXT][ENDSPEEDTEXT] ")
    fnDialogue("Thought: My shoulders are grabbed and pushed back and forth.[P] Words somehow penetrate the panic, and I am obliged to follow their instructions.")
    fnDialogue("Player:I...[P][GLITCH]I...[P][GLITCH]I...")
    fnDialogue("???: [VOICE|Voice|Eilette]Breathe, [PLAYERNAME], breathe.[P] In and out. In,[P] and out.[P] Yes. There you go.")
    fnDialogue("[BG|Root/Images/Backgrounds/All/LivingRoom][CHARENTER|Eilette]Thought: [P][P][P][P]Eilette's hands clench my shivering frame tightly.[P] It's kind of uncomfortable, but I don't want to shake her free.")
    fnDialogue("Thought: [P][P][P][P]The veins of her jaw pulse under the dim overhead lights.")
    fnDialogue("Eilette: [E|N]You[P] are safe, [PLAYERNAME]. We are all here to protect you.[P] We are here to ensure nothing will ever do you harm. ")
    fnDialogue("Thought: I can barely stand to meet my caretaker's eye.[P] She looks much like she did that day in the woods, trying to teach me to walk.")
    fnDialogue("Thought: Kind of dumbfounded, and a little bit ashamed. [ITALICS]How can this infernal woman be so stupid?[ENDITALICS] ")
    fnDialogue("Eilette: [E|N]As your caretaker, it is my duty to--")
    fnDialogue("[MUSIC|Somber]Thought: Before I know it, I sink forward, wrapping my arms desperately around her sharp torso. ")
    fnDialogue("[CHARENTER|Six][CHARENTER|Reich][CHARENTER|Three][CHARENTER|Leif]Leif: [E|Shock]Whoah!")
    fnDialogue("Reich: [E|Mad]No fair, we never get to hug the boss--")
    fnDialogue("Serena: [E|Mad]Shut up, you two!")
    fnDialogue("Thought: [FOCUS|Eilette]The lining of her jacket is as scratchy and stiff as it looks, but I just can't let go.")
    fnDialogue("Thought: After a little while, a gloved leather hand pats the top of my head.")
    fnDialogue("Eilette: [E|Blush][PLAYERNAME],[P][P] I am sorry. ")
    fnDialogue("Player: ...")
    fnDialogue("Eilette: [E|Blush]I am sorry for what has befallen you.[P] You deserve to hear the truth about this town, and decide your future for yourself.")
    fnDialogue("Player: See...*sniffle*.[P] That wasn't so hard, was it?")
    fnDialogue("Eilette: [E|Sad]Listen closely.")
    fnDialogue("[CHARENTER|AJ]Thought: I retreat back into my blanket, and the dog gives my toe a reassuring lick.")
    fnDialogue("Thought: I shrug halfheartedly, and Eilette begins.")
    fnDialogue("Eilette: [E|N]Everyone in this room is a part of my organization.")
    fnDialogue("Eilette: Reich and Leif are the ground team.[P] Five and Six are intelligence and coordination.[P] And Three--er, Serena...")
    fnDialogue("Serena: [E|N]I'm the only one on this team willing to get her hands dirty.")
    fnDialogue("Eilette: [E|Shock]That's one way to put it. [P]This team is without name--")
    fnDialogue("Twins: [FOCUS|Reich][EMOTION|Reich|Happy][EMOTION|Leif|Happy][FOCUSMOD|Leif|true][VOICE|Voice|Reich]THE FROG SOCIETY--")
    fnDialogue("Eilette: [E|Mad]NO, for the last time. ")
    fnDialogue("Twins: [EMOTION|Reich|Sad][EMOTION|Leif|Sad][FOCUS|Reich][FOCUSMOD|Leif|true][VOICE|Voice|Reich]Awww...")
    fnDialogue("Eilette: [E|Mad][ITALICS]As I was saying,[ENDITALICS] we don't have a name-- only a mission. To oppose the Foster Family empire.")
    fnDialogue("Serena: [E|N]It [ITALICS]should[ENDITALICS] be to burn this town to the ground, if you ask me.")
    fnDialogue("Eilette: I did not.")
	fnDialogue("Eilette: [E|N]Given our... past actions towards the Foster Family, being put in our care like you have has endangered you, [PLAYERNAME]. ")
	fnDialogue("Eilette: Anyone associated with us can be considered a threat to them. ")
    fnDialogue("Player: But the Foster Family practically built this town. Like,[P] the school and the parks and library and stuff.")
    fnDialogue("Player: ... Why are you trying to destroy all of that?")
    fnDialogue("Eilette: [E|Mad]The reason is something you have to see for yourself to understand.[P][E|Sad] Unfortunately, it's not something I can just show you.")
    fnDialogue("Thought: She circles the living room decisively. I can't see her stewing over what to do next, but somehow I feel it.")
    fnDialogue("Eilette: [E|N]At present, you are in a state of shock.[P] Take some time off from school.[P] Reich and Leif will attend to you.")
    fnDialogue("Player: But I--")
    fnDialogue("Eilette: [E|N]You'll do nothing else tomorrow and the weekend but rest.[P] Do a little excercise.[P] Play with the dog.")
    fnDialogue("Dog: [E|Happy]Dog decide. this is excellent plan.")
    fnDialogue("Player: I have homework, you know--")
    fnDialogue("Thought: [FOCUS|Eilette]Eilette snaps her fingers, and Six nods.[P] [ITALICS]It's been taken care of[ENDITALICS].[P] I scowl.")
    fnDialogue("Thought: My homework is probably easy peasy for all of these adults, and normally I'd be happy to pass it off, but...")
    fnDialogue("Thought: It's the one thing that might've distracted me from all the worrisome thoughts.")
    fnDialogue("Eilette: [E|N]When you're ready, I'll give you...[P]*sigh*")
    fnDialogue("Eilette: [E|Sad] ... An assignment.")
    fnDialogue("Player: [SFX|World|Blink]!")
    fnDialogue("Eilette: Oh, and one more thing--")
    
	--Save the game.
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Assignment\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
	PairanormalLevel_FlagCheckpoint("Assignment")
    
    
    fnDialogue("[HIDEALL][BG|Root/Images/Backgrounds/All/Classroom]Thought: ...")
    fnDialogue([[ [MUSIC|Jazzy][CHARENTER|Mari][CHARENTER|Stacy][CHARENTER|Bret][CHARENTER|Laura][CHARENTER|Michelle]Player: --she said to \"bring your extracurricular group along\".[P] That means you guys, right? ]])
    fnDialogue("Mari: [CRASH][E|Shock]YES!!! YES, ABSOLUTELY WE'LL COME.")
    fnDialogue("Stacy: [E|Mad]An investigation the likes of which our club has NEVER undertaken before.[P] Hell.[P] Yeah.")
    fnDialogue("Laura: [E|Happy]Erm... [P][SFX|Ugh][E|Blush]bring us along [ITALICS]where[ENDITALICS], exactly?")
    fnDialogue("Player: I was hoping you guys would know...[P] she said we need to explore a big old building near a crater or something? Somewhere an asteroid landed a long time ago.")
    fnDialogue("Thought: The classroom is quiet, considering this.[P] I look from one puzzled club member's face to another.")
	fnDialogue("Bret: [E|Sad] Well, there's only one crater that we know of in town--")
	fnDialogue("Mari: [E|Shock] THE CRATER THAT WAS CAUSED BY ALIENS!!![P][E|Happy] I KNEW IT!!")
	fnDialogue("Stacy: [E|Shock] [SFX|World|Blink]Oh, wait a minute.[P] Duh!!! They're talking about the old quartz mine![P] Has it been like an Area 54 this whole time, hiding weird alien life!?!")
    fnDialogue("Player: Okay, just to clarify, absolutely nothing was said about aliens.")
    fnDialogue("Mari: [E|Happy][CRASH]WE'RE STORMING AREA FIFTY ONEEEEEE YEAAAAAAHHHHH!!!!!")
    
	--Save the game.
	fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"MineTime\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
	PairanormalLevel_FlagCheckpoint("MineTime")
    
    LM_ExecuteScript(gsRoot .. "../Chapter 5/Gameplay_SceneA/2 - Mine Time.lua", "Start")

end
