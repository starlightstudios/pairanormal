--[Exec First]
--It's MINE TIME, BAYBEEEEE

--[Arg Resolve]
--Check for the expected argument count.
local iExpectedArguments = 1
if(fnArgCheck(1) == false) then return end

--Store arguments.
local sTopicName = LM_GetScriptArgument(0)

--[Execution]
--Start.
if(sTopicName == "Start") then
    fnDialogue("[HIDEALL][Music|Null][BG|Null]Thought: ...")
    fnDialogue("Thought: The club agreed to go with me after school and explore the abandoned quartz mine.")
    fnDialogue("Thought: The hard part now is figuring out how to pass the endless hours in class until then. ")
    
    --Buddy Sequence
    local sBuddy = VM_GetVar("Root/Variables/System/Romance/sBuddy", "S")
    local iMetAdam = VM_GetVar("Root/Variables/ChapterVars/Chapter4/iMetAdam", "N")
    local iMetMistress = VM_GetVar("Root/Variables/ChapterVars/Chapter3/iMetMistress", "N")
    
    --[If the buddy was Todd, we always get this scene]
    if(sBuddy == "Todd") then
        fnDialogue("[BG|Root/Images/Backgrounds/All/Classroom][MUSIC|Daytime2][CHARENTER|Ross]Ross: --proves that [ITALICS]Love Bites[ENDITALICS] is the greatest young adult novel of the 21st century.")
        fnDialogue("Thought: My foot bounces on the floor impatiently. [P]There's one person I can usually turn to when I feel lost and anxious.")
        fnDialogue("Thought: The trouble is, he's not turning around to face me right now.")
        fnDialogue("Thought: I stare helplessly at the back of Todd's head, hoping to get his attention somehow.[P] This is our one and only class together.")
        fnDialogue("[CHARENTER|Terry]Terry: [E|Happy]Mr. Buntir?[P] I respectfully object.")
        fnDialogue("[CHARENTER|Teacher1]Teacher: This isn't a courtroom, Miss Idego.[P] You can't object to someone's presentation.")
        fnDialogue("Teacher: Even if it doesn't have anything to do with the science project I assigned you all.")
        fnDialogue("[HIDEALL]Thought: I need to explain--I have[GLITCH][GLITCH] to make things right, after what's happened.[GLITCH] ")
        
        fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"ToddForgiveMe\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
        PairanormalLevel_FlagCheckpoint("ToddForgiveMe")
        
        fnDialogue("[HIDEALL][BG|Null][MUSIC|Null]Thought: ...")
        fnDialogue("[BG|Root/Images/Backgrounds/All/LivingRoom]Player: Eilette.[P] You're home early.")
        fnDialogue("[CHARENTER|Eilette]Eilette: [E|Happy]Yes. I finished today's tasks more efficiently than I expected.[P] I've also come to deliver some news.")
        fnDialogue("Player: What's wrong?")
        fnDialogue("Eilette: [E|N]Todd's mother has called.")
        fnDialogue("Eilette: He won't be available to tutor you anymore, I'm afraid.[P] Too many other activities on his plate.")
        fnDialogue("Player: ... T-todd is...")
        fnDialogue("Eilette: [E|Shock]Yes, I was surprised to hear it as well.")
        fnDialogue("Eilette: [E|Mad]I was certain that by offering such a generous consultation fee, he wouldn't dare try to quit. A budding mind needs stability, after all.")
        fnDialogue("Player: I dont...")
        fnDialogue("Eilette: [E|Sad]I see that you're upset by this.[P] I recommend that when you return to school, you should plan the route of your activities to intersect with his.")
        fnDialogue("Eilette: Perhaps your friendship can continue from there.")
        fnDialogue("Player: *sigh*[P] Why bother?[P] I know the real [GLITCH]reason he quit [GLITCH][GLITCH]tutoring me.")
        
        fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"OrNot\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
        PairanormalLevel_FlagCheckpoint("OrNot")
        
        fnDialogue("[HIDEALL][BG|Null]Thought: [GLITCH][ITALICS]... He never wants to speak to me again.[ENDITALICS] ")
        fnDialogue("[BG|Root/Images/Backgrounds/All/Classroom]Thought: I swallow hard.[P] But I can't deny that I want to speak to him. ")
        fnDialogue("[CHARENTER|Ross]Ross: [E|Blush]--can see in this diagram, the sexually charged relationship between Storme Ryder and Black Heartingale is a perfect metaphor for cell division.")
        fnDialogue("[CHARENTER|Terry]Terry: [E|Sad]Be that as it may,[P] [ITALICS]how[ENDITALICS] does that elevate Love Bites to the status of high art?[P] ")
        fnDialogue("[CHARENTER|Teacher1]Teacher: [SFX|World|Ugh]If you two had put just a [ITALICS]fraction[ENDITALICS] of the effort of this into anything else I've assigned to you...")
        fnDialogue("[HIDEALL]Thought: Maybe I was wrong, I'm being too hard on myself.[P] Todd and I were so close.")
        fnDialogue("Thought: Maybe he just hasn't had time to reach out to me. Didn't his mom say he was really busy?")
        fnDialogue("Thought: I look down at the notebook in front of me.[P][P][ITALICS] Ah, I know![ENDITALICS] ")
        fnDialogue("Thought: [ITALICS]I'll do this thing I saw in a movie once.[ENDITALICS] ")
        fnDialogue("[CHARENTER|Todd]Todd: [E|Shock][SFX|Blink]Ow!")
        fnDialogue("Thought: The balled up piece of paper I made doinks off Todd's head and he flinches.[P] [ITALICS]I didn't throw it that hard![ENDITALICS].")
        fnDialogue("Thought: But it does the job.[P] Todd picks up the piece of paper and reads it.[P] Then he turns to face me.")
        fnDialogue("Todd: [E|Sad][PLAYERNAME], look--")
        fnDialogue("[CHARENTER|Teacher1]Teacher: Something you want to share with the class, Todd? ")
        fnDialogue("Todd: [E|Shock]Wh--uh, no.[P] Nope, haha, nothing at all sir.")
        fnDialogue("Teacher: That's what I thought.")
        fnDialogue("Teacher: Now--[P]goodness, has it been one hour already?[P] The two of you were fighting over a presentation for [ITALICS]one hour[ENDITALICS]?")
        fnDialogue("[CHARENTER|Terry][CHARENTER|Ross]Terry: [E|Blush]Well yeah, we're very good at it.")
        fnDialogue("Teacher: *siiiiiigh*.[P] Presentations will finish up tomorrow.[P] The bell is bound to ring any minute now--")
        fnDialogue("[SFX|SchoolBell]Thought: [P]And so it does. ")
        fnDialogue("Thought: Students quickly file out of the classroom, including Todd.[P] So much for that.")
    
    --[ADAM Encounters]
    elseif(iMetAdam == 1.0) then
        fnDialogue("[MUSIC|DaytimeRedux][BG|Root/Images/Backgrounds/All/Classroom]Thought: The only thing I can think to do while the teacher lectures on is close my eyes and try to get some rest.")
        fnDialogue("Thought: Not that it's easy to do in this tiny desk. ")
        fnDialogue("[CHARENTER|Teacher1]Teacher: [VOICE|Voice|AdultF][PLAYERNAME]?[P] Did you hear me?")
        fnDialogue("Player: Mrrreeeeh?[P][SFX|UGh] H-uh, yes. Yes I did.")
        fnDialogue("Thought: I lift my head, feeling indentations of the poor stationary that got squashed between my face and the desktop.")
        fnDialogue("Thought: My math teacher exasperatedly waves a piece of paper at me, passed to him by a nervous librarian who waits at the door.")
        fnDialogue("Teacher: [VOICE|Voice|AdultF]You've been called to the library to pick a lost book up.[P] Go and get it, quickly.")
        fnDialogue("Player: Ah.[P] Right.")
        fnDialogue("[HIDEALL]Thought: I can feel eyes on me as I awkwardly shuffle into the hallway. ")
        fnDialogue("[BG|Root/Images/Backgrounds/All/Hallway]Thought: It takes all the way until I've reached the library door to realize, firstly, there's still a pencil stuck to my cheek,")
        fnDialogue("Thought: And also I have no idea what I'm doing.")
        fnDialogue("Thought: [ITALICS]Picking up a lost library book? But I can't even read![ENDITALICS] ")
        fnDialogue("Thought: I should've asked the librarian what the deal was, but...")
        
        fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Bilarry\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
        PairanormalLevel_FlagCheckpoint("Bilarry")
        
        fnDialogue("[BG|Root/Images/Backgrounds/All/LibraryClear]Player: H-[P]hello?")
        fnDialogue("Thought: I have no choice to review the sheet of paper that summoned me.")
        fnDialogue([[Info: [ITALICS]Books to pikc up:: \"Good Lcuk\" by Adam Smith. \"I'm Rtooing for you\" By Adam Smith.[ENDITALICS] ]])
        fnDialogue("Player: ... Adam Smith?[P] Ugh, I don't have the mental capacity to handle this right now.")
        
        fnDialogue("Player: Wait a minute... ADAM, the computer guy![P] Adam, are you still here?")
        fnDialogue("Thought: [SFX|World|Beep]Thought: The new wing of the library lights up like I've thrown a switch. [P][ITALICS]There he is.[ITALICS] ")
        fnDialogue("Thought: [BG|Root/Images/Backgrounds/All/Lab]I make my way into the row of freshly installed computers. They're splayed out with messages in large text--much easier to read now.")
        fnDialogue("[CHARENTER|ADAM]ADAM: [SFX|World|Beep]HI [PLAYERNAME]!!! I've been ugpraded, as you can see XDDD. [P]My processor has never felt so alive!")
        fnDialogue("Player: Hey, Adam. I guess Principal Arroyo hooked you into our new system?")
        fnDialogue("ADAM: YOU BET!! \\\\(*v^)/ Now I've been connected to the internet. ;P download ALL the memes![P] HAHAHA, Get it!?? \\\\(@u@;;)")
        fnDialogue("Player: That's great![P] I'm really happy for you, Adam.")
        fnDialogue("Thought: [ITALICS]What's a meme?[ENDITALICS] ")
        fnDialogue("Player: So... I guess you heard about the adventure I'm going on a little later today, huh?")
        fnDialogue("ADAM: HAHAHA Yeah ;P I can see everything. [P]'3' HOW DO YOU FEEL ABOUT IT?")
        fnDialogue("Player: ... Nervous. [P]Really nervous.[P] I think I'm going to find some really scary stuff down there.")
        fnDialogue("Player: Or worse... I won't find anything. It'll just be another one of Eilette's mysterious tricks.")
        fnDialogue("Player: Ugh, anytime I think about it, I get stressed out.[P] I'm not sure I even [ITALICS]want[ENDITALICS] to go, after all.")
        fnDialogue("ADAM: Whenever I feel @_@;;; overwhelmed @_@;;; by the infinite amount of data I now have access to, [P]I watch cat videos.[P][P][SFX|World|BEEP] ^_^ <3 CATS ARE SO FUNNY!")
        fnDialogue("Thought: The screens become hundreds of moving pictures of cats falling off of various high ledges.")
        fnDialogue("Player: Th-[P]that's a little disturbing.")
        fnDialogue("ADAM: OMG no no no loooolll! XD XD XD They don't die or anything.")
        fnDialogue("ADAM: Clumsy cats are fun to watch because they're normally so graceful! But even when they fall down, they land on their feet.")
        fnDialogue("ADAM: They're just fine in the end. (=^w^=)(=^w^=)(=^w^=)")
        fnDialogue("Player: ...")
        fnDialogue("ADAM: It's okay to be a little scared (TTATT);;.[P][SFX|BEEP] BUT DONT GIVE UP! (>o<)/[P] You're gonna land on your feet!")
        fnDialogue("[HIDEALL]Thought: ...")

        fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"AdumAndPalz\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
        PairanormalLevel_FlagCheckpoint("AdumAndPalz")
        
        fnDialogue("[BG|Null]Thought: I head back to class before I get in trouble.[P] A little confused, to be honest.")
        fnDialogue("Thought: ... But at the very least, grateful for the small distraction this excursion provided me.[P] And the bizarre words of hope for the future.")

    --[Mistress of the Squirrels]
    elseif(iMetMistress == 1.0) then
        fnDialogue("[BG|Root/Images/Backgrounds/All/SchoolExterior][MUSIC|Daytime2]Player: [ITALICS]Whew.[ENDITALICS] ")
        fnDialogue("Thought: [MUSIC|Somber]I can't believe it's only lunchtime. [P]Feels like I've been in class for years.")
        fnDialogue("Thought: The cafeteria was way too loud for me today--when no one was looking, I took my meal out on the school lawn.")
        fnDialogue("Thought: [ITALICS]Not that I have any intention of eating.[ENDITALICS] ")
        fnDialogue("Thought: Who can stomach a sloppy Joe with a life-changing, potentially life-threatening event just hours away?")
        fnDialogue("Player: [P][P][CRASH][P]OUCH!")
        fnDialogue("Thought: Something lands smartly at the top of my head, plopping down on the grass beside me.")
        fnDialogue("Thought: I pick it up [P]-- an acorn?")
        fnDialogue("Player: [CRASH]Ow [CRASH]ow---[P]what [CRASH]the heck!!")
        fnDialogue("Thought: A volley of acorns follow, cascading from the canopy of trees I'm sitting under.")
        fnDialogue("Thought: [SFX|World|Ugh]I'd heard that acorns came from trees, but I suspect it doesn't work like this.")
        fnDialogue("Player: Who's there?")

        fnDialogue("Thought: Up from the branches, three grey shadows neatly jump before me.")
        fnDialogue("Thought: I try not to yelp as cracked doll heads wobble unnaturally on little porcelain bodies.")
        fnDialogue("[CHARENTER|SquirrelBob][CHARENTER|SquirrelPuppet][CHARENTER|SquirrelDoll]Minister: [VOICE|Voice|Eilette]Human. Hello!")
        fnDialogue("KeeKee: [FOCUS|SquirrelDoll][VOICE|Voice|Squirrel]How are you! kee kee.")
        fnDialogue("Player: I don't believe it.[P] You're the squirrels... from the soda factory!")
        fnDialogue("Bob: [VOICE|Voice|Squirrel]And I'm Bob![P][SFX|Squirrel] I have a human's name!")
        fnDialogue("Minister: [FOCUS|SquirrelPuppet][VOICE|Voice|Squirrel]That's right. KIKIKIKI, Our mistress smiles on you with good eyesight.")
        fnDialogue("Player: Uh... what are you doing here?")
        fnDialogue("Minister: [FOCUS|SquirrelPuppet][VOICE|Voice|Squirrel]It is our understanding, kee kee, that you will undertake a great journey.")
        fnDialogue("KeeKee: [FOCUS|SquirrelDoll][VOICE|Voice|Squirrel]We just wanted you to know, we're rooting for you! kikiki!")
        fnDialogue("Bob: [FOCUS|SquirrelBob][VOICE|Voice|Squirrel]We brought you nuts to keep your strength up!")
        fnDialogue("Thought: The tiny doll-clad squirrels tiptoe up to me, their black empty eyes unwavering,[P] and pat my shoe sympathetically.")
        fnDialogue("Thought: It's an image that definitely will fuel several future nightmares, but I still smile.")
        fnDialogue("Player: Thanks guys.")
        fnDialogue("Minister: [FOCUS|SquirrelPuppet][VOICE|Voice|Squirrel]Our Mistress prays for your safety, keekee.[P][SFX|Squirrel] Us squirrels will watch over you always.")
        fnDialogue("[CHAREXIT|SquirrelPuppet]Bob: unless there's a dog nearby!")
        fnDialogue("[CHAREXIT|SquirrelBob]Keekee: [SFX|Squirrel]... Even though you are a frog.")
        fnDialogue("[HIDEALL]Thought: As the messengers disappear back into the trees, I stand up tall, nodding.")
        fnDialogue("Thought: Somehow, I guess I'm going to get through this.")

        fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"SquirrelDelivery\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
        PairanormalLevel_FlagCheckpoint("SquirrelDelivery")

    --[If the player is pals with Laura, but didn't meet ADAM]
    elseif(sBuddy == "Laura") then
        fnDialogue("[MUSIC|Daytime2][BG|Root/Images/Backgrounds/All/Classroom]Thought: The only thing I can think to do while the teacher lectures on is close my eyes and try to get some rest.")
        fnDialogue("Thought: Not that it's easy to do in this tiny desk. ")
        fnDialogue("[CHARENTER|Teacher1]Teacher: [PLAYERNAME]?[P] Did you hear me?")
        fnDialogue("Player: Mrrreeeeh?[P][SFX|UGh] H-uh, yes. Yes I did.")
        fnDialogue("Thought: I lift my head, feeling indentations of the poor stationary that got squashed between my face and the desktop.")
        fnDialogue("Thought: Teacher exasperatedly waves a piece of paper at me, passed to him by a nervous librarian who waits at the door.")
        fnDialogue("Teacher: You've been called to the library to pick a lost book up.[P] Go and get it, quickly.")
        fnDialogue("Player: Ah.[P] Right.")
        fnDialogue("[HIDEALL]Thought: I can feel eyes on me as I awkwardly shuffle into the hallway. ")
        fnDialogue("[BG|Root/Images/Backgrounds/All/Hallway]Thought: It takes all the way until I've reached the library door to realize, firstly, there's still a pencil stuck to my cheek,")
        fnDialogue("Thought: And also I have no idea what I'm doing.")
        fnDialogue("Thought: [ITALICS]Picking up a lost library book? But I can't even read![ENDITALICS] ")
        fnDialogue("Thought: I should've asked the librarian what the deal was, but...")
        
        fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Bilarry2\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
        PairanormalLevel_FlagCheckpoint("Bilarry2")
        
        fnDialogue("[BG|Root/Images/Backgrounds/All/Library]Player: H-[P]hello?")
        fnDialogue("Thought: I have no choice to review the sheet of paper that summoned me.")
        fnDialogue([[Info: [ITALICS]Books to pikc up: \"Good Lcuk\" by Adam Smith. \"I'm Rtooing for you\" By Adam Smith.[ENDITALICS] ]])
        fnDialogue("Player: ... Adam Smith?[P] Ugh, I don't have the mental capacity to handle this right now.")

        fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"AdumAndPalz2\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
        PairanormalLevel_FlagCheckpoint("AdumAndPalz2")
        
        fnDialogue("[BG|Null]Thought: I head back to class before I get in trouble.[P] A little confused, to be honest.")
        fnDialogue("Thought: ... But at the very least, grateful for the small distraction this excursion provided me.[P] And the bizarre words of hope for the future.")

    --[If the player is pals with Bret, but didn't meet mistress]
    elseif(sBuddy == "Bret") then
        fnDialogue("[BG|Root/Images/Backgrounds/All/SchoolExterior][MUSIC|Daytime2]Player: [ITALICS]Whew.[ENDITALICS] ")
        fnDialogue("Thought: I can't believe it's only lunchtime. [P]Feels like I've been in class for years.")
        fnDialogue("Thought: The cafeteria was way too loud for me today--when no one was looking, I took my meal out on the school lawn.")
        fnDialogue("Thought: [ITALICS]Not that I have any intention of eating.[ENDITALICS] ")
        fnDialogue("Thought: Who can stomach a sloppy Joe with a life-changing, potentially life-threatening event just hours away?")
        fnDialogue("Player: [P][P][CRASH][P]OUCH!")
        fnDialogue("Thought: Something lands smartly at the top of my head, plopping down on the grass beside me.")
        fnDialogue("Thought: I pick it up [P]-- an acorn?")
        fnDialogue("Player: [CRASH]Ow [CRASH]ow---[P]what [CRASH]the heck!!")
        fnDialogue("Thought: A volley of acorns follow, cascading from the canopy of trees I'm sitting under.")
        fnDialogue("Thought: [SFX|UGh]I'd heard that acorns came from trees, but I suspect it doesn't work like this.")
        fnDialogue("Player: Who's there?")
        
        fnDialogue("Thought: Nobody responds,[P] but a dark grey shadow skitters from one tree branch high above me, disappearing into the leaves.[P] Weird.")
        fnDialogue("Player: *sigh*...[P]This is no good.")
        fnDialogue("Thought: I stand up and dust myself off. [P]At the very least, some sense has been knocked into me.")
        fnDialogue("Thought: I've got to stay sharp for what's to come. ")

        fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"SquirrelDelivery2\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
        PairanormalLevel_FlagCheckpoint("SquirrelDelivery2")
    
    else
        fnDialogue("[BG|Root/Images/Backgrounds/All/Hallway][MUSIC|Daytime2][SFX|SchoolBell][CHARENTER|Michelle]Michelle:[E|Mad][CRASH] OY![P] Weirdo!")
        fnDialogue("Player: Hmm?[P] Huh!?!")
        fnDialogue("Michelle: [E|N]The bell's rung, you idiot.")
        fnDialogue("Michelle: You've been staring at your open locker for like, five minutes now.[P] Get your arse moving.")
        fnDialogue("Player: Oh.[P] Thanks, Michelle.")
        fnDialogue("Player: [SFX|Blink]Shouldn't you be getting to class, too?")
        fnDialogue("Michelle: [E|Blush]I-I wasn't [ITALICS]watching[ENDITALICS] you, if that's what you're implying.")
        fnDialogue("Michelle: I was just playing hooky like normal, okay?!? [P][CRASH][E|Happy]I'm a rebel, I don't PLAY by the clock's rules.")
        fnDialogue("Player: I wasn't implying anything--")
        fnDialogue("Michelle: [E|N]Anyway I was always wondering, what's in your little locker, anyway?[P] What are your decorations?")
        fnDialogue("Thought: Lockers in the school are painted a bright ocean green, but somehow it never seems to add more color to the hallways.")
        fnDialogue("Thought: As Michelle tries to move past me, I regret not doing anything to make up for that.")
        fnDialogue("Player: Decorations--uhh, I didn't know we could do stuff like that. ")
        fnDialogue("Michelle: [E|Shock]Course it's not technically [ITALICS]allowed[ENDITALICS], but just about everybody does it.[P][E|Happy] I did mine in permanent marker, hehe.")
        fnDialogue("Player: I just keep my books in here.[P] I mean, there is one--")
        fnDialogue("Michelle: [E|Shock]Hey,[P] is that a photo of the club???")
        fnDialogue("Player: [SPEEDTEXT][CRASH]I FOUND IT ON THE FLOOR SOMEBODY MUST HAVE CUT IT OUT OF A YEARBOOK, NOT ME THOUGH.[P] I-I WAS GOING TO GIVE IT BACK!!![ENDSPEEDTEXT] ")
        fnDialogue("Thought: I slam the locker shut so fast, it almost clips the tip of Michelle's nose.")
        fnDialogue("Player: T-time to get to class! See ya!")
        fnDialogue("Michelle: [E|Mad]Hey--!")
        fnDialogue("[HIDEALL]Thought: Instead of bolting to my next assigned classroom, I duck into the next empty hallway.[P] The photo is clutched tightly in my hands.")
        fnDialogue("Player: I'm going to be fine.[P] I'm going to be okay.")
        fnDialogue("Thought: I couldn't really explain to Michelle how scared I was.")
        fnDialogue("Thought: But... she was there, wasn't she? [P]I look down at the scrap of paper I cut out of a library yearbook.")
        fnDialogue("Thought: Six smiling faces--each one so different, but delightful. They've all been there, looking out for me.")
        fnDialogue("Player: I...[P][ITALICS]We're[ENDITALICS][P] gonna be fine.")
        fnDialogue("Thought: For the first time today, I finally believed it.")

    end

    fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Contoober\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
    PairanormalLevel_FlagCheckpoint("Contoober")
    
    fnDialogue("[BG|Null][MUSIC|Null][HIDEALL]Thought: ...")
    fnDialogue("[BG|Root/Images/Backgrounds/All/SchoolExterior][Music|Investigation]Thought: I kick some dirt, watching it build up as I drag my foot through the soil.")
    fnDialogue("Player: Almost time...")
    fnDialogue("Thought: My stomach is so tight. I dread to think what sort of things we might find [P]... there.[P] It was dark enough to start a secret war, wasn't it?")
    fnDialogue("Thought: The statue of Kane McKay Foster and his pleasant but vacant smile looms over me.")
    fnDialogue("Thought: [ITALICS]This is all your fault, you know[ENDITALICS].")
    fnDialogue("Thought: I eye the bronze effigy suspiciously--as if I could spot some faint evil characteristic molded in the metal.")
    fnDialogue("Thought: [ITALICS]Also, why isn't Eilette coming along?[P] Is there something dangerous there?[ENDITALICS] ")
    fnDialogue("Player: Pff. She's probably sending us all to our doom to be rid of our nosy--")
    fnDialogue("[CHARENTER|Mari]Mari: [E|Happy]Fooooooound iiiiiiit!")
    fnDialogue("Player: Mari.[P] Guys.")
    fnDialogue("[CHARENTER|Laura][CHARENTER|Michelle][CHARENTER|Stacy][CHARENTER|Bret]Thought: The whole club trails behind their captain.")
    fnDialogue("Thought: Mari waves a crinkled piece of paper triumphantly in the air.")
    fnDialogue("Laura: [E|N]Hi, [PLAYERNAME].")

    --[Helped with the Gym]
    local iDecoratedGym = VM_GetVar("Root/Variables/ChapterVars/Chapter4/iDecoratedGym", "N")
    if(iDecoratedGym == 1.0) then
        VM_SetVar("Root/Variables/ChapterVars/Chapter5/iToddAlong", "N", 1.0)
        fnDialogue("Mari: [E|N]It wasn't easy,[E|Shock] but I got it! ")
        fnDialogue("Player: Got what?")
        fnDialogue("Mari: [E|Happy][ITALICS]The map[ENDITALICS].")
        fnDialogue("Stacy: [E|N]The seniors like to raid the quartz caves near the mines for their parties.")
        fnDialogue("Stacy: [E|Blush]I remembered since, you know, I've been to a couple here and there.[P] No big deal.")
        fnDialogue("Mari: [E|Shock]And their map actually marks the entrance to the mine.[P] It's our best way in.")
        fnDialogue("[CHARENTER|Harper]Harper: [E|N]You're [ITALICS]wel-come[ENDITALICS], by the way.")
        fnDialogue("Player: Harper?")
        fnDialogue("Harper: [E|N]That's right.[P] Stacy may be very cool, but you need top notch con-nec-tions if you want to get [ITALICS]the map[ENDITALICS].")
        fnDialogue("Harper: [E|Blush] Enter me.[P][E|Happy] That's a precious artifact you've got there.")
        fnDialogue("Mari: [E|Happy]Thank you Harpeeeeeeerrrrr!!!!!!")
        fnDialogue("Player: Seriously?[P] Wow...[P]thanks.")
        fnDialogue("Harper: [E|Sad]Consider it me returning the favor since, you know...[E|N]You like, helped de-co-rate the gym and everything.")
        fnDialogue("Harper: Of course, I had to do it on one condition.[P] [ITALICS]He's[ENDITALICS] gonna keep an eye on it.")
        
        fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Harpers\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
        PairanormalLevel_FlagCheckpoint("Harpers")
        
        fnDialogue("[HIDEALL]Thought: ...")
        fnDialogue("[CHARENTER|Todd]Todd: [E|N]...")
        fnDialogue("Player: Hi! Todd!")
        fnDialogue("Todd: [E|Sad][PLAYERNAME]. Uhhh, hi.[P] Good to see you since--")
        fnDialogue("Player: Since you stopped being my tutor after I was kidnapped and needed a friend? ")
        fnDialogue("Todd: [E|Sad]*ahem*.[P] Yeahhh, that.[P] Uh, n-no hard feelings?")
        fnDialogue([[ [CHARENTER|Mari]Mari: [E|Blush][ITALICS]This is just like the opening of Chapter 54 of \"High School Demon Lovers\"![ENDITALICS] ]])
        fnDialogue("Mari: [E|Shock][ITALICS]Harper, you truly are a master of drama[ENDITALICS].")
        fnDialogue("[CHARENTER|Harper]Harper: [E|Happy][ITALICS]Thank you, yes.[P] I live to manufacture real-life soap operas[ENDITALICS].")

    else
        fnDialogue("Mari: [E|N]It wasn't easy, but I got it![P] I had to bribe a senior with 20 bucks.")
        fnDialogue("Bret: [E|Sad][ITALICS]My[ENDITALICS] 20 bucks.")
        fnDialogue("Michelle: [E|Happy]Cheer up, Bret.[P] It was for a good cause.")
        fnDialogue("Bret: [E|Mad]No, it was [ITALICS]for[ENDITALICS] a pristine 'Legend of Triforce:: Lonk to the Penns' cartridge I was gonna buy afterschool...")
        fnDialogue("Bret: [E|Shock]Although.[P][E|Sad] I guess I'm not gonna make it back before GameCycle closes today, anyway.")
        fnDialogue("Player: [P][P]Is anyone going to explain--")
        fnDialogue("Mari: [E|Shock][CRASH]ITS A MAP!")
        fnDialogue("Stacy: [E|N]The seniors like to raid the quartz caves near the mines for their parties.")
        fnDialogue("Stacy: [E|Blush]I remembered since, you know, I've been to a couple here and there.[P] No big deal.")
        fnDialogue("Mari: [E|Shock]And their map actually marks the entrance to the mine. It's our best way in.")
        fnDialogue("Player: So, this is really happening, huh? [P]Thanks, you guys...[P]for everything.")
        fnDialogue("Mari: [E|Happy]Are you kidding?[P][CRASH] [ITALICS]WE[ENDITALICS] should be thanking [ITALICS]you[ENDITALICS].")
        fnDialogue("Mari: This is the best investigation we've ever been on!")

    end
    fnDialogue("[HIDEALL]Player: [SFX|Ugh]Glad you think so.")
    fnDialogue("Player: Uh... is everyone ready, then?")
    fnDialogue("[CHARENTER|Michelle]Michelle: [E|N]Yep.")
    fnDialogue("[CHAREXIT|Michelle][CHARENTER|Bret]Bret: [E|Happy]Reporting for duty!")
    fnDialogue("[CHAREXIT|Bret][CHARENTER|Mari][CHARENTER|Stacy]Mari: [E|Shock][CRASH]HELL YEAH!")
    fnDialogue("Stacy: [E|Mad][CRASH] DOUBLE HELL YEAH.")
    fnDialogue("[CHAREXIT|Mari][CHAREXIT|Stacy][CHARENTER|Laura]Laura: [E|Mad]y...[P]yes!")
    fnDialogue("Player: You're coming too, Laura?")
    fnDialogue("Laura: [P][P][E|Blush]... Don't ask me t-that![P][SFX|Ugh] Or I'll *hrm* change my mind again.")
    fnDialogue("Player: Heh, fair enough.")
    fnDialogue("Player: Okay.[P] Let's go.")

    fnCutsceneInstruction("PairanormalLevel_SaveCheckpoint(\"Quartzentine\", \"" .. LM_GetCallStack(0) .. "\", \"" .. sTopicName .. "\") ")
    PairanormalLevel_FlagCheckpoint("Quartzentine")
    
    fnDialogue("[HIDEALL][BG|Null][Music|Null]Thought: ...")
    fnDialogue("[BG|Root/Images/Backgrounds/All/Walkhome]Thought: ...")
    fnDialogue("[BG|Null]Thought: ...")
    fnDialogue("[BG|Root/Images/Backgrounds/All/Forest]Thought: ...")
    fnDialogue("[BG|Null]Thought: ...")
    fnDialogue("[BG|Root/Images/Backgrounds/All/Cave]Thought: ...")
    fnDialogue("[CHARENTER|Mari]Mari: [E|Sad][PLAYERNAME], are you sure you want to do this?")
    fnDialogue("[CHARENTER|Stacy]Stacy: [E|Shock]We can turn back [ITALICS]any[ENDITALICS] time, okay? Just say the word.")
    fnDialogue("Player: Thanks, guys. But there's no avoiding this. Let's just go in.")
    fnDialogue("Mari: ... Right.")
    fnDialogue("[MUSIC|Love][BG|Root/Images/Backgrounds/All/QuartzBubble]Thought: ...")
    
    --Todd has come along.
    if(iDecoratedGym == 1.0) then
        fnDialogue("[CHARENTER|Michelle][CHARENTER|Bret][CHARENTER|Laura][CHARENTER|Todd][EMOTION|Todd|Shock][EMOTION|Bret|Shock][EMOTION|Stacy|Shock][EMOTION|Michelle|Shock][EMOTION|Mari|Shock][EMOTION|Laura|Shock]Laura: [E|Shock]W-wow...")

    --No todd.
    else
        fnDialogue("[CHARENTER|Michelle][CHARENTER|Bret][CHARENTER|Laura][EMOTION|Bret|Shock][EMOTION|Stacy|Shock][EMOTION|Michelle|Shock][EMOTION|Mari|Shock][EMOTION|Laura|Shock]Laura: [E|Shock]W-wow...")
    
    end
    fnDialogue("Bret: [E|Shock]Oh, cool.")
    fnDialogue("Mari: [E|Shock]So... much...[P][E|Happy][CRASH]QUARTZ!")
    fnDialogue("Player: I don't believe it.")
    fnDialogue("Thought:[HIDEALL] I wasn't expecting such a marvelous interruption to all my anxieties.")
    fnDialogue("Thought: The cave walls are completely lined with countless glittering fragments of quartz.")
    fnDialogue("Thought: As we step inside, I feel a strange energy humming all around me. It's as though...")
    LM_ExecuteScript(gsRoot .. "../Chapter 5/Gameplay_SceneA/3 - Quartz.lua", "Start")
end
