SugarLumps.exe -Redirect "BuildImagesLog.txt" -Call BuildImages.lua
cd Output/
if exist "Backgrounds.slf" (
	copy "Backgrounds.slf" "../../../Games/Pairanormal/Datafiles/Backgrounds.slf"
	del "Backgrounds.slf"
	echo "Copying Backgrounds.slf"
)
if exist "Characters.slf" (
	copy "Characters.slf" "../../../Games/Pairanormal/Datafiles/Characters.slf"
	del "Characters.slf"
	echo "Copying Characters.slf"
)
if exist "Scenes.slf" (
	copy "Scenes.slf" "../../../Games/Pairanormal/Datafiles/Scenes.slf"
	del "Scenes.slf"
	echo "Copying Scenes.slf"
)
if exist "UI.slf" (
	copy "UI.slf" "../../../Games/Pairanormal/Datafiles/UI.slf"
	del "UI.slf"
	echo "Copying UI.slf"
)