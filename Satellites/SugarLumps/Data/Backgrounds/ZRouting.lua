--[Adventure Scenes]
--Used for CGs and transformation sequences in Adventure Mode.
local sBasePath = fnResolvePath()

--[Setup]
SLF_Open("Output/Backgrounds.slf")
ImageLump_SetCompression(1)
if(gbUseLowDefinition) then
	ImageLump_SetScales(0.5, 0.5, 0, 0)
end

--[Backgrounds]
ImageLump_Rip("CharPortraits", sBasePath .. "CharPortraits.png", 0, 0, -1, -1, 0)
local saList = {}
saList[1] = {"BasketballCourt", "Bathroom", "Bedroom", "BedroomNight", "BedroomNightSacked", "Burning", "Cafe", "CandaceLivingRoom", "Car", "Cave", "Classroom", "ClassroomNoStuff", "Cliff"}
saList[2] = {"ClubHouse", "ComputerCloset", "ComputerClosetDark", "FlashbackCafe", "Forest", "Gate", "Grafitti", "Graveyard", "Gym", "GymFair", "Hallway", "Hospital", "KaneKolaFactory", "Kitchen"}
saList[3] = {"Lab", "Library", "LibraryClear", "LivingRoom", "LivingRoomEvening", "LivingRoomNight", "MineAgentRoom", "MineLobby", "MineQuartzRoom", "MineStorage", "MineTwinRoom"}
saList[4] = {"Museum", "MuseumOffice", "Office", "QuartzBubble", "RunHome", "SchoolExterior", "SchoolExteriorGrad", "ShadowBuilding", "SidewalkNight", "SodaFactory", "SquirrelCouncil", "StacyRoom", "Sunset", "RunHome", "Thunderstorm", "WalkHome"}
saList[5] = {"EndOfChapterOne", "EndOfChapterTwo", "EndOfChapterThree", "EndOfChapterFour"}
for i = 1, #saList, 1 do
    for p = 1, #saList[i], 1 do
        ImageLump_Rip(saList[i][p], sBasePath .. saList[i][p] .. ".png", 0, 0, -1, -1, 0)
    end
end
 
--[Finish]
ImageLump_SetScales(1.0, 1.0, 0, 0)

--These images always rip at full resolution.
--ImageLump_Rip("Classroom",         sBasePath .. "Classroom.png",         0, 0, -1, -1, 0)
--ImageLump_Rip("ClubHouse",         sBasePath .. "ClubHouse.png",         0, 0, -1, -1, 0)
--ImageLump_Rip("Library",           sBasePath .. "Library.png",           0, 0, -1, -1, 0)
--ImageLump_Rip("LivingRoom",        sBasePath .. "LivingRoom.png",        0, 0, -1, -1, 0)
SLF_Close()