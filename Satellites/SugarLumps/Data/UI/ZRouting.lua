--[Adventure Scenes]
--Used for CGs and transformation sequences in Adventure Mode.
local sBasePath = fnResolvePath()

--[Setup]
SLF_Open("Output/UI.slf")
ImageLump_SetCompression(1)

--[UI Stuff]
ImageLump_Rip("Icon|XRed",               sBasePath .. "ButtonSheet.png",    0,   7, 112,  97, 0)
ImageLump_Rip("Icon|ArrowLftGreen",      sBasePath .. "ButtonSheet.png",  123,   0, 140, 121, 0)
ImageLump_Rip("Icon|XWhite",             sBasePath .. "ButtonSheet.png",  290,   7, 112,  97, 0)
ImageLump_Rip("Icon|ArrowWhite",         sBasePath .. "ButtonSheet.png",  414,   0, 140, 121, 0)
ImageLump_Rip("Icon|GearWhite",          sBasePath .. "ButtonSheet.png",  588,   9,  94,  92, 0)
ImageLump_Rip("Icon|CheckboxEmptyWhite", sBasePath .. "ButtonSheet.png",  711,   3, 116, 115, 0)
ImageLump_Rip("Icon|CheckboxFillWhite",  sBasePath .. "ButtonSheet.png",  854,   3, 146, 115, 0)
ImageLump_Rip("Icon|CheckboxCheckWhite", sBasePath .. "ButtonSheet.png", 1014,   3, 124,  93, 0)
ImageLump_Rip("Icon|TrashWhite",         sBasePath .. "ButtonSheet.png",    2, 154, 108, 148, 0)
ImageLump_Rip("Icon|ArrowSmallWhiteL",   sBasePath .. "ButtonSheet.png",  168, 178,  51, 102, 0)
ImageLump_Rip("Icon|ArrowSmallWhiteR",   sBasePath .. "ButtonSheet.png",  265, 178,  52, 102, 0)
ImageLump_Rip("Icon|ArrowPlayWhite",     sBasePath .. "ButtonSheet.png",  370, 169,  88, 106, 0)
ImageLump_Rip("Icon|ArrowFFWhite",       sBasePath .. "ButtonSheet.png",  499, 169, 179, 106, 0)
ImageLump_Rip("Icon|VolumeWhite",        sBasePath .. "ButtonSheet.png",  729, 178,  85, 102, 0)
ImageLump_Rip("Icon|VolumeBigWhite",     sBasePath .. "ButtonSheet.png",  892, 177, 159, 103, 0)
ImageLump_Rip("Icon|BarEmpty",           sBasePath .. "ButtonSheet.png",   36, 371,1063,  78, 0)
ImageLump_Rip("Icon|BarIndicator",       sBasePath .. "ButtonSheet.png", 1141, 355,  41, 102, 0)
ImageLump_Rip("Icon|BarFull",            sBasePath .. "ButtonSheet.png",   53, 499,1029,  23, 0)
ImageLump_Rip("Icon|Frame",              sBasePath .. "ButtonSheet.png",   17, 586, 404, 389, 0)
ImageLump_Rip("Icon|Sun",                sBasePath .. "ButtonSheet.png",  488, 590, 187, 190, 0)
ImageLump_Rip("Icon|Moon",               sBasePath .. "ButtonSheet.png",  711, 604, 143, 161, 0)
ImageLump_Rip("Icon|House",              sBasePath .. "ButtonSheet.png",  906, 596, 181, 158, 0)
ImageLump_Rip("Icon|MagnifyingGlass",    sBasePath .. "ButtonSheet.png",  484, 814, 177, 176, 0)
ImageLump_Rip("Icon|Hand",               sBasePath .. "ButtonSheet.png",  706, 814, 153, 156, 0)

--[Boxes]
ImageLump_Rip("Box|Black",    sBasePath .. "BoxesSheet.png",   0,   0, 598, 598, 0)
ImageLump_Rip("Box|White",    sBasePath .. "BoxesSheet.png", 600,   0, 598, 598, 0)
ImageLump_Rip("Box|Gradient", sBasePath .. "BoxesSheet.png",   0, 600, 598, 598, 0)

--[Derived Pieces]
--Common.
ImageLump_Rip("Derived|Common|Back",     sBasePath .. "Derived/Common/Back.png",     0, 0, -1, -1, 0)
ImageLump_Rip("Derived|Common|Close",    sBasePath .. "Derived/Common/Close.png",    0, 0, -1, -1, 0)
ImageLump_Rip("Derived|Common|Settings", sBasePath .. "Derived/Common/Settings.png", 0, 0, -1, -1, 0)

--Dialogue.
ImageLump_Rip("Derived|Dialogue|BorderBarsTop", sBasePath .. "Derived/Dialogue/BorderBarsTop.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Derived|Dialogue|BorderBarsBot", sBasePath .. "Derived/Dialogue/BorderBarsBot.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Derived|Dialogue|ContinueIcon",  sBasePath .. "Derived/Dialogue/ContinueIcon.png",  0, 0, -1, -1, 0)
ImageLump_Rip("Derived|Dialogue|DecisionBlack", sBasePath .. "Derived/Dialogue/DecisionBlack.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Derived|Dialogue|DecisionWhite", sBasePath .. "Derived/Dialogue/DecisionWhite.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Derived|Dialogue|DecisionBlackHf", sBasePath .. "Derived/Dialogue/DecisionBlackHf.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Derived|Dialogue|DecisionWhiteHf", sBasePath .. "Derived/Dialogue/DecisionWhiteHf.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Derived|Dialogue|DialogueBlack", sBasePath .. "Derived/Dialogue/DialogueBlack.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Derived|Dialogue|DialogueWhite", sBasePath .. "Derived/Dialogue/DialogueWhite.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Derived|Dialogue|InfoBoxBot",    sBasePath .. "Derived/Dialogue/InfoBoxBot.png",    0, 0, -1, -1, 0)
ImageLump_Rip("Derived|Dialogue|InfoBoxTop",    sBasePath .. "Derived/Dialogue/InfoBoxTop.png",    0, 0, -1, -1, 0)
ImageLump_Rip("Derived|Dialogue|NameBarLft",    sBasePath .. "Derived/Dialogue/NameBarLft.png",    0, 0, -1, -1, 0)
ImageLump_Rip("Derived|Dialogue|NameBarRgt",    sBasePath .. "Derived/Dialogue/NameBarRgt.png",    0, 0, -1, -1, 0)
ImageLump_Rip("Derived|Dialogue|Underbar",      sBasePath .. "Derived/Dialogue/Underbar.png",      0, 0, -1, -1, 0)

--Disclaimer Stuff.
ImageLump_Rip("Derived|Disclaimer",                        sBasePath .. "Derived/Disclaimer/Disclaimer.png",                         0, 0, -1, -1, 0)
ImageLump_Rip("Derived|DisclaimerBtn|Ready|Selected",      sBasePath .. "Derived/Disclaimer/Disclaimer_Btn_Ready_Selected.png",      0, 0, -1, -1, 0)
ImageLump_Rip("Derived|DisclaimerBtn|Ready|Unselected",    sBasePath .. "Derived/Disclaimer/Disclaimer_Btn_Ready_Unselected.png",    0, 0, -1, -1, 0)
ImageLump_Rip("Derived|DisclaimerBtn|NotReady|Selected",   sBasePath .. "Derived/Disclaimer/Disclaimer_Btn_NotReady_Selected.png",   0, 0, -1, -1, 0)
ImageLump_Rip("Derived|DisclaimerBtn|NotReady|Unselected", sBasePath .. "Derived/Disclaimer/Disclaimer_Btn_NotReady_Unselected.png", 0, 0, -1, -1, 0)

--Investigation.
ImageLump_Rip("Derived|Investigation|Arrows",                 sBasePath .. "Derived/Investigation/Arrows.png",                 0, 0, -1, -1, 0)
ImageLump_Rip("Derived|Investigation|ButtonBlack",            sBasePath .. "Derived/Investigation/ButtonBlack.png",            0, 0, -1, -1, 0)
ImageLump_Rip("Derived|Investigation|ButtonWhite",            sBasePath .. "Derived/Investigation/ButtonWhite.png",            0, 0, -1, -1, 0)
ImageLump_Rip("Derived|Investigation|CloseupBacking",         sBasePath .. "Derived/Investigation/CloseupBacking.png",         0, 0, -1, -1, 0)
ImageLump_Rip("Derived|Investigation|GotSomeToShowMeBox",     sBasePath .. "Derived/Investigation/GotSomeToShowMeBox.png",     0, 0, -1, -1, 0)
ImageLump_Rip("Derived|Investigation|GotSomeToShowMeNameBox", sBasePath .. "Derived/Investigation/GotSomeToShowMeNameBox.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Derived|Investigation|Hand",                   sBasePath .. "Derived/Investigation/Hand.png",                   0, 0, -1, -1, 0)
ImageLump_Rip("Derived|Investigation|House",                  sBasePath .. "Derived/Investigation/House.png",                  0, 0, -1, -1, 0)
ImageLump_Rip("Derived|Investigation|IconBacking",            sBasePath .. "Derived/Investigation/IconBacking.png",            0, 0, -1, -1, 0)
ImageLump_Rip("Derived|Investigation|ItemNameBacking",        sBasePath .. "Derived/Investigation/ItemNameBacking.png",        0, 0, -1, -1, 0)
ImageLump_Rip("Derived|Investigation|MagnifyingLens",         sBasePath .. "Derived/Investigation/MagnifyingLens.png",         0, 0, -1, -1, 0)
ImageLump_Rip("Derived|Investigation|MoreDiaLft",             sBasePath .. "Derived/Investigation/MoreDiaLft.png",             0, 0, -1, -1, 0)
ImageLump_Rip("Derived|Investigation|MoreDiaRgt",             sBasePath .. "Derived/Investigation/MoreDiaRgt.png",             0, 0, -1, -1, 0)
ImageLump_Rip("Derived|Investigation|TimerBarBlack",          sBasePath .. "Derived/Investigation/TimerBarBlack.png",          0, 0, -1, -1, 0)
ImageLump_Rip("Derived|Investigation|TimerBarWhite",          sBasePath .. "Derived/Investigation/TimerBarWhite.png",          0, 0, -1, -1, 0)

--Loading
ImageLump_Rip("Derived|LoadMenu|Backing",         sBasePath .. "Derived/LoadMenu/Backing.png",         0, 0, -1, -1, 0)
ImageLump_Rip("Derived|LoadMenu|DeleteBtn",       sBasePath .. "Derived/LoadMenu/DeleteBtn.png",       0, 0, -1, -1, 0)
ImageLump_Rip("Derived|LoadMenu|Header",          sBasePath .. "Derived/LoadMenu/Header.png",          0, 0, -1, -1, 0)
ImageLump_Rip("Derived|LoadMenu|Pencil",          sBasePath .. "Derived/LoadMenu/Pencil.png",          0, 0, -1, -1, 0)
ImageLump_Rip("Derived|LoadMenu|PlayBtn",         sBasePath .. "Derived/LoadMenu/PlayBtn.png",         0, 0, -1, -1, 0)
ImageLump_Rip("Derived|LoadMenu|SelectedBacking", sBasePath .. "Derived/LoadMenu/SelectedBacking.png", 0, 0, -1, -1, 0)

--Settings Menu.
ImageLump_Rip("Derived|Settings|Backing",          sBasePath .. "Derived/Settings/Backing.png",          0, 0, -1, -1, 0)
ImageLump_Rip("Derived|Settings|BoxChecked",       sBasePath .. "Derived/Settings/BoxChecked.png",       0, 0, -1, -1, 0)
ImageLump_Rip("Derived|Settings|BoxUnchecked",     sBasePath .. "Derived/Settings/BoxUnchecked.png",     0, 0, -1, -1, 0)
ImageLump_Rip("Derived|Settings|HeaderBox",        sBasePath .. "Derived/Settings/HeaderBox.png",        0, 0, -1, -1, 0)
ImageLump_Rip("Derived|Settings|LowerBtnAccept",   sBasePath .. "Derived/Settings/LowerBtnAccept.png",   0, 0, -1, -1, 0)
ImageLump_Rip("Derived|Settings|LowerBtnClose",    sBasePath .. "Derived/Settings/LowerBtnClose.png",    0, 0, -1, -1, 0)
ImageLump_Rip("Derived|Settings|LowerBtnDefaults", sBasePath .. "Derived/Settings/LowerBtnDefaults.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Derived|Settings|VolumeSliders",    sBasePath .. "Derived/Settings/VolumeSliders.png",    0, 0, -1, -1, 0)
 
--Title Menu and Gallery. 
ImageLump_Rip("Derived|Title|BoxBlack",         sBasePath .. "Derived/TitleMenu/BoxBlack.png",         0, 0, -1, -1, 0)
ImageLump_Rip("Derived|Title|BoxWhite",         sBasePath .. "Derived/TitleMenu/BoxWhite.png",         0, 0, -1, -1, 0)
ImageLump_Rip("Derived|Title|GalleryBlackBack", sBasePath .. "Derived/TitleMenu/GalleryBlackBack.png", 0, 0, -1, -1, 0)
ImageLump_Rip("Derived|Title|GalleryBack",      sBasePath .. "Derived/TitleMenu/GalleryBack.png",      0, 0, -1, -1, 0)
ImageLump_Rip("Derived|Title|GalleryLft",       sBasePath .. "Derived/TitleMenu/GalleryLft.png",       0, 0, -1, -1, 0)
ImageLump_Rip("Derived|Title|GalleryRgt",       sBasePath .. "Derived/TitleMenu/GalleryRgt.png",       0, 0, -1, -1, 0)
ImageLump_Rip("Derived|Title|TitleCard",        sBasePath .. "Derived/TitleMenu/TitleCard.png",        0, 0, -1, -1, 0)

--[Overlays]
ImageLump_Rip("VHSOverlay",     sBasePath .. "VHSOverlay.png",     0, 0, -1, -1, 0)
ImageLump_Rip("CreepyOverlay0", sBasePath .. "CreepyOverlay0.png", 0, 0, -1, -1, 0)
ImageLump_Rip("CreepyOverlay1", sBasePath .. "CreepyOverlay1.png", 0, 0, -1, -1, 0)
ImageLump_Rip("CreepyOverlay2", sBasePath .. "CreepyOverlay2.png", 0, 0, -1, -1, 0)
ImageLump_Rip("CreepyOverlay3", sBasePath .. "CreepyOverlay3.png", 0, 0, -1, -1, 0)

--[Finish]
SLF_Close()