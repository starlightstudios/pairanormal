--[Large Images for Scenes]
--Large images shown during scene examination or special cutscenes.
local sBasePath = fnResolvePath()

--[Setup]
SLF_Open("Output/Scenes.slf")
ImageLump_SetCompression(1)
if(gbUseLowDefinition) then
	ImageLump_SetScales(0.5, 0.5, 0, 0)
end

--[Major Scenes]
--Setup
local saList = {}
saList[1] = {"BiminiLogo", "Bret", "CH2_AJEscape", "CH2_BretGym", "CH2_LauraComputer", "CH2_MariGym", "CH2_MariNecklace", "CH2_MichelleGym", "CH2_MichelleSquash", "CH2_NurseOffice", "CH2_PictureArroyo", "CH2_PictureFoster"}
saList[2] = {"CH2_Shack", "DogBelly", "EiletteConfrontation", "EiletteHit", "EiletteKapow", "FriendEnd", "FrogPhoto", "Hospital_Mari", "HospitalCry", "HospitalHug", "IntroTodd", "KaneFire", "KaneFire2"}
saList[3] = {"KolaBottle", "LauraCrush", "LoveNote", "MariCrush", "MariWeep", "Missing", "NewEilette", "Pyramid", "RollerDerby", "Sandwich", "Stacy", "StacyBricks", "StandingOver", "Todd", "TwinBirth"}
for i = 1, #saList, 1 do
    for p = 1, #saList[i], 1 do
        ImageLump_Rip(saList[i][p], sBasePath .. saList[i][p] .. ".png", 0, 0, -1, -1, 0)
    end
end
 
--[Finish]
SLF_Close()
ImageLump_SetScales(1.0, 1.0, 0, 0)