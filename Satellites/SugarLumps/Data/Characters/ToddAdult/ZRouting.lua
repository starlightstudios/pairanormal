--[Todd's Images]
--For now we use static images.
local sBasePath = fnResolvePath()
ImageLump_SetBlockTrimmingFlag(true)
ImageLump_Rip("ToddAdult|Expression0", sBasePath .. "Expression0.png", 0, 0, -1, -1, 0)
ImageLump_Rip("ToddAdult|Expression1", sBasePath .. "Expression1.png", 0, 0, -1, -1, 0)
ImageLump_Rip("ToddAdult|Expression2", sBasePath .. "Expression2.png", 0, 0, -1, -1, 0)
ImageLump_SetBlockTrimmingFlag(false)