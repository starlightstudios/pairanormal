-- |[AJ's Backpack Images]|
--Used for the parts where AJ is hiding in a backpack.
local sBasePath = fnResolvePath()
ImageLump_SetBlockTrimmingFlag(true)
ImageLump_Rip("AJBackpack|Expression0", sBasePath .. "Expression0.png", 0, 0, -1, -1, 0)
ImageLump_SetBlockTrimmingFlag(false)