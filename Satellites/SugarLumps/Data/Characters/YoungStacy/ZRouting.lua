--[YoungStacy's Images]
--For now we use static images.
local sBasePath = fnResolvePath()
ImageLump_SetBlockTrimmingFlag(true)
ImageLump_Rip("YStacy|Expression0", sBasePath .. "Expression0.png", 0, 0, -1, -1, 0)
ImageLump_SetBlockTrimmingFlag(false)